(function()
{
    "use strict";

    var page = document.body.className,
        colors = ["#bdeaf8","#7cd4f2","#51c6ed","#00b9f2"],
        colorc = { "over":"#e73f51", "under":"#6eb954", "normal":"#fc0" },
        chart, figure, params = [],
        defaultSeason = document.querySelector("meta").getAttribute("content"),
        URLlocal = document.querySelector("base").getAttribute("title"),
        URLline =  URLlocal + "img/line.png",
        URLlogo = URLlocal + "img/repucom.png",
        URLfilter = "/data/sponsorships/",
        URLscore = "/data/scorecard/",
        URLrevenue = "/data/revenue/",
        URLinsights = "/data/insight/",
        inputs = ["seasons","leagues","sponsors","industries","clubs","afp","mfv","roi","bgr","sos","ranking","uefarank","titles","stadium","tvquota","broadcast","social","infra","market","fan","interest","image"],
        ageRanges = [54,49,44,39,34,29,24,19,14,9,4],
        barAdjust = 28+28,
        barHeight = 55;
    
    var ajax ={
        json:false,
        jsonew:false,
        obj:false,
        async:false,
        callback:false,
        init:function()
        {
            if( window.XMLHttpRequest )  
                this.obj = new XMLHttpRequest();
            else if( window.ActiveXObject ) 
                this.obj = new ActiveXObject("Microsoft.XMLHTTP");
            this.obj.onreadystatechange = function()
            {
                if( (this.readyState == 4) && (this.status == 200) )
                    ajax.complete();
            }
        },

        get:function( url, callback )
        {
            this.callback = callback || false;
            this.obj.open("GET", url, this.async );
            this.obj.send(null);
        },

        complete:function()
        {
            if(this.jsonew)
                this[ this.jsonew ] = JSON.parse( this.obj.responseText );
            else
                this.json = JSON.parse( this.obj.responseText );

            if(this.callback)
                this.callback();
        }
    }

    var slider = {
        draggable:false,
        tag:false,
        index:0,
        coord:false,
        position:false,
        adjust:50,
        bar:false,
        init:function()
        {
            this.tooltip = document.querySelector("#tooltip");
            this.tooltip.input = this.tooltip.querySelector("input");

            document.onmousemove = function(e)
            {
                if( slider.draggable )
                {
                    var position, afp;
                    position = e.clientX + window.pageXOffset - slider.coord.x - slider.adjust;

                    if( position < 0 )
                        position = 0;
                    if( position > slider.canvasSize)
                        position = slider.canvasSize;   

                    afp = Math.round( position / ( slider.canvasSize/slider.maxValue ) );
                    
                    slider.moveCircle( afp );
                    slider.tooltip.style.left = (position + slider.adjust/2) + "px";
                    slider.tooltip.input.value = afp.format();
                }
            }

            document.onmouseup = function(e)
            {
                if(slider.draggable)
                {
                    document.querySelector("rect.active").removeAttribute("class");

                    figure.rows[ slider.index ].className += " mod";

                    for(var i in ajax.json.data)
                        figure.bgr[i].setAttribute("title",ajax.json.data[i].bgr );     //update titles

                    storage.update("table", figure.rows[ slider.index ], ajax.json.data[ slider.index ].afp );              
                }

                slider.draggable = false;
            }

            figure.updateBars = function( data, afp )
            {
                var newSize;

                for(var i in data)
                {
                    data[i].bgr = parseInt( ((figure.afpTotal + afp)/figure.mfvTotal) * data[i].mfv );
                    figure.bgr[i].textContent = setNumber( data[i].bgr, "abbr" );
                      
                    newSize = (slider.canvasSize - (data[i].bgr/slider.ratio)) - chart.series[1].data[i].plotHigh; 

                    if(newSize < 1 )
                        newSize = 1;

                    figure.bars[i].setAttribute("height", newSize);
                }
            }

            figure.updateColors = function( current )
            {
                if( current.afp < (current.mfv/2) )
                {
                    figure.circles[ slider.index ].setAttribute("class","under");
                    figure.roi.parentNode.className = "roi under";
                    slider.tooltip.className = "under";
                }
                else if( current.afp > current.mfv )
                {
                    figure.circles[ slider.index ].setAttribute("class","over");
                    figure.roi.parentNode.className = "roi over";
                    slider.tooltip.className = "over";
                }
                else
                {
                    figure.circles[ slider.index ].setAttribute("class","normal");
                    figure.roi.parentNode.className = "roi";
                    slider.tooltip.className = "";
                } 
            }
        },
        selectCircle:function( index )
        {
            slider.index = index;

            figure.roi = figure.rows[ index ].querySelector("span");
            figure.bars[ index ].setAttribute("class","active");
            figure.afpTotal = 0;
                
            for(var i in ajax.json.data)
            {
                if(i != index )         //recalculate, all values except itself
                    figure.afpTotal += ajax.json.data[i].afp;
            }
        },
        moveCircle:function( afp )
        {
            var positionCircle = slider.canvasSize - (afp/slider.ratio);

            ajax.json.data[ slider.index ].afp = afp;
            
            figure.roiTotal.textContent = Math.round( ( (figure.afpTotal + afp) /figure.mfvTotal ) * 100 );
            figure.roi.textContent = Math.round( (afp*100)/ ajax.json.data[ slider.index ].mfv ) || 0;
            figure.updateColors( ajax.json.data[ slider.index ] );
            figure.updateBars( ajax.json.data, afp );
            figure.circles[ slider.index ].setAttribute("cy", positionCircle );
        },
        load:function()
        {
            this.maxValue = chart.yAxis[0].max;
            this.canvasSize = parseInt( chart.clipRect.height );
            this.ratio = this.maxValue / this.canvasSize;
            this.coord = getXY( figure );

            figure.bars.prop("onclick", function(e)
            {
                e.stopPropagation();
                figure.circles[ this.index ].onmousedown(e);
                figure.onmousemove(e);
                figure.onmouseup(e);
            })

            figure.circles.prop("onmousedown",function(e)
            {
                e.stopPropagation();
                slider.draggable = true;
                slider.selectCircle( this.index );

                slider.tooltip.style.top = (e.clientY + window.pageYOffset - slider.coord.y - slider.adjust ) + "px";
                slider.tooltip.style.left = (e.clientX + window.pageXOffset - slider.coord.x - slider.adjust/2 ) + "px";
                slider.tooltip.input.value = ajax.json.data[ this.index ].afp.format();

                return false;
            })
        },
        clean:function()
        {
            slider.tooltip.removeAttribute("style");
        }
    }

    var tabledit = {
        init: function()
        {
            this.tbody = document.querySelector("#tab2");
            this.total1 = document.querySelector("#total1");
            this.total2 = document.querySelector("#total2 b");
        },

        styleCell:function( row, name )
        {
            row.querySelector("td."+name).className = "alt "+name+" mod";
        },

        calculate: function()
        {
            var data = ajax.jsonrev.data, total1 = 0, total2 = 0;

            this.cellShare = $("td.share",this.tbody);
            this.cellRev = $("td.revenue",this.tbody);
            this.cellInput1 = $("[name='afp']",this.tbody);
            this.cellInput2 = $("[name='rev']",this.tbody);
            this.cellPercent = $(".percent",this.tbody);

            for(var i in data)
            {
                total1 += data[i][1];
                total2 += data[i][4];        
                this.cellPercent[i].textContent = ((data[i][3]*100) / data[i][4] ).toFixed(1);
            }

            for(var i in data)
            {
                this.cellRev[i].textContent = ((total2 * data[i][2]) / 100).format();
                this.cellInput1[i].value = parseInt(this.cellInput1[i].value).format();
                this.cellInput2[i].value = parseInt(this.cellInput2[i].value).format();
            }

            this.total1.textContent = total1.format();
            this.total2.textContent = total2.format();

            $("td input").prop("onchange",function()    //error if afp greater than chart
            {
                var row = this.parentNode.parentNode;
                
                tabledit.recalculatePercent( row );
                tabledit.styleCell( row, this.name );

                if(this.name == "rev")
                    tabledit.recalculateAll();
                else
                    storage.update("chart", row, setNumber( this, "clean" ) );
            });
        },

        recalculatePercent: function( row )
        {
            var index = row.rowIndex - 1,
                inputAfp = setNumber( this.cellInput1[ index ], "clean" ),
                inputRev = setNumber( this.cellInput2[ index ], "clean" ),
                result = (inputAfp*100) / inputRev;

            row.querySelector(".percent").textContent = result.toFixed(1);
            row.className += " mod";
        },

        recalculateAll: function()
        {
            var newtotal = 0;
            
            for(var i in this.cellInput2)
            {
                if( this.cellInput2[i].value )
                    newtotal += setNumber( this.cellInput2[i], "clean" );
            }

            this.total2.textContent = newtotal.format();

            for(var i in this.cellRev)
            {
                this.cellRev[i].className = "revenue em2";
                this.cellRev[i].textContent = Math.round( (newtotal * parseInt( this.cellShare[i].textContent ) )/100 ).format();
            }
        }
    }

    var tablesort = {
        table:false,
        reverse:1,
        by:function( cell )
        {
            var newsort = [],
                index = cell.index,
                rows = $("tbody tr",this.table);
            
            this.reverse = this.reverse * -1;
            this.style( cell );

            for(var i=0;i<rows.length;i++)
                newsort[i]=rows[i].cloneNode(true);

            newsort.sort( function(a,b)
            {
                var aCell = a.getElementsByTagName("td")[index].textContent,
                    bCell = b.getElementsByTagName("td")[index].textContent;

                if( !isNaN( parseFloat(aCell) ) )   aCell = parseFloat(aCell);
                if( !isNaN( parseFloat(bCell) ) )   bCell = parseFloat(bCell);  

                if( aCell>bCell )       return tablesort.reverse;
                else if( aCell<bCell )  return -tablesort.reverse;
                else                    return 0;
            });

            for(var i=0;i<rows.length;i++)  
                rows[i].parentNode.replaceChild( newsort[i], rows[i] );
        },
        style:function( cell )
        {
            var prev = this.table.querySelector(".asc");

            if(prev)
                prev.className = "";

            prev = this.table.querySelector(".desc");

            if(prev)
                prev.className = "";

            cell.className = (this.reverse===1)? "desc":"asc";
        },
        events:function( table )
        {
            this.table = table;

            $("thead td",table).prop("onclick",function()
            {
                tablesort.by( this );
            })
        }
    }

    var storage = {
        user:false,
        init:function()
        {
            this.user = document.querySelector("nav.side var").textContent;
        },
        save:function( name,value )
        {
            localStorage.setItem( name,value );
        },
        saveAll:function( type )
        {
            var tr;

            if(type == "table")
            {
                tr = $("tr.mod",tabledit.tbody)

                for(var i in tr)
                {
                    if(tr[i].rowIndex)
                        storage.save( tr[i].className.split(" ")[0] , setNumber( tr[i].querySelector("input"), "clean" ) );
                }
            }
            else
            {
                tr = $("tr.mod",figure.tbody)
                   
                for(var i in tr)
                {
                    if(tr[i].rowIndex)
                        storage.save( tr[i].className.split(" ")[0] , ajax.json.data[ tr[i].rowIndex-1 ].afp );
                }
            }

            alert("Data saved");
        },
        update:function( type, row , newvalue )
        {
            var tr,
                key = row.className.split(" ")[0];

            if(type == "table")
            {
                tr = tabledit.tbody.querySelector("tr."+ key);

                if(tr)
                {
                    tr.querySelector("input").value = newvalue.format();
                    tabledit.recalculatePercent( tr );
                    tabledit.styleCell( tr, "afp" );
                }
            }
            else
            {
                tr = figure.tbody.querySelector("tr."+key);
                
                if(tr)
                {
                    slider.selectCircle( tr.rowIndex - 1 );
                    slider.moveCircle( newvalue );
                }
            }
        },
        load:function( type )
        {
            var data, keyId, keyAfp;

            if(type == "table")
            {
                data = ajax.jsonrev.data;
                keyId = 0;
                keyAfp = 3;
            }
            else
            {
                data = ajax.json.data;
                keyId = "club_id";
                keyAfp = "afp";
            }

            for(var key in localStorage)
            {   
                for(var i in data)
                {
                    if( key == ("c"+data[i][keyId]) )
                        data[i][keyAfp] = parseInt( localStorage.getItem( key ) );
                }
            }
        },
        remove:function()
        {
            localStorage.clear();
            
            alert("Data removed");
        }
    }

    Number.prototype.format = function(n,x)
    {
        var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    }

    function market()
    {
    	function chartStacked( chartype, name )
    	{
	    	var data = ajax.json["chart"+name],
	            param = params["market-"+name],
                i;

	    	param = {
			    chart: { renderTo:"fig-"+name, type:chartype, backgroundColor:"transparent"},
			    title: { text: ""},
			    credits: { enabled: false },
			    exporting: { enabled: false },
				plotOptions: { series: {stacking:"normal"} },
				xAxis: { categories: data.categories },
			    yAxis: { title:{ text:""}, min:0 },
			    series: []
			}

	        for(i=0;i<data.series.length;i++)
	            param.series.push( { name:data.series[i].name, color: colors[i], dataLabels:{enabled:true}, data:data.series[i].data } );

	        new Highcharts.Chart( param );
    	}

        function chartColumns( name, type, stack )
        {
            var data = ajax.json["chart"+name],
                param = params["market-"+name],
                i;

            data.series[0].color = colors[0];
            data.series[1].color = colors[1];
            data.series[0].dataLabels = {enabled:true};
            data.series[1].dataLabels = {enabled:true};

            param = {
                chart: { renderTo:"fig-"+name, type:type, backgroundColor:"transparent"},
                title: { text: ""},
                credits: { enabled: false },
                plotOptions: { series: {stacking: stack || false } },
                exporting: { enabled: false },
                xAxis: { categories: data.categories },
                yAxis: { title:{ text:""}, min:0 },
                series: [ data.series[0], data.series[1] ]
            }

            new Highcharts.Chart( param );
        }

    	function chartPie( name )
    	{
			var data = ajax.json["chart"+name],
	    	    param = params["market-"+name],
                i;

	    	param = {
			    chart: { renderTo:"fig-"+name, type:"pie", backgroundColor:"transparent"},
			    title: { text: ""},
			    credits: { enabled: false },
			    exporting: { enabled: false },
                plotOptions: { pie: { innerSize: "30%" } },
			    legend: { enabled: false },
				yAxis: { title:{ text:""}, min:0 },
				series: [
				    {name: "",size:"90%", data: [], dataLabels: { formatter: function(){return "<b>"+ this.point.name +":</b> "+ this.y}, distance: -80 } }, 
		            {name: "",size:"90%", data: [], innerSize: "75%", dataLabels: { formatter: function(){return "<b>"+ this.point.name +":</b> "+ this.y} }}
		        ]
			}
			
			param.series[0].name = data.name;
			param.series[1].name = data.name;

	        for(i=0;i<data.series.length;i++)
            {
	            param.series[0].data.push( { name:data.series[i].name, color: colors[i], y:data.series[i].data } );

                for(var j=0;j<data.series[i].child.length;j++)
                    param.series[1].data.push( { name:data.series[i].child[j].name, color: colors[i], y:data.series[i].child[j].data } );
            }
	        

	        new Highcharts.Chart( param );
    	}

    	//section1
		chartStacked("column",1);
		chartColumns(5,"bar","normal");
        chartColumns(10,"bar","normal");
        //section2
        chartColumns(2,"bar","normal");
        chartColumns(9,"column","normal");
        //section3
        chartPie(4);
        //section4
        chartColumns(6,"bar");
        chartColumns(7,"bar");
        chartColumns(12,"bar");
        //section5
        chartStacked("line",8);
        //section6
        chartColumns(11,"column");
        chartColumns(13,"column");
        chartPie(101);
        chartPie(102);
        chartPie(103);
        chartPie(104);
        chartColumns(3,"bar","normal");
    }

    function league()
    {
        function displayIcons( data )
        {
            var tmp = document.querySelector("#tmp-row").innerHTML,
                container = document.querySelector("#annex"),
                tag = "", output = "",
                texts = $("g.highcharts-data-labels + g.highcharts-axis-labels text");

            for(var i in texts) //templating?
            {
                if( texts[i].textContent )
                {
                    tag = tmp.concat("");
                    tag = tag.replace( "@icon@", texts[i].textContent );
                    tag = tag.replace( "@d0@", parseInt( texts[i].getAttribute("y") ) + 7 );
                    //tag = tag.replace( "@d1@", parseInt( texts[i].getAttribute("x") ) );
                    tag = tag.replace( "@d2@", data[i][2]);
                    texts[i].setAttribute("style","visibility:hidden");
                    output += tag;
                }
            }

            container.innerHTML = output;
        }

        function setRangeClass( value )
        {
            for(var i in ageRanges)
            {
                if(value > ageRanges[i])
                    return " r" + (parseInt(i)+1);
            }
            return " r" + (parseInt(i)+1);
        }

        function loadText( league, year )
        {
            ajax.get( URLinsights + year + "/" + league );

            tagGeneral.innerHTML = ajax.json.general_league_insights || "";
            tagSpecific.innerHTML = ajax.json.club_specific_insights || "";
        }

        function loadMap( league, year )
        {
            function add( name )
            {
                if(li)
                    li.className += " "+name;
                if(g)
                    g.setAttribute("class", g.getAttribute("class") + " "+name );
            }
            function del( name )
            {
                if(li)
                    li.className = li.className.replace(" "+name,"");
                if(g)
                {
                    g.setAttribute("class", g.getAttribute("class").replace(" "+name,"") );
                }
            }

            ajax.get("../ajax/data-league-"+league+".json");

            templating( ajax.json["year"+year], "flag");

            var li = $("#flag li"), b, g, name; 

            for(var i in li)
            {
                if(li[i].className)
                {
                    b = li[i].querySelector("b");
                    b.className += setRangeClass( parseInt( b.textContent ) );
                    document.querySelector( "g."+li[i].className+" path" ).setAttribute( "class", b.className );
                }
            }

            $("svg g,.flags li").prop("onmouseover", function()
            {
                name = this.getAttribute("class").split(" ")[0];
                li = document.querySelector(".flags li."+name);
                g = document.querySelector("g."+name);

                add("hover");

            }).prop("onmouseout",function()
            {
                li = document.querySelector(".flags li.hover");
                g = document.querySelector("g.hover");

                del("hover")
            
            }).prop("onclick",function()
            {
                if(this.getAttribute("class").indexOf("clicked") > 0)
                    del("clicked");
                else
                    add("clicked");
            });
        }

        function loadChart( league, year )
        {
            ajax.get( URLrevenue + year + "/" + league + "/0" ); //default club

            var data = ajax.json.data,
                total = 0;

            objSort( ajax.json.data, 2, -1 );

            params[ page ].xAxis.categories = [];
            params[ page ].series[0].data = [];

            for(var i in data)
                total += data[i][4];

            tagTotal.textContent = total.format();

            for(var i in data)
            {
                params[ page ].xAxis.categories.push( data[i][0] );
                params[ page ].series[0].data.push( Math.round( (total * data[i][2])/100 ) );
            }

            chart = new Highcharts.Chart( params[ page ] );

            displayIcons( data );
        }

        var tagTotal = document.querySelector("#total"),
            tagGeneral = document.querySelector("#txt_general"),
            tagSpecific = document.querySelector("#txt_specific");

        params[ page ] = {
            chart: { renderTo:document.querySelector("figure"), type:"bar", backgroundColor:"transparent" },
            title: { text: ""},
            credits: { enabled: false },
            legend: { enabled: false },
            exporting: { enabled: false },
            xAxis: { categories: [] },
            yAxis: { title:{ text:""}, min:0 },
            series: [ { name:"Share", color:"#00b9f2", dataLabels:{enabled:true}, data:[] } ]
        }

        $("a[target]").prop("onclick",function()
        {   
            var previous = this.parentNode.querySelector(".active"),
                league, year;

            previous.className = previous.className.replace("active","");
            this.className += " active";

            league = parseInt( document.querySelector("nav.wrap .active").getAttribute("target") );
            year = parseInt( document.querySelector("header.main > a.active").getAttribute("target") );
            
            loadText( league, year );
            loadChart( league, year );
            loadMap( league, year );
        });

        loadText( 1, defaultSeason )
        loadChart( 1 , defaultSeason );
        loadMap( 1, defaultSeason );
    }

    function sector()
    {
        function loadText( year )
        {
            //ajax.get( URLinsights + year + "/" + league );

            //tagGeneral.innerHTML = ajax.json.general_league_insights || "";
            //tagSpecific.innerHTML = ajax.json.club_specific_insights || "";
        }

        $("a[target]").prop("onclick",function()
        {   
            var previous = this.parentNode.querySelector(".active"),
                year;

            previous.className = previous.className.replace("active","");
            this.className += " active";

            year = parseInt( document.querySelector("header.main > a.active").getAttribute("target") );
            
            loadText( year );
        });
    }

    function phase()
    {
        function chartSort( obj )
        {
            if(obj.name == "sort-opt")
            {
                var value = parseInt(obj.value)*-1,
                    key = obj.className;

                objSort( ajax.json.data, key, value )

                obj.value = value;

                params[ page ].xAxis.categories = [];
                params[ page ].series[0].data = [];
                params[ page ].series[1].data = [];
                params[ page ].series[2].data = [];

                phaseChart();
            }
        }

        function submitForm( url, inputs )
        {
            var tag, select;

            for(var i in inputs)
            {
                select = document.querySelector("#"+inputs[i]+" ul");

                if( select )
                {
                    tag = $("input",select);

                    for(var j in tag)
                    {
                        if(tag[j].checked)
                            url += tag[j].value+",";
                    }

                    url += "/";
                    url = url.replace(",/","/");
                    
                    if(i==0)
                        url = url.replace("//","/"+tag[0].value+"/");   //default year
                    else
                        url = url.replace("//","/0/");                  //default value
                }
                else
                {
                    tag = document.querySelector("input[name='"+inputs[i]+"_start']").value || 0;
                    url += tag+",";
                    tag = document.querySelector("input[name='"+inputs[i]+"_end']").value || 0;
                    url += tag+"/";
                }
            }

            ajax.get( url ); 
        }

        function prepareSelects()
        {
            var label;

            label = secScorecard.querySelector("label");
            secScorecard.querySelector("[type='text']").value = label.textContent;
            label.querySelector("input").checked = true;

            label = secRevenue.querySelector("label");
            secRevenue.querySelector("[type='text']").value = label.textContent;
            label.querySelector("input").checked = true;
        }

        function tabs( obj )
        {
            slider.clean();
            var href = obj.getAttribute("href") 
            
            if(href == "#scorecard")
            {
                if( secScorecard.querySelector("tbody").textContent == "" )
                    secScorecard.querySelector(".search").onclick();
            }
            if(href== "#revenue")
            {
                if( secRevenue.querySelector("tbody").textContent == "" )
                    secRevenue.querySelector(".search").onclick();
            }
        }

        function getFilter( id )
        {
            var output = id.toUpperCase()+": ",
                labels = document.querySelectorAll("#"+id+" label" ),
                input;

            for(var i in labels)
            {
                if(labels[i].querySelector)
                {
                    if(labels[i].querySelector("input").checked)
                        output += labels[i].textContent + ",";
                }
            }
            return output.slice(0,-1);
        }

        figure = document.querySelector("#figure-phase");
        figure.container = document.querySelector("[hidden]");
        figure.tbody = document.querySelector(".bg0 tbody");
        figure.template = document.querySelector("#tmp-row").innerHTML;
        figure.roiTotal = document.querySelector("#roit span");

        var secFilter = document.querySelector("#filter"),
            secScorecard = document.querySelector("#scorecard"),
            secRevenue = document.querySelector("#revenue");
            //wait = document.querySelector("#wait")
        
        prepareSelects();
        setSelects( chartSort );

        secScorecard.querySelector(".search").onclick=function()
        {
            ajax.jsonew = "jsonscore";
            submitForm( URLscore, ["seasons","leagues_score"] );
            templating( ajax.jsonscore.data, "tab1" );

        }
        secRevenue.querySelector(".search").onclick=function()
        {
            ajax.jsonew = "jsonrev";
            submitForm( URLrevenue, ["seasons","leagues_revenue","clubs_revenue"] );
            storage.load("table");
            templating( ajax.jsonrev.data, "tab2" );
            tabledit.calculate();
        }
        secRevenue.querySelector(".save").onclick=function()
        {
            storage.saveAll("table");
        }
        secFilter.querySelector(".search").onclick=function()
        {
            //wait.className = "show";
            ajax.jsonew = false;
            submitForm( URLfilter, inputs );
            storage.load("chart");
            phaseChart();
            //wait.className = "hide";
        }
        secFilter.querySelector(".reset").onclick=function()
        {
            secFilter.querySelector("form").reset();
        }
        secRevenue.querySelector(".resetmap").onclick=function()
        {
            secRevenue.querySelector(".search").onclick();
        }
        secFilter.querySelector(".save").onclick=function()
        {
            storage.saveAll("chart");
        }
        secFilter.querySelector(".export").onclick=function()
        {
            var d = new Date(),
                month = d.getMonth()+1,
                day = d.getDate(),
                date = d.getFullYear() +"/"+ (month<10? "0":"") + month +"/"+ (day<10? "0":"") + day,
                data = ajax.json.data,
                style = "";

            for(var i in data)
            {
                //params[ page ].series[1].data[ i ] = data[ i ].bgr;
                style = figure.circles[i].getAttribute("class");

                params[ page ].series[2].data[ i ].y = data[ i ].afp;
                
                if( style )
                    params[ page ].series[2].data[ i ].color = colorc[style];

                params[ page ].xAxis.categories[ i ] = data[i].club_name;
            }

            chart.setTitle( { text:"Phase 1 Revenue Map"} );

            chart.exportChart( 
                { type: "image/png",url: URLlocal + "export"},
                { chart:
                    {
                        backgroundColor:"#333",
                        events: { load:function()
                        {
                            this.renderer.image( URLlogo, 400, 40, 170, 60).add(); 
                            this.renderer.text( getFilter("seasons"), 40, 40 ).css( {color:"#fff",fontSize:"14px"} ).add();
                            this.renderer.text( getFilter("leagues"), 40, 60 ).css( {color:"#fff",fontSize:"14px"} ).add();
                            this.renderer.text( getFilter("industries"), 40, 80 ).css( {color:"#fff",fontSize:"14px"} ).add();
                            this.renderer.text( getFilter("sponsors"), 40, 100 ).css( {color:"#fff",fontSize:"14px"} ).add();
                            this.renderer.text( "DATE CREATED: " + date, 40, 120 ).css( {color:"#fff",fontSize:"14px"} ).add();
                            this.renderer.text( "BY: "+storage.user , 40, 140 ).css( {color:"#fff",fontSize:"14px"} ).add();
                        }
                    } 
                },
            });
            return false;
        }

        $("form input[maxlength]").prop("onblur",function()
        {
            setNumber( this );
        });

        $(".fold").prop("onclick", function( event )
        {
            fold( event ); 
        });

        subsection( tabs );
        tablesort.events( secScorecard.querySelector("table") );
    }

    function phaseChart()
    {
        function displayAbbr()
        {
            var abbr;

            for(var i in data)
            {
                abbr = figure.rows[ i ].querySelector(".bgr span");

                if(abbr)
                    abbr.textContent = setNumber( abbr.getAttribute("title"), "abbr" );

                abbr = figure.rows[ i ].querySelector(".mfv span");

                if(abbr)
                    abbr.textContent = setNumber( abbr.getAttribute("title"), "abbr" );

                //colors

                if( data[i].afp < (data[i].mfv/2) )
                    figure.rows[ i ].querySelector(".roi").className = "roi under";

                else if( data[i].afp > data[i].mfv )
                    figure.rows[ i ].querySelector(".roi").className = "roi over";
            }
        }

        params[ page ] = {
            chart: { renderTo:"figure-"+page, type:"bar", backgroundColor:"transparent" },
            title: { text: "", style: { color: "#fff", fontWeight: "bold" }, margin:160 },
            credits: { enabled: false },
            legend: { enabled: false },
            tooltip: { enabled: false },
            //tooltip: { formatter: function() {return this.y}, positioner:function(boxWidth,boxHeight,point) { return {x:point.plotX+60,y:point.plotY-20} } },
            exporting: { enabled: false },
            xAxis: { categories:[], gridLineWidth:1, min:0, labels: { formatter: function() {return this.value}, style: {color:"#fff"} } /*,lineWidth:0, tickLength:0, plotLines:[ {value:2, color:'red', width:1 }]*/ },
            yAxis: [ 
                { title:null, gridLineWidth:0, min:0, labels: { formatter: function() {return '€'+(this.value/1000000) +'m'}, style: {color:"#fff"} } },
                { title:null, gridLineWidth:0, opposite:true, reversed:false, linkedTo:0, labels: { formatter: function() {return '€'+(this.value/1000000) +'m'}, style: {color:"#fff"} } }
            ],
            series: [ 
                { type:"scatter", name:"NLA", color: "orange", data:[], marker: {symbol:"url("+URLline+")"} }, /*"square"*/
                { type:"columnrange", name:"Revenue", color:"#999", data: [] },
                { type:"bubble", name:"AFP", maxSize: 24, data: [], marker:{ states: {hover:{enabled:false}} } }
            ]
        }

        var data = ajax.json.data, mfvTotal = 0, afpTotal = 0, style = "";

        figure.container.removeAttribute("hidden");
        figure.style.height = barAdjust + (data.length*barHeight) + "px";
        figure.tbody.innerHTML = "";

        for(var i in data)
        {
            mfvTotal += data[i].mfv;
            afpTotal += data[i].afp;
        }

        for(var i in data)
        {
            data[i].bgr = Math.round( (afpTotal/mfvTotal) * data[i].mfv );
            data[i].roi = Math.round( (data[i].afp/data[i].mfv)*100 ) || 0;

            if( data[i].afp < (data[i].mfv/2) )
                style = "under";
            else if( data[i].afp > data[i].mfv )
                style = "over";
            else
                style = "normal";

            params[ page ].xAxis.categories.push( data[i].club_id );
            params[ page ].series[0].data.push( (data[i].avg_roi/100) * data[i].mfv );
            params[ page ].series[1].data.push( [data[i].bgr, data[i].mfv] );
            params[ page ].series[2].data.push( { y:data[ i ].afp , color:colorc[style] } )

            templating( data, [figure.template,figure.tbody] );
        }

        chart = new Highcharts.Chart( params[ page ] );

        figure.mfvTotal = mfvTotal; 
        figure.afpTotal = afpTotal;
        figure.circles = $("circle");
        figure.bars = $("g rect");
        figure.rows = $("tr",figure.tbody);
        figure.bgr = $("td.bgr span", figure.tbody);
        figure.roiTotal.textContent = Math.round( (figure.afpTotal/figure.mfvTotal)*100 ) || 0;

        $("g.highcharts-tracker").func("removeAttribute","clip-path");

        displayAbbr();

        slider.load();
        slider.clean();
    }

    function profile()
    {
        document.querySelector(".remove").onclick = function()
        {
            storage.remove();
        }
    }

    function setNumber( input, mode )
    {
        if( mode == "clean" )
            return parseInt( input.value.replace(/,/g,'') );

        if( mode == "abbr")
        {
            if(input > 1000000)
                return (input/1000000).toFixed(1)+"m";
            if(input > 1000)
                return (input/1000).toFixed(1)+"k";
            return input;
        }

        if( isNaN(input.value) )
            input.value = 0;
    }

    function setSelects( callback )
    {
        function hideSelect()
        {
            var active = document.querySelector("ul.active");
                
            if( active )
                active.className = active.className.replace("active","none");
        }

        $(".select").prop("onclick",function(e)
        {   
            e.stopPropagation();
            hideSelect();

            var select = this, ul = this.querySelector("ul");

            ul.className = ul.className.replace("none","active");

            $("[type='radio']",this).prop("onclick",function(e)
            {
                e.stopPropagation();
                hideSelect();

                select.querySelector("[type='text']").value = this.parentNode.textContent;

                if(callback)
                	callback( this );
            });
        });

        document.body.onclick = function()
        {
            hideSelect();
        }
    }

    function subsection( callback )
    {  
        $("[href^='#']").prop("onclick", function() 
        { 
            var activeLink, activeSection, targetSection,
                target = this.getAttribute("href"),
                targetclass = this.getAttribute("target") || "on";

            activeLink = this.parentNode.querySelector("a.active");
            activeLink.className = activeLink.className.replace("active","");  
            this.className += " active";

            activeSection = document.querySelector("."+targetclass);
            activeSection.className = activeSection.className.replace(targetclass,"none")
            
            targetSection = document.querySelector(target)
            targetSection.className = targetSection.className.replace("none",targetclass);

            if(callback)
                callback( this );

            return false;
        });
    }

    function fold( event )
    {
        var next = event.target.nextSibling.nextSibling;

        if(next.className.indexOf("off") != -1)
            next.className = next.className.replace(" off","");
        else
            next.className += " off";
    }

    function templating( data, element, callback )
    {
        var tmp, cont, names, newTag, output = "";

        if(typeof element == "string")
        {
            tmp =  document.querySelector("#tmp-"+element).innerHTML;
            cont = document.querySelector("#"+element);
        }
        else
        {
            tmp =  element[0];
            cont = element[1];
        }

        names = tmp.match(/@[^].*?@/g);

        for(var i in names)
            names[i] = names[i].substring(1,names[i].length-1);

        for(var i in data)
        {
            newTag = tmp.concat("");

            for(var j in names)
                newTag = newTag.replace( "@"+names[j]+"@", data[i][ names[j] ] );

            output += newTag;
        }

        if(callback)
            output = callback(output);

        cont.innerHTML = output;
    }

    function $( selector, object )
    {
        var nodelist=(object || document).querySelectorAll(selector);
        nodelist.prop=function(property,value)
        {
            for(var i=nodelist.length-1;i>=0;i--)
            {
                nodelist[i][property]=value;
                nodelist[i].index=i;
            }

            return nodelist;
        }
        nodelist.func=function(func,param,param2)
        {
            for(var i=nodelist.length-1;i>=0;i--)
                nodelist[i][func](param,param2)

            return nodelist;
        }
        return nodelist;
    }

    function getXY( obj )
    {
        var x=0,y=0,rect,scrollLeft,scrollTop; 
        if(obj===document.body) 
            return {x:0,y:0}
        if(obj.getBoundingClientRect)
        {
            rect=obj.getBoundingClientRect();
            scrollLeft=document.documentElement.scrollLeft;
            scrollTop=document.documentElement.scrollTop
            x=rect.left+scrollLeft;
            y=rect.top+scrollTop;
        }
        else
        {
            x=obj.offsetLeft;
            y=obj.offsetTop;
            parent=obj.offsetParent;
            if(parent!=obj)
            {
                while(parent)
                {
                    x+=parent.offsetLeft;
                    y+=parent.offsetTop;
                    parent=parent.offsetParent
                }
            }
            parent=obj.offsetParent;
            while((parent) && (parent!=document.body))
            {
                x-=parent.scrollLeft;
                parent=parent.offsetParent
            }
        }
        return {x:x,y:y}
    }

    function objSort( obj, key, reverse )
    {
        if(!reverse)
            reverse = 1;

        obj.sort( function(a,b)
        {
            a = parseFloat( a[key] );
            b = parseFloat( b[key] );

            if(a < b)
                return -1*reverse;
            if(a > b)
                return 1*reverse;
            return 0;
        });
    }

    function initialize()
    {
        switch(page)
        {
            case "phase":
            ajax.init();
            phase();
            storage.init();
            slider.init();
            tabledit.init();
            break;

            case "league":
            ajax.init();
            league();
            break;

            case "market":
            subsection();
            ajax.init();
            ajax.get("/data/market-research/" + defaultSeason);
            market();
            break;

            case "profile":
            profile();
            break;

            case "sector":
            subsection();
            sector();
            break;

            case "home":
            case "method":
            case "sector":
            case "biblio":
            subsection();
            break;
        }
    }

    initialize();

})();