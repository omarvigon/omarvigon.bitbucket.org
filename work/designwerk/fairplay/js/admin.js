(function()
{
    "use strict";

    var section = {
        link:function( obj )
        {
            var prev = $(".active",obj.parentNode.parentNode);
            
            if(prev)
                prev.del("active");
  
            if(prev != obj)
                obj.add("active");
        },
        breadcrumb:{
            init:function()
            {
                //this.cont = $(".breadcrumb");
            },
            add:function( obj )
            {
                //this.cont.querySelectorAll("a")[1].textContent = obj.textContent;
            }
        },
        go:function( name )
        {
            var link = $("[href*='"+name+"']"),
                oldSection, newSection;
                
            name = name.replace("#","");
            document.body.id = name;
            
            //this.breadcrumb.add( link );
            section.link( link )

            oldSection = $(".on");
            oldSection.className = oldSection.className.replace("on","none")
                
            newSection = $("."+name);
            newSection.className = newSection.className.replace("none","on");
        }
    }

    function $(selector,object)
    {
        var node=(object || document).querySelector(selector);
        if(node)
        {
            node.add=function(name) {
                this.className += " "+name }
            node.del=function(name) {
                this.className = this.className.replace(name,"") }
            node.has=function(name) {
                return (this.className.indexOf(name) != -1)? true:false }
        }
        return node;
    }

    function $$(selector,object)
    {
        var nodelist=(object || document).querySelectorAll(selector);
        nodelist.prop=function(property,value)
        {
            for(var i=nodelist.length-1;i>=0;i--)
            {
                nodelist[i][property]=value;
                nodelist[i].index=i;
            }
            return nodelist;
        }
        nodelist.func=function(func,param,param2)
        {
            for(var i=nodelist.length-1;i>=0;i--)
                nodelist[i][func](param,param2)
            return nodelist;
        }

        for(var i=nodelist.length-1;i>=0;i--)
        {
            nodelist[i].add=function(name)
            {
                this.className += " "+name;
            }
            nodelist[i].del=function(name)
            {
                this.className = this.className.replace(name,"");
            }
            nodelist[i].has=function(name)
            {
                return ( this.className.indexOf(name) != -1 )? true:false;
            }
        }
        return nodelist;
    }

    function initialize()
    {
        function menuFloating()
        {
            var menu = $(".menuside")

            if(menu)
            {
                menu = $(".menu",menu);
                menu.add("none");

                $(".breadcrumb a").onclick = function()
                {
                    (menu.has("none"))? menu.del("none"):menu.add("none");
                    return false;
                }
            }
        }

        window.onhashchange = function()
        {
            section.go( window.location.hash )
        }

        window.onload = function()
        {
            if( window.location.hash )
                section.go( window.location.hash );
        }

        $$(".fold").prop("onclick", function()
        {
            section.link( this );
        });

        menuFloating();
        //section.breadcrumb.init();
    }

    initialize();

})();

tinymce.init(
{
    selector:"textarea",
    toolbar:"undo redo | bold italic underline strikethrough superscript subscript | alignleft aligncenter alignright | bullist numlist outdent indent | removeformat",
    statusbar:false,
    menubar:false,
    height:200
});

$(function() {

    $("form.bg").submit(function(e)
    {
        var form = $(this);
        var postData = form.serializeArray();
        var formURL = form.attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function(data, textStatus, jqXHR)
                    {
                        form.find("input,textarea").removeClass("error").prop("title", "");

                        if (!data.success) {
                            $.each(data.errors, function(typeId, field) {
                                $.each(field, function(name, messages) {
                                    if (messages instanceof Object) {
                                        form.find("[name='" + name + "[" + typeId + "]']")
                                                .addClass("error")
                                                .prop("title", messages)
                                                .tooltip();
                                    } else {
                                        form.find("[name='" + typeId + "']")
                                                .addClass("error")
                                                .prop("title", field)
                                                .tooltip();
                                    }
                                });

                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        //if fails      
                    }
                });
        e.preventDefault();
    });
    function triggerAC()
    {
        $("input.totaltop").each(function() {
            var elem = $(this);
            var source = eval(elem.attr('source'));

            elem.autocomplete({
                source: source,
                focus: function(event, ui) {
                    $(this).val(ui.item.label);
                    return false;
                },
                select: function(event, ui) {
                    event.preventDefault();
                    $(this).siblings("input[type='number']").attr('name', function() {
                        return $(this).attr('name').replace(/\[(.*)]/g, '[' + ui.item.value + ']');
                    });
                }
            });
        });
    }

    triggerAC();

    function removeArticle(elem)
    {
        var articles = elem.siblings("article");
        if (articles.length < 2) {
            articles.each(function() {
                $(this).find("span.ui-icon-circle-minus").remove();
            });
        }
        elem.remove();
    }
    function addArticle(elem)
    {
        elem.siblings("article").find("span.ui-icon-circle-minus").remove();
        var oldArticle = elem.prev();

        oldArticle.after(oldArticle
                .clone(false).find("input[type='number']").val("").end()
                .find("input[type='number']:last").keydown(function(e) {
            if (e.which === 9) {
                addArticle($(this).parent().next());
            }
        }).end());

        var articles = elem.siblings("article");
        if (articles.length > 1) {
            articles.each(function() {
                var span = $('<span class="ui-icon ui-icon-circle-minus" style="display: inline-table;"></span>')
                        .click(function() {
                            removeArticle($(this).parent());
                        });
                $(this).append(span);
            });
        }

        triggerAC();
    }
    
    $("button.addRow").click(function() {
        addArticle($(this));
    });

});
