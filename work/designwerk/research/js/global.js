$(function () {

    var json = false; 
    var URLbase = "http://"+window.location.host+"/api/";
    var URLmode = { 
        data:"view/data",
        charts:"view/charts",
        filters:"filter/broadcast",
        last:"?per_page="}
    var mobile = false;
    var mobileWidth = 830;
    var page = $("body").attr("class");
    var loading = $(".load");
    var table = $("#table"), headerCells;
    var group = false;
    var demogIndex = 0;
    var pageDefault = 50;
    var textColor = "#050429";
    var section = "data";
    var filters = {
        uk : ["group","seasons","competitions","clubs","channels","programme-types","timeslots","duration","packages","type","dem"],
        inter : ["group","seasons","clubs","region","territory","programme-types","channels"],
        attendance : ["group","seasons","clubs","kickoff"],
        auditing : ["group","seasons","territory","product","type"],
        download : ["group","seasons","clubs","categories","subcategories"]}
    var article = {
        container: $("#article-container"),
        tmp : $("#article-template").html()}
   
    function initialize()
    {
        function hideMenu()
        {
            $(".clicked").removeClass("clicked");
        }

        $("html").click( function()
        {
            hideMenu();
        });

        $("fieldset,.select").click( function(e)
        {
            if(!$(this).hasClass("inactive"))
            {
                e.stopPropagation();
                $(this).find("ul").click( function(e) { e.stopPropagation() });

                if($(this).hasClass("clicked"))
                {
                    $(this).removeClass("clicked");
                }    
                else
                {
                    hideMenu();
                    $(this).addClass("clicked");
                }
            }
        });

        $("form button").click( function()
        {
            hideMenu();
            httpRequest( section , filters[page] );
            return false;
        });

        $("a[href='#csv']").click(function()
        {  
            tableExport();
            return false;
        });

        $("ul.large input,ul.all input").click( function()
        {
            var ul = $(this).parent().parent().parent();
            var inputAll = ul.find("[value='all']");

            if( $(this).val() == "all" )
            {
                ul.find("input").prop("checked",false);
                inputAll.prop("checked",true);
            }    
            else
            {
                inputAll.prop("checked",false);

                if( ul.find("input:checked").length == 0 )
                    inputAll.prop("checked",true);
            }
        });

        if(window.matchMedia)
        {
            if(window.matchMedia("(max-width:"+mobileWidth+"px)").matches)
            {    
                mobile = true;
                initializeMobile();
            }
        }

        switch( page )
        {
            case "uk":
            initToggles();
            initSelects();
            initFilters();
            $("fieldset.seasons,fieldset.clubs,fieldset.packages").addClass("none");
            httpRequest( section, filters[page] );
            break;

            case "inter":
            initSelects();
            initFilters();
            httpRequest( section, filters[page] );
            break;

            case "attendance":
            initToggles();
            initSelects();
            initFilters();
            $("fieldset.clubs").addClass("none");
            $("fieldset.clubs input[value='manchester-united']").prop("checked",true);  //hack
            httpRequest( section, filters[page]);
            break;

            case "auditing":
            initSelects();
            initToggles();
            httpRequest( section, filters[page]);
            break;

            case "download":
            httpRequest( section, filters[page]);    
            break;

            case "home":
            break;
        }
    }

    function initializeMobile()
    {
        $("form.main button,.myfilters").click( function()
        {
            $("fieldset,.myfilters").toggle();
        })

        $(".major").click( function(e)
        {
            $(this).find("a").toggleClass("active");
        });

        /*hack*/
        var caption = $("tr.caption");

        if(page == "attendance")
        {
            caption.remove();
        }
        else
        {
            caption.find("th").removeAttr("colspan");
            table.find("tfoot [colspan]").removeAttr("colspan");
            table.find("tfoot td:empty").remove();
        }
    }

    function initToggles()
    {
        function toggleDataCharts( subsection, header )
        {
            httpRequest( section, filters[page] );
            subsection.toggleClass("right");
            header.find(".legend").toggleClass("none");
        }

        function toggleOvernight( subsection, header )
        {
            httpRequest( section, filters[page] );
            $("fieldset.seasons,fieldset.clubs,fieldset.packages").toggleClass("none");
            //subsection.removeClass("right");
            //header.find(".toggle").toggleClass("inactive").removeClass("right").addClass("left");
        }

        $(".toggle input").click( function()
        {
            var label = $(this).parent();
            var labelclass = label.attr("class");
            var parent = label.parent();

            if( !parent.hasClass(labelclass) ) //click the opposite
            {
                parent.toggleClass("right left");

                var name = $(this).attr("name");
                var subsection = $(".subsection");
                var header = $("header.tc");

                if( name == "section" )
                {
                    section = $(this).val();
                    toggleDataCharts( subsection, header );
                }
                if( name == "type" && page == "uk")
                {
                    toggleOvernight( subsection, header )
                }
            }
        });
    }

    function initSelects()
    {
        function selectDemographic( val )
        {
            var current = $("var."+val);
            demogIndex = current.index();

            if(group.indexOf("top10") > 0) //reload all
            {
                table.find("tr.header:visible td").unbind("click");
                loadRows();
            }
            else
            {
                $("td var").addClass("none");   
                current.removeClass("none");
            }
        }

        function groupbyUK()
        {
            if(section == "data") //reset the selectbox
            {
                var demographic = $(".demographic");
                var demoInput = demographic.find("input:eq(0)");
                demographic.find(".title").text( demoInput.prev().text() );
                demoInput.prop("checked",true);
            }
        }

        function groupbyAuditing( index )
        {
            $("td.brand span").addClass("none").filter(":eq("+index+")").removeClass("none")
        }

        function groupbyAttendance( value )
        {
            var filters = $("fieldset.clubs,fieldset.kickoff");

            if( value == "attendance-summary" )
                filters.addClass("none");
            else
                filters.removeClass("none");   
        }

        $(".select input").click( function()
        {   
            var text = $(this).prev().text();
            var parent = $(this).parents(".select").parent();

            parent.find(".title").text( text );
            parent.find(".clicked").removeClass("clicked");

            if(parent.hasClass("demographic"))
            {
                selectDemographic( $(this).val() );
            }
            else
            {
                httpRequest( section, filters[page] );

                if( page == "uk" )
                    groupbyUK();
                if( page == "auditing")
                    groupbyAuditing( $(this).parent().parent().index() );
                if( page == "attendance")
                    groupbyAttendance( $(this).val() ); 
            }
        });
    }

    function initFilters()
    {
        function setRequest( list )
        {
            var filters = [];

            filters.push( list[0] );

            if( $("fieldset."+list[1]).length )
                filters.push( list[1] );
            if( $("fieldset."+list[2]).length )
                filters.push( list[2] );

            httpRequest("filters",filters);
        }

        $("fieldset.seasons input").click( function()
        {
            setRequest(["seasons","clubs","competitions"])
        });

        $("fieldset.clubs input").click( function()
        {
            setRequest(["clubs","seasons","competitions"])
        });

        $("fieldset.competitions input").click( function()
        {
            setRequest(["competitions","clubs","seasons"])
        });
    }

    function httpRequest( mode, filterList, URLparam )
    {
        if(mode != "filters")
            loading.fadeIn();

        var url = "", input;
        var last = URLmode.last + (URLparam || pageDefault);
      
        for(var i in filterList)
        {
            url += "/"+filterList[i]+"/";
            input =  $("input[name='"+filterList[i]+"']");

            if( input.prop("type") == "radio" )
            {
                url += input.filter(":checked").val();
            }
            else
            {
                $.each( input.filter(":checked"), function()
                {
                    url += $(this).val()+",";
                });

                url = url.slice(0,-1);
            }

            if( (i == 0) && (mode == "filters"))  //hack to add /selected after the first filter clicked
                url += "/selected";
        }

        url = URLbase + URLmode[mode] + url + last;

        $.getJSON( url, function( data )
        {
            json = data;
        })
        .done( function()
        {
            if(mode == "data")
                loadRows();
            if(mode == "charts")
                loadCharts();
            if(mode == "filters")
                loadFilters();
        })
        .complete( function()
        {
            loading.fadeOut();
        });  
    }

    function tableExport()
    {
        var output = "";  
        var headers = table.find("tr.header:visible td");
        var rows = table.find("tbody tr");
        var td, tag;

        for(var i=0;i<headers.length;i++)
            output+=headers[i].textContent+",";

        output = output.slice(0,-1) + "\n";

        for(var i=0;i<rows.length;i++)
        {
            td=$("td",rows[i]);

            for(var j=0;j<td.length;j++)
            {
                if(j==1) //date cell
                {
                    tag = $("span > *",td[j]);
            
                    if( tag.length ) 
                        output+=tag.eq(0).text()+" "+tag.eq(1).text()+" "+tag.eq(2).text()+" "+tag.eq(3).text()+",";
                    else
                        output+=td[j].textContent+",";
                }
                else
                {
                    tag = $("var",td[j]);

                    if( tag.length ) //demographic cell
                        output += tag.eq(0).text()+" - "+tag.eq(1).text()+" - "+tag.eq(2).text()+",";  //bug with comma numbers
                    else
                        output += td[j].textContent+",";
                }
            }
            output = output.slice(0,-1) + "\n";
        }
        window.open("data:text/csv;charset=UTF-8,"+encodeURIComponent(output));
    }

    function loadRows()
    {
        function fixColspan()
        {
            var caption = table.find("tr.caption");

            if(mobile)
            {
                if(page == "attendance")
                {
                    caption.remove();
                }
                else
                {
                    caption.find("th").removeAttr("colspan");
                    table.find("tfoot [colspan]").removeAttr("colspan");
                    table.find("tfoot td:empty").remove();
                }
            }
            else
            {
                var columns = headerCells.length;
                var altColumns = headerCells.filter(".alt").length;
                caption.find("th").attr("colspan", (columns - altColumns) );
            }   
        }

        function loadThead()
        {
            var headers = table.find("tr.header");
            var results = 0;

            headers.addClass("none");
            headers.filter("."+group).removeClass("none");
            headerCells = headers.filter(":visible").find("td");

            table.attr("class",group);

            if(json.pagination)
                results = json.pagination.total || 0;
            else if(data)
                results = data.length;

            table.find("var").text( results.format() );
        }

        function loadTbody()
        {
            var newrow = output = "";

            if(data)
            {
                for(var i in data)
                { 
                    newrow = templateRow.concat("");
                    
                    for(var j in data[i])
                        newrow = newrow.replace( new RegExp("@data"+j+"@","g") , data[i][j] );

                    output += newrow;
                }
                paginate.next().addClass("none");
            }
            else
            {
                paginate.next().removeClass("none");
            }

            table.find("tbody").html( output );
        }

        function loadPaginate()
        {
            data = json.pagination || false;

            if(data.total)
            {
                var select = paginate.find("select");
                var span = paginate.find("span");
                var link = paginate.find("script").html();

                select.change( function ()
                {
                    window.scrollTo(0,0);
                    httpRequest( section, filters[page], $(this).val()+"&page=1" );
                });

                paginate.find("a[rel]").click( function()
                {
                    var current = span.find("a.active");

                    if( $(this).attr("rel") == "prev")
                        current.prev().click(); 
                    else
                        current.next().click();
                });

                var linklength = Math.ceil( data.total / select.val() );
                var output="";

                for(var i=1;i<=linklength;i++)
                    output += link.concat("").replace(0,i);

                span.html( output );

                span.find("a:eq("+( data.current_page - 1 )+")").addClass("active");

                span.find("a").click( function()
                {
                    window.scrollTo(0,0);
                    httpRequest( section, filters[page], select.val()+"&page="+$(this).text() );
                    return false;
                });

                paginate.removeClass("none");
            }
            else
            {
                paginate.addClass("none");
            }
        }

        function loadTfoot()
        {
            data = json.totals;
            
            var output = "";
            var footers = table.find("tfoot");
            footers.addClass("none");
            var footer = footers.filter("."+group);
            footer.removeClass("none");

            if(data)
            {
                if( page == "uk" )
                    output = $("#tfoot-uk").html().concat("");
                else
                    output = $("#tfoot-"+group).html().concat("");

                for(var i in data)
                    output = output.replace("@data"+i+"@", data[i] );
            }
            footer.html( output );  
        }

        group = $("input[name='group']:checked").val();
        var templateRow = $( "#row-"+group ).html();
        var paginate = $("footer.paginate");
        var data = json.data;

        if(data && group.indexOf("top10") > 0 ) //hack, json different in top10
            data = data[demogIndex].top10;

        loadThead();
        loadTbody();
        loadPaginate();
        loadTfoot();
        fixColspan();

        $.each( $("td em.ico,td strong.ico"), function()  //hacks  
        {
            if( $(this).css("background-position").indexOf("px") == -1 )
            {
                if($(this).text() !="" )
                    $(this).attr("class","ico default");
                else
                    $(this).remove();
            }
        })
        $.each( $("li em.ico,li strong.ico"), function()
        {
            if( $(this).css("background-position").indexOf("px") == -1 )
                $(this).parent().parent().hide();
        })
    }

    function loadFilters()
    {
        var name, templateLine, newLine, output, ul;

        for(var i in json)
        {
            name = json[i].filter;
            templateLine = $( "#filter-"+name ).html();
            ul = $("fieldset."+name+" ul");
            output = "";
  
            for(var j in json[i].data)
            { 
                newLine = templateLine.concat("");
                newLine = newLine.replace( new RegExp("@data0@","g") , json[i].data[j].abb );
                newLine = newLine.replace( "@data1@", json[i].data[j].name );

                if(json[i].data[j].selected === true)
                    newLine = newLine.replace( "input" , "input checked='checked'" );

                output += newLine;
            }
 
            ul.html( output );
            ul.find(".all").text("All").removeAttr("class");
        }

        $.each( $("li em.ico,li strong.ico"), function() //hack
        {
            if( $(this).css("background-position").indexOf("px") == -1 )
                $(this).parent().parent().hide();   //$(this).attr("class","ico default");
        })

        initFilters();
    }

    function loadCharts()
    {
        function displayIcons()
        {
            var tmp = $("#tmp-icon").html();
            var container, tag, output;

            $.each( $("figure") ,function()
            {
                container = $("g.highcharts-data-labels + g.highcharts-axis-labels", $(this) );
                output = "";

                $.each( $("text",container),function()
                {
                    tag = tmp.concat("");
                    tag = tag.replace( new RegExp("@icon@","g"), $(this).text() );
                    tag = tag.replace( "@d0@", $(this).attr("y") );
                    tag = tag.replace( "@d1@", $(this).attr("x") );
                    output += tag;
                });

                $(this).next().html( output )
            });
        }

        var newArticle = output = "";
        var param = [];

        for(var i in json)
        {
            newArticle = article.tmp.concat("");
            newArticle = newArticle.replace( "@datatitle@", json[i].title );
            newArticle = newArticle.replace( "@dataindex@", i );
            output += newArticle;

            param["chart-"+i] = {
                chart: { type: 'column', backgroundColor:'transparent' },
                title: { text: '' },
                credits: { enabled: false },
                exporting: { enabled: false },
                legend: { enabled: false },
                plotOptions: { column: { dataLabels: { enabled:true, style:{color: textColor }, formatter: function() {return Highcharts.numberFormat(this.y,2,".",",")} } } },
                xAxis: { categories: [], labels:{ style:{color: textColor } } },
                yAxis: { title: { text: '' }, labels:{ style:{color: textColor } } },
                series: [ { name: json[i].title, data: [] } ]
            };

            if( json[i].data[0].v1.data )    //attendance
            {
                for(var j in json[i].data)
                {
                    param["chart-"+i].xAxis.labels.rotation = -90;
                    param["chart-"+i].xAxis.categories.push( json[i].data[j].n );
                    param["chart-"+i].series[0].data.push( json[i].data[j].v1.data );
                    param["chart-"+i].tooltip = { formatter: function()
                        { 
                            return "<b>"+this.x.toUpperCase() +"</b><br>"+ Highcharts.numberFormat(this.point.y,2,".",",")  +"<br>"+ json[i].data[j].v1.tooltip 
                        }
                    };
                }

                if( i == "average_utilisation" )
                    param["chart-"+i].yAxis.max = 100;
            }
            else if( json[i].data[0].v2 )     //2 bars per column
            {
                param["chart-"+i].series.push( { name: json[i].title, data: [] } );

                for(var j in json[i].data)
                {
                    param["chart-"+i].xAxis.categories.push( json[i].data[j].n );
                    param["chart-"+i].series[0].data.push( json[i].data[j].v1 );
                    param["chart-"+i].series[1].data.push( json[i].data[j].v2 );
                }
            }
            else                                //1 bar per column
            {
                for(var j in json[i].data)
                {
                    param["chart-"+i].xAxis.categories.push( json[i].data[j].n );
                    param["chart-"+i].series[0].data.push( json[i].data[j].v1 );
                }
            }
        }

        article.container.html( output );

        $.each( $("figure"),function()
        {
            $(this).highcharts( param[ $(this).attr("id") ] );
        });

        $("article a.export").click(function()
        {  
            var figure = $(this).parent().next().next();
            var chart= figure.highcharts();
            chart.exportChart( 
                { type:"image/png", filename:figure.attr("id") },
                { chart: { backgroundColor:"#fff"} }
            );
        });

        if( page == "attendance" && mobile == false )
            displayIcons();
    }

    Number.prototype.format = function(n,x)
    {
        var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    }

    initialize();
 });