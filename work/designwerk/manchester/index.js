(function() 
{   
    "use strict";
    
    function $(selector,object)
	{
	    var nodelist=(object || document).querySelectorAll(selector);
	    nodelist.prop=function(property,value)
	    {
	        for(var i=nodelist.length-1;i>=0;i--)
	        {
	            nodelist[i][property]=value;
	            nodelist[i].index=i;
	        }
	        return nodelist;
	    }
	    nodelist.func=function(func,param,param2)
	    {
	        for(var i=nodelist.length-1;i>=0;i--)
	            nodelist[i][func](param,param2)
	        return nodelist;
	    }
	    return nodelist;
	}

    var page = {
        goTo:function( index )
        {
            this.nav.querySelector(".select").className = "";
            this.nav.querySelector("a:nth-child("+index+")").className = "select";
            this.navTag.textContent = index;
            this.prevTag.href = "#"+ ((index<=1)? page.last:(index-1));
            this.nextTag.href = "#"+ ((index>=page.last)? 1:(index+1));

            document.body.setAttribute("style","top:-"+(window.innerHeight)*(index-1)+"px");
            window.location.hash = index;
        },
        init:function()
        {
        	page.last = $("section").length;
        	page.prevTag = document.querySelector("[rel='prev']");
        	page.nextTag = document.querySelector("[rel='next']");
            page.nav = document.querySelector("nav span");
            page.navTag = document.querySelector("nav b");

            $("a").prop("onclick",function()
            {
                page.goTo( parseInt(this.getAttribute("href").replace("#","")) )
                return false;
            })

        	document.onkeyup=function(e)
            {
                switch(e.keyCode)
                {
                    case 37:case 38:  page.prevTag.onclick();   break;
                    case 39:case 40:  page.nextTag.onclick();   break;
                }
            }
            document.onkeydown=function(e)
            {
                if((e.keyCode > 36) && (e.keyCode < 41))
                    e.preventDefault();
            }

            if(window.location.hash)
                page.goTo( parseInt(window.location.hash.replace("#","")) );
            else
                page.goTo(1);
        }
    }

    page.init();
    
})(); 