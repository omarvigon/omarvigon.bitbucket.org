(function()
{
    "use strict";

    String.prototype.clean = function()
    {
        return parseFloat(this.replace(/,/g,''));
    }
    Number.prototype.format = function(n,x)
    {
        var re = "(\\d)(?=(\\d{"+(x||3)+"})+"+(n>0? "\\.":"$")+")";
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re,"g"),"$1,");
    }

    function $(selector,object)
    {
        var nodelist=(object || document).querySelectorAll(selector);
        nodelist.prop=function(property,value)
        {
            for(var i=nodelist.length-1;i>=0;i--)
            {
                nodelist[i][property]=value;
                nodelist[i].index=i;
            }
            return nodelist;
        }
        nodelist.func=function(func,param,param2)
        {
            for(var i=nodelist.length-1;i>=0;i--)
                nodelist[i][func](param,param2)

            return nodelist;
        }  
        return nodelist;
    }

    function objSort(obj,key,reverse)
    {
        if(!reverse) reverse = 1;

        obj.sort( function(a,b)
        {
            a = parseFloat( a[key] ) || a[key];
            b = parseFloat( b[key] ) || b[key];
            if(a < b) return -1*reverse;
            if(a > b) return 1*reverse;
            return 0;
        });
    }

    function getIndex(obj)
    {
        var parent = obj.parentNode,
            childs = parent.getElementsByTagName( obj.tagName );

        for(var i in childs)
        {
            if(childs[i] == obj)
                return i;
        }
    }

    var storage = {
        save:function( name,value )
        {
            localStorage.setItem( name,value );
        },
        saveAll:function( type )
        {
            var mod = $("article.mod",page.container);
                   
            for(var i in mod)
            {
                if(mod[i].className)
                    storage.save( mod[i].getAttribute("data-id"), mod[i].querySelector("input").value.clean() );
            }
            alert("Data saved");
        },
        get:function( key )
        {
            return parseInt(localStorage.getItem( key )) || false;
        },
        remove:function()
        {
            localStorage.clear();
            alert("Data removed");
        },
        init:function()
        {
            page.popup.querySelector(".save").onclick=function()
            {
                storage.saveAll();
            }
            page.popup.querySelector(".remove").onclick=function()
            {
                storage.remove();
            }
        }
    }

    var map= {
        zoomInit: 1,
        zoomMax: 3,
        centerX: 52,
        centerY: 24,
        ratio: 1.5,
        keyMovement: 100,
        basicWidth: 2400,
        canDrag: false,
        canClick: true,
        tag: false,
        width: 0, height: 0,
        clickX: 0, clickY: 0,
        cursorX: 0, cursorY: 0,
        
        wheel:function(wheelMode)
        {
            var zoom =parseInt(document.body.getAttribute("data-zoom"));
            var newzoom=zoom+wheelMode;
                
            if( (newzoom>=0) && (newzoom<=map.zoomMax) )
                map.setZoom(newzoom);
        },
        setZoom:function(value)
        {
            document.body.setAttribute("data-zoom",value);

            map.width=Math.pow(2,value-1)*map.basicWidth;
            map.height=map.width/map.ratio;
            map.automove();
            map.limits();
            map.tag.className="animate";
        },
        automove:function()
        {
            var newX,newY;
            
            if(map.centerX) //absolute in center
            {
                newX=( (map.centerX*map.width)/100 )*-1 + map.limitX;
                newY=( (map.centerY*map.height)/100 )*-1 + map.limitY;
                map.centerX=false;
                map.centerY=false;
            }
            else      //relative to cursor
            {
                newX=(((map.cursorX*map.width)/100)*-1) + map.clientX;
                newY=(((map.cursorY*map.height)/100)*-1) + map.clientY;
            }
            map.tag.style.left=parseInt(newX)+"px";
            map.tag.style.top=parseInt(newY)+"px";
        },
        limits:function()
        {   
            var left=parseInt(map.tag.style.left);
            var top=parseInt(map.tag.style.top);
        
            if(left > map.limitX)             map.tag.style.left=map.limitX+"px";
            if(left+map.width < map.limitX)   map.tag.style.left=(map.limitX-map.width)+"px";
            if(top > map.limitY)              map.tag.style.top=map.limitY+"px";
            if(top+map.height < map.limitY)   map.tag.style.top=(map.limitY-map.height)+"px";
        },
        setMiddle:function()
        {
            var x=(((parseInt(map.tag.style.left)-map.limitX)*100)/map.width)*-1;
            var y=(((parseInt(map.tag.style.top)-map.limitY)*100)/map.height)*-1;

            map.centerX=x;
            map.centerY=y;
        },
        init:function()
        {
            this.tag = document.querySelector("#div");

            this.tag.onmousedown=function(event)
            {
                document.activeElement.blur();
                map.canDrag=true;
                map.clickX=event.clientX-parseInt(this.style.left);
                map.clickY=event.clientY-parseInt(this.style.top);
                return false;
            }

            this.tag.onmouseup=function()
            {
                map.canDrag=false;
                map.canClick=true;
            }

            this.tag.ondblclick=function()
            {
                map.wheel(1);
            }

            this.tag.onmousewheel=function(event) //webkit
            { 
                map.wheel( (event.wheelDelta < 0)? -1:1 )
            }

            this.tag.addEventListener("DOMMouseScroll",function(event) //firefox
            {
                map.wheel( (event.detail < 0)? 1:-1 )
                    
            },false);
            
            this.tag.addEventListener("webkitTransitionEnd",function() //webkit
            {
                this.removeAttribute("class");
                    
            },true);

            this.tag.addEventListener("transitionend",function() //firefox
            { 
                this.removeAttribute("class");
                
            },true);

            this.tag.onmousemove=function(event)
            {
                map.clientX=event.clientX;
                map.clientY=event.clientY;

                if(map.canDrag)
                {   
                    map.canClick = false;
                    map.tag.style.left=(map.clientX-map.clickX)+"px";
                    map.tag.style.top=(map.clientY-map.clickY)+"px";
                    map.limits();
                }

                map.cursorX=(((map.clientX-parseInt(map.tag.style.left))*100)/map.width).toFixed(2);
                map.cursorY=(((map.clientY-parseInt(map.tag.style.top))*100)/map.height).toFixed(2);
            }

            document.onkeydown=function(event)
            {
                if(event.target.tagName == "BODY")
                {
                    switch(event.keyCode)
                    {
                        case 37: map.tag.style.left=(parseInt(map.tag.style.left)+map.keyMovement)+"px"; break;//left
                        case 38: map.tag.style.top=(parseInt(map.tag.style.top)+map.keyMovement)+"px";   break;//up
                        case 39: map.tag.style.left=(parseInt(map.tag.style.left)-map.keyMovement)+"px"; break;//right
                        case 40: map.tag.style.top=(parseInt(map.tag.style.top)-map.keyMovement)+"px";   break;//down
                        case 107:case 187: map.setMiddle(); map.wheel(1);                                break;//+
                        case 109:case 189: map.setMiddle(); map.wheel(-1);                               break;//-
                    }
                    map.limits();
                }
            }
            window.onresize=function()
            {   
                map.limitX=window.innerWidth/2;
                map.limitY=window.innerHeight/2;
            }
            window.onresize();

            map.setZoom(map.zoomInit);
        },
    }

    var page = {
        iconMinSize:20,
        iconMaxSize:192,
        selectRegion:function( id,mode )
        {
            var g = $("#"+id+" g",map.tag);

            for(var i in g)
            {
                if(g[i].id)
                    page.selectCountry(g[i].id, mode );
            }
        },
        selectCountry:function( id,mode )
        {
            var g = map.tag.querySelector("#"+id),
                input = this.fieldCountry.querySelector("input[value='"+id+"']");

            if(input && !g.getAttribute("style"))
            {
                if(mode)
                {
                    g.setAttribute("class","selected");
                    input.checked = true;
                }
                else
                {
                    g.removeAttribute("class");
                    input.checked = false;
                }

                this.getResults();
            }
        },
        selectCheckbox:function( fieldset )
        {
            var all = fieldset.querySelector("input[value='all']"),
                inputs = $("input",fieldset);

            inputs.prop("onclick",function()
            {
                if(this.value == "all")
                {
                    inputs.prop("checked",all.checked);

                    if(fieldset.name == "regions")
                    {
                        page.selectRegion("afr", this.checked );
                        page.selectRegion("asi", this.checked );
                        page.selectRegion("ams", this.checked );
                        page.selectRegion("eur", this.checked );
                        page.selectRegion("amn", this.checked );
                    }
                }
                else
                {
                    all.checked = false;

                    if(fieldset.name == "regions")
                        page.selectRegion( this.value, this.checked );
                }

                if(fieldset.name == "assets")
                    page.getResults();
            });
        },
        getRegion:function( id )
        {
            return map.tag.querySelector("#"+id).parentNode.id;
        },
        getCountryName:function( id )
        {
            return map.tag.querySelector("#"+id+" text").childNodes[0].textContent;
        },
        getIconSize:function( value )
        {
            var size = parseInt( (value*this.iconMaxSize)/this.topSum);

            if(size > this.iconMaxSize) size = this.iconMaxSize;
            if(size < this.iconMinSize) size = this.iconMinSize;

            return size;
        },
        setIconSize:function( article, value )
        {
            var newSize = this.getIconSize( value ),
                icon = article.querySelector(".icon");

            icon.style.width = newSize+"px";
            icon.style.height = newSize+"px";
        },
        calculate:function( article )
        {
            var i = getIndex( article ),
                id = article.getAttribute("data-id"),
                region = this.results[i].region,
                br = this.results[i].br,
                input = article.querySelector("input").value.clean(),
                newValue = (this.results[i].sum/br) * input,
                totalRegion = 0, totalGlobal = 0, value;

            if(this.results[i].all)
                newValue = this.calculateAll( id, newValue );

            this.outputs[i].textContent = newValue.format();
            this.setIconSize( article, newValue );
            article.className = (input == br)? region:region+" mod";
            
            for(var i in this.results)
            {
                value = this.outputs[i].textContent.clean();

                if(this.results[i].region == region)
                    totalRegion += value;

                totalGlobal += value;
            }

            this[ region ].output.textContent = totalRegion.format();
            this[ region ].className = (totalRegion == this[ region ].result)? region:region+" mod";

            this.glo.output.textContent = totalGlobal.format();
            this.glo.className = (totalGlobal == this.glo.result)? "glo":"glo mod";
        },
        calculateAll:function( id, sum )
        {
            var obj = json[id],
                opr = obj.opr || Math.ceil( (sum/100) * 6.21),
                ooh = Math.ceil( (sum/100) * ((id=="uk")? 24.5:12.89) ),
                adi = obj.adi || Math.ceil( ((sum+opr)/100) * 5 ),
                tangible = sum + ooh + opr + adi;

            if(id == "uk")
                tangible += obj.ise + obj.tic + obj.map + obj.sig + obj.pml + obj.ful + obj.mas + obj.ptt + obj.pla + obj.mta + obj.hos + obj.mcm + obj.oth + obj.web;

            return tangible + parseInt((tangible/100) * obj.int);
        },
        reset:function( article )
        {
            var i = getIndex( article );
            article.querySelector("input").value = this.results[i].br.format();
            this.calculate( article );
        },
        getResults:function()
        {
            var countries = $("input:checked",this.fieldCountry),
                checkbox = $("input:checked",this.fieldAsset),
                id, region, obj, all, sum, bro, broadcaster;

            this.results = [];
            this.afr.result = 0;
            this.asi.result = 0;
            this.amn.result = 0;
            this.eur.result = 0;
            this.ams.result = 0;
            this.glo.result = 0;
            
            if(countries.length == 0)
                countries = $("input",this.fieldCountry);

            for(var i in countries)
            {   
                id = countries[i].value;

                if(id)
                {
                    obj = json[id];

                    if(obj)
                    {
                        if((checkbox.length == 0) || (checkbox[0].value == "all"))
                        {   
                            sum = obj.dig + obj.gen + obj.jer + obj.osl + obj.ost + obj.pre + obj.spo + obj.sta + obj.sub + obj.tro;
                            all = this.calculateAll( id, sum );
                        }
                        else
                        {
                            sum = 0;
                            all = false;

                            for(var j in checkbox)
                            {
                                if( checkbox[j].value )
                                    sum += obj[ checkbox[j].value ] || 0;
                            }
                        }

                        region = this.getRegion( id );
                        this[ region ].result += all || sum;
                        bro = obj.bro;
                        broadcaster = "";

                        if(bro)
                        {
                            for(var j in bro)
                                broadcaster += channels[ bro[j] ] + "<br />";
                        }

                        this.results.push({
                            id: id,
                            region: region,
                            br: storage.get( id ) || obj.br,
                            sum: sum,
                            all: all,
                            bro: broadcaster
                        })
                    }
                }
            }

            objSort( this.results, "sum", -1);
            this.topSum = (this.results[0])? this.results[0].sum:0;
            this.glo.result = this.eur.result + this.afr.result + this.asi.result + this.amn.result + this.ams.result;
            this.total.textContent = this.glo.result.format();
        },
        showResults:function()
        {
            var newTag, txt = "";

            for(var i in this.results)
            {
                newTag = this.tmp.concat("");
                newTag = newTag.replace("@id@", this.results[i].id);
                newTag = newTag.replace("@name@", this.getCountryName( this.results[i].id ) );
                newTag = newTag.replace("@sum@", (this.results[i].all || this.results[i].sum).format() );
                newTag = newTag.replace("@br@", this.results[i].br.format());
                newTag = newTag.replace(/@size@/g, this.getIconSize( this.results[i].sum ) );
                newTag = newTag.replace("@css@", this.results[i].region );
                newTag = newTag.replace("@bro@", this.results[i].bro );

                if(storage.get( this.results[i].id ))
                    newTag = newTag.replace('class="','class="mod ');

                txt += newTag
            }

            this.container.innerHTML = txt;
            this.eur.output.textContent = this.eur.result.format();
            this.afr.output.textContent = this.afr.result.format();
            this.asi.output.textContent = this.asi.result.format();
            this.amn.output.textContent = this.amn.result.format();
            this.ams.output.textContent = this.ams.result.format();
            this.glo.output.textContent = this.glo.result.format();
            this.outputs = $("output",this.container);

            $("article input",this.container).prop("onchange",function()
            { 
                page.calculate( this.parentNode.parentNode.parentNode.parentNode );
            
            }).prop("onclick",function()
            {
                this.value = "";
            });

            $("article .reset",this.container).prop("onclick",function()
            {
                page.reset( this.parentNode.parentNode.parentNode.parentNode );
            })
        },
        autosuggest:function( input )
        {
            var value = input.value.toLowerCase(), li, g;

            if(value == "")
            {
                $("li.none",this.fieldCountry).func("removeAttribute","class");
                $("g.hover",map.tag).func("setAttribute","class","");
            }
            else
            {
                for(var i in this.liCountry)
                {
                    li = this.liCountry[i];

                    if(li.textContent)
                    {
                        g = map.tag.querySelector("#"+li.querySelector("input").value);
                        
                        if(li.textContent.toLowerCase().indexOf( value ) != -1)
                        {
                            li.className = "active";
                            g.setAttribute("class","hover");
                        }
                        else
                        {
                            li.className = "none";
                            g.setAttribute("class","");
                        }
                    }
                }
            }
        },
        populate:function()
        {
            var list = [], newTag = "", txt = "",
                ul = this.fieldCountry.querySelector("#datalist"),
                tmp = ul.innerHTML, 
                text, x, element;

            for(var i in json)
            {
                list.push( {name:page.getCountryName(i), value:i, region:page.getRegion(i)} )

                if(json[i].fan)
                {
                    text = map.tag.querySelector("#"+i+" text");
                    x = text.getAttribute("x");

                    element = document.createElementNS("http://www.w3.org/2000/svg","tspan");
                    element.setAttribute("x",x);
                    element.setAttribute("dy","3");
                    element.textContent = json[i].fan.format();
                    text.appendChild( element );

                    element = document.createElementNS("http://www.w3.org/2000/svg","tspan");
                    element.setAttribute("x",x);
                    element.setAttribute("dy","3");
                    element.textContent = "Premiere League Fans";
                    text.appendChild( element );
                }
            }

            objSort(list, "name");

            for(var i in list)
            {
                newTag = tmp.concat("");
                newTag = newTag.replace("@css@",list[i].region);
                newTag = newTag.replace("@value@",list[i].value);
                newTag = newTag.replace("@name@",list[i].name);
                txt += newTag;
            }

            ul.innerHTML = txt;
            this.liCountry = $("li",ul);

            $("input",ul).prop("onclick",function()
            {
                page.selectCountry( this.value, this.checked );
            });
        },
        tooltip:function()
        {
            this.tooltip = document.querySelector(".tooltip");

            $(".info",this.fieldAsset).prop("onclick",function()
            {
                var active = page.fieldAsset.querySelector(".active"),
                    name = this.parentNode.querySelector("input").value;

                if(active)
                    active.className = active.className.replace(" active","");

                this.className += " active";
                page.tooltip.className = "tooltip "+name;
            });

            this.tooltip.querySelector(".close").onclick=function()
            {
                page.tooltip.className = "tooltip none";
            };
        },
        welcome:function()
        {
            if(!storage.get("welcome"))
            {
                var header = document.querySelector("header");
                header.className = "active";
                header.querySelector("a").onclick=function()
                {
                    header.className = "";
                    storage.save("welcome","1");
                }
            }
        },
        export:function()
        {
            function selectElementContent(el)
            {
                var body = document.body, range, sel;
                if(document.createRange && window.getSelection)
                {
                    range = document.createRange();
                    sel = window.getSelection();
                    sel.removeAllRanges();
                    try
                    {
                        range.selectNodeContents(el);
                        sel.addRange(range);
                    }
                    catch(e)
                    {
                        range.selectNode(el);
                        sel.addRange(range);
                    }
                }
                else if(body.createTextRange)
                {
                    range = body.createTextRange();
                    range.moveToElementText(el);
                    range.select();
                }
            }

            var code = document.querySelector(".code"),
                table = code.querySelector("table");

            this.popup.querySelector(".export").onclick=function()
            {
                var d = new Date(),
                    month = d.getMonth() + 1,
                    day = d.getDate(),
                    date = ((day<10)? "0":"") + day + "/" + ((month<10)? "0":"") + month + "/" + d.getFullYear(),
                    txt = "",
                    checkbox = $("input:checked",page.fieldAsset),
                    titles = $("h3",page.container),
                    inputs = $("input",page.container);
           
                for(var i in checkbox)
                {
                    if( checkbox[i].value && checkbox[i].value!="all" )
                        txt += checkbox[i].parentNode.querySelector("span").textContent + ",";
                }

                table.querySelector(".assets").textContent = txt.slice(0,-1);
                table.querySelector(".date").textContent = date;
                table.querySelector(".glo").textContent = page.glo.output.textContent;
                table.querySelector(".afr").textContent = page.afr.output.textContent;
                table.querySelector(".asi").textContent = page.asi.output.textContent;
                table.querySelector(".amn").textContent = page.amn.output.textContent;
                table.querySelector(".ams").textContent = page.ams.output.textContent;
                table.querySelector(".eur").textContent = page.eur.output.textContent;

                txt = "";
                for(var i in page.results)
                {
                    txt += "<tr>";
                    txt += "<td>"+titles[i].textContent+"</td>";
                    txt += "<td>"+page.outputs[i].textContent+"</td>";
                    txt += "<td>"+inputs[i].value+"</td>";
                    txt += "</tr>";
                }
                
                table.querySelector("tbody").innerHTML = txt;
                code.className = "code active";
                fleXenv.updateScrollBars();//flexscroll
                selectElementContent( table );
            };

            code.querySelector(".close").onclick=function()
            {
                code.className = "code";
            };
            code.querySelector(".select").onclick=function()
            {
                selectElementContent( table );
            };
        },
        init:function()
        {
            this.tmp = document.querySelector("#tmp").innerHTML;
            this.popup = document.querySelector("#popup");
            this.container = this.popup.querySelector(".scroll");
            this.afr = this.popup.querySelector("[data-id='afr']");
            this.afr.output = this.afr.querySelector("output");
            this.asi = this.popup.querySelector("[data-id='asi']");
            this.asi.output = this.asi.querySelector("output");
            this.amn = this.popup.querySelector("[data-id='amn']");
            this.amn.output = this.amn.querySelector("output");
            this.eur = this.popup.querySelector("[data-id='eur']");
            this.eur.output = this.eur.querySelector("output");
            this.ams = this.popup.querySelector("[data-id='ams']");
            this.ams.output = this.ams.querySelector("output");
            this.glo = this.popup.querySelector("[data-id='glo']");
            this.glo.output = this.glo.querySelector("output");
            this.total = document.querySelector("#total output");
            this.fieldRegion = document.querySelector("[name='regions']");
            this.fieldCountry = document.querySelector("[name='countries']");
            this.fieldAsset = document.querySelector("[name='assets']");
              
            this.fieldCountry.querySelector("input[placeholder]").onkeyup = function()
            {
                page.autosuggest( this );
            }

            $("g g",map.tag).prop("onmouseup",function(event)
            {   
                if(map.canClick)
                {
                    if(!this.getAttribute("style"))
                        page.selectCountry( this.id, (this.getAttribute("class")!="selected")? true:false );
                }
            });

            $(".zoom a").prop("onclick",function()
            {
                map.setMiddle();
                (this.rel == "prev")? map.wheel(1):map.wheel(-1);
            });

            this.popup.querySelector(".close").onclick=function()
            {
                page.popup.className = "";
            }
            document.querySelector("button").onclick=function()
            {
                page.showResults();
                page.popup.className = "active";
                fleXenv.updateScrollBars();//flexscroll
                page.popup.querySelector(".flexcroll").fleXcroll.setScrollPos(false,0);
            }
            document.querySelector("form").onsubmit=function()
            {
                return false;
            }

            if(window.matchMedia("(max-height:700px)").matches)
            {
                fleXenv.fleXcrollMain( document.querySelector("form > div") );
            }
        }
    }

    map.init();
    page.init();
    page.populate();
    page.selectCheckbox( page.fieldAsset );
    page.selectCheckbox( page.fieldRegion );
    page.tooltip();
    page.welcome();
    page.export();
    page.getResults();
    storage.init();
})();