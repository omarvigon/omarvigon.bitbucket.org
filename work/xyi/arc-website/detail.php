<?php include_once("head.php"); ?>
<body id="detail">

<!-- Navbar Mobile -->
<aside class="navbar navbar-fixed-top hidden-desktop hidden-tablet">
 <div class="navbar-inner">
   <div class="container">
     <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </a>
     <a class="brand hidden-desktop" href="index.php"><img src="images/uip-logo.png" alt="Logo" style="height:40px;"/></a>
     <div class="nav-collapse collapse" id="main-menu">
      <ul class="nav" id="main-menu-left">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown"> INTERNATIONAL TOOLKIT <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#standard">STANDARD FORMATS</a></li>
            <li><a href="#email">EMAIL TEMPLATES</a></li>
            <li><a href="#skins">SKINS</a></li>
            <li><a href="#enhanced">ENHANCED FORMATS</a></li>
            <li><a href="#expandable">EXPANDABLE FORMATS</a></li>
            <li><a href="#mobile">MOBILE/TABLET SPECIFIC FORMAT</a></li>
            <li><a href="#hpto">HPTO</a></li>
          </ul>
        </li>
        <li><a href="#social">SOCIAL NETWORKING</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown">PUBLISHERS FORMATS <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#xbox">XBOX</a></li>
            <li><a href="#yahoo">YAHOO</a></li>
            <li><a href="#mtv">MTV</a></li>
            <li><a href="#yume">YUME</a></li>
            <li><a href="#wikia">WIKIA</a></li>
            <li><a href="#msn">MSN</a></li>
            <li><a href="#spotify">SPOTIFY</a></li>
            <li><a href="#milenial">MILLENIAL</a></li>
            <li><a href="#min90">90 MIN</a></li>
            <li><a href="#other">OTHER</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown">LOCALISED FORMATS <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#au">AU</a></li>
            <li><a href="#br">BR</a></li>
            <li><a href="#fr">FR</a></li>
            <li><a href="#de">DE</a></li>
            <li><a href="#it">IT</a></li>
            <li><a href="#mx">MX</a></li>
            <li><a href="#es">ES</a></li>
          </ul>
        </li>
      </ul>
     
     </div>
   </div>
 </div>
</aside>
<!-- Navbar Mobile -->

<?php include_once("header.php"); ?>
              
        <a class="dropdown-toggle" data-toggle="dropdown">INTERNATIONAL TOOLKIT <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li class="active"><a href="#standard">STANDARD FORMATS</a></li>
          <li><a href="#email">EMAIL TEMPLATES</a></li>
          <li><a href="#skins">SKINS</a></li>
          <li><a href="#enhanced">ENHANCED FORMATS</a></li>
          <li><a href="#expandable">EXPANDABLE FORMATS</a></li>
          <li><a href="#mobile">MOBILE/TABLET SPECIFIC FORMAT</a></li>
          <li><a href="#hpto">HPTO</a></li>
        </ul>
      </li>
      <li><a href="#social">SOCIAL NETWORKING</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown">PUBLISHERS FORMATS <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#xbox">XBOX</a></li>
          <li><a href="#yahoo">YAHOO</a></li>
          <li><a href="#mtv">MTV</a></li>
          <li><a href="#yume">YUME</a></li>
          <li><a href="#wikia">WIKIA</a></li>
          <li><a href="#msn">MSN</a></li>
          <li><a href="#spotify">SPOTIFY</a></li>
          <li><a href="#milenial">MILLENIAL</a></li>
          <li><a href="#min90">90 MIN</a></li>
          <li><a href="#other">OTHER</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown">LOCALISED FORMATS <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#au">AU</a></li>
          <li><a href="#br">BR</a></li>
          <li><a href="#fr">FR</a></li>
          <li><a href="#de">DE</a></li>
          <li><a href="#it">IT</a></li>
          <li><a href="#mx">MX</a></li>
          <li><a href="#es">ES</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>

<section data-name="#standard" class="on">
  <h1><span>STANDARD FORMATS</span></h1>

  <?php  
    $movies = simplexml_load_file('xml/main.xml');
        
    foreach ($movies as $movie):
      if ($_GET['id'] == $movie->movieId)
      {
        $title = $movie->title;
        $oneSheet = $movie->oneSheet;
        $synopsis = $movie->synopsis;
        $youtubeId = $movie->youtubeId;
        $siteLink = $movie->siteLink;
        $fbLink = $movie->social->fb;
        $twitterLink = $movie->social->twitter;
        $youtubeLink = $movie->social->youtube;
        $gallery = $movie->gallery;
        $comingsoon = $movie['comingsoon'];
        $category = ($comingsoon == "true" ? "coming soon" : "movies");
        break;
      }
    endforeach;
    ?>
<?php for($i=0;$i<4;$i++) { ?>
    <div class="movieItem">
        <div class="movieImg"><a href="#"><img src="images/picture1.jpg"></a></div>
        <div class="info"><h3><?php echo $title;?> Payoff Specific UK - Leaderboard (728x90)</h3><p><em>Standard format - Payoff artwork <i>Updated on 14/07/2015</i></em></p>
        <div class="moreHolder">
          <a class="btn more btn-small" target="_blank" href="http://platform.mediamind.com/Eyeblaster.Preview.Web/Default.aspx?previewParams=AmewTXgrNWkz0zbKb+S4P7ep19jpURHnSVovtdNdVSg7r4uwNAn2rWx3v51m9R5EAYfedzFIHlFtUfiWUgR3tQm2A0Mft0zDQtkYa4vtXCXZl8y/gSd5xQ==&AdID=29133017&lang=en-US">Preview file</a> 
          <a class="btn more btn-small" target="_blank" href="https://arc.paramount.com/teams/do/StreamAssetContent?stream_id=21157587654531c8fc8ba35ea4ce0d6ec963a23b&stream_id_type=asset&content_kind=master&disposition=attachment">Download file</a>
        </div></div>
    </div>
    <div class="movieItem">
        <div class="movieImg"><a href="#"><img src="images/picture2.jpg"></a></div>
        <div class="info"><h3><?php echo $title;?> Payoff Specific UK - Leaderboard (640x180)</h3><p><em>Standard format - <i>Updated on 14/07/2015</i></em></p>
        <div class="moreHolder">
          <a class="btn more btn-small" target="_blank" href="http://platform.mediamind.com/Eyeblaster.Preview.Web/Default.aspx?previewParams=AmewTXgrNWkz0zbKb+S4P7ep19jpURHnSVovtdNdVSg7r4uwNAn2rWx3v51m9R5EAYfedzFIHlFtUfiWUgR3tQm2A0Mft0zDQtkYa4vtXCXZl8y/gSd5xQ==&AdID=29133017&lang=en-US">Preview file</a> 
          <a class="btn more btn-small" target="_blank" href="https://arc.paramount.com/teams/do/StreamAssetContent?stream_id=21157587654531c8fc8ba35ea4ce0d6ec963a23b&stream_id_type=asset&content_kind=master&disposition=attachment">Download file</a>
        </div></div>
    </div>
<?php } ?>
</section>
<section data-name="#email">
  <h1><span>EMAIL TEMPLATES</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#skins">
  <h1><span>SKINS</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#enhanced">
  <h1><span>ENHANCED FORMATS</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#expandable">
  <h1><span>EXPANDABLE FORMATS</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#mobile">
  <h1><span>MOBILE/TABLET SPECIFIC FORMAT</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#hpto">
  <h1><span>HPTO</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>

<section data-name="#social">
  <h1><span>SOCIAL</span></h1>
  <p></p>
</section>

<section data-name="#xbox">
  <h1><span>XBOX</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#yahoo">
  <h1><span>YAHOO</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#mtv">
  <h1><span>MTV</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#yume">
  <h1><span>YUME</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#wikia">
  <h1><span>WIKIA</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#msn">
  <h1><span>MSN</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#spotify">
  <h1><span>SPOTIFY</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#milenial">
  <h1><span>MILLENIAL</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#min90">
  <h1><span>90 MIN</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#other">
  <h1><span>OTHER</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>

<section data-name="#au">
  <h1><span>AU</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#br">
  <h1><span>BR</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#fr">
  <h1><span>FR</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#de">
  <h1><span>DE</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#it">
  <h1><span>IT</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#mx">
  <h1><span>MX</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>
<section data-name="#es">
  <h1><span>ES</span></h1>
  <p class="tc">THERE IS CURRENTLY NO CONTENT AVAILABLE IN THIS SECTION</p>
</section>

<?php include_once("footer.php"); ?>
