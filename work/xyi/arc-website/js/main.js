// tooltips

$('a[rel=tooltip]').tooltip({
	'placement': 'bottom'
});

$(document).ready(function() {

	$("#detail ul.nav a[href]").click( function()
	{
		$(".hovered").removeClass("hovered");
		$(this).parent().parent().prev().addClass("hovered");

		$("li.active").removeClass("active");
		$(this).parent().addClass("active");

		$("section.on").removeClass("on");
		$("[data-name='"+$(this).attr("href")+"']").addClass("on");
	});
});

(function ($) {

	$(function(){
		// fix sub nav on scroll
		var $win = $(window),
			$body = $('body'),
			$nav = $('.subnav'),
			navHeight = $('.navbar').first().height(),
			subnavHeight = $('.subnav').first().height(),
			subnavTop = $('.subnav').length && $('.subnav').offset().top - navHeight,
			marginTop = parseInt($body.css('margin-top'), 10);
			isFixed = 0;

		processScroll();

		$win.on('scroll', processScroll);

		function processScroll() {
			var i, scrollTop = $win.scrollTop();

			if (scrollTop >= subnavTop && !isFixed) {
				isFixed = 1;
				$nav.addClass('subnav-fixed');
				$body.css('margin-top', marginTop + subnavHeight + 'px');
			} else if (scrollTop <= subnavTop && isFixed) {
				isFixed = 0;
				$nav.removeClass('subnav-fixed');
				$body.css('margin-top', marginTop + 'px');
			}
		}

	});

})(window.jQuery);