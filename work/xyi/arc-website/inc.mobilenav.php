<!-- Navbar Mobile -->
<aside class="navbar navbar-fixed-top hidden-desktop hidden-tablet">
 <div class="navbar-inner">
   <div class="container">
     <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </a>
     <a class="brand hidden-desktop" href="index.php"><img src="images/uip-logo.png" alt="Logo" style="height:40px;"/></a>
     <div class="nav-collapse collapse" id="main-menu">
      <ul class="nav" id="main-menu-left">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown"> ARC INTERACTIVE CONTENT PREVIEWS <b class="caret"></b></a>
          <ul class="dropdown-menu" id="movies-menu">
            <li><a href="index.php">CURRENT & UPCOMING RELEASES</a></li>
            <li><a href="archive.php">ARCHIVE</a></li>
          </ul>
        </li>
      </ul>
     
     </div>
   </div>
 </div>
</aside>
<!-- Navbar Mobile -->