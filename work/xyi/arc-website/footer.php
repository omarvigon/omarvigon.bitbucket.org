<footer id="footer" class="main">
  <h5><a href="https://arc.paramount.com" target="_blank">ARC.PARAMOUNT.COM</a></h5>
  <p>&copy; 2015 PARAMOUNT PICTURES.<br />ALL RIGHTS RESERVED.</p> 
</footer>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-485318-4', 'auto');ga('send', 'pageview');
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-transition.js"></script>
<script src="js/main.js"></script>
</body>
</html>