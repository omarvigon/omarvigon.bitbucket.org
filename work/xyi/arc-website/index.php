<?php include_once("head.php"); ?>
<body id="home">

<?php include_once("inc.mobilenav.php"); ?>
<?php include_once("header.php"); ?>
              
          <a class="dropdown-toggle" data-toggle="dropdown">ARC INTERACTIVE CONTENT PREVIEWS <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="active"><a href="index.php">CURRENT & UPCOMING RELEASES</a></li>
            <li><a href="archive.php">ARCHIVE</a></li>
          </ul>
        </li>
      </ul>
  </div>
</nav>

<section>
  <h1><span>CURRENT &amp; UPCOMING RELEASES</span></h1>
	<?php
	$movies = simplexml_load_file('xml/main.xml');
	$i = -1;
	foreach ($movies as $movie):
			
		$title = $movie->title;
		$oneSheet = $movie->oneSheet;
		$brief = $movie->brief;
    $date = $movie->releaseDate;
		$movieLink = "detail.php?id=".$movie->movieId;
		
  	if($movie['comingsoon'] == 'false')
  	{
  			echo '<div class="movieItem">
          <div class="movieImg"><a href="'.$movieLink.'"><img src="'.$oneSheet.'" /></a></div>
          <div class="info"><h3>'.$title.'</h3><p><em>'.$date.'</em></p><p class="desc">'.$brief.'</p>
  			<div class="moreHolder"><a class="btn more btn-small" href="'.$movieLink.'">View more</a></div></div></div>';
  	}
	endforeach;
	?>
</section>
<?php include_once("footer.php"); ?>