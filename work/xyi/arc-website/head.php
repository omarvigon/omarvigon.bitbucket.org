<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale = 1,user-scalable=no,maximum-scale=1.0">
    <title>ARC : Asset Resource Centre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ARC : Asset Resource Centre">
    <!--
        http://www.xyi.com/paramount/arc_interactive_previews/new_preview_page/index.php
        http://www.xyi.com/paramount/arc_interactive_previews/
        http://uip.com/
    -->
    <!--[if lt IE 9]>  
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Lato:400italic,400,700,700italic,900' rel='stylesheet' type='text/css'>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>