// JavaScript Document
//HTML5 Ad Template JS from DoubleClick by Google

//Declaring elements from the HTML i.e. Giving them Instance Names like in Flash - makes it easier

var EndFrame;
var container;
var content;
var bgExit;
var Girl;
var Cans;
var MainTxt;
var off;
var Man;
var line;
var title;

//Function to run with any animations starting on load, or bringing in images etc
politeInit = function(){
	
	//Assign All the elements to the element on the page
	container = document.getElementById('container_dc');
	content = document.getElementById('content_dc');	
	bgExit = document.getElementById('background_exit_dc');	
	Girl = document.getElementById('Girl');
   Man = document.getElementById('Man');
	Cans = document.getElementById('Cans');
	MainTxt = document.getElementById('MainTxt');
	off = document.getElementById('off');
   line = document.getElementById('line');
    title = document.getElementById('title');
	
	
	//Bring in listeners i.e. if a user clicks or rollsover
	addListeners();	
	//Show Ad
	container.style.display = "block";
	
	//Call Dynamic
	getDynamic();
    
    startingAd();
    
    console.trace("DC READY");
}

//Add Event Listeners
addListeners = function() {
	bgExit.addEventListener('click', bgExitHandler, false);	
}

bgExitHandler = function(e) {
	//Call Exits
	Enabler.exit('HTML5_Background_Clickthrough');	
    
    TweenMax.killAll();
    TweenLite.to(Girl,0.001,{delay:0,left:0,alpha:0, ease:Quad.easeInOut });
    TweenLite.to(Man,0.001,{delay:0,left:720,alpha:0, ease:Quad.easeInOut });
    
    TweenLite.to(Girl,0.001,{delay:0,alpha:1, ease:Quad.easeInOut });
    TweenLite.to(Man,0.001,{delay:0,alpha:1, ease:Quad.easeInOut });
    
    TweenLite.to(Cans, 0.001, {delay:0,alpha:1, ease:Quad.easeInOut});
    TweenLite.to(MainTxt, 0.001,{delay:0, alpha:0, top:-5,height:130,left:190, });
    TweenLite.to(title, 0.001,{delay:0, alpha:0, height:110,top:110,left:200, });
    TweenLite.to(off, 0.001,{delay:0, alpha:1, height:162,top:50,left:265, });
    TweenLite.to(line,0.001,{delay:0, alpha:1, width:208, ease:Quad.easeInOut  });
}

getDynamic = function(){
	//DynamicContent Start: HTML5 invocation code.
	
	// set the dynamic
	setDynamic()
}

setDynamic = function(){
	
	
}

window.onload = function() {
    
        //Initialize Enabler
		if (Enabler.isInitialized()) 
        {
			init();
		} else 
        {
			Enabler.addEventListener(studio.events.StudioEvent.INIT, init);
		}
		//Run when Enabler is ready
		function init()
        {
			//politeInit();
			if(Enabler.isPageLoaded())
            {
				politeInit();
			}else
            {
				Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, politeInit);
			} 
		}
    
        console.trace("JS READY");
};

startingAd = function() {
    
    var container = document.getElementById("container_dc");
	var content = document.getElementById("content_dc");	
	var bgExit = document.getElementById("background_exit_dc");	
    var Girl = document.getElementById('Girl');
	var Cans = document.getElementById('Cans');
	var MainTxt = document.getElementById('MainTxt');
	var off = document.getElementById('off');
    var Man = document.getElementById('Man');
    var line = document.getElementById('line');
     var title = document.getElementById('title');
		var blurElement = {a:50};
	
		TweenLite.to(Girl,0.01,{delay:0,left:0,alpha:0, ease:Quad.easeInOut });
    TweenLite.to(Man,0.01,{delay:0,left:720,alpha:0, ease:Quad.easeInOut });
    
    TweenLite.to(Girl,0.7,{delay:0,alpha:1, ease:Quad.easeInOut });
    TweenLite.to(Man,0.7,{delay:0,alpha:1, ease:Quad.easeInOut });
    
    
    TweenLite.to(Cans, 0.3, {delay:0.7,alpha:1, ease:Quad.easeInOut});
    TweenLite.to(MainTxt,0.7,{delay:1.4,alpha:1, ease:Quad.easeInOut });
    
    TweenLite.to(MainTxt, 0.1,{delay:1.7,height:132,top:-8,left:187, });
     TweenLite.to(MainTxt, 0.1,{delay:1.8,top:-6,left:189, });
     TweenLite.to(MainTxt, 0.1,{delay:1.9,top:-5,height:130,left:190, });
    
     TweenLite.to(blurElement,0.4, {delay:1.4,a:0, onUpdate:applyBlurM});
    
    TweenLite.to(title, 0.1,{delay:2.1,alpha:1, ease:Quad.easeInOut });
    
      TweenLite.to(blurElement,0.4, {delay:2.1,a:0, onUpdate:applyBlur});
    
        TweenLite.to(off,0.4, {delay:5.3,a:0, onUpdate:applyBlurO});

//here you pass the filter to the DOM element
function applyBlurM()
{
    TweenLite.set(['#MainTxt'], {webkitFilter:"blur(" + blurElement.a + "px)",filter:"blur(" + blurElement.a + "px)"});  
};
    

//here you pass the filter to the DOM element
function applyBlur()
{
    TweenLite.set(['#title'], {webkitFilter:"blur(" + blurElement.a + "px)",filter:"blur(" + blurElement.a + "px)"});  
};
    
    //here you pass the filter to the DOM element
function applyBlurO()
{
    TweenLite.set(['#off'], {webkitFilter:"blur(" + blurElement.a + "px)",filter:"blur(" + blurElement.a + "px)"});  
};
    
    TweenLite.to(title, 0.4,{delay:2.1,height:110,top:110,left:200, ease:Quad.easeInOut });
    
    TweenLite.to(title, 0.1,{delay:2.5,height:112,top:107,left:197, });
     TweenLite.to(title, 0.1,{delay:2.6,top:111,left:199, });
     TweenLite.to(title, 0.1,{delay:2.7,height:110,top:110,left:200, });
    
    TweenLite.to(MainTxt, 0.1,{delay:2.5,height:132,top:-8,left:187, });
     TweenLite.to(MainTxt, 0.1,{delay:2.6,top:-6,left:189, });
     TweenLite.to(MainTxt, 0.1,{delay:2.7,top:-5,height:130,left:190, });
    
    TweenLite.to(title, 0.3,{delay:5,alpha:0, ease:Quad.easeOut });
    TweenLite.to(MainTxt, 0.3,{delay:5,alpha:0, ease:Quad.easeOut });
    
    TweenLite.to(off, 0.1,{delay:5.3,alpha:1, ease:Quad.easeInOut });    
    
    TweenLite.to(off, 0.4,{delay:5.3,height:162,top:50,left:265, ease:Quad.easeInOut });
    
    TweenLite.to(off, 0.1,{delay:5.7,height:164,top:57,left:262, });
     TweenLite.to(off, 0.1,{delay:5.8,top:51,left:264, });
     TweenLite.to(off, 0.1,{delay:5.9,height:162,top:50,left:265, });
    
    
    //TweenLite.to(line,0.01,{delay:8.5, alpha:1, ease:Quad.easeInOut  });
    TweenLite.to(line,0.3,{delay:5.9, width:208, ease:Quad.easeInOut  });
		
		//fade in :
    
}