var frameDuration = 1250;
var animationSpeed = 800;
var animationSpeedOut = 800;

var fadeIn = function(e){
  var element = document.getElementById(e);
  element.classList.add("fadeIn");
}

var fadeOut = function(e){
  var element = document.getElementById(e);
  element.className = element.className.replace(/\bfadeIn\b/,'');
  element.classList.add("fadeOut");
}

var zoomIn = function(e){
  var element = document.getElementById(e);
  element.className = element.className.replace(/\bzoomIn\b/,'');
  element.classList.add("zoomIn");
}

function startAnimation() {
  fadeIn('bg-img1');
  fadeIn('jaguar-logo');
  fadeIn('sub-head');
  fadeIn('text-1'); 
  setTimeout(frame2,animationSpeed *2);
}

function frame2(){
  zoomIn('bg-img1');
  fadeIn('text-2');
  setTimeout(frame2out,frameDuration*2); 
}

function frame2out(){
  fadeOut('sub-head');
  fadeOut('text-1');
  fadeOut('text-2');
  setTimeout(frame3,animationSpeedOut);   
}

function frame3(){
  fadeIn('text-3');
  fadeIn('important-info');
  setTimeout(frame3out,frameDuration*3); 
}

function frame3out(){
  fadeOut('text-3');
  fadeOut('important-info');
  setTimeout(frame4,animationSpeedOut);   
}

function frame4(){
  fadeIn('text-4');
  setTimeout(frame4out,frameDuration*2); 
}

function frame4out(){
  fadeOut('text-4');
  setTimeout(frame5,animationSpeedOut);   
}

function frame5(){
  fadeIn('sub-head2');
  fadeIn('text-5');
  fadeIn('cta');
}
