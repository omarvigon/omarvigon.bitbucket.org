

var Banner = Banner || {};
var Dom = Dom || {},  timeline;
(function () {
	Banner = {
		timers:[],

		init: function () {
			document.body.className = "";

    
            var exitBtn = document.querySelector('.adContent');

					exitBtn.addEventListener('mouseenter',function(){

                        Dom.addClass('.arrow','pulse');



					});
					exitBtn.addEventListener('mouseleave',function(){

                        Dom.removeClass('.arrow','pulse');


					});


            var bgExit = document.getElementById('background_exit_dc');
            bgExit.addEventListener('click', Banner.bgExitHandler, false);

            Banner.animate();

		},

        bgExitHandler: function( e ) {

            Enabler.exit('HTML5_Background_Clickthrough');
        },

        adFinished: function( e ) {


            // looping - change numLoops to set number of loops



            // if (currLoop < numLoops )
            // {

            //     currLoop++;
            //     TweenMax.delayedCall(2,Banner.restart);

            // }


        },

         panImage:function()
        {

         TweenMax.to( "#main_image", 15, {x:0,y:-2,z: 0.1,rotationZ: 0.01,scale:1, ease: Power3.easeOut, force3D: true } );


        },



       

        restart:function(){

            TweenMax.to( "#blocker", 0.4, {autoAlpha:1, ease: Sine.easeOut, force3D: true } );
            TweenMax.delayedCall(1,Banner.animate);

        },

         
         animate : function () {

            var blocker = document.getElementById('blocker');
            var easeType = Sine.easeOut;
            var speenIn =0.4;
            var speenOut =0.4;


            TweenMax.set(blocker, { autoAlpha:1});
            TweenMax.set(".cta", { autoAlpha:0});
            TweenMax.set("#main_image", { scale:1.2,x:0});
            TweenMax.set("#image_container",{perspective:1000});




            timeline = new TimelineLite({
                onComplete: Banner.adFinished
            });

           TweenMax.delayedCall(0,Banner.panImage);


           timeline.add(TweenMax.to(blocker, 0.5, { autoAlpha: 0, ease: Sine.easeOut}));
           timeline.add( TweenMax.to( "#copy1" , speenIn, { autoAlpha:1, ease: easeType, force3D: true } ));
           timeline.add( TweenMax.to( "#copy2" , speenIn, { autoAlpha:1, ease: easeType, force3D: true } ),"+=0.3");
           timeline.add( TweenMax.to( ["#copy1","#copy2"] , speenOut, { autoAlpha:0, ease: easeType, force3D: true } ),"+=1.6");
           timeline.add( TweenMax.to( "#copy3" , speenIn, { autoAlpha:1, ease: easeType, force3D: true } ),"+=0.3");
           timeline.add( TweenMax.to( ".cta" , speenOut, { autoAlpha:1, ease: easeType, force3D: true } ),"+=0.3");




		}
	}
}());


(function(){

	Dom = {

		addClass : function (element , className) {
			var el = document.querySelector(element);

			if(!el.classList.contains(className)) {
					el.classList.add(className);
			}


		},
		removeClass : function (element , className) {

			var el = document.querySelector(element);

			if(el.classList.contains(className)) {
					el.classList.remove(className);
			}


		}
	}


}());


//window.onload = JaguarBanner.init;
