//var centreName;
var informationExit
var ctaExit

if (!Enabler.isInitialized()) {
  Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitialized);
} else {
  enablerInitialized();
}

function enablerInitialized() {

  document.getElementById('backgroundexit').addEventListener('click', bgExitHandler, false);
  document.getElementById('cta').addEventListener('click', bgExitHandler, false);
  document.getElementById('important-info').addEventListener('click', informationExit, false);

  if (!Enabler.isPageLoaded()) {
    Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, init);
  } else {
    init();
  }

}

function init() {
  // Enabler.setProfileId();
  // var devDynamicContent = {};

  //DYNAMIC PROFILE HERE IF NEEDED

  informationExit = "http://used.jaguar.co.uk/content/promotion_tnc";
  ctaExit = "http://used.jaguar.co.uk/";

  startAnimation();
}

function bgExitHandler(e) {
  e.preventDefault();
  Enabler.exitOverride('CTA', ctaExit);
  return false;
}

function informationExit(e) {
  e.preventDefault();
  Enabler.exitOverride('important-info', informationExit);
  return false;
}