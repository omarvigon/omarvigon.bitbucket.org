(function()
{	
	var conf={
		"zoomInit":4,				/*initial zoom*/
		"zoomMax":6,				/*maximum level of zoom. it needs to match with the CSS*/
		"centerX":48.7,				/*initial coordenate to display (in %)*/
		"centerY":47.3,				
		"viewboxX":16000,			/*might be get it from the viewbox attribute (in pixels)*/
		"viewboxY":10000,
		"slidebarPos":114,			/*(in pixels) top of nav + height of the a.icon, might be calculated css*/
		"slidebarHeight":150,		/*(in pixels) height of div - height of the slide, might be calculated css */
		"keyMovement":100,			/*value (in pixels) to move when you use the keyboard */
		"clientX":false,			/*alias for event.clientX*/
		"clientY":false,
		"inputFrom":document.querySelector("[name='from']"),
		"inputTo":document.querySelector("[name='to']"),
		"inputFilter":document.querySelector("[name='filter']")
	};
	
	/*
	*example of conexion with 'Bucarest', stop in 'Berlin'. to be improved
	*/
	var conex={
		"c1":{"name":"BUH","stop":"BER"}
	}
	
	/*
	*functions to convert longitude/latitude into x/y for the map
	*/
	var mercator={
		"_leftLong":-167.5,
		"_rightLong":190.2,
		"_topLat":83.7,
		"_bottomLat":-55.6,
		
		"longitudeToX":function(long)
		{
			return (long - this._leftLong) / (this._rightLong - this._leftLong) * map.width;
		},
		
		"latitudeToY":function(lat)
		{
			return (this.mercatorLatToY(lat) - this.mercatorLatToY(this._topLat)) / (this.mercatorLatToY(this._bottomLat) - this.mercatorLatToY(this._topLat)) * map.height;
		},
		
		"mercatorLatToY":function(lat)
		{
			if(lat > 89.5)	lat = 89.5;
			if(lat < -89.5)	lat = -89.5;
	
			var lat_rad = lat * (Math.PI/180);
			var y = 0.5 * Math.log((1 + Math.sin(lat_rad)) / (1 - Math.sin(lat_rad)));
			var y_deg = (y / 2) * (180/Math.PI);			
			return y_deg;
		}
	}
	
	/*
	*general selector for multiple elements, similar to 'sizzle' in jquery
	*param - string for css selector, and html element
	*/
	function $(selector,object)
	{
		var nodelist={};
		nodelist=(object || document).querySelectorAll(selector);
		nodelist.prop=function(property,value)		{for(var i=nodelist.length-1;i>=0;i--)	nodelist[i][property]=value}
		nodelist.func=function(func,param,param2)	{for(var i=nodelist.length-1;i>=0;i--)	nodelist[i][func](param,param2)}
		return nodelist;
	}
	
	/*
	*main object
	*/
	var map=document.querySelector("figure");
	
	/*
	*when you click the map, this can be dragged. also it captures the click position
	*/
	map.onmousedown=function(event)
	{
		map.canDrag=true;
		map.clickX=event.clientX-parseInt(map.style.left);
		map.clickY=event.clientY-parseInt(map.style.top);
		document.activeElement.blur();
		return false;
	}
	
	/*
	*when you use the mousewheel, increase o decrease the zoom
	*param - the wheelMode might be +1 or -1
	*/
	map.wheel=function(wheelMode)
	{
		var newzoom=map.getZoom()+wheelMode;
			
		if( (newzoom>=0) && (newzoom<=conf.zoomMax) )
		{
			map.setZoom(newzoom);
			map.setSlide(false,newzoom);	
		}
	}
	
	/*
	*if you doble-click the map, its like if you use the mousewheel to increase
	*/
	map.ondblclick=function()
	{
		map.wheel(1);
	}
	
	/*
	*set the [data-zoom] attribute of the <body>, and get the new with and height (in pixels)
	*param - number, the new value of the zoom
	*/
	map.setZoom=function(value)
	{
		document.body.setAttribute("data-zoom",value);
		
		map.width=Math.pow(2,value-1)*1000; 					//to be improved, ratio?
		map.height=map.width/(conf.viewboxX/conf.viewboxY);		
		map.automove();
		map.limits();
		
		if(conf.zoomInit || map.slide.canDrag) 	//no animation the first time nor when you drag the zoombar
			conf.zoomInit=false;
		else
			map.animate();
			
	}
	
	/*
	*returns the value of the current [data-zoom]
	*/
	map.getZoom=function()
	{
		return parseInt(document.body.getAttribute("data-zoom"));
	}
	
	/*
	*add a class to animate the <figure> with a CSS transition
	*/
	map.animate=function()
	{
		map.className="animate";
	}
	
	/*
	*it triggers when the CSS transition is finished
	*/
	map.animateEnd=function()
	{
		map.removeAttribute("class");
	}
	
	/*
	*after a zoom, the map needs to move automaticly: 1) relative to the position of the mouse, or, 2) absolute in the center
	*/
	map.automove=function() 
	{
		var newX,newY;
		
		if(conf.centerX) //absolute in the center
		{
			newX=( (conf.centerX*map.width)/100 )*-1 + map.limitX;
			newY=( (conf.centerY*map.height)/100 )*-1 + map.limitY;
			
			conf.centerX=false;
			conf.centerY=false;
		}
		else			//relative to the cursor
		{
			newX=(((map.cursorX*map.width)/100)*-1) + conf.clientX;
			newY=(((map.cursorY*map.height)/100)*-1) + conf.clientY;
		}

		map.style.left=newX+"px";
		map.style.top=newY+"px";
	}	

	/*
	*check if the position of the map is beyond the boundaries established
	*/
	map.limits=function()
	{	
		var left=parseInt(map.style.left);
		var top=parseInt(map.style.top);
	
		if( left > map.limitX )
			map.style.left=map.limitX+"px";

		if( left+map.width < map.limitX )
			map.style.left=(map.limitX-map.width)+"px";
		
		if( top > map.limitY )
			map.style.top=map.limitY+"px";

		if( top+map.height < map.limitY )
			map.style.top=(map.limitY-map.height)+"px";
			
		if(map.miniarea)
			map.miniarea.automove();
	}
	
	/*
	*returns the center-x and center-y coordenates of the map, in the screen (in %)
	*/
	map.getMiddle=function()
	{
		var center={};
		center.x=(((parseInt(map.style.left)-map.limitX)*100)/map.width)*-1;
		center.y=(((parseInt(map.style.top)-map.limitY)*100)/map.height)*-1;
		return center;
	}
	
	/*
	*set the position of the map in the center, according to the screen
	*/
	map.setMiddle=function()
	{
		var center=map.getMiddle();
		conf.centerX=center.x;
		conf.centerY=center.y;
	}
	
	/*
	*returns a string, with the name country of a <li> element, i.e. "FR"
	*/
	map.getCountry=function(li)
	{
		return li.parentElement.getAttribute("data-name");
	}
	
	/*
	*it triggers when you click a city, or type the text in the input field
	*/
	map.selectCity=function(li)
	{	
		if(map.origin==false) 	//departure
		{
			map.origin=li;
			map.origin.setAttribute("class","active1");
			map.originName=map.origin.getAttribute("data-name");
			
			conf.inputFrom.value=map.origin.textContent+" ("+map.originName+")";
			conf.inputFilter.removeAttribute("hidden");
		}
		else					//destination
		{	
			map.unselectCity();
			map.destiny=li;
			map.destiny.setAttribute("class","active2");
			map.destinyName=map.destiny.getAttribute("data-name");
			
			conf.inputTo.value=map.destiny.textContent+" ("+map.destinyName+")";
			
			map.tooltipShow("#tooltip_city",map.destiny.style.left,map.destiny.style.top);
			map.tooltipFill(false);

			map.lineDraw(1,map.origin,map.destiny);
			map.lineAnimate(1);
			map.showStops();
		}		
	}
	
	/*
	*unselect a city previously clicked. hide the tooltip,remove the lines, reset the input field
	*/
	map.unselectCity=function()
	{
		map.tooltipHide();
		
		var lines=$(".extra path");
		lines.func("removeAttribute","class");
		lines.func("removeAttribute","d");
		
		if(map.destiny)
		{
			map.destiny.removeAttribute("class");
			map.destiny=false;
		}
	
		conf.inputTo.value="";	
	}
	
	/*
	*when connections match, it changes the lines drawn. to be improved with a loop with the conex object
	*/
	map.showStops=function()
	{
		if( (map.originName==conex.c1.name && map.destinyName!=conex.c1.stop ) || (map.destinyName==conex.c1.name && map.originName!=conex.c1.stop))
		{
			map.stop=map.querySelector("li[data-name='"+conex.c1.stop+"']");
				
			map.lineDraw(1,map.origin,map.stop);
			map.lineAnimate(1);
			map.lineDraw(2,map.stop,map.destiny);
			map.lineAnimate(2);
		}
	}
	
	/*
	*draw a curve line in svg with the <path> element, between origin and destiny. it can be cubic or quadratic
	*param - the number related to the id
	*param - from/to li objects
	*/
	map.lineDraw=function(value,from,to)
	{	
		var iniX=(parseFloat(from.style.left) * conf.viewboxX)/100;
		var iniY=(parseFloat(from.style.top) * conf.viewboxY)/100;
		var finX=(parseFloat(to.style.left) * conf.viewboxX)/100;
		var finY=(parseFloat(to.style.top) * conf.viewboxY)/100;
		var dX=finX-iniX;
		var dY=finY-iniY;
		
		/*
		//method cubic start
		var curveX1=(dX/10);
		var curveX2=dX-(dX/2);
		var curveY1=(dY/2);
		var curveY2=dY-(dY/10);
		//method cubic end
		*/
		

		//method quadratic start
		var L=((dX/2) + (dY/2))/2;
		var P=Math.sqrt( (dX*dX) + (dY*dY) );
		var A=Math.atan( dY/dX );
		
		if(dX < 0 )
			A=A+Math.PI;
		
		var long=Math.atan( ((2*L)/P) );
		var part=Math.sqrt( ((P/2)*(P/2)) + (L*L) );
		var pX=(part * Math.cos( A + long )).toFixed(3);
		var pY=(part * Math.sin( A + long )).toFixed(3);
		//method quadratic end

		
		path=map.querySelector("#line_"+value);
		path.setAttribute("class","visible");
		
		//method cubic
		//path.setAttribute("d","m "+iniX+","+iniY+" c "+curveX1+","+curveY1+" "+curveX2+","+curveY2+" "+dX+","+dY);
		
		//method quadratic
		path.setAttribute("d","m "+iniX+","+iniY+" q "+pX+","+pY+" "+dX+","+dY);
		
		//rect line
		//path.setAttribute("d","m "+iniX+","+iniY+" q 0,0"+dX+","+dY);
	}
	
	/*
	*animate the plane symbol. it will move along the curve line, thanks to animateMotion element in SVG. the scale transformation is optional
	*param - the number related to the id, i.e. '1' for '#line_1'
	*/
	map.lineAnimate=function(value)
	{
		map.querySelector("#line_"+value+" + use").setAttribute("xlink:href","#airplane");
		map.querySelector("#animMotion_"+value).setAttribute("begin","1s");	
		map.querySelector("#animTransf_"+value).setAttribute("begin","1s");
	}
	
	/*
	*create the events in the zoombar
	*/
	map.initZoombar=function()
	{	
		var slideHeight=conf.slidebarHeight/conf.zoomMax; 	//(in pixels) for each step in the zoombar
		
		document.querySelector("a.minus").onclick=function()
		{
			map.setMiddle();
			map.wheel(-1);
		}
		
		document.querySelector("a.plus").onclick=function()
		{
			map.setMiddle();
			map.wheel(1);
		}
		
		/*
		*change the style.top of the slide
		*/
		map.setSlide=function(limit,value)
		{
			if(limit)
				map.slide.style.top=value+"px";
			else
				map.slide.style.top=(slideHeight*(conf.zoomMax-value))+"px";
		}
		
		/*
		*
		*/
		map.slideStep=function(event)
		{
			var pos=event.clientY-conf.slidebarPos;
			var newstep=parseInt(pos/slideHeight);
		
			if(map.getZoom()!=newstep)
			{
				map.setMiddle();
				map.setSlide(false,conf.zoomMax-newstep);
				map.setZoom(conf.zoomMax-newstep);
			}
		}
		
		var nav=document.querySelector("body > nav");

		map.slidebar=nav.querySelector("div");
		map.slide=map.slidebar.querySelector("a");
		map.slide.canDrag=false;
		map.setSlide(false,conf.zoomInit);

		map.slide.onmousedown=function(event) //not needed clickedX, slide too small?
		{
			map.slide.canDrag=true;
			return false;
		}
		
		map.slidebar.onmousemove=function(event)
		{			
			if(map.slide.canDrag)
			{
				map.slideStep(event);
			
				if(parseInt(map.slide.style.top) > conf.slidebarHeight) 	//limits
					map.setSlide(true,conf.slidebarHeight);
			}
		}
		
		map.slidebar.onclick=function(event)
		{
			map.slideStep(event);
		}
	}
	
	/*
	*create the events to navigate with the keyboard
	*/
	map.initKeyboard=function()
	{
		document.onkeydown=function(event)
		{
			if(document.activeElement.tagName=="BODY")
			{
				switch(event.keyCode)
				{
					case 37: //left
					map.style.left=(parseInt(map.style.left)+conf.keyMovement)+"px";
					break;
					
					case 38: //up
					map.style.top=(parseInt(map.style.top)+conf.keyMovement)+"px"; 	
					break;
					
					case 39: //right
					map.style.left=(parseInt(map.style.left)-conf.keyMovement)+"px";
					break;
					
					case 40: //down
					map.style.top=(parseInt(map.style.top)-conf.keyMovement)+"px";
					break;
					
					case 107: //plus
					map.setMiddle();
					map.wheel(1);
					break;
					
					case 109: //minus
					map.setMiddle();
					map.wheel(-1);
					break;
				}
				map.limits();
			}
		}
	}
	
	/*
	*create the events when click a city or a country
	*/
	map.initMapLinks=function()
	{	
		$(".cities li").prop("onclick",function()
		{
			map.selectCity(this);
		});
		
		$(".countries h4").prop("onclick",function()
		{
			map.tooltipShow("#tooltip_country",this.style.left,this.style.top);
			map.tooltipFill(this);
		});	
	}
	
	/*
	*create the events for the minimap and the miniarea
	*/
	map.initMinimap=function()
	{	
		var minimap=document.querySelector("body > footer");
		minimap.removeAttribute("class");

		var style=getComputedStyle(minimap);
		minimap.width=parseInt( style.getPropertyValue("width") );
		minimap.height=parseInt( style.getPropertyValue("height") );
		minimap.left=(map.limitX*2)- minimap.width - parseInt( style.getPropertyValue("right") );	//move to resize function?
		minimap.top=(map.limitY*2)- minimap.height - parseInt( style.getPropertyValue("bottom") );

		map.miniarea=minimap.querySelector("#miniarea");
		map.miniarea.margin=10;
		map.miniarea.canDrag=false;
		map.miniarea.className="visible";
		
		minimap.limitX=minimap.width - map.miniarea.margin;
		minimap.limitY=minimap.height - map.miniarea.margin;
		
		/*
		*emulates the click in the big map
		*/
		minimap.onclick=function(event)
		{
			if(event.target.tagName!="A") //if you aren't in the collapse link
			{
				map.cursorX=parseInt((minimap.x*100)/minimap.width);
				map.cursorY=parseInt((minimap.y*100)/minimap.height);
		
				conf.clientX=minimap.x;
				conf.clientY=minimap.y;
		
				map.automove();
				map.animate();
				map.miniarea.automove();
			}
		}
		
		minimap.onmouseup=function(event)
		{
			map.miniarea.canDrag=false;
		}
		
		minimap.onmousemove=function(event)
		{
			if(event.target.tagName!="A")
			{	
				minimap.x=(event.clientX - minimap.left); 
				minimap.y=(event.clientY - minimap.top);
	
				if(map.miniarea.canDrag)
				{
					map.miniarea.style.left=(minimap.x - map.miniarea.clickX)+"px";
					map.miniarea.style.top=(minimap.y - map.miniarea.clickY)+"px";
					map.miniarea.limits();
				}
			}
		}
		
		map.miniarea.onmousedown=function(event)
		{
			map.miniarea.canDrag=true;
			map.miniarea.clickX=event.clientX - minimap.left - parseInt(map.miniarea.style.left);
			map.miniarea.clickY=event.clientY - minimap.top - parseInt(map.miniarea.style.top)
			
			return false;				
		}
		
		map.miniarea.automove=function() //to be improved
		{
			var revZoom=conf.zoomMax - map.getZoom();
			map.miniarea.width=(2*( Math.pow(2,revZoom) ) ) + Math.pow(2,revZoom-1); //to be improved?
			map.miniarea.height=map.miniarea.width/(conf.viewboxX/conf.viewboxY);
		
			var center=map.getMiddle();
			var topLeft=parseInt( center.x - ((map.limitX*100)/map.width) )
			var topTop=parseInt( center.y - ((map.limitY*100)/map.width) );
			
			topLeft=(topLeft*minimap.width)/100;
			topTop=(topTop*minimap.height)/100;
			
			map.miniarea.style.left=topLeft+"px";
			map.miniarea.style.top=topTop+"px";
			
			map.miniarea.limits();
		}
		
		map.miniarea.limits=function()
		{
			var left=parseInt(map.miniarea.style.left);
			var top=parseInt(map.miniarea.style.top);
			var limitY=minimap.limitY - map.miniarea.height
			var limitX=minimap.limitX - map.miniarea.width;
		
			if( left > limitX )
				map.miniarea.style.left=limitX+"px";
	
			if( left < map.miniarea.margin )
				map.miniarea.style.left=map.miniarea.margin +"px";
			
			if( top > limitY )
				map.miniarea.style.top=limitY+"px";
	
			if( top < map.miniarea.margin )
				map.miniarea.style.top=map.miniarea.margin+"px";
		}
	}
	
	/*
	*create the events for the elements inside the <form> 
	*/
	map.initFormLinks=function()
	{	
		/*
		*collapse/expand for the <form> and for the <footer>
		*/
		$("form > a.ico,footer > a.ico").prop("onclick",function()
		{
			var parent=this.parentElement;
			
			if(parent.hasAttribute("class"))
				parent.removeAttribute("class");
			else
				parent.className="hide";
		});
		
		/*
		*reset button in the input field
		*/
		$("fieldset small.ico").prop("onclick",function()
		{	
			if(this.parentElement.getAttribute("name")=="departure")
				window.location.assign(window.location.href);
			else
				map.unselectCity();
		});
		
		/*
		*button for the 'select airport' popin
		*/
		$("fieldset a.ico").prop("onclick",function()
		{
			map.popinShow("#popin_airport");
		});
		
		/*
		*type the inputs and the datalist
		*/
		$("input[placeholder]").prop("oninput",function()
		{
			var start=this.value.indexOf("(");
			
			if(start>0) //if value contains a parenthesis
			{
				var abbr=this.value.substr(start+1,3);
				
				if(this.name=="from" && map.origin) //if origin is changed
				{
					map.origin.removeAttribute("class");
					map.origin=false;
					map.unselectCity();
				}

				map.selectCity( map.querySelector("li[data-name='"+abbr+"']") );
			}
		});
		
		/*
		*filter for national. highlight origin country and hide the rest
		*/
		document.querySelector("input[value='national']").onclick=function()
		{
			if(map.origin) //if you've selected some city
			{
				var country=map.getCountry(map.origin.parentElement);	
				map.querySelector("g[data-name='"+country+"']").setAttribute("class","active");
				
				$(".countries > li:not([data-name='"+country+"'])").func("setAttribute","hidden");
			}
		}
		
		/*
		*filter for international (default).reset the default state
		*/
		document.querySelector("input[value='international']").onclick=function()
		{
			var active=map.querySelector("g.active");
			
			if(active)
			{
				active.removeAttribute("class");
				$(".countries > li").func("removeAttribute","hidden");
			}
		}
	}
	
	/*
	*reading the JSON data.js, converting long/lat with mercator object, and displaying as <li> elements
	*/
	map.loadCities=function()
	{
		var countries=document.querySelector("ul.countries")
		var datalist=document.querySelector("#cities");
		var options="";
		var li="";
		var lon,lat;
		//var ul=document.querySelector("ul.cities");
		
		for(var i in data)
		{
			options+="<option value='"+data[i].name+" ("+data[i].abbr+")'/>";
			
			lon=mercator.longitudeToX(data[i].lon);
			lat=mercator.latitudeToY(data[i].lat);
			
			lon=( (lon*100)/map.width ).toFixed(3);		//convert to %
			lat=( (lat*100)/map.height ).toFixed(3);	//convert to %
		
			li='<li data-name="'+data[i].abbr+'" style="top:'+lat+'%;left:'+lon+'%"><b>'+data[i].name+'</b></li>';
			countries.querySelector("li[data-name='"+data[i].cou+"'] ul").innerHTML+=li;
		}
		
		datalist.innerHTML=options;
		//ul.innerHTML=li;
	}

	/****************************************TOOLTIPS AND POPINS****************************************/

	/*
	*display a tooltip in x,y coordenates (in %)
	*param - the id of the <aside> element. i.e. #tooltip_city or #tooltip_country
	*/
	map.tooltipShow=function(id,x,y)
	{	
		map.tooltipHide();
		map.tooltip=map.querySelector(id);	
		map.tooltip.style.left=x;
		map.tooltip.style.top=y;
		map.tooltip.className="visible";
	}
	
	map.tooltipHide=function()
	{
		if(map.tooltip)
			map.tooltip.removeAttribute("class");
	}
	
	/*
	*write specific texts into the tooltip
	*param - object which contains all the texts
	*/
	map.tooltipFill=function(obj)
	{
		if(obj)	//tooltip_country
		{
			map.tooltip.querySelector("h1 span").textContent=obj.textContent;
			map.tooltip.className+=" "+ map.getCountry(obj);	
		}
		else 	//tooltip_city
		{
			map.tooltip.querySelector("h1 b").textContent=map.destiny.textContent;
			map.tooltip.querySelector("h1 em").textContent=map.getCountry(map.destiny.parentElement);
		}
	}
	
	/*
	*display a popin
	*param - the id of the <aside> element
	*/
	map.popinShow=function(id)
	{
		map.popinHide();
		document.querySelector(id).className="visible";
	}
	
	map.popinHide=function()
	{
		var popin=document.querySelector("body > aside.visible");
		
		if(popin)
			popin.removeAttribute("class");
	}

	map.addEventListener("transitionend",function() //firefox
	{ 
		map.animateEnd();
		
	},true);
		
	map.addEventListener("webkitTransitionEnd",function() //webkit
	{
		map.animateEnd();
			
	},true);

	/****************************************DOCUMENT EVENTS****************************************/
	
	/*
	*when you release the mouse, the map cannot be dragged
	*/
	document.onmouseup=function()
	{
		map.canDrag=false;
		
		if(map.slide)
			map.slide.canDrag=false;
	}
	
	/*
	*when you move the mouse, the map is dragged, and the cursor is captured
	*/
	document.onmousemove=function(event)
	{
		conf.clientX=event.clientX;
		conf.clientY=event.clientY;
		
		if(map.canDrag)
		{
			map.style.left=(conf.clientX-map.clickX)+"px";
			map.style.top=(conf.clientY-map.clickY)+"px";	
			map.limits();
		}
		
		map.cursorX=(((conf.clientX-parseInt(map.style.left))*100)/map.width).toFixed(2);
		map.cursorY=(((conf.clientY-parseInt(map.style.top))*100)/map.height).toFixed(2);
	}
	
	document.addEventListener("DOMMouseScroll",function(event) //firefox
	{ 
		map.wheel((event.detail/3)*-1);
			
	},false);
				
	document.onmousewheel=function(event) //webkit and opera
	{
		map.wheel(event.wheelDelta/120);
	}
	
	/*
	*if initKeyboard is disabled
	*/
	document.onkeydown=function()
	{
		return false;	
	}
	
	/*
	*when resize the window, set the new limits for drag the map
	*/
	window.onresize=function()
	{	
		map.limitX=window.innerWidth/2;
		map.limitY=window.innerHeight/2;
	}

	/****************************************HTML EVENTS****************************************/
	
	/*
	*events for the 'close icon' on the tooltips
	*/
	$("figure aside header a").prop("onclick",function()
	{
		map.tooltipHide();
	});
	
	/*
	*events for the 'close icon' on the popins
	*/
	$("body > aside header a").prop("onclick",function()
	{
		map.popinHide();
	});
	
	/*
	*link in the <header>
	*/
	document.querySelector("a.print").onclick=function()
	{
		window.print();
	}
	
	/*
	*link in the <header>
	*/
	document.querySelector("a.help").onclick=function()
	{
		map.popinShow("#popin_help");
	}
	
	/*
	*link in the <header>
	*/
	document.querySelector("a.full").onclick=function()
	{
		//bug, not working in opera? safari?. use window.moveTo and window.resizeTo instead?
		
		if((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreenElement && !document.webkitFullScreenElement))
		{
			if(document.documentElement.requestFullScreen)
		  		document.documentElement.requestFullScreen();
			else if(document.documentElement.mozRequestFullScreen)
		  		document.documentElement.mozRequestFullScreen();
			else if(document.documentElement.webkitRequestFullScreen)
		  		document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	  	else
		{
			if(document.cancelFullScreen)
		  		document.cancelFullScreen();
			else if(document.mozCancelFullScreen)
		  		document.mozCancelFullScreen();
			else if (document.webkitCancelFullScreen)
		  		document.webkitCancelFullScreen();
	  	}
	}	
	
	/****************************************INITIALIZATION****************************************/

	map.origin=false;			//it will contain a <li> element
	map.originName=false;		//string for the city name, i.e. 'PAR'
	map.destiny=false;
	map.destinyName=false;	
	map.stop=false;				//it will contain a <li> element, in case of connections. to be improved
	map.tooltip=false;			//it will contain the respective <aside> element, for a city or a country
	map.clickX=false;			//(in pixels) where you have clicked in the map
	map.clickY=false;
	map.cursorX=false;		
	map.cursorY=false;		
	map.width=false;			//(in pixels) for the map, in the current zoom
	map.height=false;		
	map.miniarea=false;			//object <div> for the area, to show in the minimap
	map.slide=false;			//object <a> for the slide in the zoombar
	map.slidebar=false;			//object <div> for the slidebar in the zoombar
	map.canDrag=false;
	
	map.limitX=false;			//boundary (in pixels) for the map movement
	map.limitY=false;
	window.onresize();
	
	map.initZoombar();
	map.initKeyboard();	
	map.initFormLinks();
	//map.initMinimap();
	map.setZoom(conf.zoomInit);
	map.loadCities();
	map.initMapLinks();
	
})();