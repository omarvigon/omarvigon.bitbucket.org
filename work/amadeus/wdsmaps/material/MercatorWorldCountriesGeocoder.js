package com.amadeus.edc.maps.lib.wdsmap.mapgraphics.mercatorworldcountries
{
	import com.amadeus.edc.maps.lib.utils.MathUtils;
	import com.amadeus.edc.maps.lib.wdsmap.mapgraphics.Geocoder;
	
	public class MercatorWorldCountriesGeocoder extends Geocoder
	{		
		public function MercatorWorldCountriesGeocoder(mapWidth:Number, mapHeight:Number)
		{
			super(mapWidth, mapHeight);
			
			/* Offsets used for the map. */
			
			/** Left-most longitude on the map */
			_leftLong = -168.75;//-168.53; //-168.49;
			
			/** Right-most longitude on the map */
			_rightLong = 190.2;//190.5; //192; //190.3;
			
			/** Top-most latitude on the map */
			_topLat = 83.665;
			
			/** Bottom-most latitude on the map */
			_bottomLat = -55.6; //-55.58;
		}
		
		// FUNCTIONS ----------------------------------------------------------------------------
		
		/**
		 * Calculates the x-coordinate of the a longitude value on the map, measured in pixels.
		 * 
		 * @param long 
		 * A longitude value the x-coordinate is to be calculated for.
		 * 
		 * @return The x-coordinate of the specified longitude value on the map, measured in pixels.
		 */
		public override function longitudeToX(long:Number):Number
		{
			return (long - _leftLong) / (_rightLong - _leftLong) * this._mapWidth;
		}
		
		/**
		 * Calculates the y-coordinate of the a latitude value on the map, measured in pixels.
		 * 
		 * @param lat
		 * A latitude value the y-coordinate is to be calculated for.
		 * 
		 * @return The y-coordinate of the specified longitude value on the map, measured in pixels.
		 */
		public override function latitudeToY(lat:Number):Number
		{
			return (this.mercatorLatToY(lat) - this.mercatorLatToY(_topLat)) / (this.mercatorLatToY(_bottomLat) - this.mercatorLatToY(_topLat)) * this._mapHeight;
		}
		
		// transform lat to y
		private function mercatorLatToY(lat:Number):Number
		{
			if (lat > 89.5){
				lat = 89.5;
			}
			if (lat < -89.5){
				lat = -89.5;
			}	
			var lat_r:Number = MathUtils.degreesToRadians(lat);
			var y:Number = 0.5 * Math.log((1 + Math.sin(lat_r)) / (1 - Math.sin(lat_r)));
			var y_dg:Number = MathUtils.radiansToDegrees(y / 2);			
			return(y_dg);
		}
		
		/**
		 * Calculates the mercator longitude of a x-coordinate on the map.
		 * 
		 * @param x 
		 * The x-coordinate (pixels) the mercator longitude is to be calculated for.
		 * 
		 * @return The mercator longitude of the specified x-coordinate.
		 */
		public override function xToLongitude(x:Number):Number
		{
			return x * (_rightLong - _leftLong) / _mapWidth + _leftLong;
		}
		
		/**
		 * Calculates the mercator latitude of a y-coordinate on the map.
		 * 
		 * @param y 
		 * The y-coordinate (pixels) the mercator latitude is to be calculated for.
		 * 
		 * @return The mercator latitude of the specified y-coordinate.
		 */
		public override function yToLatitude(y:Number):Number
		{
			var lat:Number = y * (mercatorLatToY(_bottomLat) - mercatorLatToY(_topLat)) / _mapHeight + mercatorLatToY(_topLat);
			var lat_r:Number = lat * 2 * Math.PI / 180;		
			var lat_m:Number = (2 * (Math.atan(Math.exp(lat_r)))) - 0.5 * Math.PI;
			lat_m = MathUtils.radiansToDegrees(lat_m);
			return lat_m;
		}	
		
	}
}