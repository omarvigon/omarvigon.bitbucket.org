var data=[
	/*
	{"abbr":"1","lon":0,"lat":90,"name":"0,90"},
	{"abbr":"2","lon":0,"lat":45,"name":"0,45"},
	{"abbr":"3","lon":0,"lat":0,"name":"0,0"},
	{"abbr":"4","lon":0,"lat":-45,"name":"0,-45"},
	{"abbr":"5","lon":0,"lat":-90,"name":"0,-90"},
	{"abbr":"7","lon":90,"lat":0,"name":"90,0"},
	{"abbr":"8","lon":180,"lat":0,"name":"180,0"},
	{"abbr":"9","lon":-90,"lat":0,"name":"-90,0"},
	{"abbr":"10","lon":-180,"lat":0,"name":"-180,0"},
	*/
	{"abbr":"ATH","lon":23.73642,"lat":37.97615,"cou":"GR","name":"Athens"},
	{"abbr":"BCN","lon":2.16992,"lat":41.38792,"cou":"ES","name":"Barcelona"},
	{"abbr":"BER","lon":13.4114,"lat":52.52341,"cou":"DE","name":"Berlin"},
	{"abbr":"PAR","lon":2.35099,"lat":48.85667,"cou":"FR","name":"Paris"},
	{"abbr":"NCE","lon":7.26627,"lat":43.70343,"cou":"FR","name":"Nice"},
	{"abbr":"LON","lon":-0.12624,"lat":51.50015,"cou":"GB","name":"London"},
	{"abbr":"BUH","lon":26.10298,"lat":44.4342,"cou":"RO","name":"Bucarest"},
	{"abbr":"DEL","lon":77.091,"lat":28.644,"cou":"IN","name":"New Delhi"},
	{"abbr":"NYC","lon":-74.00712,"lat":40.71455,"cou":"US","name":"New York"},
	{"abbr":"BUE","lon":-58.2254,"lat":-34.3612,"cou":"AR","name":"Buenos Aires"},
	{"abbr":"WLG","lon":174.4638,"lat":-41.1720,"cou":"NZ","name":"Wellington"}
]