<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="flif">
	<h1><small class="ico"></small>FLIGHT INFORMATION</h1>
    <div>
        <ul>
            <li class="flight"><b>Flight</b> <em>AY123</em></li>
            <li class="operated"><b>Operated by <i>(with an Extralargetext to check)</i></b> <em>WDS Airways</em></li>
            <li class="aircraft"><b>Aircraft Type</b> <em>Airbus A380</em></li>        
            <li class="departure"><b>Departure</b> <em><b>London, Heathrow (LHR)</b> <i>Terminal 1</i> <?php includeTime("FRI","27","JUL","2013")?> <time>10:00</time></em></li>
            <li class="arrival"><b>Arrival</b> <em><b>Hong Kong (HKG)</b> <i>Terminal 1</i> <?php includeTime("MON","24","OCT","2013")?> <time>10:00</time></em></li>
            <li class="stopover"><b>Stopover</b> <em><b>Bangkok, Suvarnabhumi (BKK)</b></em></li>
            <li class="duration"><b>Duration</b> <em>9h05m</em></li>
            <li class="meal"><b>Meal</b> <em>Dinner</em></li>
            <li class="secured"><b>Secured flight</b> <em>This flight requires the provision of additional passenger information</em></li>
            <li class="history">
                <b>Flight history</b>
                <table>
                <thead>
                <tr>
                    <th colspan="3">Take off</th>
                    <th>Canceled</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="3">100%</td>
                    <td rowspan="3">0%</td>
                </tr>
                <tr>
                    <th>Ontime</th>
                    <th>Delay (15-30 mins)</th>
                    <th>Late (&gt;30 mins)</th>
                </tr>
                <tr>
                    <td>96%</td>
                    <td>1%</td>
                    <td>3%</td>                                        
                </tr>
                </tbody>
                </table>
            </li>
        </ul>
    </div>
</section>
<?php
include("../@includes/foot.php");
?>