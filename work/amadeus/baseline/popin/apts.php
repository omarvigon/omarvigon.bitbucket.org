<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="apts">
	<h1><small class="ico"></small>AIRPORT SELECTION</h1>
	<div>
    	<article class="continent">
        	<h3><small class="ico minus"></small><b>1. Select a continent</b></h3>
			<ul>
            	<li>Africa</li>
                <li>America</li>
                <li>Asia</li>
                <li>Australia</li>
                <li class="active">Europe</li>
            </ul>
        </article>
        <article class="country">
        	<h3><small class="ico minus"></small><b>2. Select a country</b></h3>
			<ul>
                <li>United Kingdom</li>
                <li class="active">France</li>
                <li>Spain</li>
                <li>Italy</li>
                <li>Germany</li>
                <li>Greece</li>
                <li>Austria</li>
                <li>Ireland</li>
                <li>Finland</li>
                <li>Norway</li>
            </ul>
        </article>
        <article class="city">
        	<h3><small class="ico minus"></small><b>3. Select a city &amp; airport</b></h3>
			<ul>
                <li>Paris (PAR)</li>
                <li>Marseille (MRS)</li>
                <li class="active">Nice (NCE)</li>
                <li>Toulouse (TLS)</li>
                <li>Lyon (LYS)</li>
            </ul>
        </article>
    </div>
</section>
<?php
include("../@includes/foot.php");
?>