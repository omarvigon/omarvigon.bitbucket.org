<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="babr">
	<h1><small class="ico"></small>BAGGAGE ALLOWANCE</h1>
	<div>
    	<h2><b>1A 137</b> <?php include("../@includes/module_flight_title.php")?></h2>
        <p><i>Operated by: WDS Airways</i></p>
        <table>
        <thead>
        <tr>
        	<td>Passengers</td>
            <td>Allowance Per Passenger</td>
        </tr>
        </thead>
        <tbody>
        <tr>
        	<td>Adults</td>
            <td>30kg</td>
        </tr>
        <tr>
        	<td>Children</td>
            <td>2 pieces</td>
        </tr>
        <tr>
        	<td>Infant</td>
            <td>No baggage allowed</td>
        </tr>                
        </tbody>
        </table>
    </div>
</section>
<?php
include("../@includes/foot.php");
?>