<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="wait">
	<h1>We are now processing your booking</h1>
    <h2>Please wait and do not leave this page.</h2>
    <img src="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/img/waiting.gif" alt="waiting" />
	<div>
		<h3>Did you know?</h3>
        <p>After confirmation of your booking you can see and edit your trip online.</p>
    </div>
</section>
<?php
include("../@includes/foot.php");
?>