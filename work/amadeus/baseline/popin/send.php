<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="send">
	<h1><small class="ico"></small>EMAIL A FRIEND</h1>
	<form class="send" method="post" action="#">
		<article class="warning">
            <h3><small class="ico"></small>A problem occured when sending the details of your itinerary to thomas.jones@amadeus.com</h3>
        </article>
        <?php include("../@includes/module_required.php")?>
        <fieldset name="from">
        	<h3>From</h3>
        	<p><label><em>Name <sup>*</sup></em><input type="text" name="name" /></label></p>
            <p><label><em>E-mail <sup>*</sup></em><input type="email" name="email_1" /></label></p>
        </fieldset>
        <fieldset name="to">
        	<h3>To</h3>
        	<p><label><em>Name <sup>*</sup></em><input type="text" name="name" /></label></p>
            <p><label><em>E-mail <sup>*</sup></em><input type="email" name="email_2" /></label></p>
        </fieldset>
        <label>
        	<em>Comments</em>
            <textarea>Just booked this flight to Hong Kong</textarea>
        </label>
        <?php include("../@includes/module_submit.php")?>
    </form>
</section>
<?php
include("../@includes/foot.php");
?>