<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="lang">
	<h1><small class="ico"></small>Select your location</h1>
	<div>
		<ul class="type-1">
        	<li class="en"><small class="ico"></small><b>International</b> <a href="#">English</a></li>
            <li class="fi"><small class="ico"></small><b>Finland</b> <a href="#">Suomeksi</a><a href="#">Svenska</a><a href="#">English</a></li>
            <li class="se"><small class="ico"></small><b>Sweden</b> <a href="#">Svenska</a><a href="#">English</a></li>
            <li class="au"><small class="ico"></small><b>Australia</b> <a href="#">English</a></li>
            <li class="at"><small class="ico"></small><b>Austria</b> <a href="#">Deutsch</a><a href="#">English</a></li>
            <li class="be"><small class="ico"></small><b>Belgium</b> <a href="#">English</a></li>
            <li class="ca"><small class="ico"></small><b>Canada</b> <a href="#">English</a><a href="#">Fran&ccedil;ais</a></li>
            <li class="cn"><small class="ico"></small><b>China</b> <a href="#">&#31616;&#20307;&#20013;&#25991;</a><a href="#">English</a></li>
            <li class="hr"><small class="ico"></small><b>Croatia</b> <a href="#">English</a></li>
            <li class="cz"><small class="ico"></small><b>Czech Republic</b> <a href="#">English</a></li>
        </ul>
        <ul class="type-2">
        	<li class="dk"><small class="ico"></small><b>Denmark</b> <a href="#">Dansk</a><a href="#">English</a></li>
            <li class="ee"><small class="ico"></small><b>Estonia</b> <a href="#">English</a><a href="#">&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;</a><a href="#">Estonian</a></li>
            <li class="fr"><small class="ico"></small><b>France</b> <a href="#">Fran&ccedil;ais</a><a href="#">English</a></li>
            <li class="de"><small class="ico"></small><b>Germany</b> <a href="#">Deutsch</a><a href="#">English</a></li>
            <li class="hk"><small class="ico"></small><b>Hong-Kong</b> <a href="#">&#31616;&#20307;&#20013;&#25991;</a><a href="#">English</a></li>
            <li class="hu"><small class="ico"></small><b>Hungary</b> <a href="#">English</a></li>
            <li class="in"><small class="ico"></small><b>India</b> <a href="#">English</a></li>
            <li class="it"><small class="ico"></small><b>Italy</b> <a href="#">Italiano</a><a href="#">English</a></li>
            <li class="jp"><small class="ico"></small><b>Japan</b> <a href="#">&#26085;&#26412;&#35486;</a><a href="#">English</a></li>
            <li class="kp"><small class="ico"></small><b>Korea</b> <a href="#">&#54620;&#44397;&#50612;</a><a href="#">English</a></li>
        </ul>
        <ul class="type-3">
        	<li class="lv"><small class="ico"></small><b>Latvia</b> <a href="#">English</a></li>
            <li class="lt"><small class="ico"></small><b>Lithuania</b> <a href="#">English</a></li>
            <li class="nl"><small class="ico"></small><b>The Netherlands</b> <a href="#">English</a></li>
            <li class="no"><small class="ico"></small><b>Norway</b> <a href="#">Norsk</a><a href="#">English</a></li>
            <li class="pl"><small class="ico"></small><b>Poland</b> <a href="#">Polski</a><a href="#">English</a></li>
            <li class="pt"><small class="ico"></small><b>Portugal</b> <a href="#">English</a></li>
            <li class="ru"><small class="ico"></small><b>Russia</b> <a href="#">&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;</a><a href="#">English</a></li>
            <li class="sg"><small class="ico"></small><b>Singapore</b> <a href="#">English</a></li>
            <li class="si"><small class="ico"></small><b>Slovenia</b> <a href="#">English</a></li>
            <li class="es"><small class="ico"></small><b>Spain</b> <a href="#">Espa&ntilde;ol</a><a href="#">English</a></li>
        </ul>
        <ul class="type-4">
        	<li class="ch"><small class="ico"></small><b>Switzerland</b> <a href="#">Deutsch</a><a href="#">Fran&ccedil;ais</a><a href="#">English</a></li>
            <li class="th"><small class="ico"></small><b>Thailand</b> <a href="#">English</a></li>
            <li class="us"><small class="ico"></small><b>United States</b> <a href="#">English</a></li>
            <li class="uk"><small class="ico"></small><b>United Kingdom</b> <a href="#">English</a></li>
        </ul>      
    </div>
</section>
<?php
include("../@includes/foot.php");
?>