<?php
include("../@includes/head.php");
includeBody("column1");
?>
<section class="seats">
	<h1><small class="ico"></small>SEAT RESERVATION</h1>
    <h2><b>OUTBOUND</b> <em><?php include("../@includes/module_flight_title.php")?></em> <?php includeTime("FRI","27","JUL","2013")?></h2>
	<form class="seats" method="post" action="#">
    	<div>
            <fieldset name="passengers">
                <h3>Passengers</h3>
                <p><label><input type="radio" name="radio_1" /><small class="ico">1</small><em>John Smith</em></label></p>
                <p><label><input type="radio" name="radio_1" /><small class="ico">2</small><em>Joan Smith</em></label></p>
                <p><label><input type="radio" name="radio_1" /><small class="ico">3</small><em>Junior Smith</em></label></p>
            </fieldset>
            <section>
                <h3>Aircraft Navigation</h3>
                <h4><a href="#">DECK 1</a><a class="active" href="#">DECK 2</a></h4>
                <div>
                    <table class="seats">
                    <thead>
                    <tr>
                    	<td class="limit">&nbsp;</td>
                        <td>A</td>
                        <td>B</td>
                        <td>C</td>
                        <td class="empty">&nbsp;</td>
                        <td>D</td>
                        <td>E</td>
                        <td>F</td>
                        <td>G</td>
                        <td>H</td>
                        <td class="empty">&nbsp;</td>
                        <td>I</td>
                        <td>J</td>
                        <td>K</td>
                        <td class="limit">&nbsp;</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for($i=2;$i<=25;$i++) {?>
                    <tr>
                    	<td class="limit"><small class="ico"></small></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="active"><small class="ico">1</small></td>
                        <td class="no"><?php echo $i?></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="occupied"><small class="ico"></small></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="no"><?php echo $i?></td>
                        <td class="toilet-shared"><small class="ico"></small></td>
                        <td class="vacant"><small class="ico"></small></td>
                        <td class="gallery"><small class="ico"></small></td>
                        <td class="limit"><small class="ico"></small></td>              
                    </tr>
                    <?php }?>
                    </tbody>
                    </table>
                </div>
            </section>
            <figure>
                <h3><b>Aircraft:</b> <em>A321</em></h3>
                <small class="ico aircraft"></small>
            </figure>
        </div>
        <?php
        include("../@includes/booking_legend.php");
        include("../@includes/booking_continue.php");
		?>
    </form>
</section>
<?php
include("../@includes/foot.php");
?>