$(document).ready(function(){
	
	var form_search=$("form.search");
	var form_labels=form_search.find("header label");
	form_labels.click(function()
	{
		form_labels.toggleClass("active");
		form_search.find(".return").toggleClass("none");
		return false;
	});

	$(".offermain footer a:first-child").click(function()
	{
		$(this).parentsUntil("article").next().toggleClass("none");
		return false;
	});
});