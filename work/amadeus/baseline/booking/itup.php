<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="ocup" method="post" action="#">
        	<h1>Step 2 - Select Flights</h1>
            <h2>Please select a price and your flight.</h2>
            
            <section class="itup">
            	<h3><small class="ico"></small>Select a price</h3>
                <div>
                    <table>
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <?php include("../@includes/booking_tablebound_family.php")?>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Lowest price</th>
                        <td><label><input type="radio" name="radio_1" /><strong>200&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>300&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>400&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>500&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>590&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>900&euro;</strong></label></td>
                    </tr>
                    <tr>
                        <th>Other prices</th>
                        <td><label><input type="radio" name="radio_1" /><strong>250&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>350&euro;</strong></label></td>
                        <td class="inactive">&nbsp;</td>
                        <td class="inactive">&nbsp;</td>
                        <td><label><input type="radio" name="radio_2" /><strong>700-750&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_2" /><strong>1000</strong></label></td>
                    </tr>                                                           
                    </tbody>
                    </table>
                    <p>Prices per adult and include taxes and servicing fees.</p>
                </div>
            </section>
		<?php for($i=0;$i<2;$i++) {?>
            <section class="bound <?php echo ($i==0)? "out":"in"?>">
            	<?php include("../@includes/booking_ocup_title.php")?>
                <div>
                    <table class="bound">
                    <thead>
                    <tr>
                        <td>&nbsp;</td>
                        <?php include("../@includes/booking_tablebound_head.php")?>
                    </tr>
                    </thead>
                    <tbody>
                <?php for($j=0;$j<4;$j++) {?>
                    <tr>
                        <td><label><input type="radio" name="radio_3" /></label></td>
                        <?php include("../@includes/booking_tablebound_body.php")?>
                    </tr>
                <?php }?>      
                    </tbody>
                    </table>
                </div>
            </section>
		<?php }?> 
            <?php include("../@includes/booking_continue.php")?>   
        </form>
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>