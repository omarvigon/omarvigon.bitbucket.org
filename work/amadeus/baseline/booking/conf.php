<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <section class="purc">
        	<h1>Step 5 - Confirmation</h1>
            <h2>We recommend that you save or print this page for reference.</h2>
            <ul>
            	<li class="print"><a href="#"><small class="ico"></small> Print</a></li>
                <li class="share">
                	<a href="#"><small class="ico"></small> Share this trip</a>
                	<?php include("../@includes/booking_social.php")?>
                </li>
            </ul>
            <section class="complete">
            	<h3><small class="ico"></small>Booking Complete</h3>
                <div>
                	<section>
                		<h4>Your booking is confirmed!</h4>
                   	 	<small class="ico"></small>
                    	<h5>Booking reference <b>GDT1523</b></h5>                    
                    	<p>The booking reference number is needed whenever in contact with WDS Airline</p>
                    	<p><i>We have send a confirmation message to passenger@email.com</i></p>
					</section>                        
                </div>
            </section>
            <?php
            include("../@includes/booking_passengers.php");
			include("../@includes/booking_itinerary.php");
			?>
            <section class="insurance">
            	<h3><small class="ico"></small>Travel Insurance</h3>
                <div>
                	<h4>Cancellation insurance</h4>
                    <small class="ico"></small>
                    <p>Full reimbursement In case of flight cancellation</p>
                    <p><b>Insurance reference</b> <em>0123456789</em></p>
                    <p><a href="#">Terms and conditions</a></p>
                    <footer>
                    	<p><b>Total price for all travellers</b> <strong>=60&euro;</strong></p>
                    </footer>
                </div>
            </section>
            <section class="paydetails">
            	<h3><small class="ico"></small>Payment Details</h3>
                <div>
                	<?php include("../@includes/booking_price.php")?>  
                    <section class="summary">
                    	<h3>Payment By Credit Card</h3>
                        <ul>
                        	<li><b>Card holder:</b> <em>John Smith</em></li>
                            <li><b>Card type:</b> <em>VISA</em></li>
                            <li><b>Card number:</b> <em>xxxx xxxx xxxx 1234</em></li>
                        </ul>
                    </section>
                </div>
            </section>
			<?php include("../@includes/booking_continue.php")?>
        </section>
	</div>
    <aside class="main">
		<?php include("../@includes/module_placeholder.php")?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>