<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="alpi" method="post" action="#">
        	<h1>Step 3 - Passenger Details</h1>
            <h2>Please review the fare and enter the passenger details.</h2>
            <p>Please make sure your name is entered exactly as it appears on your government issued identification</p>
            <?php include("../@includes/module_required.php")?>
 			
		<?php for($i=1;$i<3;$i++) {?>
            <fieldset class="passenger" name="adult_<?php echo $i?>">
                <h3><small class="ico"></small> Adult <?php echo $i?></h3>
                <?php include("../@includes/booking_form_alpi.php")?>

                <p><label><em>Frequent flyer program</em><select name="ff_program"><option>Airways Program 1</option></select></label></p>
                <p><label><em>Frequent flyer number <sup>*</sup></em><input type="text" name="ff_number" /></label></p>
                <p><label><em>Meal preference</em><select name="meal"><option>No meal preference</option></select></label></p>
                
                <fieldset class="contact" name="contact">
                    <h4>Contact Information</h4>
                    <div>
                        <aside>
                            <p><em>E-Mail <sup>*</sup></em><input type="email" name="email_1" /></p>
                            <p><em>Verify E-Mail <sup>*</sup></em><input type="email" name="email_2" /></p>
                        </aside>
                        <p><em>Phone 1 <sup>*</sup></em><select name="type"><option>Home</option></select><select name="country"><option>Select Country</option></select><input type="tel" name="phone_1" /></p>
                        <p><em>Phone 2</em><select name="type"><option>Mobile</option></select><select name="country"><option>Select Country</option></select><input type="tel" name="phone_2" /></p>
					</div>                        
                </fieldset>
                
                <fieldset class="passenger" name="infant_1">
                    <h3>Infant 1 <i>(traveling with Adult 1)</i></h3>
                    <?php include("../@includes/booking_form_alpi.php")?>
                </fieldset>
            </fieldset>
		<?php }?>          
            
            <p><label><input type="checkbox" />Save profile information for the next time.</label></p>
            <p><b>Note :</b> special requests can be added once booking is completed.</p>
            <?php include("../@includes/booking_continue.php")?>
        </form>                    
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>