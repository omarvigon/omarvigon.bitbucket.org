<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="itcl" method="post" action="#">
        	<h1>Step 2 - Select Dates</h1>
            <h2>Please select which date you wish to travel.</h2>
            
            <section>
            	<?php include("../@includes/booking_ocup_title.php")?>
                <div>
                    <table class="calendar it">
                    <caption>INBOUND</caption>
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <td>&nbsp;</td>
                        <td><?php includeTime("FRI","27","JUL","")?></td>
                        <td><?php includeTime("SAT","28","JUL","")?></td>
                        <td><?php includeTime("SUN","29","JUL","")?></td>
                        <td><?php includeTime("MON","30","JUL","")?></td>
                        <td><?php includeTime("TUE","31","JUL","")?></td>
                        <td><?php includeTime("WED","1","AUG","")?></td>
                        <td><?php includeTime("THU","2","AUG","")?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th rowspan="6"><b>OUTBOUND</b></th>
                        <th><?php includeTime("MON","24","OCT","")?></th>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>550.00&euro;</strong></label></td>
                        <td class="lowest"><small class="ico"></small><label><input type="radio" name="radio_1" /><strong>500.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>                    
                    </tr>
                    <tr>
                        <th><?php includeTime("TUE","25","OCT","")?></th>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>600.00&euro;</strong></label></td>
                        <td class="lowest"><small class="ico"></small><label><input type="radio" name="radio_1" /><strong>500.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>650.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>650.00&euro;</strong></label></td>                    
                    </tr>
                    <tr>
                        <th><?php includeTime("WED","26","OCT","")?></th>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td class="lowest"><small class="ico"></small><label><input type="radio" name="radio_1" /><strong>500.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>                    
                    </tr>
                    <tr>
                        <th><?php includeTime("THU","27","OCT","")?></th>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>850.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>850.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>800.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>800.00&euro;</strong></label></td>                    
                    </tr>
                    <tr>
                        <th><?php includeTime("FRI","28","OCT","")?></th>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>750.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>550.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>800.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>800.00&euro;</strong></label></td>
                        <td><label><input type="radio" name="radio_1" /><strong>800.00&euro;</strong></label></td>                    
                    </tr>
                    <tr>
                        <th><?php includeTime("SAT","29","OCT","")?></th>
                        <td class="disabled">&nbsp;</td>
                        <td class="inactive"><i>N/A</i></td>
                        <td class="inactive"><i>N/A</i></td>
                        <td class="inactive"><i>N/A</i></td>
                        <td class="inactive"><i>N/A</i></td>
                        <td class="inactive"><i>N/A</i></td>
                        <td class="inactive"><i>N/A</i></td>                  
                    </tr>                                                                          
                    </tbody>
                    </table>
                    <?php include("../@includes/booking_legend.php")?>
				</div>
			</section>
            <?php include("../@includes/booking_continue.php")?>                                  
        </form>
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>