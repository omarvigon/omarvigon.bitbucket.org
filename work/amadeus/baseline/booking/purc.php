<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="purc" method="post" action="#">
        	<h1>Step 4 - Purchase</h1>
            <h2>Please enter the payment details. You can optionally select one of our insurance products.</h2>
			<?php include("../@includes/booking_passengers.php")?>
            <section class="seat">
            	<h3><small class="ico"></small>Seat Reservation</h3>
                <div>
                    <p>We offer seat reservation for certain flights in your itinerary free of charge. If you would like to reserve a seat, please make your selection below.</p>
                    <article class="active">
                        <header>
            	            <h4>Outbound</h4>
        	                <h5><?php include("../@includes/module_flight_title.php")?></h5>
    	                    <?php includeTime("FRI","27","JUL","2013")?>
	                        <aside><a href="#">Change Seats</a></aside>
                        </header>
                        <div>
                        	<small class="ico valid"></small>
                            <ul>
                            	<li>John Smith: 5F</li>
                                <li>Joan Smith: 5G</li>
                                <li>Junior Smith: 5H</li>
                            </ul>
                        </div>
                    </article>
                    <article>
                    	<header>
	                        <h4>Inbound  <i>(with an Extralargetext)</i></h4>
    	                    <h5><i>From</i> <b>Hong Kong (HKG)</b> <i>To</i> <b>Bangkok, Suvarnabhumi (BKK)</b></h5>
        	                <?php includeTime("FRI","27","JUL","2013")?>
                            <aside>Seat Map Not Available</aside>
						</header>                            
                    </article>
                    <article>
                    	<header>
	                        <h4>Inbound</h4>
                            <h5><i>From</i> <b>Bangkok, Suvarnabhumi (BKK)</b> <i>To</i> <b>London, Heathrow (LHR)</b></h5>
        	                <?php includeTime("MON","24","OCT","2013")?>
                            <aside><a href="#">Select</a></aside>
						</header>
                    </article>
                </div>
            </section>
            <section class="insurance">
            	<h3><small class="ico"></small>Travel Insurance</h3>
                <div>
                	<p>Travel worry-free by choosing one of our insurance products:</p>
                    <table>
                    <thead>
                    <tr>
                    	<td>Insurance</td>
                        <td>Total Price</td>
                        <td>&nbsp;</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    	<td><label><input type="radio" name="radio_1" checked="checked" /> Cancellation Insurance</label><p>Full reimbursement In case of flight cancellation <a href="#">More</a></p></td>
                        <td><strong>15.00&euro;</strong></td>
                        <td colspan="4"><small class="ico insu"></small></td>
                    </tr>
                    <tr>
                    	<td><label><input type="radio" name="radio_1" /> Luggage Insurance</label><p>Luggage and Personal Possessions up to 1.250&euro; <a href="#">More</a></p></td>
                        <td><strong>20.50&euro;</strong></td>
                    </tr>
                    <tr>
                    	<td><label><input type="radio" name="radio_1" /> Comprehensive Travel Insurance</label><p>Repatriation and Overseas Medical expenses up to 150.000&euro; <a href="#">More</a></p></td>
                        <td><strong>27.00&euro;</strong></td>
                    </tr>
                    <tr>
                    	<td><label><input type="radio" name="radio_1" /> No Insurance</label></td>
                        <td>&nbsp;</td>
                    </tr>                                                           
                    </tbody>
                    </table>
                    <p><label><input type="checkbox" /> I accept the insurance <a href="#">terms and conditions</a></label></p>
                </div>
            </section>
            <section class="payment">
            	<h3><small class="ico"></small>Payment</h3>
                <div>
                	<header>
	                    <label class="type-1 active"><input type="radio" name="radio_2"><small class="ico"></small><em>Visa</em></label>
						<label class="type-2"><input type="radio" name="radio_2"><small class="ico"></small><em>Mastercard</em></label>
                        <label class="type-3"><input type="radio" name="radio_2"><small class="ico"></small><em>American Express</em></label>
                        <label class="type-4"><input type="radio" name="radio_2"><small class="ico"></small><em>Diners Club</em></label>                        
                    </header>
                    <fieldset>
                    	<small class="ico valid"></small>
                    	<p><label><em>Holder last name <sup>*</sup></em><input type="text" name="name" /></label></p>
                        <p><em>Card Number <sup>*</sup></em><input type="text" name="card_1" maxlength="4" /> <input type="text" name="card_2" maxlength="4" /> <input type="text" name="card_3" maxlength="4" /> <input type="text" name="card_4" maxlength="4" /></p>
                        <p><em>Expiry date <sup>*</sup></em><select name="month"><option value="01">01</option></select> <select name="year"><option value="2012">2012</option></select></p>
                        <p class="security"><label><em>Security code <sup>*</sup></em><input type="text" name="security_code" maxlength="4" /></label> <i>Your credit card security number is required. It is composed of 3 numbers that appear on the back of your credit card</i></p>
                        <p class="error"><label><em>Address line 1 <sup>*</sup></em><input type="text" name="address_1" /></label> <small class="ico error"></small></p>
                        <p><label><em>Address line 2</em><input type="text" name="address_2" /></label></p>
                        <p><label><em>Zip Code <sup>*</sup></em><input type="text" name="zipcode" /></label></p>
                        <p><label><em>City <sup>*</sup></em><input type="text" name="city" /></label></p>
                        <p><label><em>Country <sup>*</sup></em><select name="country"><option value="fr">France</option></select></label></p>
                    </fieldset>
                    <footer>
                    	<p><b>Note:</b> Your card will be debited on confirmation</p>
                    </footer>
                </div>
            </section>
            <?php include("../@includes/booking_continue.php")?>
        </form>
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>