<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="occl" method="post" action="#">
        	<h1>Step 2 - Select Dates</h1>
            <h2>Please select which date you wish to travel.</h2>
            
		<?php for($i=1;$i<3;$i++) {?>
            <section class="calendar <?php echo ($i==1)? "out":"in"?>">
            	<?php include("../@includes/booking_ocup_title.php")?>
                <div>
					<?php include("../@includes/module_prev_next.php")?>
                    <table class="calendar">
                    <thead>
                    <tr>
                        <td>SUNDAY</td>
                        <td>MONDAY</td>
                        <td>TUESDAY</td>
                        <td>WEDNESDAY</td>
                        <td>THURSDAY</td>
                        <td>FRIDAY</td>
                        <td>SATURDAY</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="disabled">&nbsp;</td>
                        <td class="disabled">&nbsp;</td>
                        <td class="disabled">&nbsp;</td>
                        <td><?php includeTime("","27","JUL","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","28","JUL","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","29","JUL","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","30","JUL","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                    </tr>
                    <tr>
                        <td class="inactive"><?php includeTime("","31","JUL","")?><i>N/A</i></td>
                        <td class="inactive"><?php includeTime("","1","AUG","")?><i>N/A</i></td>
                        <td><?php includeTime("","2","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","3","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td class="active"><?php includeTime("","4","AUG","")?><small class="ico"></small><label><input type="radio" name="radio_<?php echo $i?>" /><strong>500.00&euro;</strong></label></td>
                        <td class="lowest"><?php includeTime("","5","AUG","")?><small class="ico"></small><label><input type="radio" name="radio_<?php echo $i?>" /><strong>500.00&euro;</strong></label></td>
                        <td><?php includeTime("","6","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>                    
                    </tr>
                    <tr>
                        <td><?php includeTime("","7","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","8","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","9","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td><?php includeTime("","10","AUG","")?><label><input type="radio" name="radio_<?php echo $i?>" /><strong>750.00&euro;</strong></label></td>
                        <td class="disabled">&nbsp;</td>
                        <td class="disabled">&nbsp;</td>
                        <td class="disabled">&nbsp;</td>
                    </tr>                                
                    </tbody>
                    </table>
                    <?php include("../@includes/booking_legend.php")?>
                </div>
            </section>
		<?php }?>

            <?php include("../@includes/booking_continue.php")?>            
        </form>
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>