<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="alpi" method="post" action="#">
        	<h1>Step 3 - Additional Passenger Details</h1>
            <h2>The route you have chosen requires additional passenger information.</h2>
            <p>Please enter the additional information.</p>
            <?php include("../@includes/module_required.php")?>
            
		<?php for($i=1;$i<4;$i++) {?>
            <fieldset class="passenger" name="adult_<?php echo $i?>">
                <h3><small class="ico"></small> Adult <?php echo $i?></h3>
				<div>
                    <p><label><em>Gender <sup>*</sup></em><select name="gender"><option>Select</option></select></label></p>
                    <p><label><em>First Name</em><input type="text" name="name_1" /></label></p>
                    <p><label><em>Middle Name</em><input type="text" name="name_3" /></label></p>
                    <p><label><em>Last Name</em><input type="text" name="name_2" /></label></p>
                    <p><em>Date of Birth</em><input type="text" value="DD" maxlength="2" /> <input type="text" value="MM" maxlength="2" /> <input type="text" value="YYYY" maxlength="4" /></p>
                    <p><label><em>Doctument Type</em><select name="doctype"><option>Select country of residence</option></select></label></p>
                    <p><label><em>Document Number</em><input type="text" name="docnumber" /></label></p>
                    <p><label><em>Nationality</em><select name="nationality"><option>Select</option></select></label></p>
                    <p><label><em>Country of residence</em><select name="country"><option>Select country</option></select></label></p>
                    <p><label><input type="checkbox" name="country_same" />Use the same country of residence for all passengers</label></p>
                    
                    <fieldset class="address" name="address">
                        <h4>Address of the first night spent in the U.S.A.</h4>
                        <div>
							<p><label><em>Street number and name</em><input type="text" name="street" /></label></p>
                    	    <p><label><em>City</em><input type="text" name="city" /></label></p>
                        	<p><label><em>State</em><select name="doctype"><option>Select state</option></select></label></p>
	                        <p><label><em>Zip Code</em><input type="text" name="name_1" /></label></p>
							<p><label><input type="checkbox" />Use the same destination information for all passengers</label></p>
						</div>
                    </fieldset>
                    
                    <fieldset class="other" name="other">
                        <h4>Other information</h4>
                        <div>
        	                <p><label><em>Known Traveler number</em><input type="text" name="city" /></label></p>
    	                    <p><label><em>Redress number</em><input type="text" name="city" /></label></p>
	                        <a href="#"><small class="ico"></small></a>
                        </div>
                    </fieldset>
				</div>                    
            </fieldset>
		<?php }?>
        	<?php include("../@includes/booking_continue.php")?>
        </form>
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>