<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <section class="srch">
        	<h3>Flights</h3>
            <div>
				<?php
                include("../@includes/module_search_header.php");
				include("../@includes/module_search.php");
				?>
            </div>
        </section>
	</div>
    <aside class="main">
		<?php include("../@includes/module_placeholder.php")?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>