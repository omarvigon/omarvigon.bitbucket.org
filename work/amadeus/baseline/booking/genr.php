<?php
include("../@includes/head.php");
includeBody("column1");
include("../@includes/booking_header.php");
?>
	<section class="genr">
    	<article class="error">
        	<h3><small class="ico"></small>Please correct the following fields:</h3>
            <ul>
	            <li>Please select a <b>departure airport</b></li>
                <li>Invalid <b>destination airport</b></li>
                <li>Please select the <b>return date</b></li>
            </ul>
        </article>
    	<article class="error">
        	<h3><small class="ico"></small>We currently have no response for your request. Please try again later.(4616)</h3>
		</article>            
    	<?php include("../@includes/booking_continue.php")?>
    </section>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>