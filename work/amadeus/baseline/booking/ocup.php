<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <form class="ocup" method="post" action="#">
        	<h1>Step 2 - Select Flights</h1>
            <h2>Please select your flight and fare combination.</h2>
            
		<?php for($i=1;$i<3;$i++) {?>
            <section class="bound <?php echo ($i==1)? "out":"in"?>">
            	<?php include("../@includes/booking_ocup_title.php")?>
                <div>
					<?php include("../@includes/module_prev_next.php")?>
                    
                    <ol class="dates">
                        <li><?php includeTime("FRI","27","JUL","")?> <i>From <strong>750&euro;</strong></i></li>
                        <li><?php includeTime("SAT","28","JUL","")?> <i>From <strong>750&euro;</strong></i></li>
                        <li class="active"><?php includeTime("SUN","29","JUL","")?> <i>From <strong>650&euro;</strong></i></li>
                        <li><?php includeTime("MON","30","JUL","")?> <i>From <strong>650&euro;</strong></i></li>
                        <li class="lowest"><?php includeTime("TUE","31","JUL","")?> <i>From <strong>350&euro;</strong><small class="ico"></small></i></li>
                        <li><?php includeTime("WED","1","AUG","")?> <i>From <strong>550&euro;</strong></i></li>
                        <li><?php includeTime("THU","2","AUG","")?> <i>N/A</i></li>
                    </ol>
                    
                    <fieldset class="filter">
                        <h3><small class="ico minus"></small>Filter Flights</h3>
                        <h4>Filter results by departure/arrival times and stops</h4>
                        <div>
                            <fieldset class="type-1">
                                <h3>Stops</h3>
                                <label><input type="radio" name="radio_0" /> All flights</label>
                                <label><input type="radio" name="radio_0" /> Direct flights</label>
                            </fieldset>
                            <fieldset class="type-2">
                                <h3>Time</h3>
                                <p>Departure: between <time>06:00</time> and <time>18:00</time></p>
                                <input type="range" min="0" max="200" value="50" step="5" />
                                <p>Arrival: between <time>11:00</time> and <time>23:00</time></p>
                                <input type="range" min="0" max="200" value="50" step="5" />
                            </fieldset>
                            <footer>
                                <output>Showing <b>5</b> of <b>5</b> flights</output>
                                <button type="submit">Submit</button>
                            </footer>
                        </div>
                    </fieldset>
                    
                    <table class="bound">
                    <thead>
                    <tr>
                        <?php
                        include("../@includes/booking_tablebound_head.php");
						include("../@includes/booking_tablebound_family.php");
						?>
                    </tr>
                    </thead>
                    <tbody>
                <?php for($j=1;$j<5;$j++) {?>
                    <tr>
                        <?php include("../@includes/booking_tablebound_body.php")?>
                        <td class="family lowest"><small class="ico"></small><label><input type="radio" name="radio_<?php echo $j?>" /><strong>250.00&euro;</strong></label></td>
                        <td class="family"><label><input type="radio" name="radio_<?php echo $j?>" /><strong>350.00&euro;</strong></label></td>
                        <td class="family lastseat"><small class="ico">1</small><label><input type="radio" name="radio_<?php echo $j?>" /><strong>450.00&euro;</strong></label></td>
                        <?php if($option=="family6"){?>
                        <td class="family"><label><input type="radio" name="radio_<?php echo $j?>" /><strong>550.00&euro;</strong></label></td>
                        <td class="family"><label><input type="radio" name="radio_<?php echo $j?>" /><strong>700.00&euro;</strong></label></td>
                        <td class="family"><label><input type="radio" name="radio_<?php echo $j?>" /><strong>900.00&euro;</strong></label></td>
                        <?php }?>
                    </tr>
                <?php }?>
                	<tr class="details type-1">
                    	<td colspan="2" rowspan="4">&nbsp;</td>
                        <td colspan="3"><small class="ico"></small><b>Fidelilade points</b></td>
                        <td><span>50% miles</span></td>
                        <td><span>60% miles</span></td>
                        <td><span>80% miles</span></td>
                        <?php if($option=="family6"){?>
                        <td><span>90% miles</span></td>
                        <td><span>100% miles</span></td>
                        <td><span>120% miles</span></td>
                        <?php }?>
                    </tr>
                    <tr class="details type-2">
                        <td colspan="3"><small class="ico"></small><b>Rebooking</b></td>
                        <td><span>No</span></td>
                        <td><span>No</span></td>
                        <td><small class="ico"></small><i>From 30&euro;</i></td>
                        <?php if($option=="family6"){?>
                        <td><small class="ico valid"></small><i>From 20&euro;</i></td>
                        <td><small class="ico valid"></small><i>From 10&euro;</i></td>
                        <td><small class="ico valid"></small><i>From 5&euro;</i></td>
                        <?php }?>
                    </tr>
                    <tr class="details type-3">
                        <td colspan="3"><small class="ico"></small><b>Baggage alowance</b></td>
                        <td><span>20kg</span></td>
                        <td><span>30kg</span></td>
                        <td><span>40kg</span></td>
                        <?php if($option=="family6"){?>
                        <td><span>45kg</span></td>
                        <td><span>50kg</span></td>
                        <td><span>55kg</span></td>
                        <?php }?>
                    </tr>
                    <tr class="details type-4">
                        <td colspan="3"><small class="ico"></small><b>Priority services</b></td>
                        <td><span>No</span></td>
                        <td><span>No</span></td>
                        <td><span>Check-in, boarding</span></td>
                        <?php if($option=="family6"){?>
                        <td><span>Check-in, boarding, baggage</span></td>
                        <td><span>Check-in, boarding, baggage</span></td>
                        <td><span>Check-in, boarding, baggage</span></td>
                        <?php }?>
                    </tr>
                  	<tr class="details upgrade">
                    	<td colspan="11"><span>Did you know that for 150$ more, you could upgrade to FLX Fare</span><a href="#">Upgrade</a></td>
                    </tr>
                    <tr class="details hide">
                    	<td colspan="11"><a href="#"><small class="ico minus"></small>Hide</a></td>
                    </tr>
                    </tbody>
                    </table>                
                    <?php include("../@includes/booking_legend.php")?>
                </div>
            </section>
		<?php }?>
            
            <?php include("../@includes/booking_continue.php")?>
        </form>
	</div>
    <aside class="main">
    	<?php
        include("../@includes/booking_itinerary.php");
		include("../@includes/booking_price.php");
		include("../@includes/module_submit.php");
		?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>