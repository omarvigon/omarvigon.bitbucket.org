<?php
include("../@includes/head.php");
includeBody("column2r");
include("../@includes/booking_header.php");
?>
	<div class="main">
        <section class="fare">
        	<h1>Step 3 - Fare</h1>
            <h2>Please review the fare.</h2>
            <?php
			include("../@includes/booking_itinerary.php");
			include("../@includes/booking_price.php");
			include("../@includes/booking_continue.php");
			?>
        </section>
	</div>
    <aside class="main">
		<?php include("../@includes/module_placeholder.php")?>
    </aside>
<?php
include("../@includes/booking_footer.php");
include("../@includes/foot.php");
?>