<div id="mockup-panel" style="position:fixed;top:5px;right:5px;padding:10px;background:#eee;border-radius:5px;box-shadow:0 3px 3px #666">
    <nav id="mockup-skin">
    <?php if($folder=="booking"){?>
    <h3 style="font-weight:bold">User</h3>
    <a href="<?php echo $_SERVER['PHP_SELF']?>?option=logged">Logged in</a><br />
    <a href="<?php echo $_SERVER['PHP_SELF']?>">Logged out</a>
    <?php }?>
    <?php if($page=="ocup"){?>
    <h3 style="font-weight:bold">OCUP</h3>
    <a href="<?php echo $_SERVER['PHP_SELF']?>">3 Fare Families</a><br />
    <a href="<?php echo $_SERVER['PHP_SELF']?>?option=family6">6 Fare Families</a><br />
	<?php }?>
    <h3 style="font-weight:bold">Skin</h3>
    <form method="get" action="<?php echo $_SERVER['PHP_SELF']?>">
    <select name="skin"><option value="baseline">baseline</option><option value="hainan">hainan</option><option value="usair">usair</option><option value="southafrican">southafrican</option></select>
    </form>
    </nav>
    <script>
	$("select[name='skin']").change(function()
	{
		$(this).parent().submit();
	});
	$("select[name='skin']").val("<?php echo $skin?>");
    </script>
</div>

<div class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" id="ui-datepicker-div" style="position:fixed;width:200px;bottom:10px;right:5px">
    <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
        <a title="Prev" data-event="click" data-handler="prev" class="ui-datepicker-prev ui-corner-all"><span class="ui-icon ui-icon-circle-triangle-w">Prev</span></a>
        <a title="Next" data-event="click" data-handler="next" class="ui-datepicker-next ui-corner-all"><span class="ui-icon ui-icon-circle-triangle-e">Next</span></a>
    <div class="ui-datepicker-title">
    	<span class="ui-datepicker-month">October</span>&nbsp;<span class="ui-datepicker-year">2012</span></div>
    </div>
    <table class="ui-datepicker-calendar">
    <thead>
    <tr><th class="ui-datepicker-week-end"><span title="Sunday">Su</span></th><th><span title="Monday">Mo</span></th><th><span title="Tuesday">Tu</span></th><th><span title="Wednesday">We</span></th><th><span title="Thursday">Th</span></th><th><span title="Friday">Fr</span></th><th class="ui-datepicker-week-end"><span title="Saturday">Sa</span></th></tr>
    </thead>
    <tbody>
    <tr>
    	<td class="ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">1</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">2</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">3</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">4</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">5</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">6</a></td>
	</tr>
    <tr>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">7</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-days-cell-over ui-datepicker-today"><a href="#" class="ui-state-default ui-state-highlight">8</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">9</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">10</a></td>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">11</a></td>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">12</a></td>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">13</a></td>
    </tr>
    <tr>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">14</a></td>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">15</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">16</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">17</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">18</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">19</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">20</a></td>
	</tr>
    <tr>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">21</a></td>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">22</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">23</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">24</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">25</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">26</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">27</a></td>
	</tr>
    <tr>
    	<td data-year="2012" data-month="9" data-event="click" data-handler="selectDay" class="ui-datepicker-week-end"><a href="#" class="ui-state-default">28</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">29</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">30</a></td>
        <td data-year="2012" data-month="9" data-event="click" data-handler="selectDay"><a href="#" class="ui-state-default">31</a></td>
        <td class="ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td>
        <td class="ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td>
        <td class="ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td>
    </tr>
    </tbody>
    </table>
</div>