    <aside class="legend">
		<h3>Legend</h3>
		<ul>
	<?php if($page=="seat"){?>        
			<li class="vacant"><small class="ico"></small>Vacant seat</li>
			<li class="occupied"><small class="ico"></small>Occupied seat</li>
            <li class="selected"><small class="ico"></small>Selected seat</li>
            <li class="bulked"><small class="ico"></small>Bulked seat</li>
            <li class="basinet"><small class="ico"></small>Baseinet seat</li>
            <li class="exit"><small class="ico"></small>Exit</li>
            <li class="toilet-share"><small class="ico"></small>Shared toilet</li>
            <li class="toilet-male"><small class="ico"></small>Male toilet</li>
            <li class="toilet-female"><small class="ico"></small>Female toilet</li>
            <li class="gallery"><small class="ico"></small>Gallery</li>
            <li class="bar"><small class="ico"></small>Bar</li>
            <li class="closet"><small class="ico"></small>Closet</li>                                                                        
	<?php }else{?>
			<li class="lowest"><small class="ico"></small>Lowest price</li>
		<?php if($page!="occl" && $page!="itcl"){?>
            <li class="lastseat"><small class="ico"></small>Last seat availability</li>
        <?php }?>
    <?php }?>
		</ul>
		<p>Prices per adult and include taxes and servicing fees.</p>
	</aside>