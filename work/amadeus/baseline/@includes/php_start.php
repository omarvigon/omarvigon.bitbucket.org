<?php
$lang="en";

$folder=substr($_SERVER["SCRIPT_NAME"],1,strrpos($_SERVER["SCRIPT_NAME"],"/")-1);

$page=substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
$page=explode(".",$page);
$page=$page[0];

$option=(isset($_GET["option"]))? $_GET["option"]:false;

if(isset($_GET["skin"]))
{
	$skin=$_GET["skin"];
	setcookie("skin",$skin,time()+60*60*24*30,"/");
}
else
{
	if(isset($_COOKIE["skin"]))
	{
		$skin=$_COOKIE["skin"];
	}
	else
	{
		$skin="baseline";
		setcookie("skin",$skin,time()+60*60*24*30,"/");
	}
}

function includeBody($classname)
{
	echo '<body class="'.$GLOBALS["folder"].' '.$classname.'" id="'. $GLOBALS["page"].'">';
}
function includeTime($week,$day,$month,$year)
{
	$newweek=($week=="")? "":"<abbr>".$week."</abbr> ";
	$newyear=($year=="")? "":" <b>".$year."</b>";
	
	echo '<time datetime="2013-07-27">'.$newweek.'<b>'.$day.'</b> <abbr>'.$month.'</abbr>'.$newyear.'</time>';
	
	/*fix the datetime=*/
}
?>