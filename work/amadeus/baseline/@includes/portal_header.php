    <header class="main">
    	<a class="logo" href="#"><img src="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/img/logo.png" alt="Logo Airline" /></a>
        <p class="langs">
        	<a href="#"><small class="ico"></small> <span>International - English</span></a>
        </p>
        <nav class="top">
            <a class="type-1" href="#"><small class="ico"></small> <span>Help</span></a>
            <a class="type-2" href="#"><small class="ico"></small> <span>Contact</span></a>
        </nav>
    	<?php include("portal_searchsite.php")?>
        <aside>
			<?php include("module_placeholder.php")?>
		</aside>
        <nav class="side">
        	<ul>
	        	<li class="type-1"><small class="ico"></small><a href="#">Home</a></li>
                <li class="type-2"><small class="ico"></small><a href="#">Offers</a></li>
                <li class="type-3">
                	<small class="ico"></small><a href="#">Play and Book</a>
                    <ul class="sub">
                    	<li><a href="#">Flight Schedules</a></li>
                        <li><a href="#">Departures and Arrivals</a></li>
                        <li><a href="#">Destination</a></li>
                        <li><a href="#">Book</a></li>
                    </ul>
				</li>
                <li class="type-4"><small class="ico"></small><a href="#">Manage My Booking</a></li>
                <li class="type-5"><small class="ico"></small><a href="#">Customer Care</a></li>
            </ul>
        </nav>
    </header>
    <nav class="main">
    	<div>
    		<a class="type-1" href="#"><small class="ico"></small><b>Home</b></a>
        	<a class="type-2" href="#"><small class="ico"></small><b>Breadcrumb 1</b></a>
        	<a class="type-3" href="#"><small class="ico"></small><b>Breadcrumb 2</b></a>
        	<em class="type-4"><small class="ico"></small><b>Current</b></em>
        </div>
    </nav>
	<?php include("module_placeholder.php")?>
    <section class="main">