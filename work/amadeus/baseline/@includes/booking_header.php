    <header class="main">
    	<a class="logo" href="#"><img src="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/img/logo.png" alt="Logo Airline" /></a>
	<?php if($option=="logged"){?>
      	<section class="login">
        	<h3><b>Welcome</b> <em>Mr. John Smith</em></h3>
            <div>
                <p><a href="#">Log-out</a></p>
                <ul>
                    <li class="account"><b>Account</b> <em>65H54B899</em></li>
                    <li class="tier"><b>Tier</b> <em>Gold</em></li>
                    <li class="miles"><b>Miles</b> <em>125,000</em></li>
                </ul>
            </div>
        </section>  	
	<?php }else{?>
        <form class="login" method="post" action="#">
            <h3>Login</h3>
            <div>
        	    <fieldset>
        		    <label class="user"><em>User name</em><input type="text" name="username" /></label>
					<label class="pass"><em>Password</em><input type="password" name="pasword" /></label>
		            <button type="submit">Log in</button>
    	        </fieldset>
				<p><a href="#">Forgot my password</a></p>
            </div>
        </form>
   	<?php }?>
    </header>
    <nav class="main">
    	<div>
		<?php switch($page)
        { 
            case "srch":?>
            <em class="type-1 active"><small class="ico"></small><b><span>1</span> Search</b></em>
            <em class="type-2"><small class="ico"></small><b><span>2</span> Select</b></em>
            <em class="type-3"><small class="ico"></small><b><span>3</span> Passenger</b></em>
            <em class="type-4"><small class="ico"></small><b><span>4</span> Payment</b></em>
            <em class="type-5"><small class="ico"></small><b><span>5</span> Confirmation</b></em> 
            <?php break;
            
			case "fare":?>
			<a class="type-1" href="#"><small class="ico"></small><b><span>1</span> Search</b></a>
            <a class="type-2" href="#"><small class="ico"></small><b><span>2</span> Select</b></a>
            <em class="type-3x active"><small class="ico"></small><b><span>3</span> Fare</b></em>
            <em class="type-3"><small class="ico"></small><b><span>4</span> Passenger</b></em>
            <em class="type-4"><small class="ico"></small><b><span>5</span> Payment</b></em>
            <em class="type-5"><small class="ico"></small><b><span>6</span> Confirmation</b></em> 
            <?php break;
			
            case "alpi":
            case "apis":?>
			<a class="type-1" href="#"><small class="ico"></small><b><span>1</span> Search</b></a>
            <a class="type-2" href="#"><small class="ico"></small><b><span>2</span> Select</b></a>
            <em class="type-3 active"><small class="ico"></small><b><span>3</span> Passenger</b></em>
            <em class="type-4"><small class="ico"></small><b><span>4</span> Payment</b></em>
            <em class="type-5"><small class="ico"></small><b><span>5</span> Confirmation</b></em>   
            <?php break;
            
            case "purc":?>
			<a class="type-1" href="#"><small class="ico"></small><b><span>1</span> Search</b></a>
            <a class="type-2" href="#"><small class="ico"></small><b><span>2</span> Select</b></a>
            <a class="type-3" href="#"><small class="ico"></small><b><span>3</span> Passenger</b></a>
            <em class="type-4 active"><small class="ico"></small><b><span>4</span> Payment</b></em>
            <em class="type-5"><small class="ico"></small><b><span>5</span> Confirmation</b></em> 
            <?php break;
			
            case "conf":?>
			<a class="type-1" href="#"><small class="ico"></small><b><span>1</span> Search</b></a>
            <a class="type-2" href="#"><small class="ico"></small><b><span>2</span> Select</b></a>
            <a class="type-3" href="#"><small class="ico"></small><b><span>3</span> Passenger</b></a>
            <a class="type-4" href="#"><small class="ico"></small><b><span>4</span> Payment</b></a>
            <em class="type-5 active"><small class="ico"></small><b><span>5</span> Confirmation</b></em>
            <?php break;
            
            default:?>            
            <a class="type-1" href="#"><small class="ico"></small><b><span>1</span> Search</b></a>
            <em class="type-2 active"><small class="ico"></small><b><span>2</span> Select</b></em>
            <em class="type-3"><small class="ico"></small><b><span>3</span> Passenger</b></em>
            <em class="type-4"><small class="ico"></small><b><span>4</span> Payment</b></em>
            <em class="type-5"><small class="ico"></small><b><span>5</span> Confirmation</b></em>            
            <?php break;
        }
        ?>
        </div>
    </nav>
    <section class="main">