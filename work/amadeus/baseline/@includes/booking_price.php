<section class="price">
	<h3>Price Summary</h3>
    <div>
        <table>
        <tfoot>
            <tr class="total">
                <th><b>Total Price</b> <small>=</small></th><td><strong>260.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
		<?php if($page!="conf" && $page!="fare"){?>            
            <tr class="currency">
                <td colspan="2"><a href="#">Currency Converter</a></td>
            </tr>
            <tr class="details">
                <th>Display price in:</th><td><select><option>Dollar (USD)</option></select></td>
            </tr>
            <tr class="details">
                <th><small>The converted price is</small></th><td><strong>291.00$</strong> <small>(USD)</small></td>
            </tr>
		<?php }?>            
        </tfoot>
        <tbody>
            <tr class="adult">
                <th><span>Adult</span> <em>x2</em></th><td><strong>100.00&euro;</strong> <small>(EUR)</small></td>
            </tr>            
            <tr class="infant">
                <th><span>Child</span> <em>x1</em></th><td><strong>20.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
            <tr class="children">
                <th><span>Infant</span> <em>x1</em></th><td><strong>10.00&euro;</strong> <small>(EUR)</small></td>
            </tr>            
            <tr class="taxes">
                <th><?php if($page!="conf" && $page!="fare"){?><small class="ico"></small><?php }?><span>Taxes</span></th><td><strong>30.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
		<?php if($page!="conf" && $page!="fare"){?>             
            <tr class="details">
                <th>Security charges</th><td><strong>10.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
            <tr class="details">
                <th>Security bag taxes</th><td><strong>10.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
            <tr class="details">
                <th>Fuel surcharge</th><td><strong>10.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
		<?php }?>                    
            <tr class="fee">
                <th>Services fee</th><td><strong>10.00&euro;</strong> <small>(EUR)</small></td>
            </tr>
            <tr class="insurance">
                <th>Insurance</th><td><strong>20.00&euro;</strong> <small>(EUR)</small></td>
            </tr>                       
        </tbody>
        </table>
    </div>
</section>    