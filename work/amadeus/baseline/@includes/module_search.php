	<form class="search" action="#" method="post">
    	<header>
        	<label class="trip2 active"><input type="radio" name="radio_1" /><small class="ico"></small><em>Return Trip</em></label>
            <label class="trip1"><input type="radio" name="radio_1" /><small class="ico"></small><em>One Way</em></label>
		</header>
        <fieldset class="flights">
            <fieldset class="place">
                <p class="from"><label><em>From <sup>*</sup></em> <input type="text" value="city or airport (name/code)" /></label><button>&nbsp;</button><i>&nbsp;</i></p>
                <p class="to"><label><em>To <sup>*</sup></em> <input type="text" value="city or airport (name/code)" /></label><button>&nbsp;</button><i>&nbsp;</i></p>
            </fieldset>
            <fieldset class="times">
                <p class="departure"><label><em>Departure <sup>*</sup></em> <input type="datetime" value="23/11/2012" /></label><button>&nbsp;</button><i>Tuesday, 23 NOV 2013</i></p>
                <p class="return error"><label><em>Return <sup>*</sup></em> <input type="datetime" value="23/11/2012" /></label><button>&nbsp;</button><small class="ico"></small><i>&nbsp;</i></p>
            </fieldset>
	<?php if($page!="time"){?>
            <p class="flexible"><label><input type="checkbox" /> <span>My dates are flexible</span></label><i>(+/- 3 days)</i></p>      
            <fieldset class="cabin">
                <h3>Cabin</h3>
                <p><button class="type-1">Economy</button> <button class="type-2">Business</button> <button class="type-3">First</button></p>
            </fieldset>       
        </fieldset>
        <fieldset class="passengers">
        	<h3>Passengers</h3>
            <p>
            	<label class="adult"><em>Adult</em> <select><option value="1">1</option><option value="2">2</option></select></label>
            	<label class="child"><em>Child <i>(2-11)</i></em> <select><option value="0">0</option><option value="1">1</option></select></label>
            	<label class="infant"><em>Infant <i>(&lt;2)</i></em> <select><option value="0">0</option><option value="1">1</option></select></label>
            </p>
	<?php }?>              
        </fieldset>
		<?php include("module_submit.php")?>
    </form>