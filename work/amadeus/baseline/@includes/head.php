<?php include("php_start.php")?>
<!DOCTYPE html>
<html lang="<?php echo $lang?>">
<head>
    <meta charset="utf-8" />
    <title>Baseline <?php echo strtoupper($page)?></title>
    <link rel="icon" href="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/img/favicon.ico" />
    <link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/main.css" />
    <link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/media.css" media="screen and (max-width:1024px)" />
    <?php if($folder=="booking"){?>
    <link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/ff.css" />
    <?php }?>
    <!--[if IE 6]><link rel='stylesheet' type='text/css' href='../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/ie6.css'/><![endif]-->
    <!--[if IE 7]><link rel='stylesheet' type='text/css' href='../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/ie7.css'/><![endif]-->
    <!--[if IE 8]><link rel='stylesheet' type='text/css' href='../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/ie8.css'/><![endif]-->
    <!--[if IE 9]><link rel='stylesheet' type='text/css' href='../@WDS_STATIC_FILES_PATH@/skins/<?php echo $skin?>/css/ie9.css'/><![endif]-->  
    <!--[if lt IE 9]><script src="../@WDS_STATIC_FILES_PATH@/js/html5.js"></script><![endif]-->
    <meta name="viewport" content="width=device-width" />
</head>