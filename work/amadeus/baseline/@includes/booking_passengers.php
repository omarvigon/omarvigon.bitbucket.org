    <section class="passinfo">
		<h3><small class="ico"></small>Passenger Information</h3>

		<?php for($i=1;$i<3;$i++) {?>
		<article class="type-<?php echo $i?>">
			<div class="data">
				<h4>Mr. John Smith</h4>
				<ul>
					<li class="datebirth"><b>Date of birth :</b> <em>10/08/1985</em></li>
					<li class="frequent"><b>Frequent flyer :</b> <em>FF 123456789</em></li>
					<li class="meal"><b>Prefered Meal :</b> <em>Vegetarian</em></li>
					<li class="passport"><b>Passport Number :</b> <em>0123456789</em></li>
					<li class="country"><b>Country of Residence :</b> <em>United Kingdom</em></li>
				</ul>
				<aside class="contact">
					<h5>Contact Information</h5>
					<div>
						<header>
							<h6>Mr John Smith</h6>
							<p><b>E-mail:</b> <em>john-smith@amadeus.com</em></p>
						</header>
						<ul>
							<li><b>Home Phone :</b> <em>+33 535 456 987</em></li>
							<li><b>Mobile Phone :</b> <em>+33 635 456 987</em></li>
						</ul>
					</div>
				</aside>
				<ul class="infant">
					<li class="travelwith"><b>Travelling with :</b> <em>Master Baby Smith</em></li>
					<li class="datebirth"><b>Date of Birth :</b> <em>10/08/2010</em></li>
				</ul>
			</div>
			<div class="seats">
				<h4>Seats</h4>
				<div>
					<h5 class="out">Outbound</h5>
					<ul class="out">
						<li><b>London to Hong Kong</b> <em>32AB</em></li>
						<li><b>Hong Kong to Bangkok</b> <em>Not reserved</em></li>
					</ul>
					<h5 class="in">Inbound</h5>
					<ul class="in">
						<li><b>Bangkok to Hong Kong</b> <em>05A</em></li>
					</ul>
				</div>
			</div>
		</article>
		<?php }?>
	</section>