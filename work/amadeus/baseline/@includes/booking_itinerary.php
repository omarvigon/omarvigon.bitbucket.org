<section class="itinerary">
	<h3>Your Itinerary</h3>
    <div>
        <article class="bound out">
            <h4><small class="ico plus"></small>Outbound</h4>
            <div class="type-1">
                <h5><?php includeTime("FRI","27","JUL","")?></h5>
                <ul>
				<?php if($page!="occl" && $page!="itcl"){?>
                    <li class="operated"><a href="#">AY123</a> <small>operated by WDSAirways</small></li>
                <?php }?>
                    <li class="bound out"><time>10:05</time> <b>London, Heathrow</b> <i>Terminal 2</i></li>
                <?php if($page!="occl" && $page!="itcl"){?>
                    <li class="connection"><?php includeTime("FRI","27","JUL","")?></li>
                <?php }?>
                    <li class="bound in"><time>18:07</time> <b>Hong Kong</b> <i>Terminal 2</i></li>
                <?php if($page!="occl" && $page!="itcl"){?>
                    <li class="cabin"><em>Cabin:</em> <i>Business Plus</i></li>
                    <li class="faref"><em>Fare family:</em> <i class="textfam-1">Business Plus</i></li>
                    <li class="baggage"><em>Baggage alowance:</em> <a href="#">20kg</a></li>
                    <?php if($page!="fare" && $page!="conf"){?>             
                    <li class="duration"><em>Total duration:</em> <small class="ico"></small><i>8h05m</i></li>
                    <?php }?>
                <?php }?>
                </ul>
            </div>
		<?php if($page!="occl" && $page!="itcl"){?>
            <div class="connection">
                <header><small class="ico"></small></header>
                <p><em>Connection time:</em><i>1h05m</i></p>
            </div>
            <div class="type-2">
                <h5><?php includeTime("FRI","27","JUL","")?></h5>
                <ul>
                    <li class="operated"><a href="#">AY123</a> <small>operated by WDSAirways</small></li>
                    <li class="bound out"><time>10:05</time> <b>London, Heathrow</b> <i>Terminal 2</i></li>
                    <li class="bound in"><time>18:07</time> <b>Hong Kong</b> <i>Terminal 2</i></li>
                    <li class="cabin"><em>Cabin:</em> <i>Business Plus</i></li>
                    <li class="faref"><em>Fare family:</em> <i class="textfam-1">Business Plus</i></li>
                    <li class="baggage"><em>Baggage alowance:</em> <a href="#">20kg</a></li>                
                    <li class="duration"><em>Total duration:</em> <small class="ico"></small><i>8h05m</i></li>
                </ul>
            </div>
        <?php }?>
        </article>
        <article class="bound in">
            <h4><small class="ico minus"></small>Inbound</h4>
            <div>
                <h5><?php includeTime("MON","24","OCT","")?></h5>
                <ul>
                    <li class="bound out"><time>10:05</time> <b>London, Heathrow</b> <i>Terminal 2</i></li>
				<?php if($page!="occl" && $page!="itcl"){?>
                    <li class="connection"><span>Connection via Bangkok</span> <?php includeTime("MON","24","OCT","")?></li>
                <?php }?>
                    <li class="bound in"><time>18:07</time> <b>Hong Kong</b> <i>Terminal 2</i></li>
                <?php if($page!="occl" && $page!="itcl"){?>
                    <li class="faref"><em>Fare family:</em> <i class="textfam-1">Business Plus</i></li>
                    <li class="duration"><em>Total duration:</em> <small class="ico"></small><i>8h05m</i></li>
                <?php }?>
                </ul>
            </div>
        </article>
        <footer>
            <p><a href="#">Modify</a></p>
        </footer>
    </div>
</section>