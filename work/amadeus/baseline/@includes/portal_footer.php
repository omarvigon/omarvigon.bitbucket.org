	</section>
    <footer class="side">
		<?php include("module_placeholder.php")?>
	</footer>        
    <footer class="main">
    	<?php include("portal_searchsite.php")?>
        <nav>
        	<p class="type-1"><a href="#">Home</a> | <a href="#">About Us</a> | <a href="#">Contact</a> | <a href="#">FAQ</a> | <a href="#">Feedback</a></p>
            <p class="type-2"><a href="#">Sitemap</a> | <a href="#">Terms and Conditions</a> | <a href="#">Privacy Policy</a></p>
        </nav>
        <address>&copy; 2012 Airways Co. All Rights Reserved.</address>
    </footer>