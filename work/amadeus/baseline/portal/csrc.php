<?php
include("../@includes/head.php");
includeBody("column1");
include("../@includes/portal_header.php");
?>
	<section class="csrc">
        <h1>Search Results</h1>
        <header>
	        <p><output>48 results found</output> <b>Did you mean:</b> <a href="#" rel="tag">lorem</a></p>
		</header>
	<?php for($i=1;$i<5;$i++) {?>
        <article class="type-<?php echo $i?>">
            <h3><a href="#">Book flights | Easy transfer from Airport - Airline</a></h3>
            <h4><a href="#">http://www.airline.com/</a></h4>
            <p>Faster route between Europe and Asia. Modern fleet - more comfort, lower emissions. Search and book your flight today. <b>Lorem</b> ipsum....</p>
            <p>Pellentesque nisl elit, condimentum ac tincidunt vel, commodo sed sapien. Integer justo dolor, aliquam ut vestibulum non, feugiat sed orci. Fusce porttitor malesuada enim vel hendrerit.</p>
        </article>
	<?php } ?>
        <?php include("../@includes/portal_pagination.php")?>
	</section>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>