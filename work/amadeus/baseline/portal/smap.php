<?php
include("../@includes/head.php");
includeBody("column1");
include("../@includes/portal_header.php");
?>
    <nav class="smap type-4">
	<?php for($i=1;$i<5;$i++) {?>
        <div class="type-<?php echo $i?>">
		<?php for($j=1;$j<3;$j++) {?>
            <article class="type-<?php echo $j?>">
                <h2><small class="ico"></small><a>Section 1</a></h2>
                <h3><small class="ico"></small><a>Subsection 1.1</a></h3>
                <ol>
                    <li><a href="#">Page 1</a></li>
                    <li><a href="#">Page 2</a></li>
                    <li><a href="#">Page 3</a></li>
                    <li><a href="#">Page 4</a></li>
                </ol>
                <h3><small class="ico"></small><a>Subsection 1.2</a></h3>
                <ol>
                    <li><a href="#">Page 1.1</a></li>
                    <li><a href="#">Page 2.1</a></li>
                    <li>
                        <h4><small class="ico"></small><a>Subsection 1.2.1</a></h4>
                        <ol>
                            <li><a href="#">Page 1211</a></li>
                            <li><a href="#">Page 1212</a></li>
                            <li><a href="#">Page 1212</a></li>
                            <li><a href="#">Page 1214</a></li>
                        </ol>
                    </li>
                    <li><a href="#">Page 4</a></li>
                </ol>
            </article>
		<?php } ?>
		</div>
	<?php } ?>
	</nav>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>