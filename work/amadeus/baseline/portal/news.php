<?php
include("../@includes/head.php");
includeBody("column2l");
include("../@includes/portal_header.php");
?>
	<div class="main">
        <form class="news" action="#" method="post">
			<h3>News Archive</h3>
            <p class="rss"><small class="ico"></small><a href="#">RSS Feed</a></p>
            <fieldset>
        	    <label class="type-1"><em>Published:</em> <select><option value="0">Year</option><option value="2012">2012</option></select></label>
    	        <label class="type-2"><em>Category:</em> <select><option value="0">All</option><option value="cat1">Category 1</option></select></label>
	            <button type="submit">Submit</button>
            </fieldset>
        </form>
        <section class="news">
		<?php for($i=1;$i<5;$i++) {?>
           	<article class="type-<?php echo $i?>">
                <h3><time>15-03-2010</time> <a href="#">WDS Airways moves to a new terminal at Malaga airport</a></h3>
                <p>From Tuesday, March 16, 2010, WDS Airways will use the new terminal extension, Terminal 3 at Malaga Airport. T3 is located next to the already existing Terminal 2, which Finnair has used previously, and is within walking distance....</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                <ul>
                	<li>List element 1</li>
                    <li>List element 2</li>
                    <li>List element 3</li>
                </ul>
                <blockquote>Text that is quoted from another source</blockquote>
                <table>
                <caption>Title of the table</caption>
                <thead>
               	<tr>
                	<td>Header 1</td>
                   	<td>Header 2</td>
                   	<td>Header 3</td>
                </tr>
                </thead>
                <tbody>
               	<tr>
                	<td>Cell 1</td>
                   	<td>Cell 2</td>
                   	<td>Cell 3</td>
                </tr>      
                </tbody>
                </table>
                <footer>
                    <p><small class="ico"></small><a href="#">Show more</a></p>
                </footer>
            </article>
		<?php }?>
            <?php include("../@includes/portal_pagination.php")?>
        </section>
	</div>
    <aside class="main">
    	<?php include("../@includes/portal_menu.php")?>
    </aside>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>