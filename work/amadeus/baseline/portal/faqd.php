<?php
include("../@includes/head.php");
includeBody("column2l");
include("../@includes/portal_header.php");
?>
	<div class="main">
    	<section class="faq">
            <h1>FAQ: Frequently Asked Questions</h1>
            <nav>
            	<a href="#">FAQ Question 1</a> <a href="#">FAQ Question 2</a> <a href="#">FAQ Question 3</a> <a href="#">FAQ Question 4</a>
            </nav>
		<?php for($i=1;$i<5;$i++) {?>
            <article class="type-<?php echo $i?>">
                <h3><small class="ico"></small>Faq Answer</h3>
                <p>Where can I find flight schedules? Where can I get information about departure and arrival times? Where can I view a flight's seat availability?</p>
                <p>Pellentesque nisl elit, condimentum ac tincidunt vel, commodo sed sapien. Integer justo dolor, aliquam ut vestibulum non, feugiat sed orci. Fusce porttitor malesuada enim vel hendrerit.</p>
            </article>
        <?php }?>
        </section>
	</div>
    <aside class="main">
    	<?php include("../@includes/portal_menu.php")?>
    </aside>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>