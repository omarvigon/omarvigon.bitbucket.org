<?php
include("../@includes/head.php");
includeBody("column2l");
include("../@includes/portal_header.php");
?>
	<div class="main">
        <section class="offermain">
            <h1>OFFERS</h1>
            <header>
            	<p>My london vacations <a href="#">(back to default offer search)</a></p>
			</header>
            <form class="sort" action="#" method="post">
                <label><em>Sort By:</em> <select><option value="price">Price</option><option value="date">Date</option></select></label> <output>48 results found</output>
            </form>
            <?php
            include("../@includes/portal_pagination.php");
			include("../@includes/portal_offers.php");
			include("../@includes/portal_pagination.php");
			?>
        </section>
	</div>
    <aside class="main">
    	<form class="offer" action="#" method="post">
        	<header>
	            <label><em>Offers from:</em> <select><option value="paris">Paris</option><option value="helsinki">Helsinki</option></select></label>
			</header>
            <fieldset class="period">
            	<h3>Travel period:</h3>
	            <h4><label><input type="radio" name="radio_1" /> First possible date of departure</label></h4>
                <p><input type="datetime" value="23/11/2012" /><button>&nbsp;</button></p>
    	        <h4><label><input type="radio" name="radio_1" /> Last possible date of return</label></h4>
                <p><input type="datetime" value="23/11/2012" /><button>&nbsp;</button></p>
        	    <h4><label><input type="radio" name="radio_1" /> Do not filter by travel period</label></h4>
            </fieldset>
			<fieldset class="class">
            	<h3>Class:</h3>
	            <p><label><input type="checkbox" /> Economy</label></p>
    	        <p><label><input type="checkbox" /> Business</label></p>
            </fieldset>
 			<fieldset class="type">
            	<h3>Choose offer type:</h3>
	            <p><label><input type="checkbox" /> Flights</label></p>
    	        <p><label><input type="checkbox" /> Flights + Hotel</label></p>
                <p><label><input type="checkbox" /> Other service</label></p>
            </fieldset>
            <fieldset class="destination">
            	<h3>Destination:</h3>
	            <p><label><input type="checkbox" /> Intercontinental</label></p>
    	        <p><label><input type="checkbox" /> Europe</label></p>
        	    <p><label><input type="checkbox" /> Finland</label></p>
            </fieldset>
			<?php include("../@includes/module_submit.php")?>
        </form>
    </aside>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>