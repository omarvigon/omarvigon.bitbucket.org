<?php
include("../@includes/head.php");
includeBody("column2l");
include("../@includes/portal_header.php");
?>
	<div class="main">
    	<section class="time">
            <h1>Search Availability Time Table</h1>
            <?php include("../@includes/module_search.php")?>
            <aside>
	            <p><a href="#">PDF timetables</a></p>
    		</aside>
                    
            <table class="time">
            <caption>
            	<h2><?php include("../@includes/module_flight_title.php")?></h2>
                <h3><?php includeTime("FRI","27","JUL","")?> <i>to</i> <?php includeTime("MON","24","OCT","")?></h3>
                <?php include("../@includes/module_prev_next.php")?>
			</caption>
            <thead>
            <tr>
                <td class="flight">Flight</td>
                <td class="departure">Departure</td>
                <td class="arrival">Arrival</td>
                <td class="day"><?php includeTime("FRI","27","JUL","")?></td>
                <td class="day"><?php includeTime("SAT","28","JUL","")?></td>
                <td class="day"><?php includeTime("SUN","29","JUL","")?></td>
                <td class="day"><?php includeTime("MON","30","JUL","")?></td>
                <td class="day"><?php includeTime("TUE","31","JUL","")?></td>
                <td class="day"><?php includeTime("WED","1","AUG","")?></td>
                <td class="day"><?php includeTime("THU","2","AUG","")?></td>
                <td class="duration">Duration</td>
                <td class="transfer">Transfer point</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="flight"><small class="ico"></small><a href="#">AY123</a></td>
                <td class="departure"><time>06:30</time></td>
                <td class="arrival"><time>07:30</time></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day inactive"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day"><small class="ico"></small></td>
                <td class="day"><small class="ico"></small></td>
                <td class="day"><span>-</span></td>
                <td class="duration"><span>1h00m</span></td>
                <td class="transfer"><span>&nbsp;</span></td>
            </tr>
            <tr>
                <td class="flight"><small class="ico"></small><a href="#">AY124</a></td>
                <td class="departure"><time>07:30</time></td>
                <td class="arrival"><time>10:30</time></td>
                <td class="day"><span>-</span></td>
                <td class="day"><small class="ico"></small></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day active"><small class="ico"></small></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="duration"><span>3h00m</span></td>
                <td class="transfer"><span>Paris</span></td>
            </tr>
            <tr>
                <td class="flight"><small class="ico"></small><a href="#">AY125</a><br /><small class="ico"></small><a href="#">AY226</a></td>
                <td class="departure"><time>22:30</time><br /><time>00:30</time><span>(+1)</span></td>
                <td class="arrival"><time>23:30</time><br /><time>2:30</time></td>
                <td class="day"><small class="ico"></small></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day"><span>-</span></td>
                <td class="day inactive"><span>-</span></td>
                <td class="duration"><span>4h00m</span><br /><span>1 stop</span></td>
                <td class="transfer"><em>Paris (Charles de Gaulle)</em><br /><em>Paris (Orly)</em></td>
            </tr>
            </tbody>
            </table>
            <footer>
                <p><em>Book these flight options?</em> <button type="submit">Book</button></p>
            </footer>
		</section>
	</div>
    <aside class="main">
    	<?php include("../@includes/portal_menu.php")?>
    </aside>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>