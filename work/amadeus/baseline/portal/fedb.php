<?php
include("../@includes/head.php");
includeBody("column2l");
include("../@includes/portal_header.php");
?>
	<div class="main">
		<form class="fedb" action="#" method="post">
        <?php include("../@includes/module_required.php")?>
        <section>
            <fieldset class="type-1">
            	<h3>Your feeback</h3>
                <p><em>I would like to have a reply to my feedback:</em> <label><input type="radio" name="radio_1" checked="checked" /> Yes</label> <label><input type="radio" name="radio_1" /> No</label></p>
                <p><label><em>Feedback concerns: <sup>*</sup></em> <select name="topic"><option>Select a topic...</option><option value="topic1">topic1</option></select></label></p>
                <fieldset>
                	<p><input type="radio" name="radio_2" checked="checked" /><label><em>Booking reference:</em> <input type="text" name="booking_ref" /></label></p>
                    <p><input type="radio" name="radio_2" /><label><em>Flight number:</em> <input type="text" name="flight_number" disabled="disabled" /></label></p>
                    <p><em>Flight Date:</em> <input type="text" value="DD" maxlength="2" disabled="disabled" /> <input type="text" value="MM" maxlength="2" disabled="disabled" /> <input type="text" value="YYYY" maxlength="4" disabled="disabled" /></p>
                </fieldset>
                <p><label><em>Your feedback: <sup>*</sup></em> <textarea name="feedback" rows="4"></textarea></label> <small>The maximum length of the feedback text is 3 000 characters. You may still type in 3 000 characters</small></p>
            </fieldset>
            <fieldset class="type-2">
            	<h3>Your details</h3>
                <p><label><em>Name: <sup>*</sup></em> <input type="text" name="name_1" /></label></p>
                <p><label><em>Last name: <sup>*</sup> <i>(with an Extralargetext to check)</i></em> <input type="text" name="name_2" /></label></p>
                <p><label><em>E-mail: <sup>*</sup></em> <input type="email" name="email" /></label></p>
                <p><select><option value="33">+33</option><option value="34">+34</option></select> <label><em>Phone number:</em> <input type="tel" name="phone" /></label> <small>Use international format e.g. +358 701234...Case reference number can be delivered via SMS in case you provide your mobile phone number.</small></p>
                <p><label><em>Country: <sup>*</sup></em> <select name="country"><option>Select...</option><option value="uk">UK</option><option value="us">US</option></select></label></p>
                <p><em>My prefered contact method is:</em> <label><input type="radio" name="radio_3" checked="checked" /> E-mail</label> <label><input type="radio" name="radio_3" /> Telephone</label></p>
            </fieldset>
        </section>
		<?php include("../@includes/module_submit.php")?>
		</form>
	</div>
    <aside class="main">
    	<?php include("../@includes/portal_menu.php")?>
    </aside>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>