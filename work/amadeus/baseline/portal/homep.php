<?php
include("../@includes/head.php");
includeBody("column2");
include("../@includes/portal_header.php");
?>
    <div class="main">
		<section class="homebook">
        	<h2>Book Now</h2>
     		<div>
	            <?php
                include("../@includes/module_search_header.php");
				include("../@includes/module_search.php");
				?>
      		</div>
		</section>
		<?php include("../@includes/module_placeholder.php")?>
    </div>
    <div class="side">
    	<section class="homenews">
        	<h2>Latest News</h2>
            <div>
	            <p><a href="#">New routes opened to India. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</a></p>
            </div>
            <a class="prev" href="#">&laquo;</a><a class="next" href="#">&raquo;</a>
		</section>
		<section class="homeoffer">
        	<h2>Offers</h2>
        	<?php include("../@includes/portal_offers.php")?>
            <a class="prev" href="#">&laquo;</a><a class="next" href="#">&raquo;</a>
		</section>
        <section title="placeholder">
        	<p>placeholder</p>
        </section>
    </div>
<?php
include("../@includes/portal_footer.php");
include("../@includes/foot.php");
?>