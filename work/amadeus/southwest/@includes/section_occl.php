	<?php include("header_message_certificate.php");?>
	
	<h1 class="h1">Select Travel Day(s)</h1>
    <p>Select your travel day(s) by clicking on the fares in the calendar(s) below. Use the legend on the right to search for specific fare types. 
    	<?php if($booking=="revenue") {?>Our lowest fare from all matching <?php } else { ?>Lowest fares in points from all matching <?php } ?>fare types will be displayed for each date. You will be able to select a specific flight on the next page.
	</p>
    
	<?php if($booking=="redemption") { ?>
	    <div class="box8 float-right" >
	    	<h3 class="caption0 bold text-center">SHOW FARES IN</h3>
	        <p class="wrap inner"><a class="var h6" href="#"><span class="ico option2"></span>$</a> &emsp; <a class="var" href="#"><span class="ico option1"></span>POINTS</a></p>
	    </div>
	<?php } else if($booking=="revenue") { ?>    
	    <div class="box8 float-right" >
	    	<h3 class="caption0 bold text-center">SHOW FARES IN</h3>
	        <p class="wrap inner"><a class="var h6" href="#"><span class="ico option1"></span>$</a> &emsp; <a class="var" href="#"><span class="ico option2"></span>POINTS</a></p>
	    </div>
	<?php } ?>
    
    <h2 class="h2 grid7"><b>Departing Flight:</b> <span class="h4">Dallas (Love Field), TX to Cancun, MX</span></h2>
    <?php
    $i=1;
	include("module_bags_free.php");
	include("table_calendar.php");
	?>
    
    <h2 class="h2 hr"><b>Returning Flight:</b> <span class="h4">Cancun, MX to Dallas (Love Field), TX</span></h2>
    <?php
	$i=2;
    include("module_bags_free.php");
	include("table_calendar.php");
	?>
	
    <h4 class="h5 bold text-right hr">Continue to select flight</h4>
    <?php include("module_footer_continue.php");?>
    <h2 class="bold br">Important Fare &amp; Schedule Information</h2>
	<p class="nomargin br">All fares and fare ranges are subject to change until purchased.</p>
    <p class="nomargin">All fares and fare ranges listed are per person for each way of travel.</p>
    <p class="nomargin">For information on additional types of fares click the following links: <a href="#">Infants</a>, <a href="#">Children (2-11)</a>, <a href="#">Seniors (65+)</a>, <a href="#">Groups (10+)</a>, <a href="#">Military</a> or for additional information call 1-800-I-FLY-SWA (1-800-435-9792). <b>Please note that these fares are a discount off the "Anytime" fares</b>. Fares shown above may be lowe</p>