	<div class="wrap box1 br">
		<div class="float-left">			
			<h2 class="bold">Customize My Results</h2>
            <?php if($i==1 && $revenue!="senior2") { ?>
			<label class="bold"><input type="checkbox" /> Nonstop</label>
			<label class="bold midst"><input type="checkbox" /> Direct (No plane change, with stops)</label>
            <?php } else { ?>
            <label class="bold em10"><input type="checkbox" disabled="disabled" /> Nonstop</label>
			<label class="bold em10 midst"><input type="checkbox" disabled="disabled"  /> Direct (No plane change, with stops)</label>
            <?php } ?>
		</div>
		<?php if($revenue!="senior2" && $page!="rocup" && $booking!="certificate-promotional" && $booking!="certificate-transitional") { ?>
		<div class="float-right wrap">
			<h2 class="float-left em10 br">Fares in:</h2>
			<?php if($booking=="redemption") { ?>
				<a class="float-left var" href="#"><span class="float-left ico option2 midst"></span> <span class="float-left em10">DOLLARS<var class="block">$281 - $584</var></span></a>
				<a class="float-left var" href="#"><span class="float-left ico option1 midst"></span> <span class="float-left">POINTS<var class="block em10">4,320 - 10,125</var></span></a>
			<?php } else { ?>
				<a class="float-left var" href="#"><span class="float-left ico option1 midst"></span> <span class="float-left em10">DOLLARS<var class="block">$281 - $584</var></span></a>
				<a class="float-left var" href="#"><span class="float-left ico option2 midst"></span> <span class="float-left">POINTS<var class="block em10">4,320 - 10,125</var></span></a>
			<?php } ?>
		</div>
		<?php } ?>
	</div>