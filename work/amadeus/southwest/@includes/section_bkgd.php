    <?php 
	include("header_message_senior.php");
	include("header_messages.php");
    ?>
    <h1 class="h1">Your Flight for Friday, July 25, 2012</h1>
	<p class="br">This is your confirmation and itinerary.  Please print this page</p>   
    <div class="menu br text-right">
        <p><a href="#" class="wdk-toggle wdk-toggle-closed" data-wdk-toggle-target="#printAndShareMenu"><small class="ico"></small>Print &amp; Share</a> <small class="ico email"></small><small class="ico print"></small><small class="ico addcal"></small></p>
        <ul class="text-left grid2 none wdk-toggle-closed" id="printAndShareMenu">    
            <li><a href="#"><small class="ico print"></small>Print Page</a></li>
            <li><a href="#"><small class="ico email"></small>E-mail</a></li>
            <li><a href="#"><small class="ico addcal"></small>Add to Calendar</a></li>
            <li><a href="#"><small class="ico phone"></small>Get Flight Status</a></li>
        </ul>
    </div>
    <div class="box2-radius">
        <div class="date float-left text-center"><em class="block small bold">OCT 10</em> <var class="block h5">FRI</var></div>
        <h2 class="h2 bold">10/01/12 - Boston</h2>
        <div class="box-default">
            <span class="ico air-text float-left"></span>
            <div class="indent2">
                <div class="wrap">
                    <p class="float-right text-right"><a href="#">Resend Receipt</a> | <a href="#">Change</a> | <a href="#">Cancel</a><span class="block bold">Confirmation # <b class="em2">1FW54S</b></span><button class="alt">Check In</button></p>
                    <p><b class="block">Dallas (Love Field), TX - DAL to Cancun, MX - CUN</b>10/01/2012 - 10/06/2012</p>
                </div>             
                <?php 
                include("table_passengers.php"); 
                include("table_itinerary.php")
            	?>
            </div>
        </div>
    </div>