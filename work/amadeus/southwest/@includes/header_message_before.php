    <div class="warning box4 btr">
		<span class="ico excl absolute"></span>
		<h3 class="h4 bold em5">Before you modify</h3>
		<ul class="br">
			<?php if($page=="rfare") {?>
        	<li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">Modifying a fully refundable, international fare will result in the fare becoming NON-refundable. Non-refundable fares follow normal re-usable rules.</span></li>            
			<li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">Funds applied from non-refundable fares toward the purchase of a new reservation remain non-refundable.</span></li>
            <li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">The new reservation inherits the earliest expiration date from any funds applied from the old ticket. Therefore, the expiration date of your new reservation and all associated funds may be less than 12 months. Your new expiration date will be displayed on the confirmation receipt sent to you.</span></li>
		    <?php } else if($page=="ufare" || $page=="sfup") {?>
            <li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">Funds applied from non-refundable fares toward the purchase of a new reservation remain non-refundable.</span></li>       
            <li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">The new reservation inherits the earliest expiration date from any funds applied from the old ticket. Therefore, the expiration date of your new reservation and all associated funds may be less than 12 months. Your new expiration date will be displayed on the confirmation receipt sent to you.</span></li>
			<?php } else if($page=="sttc") {?>
        	<li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">Modifying a fully refundable, international fare will result in the fare becoming NON-refundable. Non-refundable fares follow normal re-usable rules.</span></li>            
        	<li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">Changes to itineraries are subject to current availability and may result in higher fares on one or more segments.</span></li>
        	<li class="wrap"><small class="ico arrow2"></small><span class="float-left grid9">Changes apply to all passengers on the reservation.</span></li>            
			<?php }?>
        </ul>
	</div>