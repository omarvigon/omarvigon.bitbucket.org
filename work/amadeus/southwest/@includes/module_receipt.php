	<h2 class="h3 bold br">Where Should We Send Your Receipt?</h2>
    <fieldset class="box2 indent3 br">
		<legend class="h6"><span class="em0">*</span> Send my confirmation receipt via...</legend>
        
	<?php if($page=="purc") { ?>
		<p class="indent"><label class="inline-block grid1 bold"><input type="radio" name="radio_3" checked="checked" /> Email</label> <input type="text" size="35" /></p>
	<?php } else { ?>
        <fieldset class="wrap indent">
            <label class="float-left grid1 rindent-small bold"><input type="radio" name="radio_3" checked="checked" /> Email</label> 
            <div class="float-left grid8">
                <select class="grid4"><option>johndoe@email.com</option></select>
                <p class="br">or enter an Alternate E-mail Address</p><input type="text" class="block" size="30" />
            </div>
        </fieldset>
	<?php } ?>
		<p class="indent br"><label class="grid1 rindent-small bold"><input type="radio" name="radio_3" /> Fax to</label> (<input type="text" size="4" />) <input type="text" size="4" /> - <input type="text" size="4" /> <span class="indent">Receipts can be sent to U.S. fax numbers only.</span></p>
        <p class="br"><b>Note:</b> Receipts contain confidential billing information.</p>
    </fieldset>