    <table class="fare hr">
        <?php if ($booking=='redemption') { ?>
            <caption class="bold">PRICE: POINTS</caption>            
        <?php } else if ($booking=='companion') { ?>
            <caption class="bold">PRICE: COMPANION</caption>          
        <?php } else if ($booking=='certificate-transitional') { ?>
    		<caption class="bold">PRICE: AWARD</caption>			
		<?php } else { ?>
    		<caption class="bold">PRICE: ADULT</caption>		
		<?php } ?>
    <thead>
    <tr class="caption1 bold small">
    	<th class="line4 grid1">Trip</th>
    	<th class="line4 grid1 text-center">Routing</th>
    	<th class="line4 h8 text-center">Fare Type | <a href="#" class="small">View Fare Rules</a></th>
    	<th class="line4 grid4 text-center">Fare Details</th>
        <th class="line4 text-center">Quantity</th>
    </tr>
    </thead>
    <tfoot>
	
	<?php if($page=='rfare' || $page=="rconf" || $page=="ufare") { ?>
    
    	<?php if($booking=="redemption") { ?>
        	<tr class="bold">
				<td colspan="3">&nbsp;</td>
                <td colspan="2" class="box9">New Ticket Total <span class="block float-right text-right">71,680 pts<b class="indent2">$20.00</b></span><a href="../popins/frbd.php?booking=<?php echo $booking ;?>" class="text-right block small wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip" data-wdk-tooltip-my="left center" data-wdk-tooltip-at="right center">Fare Breakdown</a></td>
            </tr>
            <tr>
            	<td colspan="3">&nbsp;</td>
            	<td colspan="2" class="bold">Exchanged Ticket Total <span class="block float-right text-right">70,180 pts<b class="indent2">$10.00</b></span></td>
            </tr>
            <tr>
            	<td colspan="3" class="small">&nbsp;</td>
                <td colspan="2" class="box9 bold">Difference <span class="block float-right text-right">(11,508) pts<b class="indent2">$10.00</b></span></td>
            </tr>
        <?php } else { ?>
            <tr class="bold">
                <td class="small" colspan="3">Enroll in Rapid Rewards and earn at least <a href="../popins/ptsc.php?booking=<?php echo $booking ;?>" class="small wdk-tooltip" data-wdk-tooltip-ajax>6,152 Points</a> per person for this trip. Already a Member? Log in to ensure you are getting the points you deserve.</td>
                <td colspan="2" class="box9">Pending Air Itinary <span class="block float-right text-right">$1109.20</span><a href="../popins/frbd.php?booking=<?php echo $booking ;?>" class="text-right block small wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip" data-wdk-tooltip-my="left center" data-wdk-tooltip-at="right center">Fare Breakdown</a></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
                <td colspan="2" class="line7 bold">Available Funds <span class="block float-right text-right">$1144.20</span></td>
            </tr>
            <?php if(isset($_GET["rfare"]) && $_GET["rfare"]!="refund" || $page=="ufare") { ?>
                <tr>
                    <td colspan="3" class="small">&nbsp;</td>
                    <td colspan="2" class="box9 bold">Additionnal Amount Due <span class="block float-right text-right">
						<?php if(isset($_GET["rfare2"]) && $_GET["rfare2"]=="nothing") { ?>                    		
                            $0.00
						<?php } else { ?>
                        	$102.00
                        <?php } ?>
						</span></td>
                </tr>
            <?php } ?>
            <?php if((!isset($_GET["rfare"]) && $page!="ufare") || (isset($_GET["rfare"]) && $_GET["rfare"]!="pay")) { ?>
                <tr>
                    <td colspan="3" class="small">&nbsp;</td>
                    <td colspan="2" class="box9 bold">Additionnal Credit Due <span class="block float-right text-right">($35.00)</span></td>
                </tr>
            <?php } ?>
    	<?php } ?>   
    <?php } else { ?>
    	<?php if($page!="sttc") { ?>
    		<tr class="bold">
    			<td class="small" colspan="3">
    	 	<?php if($booking!='redemption' && $booking!='companion' && $booking!='certificate-transitional') { ?>
            	Enroll in Rapid Rewards and earn at least <a href="../popins/ptsc.php?booking=<?php echo $booking ;?>" class="small wdk-tooltip" data-wdk-tooltip-ajax>6,152 Points</a> per person for this trip. Already a Member? Log in to ensure you are getting the points you deserve.
    	 	<?php } ?>
     			</td>
    			<td colspan="2" class="box9"><b>Subtotal</b> <span class="block float-right text-right">$54.25</span><a href="../popins/frbd.php?booking=<?php echo $booking ;?>" class="text-right block small wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip" data-wdk-tooltip-my="left center" data-wdk-tooltip-at="right center">Fare Breakdown</a></td>
    		</tr>
        <?php } ?>
	<?php } ?>
    
	<?php if($booking=='redemption' && $page!="rfare") { ?>
		<tr class="bold">
        	<td colspan="3">&nbsp;</td>
            <td colspan="2">Total Points: <span class="block float-right text-right">45,197 pts</span></td>
		</tr>	
	<?php } ?>	

    <?php if($page=="rfare" || $page=="fare" || $page=="ufare" || $page=="sttc") { ?>
        <tr class="caption1">
            <td colspan="5">
            	<p class="float-right text-right br">Bag Charge &emsp; $0.00</p>
                <p class="float-left grid4 small">You can't find this great fare on any other website. Southwest fares are only on southwest.com.</p>
            	<p class="float-left midst midst2"><b>1st and 2nd Checked Bags Fly Free.</b> <a class="block" href="#">Weight and size limits apply.</a></p>
			</td>
        </tr>
    <?php } else { ?>
        <tr class="caption1">
        	<td colspan="5">
            	<p class="float-right grid2 text-right br">Bag Charge &emsp; $0.00</p>
                <p class="float-left grid7 small">Carry-on Items: 1 bag + 1 small personal item are free, <a class="small" href="#">see full details</a><br />Checked Items: First and second bags are free, <a class="small" href="#">size and weight limits apply.</a></p>
			</td>
        </tr>
    <?php } ?>
	
    <?php if($page=="fare" || $page=="sttc") { ?>
        <tr class="bold">
        	<td colspan="3">&nbsp;</td>
            <td colspan="2" class="h4 box9"><b>Air Total</b> <span class="h4 float-right">$54.25</span></td>
        </tr>
    <?php } ?>
    <?php if($page=="uconf") { ?>
		<tr class="bold">
        	<td class="vertical-middle text-right" colspan="3"><a href="#" class="h6">Gov't taxes &amp; fees now included</a></td>
            <td class="h4 box9" colspan="2">Air Total <span class="h4 float-right">$1129.20</span></td>
        </tr>
    <?php } ?>
    </tfoot>
    <tbody>
    <tr>
       	<td>Depart</td>
    	<td class="h8 text-center">DAL-ATL-CUN</td>
    	<td class="text-center">
			<?php if($booking=="companion") { ?>
				<a class="block bold text-12" href="#">Companion</a>
			<?php } else if($booking=="certificate-promotional") { ?>
				<a class="block bold text-12" href="#">Promotional Award</a>
			<?php } else if($booking=="certificate-transitional") { ?>
				<a class="block bold text-12" href="#">Standard Award</a>
			<?php } else { ?>
				<a class="block bold text-12" href="#">Business Select</a><small>Superior Benefits</small>
			<?php } ?>
		</td>
			<?php if($booking=="companion") { ?>
            	<td><ul class="default small em10 h8"><li>Can travel anytime with Member</li><li>Must travel with Member</li></ul></td>
			<?php } else if($booking=="certificate-promotional") { ?>
            	<td><ul class="default small em10 h8"><li>Fully Refundable</li><li>Same-Day Changes</li><li>No Change Fees</li></ul></td>
			<?php } else if($booking=="certificate-transitional") { ?>
            	<td><ul class="default small em10 h8"><li>No Blackout Dates</li><li>Capacity-controlled Seating</li><li>Giftable</li></ul></td>
			<?php } else { ?>
            	<td class="vertical-top">
                    <ul class="default small em10">
                        <li>Priority Boarding</li>
                        <li>Maximum Rapid Rewards&reg; Points</li>
                        <li>Premium Drink</li>
                        <li class="column2">Fly By&reg; Security Lane</li>
                        <li class="column2">Free Same-Day Changes</li>
                        <li class="column2">Fully Refundable</li>
                    </ul>
                </td>
			<?php } ?>
			
    	<td class="text-center">1</td>
    </tr>
    <tr class="alt3">
       	<td>Return</td>
    	<td class="h8 text-center">CUN-DAL</td>
    	<td class="text-center">
			<?php if($booking=="companion") { ?>
				<a class="block bold text-12" href="#">Companion</a>
			<?php } elseif($booking=="certificate-promotional") { ?>
				<a class="block bold text-12" href="#">Promotional Award</a>
			<?php } elseif($booking=="certificate-transitional") { ?>
				<a class="block bold text-12" href="#">Standard Award</a>
			<?php } else { ?>
				<a class="block bold text-12" href="#">Wanna Get Away</a><small>Excellent Value</small>
			<?php } ?>
		</td>
    	<td>
			<?php if($booking=="companion") { ?>
				<ul class="default small em10 h8"><li>Can travel anytime with Member</li><li>Must travel with Member</li></ul>
			<?php } elseif($booking=="certificate-promotional") { ?>
                    <ul class="default small em10">
                        <li>Priority Boarding</li>
                        <li>Maximum Rapid Rewards&reg; Points</li>
                        <li>Premium Drink</li>
                        <li class="column2">Fly By&reg; Security Lane</li>
                        <li class="column2">Free Same-Day Changes</li>
                        <li class="column2">Fully Refundable</li>
                    </ul>
			<?php } elseif($booking=="certificate-transitional") { ?>
				<ul class="default small em10"><li>No Blackout Dates</li><li>Capacity-controlled Seating</li><li>Giftable</li></ul>
			<?php } else { ?>
				<ul class="default small em10 h8"><li>No Changes Fees</li><li>Reusable Funds</li><li>Nonrefundable</li></ul>
			<?php } ?>
		</td>
    	<td class="text-center">1</td>
    </tr>    
    </tbody>
    </table>