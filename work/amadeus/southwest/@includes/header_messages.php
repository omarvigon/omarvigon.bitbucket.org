<?php if(isset($_GET["earlybid"]) && $_GET["earlybid"]=="failure") { ?>
	<div class="error box4 btr">
    	<span class="ico oops float-left"></span>
        <ul class="em0 box4"><li class="h4 bold">We Did Not Charge Your Credit Card for EarlyBird Check-In</li><li>We're sorry for the inconvenience, please <a href="#">purchase your EarlyBird Check-In separately.</a></li></ul>
    </div>
<?php } ?>

<?php if($page=="conf") { ?>
    <div class="warning box4 btr">
		<span class="ico excl absolute"></span>
		<p class="brsmall">The passport information you entered was incomplete. You can <a href="#">retrieve your reservation</a> and enter it here.</p>
	</div>
<?php }?>
	
<?php if($page=="bkgd") { ?>
    <div class="error box4 btr caption11">
        <span class="ico irreg"></span>
        <p><em class="bold em0 h5">Irregular Operations</em> The reservation that you’re attempting to change is currently subject to irregular operations and cannot be modified online. Please contact a Southwest Customer Representative for further assistance at 1-800-435-9792.</p>
    </div>
<?php } ?>

<?php if($page=="fare" && $booking=="redemption") { ?>
    <div class="error box4 btr">
        <span class="ico oops float-left"></span>
        <ul class="em0 box4"><li>Your account is temporarily disabled. Please call 214-792-4223 for support.</li></ul>
    </div>
    <div class="error box4 btr">
        <span class="ico oops float-left"></span>
        <ul class="em0 box4"><li>Your account is in "inactive status". To redeem points for an award ticket, you must first earn points through Southwest flights or Southwest partners. <a href="#">Book your travel</a> now!</li></ul>
    </div>
    <div class="error box4 btr">
        <span class="ico oops float-left"></span>
        <ul class="em0 box4"><li>You do not have enough points available to purchase this flight. <a href="#">Purchase more points.</a></li></ul>
    </div>
<?php } ?>

<?php if($page=="fare" && $booking=="companion") { ?>
    <div class="wrap box2 btr alt">
        <h3 class="float-left h1 midst em5">Companion Pass</h3>
        <p class="float-left em10 midst">We are currently adding a Companion Fare to your existing flight. <a class="block" href="#">Companion Pass Rules and Regulations</a></p>
    </div>
<?php } ?>

<?php if(($page=="ocup" || $page=="rocup") && $revenue=="senior2") { ?>
	<div class="error box4 btr">
		<span class="ico oops float-left"></span>
		<ul class="em0 box4">
			<li>The inbound flight selected for the adult passenger(s) is not available.<br />Please select an alternate flight. We are sotty for the inconvenience.</li>
		</ul>
	</div>
    <div class="wrap box2 btr alt">
		<h2 class="float-left text-center"><span class="small">STEP</span><span class="h1 var block">2</span><small class="slope s3"></small></h2>
		<div class="float-left">
			<h3 class="h4"><b>Select Fare For Senior Passenger</b> (Age 65+)</h3>
			<p class="em10">Senior Fare Options</p><p>&nbsp;</p>
			<p>To qualify for Senior Fare on <b>southwest.com</b>:</p>
			<p><small class="ico arrow2"></small>All passengers must be age 65 or older.</p>
			<p><small class="ico arrow2"></small>Photo ID is required at airport Check-in.</p>
			<p>Visit the <a href="#">Seniors information</a> page for more details on Senior Fares and Online Check-in.</p>
		</div>
	</div>
<?php } ?>

<?php if($page=="rocup") { ?>
	<div class="wrap box2 btr alt">
		<div class="float-left">
			<h3 class="h4"><b>Select Senior Fare</b> (age 65+)</h3>
			<p class="em10">Photo ID is required at airport checkin. Visit our <a href="http://www.southwest.com/html/customer-service/unique-travel-needs/seniors/index-pol.html">Senior fares</a> page for more information.</p>
		</div>
	</div>
<?php } ?>
	
<?php if(($page=="ocup" || $page=="rocup") && $revenue=="senior1") { ?>
    <div class="wrap box2 btr alt">
		<h2 class="float-left text-center"><span class="small">STEP</span><span class="h1 var block">1</span><small class="slope s3"></small></h2>
		<div class="float-left">
			<h3 class="h4"><b>Select Flight and Fare For Adult Passenger</b> (Under Age 65)</h3>
			<p class="em10">A link will redirect you to the senior passenger fare options on the confirmation page</p>
		</div>
	</div>
<?php } ?>
    
<?php if($page=="ocup") { ?>
    <div class="noerror box4 btr">
		<span class="ico done3 absolute"></span>
		<span class="ico enjoy float-left"></span>
		<p class="bold br btr">SAVE 20% ON YOUR ROUND TRIP PURCHASE OF SELECT FARES!</p>
	</div>
    <div class="warning box4 btr">
		<span class="ico excl absolute"></span>
		<p>The flight options displayed do not include flights that have already departed. To view additional flight options, please modify your search criteria to include date(s) in the future.</p>
	</div>
<?php } ?>

<?php if($page=="pnrs") { ?>
	<div class="error box4 btr">
		<span class="ico oops float-left"></span>
		<ul class="em0 box4"><li>The Rapid Rewards Account Number(s) for the following Passenger(s) is/are invalid: JOHN PEEKTESTONE - 4815162343</li></ul>
	</div>
<?php } ?>    