    <?php 
	include("header_message_senior.php");
	include("header_messages.php");
    ?>
    <p class="float-right br"><small class="ico print"></small><a href="#">Print Page</a>  <small class="ico addcal"></small><a href="#">Add to Calendar</a>  <small class="ico phone"></small><a href="#">Get Flight Status</a></p>
    <h1 class="h1">Thank you for your purchase!</h1>
	<h2 class="h4">Dallas (Love Field), TX  - DAL to Cancun, MX - CUN</h2>
	<hr class="line11" />
    <div class="wrap columns br">
    	<div class="float-left grid3 box3">
        	<h3><span class="ico plane midst"></span><span class="h3 em5">Air</span> <a class="float-right midst" href="#">View Details</a></h3>
            <p class="caption10 h6 bold">Confirmation #1FW54S</p>
            <p><b class="block">Dallas (Love Field), TX - DAL to Cancun, MX - CUN</b>Monday, October 1, 2012 - Tuesday, October 16, 2012</p>
            <?php if(isset($_GET["earlybid"]) && $_GET["earlybid"]=="no-purchase") { ?>
            <p class="em10"><small class="ico done"></small> EarlyBird Check-In Purchased</p>
            <?php } else if(isset($_GET["earlybid"]) && $_GET["earlybid"]=="failure") { ?>
            <p class="small em0">We were unable to complete your EarlyBird Check-In purchase. Please <a href="#" class="small">purchase your EarlyBird Check-In separately.</a></p>
            <?php } else if($page!="uconf") { ?>
            	<div class="box8">
                	<button class="float-right br">Add it Now</button>
                    <span class="ico ebcheckin"></span>
                    <p class="smaller em8">Automatic Early Check-In, Better Boardin Position, Earlier Access to Overhead Storage</p>
                </div>
            <?php } else { ?>    
            	<p>&nbsp;</p>
            <?php } ?>
            <p class="caption7 absolute text-right text-15 bold">Air Total: $392.00</p>
        </div>
        <div class="float-left grid3 box3">
        	<h3><span class="ico hotel midst"></span><span class="h3 em3">Hotel</span></h3>
            <div class="box11">
                <h4 class="bold em10">Great reasons to book your hotel on southwest.com</h4>
                <ul class="default bold em10 br">
                    <li class="nomargin">$0 Southwest Cancel Fees</li>
                    <li class="nomargin">40,000 Hotel to Choose From</li>
                    <li class="nomargin">Earn Rapid Rewards Credit</li>
                </ul>
                <button>Find a Hotel</button>
            </div>
        </div>
        <div class="float-left grid3 box3">
        	<h3><span class="ico car midst"></span><span class="h3 em7">Car</span></h3>
           	<div class="box12">
                <h4 class="bold em10">Great reasons to book your car on southwest.com</h4>
                <ul class="default bold em10 br">
                    <li class="nomargin">Guaranteed Low Rates</li>
                    <li class="nomargin">14 Car Companies to Choose From</li>
                    <li class="nomargin">Earn Rapid Rewards Credit</li>
                </ul>
            	<button>Find a Car</button>
             </div>
        </div>
    </div>
	<?php
	$i=1;
	include("module_amount_paid.php");
	?>    
	<hr class="boxb" />

    <div class="hr box2-radius">
        <div class="date float-left text-center"><em class="block small">OCT 1</em> <var class="block h5">FRI</var></div>
        <h2 class="h2 bold">10/01/12 - Boston</h2>
        <div class="box-default">
            <span class="ico air-text float-left"></span>
            <div class="indent2">
                <div class="wrap">
                    <div class="float-right text-right">
                        <a href="#">Change</a> | <a href="#">Cancel</a><p class="block bold">Confirmation # <b class="em2">1FW54S</b></p><?php if ($page!="rconf") : ?><button class="alt">Check In</button><?php endif ?>
                    </div>
                    <p><b class="block">Dallas (Love Field), TX - DAL to Cancun, MX - CUN</b>10/01/2012 - 10/06/2012</p>
                </div>                
                <?php include("table_passengers.php") ?>
                <a href="#">Subscribe to Flight Status Messaging</a>                

                <?php
                include("table_itinerary.php");
            	include("table_price.php");
            	?>
                <?php if ($page!="rconf" && $page!="uconf") { ?>
                    <p class="br">Rapid Rewards A-List Members receive reserved boarding priviledges and do not qualify for EarlyBird Check-in. We have removed the EarlyBird Check-In charge(s) for the A-List Member(s). <a href="#">Learn More</a></p>                    
                <?php } ?>
                
                <?php if($page!="rconf" && $page!="uconf") { ?>
					<?php if(!isset($_GET["earlybid"]) || (isset($_GET["earlybid"]) && $_GET["earlybid"]!="purchase")) { ?>
                        
                    <table class="fare hr">
                    <caption>
                        <b>EARLYBIRD CHECK-IN PRICING</b>
                        <?php if(isset($_GET["earlybid"]) && $_GET["earlybid"]=="failure") { ?>
                        <small class="em0">Your air fare has been confirmed but we could not complete your EarlyBird Check-in Purchase</small>
                        <?php } ?>
                    </caption>
                    <thead>
                    <tr class="caption1 bold">
                        <td class="line4">Option</td>
                        <td class="line4">Passenger</td>
                        <td class="line4">Price</td>
                        <td class="line4">Quantity</td>
                        <td class="line4">Details</td>
                        <td class="line4">Receipt #</td>
                        <td class="line4 text-right">Total</td>
                    </tr>
                    </thead>
                    <?php if(isset($_GET["earlybid"]) && $_GET["earlybid"]=="failure") { ?>
                    </table>
                    <p class="em0 midst br">Please continue shopping if you still wish to purchase <a href="#">EarlyBird Check-In.</a></p>
                    
                    <?php } else { ?>
                    <!--
                    <tfoot>
                    <tr class="bold">
                        <td colspan="5">&nbsp;</td>
                        <td class="text-right box9">EarlyBird Check-In total</td>
                        <td class="text-right box9 vertical-middle">$20.00</td>
                    </tr>
                    <tr class="bold">
                        <td class="vertical-middle text-right" colspan="5"><a href="#" class="h6">Gov't taxes &amp; fees now included</a></td>
                        <td class="box9 h4">Air Total</td>
                        <td class="box9 h4">$1129.20</td>
                    </tr>
                    </tfoot>
                    -->
                    <tbody>
                    <tr>
                        <td class="h8">EarlyBird Check-in</td>
                        <td>JOHN DOE</td>
                        <td>$10.00</td>
                        <td class="text-center">1</td>
                        <td>DAL-CUN</td>
                        <td>5260600013626</td>
                        <td class="text-right bold">$10.00</td>
                    </tr>
                    <tr>
                        <td class="h8">EarlyBird Check-in</td>
                        <td>JANE DOE</td>
                        <td>$10.00</td>
                        <td class="text-center">1</td>
                        <td>DAL-CUN</td>
                        <td>5260600013625</td>
                        <td class="text-right bold">$20.00</td>
                    </tr>
                    </tbody>
                    </table>
                    <div class="text-right bold">
                        <dl class="subtotal box9 bold">
                            <dt class="float-left text-left">EarlyBird Check-In total</dt>
                            <dd class="text-right">$20.00</dd>
                        </dl>
						<hr class="nomargin" /><a href="#" class="h6">Gov't taxes &amp; fees now included</a>
                        <dl class="subtotal box9">
                            <dt class="float-left text-left h4">Air Total</dt>
                            <dd class="text-right h4">$1129.20</dd>
                        </dl>
                    </div>
                    
                    <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="box-default hr">	
            <div class="wrap">
                <h3 class="float-left midst2 grid3"><b>Purchaser Name</b> John Doe</h3>
                <h4 class="float-left indent"><b class="float-left">Billing Address</b><span class="float-left indent2">221B Baker Street<br />London, GB NX16XE</span></h4>
            </div>
            <table class="fare br">
		<?php if((!isset($_GET["rconf"]) || $_GET["rconf"]!="hold")) { ?>            
            <thead>
                <tr class="caption1 bold">
                    <td colspan="3">Form of Payment</td>
                    <td colspan="2" class="text-right">Amount Applied</td>
                </tr>
            </thead>
		<?php } ?>
            <tbody>
                <?php if($booking=="redemption") { ?>
                   	<tr>
                        <td colspan="4">Points Used - RR#34982</td>
                        <td class="box2 text-right bold">45,197 pts</td>
                    </tr>
                    <tr>
                        <td colspan="4">Visa - XXXXXXXXXXXX - 1234</td>
                        <td class="box2 text-right bold">$833.70</td>
                    </tr>
                    <tr>
                        <td class="em1 small" colspan="5"><i>Estimated point total is based on advertised fare and could be different than what you saw during the reservation process because of calculations in mixed fare type bookings. *Details are in Terms and Conditions.</i></td>
                    </tr>
                <?php } else if($booking=="certificate-transitional" || $booking=="certificate-promotional") { ?>
                    <tr>
                        <td colspan="4">Award - S133165760-B</td>
                        <td class="box2 text-right bold">N/A</td>
                    </tr>
                    <tr>
                        <td colspan="4">Award - 199495555163950-A</td>
                        <td class="box2 text-right bold">N/A</td>
                    </tr>
                    <tr>
                        <td colspan="4">Visa - XXXXXXXXXX-1234</td>
                        <td class="box2 text-right bold">$30.00</td>
                    </tr>
                <?php } else { ?>
                
                	<?php if(isset($_GET["rconf"]) && $_GET["rconf"]=="refund") { ?>
                    <tr>
                        <td colspan="4">Refund Requested</td>
                        <td class="box2 text-right bold">$70.80</td>
                    </tr>
                    <?php } else if(isset($_GET["rconf"]) && $_GET["rconf"]=="hold") { ?>
                    <tr class="caption1 bold">
                        <td colspan="3">Form of Payment</td>
                        <td colspan="2" class="text-right">Amount refunded</td>
                    </tr>
                    <tr>
                        <td colspan="4">Travel Funds - 4QGL7U - 0049</td>
                        <td class="box2 text-right bold">$23.60</td>
                    </tr>
                    <tr>
                        <td colspan="4">Travel Funds - 4QGL7U - 0050</td>
                        <td class="box2 text-right bold">$23.60</td>
                    </tr>
                    <tr>
                        <td colspan="4">Travel Funds - 4QGL7U - 0051</td>
                        <td class="box2 text-right bold">$23.60</td>
                    </tr>
                    <?php } else { ?>
                    <tr>
                        <td colspan="4">Visa - XXXXXXXXXXXX - 1234</td>
                        <td class="box2 text-right bold">$833.70</td>
                    </tr>
                    <tr class="bold small thin">
                        <td>&nbsp;</td>
                        <td class="box2 line7">Original Balance</td>
                        <td class="box2 line7">Applied</td>
                        <td class="box2 line7">Remaining</td>
                        <td class="box2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Travel Funds - YRTERE - 8365</td>
                        <td class="thin">$348.50</td>
                        <td class="thin">$280.50</td>
                        <td class="thin">$60.00</td>
                        <td class="box2 text-right bold">$280.50</td>
                    </tr>
                    <tr class="bold small thin">
                        <td>&nbsp;</td>
                        <td class="box2 line7">Original Balance</td>
                        <td class="box2 line7">Applied</td>
                        <td class="box2 line7">Remaining</td>
                        <td class="box2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>southwestgiftcard</b> - XXXXXXXXXXXX6531</td>
                        <td class="thin">$10.00</td>
                        <td class="thin">$10.00</td>
                        <td class="thin">$10.00</td>
                        <td class="box2 text-right bold">$10.00</td>
                    </tr>
                    <tr class="bold small thin">
                        <td>&nbsp;</td>
                        <td class="box2 line7">Original Balance</td>
                        <td class="box2 line7">Applied</td>
                        <td class="box2 line7">Remaining</td>
                        <td class="box2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Southwest LUV Voucher - XXXXXXXXXXXX8972</td>
                        <td class="thin">$5.00</td>
                        <td class="thin">$5.00</td>
                        <td class="thin">$0.00</td>
                        <td class="box2 text-right bold">$10.00</td>
                    </tr>
                    <?php } ?>  
                <?php } ?>      
                </tbody>
            </table>
            <?php
            $i=2;
			include("module_amount_paid.php");
			?>
		</div>            
    </div>
    <?php include("module_ads.php");?>