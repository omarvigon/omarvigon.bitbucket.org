<div class="article">
    <h3 class="bold wdk-toggle wdk-toggle-opened" data-wdk-toggle-target="#rapidRewardsSlope,#rapidRewardsContent"><small class="ico"></small>Rapid Rewards</h3>
    <small class="slope s3 wdk-toggle-opened" id="rapidRewardsSlope"></small>
<?php if($login) { ?>
	<div id="rapidRewardsContent" class="wdk-toggle-opened">
		<h4><b class="em1">TIER STATUS</b> <a class="float-right" href="#">View all</a></h4>
		<p class="box2 wrap"><a class="block bold" href="#">To A-List</a><em class="progress"><span class="float-left"></span><b class="float-right em1">0%</b></em></p>
		<h4 class="em1 bold">RAPID REWARDS CREDIT CARD</h4>
		<p class="box2">Earn points faster with the Rapid Rewards Credit Card from Chase.<button class="block br">Learn more</button></p>
		<h4 class="em1 bold">CREDIT &amp; AWARD STATUS</h4>
		<p class="box2"><span class="float-left ico question"></span><a class="bold" href="#">What Happened to my Credits and Awards?</a></p>
		<h4 class="em1 bold">COMPANION PASS</h4>
		<p class="box2"><span class="float-left ico companion"></span> <button class="float-right">Book Now</button> EXP<br />12.31.12</p>
		<p class="em0 br">Your account is temporarily disabled. Please call 214-792-4223 for support</p>
	</div>
<?php } else { ?>
	<div id="rapidRewardsContent" class="wdk-toggle-opened">
		<h4 class="bold em1 br">JOIN TODAY!</h4>
		<p>With the all new <a href="#">Rapid Rewards&copy;</a> program, you have the flexibility to earn even more than before. And, with our new online account tools, you can plan and track your progress to that next vacation!</p>
		<p><button>Enroll now</button></p>
	</div>
<?php } ?>  
</div>
