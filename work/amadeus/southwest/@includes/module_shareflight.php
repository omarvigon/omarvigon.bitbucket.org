	<h2 class="h3 bold br">Let Them Know You are on Your Way</h2>
    <fieldset class="box2 indent3 br">
    	<legend class="h6">Share your flight itinerary with...</legend>
        <div class="wrap indent">
	        <label class="block br"><b class="inline-block grid1">E-mail 1</b> <input class="input-large" type="text" /></label>
	        <label class="block br"><b class="inline-block grid1">E-mail 2</b> <input class="input-large" type="text" /></label>
            <p class="add-field absolute"><small class="ico add"></small><a href="#">Add another email</a> (up to four total).</p>
        </div>
    	<p class="hr"><b>Note:</b> Itineraries do not contain confidential billing information.</p>
    </fieldset>