    <?php include("form_header_purc.php");?>
    
<?php if($page=="ureco") {
    include("module_reco_passengers.php");
} else { ?>
    <h2 class="h3 bold br"><span class="ico blackplane"></span>Who's Flying?</h2>
    <div class="box2 indent3 br">
    
    <?php if($booking=="companion") { ?>
    	<table>
        <thead>
        <tr class="bold noborder">
        	<td>Companion:</td>
            <td>Name</td>
            <td>Rapid Reward</td>
            <td>Date of Birth</td>
            <td>Gender</td>
            <td>Redress # (optional)</td>
        </tr>
        </thead>
        <tbody>
        <tr>
         	<td>(Adult)</td>
            <td>John Smith</td>
            <td>501719595 Reward</td>
            <td>24/04/84</td>
            <td>Male</td>
            <td><input type="text" /></td>       
        </tr>
        </tbody>
        </table>
        <a class="block hr" href="#"><small class="ico disability"></small> Add/Edit Disability Assistance Options</a>
        <h4 class="bold br">Selected Disability Assistance Options:</h4>
        <ul class="default alt">
            <li class="br">Need lift/transfer assistance to and from aircraft seat.</li>
            <li>Powered wheelchair with non-spillable batteries (2)</li>
            <li>Cognitive and developmental disabilities.</li>
        </ul>
        
    <?php } else { ?>
    
    	<div class="wrap">
            <p class="float-left">Passenger 1: <br /><?php if($revenue=="senior1" || $revenue=="senior2") {?>(Senior)<?php } else { ?>(Adult)<?php } ?></p>
            <?php $passengerNumber = 1; ?>
            <?php include("form_passenger.php");?>
        </div>
        <div class="wrap hr">
            <p class="float-left">Passenger 2: <br /><?php if($revenue=="senior1" || $revenue=="senior2") {?>(Senior)<?php } else { ?>(Adult)<?php } ?></p>
            <?php $passengerNumber = 2; ?>
            <?php include("form_passenger.php");?>
        </div>
    <?php } ?>
    </div>
<?php } ?>
    
    <?php include("module_contact.php");?>

    <?php if($page!="ureco") { ?>
	<h2 class="h3 bold br">Would You Like to Add EarlyBird Check-In?</h2>
    <fieldset class="box2 indent3 br">
    	<div class="wrap">
	        <img class="float-left early-picture" src="../@WDS_STATIC_FILES_PATH@/img/earlybird_checkin.gif" alt="Earlybid Check-in" />
            <div class="float-right grid7">
            	<p>We'll automatically check you in and reserve your boarding position before regular check-in begins so you'll have a better seat selection.</p>
        	    <p><small class="ico check"></small>Automatic Check-In</p>
    	        <p><small class="ico check"></small>Better Boarding Position</p>
	            <p><small class="ico check"></small>Earlier Access to Overhead Bins</p>
            </div>
        </div>
        <p class="br">*Conveniently print your boarding pass with pre-assigned boarding position anytime within 24 hours of departure.</p>
        <div id="wdk-early" class="box2">
        	<label class="block bold"><input type="radio" name="radio_1" /> Yes, add EarlyBird Check-In for just $10.00 per passenger, one-way.</label>
            <div class="indent none">
           		<p>You now have EarlyBird Check-In for your passengers and flight segments!</p>
            	<table class="bound">
                <thead>
                <tr class="caption1 bold">
                	<td>&nbsp;</td>
                    <td>Itinerary</td>
                    <td class="text-center">Passengers</td>
                    <td class="text-center">Price</td>
                    <td class="text-center grid1">Subtotal</td>
                </tr>
                </thead>
                <tfoot>
                <tr>
                	<td colspan="2">&nbsp;</td>
                    <th colspan="3" class="box1 alt h6 bold text-right">Estimated Total $30.00</th>
                </tr>
                </tfoot>
                <tbody>
                <tr>
                	<td>N/A</td>
                    <td class="h8">Dallas (Love Field), TX (DAL) to Cancun, MX (CUN)</td>
                    <td class="text-center">3</td>
                    <td class="text-center"><span class="ico textb"></span></td>
                    <td class="text-center">$0</td>
                </tr>
                <tr class="alt3">
                	<td><small class="ico check"></small></td>
                    <td class="h8">Cancun, MX (CUN) to Dallas (Love Field), TX (DAL)</td>
                    <td class="text-center">2<br/>1</td>
                    <td class="text-center">$10<br/><span class="ico texta"></span></td>
                    <td class="text-center">$20</td>
                </tr>
                </tbody>
                </table>
                <p class="br"><b>On the <a href="#">A-List?</a></b> Congratulations, we'll automatically check you in for your flight before our EarlyBird Check-In Customers at no additional cost. Want EarlyBird Check-In for the other travelers on this itinerary? Choose 'Yes' and we will only charge for travelers who are not on the A-List.</p>
            </div>
			<label class="block bold hr"><input type="radio" name="radio_1" checked="checked" /> No, Thanks.</label>
        </div>
        <p class="br">EarlyBird Check-In&trade; is nonrefundable. Certain <a href="#">exclusions</a> may apply.</p>
    </fieldset>
    <?php } ?>
    <?php
    if(!isset($_GET["purc"]) || (isset($_GET["purc"]) && $_GET["purc"]!="ard"))
	{
		include("module_apply_travelfunds.php");
	?>
    
    <?php if($booking=="certificate-transitional") { ?>
    
    <h2 class="h3 bold br">Rapid Rewards Awards</h2>
    <div class="box2 br">
		<table class="fare text-center">	
        <thead>
        <tr class="caption1 bold">
            <td>Award Type</td>
            <td>Award Number</td>
            <td>Coupon</td>
            <td>Expiration Date</td>
        </tr>
        </thead>
        <tbody>
        <tr class="bg1">
            <td>Standard Award</td>
            <td>19949555516349</td>
            <td>B</td>   
            <td>12/30/2013</td>
        </tr>
        <tr class="alt3">
            <td>Freedom Award</td>
            <td>19949555516350</td>
            <td>A</td>
            <td>04/24/2014</td>
        </tr>    
        </tbody>
        </table>
        <p class="hr">The Awards above have been preselected to automatically use those which expire first and are valid at least through the last scheduled travel date that you have chosen.</p>
	</div>
	<?php } ?>
    
    <h2 class="h3 bold br">What Payment Method Would You Prefer?</h2>
    <fieldset class="box2 indent3 br">
    	<?php
        if(isset($_GET["purc"]) && $_GET["purc"]=="even_exchange")
            include("module_purc.php");
    	else   
    		include("module_payment_preference.php");   
    	?>
	</fieldset>
            
    <?php }
    include("module_receipt.php");
	include("module_shareflight.php");
	?>
    
    <?php if($page!="ureco") { ?>
    <h2 class="h3 bold br">What is the Purpose of this Travel?</h2>
    <fieldset class="box2 indent3 bold">
    	<label class="indent midst"><input type="radio" name="radio_4"/>Personal</label> <label class="midst"><input type="radio" name="radio_4"/>Business</label> <label class="midst"><input type="radio" name="radio_4"/>Both</label>
    </fieldset>
    <?php } ?>
        
   	<?php include("module_review_purchase.php");?>
    
<?php if($page=="ureco") { 
		include("module_note_changes.php");
} else { ?>  
    
    <h4 class="h6 hr bold em3"><span class="ico green"></span> Please Note:</h4>
    <div class="indent2 br">
	    <p>By clicking "Purchase", you acknowledge that you agree:</p>
    	<ul>
        <li class="br"><small class="ico arrow2"></small>All Passenger Names have been entered correctly. Changes after completion of purchase could result in a fare increase.</li>
	    <li class="br"><small class="ico arrow2"></small>To the <a href="#">fare rules</a> and the <a href="#">Terms &amp; Conditions</a></li>
        </ul>
    </div>
    <h4 class="h5 bold text-right hr">Complete this Purchase?</h4>
    <p class="text-right br"><button class="alt " type="submit">Purchase</button></p>
<?php } ?>

	</form>