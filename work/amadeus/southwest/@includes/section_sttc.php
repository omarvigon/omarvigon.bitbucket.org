    <?php include("header_message_before.php");?>
    <h1 class="h1">Select Flights To Change</h1>
	<p class="br btr">Select the flight(s) you would like to change and then select Continue.</p>
    <h2 class="confcode float-right bold text-center">Confirmation # 1FW54S</h2>
    <h3 class="h5 bold"><span class="ico plane"></span> AIR</h3>  
    <?php
	include("table_passengers.php");
    include("table_itinerary.php");
	include("table_price.php");
    ?>
    <h4 class="hr h5 bold text-right">Search for new flight(s)</h4>
    <?php include("module_footer_continue.php");?>