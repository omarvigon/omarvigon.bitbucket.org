<?php
$page=substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
$page=explode(".",$page);
$page=$page[0];

$booking=(isset($_GET["booking"]) && $_GET["booking"]!="")?$_GET["booking"]:"revenue";
$revenue=(isset($_GET["revenue"]) && $_GET["revenue"]!="")?$_GET["revenue"]:"adult";
$login=(isset($_GET["login"]) && ($_GET["login"]=="true" || $_GET["login"]=="nonrr"));

define("LANG","en-US");

switch($page) {
	case "genr":
	case "logi":
		$flow = "common";
		break;
		
	case "ocup":
	case "occl":
	case "fare":
	case "purc":
	case "asst":
	case "conf":
		$flow = "booking";
		break;
		
	case "sttc":
	case "rsrch":
	case "rocup":
	case "rfare":
	case "reco":
	case "rconf":
		$flow = "exchange";
		break;

	case "sfup":
	case "ufare":
	case "ureco":
	case "uconf":
		$flow = "upgrade";
		break;

	case "refo":
	case "cconf":
		$flow = "cancel";
		break;

	case "steb":
	case "ebpurc":
	case "ebconf":
		$flow = "add-earlybird";
		break;
		
	case "pnrs":
	case "arconf":
		$flow = "add-rapid-reward";
		break;
		
	case "bkgd":
	case "uasst":
	case "uapis":
		$flow = "booking-details";
		break;
}
?>