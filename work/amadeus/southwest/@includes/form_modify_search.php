	<form action="#" method="post" class="box1 caption1 br">
		<fieldset class="wrap">
			<?php if($page!="rocup") { ?>
            <h2 class="float-left grid2 bold wdk-toggle-closed"><small class="ico"></small>Modify Search</h2>
            <label class="float-left bold"><input class="float-left" type="radio" name="radio_adv" checked="checked" value="rt" /> Round Trip</label> <label class="float-left bold midst"><input class="float-left" type="radio" name="radio_adv" value="ow" /> One-Way</label>
			<a class="float-right none" href="#">Additional Search Options</a>
            <?php } else { ?>
            <h2 class="float-left grid2 bold">Modify Search</h2>
            <?php } ?>
		</fieldset>
		<fieldset class="wrap br first">
			<label class="float-left"><b>From:</b> <a class="none float-right" href="#">View nearby airports</a><input class="block alt1" type="text" value="Dallas (Love Field), TX - DAL"<?php if(isset($_GET["rocup"]) && $_GET["rocup"]=="in") {?> disabled="disabled"<?php } ?> /></label>
			<label class="float-left midst"><b>To:</b> <a class="none float-right" href="#">View nearby airports</a><input class="block alt1" type="text" value="Cancun, MX - CUN"<?php if(isset($_GET["rocup"]) && $_GET["rocup"]=="out") {?> disabled="disabled"<?php } ?>/></label>
		<?php if($booking!="certificate-promotional" && $page!="rocup") { ?>
            <label class="float-left"><b>Return:</b> <input class="block alt1"  type="text" value="Return City or Airport Code" /></label>
			<em class="float-left hr none"><small class="ico add"></small><a href="#">Add Another Flight</a></em>
		<?php } else { ?>
            
            	<?php if(isset($_GET["rocup"])) { ?>
                	<?php if($_GET["rocup"]=="out") {?>
                    <label class="float-left"><b>Depart:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="06/28/2012" /></label>
                    <?php } else if($_GET["rocup"]=="in") {?>
                    <label class="float-left"><b>Return:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="06/28/2012" /></label>
                    <?php } else {?>
                    <label class="float-left"><b>Return:</b> <input class="block alt1"  type="text" value="Return City or Airport Code" /></label>
                    <?php }?>
                <?php } else {?>
                	<label class="float-left"><b>Depart:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="06/28/2012" /></label>	
                <?php } ?>

		<?php } ?>
		</fieldset>
       
        <?php if($page=="rocup") { ?>
        <fieldset class="wrap">
            <?php if(isset($_GET["rocup"]) && $_GET["rocup"]=="both") { ?>
            <label class="float-left"><b>Depart:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="06/28/2012" /></label>
			<label class="float-left midst"><b>Return:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="08/28/2012" /></label>
            <?php } ?>
        </fieldset>
        <?php } else { ?>
		<fieldset class="wrap br none">
			<label class="float-left"><b>Depart:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="06/28/2012" /></label>
			<label class="float-left midst"><b>Return:</b> <small class="ico calendar"></small> <input class="block alt3" type="text" value="08/28/2012" /></label>
			<label class="float-left"><b>Passengers:</b> <select class="block alt3"><option>1 Adult Ages 2+</option></select></label>			
			<label class="float-left midst">&nbsp; <select class="block alt3"><option>0 Senior Ages 65+</option></select></label>
			<label class="float-left"><b>Promo Code:</b> <a class="ico info" href="#"></a> <input class="block alt3" <?php if($booking!="revenue") {?>disabled="disabled"<?php } ?> type="text" value="Promo Code" /></label>
		</fieldset>
        <?php } ?>
        <button class="absolute alt" type="submit">Search</button>
	</form>