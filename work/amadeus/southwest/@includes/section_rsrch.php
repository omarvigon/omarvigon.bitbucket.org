    <h1 class="h1">Search for New Flights</h1>
	<p class="br btr">Make changes to your return trip by choosing your new return date and (if needed) the new return airport below.</p>    
    <?php 
	if( !isset($_GET["rsrch"]) || (isset($_GET["rsrch"]) && ($_GET["rsrch"]!="both" && $_GET["rsrch"]!="flown")))
		include("table_itinerary.php");
	?>
    
    <form action="#" method="post" class="rsrch-form">
        <div class="box4 caption9">
            <fieldset class="wrap indent">
                <div class="float-left rindent-small">
                    <label class="bold text-15">From:<input class="block input-large" type="text" value="Boston Logan, MA - BOS"/></label>
                </div>
                <div class="float-left rindent-small">
                    <label class="bold text-15">To:<input class="block input-large" type="text" value="Cancun, MX - CUN" /></label>
                </div>
                <?php if(isset($_GET["rsrch"]) && $_GET["rsrch"]=="both") { ?>
                <div class="float-left rindent-small">
                    <label class="bold text-15">Return:<input class="block input-large" type="text" value="Cancun, MX - CUN" /></label>
                </div>
                <?php } ?>
            </fieldset>
            <p class="indent"><a href="#">See Where We Fly</a></p>
            
            <?php if( (!isset($_GET["rsrch"])) || (isset($_GET["rsrch"]) && ( $_GET["rsrch"]=="both" || $_GET["rsrch"]=="out" )) ) { ?>
            <fieldset class="wrap indent">
                <div class="float-left rindent-small">
                    <label class="bold text-15">Depart: <small class="ico calendar"></small> <input class="block input-large" type="text" value="07/27/2012"/></label>
                </div>
                <?php if( (!isset($_GET["rsrch"])) || ( isset($_GET["rsrch"]) && $_GET["rsrch"]=="out") ) { ?>
                <p class="float-left grid2 indent3 br">Now accepting reservations through January 4, 2013.</p>
                <?php } ?>
            </fieldset>
            <?php } ?>
            
            <?php if( isset($_GET["rsrch"]) && $_GET["rsrch"]!="out" ) { ?>
            <fieldset class="wrap br indent">
                <div class="float-left rindent-small">
                    <label class="bold text-15">Return: <input class="block input-large" type="text" value="Boston Logan, MA - BOS"/></label>
                </div>
                <p class="float-left grid2 indent3 br">Now accepting reservations through January 4, 2013.</p>
            </fieldset>
            <?php } ?>
        </div>
        <div class="box4 caption9 text-right">
	        <button class="alt" type="submit">Select New Flight</button>
        </div>
    </form>