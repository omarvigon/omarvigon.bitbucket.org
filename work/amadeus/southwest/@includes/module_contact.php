	<h2 class="h3 br"><b>How Can We Get A Hold of You?</b> (only if we need to)</h2>
    <fieldset class="box2 indent3 contact-info br">
    	<legend><p>Let us help keep you updated regarding important flight updates <b>on day of travel</b> if it becomes necessary (your information will not be shared).</p></legend>
        <div class="wrap">
        	<div class="float-left grid5">
            	<label class="block indent"><?php if($page!="reco" && $page!="ureco") { ?><span class="em0">*</span><?php } ?>
                    <select class="grid5"><option>Select Method</option><option value="Text">Text Me</option><option value="Call">Call Me</option><option value="Email">Email Me</option></select>
                </label>
                <p class="text-contact">Southwest Airlines will notify you via text messaging. Standard text message rates apply.</p>
                <p class="phone-contact wdk-toggle wdk-toggle-closed" data-wdk-toggle-target="#callMeNote"><small class="ico"></small>Quiet Time observed for voice notifications.</p>
                <p class="wdk-toggle-closed" id="callMeNote">Southwest Airlines will not call between 10:00 p.m. and 8:00 a.m. local time. However, if a flight update occurs during the quiet time and is also within four (4) hours prior to scheduled departure, then a voice notification will be sent.</p>
            </div>
            <div class="float-right grid5">
                <div class="phone-contact">
                    <fieldset><b><span class="em0">*</span> Phone Number</b> (<input type="text" size="4" />) <input type="text" size="4" /> - <input type="text" size="4" /></fieldset>
                    <fieldset class="box1 alt hr">
                        <label>If outside the U.S.:<select id="out_us"><option>France(+33)</option></select></label>
                        <input type="text" size="14" />
                        <p class="br">Wireless Customer is responsible for any international wireless service charges related to calls, voicemail, roaming, and/or data usage (e.g., text, e-mail, and Internet use).  Contact your service provider about the specific utilization criteria and costs associated with your rate plan for international calling/data.</p>
                    </fieldset>
                </div>
                <div class="email-contact">
                    <label class="block"><b><span class="em0">*</span> Email</b> <input class="grid6" type="text" /></label>
                </div>
            </div>
        </div>
    </fieldset>