	<?php include("header_message_before.php");?>
    <h1 class="h1">Select Flights to Upgrade</h1>
    <div class="wrap">
	    <div class="absolute grid5">
    	    <h2 class="h4 em5 hr text-center">Upgrade to Business Select&trade;</h2>
        	<p class="hr">Good news! Your flights are available to upgrade.</p>
        	<p class="br">Book Business Select and be one of the first to board, get extra Rapid Rewards points, enjoy a premium drink on board and get access to <a href="#">Fly By Priority Security Lanes</a> where available.</p>
    	</div>
	    <img src="../@WDS_STATIC_FILES_PATH@/img/sfup.png" alt="Sign up for Alerts" />
    </div>
    
    <h2 class="confcode float-right bold text-center">Confirmation # 1FW54S</h2>
    <h3 class="h5 bold"><span class="ico plane"></span> AIR</h3>
    <?php
    include("table_passengers.php");
    include("table_itinerary.php");
	?>
    
    <h4 class="bold br">Please Note:</h4>
    <ul>
	    <li><small class="ico arrow2"></small>Upgrades apply to all passengers in the reservation. You will be able to review the price differences before completing the transaction.</li>
    </ul>
    <h4 class="h5 bold text-right hr">Continue to review price for upgrades</h4>
    <?php include("module_footer_continue.php");?>