	<h1 class="h1">Add/Edit Passport Information</h1>
	<p>All fields are required</p>
    <form action="#" method="post">
    	<h3 class="h5 hr"><small class="ico passportbig"></small> Passenger: <b>John Doe</b></h3>
        <fieldset class="box2">        
        	<?php include("form_uapis.php");?>
        </fieldset>
        
        <hr class="line2t hr" />
        
        <h3 class="h5 hr"><small class="ico passportbig"></small> Passenger: <b>Jane Lee Doe</b></h3>
        <fieldset class="box2">        
        	<?php include("form_uapis.php");?>
        </fieldset>
    	<?php include("module_footer_continue.php");?>
	</form>