    <div class="none">
		<?php if($booking=="redemption") { ?>
		<div class="teaser teaser-feature_bs">
            <h6 class="bold">Enjoy the premium benefits of Business Select&reg; Fare.</h6>
            <p class="em13"><b>Business Select Features:</b> &#10004; Priority Boarding &nbsp; &#10004; Fly By&reg; Security Lane &nbsp; &#10004; Fully Refundable &nbsp; &#10004; Premium Drink</p>
            <span class="ico close"></span>
		</div>
		<div class="teaser teaser-feature_wga">
            <h6 class="bold">Wanna Get Away? A great value fare plus two free checked bags.</h6>
            <p class="em13"><b>Wanna Get Away Features:</b> &#10004; Fully Refundable &nbsp; &#10004; Same-day Changes &nbsp; &#10004; 2 Free Checked Bags</p>
            <span class="ico close"></span>
		</div>
		<div class="teaser teaser-feature_anytime">
			<h6 class="bold">Our Anytime Fare gives you flexibility, anytime.</h6>
			<p class="em13"><b>Anytime Features:</b> &#10004; Fully Refundable &nbsp; &#10004; 2 Free Checked Bags</p>
			<span class="ico close"></span>
		</div>
		<?php } else { ?>
		<div class="teaser teaser-center teaser-feature_bs">
			<h5 class="float-left">Business Select<sup>&reg;</sup></h5>
			<p class="text-12 em13">Priority Boarding group A1-15 | <span class="bold em2">6,540</span> Rapid Rewards<sup>&reg;</sup> Points | Fly By<sup>&reg;</sup> Priority Lane | Premium Drink (day of travel) | Same-Day Changes | Fully Refundable</p>
			<span class="ico close"></span>
		</div>
		<div class="teaser teaser-center teaser-feature_wga">
			<h5 class="inline-block">Wanna Get Away</h5>
			<p class="inline-block text-12"><b class="em2">960</b> Rapid Rewards Points | 2 Free Checked Bags</p>
			<span class="ico close"></span>
		</div>
		<div class="teaser teaser-feature_anytime">
			<div class="upsell float-left em12 text-left">
            	<small class="ico arrow"></small>
            	<span class="em4 text-19 bold">$22</span> more and upgrade to <span class="em12 bold text-15">Business Select<sup>&reg;</sup></span>
                <small class="block"><b>Anytime Benefits</b> <span class="bold em2">PLUS</span> Priority Boarding | <span class="bold em4">1,310</span> <b>more</b> Rapid Rewards<sup>&reg;</sup> Points 
                | Fly By<sup>&reg;</sup> Priority Lane | Free Premium Drink (on day of travel)</small>
            </div>
            <h5>Anytime</h5>
			<p class="text-12 em2 bold">5,230 Rapid Rewards<sup>&reg;</sup> Points</p>
            <p class="em13">Fully refundable | Same-Day Changes</p>
            <span class="ico close"></span>
		</div>
        <div class="teaser teaser-center teaser-feature_senior">
            <h5 class="inline-block">Senior</h5>
            <p class="inline-block text-12"><span class="em2 bold">1,750</span> Rapid Rewards<sup>&reg;</sup> Points</p>
            <p class="inline-block text-13 em13">Fully Refundable | Same-Day Changes</p>
        	<span class="ico close"></span>
		</div>
        <div class="teaser teaser-center teaser-feature_ding">
            <h5 class="inline-block">DING</h5>
            <p class="inline-block text-12"><span class="em2 bold">750</span> Rapid Rewards<sup>&reg;</sup> Points</p>
            <p class="inline-block text-13 em13">Fully Refundable | Same-Day Changes</p>
        	<span class="ico close"></span>
		</div>
		<?php } ?>
	</div>