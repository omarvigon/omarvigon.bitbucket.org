	<div class="head">
        <div class="caption3">
            <h2 class="bold">Quick Air Links</h2>
            <a class="block" href="#"><small class="float-left slope s2"></small> Check In</a>
            <a class="block" href="#"><small class="float-left slope s2"></small> Change Flight</a>
            <a class="block" href="#"><small class="float-left slope s2"></small> Check Flight Status</a>
        </div>        
<?php if($login) { ?>
		<div>
        	<a class="float-right" href="#">Logout</a>
            <h2><b class="h5">Hello,</b> <b class="block text-16">Mary</b></h2>
            <?php if($_GET["login"]!="nonrr") { ?>
            <h4 class="h6 bold em10">Rapid Rewards Member</h4>
            <?php } ?>        
            <p class="btr"><a class="block" href="#">My Account</a><?php if($_GET["login"]!="nonrr") { ?>R.R. #20110124576<?php } ?> </p>
		</div>
	</div>
	<?php
    $noaside="";
	if($_GET["login"]!="nonrr") {
	 	if($flow=="cancel" || $flow=="upgrade" || $flow=="add-rapid-reward" || $flow=="add-earlybird" || $page=="bkgd" || $page=="uapis" || $page=="sttc" || $page=="rsrch" || $page=="rocup" || $page=="rfare")
			$noaside=" last";
	?>
    <div class="article first points">
    	<h3 class="text-right<?php echo $noaside?>"><var class="float-left h1">99 000 250</var> <span class="em11">Available Pts</span> <a class="block" href="#">Details</a></h3>
    </div>
    <?php }?>
        
<?php } else { ?>
        <div>
            <a class="float-right" href="#">Enroll now!</a>
            <h2 class="h6">Account Login</h2>
            <p class="bold em0"><small class="slope s2 absolute"></small> Please enter your Username or Account Number.</p>
            <p class="bold em0"><small class="slope s2 absolute"></small> Please enter your Password.</p>
            <form action="#" method="post">
                <input class="em10 small grid9" type="text" value="Account Number or Username" />
                <input class="em10 small grid9" type="text" value="Password (Case Sensitive)" />
                <p class="text-right"><label class="float-left em1"><input type="checkbox" /> Remember me</label><button>Log In</button></p>
            </form>
            <a class="block btr" href="#">Need help logging in?</a>
		</div>
	</div>
<?php } ?>