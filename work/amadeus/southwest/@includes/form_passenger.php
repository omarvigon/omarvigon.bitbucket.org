	<fieldset class="float-left grid8">
        <legend>First, Middle, and Last Name must match government-issued photo identification.</legend>
        <fieldset class="wrap br">
            <label class="float-left rindent-small bold block">First Name <span class="em0">*</span><input class="block input-large" type="text" /></label>
            <label class="float-left rindent-small bold block">Middle Name <a class="ico info" href="#"></a><input class="block" type="text" /></label>
            <label class="float-left rindent-small bold block">Last Name <span class="em0">*</span><input class="block input-large" type="text" /></label>
            <label class="float-left rindent-small bold block">Suffix 
                <select class="block">
                    <option selected="selected" value=""></option>
                    <option value="CEO">CEO</option>
                    <option value="CLU">CLU</option>
                    <option value="CPA">CPA</option>
                    <option value="DC">DC</option>
                    <option value="DDS">DDS</option>
                    <option value="DO">DO</option>
                    <option value="DPM">DPM</option>
                    <option value="DVM">DVM</option>
                    <option value="I">I</option>
                    <option value="II">II</option>
                    <option value="III">III</option>
                    <option value="IV">IV</option>
                    <option value="JR">JR</option>
                    <option value="MD">MD</option>
                    <option value="OD">OD</option>
                    <option value="PHD">PHD</option>
                    <option value="PharmD">PharmD</option>
                    <option value="RN">RN</option>
                    <option value="SR">SR</option>
                    <option value="V">V</option>
                    <option value="VI">VI</option>
                </select>
            </label>
            <label class="float-left rindent-small bold block">Rapid Rewards Account #<input class="block input-large" type="text" /></label>
            <button type="submit" class="float-left link indent hr"><small class="ico disability"></small> Add/Edit Disability Assistance Options</button>
        </fieldset>
        <h4 class="bold br">Selected Disability Assistance Options:</h4>
        <ul class="default alt">
            <li>Can walk but need assistance to and from gate.</li>
            <li>Powered wheel chair with spillable batteries (2)</li>
        </ul>
        <fieldset class="box3 br">
        	<legend class="h5 bold">Required TSA Secure Flight Information <span class="em0">*</span></legend>
            <div class="wrap">
            	<label class="float-left bold">Date of Birth<span class="em2">*</span><a class="ico info" href="#"></a>
                	<select class="block">
                        <option>Select Month</option>
                        <option>1 - January</option>
                        <option>2 - February</option>
                        <option>3 - March</option>
                        <option>4 - April</option>
                        <option>5 - May</option>
                        <option>6 - June</option>
                        <option>7 - July</option>
                        <option>8 - August</option>
                        <option>9 - September</option>
                        <option>10 - October</option>
                        <option>11 - November</option>
                        <option>12 - December</option>
                    </select>
                 </label>
                <label class="float-left bold indent-small">&nbsp; <select class="block"><option>Select Day</option></select></label>
                <label class="float-left bold indent-small">&nbsp; <select class="block"><option>Select Year</option></select></label>
                <label class="float-left bold indent-small">Gender<span class="em2">*</span><a class="ico info" href="#"></a><select class="block"><option></option><option>Male</option><option>Female</option></select></label>
                <label class="float-left bold indent-small"><small class="ico add"></small>Redress # (optional) <a class="ico info" href="#"></a><input class="block input-large" type="text" /></label>
            </div>
            <p><span class="em3">*</span> The TSA Secure Flight program requires Southwest Airlines to collect this information for each passenger. <a href="#">Get Details.</a></p>
        </fieldset>
        
        <fieldset class="box2 br">
        	<legend class="h5 bold wdk-toggle wdk-toggle-opened" data-wdk-toggle-target="#apisFields<?php echo $passengerNumber; ?>"><small class="ico"></small>Enter passport information</legend>
            <small class="ico passportbig absolute"></small>
			<p>This information may also be entered in View/Share itinerary and upon check-in for your flight</p>
            
            <div id="apisFields<?php echo $passengerNumber ?>" class="wdk-toggle-opened">
            	<?php include("form_uapis.php");?>
                <p><b>Note:</b> Please make sure the name on your passport matches the name entered above.</p>
            </div>
        </fieldset>
    </fieldset>