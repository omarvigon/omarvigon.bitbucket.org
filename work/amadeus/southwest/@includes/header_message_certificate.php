	<?php if($booking=="certificate-promotional") { ?>
    <div class="noerror box4 btr">
		<span class="ico done3 absolute"></span>
		<h3 class="h4 bold em3">Feel rewarded with Southwest Airlines. Book your flight below.</h3>
		<p>Promotion Certificates cannot be combined with standard fares. Not valid for travel on 11/25/2009, 11/29/2009, 12/23/2009, 02/14/2010, 03/01/2010, 04/02/2010, 07/02/2010, 07/05/2010, 03/16/2014, 03/23/2014, 03/30/2014, 04/27/2014, 05/23/2014, 06/27/2014, 07/06/2014, 10/19/2014, 11/26/2014, 11/29/2014, 11/30/2014, 12/01/2014, 12/20/2014, 12/27/2014 and 12/28/2014. Expires on 07/01/2013.</p>
	</div>
	<?php } ?>
	<?php if($booking=="certificate-transitional") { ?>
    <div class="noerror box4 btr">
		<span class="ico done3 absolute"></span>
		<h3 class="h4 bold em3">Feel rewarded with Southwest Airlines. Book your flight below.</h3>
		<p>Standard Awards and Freedom Awards cannot be combined with tickets purchased with dollars. See the blackout dates as listed in the <a href="#">Terms and conditions</a>. Expires on 07/01/2013.</p>
	</div>
	<?php } ?>