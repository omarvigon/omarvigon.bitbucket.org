<div class="none error_messages"> 
	<div class="error box4">
		<span class="ico oops float-left"></span>
		<ul class="em0 box4">
			<li>Sorry but your session has timed out (3002)</li>
		</ul>
	</div>
    <div class="error box4 hr btr">
		<span class="ico oops float-left"></span>
		<ul class="em0 box4">
            <li>No flights were found for your selected date (Fri 23 Nov). Please select a different date in the outbound calendar. (4580-1)</li>
		</ul>
	</div>
</div>