    <fieldset class="wdk-payment">
	<?php if(isset($_GET["purc"]) && $_GET["purc"]=="even_exchange") { ?>
		<label class="block br"><b class="inline-block grid3 text-right"><span class="em0">*</span> First Name</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> Last Name</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> Billing Street Address</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right">&nbsp;</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> City</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> State</b> 
				<select class="indent-small">
					<option>Select State</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AS">American Somoa</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="AA">Armed Forces Americas (except Canada)</option>                
				</select>
			</label>
			<fieldset><b class="inline-block grid3 text-right"><span class="em0">*</span> Zip Code</b> <input type="text" maxlength="5" size="5" class="indent-small" />  - <input type="text" maxlength="4" size="5" /></label>
			<label class="block brsmall"><b class="inline-block grid3 text-right"><span class="em0">*</span> Country</b> <select class="indent-small wdk-passenger-country"><option value="MX">MEXICO - (MX)</option><option value="US">UNITED STATES OF AMERICA - (US)</option></select></label>
			<fieldset class="none"><b class="inline-block grid3 text-right"><span class="em0">*</span> Billing Phone Number</b> <span class="indent-small">(<input type="text" size="4" />)</span> <input type="text" size="4" /> - <input type="text" size="4" /></fieldset>
			<fieldset class="wdk-foreign-country"><b class="inline-block grid3 text-right"><span class="em0">*</span> Billing Phone Number</b>
				<select class="indent-small"><option value="CA">Mexico (+1)</option></select>
				<input type="text" />
			</fieldset>
				
			<p class="brsmall"><b class="inline-block grid3 text-right"><span class="em0">*</span> Address Type</b> <label class="indent-small"><input type="radio" name="radio_5" checked="checked" />Home</label> <label class="indent-small"><input type="radio" name="radio_5" />Business</label> <label class="indent-small"><input type="radio" name="radio_5" />Other</label></p>
			
			<fieldset class="box1 alt br">
				<legend>If outside the U.S.:</legend>
				<label class="block"><b class="inline-block grid3 text-right">State/Province/Region</b> <input type="text" class="indent-small" /></label>
				<label class="block"><b class="inline-block grid3 text-right">Postal Code</b> <input type="text" size="6" class="indent-small" /></label>
			</fieldset>
		<?php } else { ?>
		    <fieldset class="box2 wdk-card-payment">
				<label class="block"><em class="inline-block grid3 text-right"><span class="em0">*</span> Card Type</em> 
					<select class="indent-small">
						<option>Select Your Card</option>
						<option>American Express</option>
					</select>
				</label>
				<fieldset><em class="inline-block grid3 text-right"><span class="em0">*</span> Card Number</em> <input class="indent-small" type="text" size="4" maxlength="4" /> - <input type="text" size="4" maxlength="4" /> - <input type="text" size="4" maxlength="4" /> - <input type="text" size="4" maxlength="4" /></fieldset>
				<fieldset class="brsmall"><em class="inline-block grid3 text-right"><span class="em0">*</span> Expiration Date</em> 
					<select class="indent-small">
						<option>Select Month</option><option>January</option><option>December</option>
					</select> 
					<select>
						<option>Select Year</option>
					</select>
				</fieldset>
				<label class="block brsmall"><em class="inline-block grid3 text-right"><span class="em0">*</span> Security Code<a class="ico info" href="#"></a> </em> <input type="text" size="6" class="indent-small" /></label>
			</fieldset>

			<label class="block br"><b class="inline-block grid3 text-right"><span class="em0">*</span> First Name</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> Last Name</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> Billing Street Address</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right">&nbsp;</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> City</b> <input type="text" size="35" class="indent-small" /></label>
			<label class="block"><b class="inline-block grid3 text-right"><span class="em0">*</span> State</b> 
				<select class="indent-small">
					<option>Select State</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AS">American Somoa</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="AA">Armed Forces Americas (except Canada)</option>                
				</select>
			</label>
			<fieldset><b class="inline-block grid3 text-right"><span class="em0">*</span> Zip Code</b> <input type="text" maxlength="5" size="5" class="indent-small" />  - <input type="text" maxlength="4" size="5" /></fieldset>
			<label class="block brsmall"><b class="inline-block grid3 text-right"><span class="em0">*</span> Country</b> <select class="indent-small wdk-passenger-country"><option value="MX">MEXICO - (MX)</option><option value="US">UNITED STATES OF AMERICA - (US)</option></select></label>
			<fieldset class="none"><b class="inline-block grid3 text-right"><span class="em0">*</span> Billing Phone Number</b> <span class="indent-small">(<input type="text" size="4" />)</span> <input type="text" size="4" /> - <input type="text" size="4" /></fieldset>
			<fieldset class="wdk-foreign-country"><b class="inline-block grid3 text-right"><span class="em0">*</span> Billing Phone Number</b>
				<select class="indent-small"><option value="CA">Mexico (+1)</option></select>
				<input type="text" />
			</fieldset>
				
			<p class="brsmall"><b class="inline-block grid3 text-right"><span class="em0">*</span> Address Type</b> <label class="indent-small"><input type="radio" name="radio_5" checked="checked" />Home</label> <label class="indent-small"><input type="radio" name="radio_5" />Business</label> <label class="indent-small"><input type="radio" name="radio_5" />Other</label></p>
			
			<fieldset class="box1 alt br">
				<legend>If outside the U.S.:</legend>
				<label class="block"><b class="inline-block grid3 text-right">State/Province/Region</b> <input type="text" class="indent-small" /></label>
				<label class="block"><b class="inline-block grid3 text-right">Postal Code</b> <input type="text" size="6" class="indent-small" /></label>
			</fieldset>
		<?php } ?>
	</fieldset>