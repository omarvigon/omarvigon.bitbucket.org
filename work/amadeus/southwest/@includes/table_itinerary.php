	<table class="fare line1t<?php if($page=="ebconf") { ?> br<?php }?><?php if($page=="rocup") { ?> btr<?php }?>">
    <caption class="bold">
		<?php if($page=="fare"){ ?>ITINERARY<?php } ?>
    	<?php if($page=="steb" || $page=="ebconf"){ ?>EARLYBIRD CHECK-IN<?php } ?>
        <?php if($page=="rocup" || $page=="rsrch"){ ?>YOUR UNCHANGED <?php if(isset($_GET["rsrch"]) && $_GET["rsrch"]=="in") {?>DEPARTING<?php }else{?>RETURN<?php } ?> TRIP<?php } ?>
    	<?php if($page=="ufare" || $page=="sfup"){ ?>AIR ITINERARY<?php } ?>
    </caption>
    
    <?php if($page=="steb" || $page=="ebconf") { ?>
	<tfoot>
    <tr class="text-right">
    	<?php if($page=="steb") { ?>
        	<td class="box2 bold vertical-middle"><span class="float-left">Total:</span>$10</td>
        <?php } else { ?>
        	<td>&nbsp;</td>
        <?php } ?>
        <td colspan="4"><span class="ico texteb"></span> = <a href="#">EarlyBird Check-In</a> <span class="ico textb"></span> = <a href="#">Business Select</a> <span class="ico texta"></span> = <a href="#">A-List</a></td>
    </tr>
    </tfoot>
	<?php } ?>
    
    <?php if($page=="rocup" || ( isset($_GET["rsrch"]) && ($_GET["rsrch"]=="out" || $_GET["rsrch"]=="both" || $_GET["rsrch"]=="flown" ) ) || ($page=="rsrch" && !isset($_GET["rsrch"]))) { ?>
    
    <?php } else { ?>
    <tr>
    	<?php if($page=="steb" || $page=="ebconf"){ ?>
        <td class="box2 line1 line3 grid2">
            <p><span class="ico textb text-align"></span> JOHN DOE</p>
            <?php if($booking!="companion") {?>
            <p><span class="ico textb text-align"></span> JANE DOE</p>
            <p><span class="ico textb text-align"></span> JIM DOE</p>
            <?php }?>
        </td>
		<?php } ?>
        <?php if($page=="sfup"){ ?>
        <td class="line1 line3 grid1 text-center">
            <small class="block em3">Upgrade?</small><input type="checkbox" />
        </td>
		<?php } ?>
        <?php if($page=="sttc"){ ?>
        <td class="box2 line1 line3 text-center"><input type="checkbox" /></td>
		<?php } ?>
    	<td class="line3 line1 text-center vertical-top"><h2 class="em8">DEPART</h2><h3 class="date"><em class="block small">OCT 1</em> <var class="block h5">FRI</var></h3></td>
        <td class="line3 line1 grid5">
        	<div class="wrap">
            	<b class="float-left grid2">07:35 AM</b>
                <span class="float-left grid7"><b>Dallas (Love Field)</b>, TX (DAL) to</span>
            </div>
        	<div class="wrap">
            	<b class="float-left grid2">18:55 PM</b>
                <span class="float-left grid7"><b>Cancun</b>, MX (CUN)</span>
			</div>                
		</td>
        <td class="line3 line1 text-center"><b class="em10">Flight</b> <b class="block">#1184</b></td>
        <td class="line1 grid4"><b>Monday, October 1, 2012</b><span class="em10 block">Travel Time 5 h 15 m</span> (Nonstop) 
			<?php if($booking=="companion") { ?>
                <a class="block" href="#">Companion</a>
            <?php } else if($booking=="certificate-promotional") { ?>
                <a class="block" href="#">Promotional Award</a>
            <?php } else if($booking=="certificate-transitional") { ?>    
                <a class="block" href="#">Standard Award</a>
            <?php } else { ?>
                <a class="block" href="#">Business Select</a>
            <?php } ?>
		</td>	
    </tr>
    <? } ?>
    
    <?php if(isset($_GET["rsrch"]) && ($_GET["rsrch"]=="in" || $_GET["rsrch"]=="both" || $_GET["rsrch"]=="flown")) { ?>
    	
    <?php } else { ?>
    <tr class="alt3">
   	 	<?php if($page=="steb" || $page=="ebconf"){ ?>
        <td class="box2 line1 line3" rowspan="2">
           <?php if($page=="steb") { ?>
           <p><small class="float-right">$10</small><input type="checkbox" /> JOHN DOE</p>
           <?php } else { ?>
           <p><small class="float-right">$10</small><span class="ico texteb text-align"></span>JOHN DOE</p>
           <?php } ?>
           
           <?php if($booking!="companion") {?>
           <p><span class="ico texta text-align"></span> JANE DOE</p>
           <p><small class="float-right">$10</small> <span class="ico texteb text-align"></span> JIM DOE </p>
           <?php }?>
        </td>
		<?php } ?>
        <?php if($page=="sfup"){ ?>
        <td class="line3 line1 text-center" rowspan="2">
            <small>Upgrade Unavailable</small>
        </td>
		<?php } ?>
        <?php if($page=="sttc"){ ?>
        <td class="box2 line1 line3 text-center" rowspan="2"><input type="checkbox" /></td>
		<?php } ?>
    	<td class="line3 line1 text-center vertical-top" rowspan="2"><h2 class="em8">RETURN</h2><h3 class="date"><em class="block small">OCT 16</em> <var class="block h5">MON</var></h3></td>
        <td class="line2 line3">
        	<div class="wrap">
            	<b class="float-left grid2">07:00 AM</b>
                <div class="float-left grid7">
                	<b>Cancun</b>, MX (CUN) to
                    <dl class="indent em10">
                    	<dt class="absolute"><i>Stops:</i></dt>
                    	<dd><i>Houston, TX</i></dd>
                    	<dd><i>Dallas, TX</i></dd>
                	</dl>
				</div>
            </div>    
        	<div class="wrap">
        		<em class="em10 float-left grid2">08:05 AM</em>
                <em class="em10 float-left grid7">Arrive in Atlanta, GA (ATL)</em>
			</div>                
		</td>
        <td class="line2 line3 text-center"><b class="em10">Flight</b> <b class="block">#1352</b></td>
        <td class="line1 grid4" rowspan="2"><?php if ($page=="bkgd"){ ?><p class="caption11 em0 bold text-center10">Flight Information Alert</p><?php } ?><b>Tuesday, October 16, 2012</b><span class="em10 block">Travel Time 4 h 10 m</span> (2 stops, includes 1 plane change)
			<?php if($booking=="companion") { ?>
                <a class="block" href="#">Companion</a>
            <?php } else if($booking=="certificate-promotional") { ?>
                <a class="block" href="#">Promotional Award</a>
            <?php } else if($booking=="certificate-transitional") { ?>    
                <a class="block" href="#">Standard Award</a>
            <?php } else { ?>
                <a class="block" href="#">Wanna Get Away</a>
            <?php } ?>
		</td>
    </tr>
    <tr class="alt3">
    	<td class="line1 line3">
        	<div class="wrap">
	        	<em class="em10 float-left grid2">08:45 AM</em>
               	<em class="em10 float-left grid7">Change <span class="ico change"></span> in Atlanta, GA (ATL)</em>
        	</div>
            <div class="wrap">
            	<span class="float-left grid2"><b>10:10 AM</b> <small class="block">(overnight)</small></span>
                <span class="float-left grid7"><b>Dallas (Love Field)</b>, TX (DAL)</span>
            </div>
		</td>
        <td class="line1 line3 text-center"><b class="em10">Flight</b> <b class="block">#682</b></td>
    </tr>
    <?php } ?>
    </table>