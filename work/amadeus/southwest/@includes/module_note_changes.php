	<h4 class="h6 hr bold em0"><span class="ico red2"></span> Please Note:</h4>
    <div class="indent2 br">
	    <p>By clicking "I Want To Make These Changes", you acknowledge that you agree:</p>
    	<ul>
        <li><small class="ico arrow2"></small>Exchanged Ticket points have been returned to the member's account.</li>
        <li><small class="ico arrow2"></small>All nonrefundable funds applied toward the purchase of a new reservation remain nonrefundable.</li>
	    <li><small class="ico arrow2"></small>Expiration dates of applied funds may result in new ticket(s) expiring earlier than the 12 months from date of purchase. The new expiration date will be printed on the confirmation receipt sent to you.</li>
        <li><small class="ico arrow2"></small>Modifying a fully refundable, international fare will result in the fare becoming NON-refundable. Non-refundable fares follow normal re-usable rules.</li>        
        </ul>
    </div>
    <h4 class="h5 bold text-right hr">Complete this Purchase?</h4>
    <p class="text-right br"><button class="alt " type="submit">I Want To Make These Changes</button></p>