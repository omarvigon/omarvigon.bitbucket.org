	<?php
    include("header_message_certificate.php");
    if($page=="rocup")
	    include("table_itinerary.php");
    include("header_messages.php");
	?>
    
	<h1 class="h1">Select Departing Flight:</h1>
	<h2 class="h2">Dallas (Love Field), TX to Cancun, MX</h2>
	<?php
	include("form_modify_search.php");
    include("module_bags_free.php");
    include("module_try_calendar.php");
	?>
	<ul class="date text-center">
		<li class="float-left unselect"><a href="#"><em class="block small">MAY</em> <span class="box3 block"><var class="block">20</var> <span class="small">SUN</span></span></a></li>
		<li class="float-left unselect"><a href="#"><em class="block small">MAY</em> <span class="box3 block"><var class="block">21</var> <span class="small">MON</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 22"><em class="block small">MAY</em> <span class="box3 block"><var class="block">22</var> <span class="small">TUE</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 23"><em class="block small">MAY</em> <span class="box3 block"><var class="block">23</var> <span class="small">WED</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 24"><em class="block small">MAY</em> <span class="box3 block"><var class="block">24</var> <span class="small">THU</span></span></a></li>
		<li class="float-left select"><em class="block h5">MAY</em> <var class="block">25</var> <span>FRI</span></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 26"><em class="block small">MAY</em> <span class="box3 block"><var class="block">26</var> <span class="small">SAT</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 27"><em class="block small">MAY</em> <span class="box3 block"><var class="block">27</var> <span class="small">SUN</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 28"><em class="block small">MAY</em> <span class="box3 block"><var class="block">28</var> <span class="small">MON</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 29"><em class="block small">MAY</em> <span class="box3 block"><var class="block">29</var> <span class="small">TUE</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Depart trip date to MAY 30"><em class="block small">MAY</em> <span class="box3 block"><var class="block">30</var> <span class="small">WED</span></span></a></li>
	</ul>
	
	<?php 
	$i=1;
	include("module_customize.php");
	?>
	
	<table class="bound text-center br">
	<?php include("module_ocup_caption.php");?>
	<thead>
	<tr>
		<?php include("table_bound_head.php");?>
        
<?php if($booking=="certificate-promotional") { ?>
		<td class="caption2 bold">Promotional Award</td>
<?php } else if($booking=="certificate-transitional") {?>
		<?php if($page!="rocup") {?>
		<td class="caption2 bold">Standard Award</td>
        <?php }?>
		<td class="caption2 bold">Freedom Award</td>
<?php } else { ?>      
		<td class="caption2 bold" data-family="feature_bs"><a class="block" href="#">Business Select</a></td>
		<td class="caption2 bold" data-family="feature_anytime"><a class="block" href="#">Anytime</a></td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="caption2 bold" data-family="feature_senior"><a class="block" href="#">Senior</a></td>
		<?php } ?>
		<td class="caption2 bold" data-family="feature_wga"><a class="block" href="#">Wanna Get Away</a></td>
<?php } ?>
		<?php if(isset($_GET["5columns"])) { ?>
		<td class="caption2 bold" data-family="feature_ding"><a class="block" href="#">DING</a></td>
		<?php } ?>
	</tr>
    <?php if($booking!="certificate-promotional" && $booking!="certificate-transitional") {?>
    <tr>
    	<td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") {?>58,320 Pts<?php } else {?>$574<?php }?></em></td>
        <td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") {?>46,400 Pts<?php } else {?>$546<?php }?></em></td>
        <?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") {?>19,920 Pts<?php } else {?>$450<?php }?></em></td>
		<?php } ?>
        <td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") {?>from 20,650 Pts<?php } else {?>$410 - $425<?php }?></em></td>
        <?php if(isset($_GET["5columns"])) { ?>
		<td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") {?>from 650 Pts<?php } else {?>$99<?php }?></em></td>
		<?php } ?>
    </tr>
    <?php } ?>
	</thead>
	<tbody>
<?php if($revenue!="senior2") { ?>
	<tr>
		<td class="h6 h8"><var class="text-12">6:25</var> <small class="em10">AM</small></td>
		<td class="h6 h8"><var class="text-12">5:20</var> <small class="em10">PM</small></td>
		<td><a href="../popins/flif.php" target="_blank" class="wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1739/2118</a></td>
		<td class="h8 noline"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>Change Planes<br />ATL</td>
		<td><small>7:55</small></td>
        
<?php if($booking=="certificate-promotional") { ?>
       <td class="em11 bold">Sold out</td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td class="em11 bold">Sold out</td>
       <?php }?>
       <td class="em11 bold">Sold out</td>
<?php } else { ?>         
        <td class="em11 bold" data-family="feature_bs">Sold out</td>
		<td class="em11 bold" data-family="feature_anytime">Sold out</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="em11 bold" data-family="feature_senior">Sold out</td>
		<?php } ?>
		<td class="em11 bold" data-family="feature_wga">Sold out</td>
        <?php if(isset($_GET["5columns"])) { ?>
		<td class="em11 bold" data-family="feature_wga">Sold out</td>
		<?php } ?>
<?php } ?>        
	</tr>
	<tr>
		<td class="h6 h8"><var class="text-12">6:50</var> <small class="em10">AM</small></td>
		<td class="h6 h8"><var class="text-12">4:35</var> <small class="em10">PM</small></td>
		<td><a href="../popins/flif.php" target="_blank" class="wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">2671/2008</a></td>
		<td class="h8 noline"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>Change Planes<br />ATL</td>
		<td><small>6:45</small></td>
		
<?php if($booking=="certificate-promotional") { ?>
       	<td class="em11 bold">Sold out</td>
<?php } else if($booking=="certificate-transitional") {?>
       	<?php if($page!="rocup") {?>
        <td class="em11 bold">Sold out</td>
        <?php } ?>
       	<td class="em11 bold">Sold out</td>
<?php } else { ?>         
        <td class="em11 bold" data-family="feature_bs">Sold out</td>
		<td class="em11 bold" data-family="feature_anytime">Sold out</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="em11 bold" data-family="feature_senior">Sold out</td>
		<?php } ?>
		<td class="em11 bold" data-family="feature_wga">Sold out</td>
<?php } ?>
		<?php if(isset($_GET["5columns"])) { ?>
		<td class="em11 bold" data-family="feature_wga">Sold out</td>
		<?php } ?>
	</tr>
	<tr>
		<td class="h6 h8"><var class="text-12">7:55</var> <small class="em10">PM</small></td>
		<td class="h6 h8"><var class="text-12">6:10</var> <small class="em10">PM</small></td>
		<td><a class="wdk-tooltip" href="../popins/flif.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">692</a></td>
		<td><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">Nonstop</a></td>
		<td><small>7:15</small></td>

<?php if($booking=="certificate-promotional") { ?>
       <td><label class="h8 bold midst"><input type="radio" name="radio_2" /> Promotional Award</label><a href="#" class="block var small">+$22.80</a></td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
       <?php }?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
<?php } else { ?> 		
        <td class="price" data-family="feature_bs" title="Departing Flight 692 $574 &#13; 7:55PM depart 6:10PM arrive &#13; Nonstop">
        	<div><?php if($booking=="revenue" && $page=="ocup"){?><small class="ico promoribbon" title="Promo Rate Applied"></small><?php }?> <label class="bold"><input type="radio" name="radio_2" /><var class="h5">
            <?php if($booking=="redemption") { ?>
            58,320 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>574</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">4,260 pts</strong></small>
            <?php } ?>
			</div>
		</td>
		<td class="price" data-family="feature_anytime" title="Departing Flight 692 $546 &#13; 7:55PM depart 6:10PM arrive &#13; Nonstop">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            46,400 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>546</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">4,260 pts</strong></small>
            <?php } ?>
            </div>
        </td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_senior" title="Departing Flight 692 $450 &#13; 7:55PM depart 6:10PM arrive &#13; Nonstop">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            19,920 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>450</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
		<td class="price" data-family="feature_wga" title="Departing Flight 692 $425 &#13; 7:55PM depart 6:10PM arrive &#13; Nonstop">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            20,650 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>425</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
        <?php if(isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_ding" title="Departing Flight 692 $99 &#13; 7:55PM depart 6:10PM arrive &#13; Nonstop">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            650 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>99</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
<?php } ?>            
	</tr>
	<tr>
		<td class="h6 h8"><var class="text-12">1:45</var> <small class="em10">PM</small></td>
		<td class="h6 h8"><var class="text-12">11:50</var> <small class="em10">PM</small></td>
		<td><a href="../popins/flif.php" target="_blank" class="wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">196</a></td>
		<td class="h8"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>No Plane Change</td>
		<td><small>7:05</small></td>

<?php if($booking=="certificate-promotional") { ?>
      <td><label class="h8 bold midst"><input type="radio" name="radio_2" /> Promotional Award</label><a href="#" class="block var small">+$22.80</a></td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
       <?php } ?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
<?php } else { ?> 		
        <td class="price" data-family="feature_bs" title="Departing Flight 196 $574 &#13; 1:45PM depart 11:50PM arrive &#13; 1 Stop No Plane Change">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            58,320 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>574</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<td class="price" data-family="feature_anytime" title="Departing Flight 196 $546 &#13; 1:45PM depart 11:50PM arrive &#13; 1 Stop No Plane Change">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            46,400 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>546</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_senior" title="Departing Flight 196 $450 &#13; 1:45PM depart 11:50PM arrive &#13; 1 Stop No Plane Change">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            23,652 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>450</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
		<td class="price" data-family="feature_wga" title="Departing Flight 196 $410 &#13; 1:45PM depart 11:50PM arrive &#13; 1 Stop No Plane Change">
        	<div><small class="absolute em10">Web Only!</small><?php if($booking=="revenue" && $page=="ocup"){?><small class="ico promoribbon" title="Promo Rate Applied"></small><?php }?><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            23,652 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>410</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
        <?php if(isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_ding" title="Departing Flight 196 $99 &#13; 1:45PM depart 11:50PM arrive &#13; 1 Stop No Plane Change">
        	<div><small class="absolute em10">Web Only!</small><?php if($booking=="revenue" && $page=="ocup"){?><small class="ico promoribbon" title="Promo Rate Applied"></small><?php }?><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            652 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>99</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
<?php } ?>            
	</tr>
<?php } ?> 
	<tr>
		<td class="h6 h8"><var class="text-12">10:10</var> <small class="em10">PM</small></td>
		<td class="h6 h8"><var class="text-12">7:55</var> <small class="em10">AM</small><small class="sub">(overnight)</small></td>
		<td><a href="../popins/flif.php" target="_blank" class="wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">394/223</a></td>
		<td class="h8"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>No Plane Change</td>
		<td><small>6:45</small></td>

<?php if($booking=="certificate-promotional") { ?>
       <td class="em11 bold">Sold out</td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
       <?php }?>
       <td class="em11 bold">Unavailable</td>
<?php } else { ?> 
		<td class="price" data-family="feature_bs" title="Departing Flight 394/223 $574 &#13; 10:10PM depart 7:55AM arrive &#13; 1 Stop No Plane Change">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            58,320 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>574</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">4,260 pts</strong></small>
            <?php } ?>
            </div>
        </td>
		<td class="price" data-family="feature_anytime" title="Departing Flight 394/223 $546 &#13; 10:10PM depart 7:55AM arrive &#13; 1 Stop No Plane Change">
        	<div><small class="absolute em10">Web Only!</small><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            46,400 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>546</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">4,260 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_senior" title="Departing Flight 394/223 $450 &#13; 10:10PM depart 7:55AM arrive &#13; 1 Stop No Plane Change">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            19,920</var></label>
            <?php } else { ?>
            <sup>$</sup>450</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
		<td class="em11 bold" data-family="feature_wga">Unavailable</td>
        <?php if(isset($_GET["5columns"])) { ?>
		<td class="em11 bold" data-family="feature_wga">Unavailable</td>
		<?php } ?>
<?php } ?>        
	</tr>
	</tbody>
	</table>
	<h1 class="h1 br">Select Returning Flight:</h1>
	<h2 class="h2">Cancun, MX to Dallas (Love Field), TX</h2>
	<?php
    include("module_bags_free.php");
	include("module_try_calendar.php");
	?>
    <ul class="date text-center">
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 7"><em class="block small">JUN</em> <span class="box3 block"><var class="block">7</var> <span class="small">THU</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 8"><em class="block small">JUN</em> <span class="box3 block"><var class="block">8</var> <span class="small">FRI</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 9"><em class="block small">JUN</em> <span class="box3 block"><var class="block">9</var> <span class="small">SAT</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 10"><em class="block small">JUN</em> <span class="box3 block"><var class="block">10</var> <span class="small">SUN</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 11"><em class="block small">JUN</em> <span class="box3 block"><var class="block">11</var> <span class="small">MON</span></span></a></li>
		<li class="float-left select"><em class="block h5">JUN</em> <var class="block">12</var> <span>TUE</span></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 13"><em class="block small">JUN</em> <span class="box3 block"><var class="block">13</var> <span class="small">WED</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 14"><em class="block small">JUN</em> <span class="box3 block"><var class="block">14</var> <span class="small">THU</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JUNE 15"><em class="block small">JUN</em> <span class="box3 block"><var class="block">15</var> <span class="small">FRI</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JULY 16"><em class="block small">JUL</em> <span class="box3 block"><var class="block">16</var> <span class="small">SAT</span></span></a></li>
		<li class="float-left"><a href="#" title="Change Return trip date to JULY 17"><em class="block small">JUL</em> <span class="box3 block"><var class="block">17</var> <span class="small">SUN</span></span></a></li>
	</ul>
	
    <?php 
	$i=2;
	include("module_customize.php");
	?>
    	
	<table class="bound text-center br">
	<?php include("module_ocup_caption.php");?>
	<thead>
	<tr>
		<?php include("table_bound_head.php");?>
        
<?php if($booking=="certificate-promotional") { ?>
        <td class="caption2 bold">Promotional Award</td>
<?php } else if($booking=="certificate-transitional") {?>
		<?php if($page!="rocup") {?>
       	<td class="caption2 bold">Standard Award</td>
        <?php } ?>
       	<td class="caption2 bold">Freedom Award</td>
<?php } else { ?>           
		<td class="caption2 bold" data-family="feature_bs"><a class="block" href="#">Business Select</a></td>
		<td class="caption2 bold" data-family="feature_anytime"><a class="block" href="#">Anytime</a></td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="caption2 bold" data-family="feature_senior"><a class="block" href="#">Senior</a></td>
		<?php } ?>
		<td class="caption2 bold" data-family="feature_wga"><a class="block" href="#">Wanna Get Away</a></td>
<?php } ?>        
	</tr>
    
    <?php if($booking!="certificate-promotional" && $booking!="certificate-transitional") {?>
    <tr>
    	<td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") { ?>58,320 Pts<?php } else { ?>$584<?php }?></em></td>
        <td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") { ?>46,400 Pts<?php } else { ?>$556<?php }?></em></td>
        <?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") { ?>23,000 Pts<?php } else { ?>$450<?php }?></em></td>
		<?php } ?>
        <td class="caption2 alt"><em class="bold em10"><?php if($booking=="redemption") { ?>19,920 Pts<?php } ?></em></td>
    </tr>
    <?php } ?>
    
	</thead>
	<tbody>
	<tr>
		<td class="h6 h8"><var class="text-12">7:10</var> <small class="em10">AM</small></td>
		<td class="h6 h8"><var class="text-12">12:25</var> <small class="em10">PM</small></td>
		<td><a href="../popins/flif.php" target="_blank" class="wdk-tooltip" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">2671/2008</a></td>
		<td class="h8 noline"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>Change Planes<br />ATL</td>
		<td><small>6:45</small></td>

<?php if($booking=="certificate-promotional") { ?>
       <td class="em11 bold">Sold out</td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td class="em11 bold">Sold out</td>
       <?php }?>
       <td class="em11 bold">Sold out</td>
<?php } else { ?>         
		<td class="price" data-family="feature_bs">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            58,320 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>584</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<td class="price" data-family="feature_anytime">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            46,400 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>556</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_senior">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            19,920 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>450</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
		
       	<?php if($booking=="redemption") { ?>
        <td class="price" data-family="feature_wga">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">19,920 <a class="block var small" href="#">+$2.50</a></var></label></div>
		</td>
        <?php } else { ?>
        <td rowspan="4" class="noavailable">&nbsp;</td>
        <?php } ?>
		
<?php } ?>        
	</tr>
	<tr>
		<td class="h6 h8"><var class="text-12">8:55</var> <small class="em10">AM</small></td>
		<td class="h6 h8"><var class="text-12">2:50</var> <small class="em10">PM</small></td>
		<td><a class="wdk-tooltip" href="../popins/flif.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">451/323</a></td>
		<td class="h8 noline"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>Change Planes<br />ATL</td>
		<td><small>8:15</small></td>

<?php if($booking=="certificate-promotional") { ?>
       <td class="em11 bold">Sold out</td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
       <?php } ?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
<?php } else { ?> 		
        <td class="price" data-family="feature_bs">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            58,320 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>584</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<td class="price" data-family="feature_anytime">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            46,400 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>556</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_senior">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            19,920 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>450</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
		
        <?php if($booking=="redemption") { ?>
        <td class="price" data-family="feature_wga">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">19,920 <a class="block var small" href="#">+$2.50</a></var></label></div>
		</td>
        <?php } ?>            
		
<?php } ?>                  
	</tr>
	<tr>
		<td class="h6 h8"><var class="text-12">9:50</var> <small class="em10">AM</small></td>
		<td class="h6 h8"><var class="text-12">3:40</var> <small class="em10">PM</small></td>
		<td><a class="wdk-tooltip" href="../popins/flif.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">2320/384</a></td>
		<td class="h8 noline"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>Change Planes<br />ATL</td>
		<td><small>8:55</small></td>

<?php if($booking=="certificate-promotional") { ?>
       <td><label class="h8 bold midst"><input type="radio" name="radio_2" /> Promotional Award</label><a href="#" class="block var small">+$22.80</a></td>
<?php } else if($booking=="certificate-transitional") {?>
       <?php if($page!="rocup") {?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
       <?php }?>
       <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
<?php } else { ?>		
        <td class="price" data-family="feature_bs">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            58,320 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>584</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<td class="price" data-family="feature_anytime">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            46,400 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>556</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="price" data-family="feature_senior">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">
        	<?php if($booking=="redemption") { ?>
            19,920 <a class="block var small" href="#">+$2.50</a></var></label>
            <?php } else { ?>
            <sup>$</sup>450</var></label><small class="block h8 bold hidden">earn <strong class="em3 bold">1,536 pts</strong></small>
            <?php } ?>
            </div>
		</td>
		<?php } ?>
		
        <?php if($booking=="redemption") { ?>
        <td class="price" data-family="feature_wga">
        	<div><label class="bold"><input type="radio" name="radio_2" /><var class="h5">19,920 <a class="block var small" href="#">+$2.50</a></var></label></div>
		</td>
        <?php } ?>            
		
<?php } ?>
	</tr>
	<tr>
		<td class="h6 h8"><var class="text-12">9:50</var> <small class="em10">AM</small></td>
		<td class="h6 h8"><var class="text-12">3:40</var> <small class="em10">PM</small></td>
		<td><a class="wdk-tooltip" href="../popins/flif.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">234/331</a></td>
		<td class="h8 noline"><a class="block wdk-tooltip" href="../popins/flid.php" target="_blank" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip">1 stop</a>Change Planes<br />ATL</td>
		<td><small>7:50</small></td>

<?php if($booking=="certificate-promotional") { ?>
       	<td><label class="h8 bold midst"><input type="radio" name="radio_2" /> Promotional Award</label><a href="#" class="block var small">+$22.80</a></td>
<?php } else if($booking=="certificate-transitional") {?>
		<?php if($page!="rocup") {?>
        <td><label class="h8 bold"><input type="radio" name="radio_2" /> <a href="#" class="block var small">+$2.50</a></label></td>
        <?php }?>
       	<td class="em11 bold">Unavailable</td>
<?php } else { ?>         
		<td class="em11 bold" data-family="feature_bs">Sold out</td>
		<td class="em11 bold" data-family="feature_anytime">Sold out</td>
		<?php if($revenue=="senior2" || $page=="rocup" || isset($_GET["5columns"])) { ?>
		<td class="em11 bold" data-family="feature_senior">Sold out</td>
		<?php } ?>
		
		<?php if($booking=="redemption") { ?>
        <td class="em11 bold" data-family="feature_wga">Sold out</td>
        <?php } ?> 
<?php } ?>        
	</tr>
	</tbody>
	</table>
    
    <h4 class="h5 bold text-right hr">Price selected flight(s)</h4>
    <p class="text-right br"><button class="alt" type="submit">Continue</button></p>
	<h2 class="bold br">Important Fare &amp; Schedule Information</h2>
	<p class="nomargin br">All fares and fare ranges are subject to change until purchased.</p>
	<p class="nomargin">Flight ontime performance statistics can be viewed by clicking on the individual flight numbers.</p>
	<p class="nomargin">All fares and fare ranges listed are per person for each way of travel.</p>
	<p class="nomargin">"Unavailable" indicates the corresponding fare is unavailable for the selected travel date(s), the search did not meet certain fare requirements, or the flight has already departed.</p>
	<p class="nomargin">"Sold Out" indicates that flight is sold out for the corresponding fare type.</p>
	<p class="nomargin">"Invalid w / Depart or Return Dates" indicates that our system cannot return a valid itinerary option(s) with the search criteria submitted. This can occur when flights are sold out in one direction of a roundtrip search or with a same-day roundtrip search. These itineraries may become valid options if you search with a different depart or return date and/or for a one way flight instead.</p>
	<p class="nomargin">"Available" in the Senior Fare column indicates the Senior Fare is available for the flight and may be selected for the Senior Passenger, after selecting a fare for the Adult Passenger on this page.</p>
	<p class="nomargin">"Travel Time" represents the total elapsed time for your trip from your departure city to your final destination including stops, layovers, and time zone changes.</p>
	<p class="nomargin">For <a href="#">infant</a>, <a href="#">child (2-11)</a>, <a href="#">group (10+)</a>, and <a href="#">military</a> fares please call 1-800-I-FLY-SWA (1-800-435-9792).<b> These fares are a discount off the "Anytime" fares.</b> Other fares may be lower.</p>
    
    <?php if($page=="ocup") {?>
	<p class="nomargin">*"Save $250 with Flight + Hotel" claim is based on 2011 available data for average savings on Jackpot bookings purchased in a bundled package vs purchasing components separately (i.e. a la carte). Savings on any given package will vary based on the selected origin, destination, travel dates, hotel property, length of stay, car rental, and activity tickets. Savings may not be available on all packages.</p>
    <?php }?>
    <?php include("module_teaser.php");?>