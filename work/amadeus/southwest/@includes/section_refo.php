    <h1 class="h1">Cancel Air Reservation # 435S47</h1>
	<p class="bold br">Things you should know before you cancel:</p>
    <ul class="br btr">
        <li class="wrap nomargin"><small class="ico arrow2"></small><span class="float-left grid9">By cancelling this reservation you will be giving up your space and fare on this flight.</span></li>
        <li class="wrap nomargin"><small class="ico arrow2"></small><span class="float-left grid9">Rebooking is subject to current flight availability and may result in a higher fare.</span></li>
        <li class="wrap nomargin"><small class="ico arrow2"></small><span class="float-left grid9">Due to system processing time, funds from this reservation may not be available for immediate use toward the purchase of a new flight reservation.</span></li>
        <li class="wrap nomargin"><small class="ico arrow2"></small><span class="float-left grid9">By cancelling this reservation you will lose EarlyBird Check-In status and the funds will not be refunded from this purchase. <a href="#">More information on EarlyBird Check-in</a></span></li>
    </ul>

	<h2 class="confcode float-right bold text-center">Confirmation # 1FW54S</h2>
    <h3 class="h5 bold"><span class="ico plane"></span> AIR</h3>
    
    <?php
    include("table_passengers.php");
    include("table_itinerary.php");
    ?>
    <table class="fare hr">
    <caption class="midst bold">BILLING ADDRESS</caption>
    <thead>
    <tr class="caption1 bold line1t">
        <td>Purchaser's Name</td>
        <td>Billing Address</td>
        <td>City, State &amp; Zip</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>JOHN DOE</td>
        <td>221B BAKER STREET</td>
        <td>LONDON, GB NW16XE</td>
    </tr>
    </tbody>
    </table>
  	<?php include("module_travel_funds.php");?>  

    <h4 class="hr h5 bold text-right">Cancel this reservation?</h4>
    <p class="text-right br"><a href="#">No, Do Not Cancel</a> &nbsp; <button class="alt">Yes, Cancel</button></p>