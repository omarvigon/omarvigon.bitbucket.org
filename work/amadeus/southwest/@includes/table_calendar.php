	<p class="float-right grid3">Now accepting reservations through January 4, 2013. <?php if ($booking=='revenue'){ ?><b>All fares are rounded up to the nearest dollar.</b><?php } ?></p>
	<ul class="date text-center">
    	<?php if($i==1){?>
        <li class="float-left select"><a href="#"><em class="block h7 em8">2012</em> <var class="block">May</var> <span>&nbsp;</span></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Jun</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Jul</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Aug</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Sep</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Oct</var></a></li>
        <li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Nov</var></a></li>
        <?php }else{ ?>
        <li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">May</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Jun</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Jul</var></a></li>
        <li class="float-left select"><a href="#"><em class="block h7 em8">2012</em> <var class="block">Aug</var> <span>&nbsp;</span></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Sep</var></a></li>
		<li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Oct</var></a></li>
        <li class="float-left"><a href="#"><em class="block">2012</em> <var class="block h5">Nov</var></a></li>
        <?php } ?>
	</ul>
    
    <div class="wrap br line10">
		<div class="float-right grid2 box1">
			<?php if($booking=="certificate-promotional" || $booking=="certificate-transitional") { ?>
				<?php if($booking=="certificate-transitional") { ?>
                <ul>
                    <li class="wrap"><small class="ico family f1 float-left"></small><a class="bold grid7 float-left indent-small br" href="#">Freedom Award</a></li>
                    <li class="wrap"><small class="ico family f2 float-left"></small><a class="bold grid7 float-left indent-small br" href="#">Standard Award</a></li>
                </ul>
                <?php } ?>
            
                <h2 class="h6 em10 bold">Legend:</h2>
                <ul class="br">
                    <li class="wrap"><small class="ico family f2 float-left text-center"><span class="ico done2 brsmall"></span></small><a class="bold grid7 float-left indent-small" href="#">Flights are available on this date</a></li>
                    <li class="wrap"><small class="ico family f3 text-center float-left"><div class="br">N/A</div></small><a class="bold grid7 float-left indent-small" href="#">Flights are not available on this date</a></li>
                </ul>
                
                <?php if($booking=="certificate-transitional") { ?>
                <ul class="br">
                    <li class="wrap"><span class="ico info2 float-left">&nbsp;</span><a class="bold grid7 float-left indent-small" href="#">Freedom awards can be used to purchase flights available for Standard Awards.</a></li>
                </ul>
                <?php } ?>
            <?php } else { ?>
            <h2 class="h6 em10 bold">Legend:</h2>
            <ul class="br">
            	<li><small class="ico family f1"></small> <a class="bold" href="#">Business Select</a></li>
                <li><small class="ico family f2"></small> <a class="bold" href="#">Anytime</a></li>
                <li><small class="ico family f3"></small> <a class="bold" href="#">Wanna Get Away</a></li>
            </ul>
            <?php } ?>
		</div>
	
		<table class="calendar">
		<thead>
		<tr class="caption1 bold text-center">
			<td>Sun</td>
			<td>Mon</td>
			<td>Tue</td>
			<td>Wed</td>
			<td>Thu</td>
			<td>Fri</td>
			<td>Sat</td>
		</tr>
		</thead>
		<tbody class="h4">
		<tr>
            <td>&nbsp;</td>
            <td class="out"><small class="em10">1</small></td>
            <td class="out"><small class="em10">2</small></td>
            <td class="out"><small class="em10">3</small></td>
            <td class="out"><small class="em10">4</small></td>
            <td class="out"><small class="em10">5</small></td>
            <td class="out"><small class="em10">6</small></td>
        </tr>
<?php if($booking=="redemption") { ?>
        <tr>
            <td class="out"><small class="em10">7</small></td>
            <td class="out"><small class="em10">8</small></td>
            <td class="out"><small class="em10">9</small></td>
            <td class="out"><small class="em10">10</small></td>
            <td class="out"><small class="em10">11</small></td>
            <td class="out"><small class="em10">12</small></td>
            <td class="out"><small class="em10">13</small></td>                
        </tr>		
        <tr>    
            <td class="out"><small class="em10">14</small></td>
            <td class="out"><small class="em10">15</small></td>
            <td class="out"><small class="em10">16</small></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">17</small><var class="h5 block text-center">21,458 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">18</small><var class="h5 block text-center">45,425 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">19</small><var class="h5 block text-center">2,458 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">20</small><var class="h5 block text-center">47,012 <a class="block var small" href="#">+$187.97</a></var></td>                 
        </tr>			
        <tr>
            <td class="family f2 caption8"><input type="radio" class="none" /><small class="em10">21</small><var class="h5 block text-center">19,920 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">22</small><var class="h5 block text-center">31,012 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="noavailable"><small class="em10">23</small><em class="block text-center em10">No Availability</em></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">24</small><var class="h5 block text-center">31,012 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">25</small><var class="h5 block text-center">31,012 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">26</small><var class="h5 block text-center">58,475 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">27</small><var class="h5 block text-center">12,582 <a class="block var small" href="#">+$187.97</a></var></td>					
        </tr>			
        <tr>    
            <td class="family f3"><input type="radio" class="none" /><small class="em10">28</small><var class="h5 block text-center">32,465 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f3"><input type="radio" class="none" /><small class="em10">29</small><var class="h5 block text-center">12,325 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f3"><input type="radio" class="none" /><small class="em10">30</small><var class="h5 block text-center">42,654 <a class="block var small" href="#">+$187.97</a></var></td>
            <td class="family f3"><input type="radio" class="none" /><small class="em10">31</small><var class="h5 block text-center">41,245 <a class="block var small" href="#">+$187.97</a></var></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
<?php } else if($booking=="certificate-promotional" || $booking=="certificate-transitional"){ ?>	
		<tr>
			<td class="out"><small class="em10">7</small></td>
			<td class="out"><small class="em10">8</small></td>
			<td class="out"><small class="em10">9</small></td>
			<td class="out"><small class="em10">10</small></td>
			<td class="out"><small class="em10">11</small></td>
			<td class="out"><small class="em10">12</small></td>
			<td class="out"><small class="em10">13</small></td>
		</tr>		
		<tr>    
			<td class="out"><small class="em10">14</small></td>
			<td class="out"><small class="em10">15</small></td>
			<td class="out"><small class="em10">16</small></td>
			<td class="out"><small class="em10">17</small></td>
			<td class="out"><small class="em10">18</small></td>
			<td class="out"><small class="em10">19</small></td>
			<td class="out"><small class="em10">20</small></td>                 
		</tr>			
		<tr>
			<td class="out"><small class="em10">21</small></td>
			<td class="out"><small class="em10">22</small></td>
			<td class="out"><small class="em10">23</small></td>
			<?php if($booking=="certificate-transitional") { ?>
			<td class="family f1 certificate"><small class="em10">24</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<?php } else { ?>
			<td class="family f2 certificate"><small class="em10">24</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<?php } ?>
			<td class="family f2 certificate"><small class="em10">25</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<td class="family f2 certificate"><small class="em10">26</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<td class="family f2 certificate"><small class="em10">27</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>					
		</tr>			
		<tr>    
			<td class="family certificate caption8"><small class="em10">28</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<td class="family f2 certificate"><small class="em10">29</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<td class="noavailable"><small class="em10">30</small><em class="block text-center em10">No Availability</em></td>
			<td class="family f2 certificate"><small class="em10">31</small><p class="text-center"><span class="ico done2"></span><a class="block var small" href="#">+$187.97</a></p></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
<?php } else { ?>
        <tr>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">7</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">8</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">9</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">10</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">11</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">12</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">13</small><var class="h5 block text-center">$277</var></td>                
        </tr>		
        <tr>    
            <td class="family f2"><input type="radio" class="none" /><small class="em10">14</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">15</small><var class="h5 block text-center">$277</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">16</small><var class="h5 block text-center">$277</var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">17</small><var class="h5 block text-center">$469</var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">18</small><var class="h5 block text-center">$469</var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">19</small><var class="h5 block text-center">$469</var></td>
            <td class="family f1"><input type="radio" class="none" /><small class="em10">20</small><var class="h5 block text-center">$500</var></td>                 
        </tr>			
        <tr>
            <td class="family f2 caption8"><input type="radio" class="none" /><small class="em10">21</small><var class="h5 block text-center">$280</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">22</small><var class="h5 block text-center">$280</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">23</small><var class="h5 block text-center">$290</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">24</small><var class="h5 block text-center">$290</var></td>
            <td class="noavailable"><small class="em10">25</small><em class="block text-center em10">No Availability</em></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">26</small><var class="h5 block text-center">$300</var></td>
            <td class="family f2"><input type="radio" class="none" /><small class="em10">27</small><var class="h5 block text-center">$310</var></td>					
        </tr>			
        <tr>    
            <td class="family f3"><input type="radio" class="none" /><small class="em10">28</small><var class="h5 block text-center">$447</var></td>
            <td class="family f3"><input type="radio" class="none" /><small class="em10">29</small><var class="h5 block text-center">$447</var></td>
            <td class="family f3"><input type="radio" class="none" /><small class="em10">30</small><var class="h5 block text-center">$447</var></td>
            <td class="family f3"><input type="radio" class="none" /><small class="em10">31</small><var class="h5 block text-center">$450</var></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>	
<?php } ?>
		</tbody>
		</table>
	</div>