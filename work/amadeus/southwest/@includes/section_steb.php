    <h1><b class="h1">EarlyBird Check-In:</b> <span class="h2">A More Convenient Way to Travel</span></h1>
    <div class="steb">
    	<div class="float-right">
        	<p class="h5 h9 bold line2"><small class="ico done"></small>Automatic Check-In</p>
        	<p class="h5 h9 bold line2"><small class="ico done"></small>Better Boarding Position</p>
        	<p class="h5 h9 bold line2"><small class="ico done"></small>Earlier Access to Overhead Bins</p>
        	<p class="text-right"><a href="#">Learn More</a></p>
        </div>
    </div>
    <h2 class="h4 br btr">Choose Flights and Passengers for EarlyBird Check-In</h2>
    <?php include("table_itinerary.php");?>    
    <h4 class="h5 bold text-right hr">Continue to pricing and payment</h4>
    <?php include("module_footer_continue.php");?>