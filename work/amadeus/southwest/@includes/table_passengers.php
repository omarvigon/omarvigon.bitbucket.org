<table class="tdefault <?php if($page!="reco" && $page!="ureco" && $page!="uconf") { ?>br<?php } ?>">
<thead>
    <tr class="bold noborder">
        <?php if($booking=="companion"){ ?>
        <td class="grid5">Companion</td>                        
        <?php } else { ?>
        <td class="grid5">Passenger(s)</td>                 
        <?php } ?>
        <td class="grid5">Rapid Rewards #</td>
        <td><?php if( $page=="conf" || $page=="rconf" || $page=="bkgd") { ?>Options<?php }?></td>
    </tr>
</thead>
<tbody>
    <tr>
        <td class="text-upper">John Doe<?php if($page!="reco") {?> <span class="ico texta"></span><?php }?></td>
        <td>00000123456789</td>
        <td class="vertical-top">
            <?php if( $page=="conf" || $page=="rconf" || $page=="bkgd" ) { ?>
            <button class="h8">Add EarlyBird Check-In</button>
            <?php } ?>    
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <?php if($page=='bkgd') { ?>
                <p><small class="ico passport"></small> <a href="#">Add/Edit Passport Information</a></p>
                <p><small class="ico disability"></small> <a href="#">Add/Edit Disability Assistance Options</a> <em class="em3">Updated successfully</em></p>
            <?php } ?>
            <p class="wrap"><span class="ico info2 float-left"></span> Assistance Requested. Please Inform Southwest Airlines Representatives upon arrival at the airport.</p>
        </td>
    </tr>
<?php if($booking!="companion") { ?>    
    <tr class="nopadding">
        <td colspan="3"><hr class="line2" /></td>
    </tr> 
    <tr>
        <td class="text-upper ">Jane Doe<?php if($page!="reco") {?> <span class="ico textb"></span><?php }?></td>    
        <td>- None Entered -</td>
        <td>&nbsp;</td>
    </tr>
	<?php if($page=="bkgd") { ?>
    <tr>
        <td colspan="3">
        <p><small class="ico passport"></small> <a href="#">Add/Edit Passport Information</a></p>
        <p><small class="ico disability"></small> <a href="#">Add/Edit Disability Assistance Options</a> <em class="em3">Updated successfully</em></p>
        </td>
    </tr>
    <?php } ?>
<?php } ?>    
</tbody>
</table>