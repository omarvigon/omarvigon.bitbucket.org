<?php
function seturl($booking,$revenue,$login)
{
	$output=$_SERVER["PHP_SELF"]."?";
	$sign="&nbsp; ";

	if($booking=="" && isset($_GET["booking"]))
		$output.="booking=".$_GET["booking"];
	else
		$output.="booking=".$booking;
		
	if($revenue=="" && isset($_GET["revenue"]))
		$output.="&amp;revenue=".$_GET["revenue"];
	else
		$output.="&amp;revenue=".$revenue;
		
	if($login=="" && isset($_GET["login"]))
		$output.="&amp;login=".$_GET["login"];
	else
		$output.="&amp;login=".$login;
	
	/* Booking */
	if($booking=="revenue")
	{	
		if(!isset($_GET["booking"]) || $_GET["booking"]=="revenue")
			$sign="&#10004;";
	}
	else if ($booking != "")
	{
		if(isset($_GET["booking"]) && $_GET["booking"]==$booking)
			$sign="&#10004;";
	}
	
	/* Revenue */
	if($revenue=="adult")
	{	
		if(!isset($_GET["revenue"]) || $_GET["revenue"]=="adult")
			$sign="&#10004;";
	}
	else if ($revenue != "")
	{
		if(isset($_GET["revenue"]) && $_GET["revenue"]==$revenue)
			$sign="&#10004;";
	}
	
	/* Login */
	if($login=="false") {	
		if(!isset($_GET["login"]) || $_GET["login"]=="false")
			$sign="&#10004;";
	} else if($login=="true") {
		if(isset($_GET["login"]) && $_GET["login"]=="true")
			$sign="&#10004;";
	} else if($login=="nonrr") {
		if(isset($_GET["login"]) && $_GET["login"]=="nonrr")
			$sign="&#10004;";
	}
	echo $sign."</span><a href='".$output."'>";	
}
?>
<div style="position:fixed;top:5px;right:5px;padding:10px;background:#eee;border-radius:5px;box-shadow:0 3px 3px #666;z-index:10; -position:absolute; -width:160px;">
<?php if($page=="prch") { ?>
     <p class="bold">Booking Mode</p>
     <p>&nbsp; <a href="./prch.php?booking=revenue">Revenue</a></p>
     <p>&nbsp; <a href="./prch.php?booking=redemption">Redemption</a></p>
<?php } else { ?>

    <p class="bold">Booking Mode</p>
    <p><span style="color:#2683F9"><?php seturl('revenue','','');?>Revenue</a></p>
    <p><span style="color:#2683F9"><?php seturl('redemption','','');?>Redemption</a></p>
    <?php if($page=="ocup" ) { ?>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?5columns">5 Columns</a></p>
    <?php } ?>
	<?php if($page!="occl" && $page!="ocup" ) { ?>
    <p><span style="color:#2683F9"><?php seturl('companion','','');?>Companion</a></p>
    <?php } ?>
    <p><span style="color:#2683F9"><?php seturl('certificate-promotional','','');?>Certificate - Promotional</a></p>
    <p><span style="color:#2683F9"><?php seturl('certificate-transitional','','');?>Certificate - Transitional</a></p>

    <?php if($booking=="revenue" && $page!="rconf") { ?>
        <p class="bold">Revenue Mode</p>
        <p><span style="color:#2683F9"><?php seturl('','adult','');?>Adult</a></p>
        <p><span style="color:#2683F9"><?php seturl('','senior1','');?>Senior Step 1</a></p>
        <p><span style="color:#2683F9"><?php seturl('','senior2','');?>Senior Step 2</a></p>
     <?php } ?>
	
    <?php if($flow=="exchange" ) { ?>
    <p class="bold">Exchange Mode</p>
	<p><span style="color:#2683F9"><?php seturl('revenue','','');?>Revenue</a></p>
    <p><span style="color:#2683F9"><?php seturl('redemption','','');?>Redemption</a></p>
	<?php } ?>
	
<?php if($page=="rsrch") { ?>
    <p class="bold">New Trip Panel States</p>
	<p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rsrch=both">Change both bounds</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rsrch=out">Change outbound</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rsrch=in">Change inbound</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rsrch=flown">Outbound flown</a></p>
<?php } if($page=="conf") { ?>
    <p class="bold">EarlyBird States</p>
	<p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?earlybid=purchase">Purchase</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?earlybid=no-purchase">No Purchase</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?earlybid=failure">Purchase Error</a></p>
<?php } if($page=="rocup") { ?>
	<p class="bold">Modify States</p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rocup=both">Change both bounds</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rocup=out">Change outbound</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rocup=in">Change inbound</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rocup=out">Change one-way</a></p>
<?php } if($page=="rfare") { ?>
	<p class="bold">Pricing Summary Panel</p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rfare=pay">Payment</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rfare=refund">Refund</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rfare=pay&rfare2=nothing">Nothing to pay</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rfare=both">Payment and refund</a></p>
<?php } if($page=="rconf") { ?>
	<p class="bold">Form of Payment states</p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>">With additional collection</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rconf=refund">With refund</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?rconf=hold">With hold travel funds</a></p>   
<?php } if($page=="purc") { ?>
    <p class="bold">Payment Method</p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?purc=collection">Collection</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?purc=even_exchange">Even exchange</a></p>
    <p>&nbsp; <a href="<?php $_SERVER["PHP_SELF"]?>?purc=ard">ARD Mode</a></p>
<?php } ?>

    <p class="bold">Login Status</p>
    <p><span style="color:#2683F9"><?php seturl('','','false');?>Logged-out</a></p>
    <p><span style="color:#2683F9"><?php seturl('','','true');?>Logged-in</a></p>
    <p><span style="color:#2683F9"><?php seturl('','','nonrr');?>Logged-in Non RR member</a></p>
    <p><label id="errors"><input type="checkbox" />Show Error Panel</label></p>
<?php } ?>
</div>