    <h1 class="h1">Your reservation has been cancelled.</h1>
<?php if($booking=="revenue") { ?>
	<p class="br">Your request for a refund of <b>$561.20</b> has been submitted.</p>
<?php } else if($booking=="redemption") { ?>
    <p class="br">Your request for a refund of <b>$10.00</b> has been submitted. <b>10,500 points</b> will be returned to Rapid Rewards Account number 501719595.</p>
<?php } else { ?>
	<p class="br">Your request for a refund of <b>$10.00</b> has been submitted. <b>2</b> award coupon(s) will be returned to Rapid Rewards Account number 501719595.</p>
<?php } ?>
    <p>You may <a href="#">View Travel Funds</a> or <a href="#">Book a Flight</a> while applying Travel Funds prior to its expiration date.</p>	
    <p>Please print this page for your records.</p>	
    <table class="fare hr border">
    <caption class="bold">TRAVEL FUNDS INFORMATION</caption>
    <thead>
    <tr class="bold small">
        <th class="caption1 line4">Confirmation Number</th>
        <th class="caption1 line4">Passenger(s)</th>
        <th class="caption1 line4">Depart</th>
        <th class="caption1 line4">Return</th>
        <th class="caption1 line4">Expiration Date</th>
    </tr>
    </thead>
    <tbody>
    <tr class="small">
        <td>435S47</td>
        <td><em class="block">JOHN DOE</em><em class="block">JANE DOE</em><em class="block">MIKE DOE</em></td>
        <td>Jul 15</td>
        <td>Jul 24</td>
        <td>05/03/2013</td>
    </tr>
    </tbody>
    </table>
    <h4 class="h5 bold hr text-right">What's Next?</h4>
    <p class="text-right br"><button class="alt " type="submit">Book a Flight</button></p>