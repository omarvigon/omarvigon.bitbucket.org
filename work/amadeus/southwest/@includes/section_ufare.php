	<?php include("header_message_before.php");?>
    <h1 class="h1">Air Itinerary and Pricing</h1>
    <h4 class="bold br">Please note:</h4>
    <ul class="wrap btr">
    	<li class="nomargin"><small class="ico arrow2"></small>Funds applied from nonrefundable fares toward the purchase of a new reservation remain nonrefundable.</li>
		<li class="nomargin"><small class="ico arrow2"></small>The new reservation inherits the earliest expiration date from any funds applied from the old ticket. Therefore, the expiration date of your new reservation and all associated funds may be less than 12 months. Your new expiration date will be displayed on the confirmation receipt sent to you.</li>
    </ul>
    <?php
    include("table_itinerary.php");
	include("table_price.php");
	?>

	<h4 class="h5 bold text-right hr">Purchase your new flights</h4>
    <p class="text-right">By clicking 'Continue', you agree to accept the fare rules and want to continue with this purchase</p>
    <?php include("module_footer_continue.php");?>