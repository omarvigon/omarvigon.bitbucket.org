	<h1 class="h1">Add Rapid Rewards Number</h1>
    <p class="br">We have your Rapid Rewards number and when you board your upcoming flight, we will be sure to give you credit for it. To view your current Rapid Rewards credits, <a href="#">sign in to MySouthwest.</a></p>
    <?php include("module_pnrs.php");?>

        <caption class="h4">Updated Reservation Information</caption>
        <thead>
        <tr class="bold noborder">
            <td class="text-right" colspan="2">Rapid Rewards #</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Passenger 1:</b> NANCY  PEEKTESTONE</td>
            <td class="text-right">501719595</td>
        </tr>
        <tr>
            <td><b>Passenger 2:</b> JOHN  PEEKTESTONE</td>
            <td class="text-right">– None Entered –</td>
        </tr>
        </tbody>
        </table>
    </div>
    
    <h2 class="br"><b>Want to board first?</b> Get priority seating on this flight</h2>
    <button class="alt2 br">Upgrade to Business Select</button>