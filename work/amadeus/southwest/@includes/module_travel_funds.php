	<h2 class="h3 bold br">Travel Funds</h2>
<?php if($page=="reco") {?>	
    <div class="box2 indent3 br">
        <h6 class="bold">Since a credit exists with the changes you have elected to make on this itinerary, please select what you would like to do with the balance of these funds. Unused travel funds may only be applied toward the purchase of future travel for the individual named on the ticket.</h6>
        <table class="tdefault grid6 box-center">
        <tr class="line2 vertical-top">
            <th class="bold">Refundable</th>
            <td class="text-center">($912.60)</td>
            <td><label class="block"><input type="radio" name="radio_2" /> Hold for future use.</label><label class="block"><input type="radio" name="radio_2" /> Request a refund of the refundable balance.</label></td>
        </tr>
        <tr>
            <th class="bold br">Non-refundable</th>
            <td class="text-center">$20.00</td>
            <td><span class="indent3">Hold for future use</span></td>
        </tr>
        </table>
<?php } else /*refo*/ {?>
    <div class="box2 br">
        <table class="tdefault">
        <caption>Please select what you would like to do with the balance of these funds</caption>
        <tr class="line2 vertical-top">
            <th class="bold">Refundable</th>
            <td class="text-center"><?php if($booking=="revenue") { ?>$561.20<?php } else { ?>$10.00<?php } ?></td>
            <td><fieldset>
                    <label class="block bold"><input type="radio" /> Hold for future use.</label>
                    <label class="block bold"><input type="radio" /> Request a refund of the refundable balance.</label>
                </fieldset>
            </td>
        </tr>
        <tr<?php if($booking!="revenue" && $booking!="companion") { ?> class="line2"<?php } ?>>
            <th class="bold br">Non-refundable</th>
            <td class="text-center"><?php if($booking=="revenue") { ?>$43.10<?php } else { ?>$0.00<?php } ?></td>
            <td><span class="indent3"><?php if($booking=="revenue") { ?>These funds will be held for future use.<?php } else { ?>N/A<?php } ?></span></td>
        </tr>
        <?php if($booking=="redemption") { ?>
        <tr>
            <th class="bold br">Points</th>
            <td class="text-center">10,500</td>
            <td><b class="indent3">Points will be returned to Rapid Rewards Account number 501719595.</b></td>
        </tr>
        <?php } else if($booking=="certificate-promotional" || $booking=="certificate-transitional") { ?>
        <tr>
            <th class="bold br">Award(s)</th>
            <td class="text-center">2</td>
            <td class="bold"><span class="indent3">Award coupon(s) will be returned to Rapid Reward Account #XXXXXX.</span></td>
        </tr>
        <?php } ?>
        </table>
        <?php if($booking=="redemption") { ?>
        <p class="bold br">This reservation was purchased using Rapid Rewards points. If cancelled, you may use the points to make another reservation.</p>
        <?php } else if($booking=="certificate-promotional" || $booking=="certificate-transitional") { ?>
        <p class="bold br">This reservation was purchased using a Rapid Reward Award or Promocert Coupon. If cancelled, you may use the Rapid Reward Award or Promocert Coupon to make another reservation, but be aware that it will maintain the original expiration date.</p>
        <?php } ?>
<?php } ?>
	</div>