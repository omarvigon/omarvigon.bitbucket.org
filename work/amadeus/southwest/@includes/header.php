<?php include("../@includes/head.php");?>
<div class="main header">
	<a class="float-left" href="#"><img src="../@WDS_STATIC_FILES_PATH@/img/southwest.png" alt="Southwest" /></a>
	<form action="#" method="post" class="text-right">
    	<input class="input em1" type="text" value="Search Southwest" />
        <button class="bold" type="submit">GO</button>
	</form>
	<p class="em1 text-right br"><a href="#"><b>southwest</b>giftcard &reg;</a> <small class="ico card"></small> | <a href="#">The Southwest Travel Experience</a> | <a href="#">Sign Up 'n Save</a> | <a href="#">Travel Fees</a> | <a href="#">Help</a> 
    <a class="bold input toggle wdk-toggle" data-wdk-toggle-target="#travelToolsMenu" href="#">Travel Tools <small class="float-right slope s1"></small></a>
    </p>
    <ul class="alt none" id="travelToolsMenu">
        <li><a href="#">MySouthwest Account</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Check In</a></li>
        <li><a href="#">Check Flight Status</a></li>
        <li><a href="#">Flight Status Messaging</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">View/E-Mail Air Reservation</a></li>
        <li><a href="#">Change Air Reservation</a></li>
        <li><a href="#">Cancel Air Reservation</a></li>
        <li><a href="#">Add Rapid Rewards Number</a></li>
        <li><a href="#">Look up Rapid Rewards Number</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#"><strong>southwest</strong>giftcard Balance</a></li>
        <li><a href="#">Travel Fund Balance</a></li>
        <li><a href="#">LUV Voucher Balance</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Car Travel Tools</a></li>
        <li><a href="#">Hotel Travel Tools</a></li>
        <li><a href="#">View All Travel Tools</a></li>
        <li class="float-right"><a class="toggle wdk-toggle" data-wdk-toggle-target="#travelToolsMenu" href="#">Close<span class="ico close"></span></a></li>
	</ul>
</div>
<div class="main nav">
    <div class="float-left v1">
    	<a href="#"><b class="h4 block">Special Offers</b><span class="em11">Great Travel Deals</span></a>
        <ul>
        <li><a href="#">Air Offers</a></li>
        <li><a href="#">Car Offers</a></li>
        <li><a href="#">Hotel Offers</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Vacation Offers</a></li>
        <li><a href="#">Cruise Offers</a></li>
        <li><a href="#">Theme Park Tickets</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Click 'n Save E-mails</a></li>
        <li><a href="#">DING! Fare Alerts</a></li>
        <li><a href="#">View All Offers</a></li></ul>
    </div>
    <div class="float-left v2">&nbsp;</div>
    <div class="float-left v3">
    	<a href="#"><b class="h4 block">Travel Guide</b><span class="em11">Forum &amp; Destination Suggestions</span></a>
        <ul>
        <li><a href="#">Route Map</a></li>
        <li><a href="#">Featured Destinations</a></li>
        <li><a href="#">All Destinations</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Activities</a></li>
        <li><a href="#">Themes</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Community</a></li>
        <li><a href="#">Conversations</a></li>
        <li><a href="#">GetAway Finder<sup>SM</sup></a></li>
        </ul>
    </div>
    <div class="float-left v4">
    	<a href="#"><b class="h4 block">Rapid Rewards <small>&reg;</small></b><span class="em11">Earn Flight Faster</span></a>
        <ul>
        <li><a href="#">Program Details</a></li>
        <li><a href="#">Enroll</a></li>
        <li><a href="#">My Account</a></li>
        <li><a href="#">Look up Rapid Rewards Number</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">How to Earn</a></li>
        <li><a href="#">How to Redeem</a></li>
        <li><a href="#">Book with Points</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">A-List</a></li>
        <li><a href="#">A-List Preferred</a></li>
        <li><a href="#">Companion Pass</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Partners</a></li>
        <li><a href="#">Rapid Rewards VISA</a></li>
        <li><a href="#">Promotions</a></li>
        </ul>
    </div>
    <div class="absolute v5 select">
        <a href="#" class="h5 bold">Air</a><small class="ico"></small>
        <ul class="alt">
        <li><a href="#">Book a Flight</a></li>
        <li><a href="#">Book Award Travel</a></li>
        <li><a href="#">Special Offers</a></li>
        <li><a href="#">Travel Outside U.S.</a></li>
        <li><a href="#">Where We Fly</a></li>
        <li><a href="#">Travel on AirTran</a></li>
        <li><a href="#">Low Fare Calendar</a></li>
        <li><a href="#">Travel on Volaris</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Check In</a></li>
        <li><a href="#">Business Travel &amp; Groups</a></li>
        <li><a href="#">Check Flight Status</a></li>
        <li><a href="#">Baggage Policies</a></li>
        <li><a href="#">Flight Status Updates</a></li>
        <li><a href="#">Boarding Procedures</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Flight Schedules</a></li>
        <li><a href="#">EarlyBird Check-In<sup>TM</sup></a></li>
        <li class="nolink">&nbsp;</li>
        <li><a href="#"><b>southwest</b>giftcard&copy;</a></li>
        <li class="nolink">&nbsp;</li>
        <li class="nolink"><hr class="caption3" /></li>
        <li class="nolink">&nbsp;</li>
        <li><a href="#">View Reservation</a></li>
        <li class="nolink">&nbsp;</li>
        <li><a href="#">Change Reservation</a></li>
        <li class="nolink">&nbsp;</li>
        <li><a href="#">Cancel Reservation</a></li>
        <li class="nolink">&nbsp;</li>
        <li><a href="#">Add Rapid Rewards #</a></li>
        </ul>
    </div>
    <div class="absolute v6">
        <a href="#" class="h5 bold">Hotel</a><small class="ico"></small>
        <ul class="alt">
        <li><a href="#">Book a Hotel</a></li>
        <li><a href="#">Preferred Partners</a></li>
        <li><a href="#">Special Offers</a></li>
        <li><a href="#">Room Types</a></li>
        <li class="nolink"><hr class="caption3" /></li>
        <li class="nolink">&nbsp;</li>
        <li><a href="#">View Reservation</a></li>
		<li class="nolink">&nbsp;</li>
        <li><a href="#">Cancel Reservation</a></li>
        <li class="nolink">&nbsp;</li>
		</ul>
    </div>
    <div class="absolute v7">
        <a href="#" class="h5 bold">Car</a><small class="ico"></small>
        <ul class="alt">
        <li><a href="#">Book a Car</a></li>
        <li><a href="#">Policies &amp; Procedures</a></li>        
        <li><a href="#">Special Offers</a></li>
        <li><a href="#">Preferred Partners</a></li>
		<li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Vehicle Types</a></li>
        <li><a href="#">View Reservation</a></li>
		<li class="nolink"><hr class="caption3" /></li>
        <li><a href="#">Cancel Reservation</a></li>
        <li><a href="#">Book a Shuttle</a></li>
		</ul>
    </div>
    <div class="absolute v8">
        <a href="#" class="h5 bold">Vacations</a><small class="ico"></small>
        <ul class="alt">
        <li><a href="#">Book a Southwest Vacation</a></li>
        <li class="column2"><a href="#">Special Offers</a></li>
        <li><a href="#">Book a Cruise</a></li>
        <li class="column2"><a href="#">Vacation Destinations</a></li>
        <li><a href="#">Book a Jackpot Package</a></li>
        </ul>
    </div>
    <div class="v9"><a href="#" class="bold">WHY FLY SOUTHWEST</a></div>
</div>
<?php if($page=="bkgd" || $page=="uasst" || $page=="uapis") {?>
<div class="main nav_1 menu line11 small">
<?php } else {?>
<div class="main breadcrumb">
<?php }?>
<?php if($page=="logi") {?>
	<p class="h6"><b class="em1">My Account Login</b></p>
<?php } ?>