	<h2 class="h3 bold br">Do You Want to Apply Travel Funds?</h2>
    <fieldset class="box2 indent3 br">

	<?php if($page=="reco") {?>
		<div>
    <?php } else { ?>
    	<legend><p>You may apply funds from unused or cancelled reservations, <b>southwest</b>giftcards&reg;, or Southwest LUV Vouchers toward the purchase of this reservation.</p></legend>
        <button value="applyfunds">Apply Travel Funds</button>
        <div class="none">
	<?php } ?>
			<p>Apply funds one at a time. Up to two unused tickets per passenger may be applied. Unused tickets are always applied first. Up to four of the following may also be applied in any combination: <b>southwest</b>giftcards&reg;, Southwest LUV Vouchers, leftover electronic ticket funds, and one Credit Card.</p>
            <div class="wrap br">
                <div class="float-left grid3">
                    <h2 class="h6 bold">Travel Funds</h2>
                    <fieldset class="box2">
                        <label class="bold block">Confirmation Number: <input class="block" type="text" /></label>
                        <label class="br bold block">Passenger First Name: <input class="block" type="text" /></label>
                        <label class="br bold block">Passenger Last Name: <input class="block" type="text" /></label>
                        <button class="br">Apply Funds</button>
                    </fieldset>
                </div>
                <div class="float-left grid3">
                    <h2 class="h6"><b>southwest</b>giftcard&reg;</h2>
                    <fieldset class="box2">
                        <label class="bold block block">Card Number: <input class="block" type="text" /></label>
                        <label class="br bold block">Security Code: <br /><input class="inline-block" type="text" size="6" /> <a class="ico info" href="#"></a></label>
                        <button class="br">Apply Funds</button>
                    </fieldset>
                </div>
                <div class="float-left grid3">
                    <h2 class="h6 bold">Southwest LUV Voucher</h2>
                    <fieldset class="box2">
                        <label class="bold block">Voucher Number: <input class="block" type="text" /></label>
                        <label class="br bold block">Security Code: <br /><input class="inline-block" type="text" size="6" /> <a class="ico info" href="#"></a></label>
                        <button class="br">Apply Funds</button>
                        <p class="br"><b>Note:</b> LUV Vouchers will not be applied as payment toward government-imposed charges and fees.</p>
                   </fieldset>
                </div>
            </div>
            <p class="br"><b>Note:</b> All nonrefundable funds applied toward the purchase of a new reservation remain nonrefundable. The new reservation inherits the earliest expiration date from any funds applied.</p>
            <div class="wrap br box2">
                <table class="fdetails">
                <caption class="h5 btr">Applied Travel Funds</caption>
                <thead>
                    <tr class="caption1 bold noborder">
                        <td>&nbsp;</td> 
                    <?php if($page=="reco") { ?>
                        <td class="bold" colspan="4">Applied Travel Funds Summary</td>
                    <?php } else { ?>
                    	<td class="bold" colspan="5">Applied Travel Funds Summary</td>
                    <?php } ?>
                        <td class="bold text-right">Total</td> 
                    </tr>
                </thead>
                <tfoot>
                    <tr class="alt3 bold">
                        <td>&nbsp;</td>
                        <td class="bold">Total Due Now</td>
					<?php if($page!="reco") { ?>
                        <td>&nbsp;</td>
                    <?php } ?>
                    	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td> 
                        <td class="bold text-right">$1862.30</td> 
                    </tr>
                </tfoot>
                <tbody id="travelFunds">
                    <tr class="alt3 line2">
                        <td class="text-center wdk-toggle wdk-toggle-opened" data-wdk-toggle-target="#travelFunds tr.air-charges"><small class="ico"></small></td>
                        <td class="bold"><span class="toggle">Air Charges</span></td>
					<?php if($page!="reco") { ?>
                        <td>&nbsp;</td>
                    <?php } ?>
                    	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td> 
                        <td class="bold text-right">$1842.30</td>
                    </tr>
                    <tr class="line2 air-charges wdk-toggle-opened">
                        <td>&nbsp;</td>
                        <td><span class="absolute">Boston to Cancun Round Trip</span></td>
                    <?php if($page!="reco") { ?>
                        <td>&nbsp;</td>
                    <?php } ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td> 
                        <td>&nbsp;</td> 
                        <td class="text-right">$2202.40</td>
                    </tr>
                    <tr class="line2 air-charges wdk-toggle-opened">
                        <td class="text-center vertical-top"><small class="ico valid"></small></td>
                        <td>Exchanged Ticket<br />JOHN DOE<br />Travel Fund WNSOPL-4685<br />Expiration: 05/04/2013</td>
                        <?php if($page!="reco") { ?><td><a href="#">Remove</a></td><?php } ?>
                        <td class="h8">Full Balance<br />$453.20</td>
                        <td>Funds Applied<br />$345.10</td>
                        <td class="h8">Funds Remaining<br />$108.10</td>
                        <td class="text-right">($345.10)</td>
                    </tr>
                    <tr class="line2 air-charges wdk-toggle-opened">
                        <td class="text-center vertical-top"><small class="ico valid"></small></td>
                        <td><b>southwest</b>giftcard&reg;<br />XXXXX-7563</td>
                        <?php if($page!="reco") {?><td><a href="#">Remove</a></td><?php } ?>
                        <td>Full Balance<br />$10.00</td>
                        <td>Funds Applied<br />$10.00</td>
                        <td>Funds Remaining<br />$0.00</td>
                        <td class="text-right">($10.00)</td>
                    </tr>
                    <tr class="line2 air-charges wdk-toggle-opened">
                        <td class="text-center vertical-top"><small class="ico valid"></small></td>
                        <td>Southwest LUV Voucher-8942<br />Expiration: 12/31/2012</td>
                        <?php if($page!="reco") {?><td><a href="#">Remove</a></td><?php } ?>
                        <td>Full Balance<br />$5.00</td>
                        <td>Funds Applied<br />$5.00</td>
                        <td>Funds Remaining<br />$0.00</td>
                        <td class="text-right">($5.00)</td>
                    </tr>
                    <tr class="line2 air-charges wdk-toggle-opened caption6">
                        <td class="text-center vertical-top"><small class="ico invalid"></small></td>
                        <td>JOHN DOE<br />Travel Fund WNSOPL-4685<br />Expiration: 12/31/2012</td>
                        <?php if($page!="reco") {?><td><a href="#">Remove</a></td><?php } ?>
                        <td>Full Balance<br />$125.00</td>
                        <td class="em0">Maximum number of Travel Funds reached or Travel Fund not applicable</td>
                        <td>&nbsp;</td>
                        <td class="text-right">($0.00)</td>
                    </tr>
                    <tr class="line2 air-charges wdk-toggle-opened caption6">
                        <td class="text-center vertical-top"><small class="ico invalid"></small></td>
                        <td>OLIVER TWIST<br />Travel Fund WNIJKL-4221<br />Expiration: 01/05/2013</td>
                        <?php if($page!="reco") {?><td><a href="#">Remove</a></td><?php } ?>
                        <td>Full Balance<br />$1134.00</td>
                        <td class="em0">The name attached to this Travel Fund does not match the name above. Please edit the <a href="#">Passenger Name</a> and click Retry</td>
                        <td><button class="alt">Retry</button></td>
                        <td class="text-right">($0.00)</td>
                    </tr>
               
               <?php if($page!="reco") { ?>     
                    <tr class="alt3 line2">
                        <td class="text-center wdk-toggle wdk-toggle-opened" data-wdk-toggle-target="#travelFunds tr.additional-charges"><small class="ico"></small></td>
                        <td><span class="absolute toggle bold">Additional Charges</span></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="bold text-right">$20.00</td>
                    </tr>
                    <tr class="line2 additional-charges wdk-toggle-opened">
                        <td>&nbsp;</td>
                        <td>EarlyBird Check-In</td>
                        <td><a href="#">Remove</a></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="text-right">$20.00</td>
                    </tr>
				<?php } ?>
                                    
                </tbody>
                </table>
            </div>
        </div>
    </fieldset>