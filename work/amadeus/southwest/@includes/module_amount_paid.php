    <div class="text-right">
        <dl class="subtotal">
            <dt class="float-left text-left"><b>Amount Paid</b><?php if($i==1) {?><a href="#" class="small block">View Billing Details</a><?php } ?></dt>
            <dd class="text-right bold">
            	<?php if($i==1) {?>
            	$392.00
                <?php } else { ?>
                	<?php if($booking=='certificate-transitional') { ?>$30.00<?php } else { ?>$1129.20<?php } ?>
                <?php } ?>
			</dd>
        </dl>
		<hr class="nomargin" />
        <dl class="subtotal box9">
            <dt class="float-left text-left h4 bold">Trip Total</dt>
            <dd class="text-right h4 bold">
            	<?php if($i==1) {?>
            	$392.00
                <?php } else { ?>
                	<?php if($booking=='certificate-transitional') { ?>$30.00<?php } else { ?>$1129.20<?php } ?>
                <?php } ?>
            </dd>
        </dl>
    </div>