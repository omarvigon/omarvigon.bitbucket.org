</div>
<div class="main footer2 rel">
	<a class="ico swetravel absolute" href="#">Southwest Business Travel</a>
    <a class="ico swecargo absolute" href="#">Southwest Cargo</a>
	<div class="wrap line2t">
    	<p class="float-left midst"><span class="ico nuts float-left"></span> <b>Nuts About Southwest?</b> <a class="block" href="#">So are we, read the blog</a></p>
    	<p class="float-left midst h8"><span class="ico chat float-left midst"></span> Be a part of it all in the <a href="#">Community</a><br />Tell us what you think in <a href="#">Conversations</a></p>
        <p class="float-left midst"><span class="ico facebook float-left midst"></span> Become a Fan <a class="block" href="#">Facebook</a></p>
        <p class="float-left midst grid1"><span class="ico twitter float-left midst"></span> Follow Us <a class="block" href="#">Twitter</a></p>
        <p class="float-left midst grid1"><span class="ico youtube float-left midst"></span> Tune In <a class="block" href="#">YouTube</a></p>
        <p class="float-right midst"><span class="ico flickr float-left midst"></span> Share Photos <a class="block" href="#">Flickr</a></p>
    </div>
    <div class="wrap box">
    	<ul class="float-left">
            <li class="h8"><a class="bold text-13" href="#">About Southwest</a></li>
            <li><a class="em10" href="#">What's New</a></li>
            <li><a class="em10" href="#">Press Room</a></li>
            <li><a class="em10" href="#">Investor Relations</a></li>
            <li><a class="em10" href="#">Careers</a></li>
            <li><a class="em10" href="#">Supplier Information</a></li>
            <li><a class="em10" href="#">Customer Commitments</a></li>
            <li><a class="em10" href="#">The Southwest Difference</a></li>
            <li><a class="em10" href="#">Southwest Citizenship</a></li>
            <li class="btr"><a class="em10" href="#">Sponsorships</a></li>                        
        </ul>
        <ul class="float-left big">
            <li class="h8"><a class="bold text-13" href="#">Southwest Travel Experience</a></li>
            <li><a class="em10" href="#">Finding Low Fares</a></li>
            <li><a class="em10" href="#">Booking Your Trip</a></li>
            <li><a class="em10" href="#">Moving Through The Airport</a></li>
            <li><a class="em10" href="#">Boarding Your Flight</a></li>
            <li><a class="em10" href="#">During Your Flight</a></li>
            <li><a class="em10" href="#">Staying Connected</a></li>
            <li class="btr"><a class="em10" href="#">Popular Routes</a></li>                      
        </ul>
        <ul class="float-left">
            <li class="h8"><a class="bold text-13" href="#">Southwest Products</a></li>
            <li><a class="em10" href="#">EarlyBird Check-In</a></li>
            <li><a class="em10" href="#">Business Select</a></li>
            <li><a class="em10" href="#"><b>southwest</b>giftcard&reg;</a></li>
            <li><a class="em10" href="#">Mobile</a></li>
            <li><a class="em10" href="#">WiFi Hotspot</a></li>
            <li><a class="em10" href="#">Pets</a></li>
            <li class="btr"><a class="em10" href="#">Business Travel &amp; Groups</a></li>            
        </ul>
        <ul class="float-left"> 
            <li class="h8"><a class="bold text-13" href="#">Customer Service</a></li>
            <li><a class="em10" href="#">Contact Us</a></li>
            <li><a class="em10" href="#">FAQs</a></li>
            <li><a class="em10" href="#">Travel Tools</a></li>
            <li><a class="em10" href="#">Airport Information</a></li>
            <li><a class="em10" href="#">Baggage Policies</a></li>
            <li class="btr"><a class="em10" href="#">Refunds</a></li>                       
        </ul>
        <ul class="float-left noborder">
            <li class="h8"><a class="bold text-13" href="#">My Account</a></li>
            <li><a class="em10" href="#">Login</a></li>
            <li><a class="em10" href="#">Enroll in Rapid Rewards</a></li>
            <li class="btr"><a class="em10" href="#">Need help logging in?</a></li>                       
        </ul>
        <span class="ico expand toggle absolute"></span>
    </div>