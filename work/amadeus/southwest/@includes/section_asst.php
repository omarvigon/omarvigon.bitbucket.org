    <h1 class="h1">Assistance with Disabilities</h1>
    <h2 class="h2">(Passenger 1 - John Doe)</h2>
	<p class="br">Each category listed has travel requirements that Customers with disabilities need to know prior to traveling. Please click on "More Information" to view additional information.</p>
    
    <form action="#" method="post">
    	<div class="box2">
            <fieldset class="wrap br">
                <legend class="grid3 text-right"><b>Assistance with wheelchair</b></legend>
                <div class="float-left indent3">
                    <p><label><input type="radio" name="radio_1" checked="checked" /> No wheelchair assistance needed.</label></p>
                    <p><label><input type="radio" name="radio_1" /> Can walk but need assistance to and from gate.</label> <a class="ico info" href="#"></a></p>
                    <p><label><input type="radio" name="radio_1" /> Need lift/transfer assistance to and from aircraft seat.</label> <a class="ico info" href="#"></a></p>
                </div>
            </fieldset>
            <fieldset class="wrap hr">
                <legend class="grid3 text-right"><b>Personal wheelchair stowage</b></legend>
                <div class="float-left indent3">
                    <p><label><input type="radio" name="radio_2" checked="checked" /> No wheelchair stowage needed.</label></p>
                    <p><label><input type="radio" name="radio_2" /> Manual wheelchair.</label> <a class="ico info" href="#"></a></p>
                    <p><label><input type="radio" name="radio_2" /> Powered wheelchair with spillable batteries.</label> <a class="ico info" href="#"></a></p>
                    <p><label class="indent3">Number of spillable batteries: <select><option>0</option></select></label></p>
                    <p><label><input type="radio" name="radio_2" /> Powered wheelchair with non-spillable batteries.</label> <a class="ico info" href="#"></a></p>
                    <p><label class="indent3">Number of nonspillable batteries: <select><option>0</option></select></label></p>
                </div>
            </fieldset>
            <fieldset class="wra hr">
                <legend class="grid3 text-right"><b>Assistance in the airport and with boarding</b></legend>
                <div class="float-left indent3">
                    <p><label><input type="checkbox" /> Blind or have low vision.</label></p>
                    <p><label><input type="checkbox" /> Deaf or hard of hearing.</label> <a class="ico info" href="#"></a></p>
                    <p><label><input type="checkbox" /> Cognitive and developmental disabilities.</label> <a class="ico info" href="#"></a></p>
                </div>
            </fieldset>
            <fieldset class="wrap hr">
                <legend class="grid3 text-right bold"><b>Assistance animals for Customers with disabilities</b></legend>
                <div class="float-left indent3">
                    <p><label><input type="checkbox" /> Traveling with trained assistance animal.</label> <a class="ico info" href="#"></a></p>
                    <p><label><input type="checkbox" /> Traveling with emotional support animal.</label> <a class="ico info" href="#"></a></p>
                </div>
            </fieldset>
            <fieldset class="wrap hr">
                <legend class="grid3 text-right"><b>Other assistance requirements</b></legend>
                <div class="float-left indent3">
                    <p><label><input type="checkbox" /> Have peanut dust allergy.</label> <a class="ico info" href="#"></a></p>
                    <p><label><input type="checkbox" /> Bringing my own approved Portable Oxygen Concentrator.</label> <a class="ico info" href="#"></a></p>
                </div>
            </fieldset>
        </div>
        <p class="hr"><b>Note:</b> Please be sure to inform a Southwest Airlines Agent or Skycap at your first point of contact upon arrival at the airport, either at the Skycap podium or ticket counter, if you have requested assistance within the airport.</p>
        
        <h4 class="h5 bold text-right hr">Continue and apply these options</h4>
        <?php include("module_footer_continue.php");?>
	</form>