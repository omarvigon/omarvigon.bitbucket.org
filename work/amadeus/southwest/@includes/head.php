<?php include("php_start.php");?>  
<!DOCTYPE html>
<html lang="<?php echo LANG;?>">
<head>
    <title>Southwest Airlines</title>
    <meta charset="utf-8" />
	<link rel="icon" href="../@WDS_STATIC_FILES_PATH@/img/favicon.ico" />
    <link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/jquery.qtip.css" />
    <link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/main.css" />
    <!--[if IE 6]><link rel='stylesheet' type='text/css'  href='../@WDS_STATIC_FILES_PATH@/css/ie6.css'/><![endif]-->
    <!--[if IE 7]><link rel='stylesheet' type='text/css'  href='../@WDS_STATIC_FILES_PATH@/css/ie7.css'/><![endif]-->
    <!--[if IE 8]><link rel='stylesheet' type='text/css'  href='../@WDS_STATIC_FILES_PATH@/css/ie8.css'/><![endif]-->
    <!--[if IE 9]><link rel='stylesheet' type='text/css'  href='../@WDS_STATIC_FILES_PATH@/css/ie9.css'/><![endif]-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="../@WDS_STATIC_FILES_PATH@/js/selectivizr.js"></script>
    <![endif]-->
</head>
<body id="<?php echo $page;?>" class="flow-<?php echo $booking;?> js-disabled">