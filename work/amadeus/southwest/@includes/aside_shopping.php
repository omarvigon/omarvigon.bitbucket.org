<?php if ($login) { ?>
	<div class="article<?php if($_GET["login"]=="nonrr") { ?> first<?php } ?>">
    	<h3 class="h6 bold wdk-toggle wdk-toggle-opened" data-wdk-toggle-target="#shoppingCartSlope, #shoppingCartContent"><small class="ico"></small> My Cart</h3>
<?php } else { ?>
    <div class="article first">
    	<h3 class="h6 bold wdk-toggle wdk-toggle-opened" data-wdk-toggle-target="#shoppingCartSlope, #shoppingCartContent"><small class="ico"></small> Shopping Cart</h3>
<?php } ?>        
        <div class="background">
            <small class="slope s3 wdk-toggle-opened" id="shoppingCartSlope"></small>    
            
            <?php if ($page=="ocup" || $page=="occl") { ?>
            <div id="shoppingCartContent" class="wdk-toggle-opened">
                <h4 class="bold h4 em5"><span class="ico plane"></span> Air</h4>
                <p class="br">Please select a flight<?php if ($page!="occl") { ?> and click "Continue"<?php }?>.</p>
            </div>
            
            <?php } else if($page=="conf" || $page=="rconf") { ?>
            <div id="shoppingCartContent" class="wdk-toggle-opened" role="wdk-shopping-cart">
                <h4 class="text-13 bold">Your purchase is complete.</h4>
                <p><b>Get text or email updates on your flight.</b> <a href="http://southwest.com/flight/flight-notification-subscribe.html">Sign up for Flight Status Messaging</a></p>
            </div>
            
            <?php } else { ?>
            <div id="shoppingCartContent" class="wdk-toggle-opened cart">
                <?php if(($booking=="revenue" && $page=="purc") || ($booking=="revenue" && $page=="reco")) { ?>
                <p class="message small">Earn at least <b class="em5">6,012 Points</b> when you take this trip.</p>
                <?php } ?>
                <p class="float-right brsmall"><a href="#">Modify</a><?php if($page!="reco") { ?> | <a href="#">Remove<?php }?></a></p>
                <h5 class="bold h4 em5 wdk-toggle wdk-toggle-closed" data-wdk-toggle-target="#shoppingCartCollapsed, #shoppingCartExpanded"><?php if($page!="reco") { ?><small class="ico"></small> <?php }?>Air</h5>
                    
                <?php /* Collapsed shopping cart */ ?>
                <div role="wdk-shopping-cart" class="<?php if($page=="reco") { ?>none wdk-toggle-closed<?php }else{?>wdk-toggle-opened<?php }?>" id="shoppingCartCollapsed">
                    <hr class="clear line2" />
                    <h6 class="bold">Depart - Oct. 1</h6>
                    <p>DAL <small class="ico arrow"></small> CUN</p>
                    <h6 class="bold">Return - Oct. 16</h6>
                    <p>CUN <small class="ico arrow"></small> DAL</p>     
                    <p class="total clear br"><var class="float-right bold h6">$1,886.20</var> <span class="bold h6 block">Trip Total</span></p>
                    <p class="br small">We'll reserve the flight upon purchase completion.</p>
                </div>
                <?php /* Expanded shopping cart */ ?>
                <div role="wdk-shopping-cart" class="<?php if($page=="reco") { ?>wdk-toggle-opened<?php }else{?>none wdk-toggle-closed<?php }?>" id="shoppingCartExpanded">
                    <hr class="clear line2" />
                    <h4 class="date float-left grid2 text-center"><em class="block small">OCT 1</em> <var class="block h5">FRI</var></h4>                
                    <h6 class="bold">Depart <em class="bold h8">Flt 1184 / 2483</em></h6>
                    <p class="wrap">
                        <span class="float-left"><b>DAL</b><span class="block">7:35 AM</span></span>
                        <small class="ico arrow float-left"></small>
                        <span class="float-left"><b>CUN</b><span class="block">1:50 PM</span></span>
                        <span>
							<?php if($booking=="companion"){?>
                            Companion
                            <?php }else if($booking=="certificate-promotional"){?>
                            Promotional Award
                            <?php }else if($booking=="certificate-transitional"){?>
                            Standard Award
                            <?php }else{?>
                            Business Select fare
                        <?php }?>
                        </span>
                    </p>
                    
                    <hr class="clear line2" />         
                    <h4 class="date float-left grid2 text-center"><em class="block small">OCT 1</em> <var class="block h5">FRI</var></h4>
                    <h6 class="bold">Return <em class="bold h8">Flt 1352 / 682</em></h6>
                    <p class="wrap">
                        <span class="float-left"><b>CUN</b><span class="block">7:00 AM</span></span>
                        <small class="ico arrow float-left"></small>
                        <span class="float-left"><b>DAL</b><span class="block">10:10 AM <small class="block">(overnight)</small></span></span>
                        <span>
							<?php if($booking=="companion"){?>
                            Companion
                            <?php }else if($booking=="certificate-promotional"){?>
                            Promotional Award
                            <?php }else if($booking=="certificate-transitional"){?>
                            Standard Award
                            <?php }else{?>
                            Business Select fare
                            <?php }?>
                        </span>
                    </p>
                                    
                    <hr class="clear line2" />
                    <h6 class="bold">Cost Breakdown</h6>
                    <p>Adult $919.50 x1 <a href="../popins/frbd.php?booking=<?php echo $booking ;?>" class="wdk-tooltip float-right bold" data-wdk-tooltip-ajax data-wdk-tooltip-classes="southwest-tooltip" data-wdk-tooltip-my="right center" data-wdk-tooltip-at="left center">$471.90</a></p>   
                    <p class="total clear br"><var class="float-right bold h6">$1,886.20</var> <span class="bold h6 block">Air Total</span></p>
                    <?php if($page!="reco") { ?><p class="br small">We'll reserve the flight upon purchase completion.</p><?php }?>
                    
                    <?php if($page!="fare" && $flow!="exchange") { ?>
                    	<hr class="line12" />
                        <h5 class="line2 bold h5">Add-ons</h5>
                        <p class="float-right"><a href="#" class="smaller">Modify</a>|<a href="#" class="smaller">Remove</a></p>
                        <h6 class="bold smaller">EarlyBird Check-In</h6>                    
                        <p class="br">Total<span class="float-right bold">$10.00</span></p>       
                        <p class="total clear"><var class="float-right bold h6">$10.00</var> <span class="bold h6 block">Add-Ons</span></p>       
                        <p class="total clear br"><var class="float-right bold h6">$15.00</var> <span class="bold h6 block">Trip Total</span></p>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        </div>
	</div>