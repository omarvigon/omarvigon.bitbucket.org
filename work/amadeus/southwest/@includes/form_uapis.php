	<fieldset class="wrap">
        <label class="float-left bold">Passport Number: <?php if(isset($passengerNumber) && $passengerNumber==1) {?><span class="em0">*</span><?php }?>
        	<input class="block input-large" type="text" <?php if(isset($passengerNumber) && $passengerNumber==1) {?>value="XXX-123456"<?php }?>/>
		</label>
        <label class="float-left bold midst">Passport Issued By: <?php if(isset($passengerNumber) && $passengerNumber==1) {?><span class="em0">*</span><?php }?>
        	<select class="block"<?php if(isset($passengerNumber) && $passengerNumber==2) {?> disabled="disabled"<?php }?>><option>UNITED STATES OF AMERICA - (US)</option></select>
		</label>
    </fieldset>
    <label class="block bold">Nationality: <?php if(isset($passengerNumber) && $passengerNumber==1) {?><span class="em0">*</span><?php }?>
    	<select class="block"<?php if(isset($passengerNumber) && $passengerNumber==2) {?> disabled="disabled"<?php }?>><option value="US">UNITED STATES OF AMERICA - (US)</option></select>
	</label>
    <fieldset class="wrap">
        <fieldset class="float-left bold input-large">Passport Expiration Date: <?php if(isset($passengerNumber) && $passengerNumber==1) {?><span class="em0">*</span><?php }?>
            <p>
            <select class="grid5"<?php if(isset($passengerNumber) && $passengerNumber==2) {?> disabled="disabled"<?php }?>><option>Month</option></select> 
            <select class="grid5"<?php if(isset($passengerNumber) && $passengerNumber==2) {?> disabled="disabled"<?php }?>><option>Year</option></select>
            </p>
        </fieldset>
        <label class="float-left bold midst">Country of Residence: <?php if(isset($passengerNumber) && $passengerNumber==1) {?><span class="em0">*</span><?php }?>
        	<select class="block"<?php if(isset($passengerNumber) && $passengerNumber==2) {?> disabled="disabled"<?php }?>><option value="US">UNITED STATES OF AMERICA - (US)</option></select>
		</label>
    </fieldset>
    <?php if($page=="uapis") {?>
    <fieldset class="wrap">
        <label class="float-left bold">Emergency Contact Name: <input class="block input-large" type="text"></label>
        <fieldset class="float-left grid5 bold midst">Phone Number: <p><select class="grid3"><option>US (+1)</option></select> <input type="text" size="16"/></p></fieldset>
    </fieldset>
    <?php }?>