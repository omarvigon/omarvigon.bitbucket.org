    <h1 class="h1">EarlyBird Check-In Purchase</h1>
    <h2 class="h4">Payment</h2>
    
    <table class="fare small hr">
	<caption class="bold">EARLYBIRD CHECK-IN PRICING</caption>
    <thead>
    <tr class="caption1 bold">
        <td class="line4">Passenger</td>
        <td class="line4 grid1">Price</td>
        <td class="line4">Trip</td>
        <td class="line4">Routing</td>
    </tr>
    </thead>
    <tfoot>
    <tr class="line12">
	    <td class="box2 bold">Total</td>
        <td class="box2 text-right bold">$30</td>
        <td class="text-right" colspan="2"><a href="#" class="small">Edit flight choices</a></td>
    </tr>
    </tfoot>
    <tbody>
    <tr>
        <td class="box2"><small class="ico check"></small>John Doe</td>
        <td class="box2 text-right bold">$10</td>
        <td class="box1">Depart</td>
        <td class="box1"><b>Boston, MA (BOS)</b> to <b>Dallas, TX (DAL)</b></td>
    </tr>
    <tr>
        <td class="box2"><small class="ico check"></small>John Doe</td>
        <td class="box2 text-right bold">$10</td>
        <td class="box1">Return</td>
        <td class="box1"><b>Dallas, TX (DAL)</b> to <b>Boston, MA (BOS)</b></td>
    </tr>
    <tr>
        <td class="box2"><small class="ico check"></small>Jane Doe</td>
        <td class="box2 text-right bold">$10</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
    </table>
    <p class="small">Note: EarlyBird Check-In purchases are non refundable.</p>
    
    <form action="#" method="post">
    	<fieldset class="box2 br">
    		<?php include("module_payment_preference.php");?>
        </fieldset>
        <h4 class="h5 bold text-right hr">Complete this purchase?</h4>
        <p class="text-right br"><button type="submit" class="alt">Continue</button></p>
    </form>