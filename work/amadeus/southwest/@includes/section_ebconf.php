    <h1 class="h1">EarlyBird Check-In Option Purchase</h1>
    <h2 class="br line2"><small class="float-left ico done midst br"></small> <span class="h4">EarlyBird Check-In has been added to reservation #4HFZ2J. Your payment of $20.00 has been processed successfully.</span> <a href="#"><small class="ico print"></small> Print this page to serve as your receipt</a></h2>
    <?php include("table_itinerary.php");?>
    
    <table class="fare hr">
	<caption class="midst"><b>BILLING</b> (Receipt #: 5260600012881, 5260600012882)</caption>
    <thead>
    <tr class="caption1 bold">
        <td class="line4">Credit card holder</td>
        <td class="line4">Form of payment</td>
        <td class="line4">Billing Address</td>
        <td class="line4">Amount</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>John Doe</td>
        <td>VISA XXXXXXXXXXX-1111</td>
        <td>380 Summer Street, New York, AK 12342-3212</td>
        <td>$20.00</td>
    </tr>
    </table>