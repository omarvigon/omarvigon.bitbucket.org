/*tooltip.js Setting global option for tooltips*/
$.extend(true, wdk.annotations.tooltip.options, {
	_addButton: true,
	content : {
		title: {
			text: " "
		}
	},
	position: {
		at: "top center",
		my: "bottom center",
		adjust: {
			method: "none none"
		}
	},
	style: {
		tip: {
			width: 27,
			height: 14
		},
		classes: "southwest-tooltip"
	}
});

$(document).ready(function() {
	$("body").removeClass("js-disabled").addClass("js-enabled");
	loadCss();
	
	var modify_search=$("#ocup form.box1");//rocup?
	
	modify_search.find("fieldset:first-child a.float-right").removeClass("none");
	modify_search.find("em.float-left").removeClass("none");
	modify_search.find("fieldset:nth-child(2) label:nth-child(3)").addClass("none");
	modify_search.find("fieldset:nth-child(3)").addClass("none");
	modify_search.find("button.absolute").addClass("none");
	
	modify_search.find("a.float-right,em.float-left").click( function()
	{
		modify_search.find("a.float-right").toggleClass("none");
		modify_search.find("em.float-left").toggleClass("none");
		modify_search.find("fieldset:nth-child(2) label:nth-child(3)").toggleClass("none");
		modify_search.find("fieldset:nth-child(3)").toggleClass("none");
		modify_search.find("button.absolute").toggleClass("none");
		return false;
	});
	
	var contact=$(".contact-info");
	contact.find(".float-right > div").addClass("none");
	contact.find("div.float-left p").addClass("none");
	contact.find("select").change(function()
	{
		contact.find(".float-right > div").addClass("none");
		contact.find("div.float-left p").addClass("none");
		$("#callMeNote").hide();
		
		switch($(this).val())
		{
			case "Text":
			contact.find('.float-right div.phone-contact').removeClass("none");
			contact.find('p.text-contact').removeClass("none");
			break;
			
			case "Call":
			contact.find(".float-right div.phone-contact").removeClass("none");
			contact.find('p.phone-contact').removeClass("none");
			break;
			
			case "Email":
			contact.find(".float-right div.email-contact").removeClass("none");
			break;
			
			default:
			break;
		}
	});
	
	$("button[value='applyfunds']").click(function()
	{
		$(this).prev().addClass("none");
		$(this).addClass("none");
		$(this).next().removeClass("none");
		return false;
	});
	
	$("div[role=wdk-shopping-cart] h5 .ico").click(function()
	{
		$("div[role=wdk-shopping-cart]").toggle();   
	});
	
	$("td.price").click(function() //teasers
	{
		var row=$(this).parent();
		var table=row.parent();
		var target=$("div.teaser-"+$(this).data("family")).clone();
		
		clean();
		
		$(this).find("input").attr("checked","checked");
		if(target.html()!=null)
		{
			$(this).addClass("on");
			row.after("<tr class='teaser'><td colspan='10'><div class='"+target.attr("class")+"'>"+target.html()+"</div></td></tr>");
		}
		
		$(".teaser span.close").click(function()
		{
			table=$(this).parentsUntil("table");
			clean();
		});
		
		function clean()
		{
			table.find("td.on").removeClass("on");
			table.find("tr.teaser").remove();
		}
	});

	var footer2=$("div.footer2");
	footer2.find("span.toggle").click(function()
	{
		$(this).toggleClass("expand");
		$(this).toggleClass("collapse");
		footer2.find("li:not(:first-child)").toggleClass("none");
		footer2.find("div.box").toggleClass("alt");
		return false;
	});
	footer2.find("span.toggle").click();
	
	$("a[href='#']").attr("href","javascript:void(0)");	
	
});

/**
Loads the browser-specific css.
*/
function loadCss() {
	
	var browser = jQuery.uaMatch(navigator.userAgent).browser;
	var cssLink = $('<link />');
	cssLink.attr("rel","stylesheet")

	switch(browser)
	{				 
	 	case 'webkit':
			if((navigator.userAgent.indexOf("Safari")>0) && (navigator.userAgent.indexOf("Chrome")==-1))
				cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/safari.css");
			else 
				cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/webkit.css");
	 	break;
	 	
		case 'opera':
			cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/opera.css");
		break;
	 	
		case 'mozilla':
			cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/mozilla.css");
	 	break;
		
	}
	$("head").append(cssLink);

	/* @group Displaying error message */

    $('#errors').click(function()
	{
		$('.error_messages').toggleClass("none");
    });

	/* @group Adding early message */

    $('#wdk-early input').click(function()
	{
		$('#wdk-early .indent').hide();
    	$(this).parent().next('.indent').show();
    });

	/* @group Payment panel */

	var paymentForm=$(".wdk-payment-form");
    paymentForm.find("input").click(function()
	{
    	paymentForm.next().toggleClass("none");
    });

    $('.wdk-passenger-country').change(function()
	{
    	if($(this).find('option:selected').attr('value') != 'US')
		{
    		$(this).parent().next('fieldset').toggleClass('none'); 		
    		$(this).parents('.wdk-payment').find('fieldset.wdk-foreign-country').show();    		
    	}
    	else
		{
    		$(this).parent().next('fieldset').toggleClass('none');
			$('.wdk-payment fieldset.wdk-foreign-country').hide();
    	}
    })

	/* @group Add slope icon */
	if ($.browser.msie && $.browser.version == 6 || $.browser.version == 7) {
		$('.nav div ul li:first-child').prepend('<small class="float-right slope"></small>');
		$('.steb p.done').prepend('<small class="ico done"></small>');
		$('p.check').prepend('<small class="ico check"></small>');
	}
}