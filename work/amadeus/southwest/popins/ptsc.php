<?php include("../@includes/head.php");?>
<div class="popin pop8">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">How are points calculated?</h4>
    </div>
    <div class="inner">
        <table>
        <caption><b class="h5">Earning points</b> - <b class="h5 em0">Sample Calculation</b><span class="block">Example of $125 fare points calculation</span></caption>
        <thead>
            <tr class="text-center caption3">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="caption2"><b class="block">Business Select</b>Maximum Points<b class="block">x12</b></td>
                <td class="caption2"><b class="block">Anytime</b>More Points<b class="block">x10</b></td>
                <td class="caption2"><b class="block">Wanna Get Away</b>Base Points<b class="block">x6</b></td>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="5">&nbsp;</td></tr>
            <tr class="box4">
                <th class="bold">Base Fare</th>
                <td class="text-right">$100</td>
                <td class="em3 bold text-center">1,200 pts</td>
                <td class="em3 bold text-center">1,000 pts</td>
                <td class="em3 bold text-center">600 pts</td>
            </tr>
            <tr>
                <th class="bold">Govt Taxes &amp; Fees</th>
                <td class="text-right">$25</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="caption3">&nbsp;</td>
            </tr>
            <tr>
                <th class="bold">Displayed Fare</th>
                <td class="bold text-right"><p class="br">$125</p></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        </table>	
        <p class="br">Points are not calculated on the Govt Taxes &amp; Fees.</p>
    </div>
</div>    
<?php include("../@includes/foot.php");?>