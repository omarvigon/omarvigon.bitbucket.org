<?php include("../@includes/head.php");?>
<div class="popin pop3">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">Fare Details</h4>
    </div>
    <table class="fdetails line4">
    <thead>
    <tr class="text-center bold line5">
        <td class="grid4">&nbsp;</td>
        <td class="line6">Business Select</td>
        <td class="line6">Anytime</td>
        <td class="line6">Senior</td>
        <td class="line6">Wanna Get Away</td>
        <td class="line6">DING!</td>
    </tr>
    </thead>
    <tbody>
    <tr class="line5">
        <th class="bold">Fully Refundable</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="bold">Priority Boarding</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="bold">Fly By&trade; (Priority Security Lane)</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="bold">Same-day Changes</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="text-center line6">applicable fare<br />difference applies</td>
        <td class="text-center line6">applicable fare<br />difference applies</td>
        <td class="text-center line6">applicable fare<br />difference applies</td>
    </tr>
    <tr class="line5">
        <th class="bold">Premium Drink (valid day of travel)</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="bold">Refundable Points and Fees</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
    </tr>
    <tr class="line5">
        <th class="bold">Two Free Checked Bags, Snacks, and MORE!</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
    </tr>
    <tr>
        <th class="bold">Rapid Rewards® Earning Formula</th>
        <td class="text-center box2 line6">12 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">10 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">6 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">6 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">6 x<br />(Base Fare + Excise Tax)</td>
    </tr>
    </tbody>
    </table>
    <p class="box4">Restrictions apply. See <a href="#">Full Details</a>
</div>
<?php include("../@includes/foot.php");?>
