<?php include("../@includes/head.php");?>
<?php include("../@includes/php_panel.php");?>   
<div class="popin pop7">
    <div class="box5 midst2">
    	<h4 class="h5 bold brsmall">We are unable to price the flight you selected</h4>
        <div class="inner br">
            <p>The next lowest fares for the flights you selected are listed below. If you accept the new price, please click "Accept and Continue" to proceed. If not, click "Modify Flight(s)" to go back to the flight selections.</p>
            <?php if($booking!="redemption") { ?>
            <p class="br">All fares are rounded up to the nearest dollar.</p>
            <?php } ?>
            <table class="fare br">
            <thead>
            <tr class="caption2 em10">
                <td class="line7a line4 box4"><b>Date</b></td>
                <td class="line7a line4 box4"><b>Depart</b></td>
                <td class="line7a line4 box4"><b>Arrive</b></td>
                <td class="line7a line4 text-center h8"><b class="block">Flight</b><small class="smaller">(% ontime)</small></td>
                <td class="line7a line4 text-center"><b>Routing</b></td>
                <td class="line7a line4 text-center h8"><b class="block">Travel Time</b><small class="smaller">(hh:mm)</small></td>
                <td class="line7a line4 text-center"><b>Fare Family</b></td>
            </tr>
            </thead>
            <tbody>
            <tr class="h8">
                <td class="line8">Oct 13</td>
                <td class="line7a"><strong class="bold text-12">6:15</strong> <span class="em10">AM</span></td>
                <td class="line7a"><strong class="bold text-12">7:15</strong> <span class="em10">AM</span></td>
                <td class="line7a text-center"><a href="#">1628</a></td>
                <td class="line7a text-center"><a href="#">Nonstop</a></td>
                <td class="line7a text-center">1:00</td>
                <td class="line7a text-center"><a href="#">Business select</a></td>
            </tr>
            <tr class="h8 alt3">
                <td>Oct 14</td>
                <td class="line7a line8"><strong class="bold text-12">6:30</strong> <span class="em10">AM</span></td>
                <td class="line7a line8"><strong class="bold text-12">7:30</strong> <span class="em10">AM</span></td>
                <td class="line7a text-center line8"><a href="#">201</a></td>
                <td class="line7a text-center line8"><a href="#">Nonstop</a></td>
                <td class="line7a text-center line8">1:00</td>
                <td class="line7a text-center line8"><a href="#">Business select</a></td>
            </tr>
            <tr>
                <td colspan="6"></td>
                <td class="line7a bold text-center box2"><span class="bold em0">New price</span><br />
                	<?php if($booking=="redemption") { ?>
                    10,145 pts<a href="#" class="block var small">+$2.50</a>
                    <?php } else { ?>
                    $124
                    <?php } ?>                	
                </td>
            </tr>
            </tbody>
            </table>
            <p class="wrap text-right hr btr">
                <a href="#" class="inline-block button">Modify flight(s)</a> <button type="submit" class="alt">Accept and Continue</button>
            </p>
        </div>
    </div>
</div>    
<?php include("../@includes/foot.php");?>    