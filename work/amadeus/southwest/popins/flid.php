<?php include("../@includes/head.php");?>
<div class="popin pop2">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">Flight Details</h4><span> (2 stops, includes 1 plane change)</span>
    </div>
    <table class="vertical-top">
    <tr>
        <th class="text-center midst2">#230</th>
        <td class="shadowl">Depart Dallas (Love Field), TX (DAL)
            <dl class="indent em10">
            <dt class="absolute"><i>Stops:</i></dt>
            <dd><i>Houston, TX</i></dd>
            <dd><i>Dallas, TX</i></dd>
            <dd><i>Kansas City, KS</i></dd>
            </dl>
        </td>
        <td class="text-right midst2 h8">6:00 AM</td>
    </tr>
    <tr class="caption3">
        <td></td>
        <td class="shadowl em10">Arrive in Baltimore/Washington, MD (BWI)</td>
        <td class="text-right midst2 em10 h8">11:00 AM</td>
    </tr>
    <tr class="alt3">
        <th rowspan="2" class="text-center midst2">#431</th>
        <td class="shadowl em10">Change <span class="ico change"></span> in  Baltimore/Washington, MD (BWI)</td>
        <td class="text-right midst2 em10">2:50 AM</td>
    </tr>
    <tr class="alt3">
        <td class="shadowl">Arrive in Cancun, MX (CUN)</td>
        <td class="text-right midst2">3:55 PM</td>
    </tr>
    </table>
</div>    
<?php include("../@includes/foot.php");?>
