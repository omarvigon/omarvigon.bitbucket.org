<?php include("../@includes/head.php");?>
<div class="popin pop5">
    <h4 class="h3 bold">E-mail Itinerary</h4>
    <p>We have the following e-mail on file for this reservation. You can change and/or add an e-mail address to send the itinerary.</p>
    <hr class="line2 br" />
   	<label class="block bold">Email<input class="block grid9" type="text" /></label>
    <p class="br"><a href="#"><small class="ico add"></small>Add an other email</a> (up to 4)</p>
    <hr class="line2 br" />
    <p class="text-right br"><a href="#">Cancel</a> <button class="alt midst">Send</button></p>
</div>           
<?php include("../@includes/foot.php");?>