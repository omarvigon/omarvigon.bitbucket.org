<?php include("../@includes/head.php");?>
<div class="popin pop5">
    <h4 class="h3 bold">E-mail Reservation</h4>
    <p>We have the following email on file for this reservation. Would you like us to resend a confirmation e-mail with your reservation details to this address?</p>
    <hr class="line2 br" />
    <p class="em1">E-mail: JOHN.DOE@DOMAIN.COM</p>
    <p class="em1">Fax: (123)456-7890</p>
    <hr class="line2 br" />
    <p class="text-right br"><a href="#">Cancel</a> <button class="alt midst">Send</button></p>
</div>           
<?php include("../@includes/foot.php");?>