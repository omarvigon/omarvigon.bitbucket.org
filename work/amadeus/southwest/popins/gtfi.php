<?php include("../@includes/head.php");?>
<div class="popin pop1">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">Gov't taxes &amp; fees now included</h4>
    </div>
    <div class="inner">
        <h5 class="bold">Did you know?</h5>
        <p>In compliance with new Department of Transportation rules, the fares you see now include all government taxes and fees.</p>
    </div>
</div>    
<?php include("../@includes/foot.php");?>