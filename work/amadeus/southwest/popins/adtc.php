<?php include("../@includes/head.php");?>
<div class="popin pop2">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">Add to Calendar</h4>
    </div>
    <div class="inner">
        <h5>Export your <strong class="bold">Air</strong> booking from <strong class="bold">BOS to CUN</strong>  on <strong class="bold">05/25/2012</strong></h5>
        <p><small class="ico outlook"></small> <a href="#">Outlook&reg;</a> &emsp; <small class="ico calendar2"></small> <a href="#">iCal compatible</a></p>
        <hr class="caption3" />
        <h5>Export your <strong class="bold">Air</strong> booking from <strong class="bold">CUN to BOS</strong>  on <strong class="bold">05/25/2012</strong></h5>
        <p><small class="ico outlook"></small> <a href="#">Outlook&reg;</a>  &emsp; <small class="ico calendar2"></small> <a href="#">iCal compatible</a></p>
    </div>
</div>    
<?php include("../@includes/foot.php");?>