<?php include("../@includes/head.php");?>
<div class="popin pop6">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">Please Confirm to Remove Your Air</h4>
    </div>
    <div class="inner">
        <p>Just say the word, and we will delete your flight from your cart for you</p>
        <p class="text-right br">
        	<button>Do not remove</button>
            <button class="alt">Remove</button>
        </p>
    </div>
</div>    
<?php include("../@includes/foot.php");?>