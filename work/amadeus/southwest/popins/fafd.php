﻿<?php include("../@includes/head.php");?>
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">&nbsp;</h4>
    </div>
    <table class="fdetails line4">
    <thead>
    <tr class="text-center caption1 line5">
        <td>&nbsp;</td>
        <td class="h6 line6 bold">Business Select</td>
        <td class="h6 line6 bold">Anytime</td>
        <td class="h6 line6"><b>Senior</b> <sup><small class="em1">6</small></sup></td>
        <td class="h6 line6 bold">Wanna Get Away</td>
        <td class="h6 line6 bold">DING!</td>
    </tr>
    </thead>
    <tbody>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Fully Refundable</span><br />If you cancel your flight, you are eligible to receive 100% of your ticket value as a refund to your original form of payment. Southwest Travel Funds from a previous reservation that are applied toward a Business Select Fare will be refunded as residual travel funds (RTF) since the RTF would be the original form of payment for the Business Select transaction.</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Priority Boarding</span><br />Purchasing this fare will enable you to board the plane at the beginning of the "A" boarding group.</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Fly By™ (Priority Security Lane)</span><sup><small class="em1">1</small></sup><br />This lane will allow you to access the security screening process more quickly.</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Same-day Changes</span><br />On your original date of travel you are free to fly standby or make confirmed changes (if seats are available) for another flight to your destination at no additional cost.</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="text-center line6">applicable fare<br />difference applies</td>
        <td class="text-center line6">applicable fare<br />difference applies</td>
        <td class="text-center line6">applicable fare<br />difference applies</td>
    </tr>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Premium Drink (valid day of travel)</span><sup><small class="em1">2</small></sup><br />One premium beverage of your choice with Premium Drink Coupon</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
        <td class="line6">&nbsp;</td>
    </tr>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Refundable Points and Fees</span><sup><small class="em1">3</small></sup><br />If you cancel your flight, 100% of your ticket value can be applied to future travel for up to 12 months.</th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
    </tr>
    <tr class="line5">
        <th class="grid5"><span class="h6 bold">Two Free Checked Bags, Snacks, and MORE!</span><sup><small class="em1">4</small></sup></th>
        <td class="box2 text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
        <td class="text-center line6"><small class="ico done"></small></td>
    </tr>
    <tr>
        <th class="grid5"><span class="h6 bold">Rapid Rewards® Earning Formula</span><sup><small class="em1">5</small></sup><br />The number of Rapid Rewards points you will receive for your purchase.</th>
        <td class="text-center box2 line6">12 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">10 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">6 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">6 x<br />(Base Fare + Excise Tax)</td>
        <td class="text-center line6">6 x<br />(Base Fare + Excise Tax)</td>
    </tr>
    </tbody>
    </table>
    <ul class="br">
        <li class="small"><small class="em1">1</small>. Fly By&trade; at participating airports</li>
        <li class="small"><small class="em1">2</small>. One premium beverage of your choice on day of travel. Only customers 21 years of age or older will be served alcoholic beverages.</li>
        <li class="small"><small class="em1">3</small>. On cancellations, 100% of the ticket value may be applied to future travel to be completed for up to 12 months or by your ticket's expiration date, whichever comes first.</li>
        <li class="small"><small class="em1">4</small>. All fares on Southwest Airlines come with the following free amenities: two checked bags, online checkin, curbside checkin, as well as complimentary snacks, soft drinks and LIFT™ coffee onboard all flights. Baggage weight and size limits apply.</li>
        <li class="small"><small class="em1">5</small>. Earned points are calculated using base fare and excise tax.</li>
        <li class="small"><small class="em1">6</small>. Senior Fares - Applicable for passengers age 65 and over. Restrictions and seating limitations may apply. Senior fares cannot be combined with or priced with fares with a "Web Only" designation on the fare display screen.</li>
    </ul>
<?php include("../@includes/foot.php");?>