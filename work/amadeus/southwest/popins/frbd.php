<?php include("../@includes/head.php");?>
<div class="popin pop4">
    <div class="caption5 wrap">
    	<h4 class="h5 bold inline-block">Fare Breakdown</h4>
    </div>
    <table class="tdefault">
    <thead>
    <?php if($booking=="companion") { ?>
        <tr class="caption3"><th class="bold grid7">Fare</th><td class="text-right bold">Free</td></tr>
    </thead>
    <tfoot>
    	<tr><th class="bold grid7 text-right">$ Total</th><td class="text-right bold">$35.72</td></tr>
        <tr><td class="small" colspan="2"><sup class="float-left rindent-small btr">1</sup> Security Fee is the government-imposed September 11th Security Fee.</td></tr>
    </tfoot>
    <tbody>
        <tr class="nopadding"><th class="grid7">+ Segment Fee</th><td class="text-right">$0.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Passenger Facility Charge</th><td class="text-right">$9.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Security Fee<sup>1</sup></th><td class="text-right">$5.00</td></tr>
        <tr class="nopadding caption3"><th class="grid7">+ Mexico Tourism Tax</th><td class="text-right">$21.72</td></tr>
    <?php } else if($booking=="redemption") { ?>
        <tr><th class="bold grid7">Fare</th><td class="text-right bold">89,625 pts</td></tr>
    </thead>
    <tfoot>
    	<tr class="text-right"><th class="bold grid7">$ Total</th><td class="bold">$74.23</td></tr>
        <tr><td class="small" colspan="2"><sup class="float-left rindent-small btr">1</sup> Security Fee is the government-imposed September 11th Security Fee.</td></tr>
    </tfoot>
    <tbody>
        <tr class="nopadding"><th class="grid7">+ Segment Fee</th><td class="text-right">$0.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Passenger Facility Charge</th><td class="text-right">$9.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Security Fee<sup>1</sup></th><td class="text-right">$5.00</td></tr>
        <tr class="nopadding caption3"><th class="grid7">+ Mexico Tourism Tax</th><td class="text-right">$21.72</td></tr>
        <tr class="nopadding"><th class="grid7">Total per Passenger</th><td class="text-right">89,625 pts</td></tr>
        <tr class="nopadding"><th class="grid7"></th><td class="text-right">$74.23</td></tr>
        <tr class="nopadding"><th class="grid7"></th><td class="text-right h8">x 1 Passenger(s)</td></tr>
        <tr class="caption3 text-right"><th class="bold grid7">Points Total</th><td class="bold">89,625 pts</td></tr>
    <?php } else if($booking=="certificate-transitional" || $booking=="certificate-promotional") { ?>
        <tr class="caption3"><th class="bold grid7">Fare</th><td class="text-right bold">Free</td></tr>
    </thead>
    <tfoot>
    	<tr class="text-right"><th class="bold grid7">$ Total</th><td class="bold">$35.72</td></tr>
        <tr><td class="small" colspan="2"><sup class="float-left rindent-small btr">1</sup> Security Fee is the government-imposed September 11th Security Fee.</td></tr>
    </tfoot>
    <tbody>            
        <tr class="nopadding"><th class="grid7">+ Segment Fee</th><td class="text-right">$0.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Passenger Facility Charge</th><td class="text-right">$9.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Security Fee<sup>1</sup></th><td class="text-right">$5.00</td></tr>
        <tr class="nopadding caption3"><th class="grid7">+ Mexico Tourism Tax</th><td class="text-right">$21.72</td></tr>
        <tr class="nopadding"><th class="grid7">Total per Passenger</th><td class="text-right">Free</td></tr>
        <tr class="nopadding"><th class="grid7"></th><td class="text-right">$5.00</td></tr>
        <tr class="nopadding"><th class="grid7"></th><td class="text-right h8">x 1 Passenger(s)</td></tr>
        <tr class="caption3 text-right"><th class="bold grid7">Points Total</th><td class="bold">Free</td></tr>
	<?php } else { ?>
        <tr><th class="grid7">Base Fare</th><td class="text-right">$513.49</td></tr>
        <tr class="nopadding"><th class="grid7">+ Excise Taxes</th><td class="text-right">$38.51</td></tr>
        <tr class="caption3"><th class="grid7"></th><td class="text-right bold">$552.00</td></tr>
    </thead>
    <tfoot>
    	<tr class="text-right"><th class="bold grid7">Total</th><td class="bold">$587.72</td></tr>
        <tr><td class="small" colspan="2"><sup class="float-left rindent-small btr">1</sup> Security Fee is the government-imposed September 11th Security Fee.</td></tr>    
    </tfoot>
    <tbody>        
        <tr class="nopadding"><th class="grid7">+ Segment Fee</th><td class="text-right">$0.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Passenger Facility Charge</th><td class="text-right">$9.00</td></tr>
        <tr class="nopadding"><th class="grid7">+ Security Fee<sup>1</sup></th><td class="text-right">$5.00</td></tr>
        <tr class="nopadding caption3"><th class="grid7">+ Mexico Tourism Tax</th><td class="text-right">$21.72</td></tr>
        <tr><th class="grid7">Total per Passenger</th><td class="text-right">$573.66</td></tr>
        <tr class="nopadding caption3"><th class="grid7"></th><td class="text-right h8">x 1 Passenger(s)</td></tr>      
    <?php } ?>
   	</tbody>
	</table>         
</div>        
<?php include("../@includes/foot.php");?>
