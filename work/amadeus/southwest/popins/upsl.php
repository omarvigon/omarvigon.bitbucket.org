<?php include("../@includes/head.php");?>
<div class="popin pop7">
    <form action="#" method="post" class="box5 midst2">
    	<h4 class="h5 bold line9"><span class="ico close float-right toggle" title="Close"></span>May We Suggest</h4>

        <img src="../@WDS_STATIC_FILES_PATH@/img/business_options.png" alt="Business select" width="163" height="121" class="float-right" />
        <h5 class="text-12 br"><b class="h5">Upgrade to Business Select<sup>&reg;</sup></b> starting at $201<sup class="var smaller">1</sup></h5>
        <div class="midst">
            <ul>
                <li class="text-12"><small class="ico arrow2"></small>Priority Boarding group A1 - A15</li>
                <li class="text-12"><small class="ico arrow2"></small>More Rapid Rewards<sup class="absolute">&reg;</sup> &nbsp; Points</li>
                <li class="text-12"><small class="ico arrow2"></small>Breeze through security with Fly By<sup class="absolute">&reg;</sup> &nbsp; Priority Lane</li>
                <li class="text-12"><small class="ico arrow2"></small>Receive a premium drink on your day of travel</li>
            </ul>
                
            <h5 class="bold br">Includes Anytime Features</h5>
            <p>Fully Refundable | Same-day Changes | 2 Free Checked Bags<sup class="var smaller">2</sup></p>
            <p class="small"><sup class="var smaller">1</sup>Per Passenger, one-way. <sup class="var smaller">2</sup>Weight and size limits apply.</p>
    
            <div class="box3 box4 br">
                <label><input type="radio" /> <b>Yes</b>, upgrade my selected flights to Business Select<sup>&reg;</sup> for the additional cost listed below</label>
                <table class="small br">
                <thead>
                <tr class="alt3">
                    <td class="line8">&nbsp;</td>
                    <td class="line7a line8">Flight</td>
                    <td class="line7a line8">Upgrade From</td>
                    <td class="line7a line8">Add'l RR Points</td>
                    <td class="line7a line8">Add'l Cost</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="box2 line8 text-center"><input type="checkbox"></td>
                    <td class="box2 line7a line8">Depart</td>
                    <td class="box2 line7a line8"><a href="#" class="small">Anytime</a></td>
                    <td class="box2 line7a line8">+<small class="em3">1,404</small></td>
                    <td class="box2 line7a line8">add $28</td>
                </tr>
                <tr>
                    <td class="box2 line8 text-center"><small class="ico done"></small></td>
                    <td class="box2 line7a">Return</td>
                    <td class="box2 line7a"><a href="#" class="small">Business Select</a></td>
                    <td class="box2 line7a"></td>
                    <td class="box2 line7a"></td>
                </tr>
                </tbody>
                </table>
            </div>
            <label class="block bold midst br"><input type="radio" > No, thanks</label>
            <p class="wrap hr btr text-right">
                <label class="float-left midst"><input type="checkbox"> Don't show me this message again</label>
                <button type="submit" class="alt float-right">Continue</button>
            </p>
		</div>            
	</form>
</div>    
<?php include("../@includes/foot.php");?>