<?php include("../@includes/head.php");?>
<div class="popin pop2">
    <div class="caption5">
        <h4 class="h5 bold">Flight Information</h4>
    </div>
    <table class="tdefault">
    <caption class="h7 caption6 bold midst2">Ontime Performance</caption>
    <tr class="text-center">
        <td class="bold">Flight # 250</td>
        <td>&nbsp;</td>
        <td class="bold">Flight # 1140</td>
    </tr>
    <tr class="alt3 text-center">
        <td>NA</td>
        <td>% Ontime Arrival</td>
        <td>100%</td>
    </tr>
    <tr class="text-center">
        <td>NA</td>
        <td>% > 30 Minutes Late</td>
        <td>0%</td>
    </tr>
    <tr class="alt3 text-center">
        <td>NA</td>
        <td>% Cancellations</td>
        <td>0%</td>
    </tr>
    </table>
    <h5 class="h7 caption6 bold midst2">Aircraft Information</h5>
        <div class="grid5 float-left">
            <div class="box4">
                <strong class="bold">Aircraft Type:</strong> Boeing 737-700<br />
                <strong class="bold">No. of Seats:</strong> 137<br />
            </div>
        </div>
        <div class="grid5 inline-block">
            <div class="shadowl box4">
                <strong class="bold">Aircraft Type:</strong> Boeing 737-800<br />
                <strong class="bold">No. of Seats:</strong> 175<br />
            </div>
        </div>
    <h5 class="h7 caption6 bold midst2">Additional Information</h5>
    <p class="box4">The statistics above reflect the most recently published data and therefore may not be indicative of future ontime performance. <a href="#">Learn more.</a></p>
</div>
<?php include("../@includes/foot.php");?>
