/* WDK configuration */

/* Custom class for toggle annotations*/
wdk.annotations.toggle.className="toggle";

wdk.annotations.toggle.options = {
		triggerOpenedClass: "plus",
		triggerClosedClass: "minus",		
		targetOpenedClass: "none",
		targetClosedClass: " "
};

wdk.annotations.popin.options = {
		title: "",
		modal: true,
		autoOpen: true,
		resizable: false,
		draggable: false
	};