<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0;margin:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">YOUR REFUNDS DETAILS</th>
    </tr>
    <tr>
        <td height="5" align="left" style="font-size:height:5px;margin:0;padding:0"></td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="3" width="512" style="border-collapse:collapse;border-spacing:0;font-size:11px;margin:0">
    <tr>
        <th bgcolor="#eaeaea" colspan="4" style="background-color:#eaeaea;margin:0;text-align:left;font-size:11px;">Your Total refund summary</th>
    </tr>
    <tr>
        <td style="margin:0;">Paid Fare</td>
        <td style="margin:0;text-align:left">&nbsp;</td>
        <td style="font-weight:bold;margin:0;text-align:right">2,720.05$</td>
        <td style="margin:0;text-align:left" width="100">&nbsp;</td>
    </tr>
    <tr>
        <td style="margin:0;text-align:left">Used Fare</td>
        <td style="margin:0;text-align:left">&nbsp;</td>
        <td style="font-weight:bold;margin:0;text-align:right">-200.00$</td>
        <td style="margin:0;text-align:left" width="100">&nbsp;</td>
    </tr>
    <tr>
        <td style="margin:0;text-align:left">Tax to be refunded</td>
        <td style="margin:0;text-align:left">&nbsp;</td>
        <td style="font-weight:bold;margin:0;text-align:right">80.00$</td>
        <td style="margin:0;text-align:left" width="100">&nbsp;</td>
    </tr>
    <tr>
        <td style="margin:0;text-align:left">Penalty</td>
        <td style="margin:0;text-align:left">&nbsp;</td>
        <td style="font-weight:bold;margin:0;text-align:right">-100.00$</td>
        <td style="margin:0;text-align:left" width="100">&nbsp;</td>
    </tr>    
    <tr style="border-top:1px solid #ccc">
        <th bgcolor="#eaeaea" colspan="2" style="background-color:#eaeaea;color:#C0211D;margin:0;text-align:left;font-size:11px;">Sub-total to be refunded for the bound</th>
        <td bgcolor="#eaeaea" style="background-color:#eaeaea;color:#C0211D;font-weight:bold;margin:0;text-align:right">2500.00$</td>
        <td bgcolor="#eaeaea" style="background-color:margin:0;text-align:left" width="100">&nbsp;</td>
    </tr>
    <tr>
        <td style="color:#999;margin:0;text-align:left;padding-top:5px">* NB: Some of the data realted to the total to be refund were missing.</td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0">
    <tr>
        <td height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>