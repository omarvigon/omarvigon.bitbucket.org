<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">TRAVEL INSURANCE</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0">
            <div style="display:block;margin-top:10px;font-size:11px;padding:0">This provides protection for the non-refundable cost of your TAM ticket (up to R$500 per person) if you need to cancel your trip for a number of  specified reasons such as illness or redundancy... <a href="#" style="color:#BD282E">More info</a>
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="color:#BD282E;font-size:20px;font-weight:bold;padding:3px 0;text-align:right">TOTAL 14,000,000 $</td>
    </tr>
    <tr>
        <th height="40" align="left" style="height:40px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>