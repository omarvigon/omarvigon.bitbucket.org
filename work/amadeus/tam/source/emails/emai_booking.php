<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">BOOKING REFERENCE: <strong>GDT1523</strong> YOUR BOOKING IS ON HOLD !</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" colspan="2" style="font-size:11px;padding:0;" valign="top">
            <div style="font-size:11px;margin-top:10px">
                <span style="font-size: 14px; font-weight: bold;"><span style="color:#C0211D;">GDT1523</span> Keep this number</span>
                <br /><span style="margin-top: 3px;">The booking reference number is needed whenever in contact with TAM.</span>
                <br />
                <br />Transaction number: <span style="color:#C0211D;font-weight:bold">123456789</span>
                <br />
                <br />
            </div>
        </td>
    </tr>
    <tr>        
        <td align="left" bgcolor="#C0211D" style="background-color:#C0211D;color:#fff;font-size:14px;padding:6px;text-align:center;width:225px" valign="top" width="225">
            <a href="#" style="color:#fff;text-decoration:none;width:225px">Click here to pay your transaction</a>
        </td>        
        <td style="width:287px" width="287">&nbsp;</td>     
    </tr>
    <tr>
        <td align="left" colspan="2" style="font-size:11px;margin:0;padding:0" valign="top">            
                <br />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</td>
    </tr>
    <tr>
        <td align="left" colspan="2" height="20" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>