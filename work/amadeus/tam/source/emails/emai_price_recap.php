<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">PRICE RECAP</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="color:#BD282E;font-size:20px;font-weight:bold;padding:3px 0;text-align:right">TOTAL 14,000,000 $</td>
    </tr>
    <tr>
        <td align="left" valign="top" style="color:#999;font-size:11px;text-align:right">For all passengers including taxes</td>
    </tr>
    <tr>
        <th height="40" align="left" style="height:40px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>