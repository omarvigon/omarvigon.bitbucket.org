<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">OTHER REQUESTS</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0;">
            <div>
                <table summary="" border="0" cellspacing="0" cellpadding="3" width="512" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                    <tr>
                        <th style="margin:0;text-align:left;font-size:11px;">Passenger</th>
                        <th style="margin:0;text-align:left">&nbsp;</th>
                        <th style="margin:0;text-align:left;font-size:11px;">Assistance needed</th>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <th style="margin:0;text-align:left;font-size:11px;">Meet and assist</th>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <th style="margin:0;text-align:left;font-size:11px;">Wheelchairs needed</th>
                    </tr>
                    <tr>
                        <td style="margin:0;text-align:left">Mr. Sigmundur David Gunnlaugsson</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">Deaf without dog</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">Disable person</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">Wheelchair and assistance to boarding gate</td>
                    </tr>
                    <tr>                                           
                        <td style="margin:0;text-align:left">Mrs. Jonina Ros Guomundsdottir</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">No preference</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">No preference</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">No preference</td>
                    </tr>
                    <tr>                                                        
                        <td style="margin:0;text-align:left">Mr. Steingrimur J. Sigfusson <span style="color:#0074AF">(infant)</span></td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">No preference</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">No preference</td>
                        <td style="margin:0;text-align:left">&nbsp;</td>
                        <td style="margin:0;text-align:left">No preference</td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <th height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>