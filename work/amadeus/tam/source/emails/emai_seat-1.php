<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">SEAT RESERVATION</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;">
            <div style="display:block;margin-top:10px;padding:5px;background-color:#eaeaea">
                <span style="display:block;color:#006eab;font-weight:bold;font-size:14px">OUTBOUND</span>
                <span style="color:#4a4a4a">From <span style="font-weight:bold">Brasilia</span> to <span style="font-weight:bold">Rio de Janeiro</span></span>
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="font-size:11px;">
            <div style="display:block;margin-top:10px;padding:0;">
                <table summary="" border="0" cellspacing="0" cellpadding="0" width="502" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                    <tr>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1" >
                            <span style="display:block;">John Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0" ><span style="display:block;padding-left:5px">Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">23F</span></td>
                        <td style="background-color:#fff;" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1"><span style="display:block;">John Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0;" ><span style="display:block;padding-left:5px">Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">24F</span></td>
                    </tr>
                    <tr>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1">&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >12500,00</td>
                        <td style="background-color:#fff;padding:5px 0 5px 0" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >12500,00</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="7" height="7" align="left" style="height:7px;padding:0;margin:0;font-size:0">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1" >
                            <span style="display:block;">Barbara Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0" ><span style="display:block;padding-left:5px">XL Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">123F</span></td>
                        <td style="background-color:#fff;" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1"><span style="display:block;">Carlos Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0" ><span style="display:block;padding-left:5px">XL Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">124F</span></td>
                    </tr>
                    <tr>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >112500,00</td>
                        <td style="background-color:#fff;padding:5px 0 5px 0" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >712500,00</td>
                    </tr>
                </table>
            </div>
        </td>
    </tr> 
    <tr>
        <td align="left" valign="top" style="font-size:11px;">
            <div style="display:block;margin-top:10px;padding:5px;background-color:#eaeaea">
                <span style="display:block;color:#006eab;font-weight:bold;font-size:14px">INBOUND</span>
                <span style="color:#4a4a4a">From <span style="font-weight:bold">Brasilia</span> to <span style="font-weight:bold">Rio de Janeiro</span></span>
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="font-size:11px;">
            <div style="display:block;margin-top:10px;padding:0;">
                <table summary="" border="0" cellspacing="0" cellpadding="0" width="502" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                    <tr>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1" >
                            <span style="display:block;">John Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0" ><span style="display:block;padding-left:5px">Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">23F</span></td>
                        <td style="background-color:#fff;" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1"><span style="display:block;">John Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0;" ><span style="display:block;padding-left:5px">Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">24F</span></td>
                    </tr>
                    <tr>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1">&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >12500,00</td>
                        <td style="background-color:#fff;padding:5px 0 5px 0" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >12500,00</td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="7" height="7" align="left" style="height:7px;padding:0;margin:0;font-size:0">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1" >
                            <span style="display:block;">Barbara Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0" ><span style="display:block;padding-left:5px">XL Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">123F</span></td>
                        <td style="background-color:#fff;" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 5px;border-right:1px solid #a1a1a1"><span style="display:block;">Carlos Doe</span></td>
                        <td style="background-color:#eaeaea;font-weight:bold;padding:5px 0 0 0" ><span style="display:block;padding-left:5px">XL Seat</span></td>
                        <td style="background-color:#eaeaea;text-align:right;padding:8px 5px 0 0" ><span style="padding:3px 5px;background:#bd282e;color:#fff;">124F</span></td>
                    </tr>
                    <tr>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >112500,00</td>
                        <td style="background-color:#fff;padding:5px 0 5px 0" width="20" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0;border-right:1px solid #a1a1a1" >&nbsp;</td>
                        <td style="background-color:#eaeaea;padding:5px 0 5px 0" >&nbsp;</td>
                        <td style="background-color:#eaeaea;text-align:right;padding:5px 5px 5px 0" >712500,00</td>
                    </tr>
                </table>
            </div>
        </td>
    </tr> 
    <tr>
        <th height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>