<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">ECONOMY</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0">
            <div style="display:block;margin-top:10px;font-size:11px;padding:0"> 
                <span style="font-weight:bold">Changes:</span> Permitted within Economy for a fee. See detailed fare rules below.<br />
                <span style="font-weight:bold">Refund:</span> No refund. Taxes and fees only on wholly unues tickets are refundable.<br />
                <span style="font-weight:bold">Child discount:</span> 0-2 years 90% discount, 2-11 years 25% discount.<br /><br />
                The booking can be cancelled within 24 hours. The total price will be refunded.<br />
                Cancel at flytam.com or call us on 05400. 
            </div>
        </td>
    </tr>
    <tr>
        <th height="40" align="left" style="height:40px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>