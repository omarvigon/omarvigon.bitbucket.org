<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0;padding:5px;border: 1px solid #FF9600">
    <tr>
        <td align="left" valign="top" style="font-size:11px;">
            <span style="font-weight:bold;color:#FF9600">Alert: </span>The following warning(s) occured : The refund process affects only your flights.
        </td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;color:#FFF;">YOUR REFUND DETAILS FOR RESERVATION GDT1523</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0;">
            <div style="font-size:11px;margin-top:10px">
                <font style="font-size: 14px; font-weight: bold;">Booking reservation number: <span style="color:#C0211D; font-weight: bold;">GDT1523</span>
                </font>
                <br />                                                   
                <br />                                                   
                <font style="font-size: 11px;">Your trip has been cancelled
                    <br />The refund of the trip will be done on the same credit card used for the purchase of the trip.</font> 
                <br />                                                   
                <br />                                                   
                <font style="color:#0068AC;font-size:18px;font-weight:bold">TOTAL TO BE REFUNDED: R$1,555,000.10.</font> 
            </div>
        </td>
    </tr>
    <tr>
        <td height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>