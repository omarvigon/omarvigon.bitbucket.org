<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0;margin:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">E-TICKET:</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0">
    <tr>
        <td colspan="3" align="left" valign="top" style="font-size:11px;margin:0;padding:0">
            <div style="margin-top:10px">Electronic tickets numbers are shown during the emission.</div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0;width:265px" width="265">
            <div style="margin-top:10px">
                <table summary="" border="0" cellspacing="0" cellpadding="5" width="265" style="border-collapse:collapse;border-spacing:0;font-size:11px;width:265px">
                    <tr>
                        <td bgcolor="#eaeaea" width="265" style="background-color:#eaeaea;margin:0;font-weight:bold;width:265px">
                            <div>
                                <span style="font-weight:bold;">Mr John Smith</span><br />
                                <span style="font-size:12px;color:#0074af">E-ticket: 957-242-811-169-8</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="5" align="left" style="height:5px;padding:0;margin:0"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#eaeaea" width="265" style="background-color:#eaeaea;margin:0;font-weight:bold;width:265px">
                            <div>
                                <span style="font-weight:bold;">Mss Barbara Smith</span><br />
                                <span style="font-size:12px;color:#0074af">E-ticket: 957-242-811-169-8</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="5" align="left" style="height:5px;padding:0;margin:0"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#eaeaea" width="265" style="background-color:#eaeaea;margin:0;font-weight:bold;width:265px">
                            <div>
                                <span style="font-weight:bold;">Mrs. Gaddigoudar Shri Parvatagouda Chandanagouda</span><br />
                                <span style="font-size:12px;color:#0074af">E-ticket: 957-242-811-169-8</span>
                            </div>
                        </td>
                        <td width="20" style="margin:0;padding:0;width:20px"></td>
                    </tr>
                </table>
            </div>
        </td>
        <td width="20" style="margin:0;padding:0;width:20px"></td>
        <td align="left" valign="top" style="font-size:11px;padding:0">
            <div style="margin-top:10px">
                <span style="color:#0074AF;display:block;font-weight:bold;">Outbound</span>
                <span style="display:block;font-weight:normal;margin-top:3px">From: Comodoro Rivadavia <span style="font-weight:bold">(CRD)</span></span>
                <span style="display:block;font-weight:normal;margin-top:3px">To: Washington D.C. <span style="font-weight:bold">(AID)</span></span>
                <span style="color:#0074AF;display:block;font-weight:bold;margin-top:3px">Inbound</span>
                <span style="display:block;font-weight:normal;margin-top:3px">From: Sao Paulo <span style="font-weight:bold">(GRU)</span></span>
                <span style="display:block;font-weight:normal;margin-top:3px">To: Santiago de Chile <span style="font-weight:bold">(SCL)</span></span>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3" height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>