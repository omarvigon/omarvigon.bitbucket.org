<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">PRICE DETAILS</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0;">
            <div style="display:block;margin-top:10px;color:#333">
                <table summary="" border="0" cellspacing="0" cellpadding="5" width="100%" style="border-collapse:collapse;border-spacing:0;font-size:11px;color:#333">
                    <tr>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0;text-align:center;font-weight:bold;color:#000">2 adult</td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td width="58" bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0;text-align:center;font-weight:bold;color:#000"> 2 children</td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0;text-align:center;font-weight:bold;color:#000">1 infent</td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                    </tr>
                    <tr>
                        <td colspan="8" style="color:#C0211D;font-size:20px;font-weight:bold;margin:0;padding:5px 0;">NEW PRICE</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="font-size:14px;font-weight:bold;margin:0;padding:5px 0;">TRAVELLERS</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total travellers</td>
                        <td style="font-weight:bold;margin:0;text-align:right">2.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">2.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">2.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">6.000,00$</td>
                    </tr>
                    <tr>
                        <td colspan="8" valign="top" style="font-weight:bold;font-size:14px;margin:0;padding:5px 0;">TAXES</td>
                    </tr>
                    <tr>
                        <td style="margin:0;">Taxa de Combustivel e Seguranca</td>
                        <td style="margin:0;text-align:right">4.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td style="margin:0;text-align:right">2.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td style="margin:0;text-align:right">2.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="margin:0;">Seguranca Aeroportuaria</td>
                        <td style="margin:0;text-align:right">4.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td style="margin:0;text-align:right">2.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td style="margin:0;text-align:right">2.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="margin:0;">Service Taxes</td>
                        <td style="margin:0;text-align:right">4.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td style="margin:0;text-align:right">2.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td style="margin:0;text-align:right">2.000,00</td>
                        <td width="30">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total taxes</td>
                        <td style="font-weight:bold;margin:0;text-align:right">12.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">12.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">12.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">36.000,00$</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Rebooking fee</td>
                        <td colspan="5">&nbsp;</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">10.000,00$</td>
                    </tr>
                    <tr>
                        <td style="color:#C0211D;font-size:14px;font-weight:bold;margin:0;">Total New price</td>
                        <td style="color:#C0211D;font-weight:bold;margin:0;text-align:right">14.000,00</td>
                        <td width="30" style="color:#C0211D;margin:0;text-align:center;font-size:14px">+</td>
                        <td style="color:#C0211D;font-weight:bold;margin:0;text-align:right">14.000,00</td>
                        <td width="30" style="color:#C0211D;margin:0;text-align:center;font-size:14px">+</td>
                        <td style="color:#C0211D;font-weight:bold;margin:0;text-align:right">14.000,00</td>
                        <td width="30" style="color:#C0211D;margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="color:#C0211D;font-weight:bold;margin:0;text-align:right">52.000,00$</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc;">
                        <td colspan="8" style="color:#0074AF;font-size:20px;font-weight:bold;margin:0;padding:5px 0;">ORIGINAL PRICE</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total travellers</td>
                        <td style="font-weight:bold;margin:0;text-align:right">5.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">5.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">6.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">16.000,00$</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total taxes</td>
                        <td style="font-weight:bold;margin:0;text-align:right">12.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">12.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">12.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">36.000,00$</td>
                    </tr>
                    <tr>
                        <td style="color:#0074AF;font-size:14px;font-weight:bold;margin:0;">Total Original price</td>
                        <td style="color:#0074AF;font-weight:bold;margin:0;text-align:right">17.000,00</td>
                        <td width="30" style="color:#0074AF;margin:0;text-align:center;font-size:14px">+</td>
                        <td style="color:#0074AF;font-weight:bold;margin:0;text-align:right">17.000,00</td>
                        <td width="30" style="color:#0074AF;margin:0;text-align:center;font-size:14px">+</td>
                        <td style="color:#0074AF;font-weight:bold;margin:0;text-align:right">18.000,00</td>
                        <td width="30" style="color:#0074AF;margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="color:#0074AF;font-weight:bold;margin:0;text-align:right">52.000,00$</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc;">
                        <th bgcolor="#eaeaea" style="font-size:11px;font-weight:bold;margin:0;background-color:#eaeaea;text-align:left">Installment fee</th>
                        <td class="tr" bgcolor="#eaeaea" colspan="5" style="font-size:11px;font-weight:bold;margin:0;background-color:#eaeaea;"><b>Installments plan 5 (1 x 1.000 + 4 x 2.000)</b></td>
                        <td colspan="2" bgcolor="#eaeaea" style="color:#C0211D;font-weight:bold;margin:0;background-color:#eaeaea;text-align:right">55.000,00 $</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc;">
                        <td colspan="6" bgcolor="#eaeaea" style="font-size:11px;font-weight:bold;margin:0;background-color:#eaeaea;;">Total paid</td>
                        <td colspan="2" bgcolor="#eaeaea" style="font-weight:bold;margin:0;background-color:#eaeaea;text-align:right">0,00 $</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc;">
                        <td colspan="6" bgcolor="#eaeaea" style="font-size:11px;font-weight:bold;margin:0;background-color:#eaeaea;;">Total refunded</td>
                        <td colspan="2" bgcolor="#eaeaea" style="color:#0074AF;font-weight:bold;margin:0;background-color:#eaeaea;color:#0074AF;text-align:right">-55.000,00 $</td>
                    </tr>
                    <tr>
                        <td colspan="5" bgcolor="#eaeaea" style="font-size:11px;;margin:0;padding:0 5px 5px 5px;background-color:#eaeaea;color:#0074AF;font-size:18px;font-weight:bold">TOTAL BALANCE</td>
                        <td colspan="3" bgcolor="#eaeaea" style="font-weight:bold;font-size:18px;margin:0;padding:0 5px 5px 5px;background-color:#eaeaea;color:#0074AF;text-align:right">-58.000,00 R$</td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <th height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>