<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">PRICE DETAILS</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0;">
            <div style="display:block;margin-top:10px;color:#333">
                <table summary="" border="0" cellspacing="0" cellpadding="5" width="100%" style="border-collapse:collapse;border-spacing:0;font-size:11px;color:#333">
                    <tr>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0;text-align:center;font-weight:bold;color:#000">2 adult</td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0;text-align:center;font-weight:bold;color:#000"> 2 children</td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0;text-align:center;font-weight:bold;color:#000">1 infent</td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                        <td bgcolor="#eaeaea" style="background-color:#eaeaea;margin:0"></td>
                    </tr>
                    <tr>
                        <td colspan="8" valign="top" style="font-weight:bold;font-size:14px;margin:0;padding:5px 0;">TRAVELLERS</td>
                    </tr>
                    <tr>
                        <td style="margin:0;">Travellers</td>
                        <td style="margin:0;text-align:right">15.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right">20.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right">25.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="margin:0;color:#0074AF;">Discount</td>
                        <td style="margin:0;text-align:right;color:#0074AF">5.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right;color:#0074AF">10.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right;color:#0074AF">5.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total travellers</td>
                        <td style="font-weight:bold;margin:0;text-align:right">10.000.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">10.000.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">20.000.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">40.000.000,00$</td>
                    </tr>
                    <tr>
                        <td colspan="8" valign="top" style="border-top:1px solid #ccc;font-weight:bold;font-size:14px;margin:0;padding:5px 0;">AIRPORT TAXES</td>
                    </tr>
                    <tr>
                        <td style="margin:0;">Taxa de Combustivel e Seguranca</td>
                        <td style="margin:0;text-align:right">4.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right">2.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right">2.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="margin:0;">Seguranca Aeroportuaria</td>
                        <td style="margin:0;text-align:right">4.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right">2.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td style="margin:0;text-align:right">2.000.000,00</td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total airport taxes</td>
                        <td style="font-weight:bold;margin:0;text-align:right">8.000.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">4.000.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">4.000.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">16.000.000,00$</td>
                    </tr>
                    <tr>
                        <td colspan="8" valign="top" style="border-top:1px solid #ccc;font-weight:bold;font-size:14px;margin:0;padding:5px 0;">DU TAXES TAXES</td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;padding:2px">Total du taxes</td>
                        <td style="font-weight:bold;margin:0;text-align:right">4.000.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">2.000.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">2.000.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">8.000.000,00$</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="color:#4a4a4a;font-weight:bold;font-size:12px;margin:0;background-color:#eaeaea">FLIGHT OPTIONS</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="font-weight:bold;margin:0;">CGH - SDU</td>
                    </tr>
                    <tr>
                        <td rowspan="2" valign="top" style="border-top:1px solid #ccc;margin:0;">Excess luggages</td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td valign="top" style="margin:0;">Seat luggages</td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">2 XL seats<br /><span style="color:#000">25.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">1 exit seat<br /><span style="color:#000">25.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td colspan="8" style="font-weight:bold;margin:0;">SDU - CGH</td>
                    </tr>
                    <tr>
                        <td valign="top" rowspan="2" style="border-top:1px solid #ccc;margin:0;">Excess luggages</td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">4 pièces<br /><span style="color:#000">50.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td valign="top" style="margin:0;">Seat luggages</td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">2 XL seats<br /><span style="color:#000">25.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td style="color:#999;margin:0;padding-left:20px;text-align:right">1 exit seat<br /><span style="color:#000">25.000,00</span></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                        <td valign="top" style="margin:0"></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;margin:0;">Total flight options</td>
                        <td style="font-weight:bold;margin:0;text-align:right">250.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">250.000,00</td>
                        <td width="30" style="margin:0;text-align:center;font-size:14px">+</td>
                        <td style="font-weight:bold;margin:0;text-align:right">100.000,00</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">600.000,00$</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc">
                        <td style="font-weight:bold;margin:0;">INSTALLMENT FEE</td>
                        <td colspan="5" style="font-weight:bold;margin:0;text-align:right;">Installments plan 5 (1 x 10.000 + 4 x 20.000)</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px;">=</td>
                        <td style="font-weight:bold;margin:0;text-align:right">90.000,00$</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc;">
                        <td colspan="6" bgcolor="#eaeaea" style="font-size:11px;font-weight:bold;margin:0;background-color:#eaeaea">SUB TOTAL</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px;background-color:#eaeaea">=</td>
                        <td bgcolor="#eaeaea" style="font-weight:bold;margin:0;background-color:#eaeaea;text-align:right">342.000,00 $</td>
                    </tr>
                    <tr style="border-top:1px solid #ccc;">
                        <td colspan="6" bgcolor="#eaeaea" style="font-size:11px;font-weight:bold;margin:0;background-color:#eaeaea;color:#bd282e;">INSURANCE</td>
                        <td width="30" style="margin:0;padding:0;text-align:center;font-size:14px;background-color:#eaeaea">=</td>
                        <td bgcolor="#eaeaea" style="font-weight:bold;margin:0;background-color:#eaeaea;color:#bd282e;text-align:right">55.000,00$</td>
                    </tr>
                    <tr>
                        <td colspan="8" bgcolor="#eaeaea" style="font-size:11px;;margin:0;padding:0 5px 0 5px;background-color:#eaeaea;"><span style="display:block;margin-top:10px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus nunc vel orci aliquet eget imperdiet nulla tempus. Donec at ligula sed odio elementum auctor</span></td>
                    </tr>
                    <tr>
                        <td colspan="5" bgcolor="#eaeaea" style="font-size:11px;;margin:0;padding:0 5px 5px 5px;background-color:#eaeaea;color:#bd282e"><span style="display:block;margin-top:10px;"><font style="font-size:18px;font-weight:bold">TOTAL</font> For all passengers including</span></td>
                        <td colspan="3" bgcolor="#eaeaea" style="font-weight:bold;font-size:18px;margin:0;padding:0 5px 5px 5px;background-color:#eaeaea;color:#bd282e;text-align:right"><span style="display:block;margin-top:10px;">7.158.000,00 R$</span></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <th height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>