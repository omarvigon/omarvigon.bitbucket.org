<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0;margin:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">LUGGAGE</th>
    </tr>
    <tr>
        <td height="5" align="left" style="font-size:height:5px;margin:0;padding:0"></td>
    </tr>
    <tr>
        <td align="left" bgcolor="#eaeaea" style="background-color:#eaeaea;font-size:11px;" valign="top">
            <div>
                <span style="color:#006eab;font-size:14px;font-weight:bold">OUTBOUND</span>
                <span style="color:#4a4a4a">From <span style="font-weight:bold">Brasilia</span> to <span style="font-weight:bold">Rio de Janeiro</span></span>
            </div>
        </td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="3" width="512" style="border-collapse:collapse;border-spacing:0;font-size:11px;margin:0">
    <tr>
        <th width="200" style="margin:0;text-align:left;font-size:11px;">Passenger</th>
        <th width="30" style="margin:0;text-align:left">&nbsp;</th>
        <th style="margin:0;text-align:left;font-size:11px;">Weight</th>
    </tr>
    <tr>
        <td width="200" style="margin:0;">Mr. Sigmundur David Gunnlaugsson</td>
        <td width="30" style="margin:0;text-align:left">&nbsp;</td>
        <td style="margin:0;text-align:left">40kg</td>
    </tr>
    <tr>
        <td width="200" style="margin:0;text-align:left">Mrs. Jonina Ros Guomundsdottir</td>
        <td width="30" style="margin:0;text-align:left">&nbsp;</td>
        <td style="margin:0;text-align:left">20kg</td>
    </tr>
    <tr>
        <td width="200" style="margin:0;text-align:left">Mr. Steingrimur J. Sigfusson <span style="color:#0074AF">(infant)</span></td>
        <td width="30" style="margin:0;text-align:left">&nbsp;</td>
        <td style="margin:0;text-align:left">n/a</td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0;">
    <tr>
        <td align="left" bgcolor="#eaeaea" style="background-color:#eaeaea;font-size:11px;" valign="top">
            <div>
                <span style="color:#006eab;font-size:14px;font-weight:bold">INTBOUND</span>
                <span style="color:#4a4a4a">From <span style="font-weight:bold">Brasilia</span> to <span style="font-weight:bold">Rio de Janeiro</span></span>
            </div>
        </td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="3" width="512" style="border-collapse:collapse;border-spacing:0;font-size:11px">
    <tr>
        <th width="200" style="margin:0;text-align:left;font-size:11px;">Passenger</th>
        <th width="30" style="margin:0;text-align:left">&nbsp;</th>
        <th style="margin:0;text-align:left;font-size:11px;">Weight</th>
    </tr>
    <tr>
        <td width="200" style="margin:0;">Mr. Sigmundur David Gunnlaugsson</td>
        <td width="30" style="margin:0;text-align:left">&nbsp;</td>
        <td style="margin:0;text-align:left">40kg</td>
    </tr>
    <tr>
        <td width="200" style="margin:0;text-align:left">Mrs. Jonina Ros Guomundsdottir</td>
        <td width="30" style="margin:0;text-align:left">&nbsp;</td>
        <td style="margin:0;text-align:left">20kg</td>
    </tr>
    <tr>
        <td width="200" style="margin:0;text-align:left">Mr. Steingrimur J. Sigfusson <span style="color:#0074AF">(infant)</span></td>
        <td width="30" style="margin:0;text-align:left">&nbsp;</td>
        <td style="margin:0;text-align:left">n/a</td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0">
    <tr>
        <th height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</th>
    </tr>
</table>