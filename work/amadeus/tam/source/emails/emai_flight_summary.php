<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">FLIGHT SUMMARY</th>
    </tr>
    <tr>
        <td height="5" align="left" style="font-size:height:5px;margin:0;padding:0"></td>
    </tr>
    <tr>
        <td align="left" bgcolor="#eaeaea" style="background-color:#eaeaea;font-size:11px;" valign="top">
            <div>
                <span style="color:#006eab;font-size:14px;font-weight:bold">OUTBOUND</span>
                <span style="color:#4a4a4a">From <span style="font-weight:bold">Brasilia</span> to <span style="font-weight:bold">Rio de Janeiro - SUN 20 JUL 2012</span></span>
            </div>
        </td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">    
    <tr>
        <td>
            <table summary="" border="0" cellspacing="0" cellpadding="3" width="502" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                <tr>
                    <td valign="top" width="125" rowspan="11" style="margin:0;padding:5px 0 0 0">
                        <table summary="" border="0" cellspacing="0" cellpadding="3" width="125" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                            <tr>
                                <td valign="top" width="125" style="border:1px solid #eaeaea;border-bottom:0;color:#006EAB;font-weight:bold;margin:0;padding-top:5px;text-align:center;">Sun</td>
                            </tr>
                            <tr>
                                <td valign="top" width="125" style="border-left:1px solid #eaeaea;border-right:1px solid #eaeaea;font-size:14px;font-weight:bold;margin:0;text-align:center;">21</td> 
                            </tr>
                            <tr>
                                <td valign="top" width="125" style="border:1px solid #eaeaea;border-top:0;margin:0;text-align:center;padding-bottom:5px">JAN</td> 
                            </tr>
                            <tr>
                                <td height="5" align="left" style="font-size:height:5px;margin:0;padding:0"></td>
                            </tr>
                            <tr>
                                <td bgcolor="#CB242D" valign="top" width="125" style="background-color:#CB242D;color:#fff;padding:5px;margin:0;text-align:center;border:0">Light</td> 
                            </tr>
                        </table>
                    </td>                                
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Flight Number:</td>
                    <td valign="top" style="margin:0"><img src="../emails/img/emai_thumb_logo.gif" alt="TAM" width="29" height="9" /> <a href="#" style="color:#C0211D">JJ3120</a> <a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Operated by:</td>
                    <td valign="top" style="font-weight:bold;margin:0">TAM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Departure:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Brasilia International Airport</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Time:</td>
                    <td valign="top" style="font-weight:bold;margin:0">10:15 AM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Arrival:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Rio de Janerio, Santos Dumont</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Time:</td>
                    <td valign="top" style="font-weight:bold;margin:0">15:15 PM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Flight Duration:</td>
                    <td valign="top" style="font-weight:bold;margin:0">2h15m</td>
                </tr> 
                <tr>                            
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Aircraft:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Airbus A319<a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Cabin:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Economy</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Baggage Allowance:</td>
                    <td valign="top" style="font-weight:bold;margin:0">20kg<a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>       
            </table>
        </td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0;">
    <tr>
        <td align="left" bgcolor="#eaeaea" style="background-color:#eaeaea;font-size:11px;" valign="top">
            <div>
                <span style="color:#006eab;font-size:14px;font-weight:bold">INBOUND</span>
                <span style="color:#4a4a4a">From <span style="font-weight:bold">Brasilia</span> to <span style="font-weight:bold">Rio de Janeiro - SUN 20 JUL 2012</span></span>
            </div>
        </td>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0;">
    <tr>
        <td align="left" valign="top" style="font-size:11px;padding:0">
            <table summary="" border="0" cellspacing="0" cellpadding="3" width="502" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                <tr>
                    <td valign="top" width="125" rowspan="11" style="margin:0;padding:5px 0 0 0"> 
                        <table summary="" border="0" cellspacing="0" cellpadding="3" width="125" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                            <tr>
                                <td valign="top" width="125" style="border:1px solid #eaeaea;border-bottom:0;color:#006EAB;font-weight:bold;margin:0;padding-top:5px;text-align:center;">Sun</td>
                            </tr>
                            <tr>
                                <td valign="top" width="125" style="border-left:1px solid #eaeaea;border-right:1px solid #eaeaea;font-size:14px;font-weight:bold;margin:0;text-align:center;">21</td> 
                            </tr>
                            <tr>
                                <td valign="top" width="125" style="border:1px solid #eaeaea;border-top:0;margin:0;text-align:center;padding-bottom:5px">JAN</td> 
                            </tr>
                            <tr>
                                <td height="5" align="left" style="font-size:height:5px;margin:0;padding:0"></td>
                            </tr>
                            <tr>
                                <td bgcolor="#CB242D" valign="top" width="125" style="background-color:#CB242D;color:#fff;padding:5px;margin:0;text-align:center;border:0">Light</td> 
                            </tr>
                        </table>
                    </td> 
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Flight Number:</td>
                    <td valign="top" style="margin:0"><img src="../emails/img/emai_thumb_logo.gif" alt="TAM" width="29" height="9" /> <a href="#" style="color:#C0211D">JJ3120</a> <a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Operated by:</td>
                    <td valign="top" style="font-weight:bold;margin:0">TAM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Departure:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Brasilia International Airport</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Time:</td>
                    <td valign="top" style="font-weight:bold;margin:0">10:15 AM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Arrival:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Rio de Janerio, Santos Dumont</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Time:</td>
                    <td valign="top" style="font-weight:bold;margin:0">15:15 PM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Flight Duration:</td>
                    <td valign="top" style="font-weight:bold;margin:0">2h15m</td>
                </tr>                             
                <tr>                             
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Aircraft:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Airbus A319<a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Cabin:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Economy</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Baggage Allowance:</td>
                    <td valign="top" style="font-weight:bold;margin:0">20kg<a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top" style="border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;margin:0;padding:20px 0 20px 100px;color:#333;font-weight:bold;font-size:12px;"><img border="0" style="vertical-align:middle;padding-right:10px;border:0;" src="../emails/img/emai_connection.gif" alt="" width="21" height="26" /></td>
                    <td colspan="2" valign="top" style="border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;margin:0;padding:20px 0 20px 0;font-size:11px;">Connection time <font style="font-weight:bold">1 h 05m</font> Sao Paulo Congonhas :<br /><font style="color:#006eab;font-weight:bold">Your next flight departs on Mon 21 Jan</font></td>
                </tr>
                <tr>
                    <td valign="top" width="125" rowspan="11" style="margin:0;padding:5px 0 0 0"> 
                        <table summary="" border="0" cellspacing="0" cellpadding="3" width="125" style="border-collapse:collapse;border-spacing:0;font-size:11px">
                            <tr>
                                <td valign="top" width="125" style="border:1px solid #eaeaea;border-bottom:0;color:#006EAB;font-weight:bold;margin:0;padding-top:5px;text-align:center;">Sun</td>
                            </tr>
                            <tr>
                                <td valign="top" width="125" style="border-left:1px solid #eaeaea;border-right:1px solid #eaeaea;font-size:14px;font-weight:bold;margin:0;text-align:center;">21</td> 
                            </tr>
                            <tr>
                                <td valign="top" width="125" style="border:1px solid #eaeaea;border-top:0;margin:0;text-align:center;padding-bottom:5px">JAN</td> 
                            </tr>
                            <tr>
                                <td height="5" align="left" style="font-size:height:5px;margin:0;padding:0"></td>
                            </tr>
                            <tr>
                                <td bgcolor="#CB242D" valign="top" width="125" style="background-color:#CB242D;color:#fff;padding:5px;margin:0;text-align:center;border:0">Light</td> 
                            </tr>
                        </table>
                    </td>                                
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Flight Number:</td>
                    <td valign="top" style="margin:0"><img src="../emails/img/emai_thumb_logo.gif" alt="TAM" width="29" height="9" /> <a href="#" style="color:#C0211D">JJ3120</a> <a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Operated by:</td>
                    <td valign="top" style="font-weight:bold;margin:0">TAM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Departure:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Brasilia International Airport</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Time:</td>
                    <td valign="top" style="font-weight:bold;margin:0">10:15 AM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Arrival:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Rio de Janerio, Santos Dumont</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Time:</td>
                    <td valign="top" style="font-weight:bold;margin:0">15:15 PM</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Flight Duration:</td>
                    <td valign="top" style="font-weight:bold;margin:0">2h15m</td>
                </tr>                 
                <tr>            
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Aircraft:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Airbus A319<a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Cabin:</td>
                    <td valign="top" style="font-weight:bold;margin:0">Economy</td>
                </tr>
                <tr>                                
                    <td width="10" style="width:10px"></td>  
                    <td valign="top" style="margin:0">Baggage Allowance:</td>
                    <td valign="top" style="font-weight:bold;margin:0">20kg<a style="text-decoration:none;vertical-align:middle" href="#"><img border="0" style="padding-left:5px;border:0;" src="../emails/img/emai_info.gif" alt="Informations" width="12" height="13" /></a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>