<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">BOOKING REFERENCE: <strong>GDT1523</strong> YOUR BOOKING IS ON HOLD !</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <td colspan="2" align="left" style="font-size:11px;padding:0;" valign="top">
            <div style="font-size:11px;margin-top:10px">
                <span style="font-size: 14px; font-weight: bold;"><span style="color:#C0211D;">GDT1523</span> Keep this number</span>
                <br /><span style="margin-top: 3px;">The booking reference number is needed whenever in contact with TAM.</span>
                <br />
                <br />Transaction number: <span style="color:#C0211D;font-weight:bold">123456789</span>
                <br />
                <br />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
            </div>
        </td>
        <td align="left" colspan="2" style="font-size:11px;margin:0;padding:0" valign="top"> 
            <div style="font-size:11px;margin-top:10px">           
                <a href="#" style="text-decoration:none"><img border="0" src="../emails/img/emai_barcode.gif" alt="" width="98" height="88" style="display:block;border:0" /></a>
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" colspan="3" height="20" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>