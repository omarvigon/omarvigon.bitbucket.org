<table border="0" summary="" cellspacing="0" cellpadding="5" width="512" style="border-collapse:collapse;border-spacing:0">
    <tr>
        <th align="left" bgcolor="#0068AC" style="font-size:14px;padding:5px;color:#FFF;">PASSENGERS</th>
    </tr>
</table>
<table border="0" summary="" cellspacing="0" cellpadding="0" width="512" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0">
    <tr>
        <td align="left" valign="top" style="font-size:11px;margin:0;padding:0">
            <div style="margin-top:10px;">
                <table  align="left" border="0" cellspacing="0" cellpadding="5" style="border-collapse:collapse;border-spacing:0;font-size:11px" summary="" width="265" >
                    <tr>
                        <td bgcolor="#eaeaea" style="background:#eaeaea;margin:0;width:265px" valign="top" width="265">
                            <span style="display:block;font-weight:bold;">Mr John Smith</span>
                            <span style="display:block;margin-top:3px">User: <font style="font-weight:bold;">BA 45875865</font></span>
                            <span style="color:#666;display:block;font-weight:bold;margin-top:3px">Secure flight information</span>
                            <span style="display:block;margin-top:3px">Middle name: <font style="font-weight:bold;">Joshua</font></span>
                            <span style="display:block;margin-top:3px">Data de nacimento: <font style="font-weight:bold;">05-02-1985</font></span>
                            <span style="display:block;margin-top:3px">Passport Number.:<font style="font-weight:bold;">050-654-321-987</font></span>
                            <span style="display:block;margin-top:3px">Redress Number:<font style="font-weight:bold;">123-456-789-789</font></span>                
                        </td>
                    </tr>
                    <tr>
                        <td height="5" align="left" style="height:5px;padding:0;margin:0"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#eaeaea" style="background:#eaeaea;margin:0;width:265px" valign="top" width="265">
                            <span style="display:block;font-weight:bold;">Mss Barbara Smith</span>
                            <span style="display:block;margin-top:3px">User: <font style="font-weight:bold;">BA 45875865</font></span>
                            <span style="color:#666;display:block;font-weight:bold;margin-top:3px">Secure flight information</span>
                            <span style="display:block;margin-top:3px">Middle name: <font style="font-weight:bold;">Carolina</font></span>
                            <span style="display:block;margin-top:3px">Data de nacimento: <font style="font-weight:bold;">05-02-1985</font></span>
                            <span style="display:block;margin-top:3px">Passport Number.:<font style="font-weight:bold;">050-654-321-987</font></span>
                            <span style="display:block;margin-top:3px">Redress Number:<font style="font-weight:bold;">123-456-789-789</font></span>  
                        </td>
                    </tr>
                    <tr>
                        <td style="margin:0;padding:0;width:265px" valign="top" width="265">
                            <div style="margin:0;padding:0;display:block"><img border="0" style="margin:0;padding:0;display:block;border:0;" src="../emails/img/emai_puce_01.gif" alt="" height="9" width="22" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#eaeaea" style="background:#eaeaea;margin:0;width:265px" valign="top" width="265">
                            <span style="font-weight:bold;">Master Paulo Smith</span><br />
                            <span style="display:block;color:#0074af;margin-top:3px;font-weight:bold;">Infant travelling with:</span> <span style="font-size:11px;">Yelena Vlagdimirovna Afanasyeva</span>
                            <span style="display:block;margin-top:3px">User: <font style="font-weight:bold;">BA 45875865</font></span>
                            <span style="color:#666;display:block;font-weight:bold;margin-top:3px">Secure flight information</span>
                            <span style="display:block;margin-top:3px">Middle name: <font style="font-weight:bold;">Carolina</font></span>
                            <span style="display:block;margin-top:3px">Data de nacimento: <font style="font-weight:bold;">05-02-1985</font></span>
                            <span style="display:block;margin-top:3px">Passport Number.:<font style="font-weight:bold;">050-654-321-987</font></span>
                            <span style="display:block;margin-top:3px">Redress Number:<font style="font-weight:bold;">123-456-789-789</font></span>  
                        </td>
                    </tr>
                </table> 
            </div>           
        </td>
        <td align="left" valign="top" style="font-size:11px;margin:0;padding:0">
            <div style="margin-top:10px;">
                <table  align="left" border="0" cellspacing="0" cellpadding="5" style="border-collapse:collapse;border-spacing:0;font-size:11px" summary="" width="237" >
                    <tr>
                        <td valign="top" width="10" style="margin:0;padding:0;width:10px;">
                           <img border="0" style="margin:0;padding:0;margin-top:10px;display:block;border:0;" src="../emails/img/emai_puce_02.gif" alt="" height="25" width="10" /></td>
                        <td bgcolor="#eaeaea" style="background:#eaeaea;margin:0;width:237px" valign="top" width="237" >
                            <div style="font-size:11px;background:#eaeaea;padding:5px">
                                <span style="display:block;font-weight:bold;">Contact</span>
                                <span style="display:block;margin-top:3px;">E-mail:<font style="font-weight:bold">jhon@smith.com</font></span>
                                <span style="display:block;margin-top:3px;">Mobile phone: <font style="font-weight:bold">+33 0665845025</font></span>
                                <span style="display:block;margin-top:3px;">Provider: <font style="font-weight:bold">TIM</font></span>
                                <span style="display:block;margin-top:3px;">Country: <font style="font-weight:bold">Brazil</font></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="20" align="left" style="height:20px;padding:0;margin:0">&nbsp;</td>
    </tr>
</table>