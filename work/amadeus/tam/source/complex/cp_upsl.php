<?php
include("../@includes/php_start.php");
include("../@includes/header.php");
include("../@includes/module_select_flight.php");
?>
		<ul class="wrap hr">
        	<li class="fl boxg grid3 caption"><input class="fl" type="radio" /> Guest Value <b>2,000</b></li>
            <li class="fl boxg grid3 selected midst"><input class="fl" type="radio" /> Business Value <b>2,500</b></li>
            <li class="fl boxg grid3 caption"><input class="fl" type="radio" /> First Premium <b>3,500</b></li>
        </ul>

       	<header class="caption4 hr">
	        <small class="fl ico plane2 midst br"></small><p class="fr br"><b>Mon 03 JUN 2012</b></p>
            <h3>BOUND 1</h3>
            <p>From <b>Sao Paulo</b> to <b>Congonhas</b></p>
        </header>
        
		<?php
        include("../@includes/tcaption_direct.php");
		include("../@includes/table_avai.php");
		include("../@includes/module_legend.php");
		?>
		
        <header class="caption4 hr">
	        <small class="fl ico plane2 midst br"></small><p class="fr br"><b>Mon 16 JUN 2012</b></p>
            <h3>BOUND 2</h3>
            <p>From <b>Congonhas</b> to <b>Rio de Janerio</b></p>
        </header>
        <?php
        include("../@includes/tcaption_direct.php");
		include("../@includes/table_avai.php");
        include("../@includes/module_legend.php");
		?>
        
         <header class="caption4 hr">
	        <small class="fl ico plane2 midst br"></small><p class="fr br"><b>Mon 06 SEP 2012</b></p>
            <h3>BOUND 3</h3>
            <p>From <b>Rio de Janerio</b> to <b>Santos Dumont</b></p>
        </header>
        <?php
        include("../@includes/tcaption_direct.php");
		include("../@includes/table_avai.php");
        include("../@includes/module_legend.php");
		?>
        
         <header class="caption4 hr">
	        <small class="fl ico plane2 midst br"></small><p class="fr br"><b>Mon 26 SEP 2012</b></p>
            <h3>BOUND 4</h3>
            <p>From <b>Santos Dumont</b> to <b>Sao Paulo</b></p>
        </header>
        <?php
        include("../@includes/tcaption_direct.php");
		include("../@includes/table_avai.php");
        include("../@includes/module_legend.php");
		?>
        
        <?php
		include("../@includes/footer_prev_next.php");
		?>
    </div>
    <aside class="main fr">
		<?php include("../@includes/aside_search.php"); ?>
		
        <h3 class="caption4 hr">FLIGHT SUMMARY</h3>
        <article>
            <h4 class="toggle plus" data-wdk-toggle-target="#bound0NoDetails_1,#bound0WithDetails_1"><small class="ico sign"></small> BOUND 1</h4>
            <section class="boxg wrap column" id="bound0NoDetails_1" data-wdk-toggle-transition="none">
            	<aside class="fl">
                <?php include("../@includes/aside_date.php");?>
                </aside>
                <div class="fr">
	            	<p><b>8:30</b> Sao Paulo</p>
    	            <p><b>10:55</b> Congonhas</p>
                </div>
            </section>
            <section class="boxg wrap column none" id="bound0WithDetails_1" data-wdk-toggle-transition="none">
            	<?php include("../@includes/aside_outbound.php");?>
			</section>
            
            <h4 class="toggle plus" data-wdk-toggle-target="#bound0NoDetails_2,#bound0WithDetails_2"><small class="ico sign"></small> BOUND 2</h4>
            <section class="boxg wrap column" id="bound0NoDetails_2" data-wdk-toggle-transition="none">
            	<aside class="fl">
                <?php include("../@includes/aside_date.php");?>
                </aside>
                <div class="fr">
	            	<p><b>8:30</b> Congonhas</p>
    	            <p><b>10:55</b> Rio de Janerio</p>
                </div>
            </section>
            <section class="boxg wrap column none" id="bound0WithDetails_2" data-wdk-toggle-transition="none">
            	<?php include("../@includes/aside_outbound.php");?>
			</section>
            
            <h4 class="toggle plus" data-wdk-toggle-target="#bound0NoDetails_3,#bound0WithDetails_3"><small class="ico sign"></small> BOUND 3</h4>
            <section class="boxg wrap column" id="bound0NoDetails_3" data-wdk-toggle-transition="none">
            	<aside class="fl">
                <?php include("../@includes/aside_date.php");?>
                </aside>
                <div class="fr">
	            	<p><b>8:30</b> Rio de Janerio</p>
    	            <p><b>10:55</b> Santos Dumont</p>
                </div>
            </section>
            <section class="boxg wrap column none" id="bound0WithDetails_3" data-wdk-toggle-transition="none">
            	<?php include("../@includes/aside_outbound.php");?>
			</section>
            
            <h4 class="toggle plus" data-wdk-toggle-target="#bound0NoDetails_4,#bound0WithDetails_4"><small class="ico sign"></small> BOUND 4</h4>
            <section class="boxg wrap column" id="bound0NoDetails_4" data-wdk-toggle-transition="none">
            	<aside class="fl">
                <?php include("../@includes/aside_date.php");?>
                </aside>
                <div class="fr">
	            	<p><b>8:30</b> Santos Dumont</p>
    	            <p><b>10:55</b> Sao Paulo</p>
                </div>
            </section>
            <section class="boxg wrap column none" id="bound0WithDetails_4" data-wdk-toggle-transition="none">
            	<?php include("../@includes/aside_outbound.php");?>
			</section>
		
		<?php
        include("../@includes/aside_total.php");
		include("../@includes/footer_next.php");
		?>
        </article>    
    </aside>
<?php
include("../@includes/footer.php");
include("../@includes/foot.php");
?>