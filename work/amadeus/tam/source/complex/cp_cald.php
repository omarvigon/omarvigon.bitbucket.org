<?php
include("../@includes/php_start.php");
include("../@includes/head_popin.php");
include("../@includes/header_change_dates.php");
?>
    <table class="cald tc hr">
    <caption class="h2 caption">Please select day in for Sao Paulo to Congonhas</caption>
    <thead>
    <tr class="caption grid0">
        <td><b>MON</b></td>
        <td><b>TUE</b></td>
        <td><b>WED</b></td>
        <td><b>THU</b></td>
        <td><b>FRI</b></td>
        <td><b>SAT</b></td>
        <td><b>SUN</b></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><b class="block">03 JUN</b>2.500,00</td>
        <th class="em1">No flight available</th>
        <td><small class="ico uncomb"></small> <b class="block">05 JUN</b>2.500,00</td>
        <td><b class="block">06 JUN</b>2.500,00</td>
        <th class="selected"><b class="block">07 JUN</b>2.500,00</th>
        <td class="lowest em3"><small class="fr"><small class="ico cheap"></small></small><b class="block">08 JUN</b>2.000,00</td>
        <td><b class="block">09 JUN</b>2.500,00</td>
    </tr>  
    </tbody>      
    </table>
      
	<table class="cald tc hr">
    <caption class="h2 caption">Please select day in for Congonhas to Rio de Janerio</caption>
    <thead>
    <tr class="caption grid0">
        <td><b>MON</b></td>
        <td><b>TUE</b></td>
        <td><b>WED</b></td>
        <td><b>THU</b></td>
        <td><b>FRI</b></td>
        <td><b>SAT</b></td>
        <td><b>SUN</b></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><b class="block">16 JUN</b>1.500,00</td>
        <td><b class="block">17 JUN</b>1.500,00</td>
        <th class="em1">No flight available</th>
        <th class="selected"><b class="block">19 JUN</b>1.500,00</th>
        <td><small class="ico uncomb"></small>  <b class="block">20 JUN</b>1.500,00</td>
        <td><b class="block">21 JUN</b>1.500,00</td>
        <td class="lowest em3"><small class="fr"><small class="ico cheap"></small></small><b class="block">22 JUN</b>500,00</td>
    </tr>  
    </tbody>      
    </table>
    
    <table class="cald tc hr">
    <caption class="h2 caption">Please select day in for Rio de Janerio to Santos Dumont</caption>
    <thead>
    <tr class="caption grid0">
        <td><b>MON</b></td>
        <td><b>TUE</b></td>
        <td><b>WED</b></td>
        <td><b>THU</b></td>
        <td><b>FRI</b></td>
        <td><b>SAT</b></td>
        <td><b>SUN</b></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th class="selected"><b class="block">06 SEP</b>2.500,00</th>
        <td><b class="block">07 SEP</b>2.500,00</td>
        <td><b class="block">08 SEP</b>2.500,00</td>
        <th class="em1">Out of range</th>
        <td><small class="ico uncomb"></small>  <b class="block">10 SEP</b>2.500,00</td>
        <td><b class="block">11 SEP</b>2.500,00</td>
        <td><b class="block">12 SEP</b>2.500,00</td>
    </tr>  
    </tbody>      
    </table>
    
    <table class="cald tc hr">
    <caption class="h2 caption">Please select day in for Santos Dumont to Sao Paulo</caption>
    <thead>
    <tr class="caption grid0">
        <td><b>MON</b></td>
        <td><b>TUE</b></td>
        <td><b>WED</b></td>
        <td><b>THU</b></td>
        <td><b>FRI</b></td>
        <td><b>SAT</b></td>
        <td><b>SUN</b></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><b class="block">20 SEP</b>1.500,00</th>
        <td class="selected"><b class="block">21 SEP</b>1.500,00</td>
        <td><b class="block">22 SEP</b>1.500,00</td>
        <td><b class="block">23 SEP</b>1.500,00</td>
        <th class="em1">No flight available</th>
        <td><small class="ico uncomb"></small>  <b class="block">25 SEP</b>1.500,00</td>
        <td><b class="block">26 SEP</b>1.500,00</td>
    </tr>  
    </tbody>      
    </table>
    
    <p class="hr"><b>Legend:</b> <small class="ico uncomb midst"></small>Uncombinable flight</p>
<?php
include("../@includes/footer_next_popin.php");
include("../@includes/foot.php");
?>