(function (a, f) {
    function i(b) {
        var e = Q[b] = {},
            m, o;
        b = b.split(/\s+/);
        m = 0;
        for (o = b.length; m < o; m++) e[b[m]] = true;
        return e
    }
    function g(b, e, m) {
        if (m === f && b.nodeType === 1) {
            m = "data-" + e.replace(O, "-$1").toLowerCase();
            m = b.getAttribute(m);
            if (typeof m === "string") {
                try {
                    m = m === "true" ? true : m === "false" ? false : m === "null" ? null : d.isNumeric(m) ? +m : H.test(m) ? d.parseJSON(m) : m
                } catch (o) {}
                d.data(b, e, m)
            } else m = f
        }
        return m
    }
    function h(b) {
        for (var e in b) if (!(e === "data" && d.isEmptyObject(b[e]))) if (e !== "toJSON") return false;
        return true
    }
    function j(b, e, m) {
        var o = e + "defer",
            r = e + "queue",
            w = e + "mark",
            A = d._data(b, o);
        if (A && (m === "queue" || !d._data(b, r)) && (m === "mark" || !d._data(b, w))) setTimeout(function () {
            if (!d._data(b, r) && !d._data(b, w)) {
                d.removeData(b, o, true);
                A.fire()
            }
        }, 0)
    }
    function l() {
        return false
    }
    function c() {
        return true
    }
    function k(b, e, m) {
        e = e || 0;
        if (d.isFunction(e)) return d.grep(b, function (r, w) {
            return !!e.call(r, w, r) === m
        });
        else if (e.nodeType) return d.grep(b, function (r) {
            return r === e === m
        });
        else if (typeof e === "string") {
            var o = d.grep(b, function (r) {
                return r.nodeType === 1
            });
            if (Xa.test(e)) return d.filter(e, o, !m);
            else e = d.filter(e, o)
        }
        return d.grep(b, function (r) {
            return d.inArray(r, e) >= 0 === m
        })
    }
    function q(b) {
        var e = Ra.split("|");
        b = b.createDocumentFragment();
        if (b.createElement) for (; e.length;) b.createElement(e.pop());
        return b
    }
    function s(b, e) {
        if (!(e.nodeType !== 1 || !d.hasData(b))) {
            var m, o, r;
            o = d._data(b);
            var w = d._data(e, o),
                A = o.events;
            if (A) {
                delete w.handle;
                w.events = {};
                for (m in A) {
                    o = 0;
                    for (r = A[m].length; o < r; o++) d.event.add(e, m, A[m][o])
                }
            }
            if (w.data) w.data = d.extend({}, w.data)
        }
    }
    function p(b, e) {
        var m;
        if (e.nodeType === 1) {
            e.clearAttributes && e.clearAttributes();
            e.mergeAttributes && e.mergeAttributes(b);
            m = e.nodeName.toLowerCase();
            if (m === "object") e.outerHTML = b.outerHTML;
            else if (m === "input" && (b.type === "checkbox" || b.type === "radio")) {
                if (b.checked) e.defaultChecked = e.checked = b.checked;
                if (e.value !== b.value) e.value = b.value
            } else if (m === "option") e.selected = b.defaultSelected;
            else if (m === "input" || m === "textarea") e.defaultValue = b.defaultValue;
            else if (m === "script" && e.text !== b.text) e.text = b.text;
            e.removeAttribute(d.expando);
            e.removeAttribute("_submit_attached");
            e.removeAttribute("_change_attached")
        }
    }
    function n(b) {
        return typeof b.getElementsByTagName !== "undefined" ? b.getElementsByTagName("*") : typeof b.querySelectorAll !== "undefined" ? b.querySelectorAll("*") : []
    }
    function u(b) {
        if (b.type === "checkbox" || b.type === "radio") b.defaultChecked = b.checked
    }
    function z(b) {
        var e = (b.nodeName || "").toLowerCase();
        if (e === "input") u(b);
        else e !== "script" && typeof b.getElementsByTagName !== "undefined" && d.grep(b.getElementsByTagName("input"), u)
    }
    function E(b, e, m) {
        var o = e === "width" ? b.offsetWidth : b.offsetHeight,
            r = e === "width" ? 1 : 0;
        if (o > 0) {
            if (m !== "border") for (; r < 4; r += 2) {
                m || (o -= parseFloat(d.css(b, "padding" + pb[r])) || 0);
                if (m === "margin") o += parseFloat(d.css(b, m + pb[r])) || 0;
                else o -= parseFloat(d.css(b, "border" + pb[r] + "Width")) || 0
            }
            return o + "px"
        }
        o = yb(b, e);
        if (o < 0 || o == null) o = b.style[e];
        if (Ib.test(o)) return o;
        o = parseFloat(o) || 0;
        if (m) for (; r < 4; r += 2) {
            o += parseFloat(d.css(b, "padding" + pb[r])) || 0;
            if (m !== "padding") o += parseFloat(d.css(b, "border" + pb[r] + "Width")) || 0;
            if (m === "margin") o += parseFloat(d.css(b, m + pb[r])) || 0
        }
        return o + "px"
    }
    function M(b) {
        return function (e, m) {
            if (typeof e !== "string") {
                m = e;
                e = "*"
            }
            if (d.isFunction(m)) for (var o = e.toLowerCase().split(Ob), r = 0, w = o.length, A, B; r < w; r++) {
                A = o[r];
                if (B = /^\+/.test(A)) A = A.substr(1) || "*";
                A = b[A] = b[A] || [];
                A[B ? "unshift" : "push"](m)
            }
        }
    }
    function ma(b, e, m, o, r, w) {
        r = r || e.dataTypes[0];
        w = w || {};
        w[r] = true;
        r = b[r];
        for (var A = 0, B = r ? r.length : 0, I = b === Jb, U; A < B && (I || !U); A++) {
            U = r[A](e, m, o);
            if (typeof U === "string") if (!I || w[U]) U = f;
            else {
                e.dataTypes.unshift(U);
                U = ma(b, e, m, o, U, w)
            }
        }
        if ((I || !U) && !w["*"]) U = ma(b, e, m, o, "*", w);
        return U
    }
    function oa(b, e) {
        var m, o, r = d.ajaxSettings.flatOptions || {};
        for (m in e) if (e[m] !== f)(r[m] ? b : o || (o = {}))[m] = e[m];
        o && d.extend(true, b, o)
    }
    function Da(b, e, m, o) {
        if (d.isArray(e)) d.each(e, function (w, A) {
            m || Zb.test(b) ? o(b, A) : Da(b + "[" + (typeof A === "object" ? w : "") + "]", A, m, o)
        });
        else if (!m && d.type(e) === "object") for (var r in e) Da(b + "[" + r + "]", e[r], m, o);
        else o(b, e)
    }
    function ya() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {}
    }
    function pa() {
        setTimeout(Ma, 0);
        return Eb = d.now()
    }
    function Ma() {
        Eb = f
    }
    function Z(b, e) {
        var m = {};
        d.each(Fb.concat.apply([], Fb.slice(0, e)), function () {
            m[this] = b
        });
        return m
    }
    function ja(b) {
        if (!Kb[b]) {
            var e = x.body,
                m = d("<" + b + ">").appendTo(e),
                o = m.css("display");
            m.remove();
            if (o === "none" || o === "") {
                if (!hb) {
                    hb = x.createElement("iframe");
                    hb.frameBorder = hb.width = hb.height = 0
                }
                e.appendChild(hb);
                if (!zb || !hb.createElement) {
                    zb = (hb.contentWindow || hb.contentDocument).document;
                    zb.write((d.support.boxModel ? "<!doctype html>" : "") + "<html><body>");
                    zb.close()
                }
                m = zb.createElement(b);
                zb.body.appendChild(m);
                o = d.css(m, "display");
                e.removeChild(hb)
            }
            Kb[b] = o
        }
        return Kb[b]
    }
    function C(b) {
        return d.isWindow(b) ? b : b.nodeType === 9 ? b.defaultView || b.parentWindow : false
    }
    var x = a.document,
        K = a.navigator,
        y = a.location,
        d = function () {
            function b() {
                if (!e.isReady) {
                    try {
                        x.documentElement.doScroll("left")
                    } catch (F) {
                        setTimeout(b, 1);
                        return
                    }
                    e.ready()
                }
            }
            var e = function (F, ca) {
                    return new e.fn.init(F, ca, r)
                },
                m = a.jQuery,
                o = a.$,
                r, w = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
                A = /\S/,
                B = /^\s+/,
                I = /\s+$/,
                U = /^<(\w+)\s*\/?>(?:<\/\1>)?$/,
                aa = /^[\],:{}\s]*$/,
                V = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
                qa = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
                ba = /(?:^|:|,)(?:\s*\[)+/g,
                Ia = /(webkit)[ \/]([\w.]+)/,
                Pa = /(opera)(?:.*version)?[ \/]([\w.]+)/,
                Oa = /(msie) ([\w.]+)/,
                Qa = /(mozilla)(?:.*? rv:([\w.]+))?/,
                mb = /-([a-z]|[0-9])/ig,
                db = /^-ms-/,
                Ka = function (F, ca) {
                    return (ca + "").toUpperCase()
                },
                eb = K.userAgent,
                v, D, P = Object.prototype.toString,
                R = Object.prototype.hasOwnProperty,
                T = Array.prototype.push,
                X = Array.prototype.slice,
                ra = String.prototype.trim,
                ia = Array.prototype.indexOf,
                La = {};
            e.fn = e.prototype = {
                constructor: e,
                init: function (F, ca, ha) {
                    var ea;
                    if (!F) return this;
                    if (F.nodeType) {
                        this.context = this[0] = F;
                        this.length = 1;
                        return this
                    }
                    if (F === "body" && !ca && x.body) {
                        this.context = x;
                        this[0] = x.body;
                        this.selector = F;
                        this.length = 1;
                        return this
                    }
                    if (typeof F === "string") if ((ea = F.charAt(0) === "<" && F.charAt(F.length - 1) === ">" && F.length >= 3 ? [null, F, null] : w.exec(F)) && (ea[1] || !ca)) if (ea[1]) {
                        ha = (ca = ca instanceof e ? ca[0] : ca) ? ca.ownerDocument || ca : x;
                        if (F = U.exec(F)) if (e.isPlainObject(ca)) {
                            F = [x.createElement(F[1])];
                            e.fn.attr.call(F, ca, true)
                        } else F = [ha.createElement(F[1])];
                        else {
                            F = e.buildFragment([ea[1]], [ha]);
                            F = (F.cacheable ? e.clone(F.fragment) : F.fragment).childNodes
                        }
                        return e.merge(this, F)
                    } else {
                        if ((ca = x.getElementById(ea[2])) && ca.parentNode) {
                            if (ca.id !== ea[2]) return ha.find(F);
                            this.length = 1;
                            this[0] = ca
                        }
                        this.context = x;
                        this.selector = F;
                        return this
                    } else return !ca || ca.jquery ? (ca || ha).find(F) : this.constructor(ca).find(F);
                    else if (e.isFunction(F)) return ha.ready(F);
                    if (F.selector !== f) {
                        this.selector = F.selector;
                        this.context = F.context
                    }
                    return e.makeArray(F, this)
                },
                selector: "",
                jquery: "1.7.2",
                length: 0,
                size: function () {
                    return this.length
                },
                toArray: function () {
                    return X.call(this, 0)
                },
                get: function (F) {
                    return F == null ? this.toArray() : F < 0 ? this[this.length + F] : this[F]
                },
                pushStack: function (F, ca, ha) {
                    var ea = this.constructor();
                    e.isArray(F) ? T.apply(ea, F) : e.merge(ea, F);
                    ea.prevObject = this;
                    ea.context = this.context;
                    if (ca === "find") ea.selector = this.selector + (this.selector ? " " : "") + ha;
                    else if (ca) ea.selector = this.selector + "." + ca + "(" + ha + ")";
                    return ea
                },
                each: function (F, ca) {
                    return e.each(this, F, ca)
                },
                ready: function (F) {
                    e.bindReady();
                    v.add(F);
                    return this
                },
                eq: function (F) {
                    F = +F;
                    return F === -1 ? this.slice(F) : this.slice(F, F + 1)
                },
                first: function () {
                    return this.eq(0)
                },
                last: function () {
                    return this.eq(-1)
                },
                slice: function () {
                    return this.pushStack(X.apply(this, arguments), "slice", X.call(arguments).join(","))
                },
                map: function (F) {
                    return this.pushStack(e.map(this, function (ca, ha) {
                        return F.call(ca, ha, ca)
                    }))
                },
                end: function () {
                    return this.prevObject || this.constructor(null)
                },
                push: T,
                sort: [].sort,
                splice: [].splice
            };
            e.fn.init.prototype = e.fn;
            e.extend = e.fn.extend = function () {
                var F, ca, ha, ea, Fa, za = arguments[0] || {},
                    Ta = 1,
                    $a = arguments.length,
                    ib = false;
                if (typeof za === "boolean") {
                    ib = za;
                    za = arguments[1] || {};
                    Ta = 2
                }
                if (typeof za !== "object" && !e.isFunction(za)) za = {};
                if ($a === Ta) {
                    za = this;
                    --Ta
                }
                for (; Ta < $a; Ta++) if ((F = arguments[Ta]) != null) for (ca in F) {
                    ha = za[ca];
                    ea = F[ca];
                    if (za !== ea) if (ib && ea && (e.isPlainObject(ea) || (Fa = e.isArray(ea)))) {
                        if (Fa) {
                            Fa = false;
                            ha = ha && e.isArray(ha) ? ha : []
                        } else ha = ha && e.isPlainObject(ha) ? ha : {};
                        za[ca] = e.extend(ib, ha, ea)
                    } else if (ea !== f) za[ca] = ea
                }
                return za
            };
            e.extend({
                noConflict: function (F) {
                    if (a.$ === e) a.$ = o;
                    if (F && a.jQuery === e) a.jQuery = m;
                    return e
                },
                isReady: false,
                readyWait: 1,
                holdReady: function (F) {
                    if (F) e.readyWait++;
                    else e.ready(true)
                },
                ready: function (F) {
                    if (F === true && !--e.readyWait || F !== true && !e.isReady) {
                        if (!x.body) return setTimeout(e.ready, 1);
                        e.isReady = true;
                        if (!(F !== true && --e.readyWait > 0)) {
                            v.fireWith(x, [e]);
                            e.fn.trigger && e(x).trigger("ready").off("ready")
                        }
                    }
                },
                bindReady: function () {
                    if (!v) {
                        v = e.Callbacks("once memory");
                        if (x.readyState === "complete") return setTimeout(e.ready, 1);
                        if (x.addEventListener) {
                            x.addEventListener("DOMContentLoaded", D, false);
                            a.addEventListener("load", e.ready, false)
                        } else if (x.attachEvent) {
                            x.attachEvent("onreadystatechange", D);
                            a.attachEvent("onload", e.ready);
                            var F = false;
                            try {
                                F = a.frameElement == null
                            } catch (ca) {}
                            x.documentElement.doScroll && F && b()
                        }
                    }
                },
                isFunction: function (F) {
                    return e.type(F) === "function"
                },
                isArray: Array.isArray ||
                function (F) {
                    return e.type(F) === "array"
                },
                isWindow: function (F) {
                    return F != null && F == F.window
                },
                isNumeric: function (F) {
                    return !isNaN(parseFloat(F)) && isFinite(F)
                },
                type: function (F) {
                    return F == null ? String(F) : La[P.call(F)] || "object"
                },
                isPlainObject: function (F) {
                    if (!F || e.type(F) !== "object" || F.nodeType || e.isWindow(F)) return false;
                    try {
                        if (F.constructor && !R.call(F, "constructor") && !R.call(F.constructor.prototype, "isPrototypeOf")) return false
                    } catch (ca) {
                        return false
                    }
                    for (var ha in F);
                    return ha === f || R.call(F, ha)
                },
                isEmptyObject: function (F) {
                    for (var ca in F) return false;
                    return true
                },
                error: function (F) {
                    throw Error(F);
                },
                parseJSON: function (F) {
                    if (typeof F !== "string" || !F) return null;
                    F = e.trim(F);
                    if (a.JSON && a.JSON.parse) return a.JSON.parse(F);
                    if (aa.test(F.replace(V, "@").replace(qa, "]").replace(ba, ""))) return (new Function("return " + F))();
                    e.error("Invalid JSON: " + F)
                },
                parseXML: function (F) {
                    if (typeof F !== "string" || !F) return null;
                    var ca, ha;
                    try {
                        if (a.DOMParser) {
                            ha = new DOMParser;
                            ca = ha.parseFromString(F, "text/xml")
                        } else {
                            ca = new ActiveXObject("Microsoft.XMLDOM");
                            ca.async = "false";
                            ca.loadXML(F)
                        }
                    } catch (ea) {
                        ca = f
                    }
                    if (!ca || !ca.documentElement || ca.getElementsByTagName("parsererror").length) e.error("Invalid XML: " + F);
                    return ca
                },
                noop: function () {},
                globalEval: function (F) {
                    if (F && A.test(F))(a.execScript ||
                    function (ca) {
                        a.eval.call(a, ca)
                    })(F)
                },
                camelCase: function (F) {
                    return F.replace(db, "ms-").replace(mb, Ka)
                },
                nodeName: function (F, ca) {
                    return F.nodeName && F.nodeName.toUpperCase() === ca.toUpperCase()
                },
                each: function (F, ca, ha) {
                    var ea, Fa = 0,
                        za = F.length,
                        Ta = za === f || e.isFunction(F);
                    if (ha) if (Ta) for (ea in F) {
                        if (ca.apply(F[ea], ha) === false) break
                    } else for (; Fa < za;) {
                        if (ca.apply(F[Fa++], ha) === false) break
                    } else if (Ta) for (ea in F) {
                        if (ca.call(F[ea], ea, F[ea]) === false) break
                    } else for (; Fa < za;) if (ca.call(F[Fa], Fa, F[Fa++]) === false) break;
                    return F
                },
                trim: ra ?
                function (F) {
                    return F == null ? "" : ra.call(F)
                } : function (F) {
                    return F == null ? "" : F.toString().replace(B, "").replace(I, "")
                },
                makeArray: function (F, ca) {
                    var ha = ca || [];
                    if (F != null) {
                        var ea = e.type(F);
                        F.length == null || ea === "string" || ea === "function" || ea === "regexp" || e.isWindow(F) ? T.call(ha, F) : e.merge(ha, F)
                    }
                    return ha
                },
                inArray: function (F, ca, ha) {
                    var ea;
                    if (ca) {
                        if (ia) return ia.call(ca, F, ha);
                        ea = ca.length;
                        for (ha = ha ? ha < 0 ? Math.max(0, ea + ha) : ha : 0; ha < ea; ha++) if (ha in ca && ca[ha] === F) return ha
                    }
                    return -1
                },
                merge: function (F, ca) {
                    var ha = F.length,
                        ea = 0;
                    if (typeof ca.length === "number") for (var Fa = ca.length; ea < Fa; ea++) F[ha++] = ca[ea];
                    else for (; ca[ea] !== f;) F[ha++] = ca[ea++];
                    F.length = ha;
                    return F
                },
                grep: function (F, ca, ha) {
                    var ea = [],
                        Fa;
                    ha = !! ha;
                    for (var za = 0, Ta = F.length; za < Ta; za++) {
                        Fa = !! ca(F[za], za);
                        ha !== Fa && ea.push(F[za])
                    }
                    return ea
                },
                map: function (F, ca, ha) {
                    var ea, Fa, za = [],
                        Ta = 0,
                        $a = F.length;
                    if (F instanceof e || $a !== f && typeof $a === "number" && ($a > 0 && F[0] && F[$a - 1] || $a === 0 || e.isArray(F))) for (; Ta < $a; Ta++) {
                        ea = ca(F[Ta], Ta, ha);
                        if (ea != null) za[za.length] = ea
                    } else for (Fa in F) {
                        ea = ca(F[Fa], Fa, ha);
                        if (ea != null) za[za.length] = ea
                    }
                    return za.concat.apply([], za)
                },
                guid: 1,
                proxy: function (F, ca) {
                    if (typeof ca === "string") {
                        var ha = F[ca];
                        ca = F;
                        F = ha
                    }
                    if (!e.isFunction(F)) return f;
                    var ea = X.call(arguments, 2);
                    ha = function () {
                        return F.apply(ca, ea.concat(X.call(arguments)))
                    };
                    ha.guid = F.guid = F.guid || ha.guid || e.guid++;
                    return ha
                },
                access: function (F, ca, ha, ea, Fa, za, Ta) {
                    var $a, ib = ha == null,
                        ab = 0,
                        qb = F.length;
                    if (ha && typeof ha === "object") {
                        for (ab in ha) e.access(F, ca, ab, ha[ab], 1, za, ea);
                        Fa = 1
                    } else if (ea !== f) {
                        $a = Ta === f && e.isFunction(ea);
                        if (ib) if ($a) {
                            $a = ca;
                            ca = function (Lb, Db, jb) {
                                return $a.call(e(Lb), jb)
                            }
                        } else {
                            ca.call(F, ea);
                            ca = null
                        }
                        if (ca) for (; ab < qb; ab++) ca(F[ab], ha, $a ? ea.call(F[ab], ab, ca(F[ab], ha)) : ea, Ta);
                        Fa = 1
                    }
                    return Fa ? F : ib ? ca.call(F) : qb ? ca(F[0], ha) : za
                },
                now: function () {
                    return (new Date).getTime()
                },
                uaMatch: function (F) {
                    F = F.toLowerCase();
                    F = Ia.exec(F) || Pa.exec(F) || Oa.exec(F) || F.indexOf("compatible") < 0 && Qa.exec(F) || [];
                    return {
                        browser: F[1] || "",
                        version: F[2] || "0"
                    }
                },
                sub: function () {
                    function F(ha, ea) {
                        return new F.fn.init(ha, ea)
                    }
                    e.extend(true, F, this);
                    F.superclass = this;
                    F.fn = F.prototype = this();
                    F.fn.constructor = F;
                    F.sub = this.sub;
                    F.fn.init = function (ha, ea) {
                        if (ea && ea instanceof e && !(ea instanceof F)) ea = F(ea);
                        return e.fn.init.call(this, ha, ea, ca)
                    };
                    F.fn.init.prototype = F.fn;
                    var ca = F(x);
                    return F
                },
                browser: {}
            });
            e.each("Boolean Number String Function Array Date RegExp Object".split(" "), function (F, ca) {
                La["[object " + ca + "]"] = ca.toLowerCase()
            });
            eb = e.uaMatch(eb);
            if (eb.browser) {
                e.browser[eb.browser] = true;
                e.browser.version = eb.version
            }
            if (e.browser.webkit) e.browser.safari = true;
            if (A.test("\u00a0")) {
                B = /^[\s\xA0]+/;
                I = /[\s\xA0]+$/
            }
            r = e(x);
            if (x.addEventListener) D = function () {
                x.removeEventListener("DOMContentLoaded", D, false);
                e.ready()
            };
            else if (x.attachEvent) D = function () {
                if (x.readyState === "complete") {
                    x.detachEvent("onreadystatechange", D);
                    e.ready()
                }
            };
            return e
        }(),
        Q = {};
    d.Callbacks = function (b) {
        b = b ? Q[b] || i(b) : {};
        var e = [],
            m = [],
            o, r, w, A, B, I, U = function (qa) {
                var ba, Ia, Pa, Oa;
                ba = 0;
                for (Ia = qa.length; ba < Ia; ba++) {
                    Pa = qa[ba];
                    Oa = d.type(Pa);
                    if (Oa === "array") U(Pa);
                    else if (Oa === "function") if (!b.unique || !V.has(Pa)) e.push(Pa)
                }
            },
            aa = function (qa, ba) {
                ba = ba || [];
                o = !b.memory || [qa, ba];
                w = r = true;
                I = A || 0;
                A = 0;
                for (B = e.length; e && I < B; I++) if (e[I].apply(qa, ba) === false && b.stopOnFalse) {
                    o = true;
                    break
                }
                w = false;
                if (e) if (b.once) if (o === true) V.disable();
                else e = [];
                else if (m && m.length) {
                    o = m.shift();
                    V.fireWith(o[0], o[1])
                }
            },
            V = {
                add: function () {
                    if (e) {
                        var qa = e.length;
                        U(arguments);
                        if (w) B = e.length;
                        else if (o && o !== true) {
                            A = qa;
                            aa(o[0], o[1])
                        }
                    }
                    return this
                },
                remove: function () {
                    if (e) for (var qa = arguments, ba = 0, Ia = qa.length; ba < Ia; ba++) for (var Pa = 0; Pa < e.length; Pa++) if (qa[ba] === e[Pa]) {
                        if (w) if (Pa <= B) {
                            B--;
                            Pa <= I && I--
                        }
                        e.splice(Pa--, 1);
                        if (b.unique) break
                    }
                    return this
                },
                has: function (qa) {
                    if (e) for (var ba = 0, Ia = e.length; ba < Ia; ba++) if (qa === e[ba]) return true;
                    return false
                },
                empty: function () {
                    e = [];
                    return this
                },
                disable: function () {
                    e = m = o = f;
                    return this
                },
                disabled: function () {
                    return !e
                },
                lock: function () {
                    m = f;
                    if (!o || o === true) V.disable();
                    return this
                },
                locked: function () {
                    return !m
                },
                fireWith: function (qa, ba) {
                    if (m) if (w) b.once || m.push([qa, ba]);
                    else b.once && o || aa(qa, ba);
                    return this
                },
                fire: function () {
                    V.fireWith(this, arguments);
                    return this
                },
                fired: function () {
                    return !!r
                }
            };
        return V
    };
    var N = [].slice;
    d.extend({
        Deferred: function (b) {
            var e = d.Callbacks("once memory"),
                m = d.Callbacks("once memory"),
                o = d.Callbacks("memory"),
                r = "pending",
                w = {
                    resolve: e,
                    reject: m,
                    notify: o
                },
                A = {
                    done: e.add,
                    fail: m.add,
                    progress: o.add,
                    state: function () {
                        return r
                    },
                    isResolved: e.fired,
                    isRejected: m.fired,
                    then: function (U, aa, V) {
                        B.done(U).fail(aa).progress(V);
                        return this
                    },
                    always: function () {
                        B.done.apply(B, arguments).fail.apply(B, arguments);
                        return this
                    },
                    pipe: function (U, aa, V) {
                        return d.Deferred(function (qa) {
                            d.each({
                                done: [U, "resolve"],
                                fail: [aa, "reject"],
                                progress: [V, "notify"]
                            }, function (ba, Ia) {
                                var Pa = Ia[0],
                                    Oa = Ia[1],
                                    Qa;
                                if (d.isFunction(Pa)) B[ba](function () {
                                    if ((Qa = Pa.apply(this, arguments)) && d.isFunction(Qa.promise)) Qa.promise().then(qa.resolve, qa.reject, qa.notify);
                                    else qa[Oa + "With"](this === B ? qa : this, [Qa])
                                });
                                else B[ba](qa[Oa])
                            })
                        }).promise()
                    },
                    promise: function (U) {
                        if (U == null) U = A;
                        else for (var aa in A) U[aa] = A[aa];
                        return U
                    }
                },
                B = A.promise({}),
                I;
            for (I in w) {
                B[I] = w[I].fire;
                B[I + "With"] = w[I].fireWith
            }
            B.done(function () {
                r = "resolved"
            }, m.disable, o.lock).fail(function () {
                r = "rejected"
            }, e.disable, o.lock);
            b && b.call(B, B);
            return B
        },
        when: function (b) {
            function e(aa) {
                return function (V) {
                    o[aa] = arguments.length > 1 ? N.call(arguments, 0) : V;
                    --B || I.resolveWith(I, o)
                }
            }
            function m(aa) {
                return function (V) {
                    A[aa] = arguments.length > 1 ? N.call(arguments, 0) : V;
                    I.notifyWith(U, A)
                }
            }
            var o = N.call(arguments, 0),
                r = 0,
                w = o.length,
                A = Array(w),
                B = w,
                I = w <= 1 && b && d.isFunction(b.promise) ? b : d.Deferred(),
                U = I.promise();
            if (w > 1) {
                for (; r < w; r++) if (o[r] && o[r].promise && d.isFunction(o[r].promise)) o[r].promise().then(e(r), I.reject, m(r));
                else --B;
                B || I.resolveWith(I, o)
            } else if (I !== b) I.resolveWith(I, w ? [b] : []);
            return U
        }
    });
    d.support = function () {
        var b, e, m, o, r, w, A, B, I = x.createElement("div");
        I.setAttribute("className", "t");
        I.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>";
        e = I.getElementsByTagName("*");
        m = I.getElementsByTagName("a")[0];
        if (!e || !e.length || !m) return {};
        o = x.createElement("select");
        r = o.appendChild(x.createElement("option"));
        e = I.getElementsByTagName("input")[0];
        b = {
            leadingWhitespace: I.firstChild.nodeType === 3,
            tbody: !I.getElementsByTagName("tbody").length,
            htmlSerialize: !! I.getElementsByTagName("link").length,
            style: /top/.test(m.getAttribute("style")),
            hrefNormalized: m.getAttribute("href") === "/a",
            opacity: /^0.55/.test(m.style.opacity),
            cssFloat: !! m.style.cssFloat,
            checkOn: e.value === "on",
            optSelected: r.selected,
            getSetAttribute: I.className !== "t",
            enctype: !! x.createElement("form").enctype,
            html5Clone: x.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>",
            submitBubbles: true,
            changeBubbles: true,
            focusinBubbles: false,
            deleteExpando: true,
            noCloneEvent: true,
            inlineBlockNeedsLayout: false,
            shrinkWrapBlocks: false,
            reliableMarginRight: true,
            pixelMargin: true
        };
        d.boxModel = b.boxModel = x.compatMode === "CSS1Compat";
        e.checked = true;
        b.noCloneChecked = e.cloneNode(true).checked;
        o.disabled = true;
        b.optDisabled = !r.disabled;
        try {
            delete I.test
        } catch (U) {
            b.deleteExpando = false
        }
        if (!I.addEventListener && I.attachEvent && I.fireEvent) {
            I.attachEvent("onclick", function () {
                b.noCloneEvent = false
            });
            I.cloneNode(true).fireEvent("onclick")
        }
        e = x.createElement("input");
        e.value = "t";
        e.setAttribute("type", "radio");
        b.radioValue = e.value === "t";
        e.setAttribute("checked", "checked");
        e.setAttribute("name", "t");
        I.appendChild(e);
        m = x.createDocumentFragment();
        m.appendChild(I.lastChild);
        b.checkClone = m.cloneNode(true).cloneNode(true).lastChild.checked;
        b.appendChecked = e.checked;
        m.removeChild(e);
        m.appendChild(I);
        if (I.attachEvent) for (A in {
            submit: 1,
            change: 1,
            focusin: 1
        }) {
            e = "on" + A;
            B = e in I;
            if (!B) {
                I.setAttribute(e, "return;");
                B = typeof I[e] === "function"
            }
            b[A + "Bubbles"] = B
        }
        m.removeChild(I);
        m = o = r = I = e = null;
        d(function () {
            var aa, V, qa, ba, Ia = x.getElementsByTagName("body")[0];
            if (Ia) {
                aa = x.createElement("div");
                aa.style.cssText = "padding:0;margin:0;border:0;visibility:hidden;width:0;height:0;position:static;top:0;margin-top:1px";
                Ia.insertBefore(aa, Ia.firstChild);
                I = x.createElement("div");
                aa.appendChild(I);
                I.innerHTML = "<table><tr><td style='padding:0;margin:0;border:0;display:none'></td><td>t</td></tr></table>";
                w = I.getElementsByTagName("td");
                B = w[0].offsetHeight === 0;
                w[0].style.display = "";
                w[1].style.display = "none";
                b.reliableHiddenOffsets = B && w[0].offsetHeight === 0;
                if (a.getComputedStyle) {
                    I.innerHTML = "";
                    V = x.createElement("div");
                    V.style.width = "0";
                    V.style.marginRight = "0";
                    I.style.width = "2px";
                    I.appendChild(V);
                    b.reliableMarginRight = (parseInt((a.getComputedStyle(V, null) || {
                        marginRight: 0
                    }).marginRight, 10) || 0) === 0
                }
                if (typeof I.style.zoom !== "undefined") {
                    I.innerHTML = "";
                    I.style.width = I.style.padding = "1px";
                    I.style.border = 0;
                    I.style.overflow = "hidden";
                    I.style.display = "inline";
                    I.style.zoom = 1;
                    b.inlineBlockNeedsLayout = I.offsetWidth === 3;
                    I.style.display = "block";
                    I.style.overflow = "visible";
                    I.innerHTML = "<div style='width:5px;'></div>";
                    b.shrinkWrapBlocks = I.offsetWidth !== 3
                }
                I.style.cssText = "position:absolute;top:0;left:0;width:1px;height:1px;padding:0;margin:0;border:0;visibility:hidden;";
                I.innerHTML = "<div style='position:absolute;top:0;left:0;width:1px;height:1px;padding:0;margin:0;border:5px solid #000;display:block;'><div style='padding:0;margin:0;border:0;display:block;overflow:hidden;'></div></div><table style='position:absolute;top:0;left:0;width:1px;height:1px;padding:0;margin:0;border:5px solid #000;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";
                V = I.firstChild;
                qa = V.firstChild;
                ba = {
                    doesNotAddBorder: qa.offsetTop !== 5,
                    doesAddBorderForTableAndCells: V.nextSibling.firstChild.firstChild.offsetTop === 5
                };
                qa.style.position = "fixed";
                qa.style.top = "20px";
                ba.fixedPosition = qa.offsetTop === 20 || qa.offsetTop === 15;
                qa.style.position = qa.style.top = "";
                V.style.overflow = "hidden";
                V.style.position = "relative";
                ba.subtractsBorderForOverflowNotVisible = qa.offsetTop === -5;
                ba.doesNotIncludeMarginInBodyOffset = Ia.offsetTop !== 1;
                if (a.getComputedStyle) {
                    I.style.marginTop = "1%";
                    b.pixelMargin = (a.getComputedStyle(I, null) || {
                        marginTop: 0
                    }).marginTop !== "1%"
                }
                if (typeof aa.style.zoom !== "undefined") aa.style.zoom = 1;
                Ia.removeChild(aa);
                I = null;
                d.extend(b, ba)
            }
        });
        return b
    }();
    var H = /^(?:\{.*\}|\[.*\])$/,
        O = /([A-Z])/g;
    d.extend({
        cache: {},
        uuid: 0,
        expando: "jQuery" + (d.fn.jquery + Math.random()).replace(/\D/g, ""),
        noData: {
            embed: true,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            applet: true
        },
        hasData: function (b) {
            b = b.nodeType ? d.cache[b[d.expando]] : b[d.expando];
            return !!b && !h(b)
        },
        data: function (b, e, m, o) {
            if (d.acceptData(b)) {
                var r;
                r = d.expando;
                var w = typeof e === "string",
                    A = b.nodeType,
                    B = A ? d.cache : b,
                    I = A ? b[r] : b[r] && r,
                    U = e === "events";
                if (!((!I || !B[I] || !U && !o && !B[I].data) && w && m === f)) {
                    if (!I) if (A) b[r] = I = ++d.uuid;
                    else I = r;
                    if (!B[I]) {
                        B[I] = {};
                        if (!A) B[I].toJSON = d.noop
                    }
                    if (typeof e === "object" || typeof e === "function") if (o) B[I] = d.extend(B[I], e);
                    else B[I].data = d.extend(B[I].data, e);
                    r = b = B[I];
                    if (!o) {
                        if (!b.data) b.data = {};
                        b = b.data
                    }
                    if (m !== f) b[d.camelCase(e)] = m;
                    if (U && !b[e]) return r.events;
                    if (w) {
                        m = b[e];
                        if (m == null) m = b[d.camelCase(e)]
                    } else m = b;
                    return m
                }
            }
        },
        removeData: function (b, e, m) {
            if (d.acceptData(b)) {
                var o, r, w, A = d.expando,
                    B = b.nodeType,
                    I = B ? d.cache : b,
                    U = B ? b[A] : A;
                if (I[U]) {
                    if (e) if (o = m ? I[U] : I[U].data) {
                        if (!d.isArray(e)) if (e in o) e = [e];
                        else {
                            e = d.camelCase(e);
                            e = e in o ? [e] : e.split(" ")
                        }
                        r = 0;
                        for (w = e.length; r < w; r++) delete o[e[r]];
                        if (!(m ? h : d.isEmptyObject)(o)) return
                    }
                    if (!m) {
                        delete I[U].data;
                        if (!h(I[U])) return
                    }
                    if (d.support.deleteExpando || !I.setInterval) delete I[U];
                    else I[U] = null;
                    if (B) if (d.support.deleteExpando) delete b[A];
                    else if (b.removeAttribute) b.removeAttribute(A);
                    else b[A] = null
                }
            }
        },
        _data: function (b, e, m) {
            return d.data(b, e, m, true)
        },
        acceptData: function (b) {
            if (b.nodeName) {
                var e = d.noData[b.nodeName.toLowerCase()];
                if (e) return !(e === true || b.getAttribute("classid") !== e)
            }
            return true
        }
    });
    d.fn.extend({
        data: function (b, e) {
            var m, o, r, w, A, B = this[0],
                I = 0,
                U = null;
            if (b === f) {
                if (this.length) {
                    U = d.data(B);
                    if (B.nodeType === 1 && !d._data(B, "parsedAttrs")) {
                        r = B.attributes;
                        for (A = r.length; I < A; I++) {
                            w = r[I].name;
                            if (w.indexOf("data-") === 0) {
                                w = d.camelCase(w.substring(5));
                                g(B, w, U[w])
                            }
                        }
                        d._data(B, "parsedAttrs", true)
                    }
                }
                return U
            }
            if (typeof b === "object") return this.each(function () {
                d.data(this, b)
            });
            m = b.split(".", 2);
            m[1] = m[1] ? "." + m[1] : "";
            o = m[1] + "!";
            return d.access(this, function (aa) {
                if (aa === f) {
                    U = this.triggerHandler("getData" + o, [m[0]]);
                    if (U === f && B) {
                        U = d.data(B, b);
                        U = g(B, b, U)
                    }
                    return U === f && m[1] ? this.data(m[0]) : U
                }
                m[1] = aa;
                this.each(function () {
                    var V = d(this);
                    V.triggerHandler("setData" + o, m);
                    d.data(this, b, aa);
                    V.triggerHandler("changeData" + o, m)
                })
            }, null, e, arguments.length > 1, null, false)
        },
        removeData: function (b) {
            return this.each(function () {
                d.removeData(this, b)
            })
        }
    });
    d.extend({
        _mark: function (b, e) {
            if (b) {
                e = (e || "fx") + "mark";
                d._data(b, e, (d._data(b, e) || 0) + 1)
            }
        },
        _unmark: function (b, e, m) {
            if (b !== true) {
                m = e;
                e = b;
                b = false
            }
            if (e) {
                m = m || "fx";
                var o = m + "mark";
                if (b = b ? 0 : (d._data(e, o) || 1) - 1) d._data(e, o, b);
                else {
                    d.removeData(e, o, true);
                    j(e, m, "mark")
                }
            }
        },
        queue: function (b, e, m) {
            var o;
            if (b) {
                e = (e || "fx") + "queue";
                o = d._data(b, e);
                if (m) if (!o || d.isArray(m)) o = d._data(b, e, d.makeArray(m));
                else o.push(m);
                return o || []
            }
        },
        dequeue: function (b, e) {
            e = e || "fx";
            var m = d.queue(b, e),
                o = m.shift(),
                r = {};
            if (o === "inprogress") o = m.shift();
            if (o) {
                e === "fx" && m.unshift("inprogress");
                d._data(b, e + ".run", r);
                o.call(b, function () {
                    d.dequeue(b, e)
                }, r)
            }
            if (!m.length) {
                d.removeData(b, e + "queue " + e + ".run", true);
                j(b, e, "queue")
            }
        }
    });
    d.fn.extend({
        queue: function (b, e) {
            var m = 2;
            if (typeof b !== "string") {
                e = b;
                b = "fx";
                m--
            }
            if (arguments.length < m) return d.queue(this[0], b);
            return e === f ? this : this.each(function () {
                var o = d.queue(this, b, e);
                b === "fx" && o[0] !== "inprogress" && d.dequeue(this, b)
            })
        },
        dequeue: function (b) {
            return this.each(function () {
                d.dequeue(this, b)
            })
        },
        delay: function (b, e) {
            b = d.fx ? d.fx.speeds[b] || b : b;
            return this.queue(e || "fx", function (m, o) {
                var r = setTimeout(m, b);
                o.stop = function () {
                    clearTimeout(r)
                }
            })
        },
        clearQueue: function (b) {
            return this.queue(b || "fx", [])
        },
        promise: function (b, e) {
            function m() {
                --A || o.resolveWith(r, [r])
            }
            if (typeof b !== "string") {
                e = b;
                b = f
            }
            b = b || "fx";
            for (var o = d.Deferred(), r = this, w = r.length, A = 1, B = b + "defer", I = b + "queue", U = b + "mark", aa; w--;) if (aa = d.data(r[w], B, f, true) || (d.data(r[w], I, f, true) || d.data(r[w], U, f, true)) && d.data(r[w], B, d.Callbacks("once memory"), true)) {
                A++;
                aa.add(m)
            }
            m();
            return o.promise(e)
        }
    });
    var Y = /[\n\t\r]/g,
        L = /\s+/,
        S = /\r/g,
        ga = /^(?:button|input)$/i,
        la = /^(?:button|input|object|select|textarea)$/i,
        Ga = /^a(?:rea)?$/i,
        Na = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        G = d.support.getSetAttribute,
        da, sa, ta;
    d.fn.extend({
        attr: function (b, e) {
            return d.access(this, d.attr, b, e, arguments.length > 1)
        },
        removeAttr: function (b) {
            return this.each(function () {
                d.removeAttr(this, b)
            })
        },
        prop: function (b, e) {
            return d.access(this, d.prop, b, e, arguments.length > 1)
        },
        removeProp: function (b) {
            b = d.propFix[b] || b;
            return this.each(function () {
                try {
                    this[b] = f;
                    delete this[b]
                } catch (e) {}
            })
        },
        addClass: function (b) {
            var e, m, o, r, w, A, B;
            if (d.isFunction(b)) return this.each(function (I) {
                d(this).addClass(b.call(this, I, this.className))
            });
            if (b && typeof b === "string") {
                e = b.split(L);
                m = 0;
                for (o = this.length; m < o; m++) {
                    r = this[m];
                    if (r.nodeType === 1) if (!r.className && e.length === 1) r.className = b;
                    else {
                        w = " " + r.className + " ";
                        A = 0;
                        for (B = e.length; A < B; A++)~w.indexOf(" " + e[A] + " ") || (w += e[A] + " ");
                        r.className = d.trim(w)
                    }
                }
            }
            return this
        },
        removeClass: function (b) {
            var e, m, o, r, w, A, B;
            if (d.isFunction(b)) return this.each(function (I) {
                d(this).removeClass(b.call(this, I, this.className))
            });
            if (b && typeof b === "string" || b === f) {
                e = (b || "").split(L);
                m = 0;
                for (o = this.length; m < o; m++) {
                    r = this[m];
                    if (r.nodeType === 1 && r.className) if (b) {
                        w = (" " + r.className + " ").replace(Y, " ");
                        A = 0;
                        for (B = e.length; A < B; A++) w = w.replace(" " + e[A] + " ", " ");
                        r.className = d.trim(w)
                    } else r.className = ""
                }
            }
            return this
        },
        toggleClass: function (b, e) {
            var m = typeof b,
                o = typeof e === "boolean";
            if (d.isFunction(b)) return this.each(function (r) {
                d(this).toggleClass(b.call(this, r, this.className, e), e)
            });
            return this.each(function () {
                if (m === "string") for (var r, w = 0, A = d(this), B = e, I = b.split(L); r = I[w++];) {
                    B = o ? B : !A.hasClass(r);
                    A[B ? "addClass" : "removeClass"](r)
                } else if (m === "undefined" || m === "boolean") {
                    this.className && d._data(this, "__className__", this.className);
                    this.className = this.className || b === false ? "" : d._data(this, "__className__") || ""
                }
            })
        },
        hasClass: function (b) {
            b = " " + b + " ";
            for (var e = 0, m = this.length; e < m; e++) if (this[e].nodeType === 1 && (" " + this[e].className + " ").replace(Y, " ").indexOf(b) > -1) return true;
            return false
        },
        val: function (b) {
            var e, m, o, r = this[0];
            if (arguments.length) {
                o = d.isFunction(b);
                return this.each(function (w) {
                    var A = d(this);
                    if (this.nodeType === 1) {
                        w = o ? b.call(this, w, A.val()) : b;
                        if (w == null) w = "";
                        else if (typeof w === "number") w += "";
                        else if (d.isArray(w)) w = d.map(w, function (B) {
                            return B == null ? "" : B + ""
                        });
                        e = d.valHooks[this.type] || d.valHooks[this.nodeName.toLowerCase()];
                        if (!e || !("set" in e) || e.set(this, w, "value") === f) this.value = w
                    }
                })
            } else if (r) {
                if ((e = d.valHooks[r.type] || d.valHooks[r.nodeName.toLowerCase()]) && "get" in e && (m = e.get(r, "value")) !== f) return m;
                m = r.value;
                return typeof m === "string" ? m.replace(S, "") : m == null ? "" : m
            }
        }
    });
    d.extend({
        valHooks: {
            option: {
                get: function (b) {
                    var e = b.attributes.value;
                    return !e || e.specified ? b.value : b.text
                }
            },
            select: {
                get: function (b) {
                    var e, m, o = b.selectedIndex,
                        r = [],
                        w = b.options,
                        A = b.type === "select-one";
                    if (o < 0) return null;
                    b = A ? o : 0;
                    for (m = A ? o + 1 : w.length; b < m; b++) {
                        e = w[b];
                        if (e.selected && (d.support.optDisabled ? !e.disabled : e.getAttribute("disabled") === null) && (!e.parentNode.disabled || !d.nodeName(e.parentNode, "optgroup"))) {
                            e = d(e).val();
                            if (A) return e;
                            r.push(e)
                        }
                    }
                    if (A && !r.length && w.length) return d(w[o]).val();
                    return r
                },
                set: function (b, e) {
                    var m = d.makeArray(e);
                    d(b).find("option").each(function () {
                        this.selected = d.inArray(d(this).val(), m) >= 0
                    });
                    if (!m.length) b.selectedIndex = -1;
                    return m
                }
            }
        },
        attrFn: {
            val: true,
            css: true,
            html: true,
            text: true,
            data: true,
            width: true,
            height: true,
            offset: true
        },
        attr: function (b, e, m, o) {
            var r, w, A = b.nodeType;
            if (!(!b || A === 3 || A === 8 || A === 2)) {
                if (o && e in d.attrFn) return d(b)[e](m);
                if (typeof b.getAttribute === "undefined") return d.prop(b, e, m);
                if (o = A !== 1 || !d.isXMLDoc(b)) {
                    e = e.toLowerCase();
                    w = d.attrHooks[e] || (Na.test(e) ? sa : da)
                }
                if (m !== f) if (m === null) d.removeAttr(b, e);
                else if (w && "set" in w && o && (r = w.set(b, m, e)) !== f) return r;
                else {
                    b.setAttribute(e, "" + m);
                    return m
                } else if (w && "get" in w && o && (r = w.get(b, e)) !== null) return r;
                else {
                    r = b.getAttribute(e);
                    return r === null ? f : r
                }
            }
        },
        removeAttr: function (b, e) {
            var m, o, r, w, A, B = 0;
            if (e && b.nodeType === 1) {
                o = e.toLowerCase().split(L);
                for (w = o.length; B < w; B++) if (r = o[B]) {
                    m = d.propFix[r] || r;
                    (A = Na.test(r)) || d.attr(b, r, "");
                    b.removeAttribute(G ? r : m);
                    if (A && m in b) b[m] = false
                }
            }
        },
        attrHooks: {
            type: {
                set: function (b, e) {
                    if (ga.test(b.nodeName) && b.parentNode) d.error("type property can't be changed");
                    else if (!d.support.radioValue && e === "radio" && d.nodeName(b, "input")) {
                        var m = b.value;
                        b.setAttribute("type", e);
                        if (m) b.value = m;
                        return e
                    }
                }
            },
            value: {
                get: function (b, e) {
                    if (da && d.nodeName(b, "button")) return da.get(b, e);
                    return e in b ? b.value : null
                },
                set: function (b, e, m) {
                    if (da && d.nodeName(b, "button")) return da.set(b, e, m);
                    b.value = e
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function (b, e, m) {
            var o, r, w = b.nodeType;
            if (!(!b || w === 3 || w === 8 || w === 2)) {
                if (w !== 1 || !d.isXMLDoc(b)) {
                    e = d.propFix[e] || e;
                    r = d.propHooks[e]
                }
                return m !== f ? r && "set" in r && (o = r.set(b, m, e)) !== f ? o : b[e] = m : r && "get" in r && (o = r.get(b, e)) !== null ? o : b[e]
            }
        },
        propHooks: {
            tabIndex: {
                get: function (b) {
                    var e = b.getAttributeNode("tabindex");
                    return e && e.specified ? parseInt(e.value, 10) : la.test(b.nodeName) || Ga.test(b.nodeName) && b.href ? 0 : f
                }
            }
        }
    });
    d.attrHooks.tabindex = d.propHooks.tabIndex;
    sa = {
        get: function (b, e) {
            var m, o = d.prop(b, e);
            return o === true || typeof o !== "boolean" && (m = b.getAttributeNode(e)) && m.nodeValue !== false ? e.toLowerCase() : f
        },
        set: function (b, e, m) {
            if (e === false) d.removeAttr(b, m);
            else {
                e = d.propFix[m] || m;
                if (e in b) b[e] = true;
                b.setAttribute(m, m.toLowerCase())
            }
            return m
        }
    };
    if (!G) {
        ta = {
            name: true,
            id: true,
            coords: true
        };
        da = d.valHooks.button = {
            get: function (b, e) {
                var m;
                return (m = b.getAttributeNode(e)) && (ta[e] ? m.nodeValue !== "" : m.specified) ? m.nodeValue : f
            },
            set: function (b, e, m) {
                var o = b.getAttributeNode(m);
                if (!o) {
                    o = x.createAttribute(m);
                    b.setAttributeNode(o)
                }
                return o.nodeValue = e + ""
            }
        };
        d.attrHooks.tabindex.set = da.set;
        d.each(["width", "height"], function (b, e) {
            d.attrHooks[e] = d.extend(d.attrHooks[e], {
                set: function (m, o) {
                    if (o === "") {
                        m.setAttribute(e, "auto");
                        return o
                    }
                }
            })
        });
        d.attrHooks.contenteditable = {
            get: da.get,
            set: function (b, e, m) {
                if (e === "") e = "false";
                da.set(b, e, m)
            }
        }
    }
    d.support.hrefNormalized || d.each(["href", "src", "width", "height"], function (b, e) {
        d.attrHooks[e] = d.extend(d.attrHooks[e], {
            get: function (m) {
                m = m.getAttribute(e, 2);
                return m === null ? f : m
            }
        })
    });
    if (!d.support.style) d.attrHooks.style = {
        get: function (b) {
            return b.style.cssText.toLowerCase() || f
        },
        set: function (b, e) {
            return b.style.cssText = "" + e
        }
    };
    if (!d.support.optSelected) d.propHooks.selected = d.extend(d.propHooks.selected, {
        get: function () {
            return null
        }
    });
    if (!d.support.enctype) d.propFix.enctype = "encoding";
    d.support.checkOn || d.each(["radio", "checkbox"], function () {
        d.valHooks[this] = {
            get: function (b) {
                return b.getAttribute("value") === null ? "on" : b.value
            }
        }
    });
    d.each(["radio", "checkbox"], function () {
        d.valHooks[this] = d.extend(d.valHooks[this], {
            set: function (b, e) {
                if (d.isArray(e)) return b.checked = d.inArray(d(b).val(), e) >= 0
            }
        })
    });
    var Ba = /^(?:textarea|input|select)$/i,
        W = /^([^\.]*)?(?:\.(.+))?$/,
        na = /(?:^|\s)hover(\.\S+)?\b/,
        ua = /^key/,
        va = /^(?:mouse|contextmenu)|click/,
        J = /^(?:focusinfocus|focusoutblur)$/,
        fa = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,
        ka = function (b) {
            if (b = fa.exec(b)) {
                b[1] = (b[1] || "").toLowerCase();
                b[3] = b[3] && RegExp("(?:^|\\s)" + b[3] + "(?:\\s|$)")
            }
            return b
        },
        Ha = function (b) {
            return d.event.special.hover ? b : b.replace(na, "mouseenter$1 mouseleave$1")
        };
    d.event = {
        add: function (b, e, m, o, r) {
            var w, A, B, I, U, aa, V, qa, ba;
            if (!(b.nodeType === 3 || b.nodeType === 8 || !e || !m || !(w = d._data(b)))) {
                if (m.handler) {
                    V = m;
                    m = V.handler;
                    r = V.selector
                }
                if (!m.guid) m.guid = d.guid++;
                B = w.events;
                if (!B) w.events = B = {};
                A = w.handle;
                if (!A) {
                    w.handle = A = function (Ia) {
                        return typeof d !== "undefined" && (!Ia || d.event.triggered !== Ia.type) ? d.event.dispatch.apply(A.elem, arguments) : f
                    };
                    A.elem = b
                }
                e = d.trim(Ha(e)).split(" ");
                for (w = 0; w < e.length; w++) {
                    I = W.exec(e[w]) || [];
                    U = I[1];
                    aa = (I[2] || "").split(".").sort();
                    ba = d.event.special[U] || {};
                    U = (r ? ba.delegateType : ba.bindType) || U;
                    ba = d.event.special[U] || {};
                    I = d.extend({
                        type: U,
                        origType: I[1],
                        data: o,
                        handler: m,
                        guid: m.guid,
                        selector: r,
                        quick: r && ka(r),
                        namespace: aa.join(".")
                    }, V);
                    qa = B[U];
                    if (!qa) {
                        qa = B[U] = [];
                        qa.delegateCount = 0;
                        if (!ba.setup || ba.setup.call(b, o, aa, A) === false) if (b.addEventListener) b.addEventListener(U, A, false);
                        else b.attachEvent && b.attachEvent("on" + U, A)
                    }
                    if (ba.add) {
                        ba.add.call(b, I);
                        if (!I.handler.guid) I.handler.guid = m.guid
                    }
                    r ? qa.splice(qa.delegateCount++, 0, I) : qa.push(I);
                    d.event.global[U] = true
                }
                b = null
            }
        },
        global: {},
        remove: function (b, e, m, o, r) {
            var w = d.hasData(b) && d._data(b),
                A, B, I, U, aa, V, qa, ba, Ia, Pa;
            if (w && (qa = w.events)) {
                e = d.trim(Ha(e || "")).split(" ");
                for (A = 0; A < e.length; A++) {
                    B = W.exec(e[A]) || [];
                    I = U = B[1];
                    B = B[2];
                    if (I) {
                        ba = d.event.special[I] || {};
                        I = (o ? ba.delegateType : ba.bindType) || I;
                        Ia = qa[I] || [];
                        aa = Ia.length;
                        B = B ? RegExp("(^|\\.)" + B.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
                        for (V = 0; V < Ia.length; V++) {
                            Pa = Ia[V];
                            if ((r || U === Pa.origType) && (!m || m.guid === Pa.guid) && (!B || B.test(Pa.namespace)) && (!o || o === Pa.selector || o === "**" && Pa.selector)) {
                                Ia.splice(V--, 1);
                                Pa.selector && Ia.delegateCount--;
                                ba.remove && ba.remove.call(b, Pa)
                            }
                        }
                        if (Ia.length === 0 && aa !== Ia.length) {
                            if (!ba.teardown || ba.teardown.call(b, B) === false) d.removeEvent(b, I, w.handle);
                            delete qa[I]
                        }
                    } else for (I in qa) d.event.remove(b, I + e[A], m, o, true)
                }
                if (d.isEmptyObject(qa)) {
                    if (e = w.handle) e.elem = null;
                    d.removeData(b, ["events", "handle"], true)
                }
            }
        },
        customEvent: {
            getData: true,
            setData: true,
            changeData: true
        },
        trigger: function (b, e, m, o) {
            if (!(m && (m.nodeType === 3 || m.nodeType === 8))) {
                var r = b.type || b,
                    w = [],
                    A, B, I, U, aa;
                if (!J.test(r + d.event.triggered)) {
                    if (r.indexOf("!") >= 0) {
                        r = r.slice(0, -1);
                        A = true
                    }
                    if (r.indexOf(".") >= 0) {
                        w = r.split(".");
                        r = w.shift();
                        w.sort()
                    }
                    if (!((!m || d.event.customEvent[r]) && !d.event.global[r])) {
                        b = typeof b === "object" ? b[d.expando] ? b : new d.Event(r, b) : new d.Event(r);
                        b.type = r;
                        b.isTrigger = true;
                        b.exclusive = A;
                        b.namespace = w.join(".");
                        b.namespace_re = b.namespace ? RegExp("(^|\\.)" + w.join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
                        A = r.indexOf(":") < 0 ? "on" + r : "";
                        if (m) {
                            b.result = f;
                            if (!b.target) b.target = m;
                            e = e != null ? d.makeArray(e) : [];
                            e.unshift(b);
                            I = d.event.special[r] || {};
                            if (!(I.trigger && I.trigger.apply(m, e) === false)) {
                                aa = [
                                    [m, I.bindType || r]
                                ];
                                if (!o && !I.noBubble && !d.isWindow(m)) {
                                    U = I.delegateType || r;
                                    w = J.test(U + r) ? m : m.parentNode;
                                    for (B = null; w; w = w.parentNode) {
                                        aa.push([w, U]);
                                        B = w
                                    }
                                    if (B && B === m.ownerDocument) aa.push([B.defaultView || B.parentWindow || a, U])
                                }
                                for (B = 0; B < aa.length && !b.isPropagationStopped(); B++) {
                                    w = aa[B][0];
                                    b.type = aa[B][1];
                                    (U = (d._data(w, "events") || {})[b.type] && d._data(w, "handle")) && U.apply(w, e);
                                    (U = A && w[A]) && d.acceptData(w) && U.apply(w, e) === false && b.preventDefault()
                                }
                                b.type = r;
                                if (!o && !b.isDefaultPrevented()) if ((!I._default || I._default.apply(m.ownerDocument, e) === false) && !(r === "click" && d.nodeName(m, "a")) && d.acceptData(m)) if (A && m[r] && (r !== "focus" && r !== "blur" || b.target.offsetWidth !== 0) && !d.isWindow(m)) {
                                    if (B = m[A]) m[A] = null;
                                    d.event.triggered = r;
                                    m[r]();
                                    d.event.triggered = f;
                                    if (B) m[A] = B
                                }
                                return b.result
                            }
                        } else {
                            m = d.cache;
                            for (B in m) m[B].events && m[B].events[r] && d.event.trigger(b, e, m[B].handle.elem, true)
                        }
                    }
                }
            }
        },
        dispatch: function (b) {
            b = d.event.fix(b || a.event);
            var e = (d._data(this, "events") || {})[b.type] || [],
                m = e.delegateCount,
                o = [].slice.call(arguments, 0),
                r = !b.exclusive && !b.namespace,
                w = d.event.special[b.type] || {},
                A = [],
                B, I, U, aa, V, qa, ba;
            o[0] = b;
            b.delegateTarget = this;
            if (!(w.preDispatch && w.preDispatch.call(this, b) === false)) {
                if (m && !(b.button && b.type === "click")) {
                    U = d(this);
                    U.context = this.ownerDocument || this;
                    for (I = b.target; I != this; I = I.parentNode || this) if (I.disabled !== true) {
                        V = {};
                        qa = [];
                        U[0] = I;
                        for (B = 0; B < m; B++) {
                            aa = e[B];
                            ba = aa.selector;
                            if (V[ba] === f) {
                                var Ia = V,
                                    Pa = ba,
                                    Oa;
                                if (aa.quick) {
                                    Oa = aa.quick;
                                    var Qa = I.attributes || {};
                                    Oa = (!Oa[1] || I.nodeName.toLowerCase() === Oa[1]) && (!Oa[2] || (Qa.id || {}).value === Oa[2]) && (!Oa[3] || Oa[3].test((Qa["class"] || {}).value))
                                } else Oa = U.is(ba);
                                Ia[Pa] = Oa
                            }
                            V[ba] && qa.push(aa)
                        }
                        qa.length && A.push({
                            elem: I,
                            matches: qa
                        })
                    }
                }
                e.length > m && A.push({
                    elem: this,
                    matches: e.slice(m)
                });
                for (B = 0; B < A.length && !b.isPropagationStopped(); B++) {
                    m = A[B];
                    b.currentTarget = m.elem;
                    for (e = 0; e < m.matches.length && !b.isImmediatePropagationStopped(); e++) {
                        aa = m.matches[e];
                        if (r || !b.namespace && !aa.namespace || b.namespace_re && b.namespace_re.test(aa.namespace)) {
                            b.data = aa.data;
                            b.handleObj = aa;
                            aa = ((d.event.special[aa.origType] || {}).handle || aa.handler).apply(m.elem, o);
                            if (aa !== f) {
                                b.result = aa;
                                if (aa === false) {
                                    b.preventDefault();
                                    b.stopPropagation()
                                }
                            }
                        }
                    }
                }
                w.postDispatch && w.postDispatch.call(this, b);
                return b.result
            }
        },
        props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function (b, e) {
                if (b.which == null) b.which = e.charCode != null ? e.charCode : e.keyCode;
                return b
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (b, e) {
                var m, o, r = e.button,
                    w = e.fromElement;
                if (b.pageX == null && e.clientX != null) {
                    m = b.target.ownerDocument || x;
                    o = m.documentElement;
                    m = m.body;
                    b.pageX = e.clientX + (o && o.scrollLeft || m && m.scrollLeft || 0) - (o && o.clientLeft || m && m.clientLeft || 0);
                    b.pageY = e.clientY + (o && o.scrollTop || m && m.scrollTop || 0) - (o && o.clientTop || m && m.clientTop || 0)
                }
                if (!b.relatedTarget && w) b.relatedTarget = w === b.target ? e.toElement : w;
                if (!b.which && r !== f) b.which = r & 1 ? 1 : r & 2 ? 3 : r & 4 ? 2 : 0;
                return b
            }
        },
        fix: function (b) {
            if (b[d.expando]) return b;
            var e, m, o = b,
                r = d.event.fixHooks[b.type] || {},
                w = r.props ? this.props.concat(r.props) : this.props;
            b = d.Event(o);
            for (e = w.length; e;) {
                m = w[--e];
                b[m] = o[m]
            }
            if (!b.target) b.target = o.srcElement || x;
            if (b.target.nodeType === 3) b.target = b.target.parentNode;
            if (b.metaKey === f) b.metaKey = b.ctrlKey;
            return r.filter ? r.filter(b, o) : b
        },
        special: {
            ready: {
                setup: d.bindReady
            },
            load: {
                noBubble: true
            },
            focus: {
                delegateType: "focusin"
            },
            blur: {
                delegateType: "focusout"
            },
            beforeunload: {
                setup: function (b, e, m) {
                    if (d.isWindow(this)) this.onbeforeunload = m
                },
                teardown: function (b, e) {
                    if (this.onbeforeunload === e) this.onbeforeunload = null
                }
            }
        },
        simulate: function (b, e, m, o) {
            b = d.extend(new d.Event, m, {
                type: b,
                isSimulated: true,
                originalEvent: {}
            });
            o ? d.event.trigger(b, null, e) : d.event.dispatch.call(e, b);
            b.isDefaultPrevented() && m.preventDefault()
        }
    };
    d.event.handle = d.event.dispatch;
    d.removeEvent = x.removeEventListener ?
    function (b, e, m) {
        b.removeEventListener && b.removeEventListener(e, m, false)
    } : function (b, e, m) {
        b.detachEvent && b.detachEvent("on" + e, m)
    };
    d.Event = function (b, e) {
        if (!(this instanceof d.Event)) return new d.Event(b, e);
        if (b && b.type) {
            this.originalEvent = b;
            this.type = b.type;
            this.isDefaultPrevented = b.defaultPrevented || b.returnValue === false || b.getPreventDefault && b.getPreventDefault() ? c : l
        } else this.type = b;
        e && d.extend(this, e);
        this.timeStamp = b && b.timeStamp || d.now();
        this[d.expando] = true
    };
    d.Event.prototype = {
        preventDefault: function () {
            this.isDefaultPrevented = c;
            var b = this.originalEvent;
            if (b) if (b.preventDefault) b.preventDefault();
            else b.returnValue = false
        },
        stopPropagation: function () {
            this.isPropagationStopped = c;
            var b = this.originalEvent;
            if (b) {
                b.stopPropagation && b.stopPropagation();
                b.cancelBubble = true
            }
        },
        stopImmediatePropagation: function () {
            this.isImmediatePropagationStopped = c;
            this.stopPropagation()
        },
        isDefaultPrevented: l,
        isPropagationStopped: l,
        isImmediatePropagationStopped: l
    };
    d.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function (b, e) {
        d.event.special[b] = {
            delegateType: e,
            bindType: e,
            handle: function (m) {
                var o = m.relatedTarget,
                    r = m.handleObj,
                    w;
                if (!o || o !== this && !d.contains(this, o)) {
                    m.type = r.origType;
                    w = r.handler.apply(this, arguments);
                    m.type = e
                }
                return w
            }
        }
    });
    if (!d.support.submitBubbles) d.event.special.submit = {
        setup: function () {
            if (d.nodeName(this, "form")) return false;
            d.event.add(this, "click._submit keypress._submit", function (b) {
                b = b.target;
                if ((b = d.nodeName(b, "input") || d.nodeName(b, "button") ? b.form : f) && !b._submit_attached) {
                    d.event.add(b, "submit._submit", function (e) {
                        e._submit_bubble = true
                    });
                    b._submit_attached = true
                }
            })
        },
        postDispatch: function (b) {
            if (b._submit_bubble) {
                delete b._submit_bubble;
                this.parentNode && !b.isTrigger && d.event.simulate("submit", this.parentNode, b, true)
            }
        },
        teardown: function () {
            if (d.nodeName(this, "form")) return false;
            d.event.remove(this, "._submit")
        }
    };
    if (!d.support.changeBubbles) d.event.special.change = {
        setup: function () {
            if (Ba.test(this.nodeName)) {
                if (this.type === "checkbox" || this.type === "radio") {
                    d.event.add(this, "propertychange._change", function (b) {
                        if (b.originalEvent.propertyName === "checked") this._just_changed = true
                    });
                    d.event.add(this, "click._change", function (b) {
                        if (this._just_changed && !b.isTrigger) {
                            this._just_changed = false;
                            d.event.simulate("change", this, b, true)
                        }
                    })
                }
                return false
            }
            d.event.add(this, "beforeactivate._change", function (b) {
                b = b.target;
                if (Ba.test(b.nodeName) && !b._change_attached) {
                    d.event.add(b, "change._change", function (e) {
                        this.parentNode && !e.isSimulated && !e.isTrigger && d.event.simulate("change", this.parentNode, e, true)
                    });
                    b._change_attached = true
                }
            })
        },
        handle: function (b) {
            var e = b.target;
            if (this !== e || b.isSimulated || b.isTrigger || e.type !== "radio" && e.type !== "checkbox") return b.handleObj.handler.apply(this, arguments)
        },
        teardown: function () {
            d.event.remove(this, "._change");
            return Ba.test(this.nodeName)
        }
    };
    d.support.focusinBubbles || d.each({
        focus: "focusin",
        blur: "focusout"
    }, function (b, e) {
        var m = 0,
            o = function (r) {
                d.event.simulate(e, r.target, d.event.fix(r), true)
            };
        d.event.special[e] = {
            setup: function () {
                m++ === 0 && x.addEventListener(b, o, true)
            },
            teardown: function () {
                --m === 0 && x.removeEventListener(b, o, true)
            }
        }
    });
    d.fn.extend({
        on: function (b, e, m, o, r) {
            var w, A;
            if (typeof b === "object") {
                if (typeof e !== "string") {
                    m = m || e;
                    e = f
                }
                for (A in b) this.on(A, e, m, b[A], r);
                return this
            }
            if (m == null && o == null) {
                o = e;
                m = e = f
            } else if (o == null) if (typeof e === "string") {
                o = m;
                m = f
            } else {
                o = m;
                m = e;
                e = f
            }
            if (o === false) o = l;
            else if (!o) return this;
            if (r === 1) {
                w = o;
                o = function (B) {
                    d().off(B);
                    return w.apply(this, arguments)
                };
                o.guid = w.guid || (w.guid = d.guid++)
            }
            return this.each(function () {
                d.event.add(this, b, o, m, e)
            })
        },
        one: function (b, e, m, o) {
            return this.on(b, e, m, o, 1)
        },
        off: function (b, e, m) {
            if (b && b.preventDefault && b.handleObj) {
                var o = b.handleObj;
                d(b.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler);
                return this
            }
            if (typeof b === "object") {
                for (o in b) this.off(o, e, b[o]);
                return this
            }
            if (e === false || typeof e === "function") {
                m = e;
                e = f
            }
            if (m === false) m = l;
            return this.each(function () {
                d.event.remove(this, b, m, e)
            })
        },
        bind: function (b, e, m) {
            return this.on(b, null, e, m)
        },
        unbind: function (b, e) {
            return this.off(b, null, e)
        },
        live: function (b, e, m) {
            d(this.context).on(b, this.selector, e, m);
            return this
        },
        die: function (b, e) {
            d(this.context).off(b, this.selector || "**", e);
            return this
        },
        delegate: function (b, e, m, o) {
            return this.on(e, b, m, o)
        },
        undelegate: function (b, e, m) {
            return arguments.length == 1 ? this.off(b, "**") : this.off(e, b, m)
        },
        trigger: function (b, e) {
            return this.each(function () {
                d.event.trigger(b, e, this)
            })
        },
        triggerHandler: function (b, e) {
            if (this[0]) return d.event.trigger(b, e, this[0], true)
        },
        toggle: function (b) {
            var e = arguments,
                m = b.guid || d.guid++,
                o = 0,
                r = function (w) {
                    var A = (d._data(this, "lastToggle" + b.guid) || 0) % o;
                    d._data(this, "lastToggle" + b.guid, A + 1);
                    w.preventDefault();
                    return e[A].apply(this, arguments) || false
                };
            for (r.guid = m; o < e.length;) e[o++].guid = m;
            return this.click(r)
        },
        hover: function (b, e) {
            return this.mouseenter(b).mouseleave(e || b)
        }
    });
    d.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (b, e) {
        d.fn[e] = function (m, o) {
            if (o == null) {
                o = m;
                m = null
            }
            return arguments.length > 0 ? this.on(e, null, m, o) : this.trigger(e)
        };
        if (d.attrFn) d.attrFn[e] = true;
        if (ua.test(e)) d.event.fixHooks[e] = d.event.keyHooks;
        if (va.test(e)) d.event.fixHooks[e] = d.event.mouseHooks
    });
    (function () {
        function b(v, D, P, R, T, X) {
            T = 0;
            for (var ra = R.length; T < ra; T++) {
                var ia = R[T];
                if (ia) {
                    var La = false;
                    for (ia = ia[v]; ia;) {
                        if (ia[o] === P) {
                            La = R[ia.sizset];
                            break
                        }
                        if (ia.nodeType === 1 && !X) {
                            ia[o] = P;
                            ia.sizset = T
                        }
                        if (ia.nodeName.toLowerCase() === D) {
                            La = ia;
                            break
                        }
                        ia = ia[v]
                    }
                    R[T] = La
                }
            }
        }
        function e(v, D, P, R, T, X) {
            T = 0;
            for (var ra = R.length; T < ra; T++) {
                var ia = R[T];
                if (ia) {
                    var La = false;
                    for (ia = ia[v]; ia;) {
                        if (ia[o] === P) {
                            La = R[ia.sizset];
                            break
                        }
                        if (ia.nodeType === 1) {
                            if (!X) {
                                ia[o] = P;
                                ia.sizset = T
                            }
                            if (typeof D !== "string") {
                                if (ia === D) {
                                    La = true;
                                    break
                                }
                            } else if (V.filter(D, [ia]).length > 0) {
                                La = ia;
                                break
                            }
                        }
                        ia = ia[v]
                    }
                    R[T] = La
                }
            }
        }
        var m = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
            o = "sizcache" + (Math.random() + "").replace(".", ""),
            r = 0,
            w = Object.prototype.toString,
            A = false,
            B = true,
            I = /\\/g,
            U = /\r\n/g,
            aa = /\W/;
        [0, 0].sort(function () {
            B = false;
            return 0
        });
        var V = function (v, D, P, R) {
                P = P || [];
                var T = D = D || x;
                if (D.nodeType !== 1 && D.nodeType !== 9) return [];
                if (!v || typeof v !== "string") return P;
                var X, ra, ia, La, F, ca = true,
                    ha = V.isXML(D),
                    ea = [],
                    Fa = v;
                do {
                    m.exec("");
                    if (X = m.exec(Fa)) {
                        Fa = X[3];
                        ea.push(X[1]);
                        if (X[2]) {
                            La = X[3];
                            break
                        }
                    }
                } while (X);
                if (ea.length > 1 && Ia.exec(v)) if (ea.length === 2 && ba.relative[ea[0]]) ra = eb(ea[0] + ea[1], D, R);
                else for (ra = ba.relative[ea[0]] ? [D] : V(ea.shift(), D); ea.length;) {
                    v = ea.shift();
                    if (ba.relative[v]) v += ea.shift();
                    ra = eb(v, ra, R)
                } else {
                    if (!R && ea.length > 1 && D.nodeType === 9 && !ha && ba.match.ID.test(ea[0]) && !ba.match.ID.test(ea[ea.length - 1])) {
                        X = V.find(ea.shift(), D, ha);
                        D = X.expr ? V.filter(X.expr, X.set)[0] : X.set[0]
                    }
                    if (D) {
                        X = R ? {
                            expr: ea.pop(),
                            set: Qa(R)
                        } : V.find(ea.pop(), ea.length === 1 && (ea[0] === "~" || ea[0] === "+") && D.parentNode ? D.parentNode : D, ha);
                        ra = X.expr ? V.filter(X.expr, X.set) : X.set;
                        if (ea.length > 0) ia = Qa(ra);
                        else ca = false;
                        for (; ea.length;) {
                            X = F = ea.pop();
                            if (ba.relative[F]) X = ea.pop();
                            else F = "";
                            if (X == null) X = D;
                            ba.relative[F](ia, X, ha)
                        }
                    } else ia = []
                }
                ia || (ia = ra);
                ia || V.error(F || v);
                if (w.call(ia) === "[object Array]") if (ca) if (D && D.nodeType === 1) for (v = 0; ia[v] != null; v++) {
                    if (ia[v] && (ia[v] === true || ia[v].nodeType === 1 && V.contains(D, ia[v]))) P.push(ra[v])
                } else for (v = 0; ia[v] != null; v++) ia[v] && ia[v].nodeType === 1 && P.push(ra[v]);
                else P.push.apply(P, ia);
                else Qa(ia, P);
                if (La) {
                    V(La, T, P, R);
                    V.uniqueSort(P)
                }
                return P
            };
        V.uniqueSort = function (v) {
            if (db) {
                A = B;
                v.sort(db);
                if (A) for (var D = 1; D < v.length; D++) v[D] === v[D - 1] && v.splice(D--, 1)
            }
            return v
        };
        V.matches = function (v, D) {
            return V(v, null, null, D)
        };
        V.matchesSelector = function (v, D) {
            return V(D, null, null, [v]).length > 0
        };
        V.find = function (v, D, P) {
            var R, T, X, ra, ia, La;
            if (!v) return [];
            T = 0;
            for (X = ba.order.length; T < X; T++) {
                ia = ba.order[T];
                if (ra = ba.leftMatch[ia].exec(v)) {
                    La = ra[1];
                    ra.splice(1, 1);
                    if (La.substr(La.length - 1) !== "\\") {
                        ra[1] = (ra[1] || "").replace(I, "");
                        R = ba.find[ia](ra, D, P);
                        if (R != null) {
                            v = v.replace(ba.match[ia], "");
                            break
                        }
                    }
                }
            }
            R || (R = typeof D.getElementsByTagName !== "undefined" ? D.getElementsByTagName("*") : []);
            return {
                set: R,
                expr: v
            }
        };
        V.filter = function (v, D, P, R) {
            for (var T, X, ra, ia, La, F, ca, ha, ea = v, Fa = [], za = D, Ta = D && D[0] && V.isXML(D[0]); v && D.length;) {
                for (ra in ba.filter) if ((T = ba.leftMatch[ra].exec(v)) != null && T[2]) {
                    F = ba.filter[ra];
                    La = T[1];
                    X = false;
                    T.splice(1, 1);
                    if (La.substr(La.length - 1) !== "\\") {
                        if (za === Fa) Fa = [];
                        if (ba.preFilter[ra]) if (T = ba.preFilter[ra](T, za, P, Fa, R, Ta)) {
                            if (T === true) continue
                        } else X = ia = true;
                        if (T) for (ca = 0;
                        (La = za[ca]) != null; ca++) if (La) {
                            ia = F(La, T, ca, za);
                            ha = R ^ ia;
                            if (P && ia != null) if (ha) X = true;
                            else za[ca] = false;
                            else if (ha) {
                                Fa.push(La);
                                X = true
                            }
                        }
                        if (ia !== f) {
                            P || (za = Fa);
                            v = v.replace(ba.match[ra], "");
                            if (!X) return [];
                            break
                        }
                    }
                }
                if (v === ea) if (X == null) V.error(v);
                else break;
                ea = v
            }
            return za
        };
        V.error = function (v) {
            throw Error("Syntax error, unrecognized expression: " + v);
        };
        var qa = V.getText = function (v) {
                var D, P;
                D = v.nodeType;
                var R = "";
                if (D) if (D === 1 || D === 9 || D === 11) if (typeof v.textContent === "string") return v.textContent;
                else if (typeof v.innerText === "string") return v.innerText.replace(U, "");
                else for (v = v.firstChild; v; v = v.nextSibling) R += qa(v);
                else {
                    if (D === 3 || D === 4) return v.nodeValue
                } else for (D = 0; P = v[D]; D++) if (P.nodeType !== 8) R += qa(P);
                return R
            },
            ba = V.selectors = {
                order: ["ID", "NAME", "TAG"],
                match: {
                    ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
                    CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
                    NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
                    ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
                    TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
                    CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
                    POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
                    PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/
                },
                leftMatch: {},
                attrMap: {
                    "class": "className",
                    "for": "htmlFor"
                },
                attrHandle: {
                    href: function (v) {
                        return v.getAttribute("href")
                    },
                    type: function (v) {
                        return v.getAttribute("type")
                    }
                },
                relative: {
                    "+": function (v, D) {
                        var P = typeof D === "string",
                            R = P && !aa.test(D);
                        P = P && !R;
                        if (R) D = D.toLowerCase();
                        R = 0;
                        for (var T = v.length, X; R < T; R++) if (X = v[R]) {
                            for (;
                            (X = X.previousSibling) && X.nodeType !== 1;);
                            v[R] = P || X && X.nodeName.toLowerCase() === D ? X || false : X === D
                        }
                        P && V.filter(D, v, true)
                    },
                    ">": function (v, D) {
                        var P, R = typeof D === "string",
                            T = 0,
                            X = v.length;
                        if (R && !aa.test(D)) for (D = D.toLowerCase(); T < X; T++) {
                            if (P = v[T]) {
                                P = P.parentNode;
                                v[T] = P.nodeName.toLowerCase() === D ? P : false
                            }
                        } else {
                            for (; T < X; T++) if (P = v[T]) v[T] = R ? P.parentNode : P.parentNode === D;
                            R && V.filter(D, v, true)
                        }
                    },
                    "": function (v, D, P) {
                        var R, T = r++,
                            X = e;
                        if (typeof D === "string" && !aa.test(D)) {
                            R = D = D.toLowerCase();
                            X = b
                        }
                        X("parentNode", D, T, v, R, P)
                    },
                    "~": function (v, D, P) {
                        var R, T = r++,
                            X = e;
                        if (typeof D === "string" && !aa.test(D)) {
                            R = D = D.toLowerCase();
                            X = b
                        }
                        X("previousSibling", D, T, v, R, P)
                    }
                },
                find: {
                    ID: function (v, D, P) {
                        if (typeof D.getElementById !== "undefined" && !P) return (v = D.getElementById(v[1])) && v.parentNode ? [v] : []
                    },
                    NAME: function (v, D) {
                        if (typeof D.getElementsByName !== "undefined") {
                            for (var P = [], R = D.getElementsByName(v[1]), T = 0, X = R.length; T < X; T++) R[T].getAttribute("name") === v[1] && P.push(R[T]);
                            return P.length === 0 ? null : P
                        }
                    },
                    TAG: function (v, D) {
                        if (typeof D.getElementsByTagName !== "undefined") return D.getElementsByTagName(v[1])
                    }
                },
                preFilter: {
                    CLASS: function (v, D, P, R, T, X) {
                        v = " " + v[1].replace(I, "") + " ";
                        if (X) return v;
                        X = 0;
                        for (var ra;
                        (ra = D[X]) != null; X++) if (ra) if (T ^ (ra.className && (" " + ra.className + " ").replace(/[\t\n\r]/g, " ").indexOf(v) >= 0)) P || R.push(ra);
                        else if (P) D[X] = false;
                        return false
                    },
                    ID: function (v) {
                        return v[1].replace(I, "")
                    },
                    TAG: function (v) {
                        return v[1].replace(I, "").toLowerCase()
                    },
                    CHILD: function (v) {
                        if (v[1] === "nth") {
                            v[2] || V.error(v[0]);
                            v[2] = v[2].replace(/^\+|\s*/g, "");
                            var D = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(v[2] === "even" && "2n" || v[2] === "odd" && "2n+1" || !/\D/.test(v[2]) && "0n+" + v[2] || v[2]);
                            v[2] = D[1] + (D[2] || 1) - 0;
                            v[3] = D[3] - 0
                        } else v[2] && V.error(v[0]);
                        v[0] = r++;
                        return v
                    },
                    ATTR: function (v, D, P, R, T, X) {
                        D = v[1] = v[1].replace(I, "");
                        if (!X && ba.attrMap[D]) v[1] = ba.attrMap[D];
                        v[4] = (v[4] || v[5] || "").replace(I, "");
                        if (v[2] === "~=") v[4] = " " + v[4] + " ";
                        return v
                    },
                    PSEUDO: function (v, D, P, R, T) {
                        if (v[1] === "not") if ((m.exec(v[3]) || "").length > 1 || /^\w/.test(v[3])) v[3] = V(v[3], null, null, D);
                        else {
                            v = V.filter(v[3], D, P, 1 ^ T);
                            P || R.push.apply(R, v);
                            return false
                        } else if (ba.match.POS.test(v[0]) || ba.match.CHILD.test(v[0])) return true;
                        return v
                    },
                    POS: function (v) {
                        v.unshift(true);
                        return v
                    }
                },
                filters: {
                    enabled: function (v) {
                        return v.disabled === false && v.type !== "hidden"
                    },
                    disabled: function (v) {
                        return v.disabled === true
                    },
                    checked: function (v) {
                        return v.checked === true
                    },
                    selected: function (v) {
                        return v.selected === true
                    },
                    parent: function (v) {
                        return !!v.firstChild
                    },
                    empty: function (v) {
                        return !v.firstChild
                    },
                    has: function (v, D, P) {
                        return !!V(P[3], v).length
                    },
                    header: function (v) {
                        return /h\d/i.test(v.nodeName)
                    },
                    text: function (v) {
                        var D = v.getAttribute("type"),
                            P = v.type;
                        return v.nodeName.toLowerCase() === "input" && "text" === P && (D === P || D === null)
                    },
                    radio: function (v) {
                        return v.nodeName.toLowerCase() === "input" && "radio" === v.type
                    },
                    checkbox: function (v) {
                        return v.nodeName.toLowerCase() === "input" && "checkbox" === v.type
                    },
                    file: function (v) {
                        return v.nodeName.toLowerCase() === "input" && "file" === v.type
                    },
                    password: function (v) {
                        return v.nodeName.toLowerCase() === "input" && "password" === v.type
                    },
                    submit: function (v) {
                        var D = v.nodeName.toLowerCase();
                        return (D === "input" || D === "button") && "submit" === v.type
                    },
                    image: function (v) {
                        return v.nodeName.toLowerCase() === "input" && "image" === v.type
                    },
                    reset: function (v) {
                        var D = v.nodeName.toLowerCase();
                        return (D === "input" || D === "button") && "reset" === v.type
                    },
                    button: function (v) {
                        var D = v.nodeName.toLowerCase();
                        return D === "input" && "button" === v.type || D === "button"
                    },
                    input: function (v) {
                        return /input|select|textarea|button/i.test(v.nodeName)
                    },
                    focus: function (v) {
                        return v === v.ownerDocument.activeElement
                    }
                },
                setFilters: {
                    first: function (v, D) {
                        return D === 0
                    },
                    last: function (v, D, P, R) {
                        return D === R.length - 1
                    },
                    even: function (v, D) {
                        return D % 2 === 0
                    },
                    odd: function (v, D) {
                        return D % 2 === 1
                    },
                    lt: function (v, D, P) {
                        return D < P[3] - 0
                    },
                    gt: function (v, D, P) {
                        return D > P[3] - 0
                    },
                    nth: function (v, D, P) {
                        return P[3] - 0 === D
                    },
                    eq: function (v, D, P) {
                        return P[3] - 0 === D
                    }
                },
                filter: {
                    PSEUDO: function (v, D, P, R) {
                        var T = D[1],
                            X = ba.filters[T];
                        if (X) return X(v, P, D, R);
                        else if (T === "contains") return (v.textContent || v.innerText || qa([v]) || "").indexOf(D[3]) >= 0;
                        else if (T === "not") {
                            D = D[3];
                            P = 0;
                            for (R = D.length; P < R; P++) if (D[P] === v) return false;
                            return true
                        } else V.error(T)
                    },
                    CHILD: function (v, D) {
                        var P, R, T, X, ra, ia;
                        P = D[1];
                        ia = v;
                        switch (P) {
                        case "only":
                        case "first":
                            for (; ia = ia.previousSibling;) if (ia.nodeType === 1) return false;
                            if (P === "first") return true;
                            ia = v;
                        case "last":
                            for (; ia = ia.nextSibling;) if (ia.nodeType === 1) return false;
                            return true;
                        case "nth":
                            P = D[2];
                            R = D[3];
                            if (P === 1 && R === 0) return true;
                            T = D[0];
                            if ((X = v.parentNode) && (X[o] !== T || !v.nodeIndex)) {
                                ra = 0;
                                for (ia = X.firstChild; ia; ia = ia.nextSibling) if (ia.nodeType === 1) ia.nodeIndex = ++ra;
                                X[o] = T
                            }
                            ia = v.nodeIndex - R;
                            return P === 0 ? ia === 0 : ia % P === 0 && ia / P >= 0
                        }
                    },
                    ID: function (v, D) {
                        return v.nodeType === 1 && v.getAttribute("id") === D
                    },
                    TAG: function (v, D) {
                        return D === "*" && v.nodeType === 1 || !! v.nodeName && v.nodeName.toLowerCase() === D
                    },
                    CLASS: function (v, D) {
                        return (" " + (v.className || v.getAttribute("class")) + " ").indexOf(D) > -1
                    },
                    ATTR: function (v, D) {
                        var P = D[1];
                        P = V.attr ? V.attr(v, P) : ba.attrHandle[P] ? ba.attrHandle[P](v) : v[P] != null ? v[P] : v.getAttribute(P);
                        var R = P + "",
                            T = D[2],
                            X = D[4];
                        return P == null ? T === "!=" : !T && V.attr ? P != null : T === "=" ? R === X : T === "*=" ? R.indexOf(X) >= 0 : T === "~=" ? (" " + R + " ").indexOf(X) >= 0 : !X ? R && P !== false : T === "!=" ? R !== X : T === "^=" ? R.indexOf(X) === 0 : T === "$=" ? R.substr(R.length - X.length) === X : T === "|=" ? R === X || R.substr(0, X.length + 1) === X + "-" : false
                    },
                    POS: function (v, D, P, R) {
                        var T = ba.setFilters[D[2]];
                        if (T) return T(v, P, D, R)
                    }
                }
            },
            Ia = ba.match.POS,
            Pa = function (v, D) {
                return "\\" + (D - 0 + 1)
            },
            Oa;
        for (Oa in ba.match) {
            ba.match[Oa] = RegExp(ba.match[Oa].source + /(?![^\[]*\])(?![^\(]*\))/.source);
            ba.leftMatch[Oa] = RegExp(/(^(?:.|\r|\n)*?)/.source + ba.match[Oa].source.replace(/\\(\d+)/g, Pa))
        }
        ba.match.globalPOS = Ia;
        var Qa = function (v, D) {
                v = Array.prototype.slice.call(v, 0);
                if (D) {
                    D.push.apply(D, v);
                    return D
                }
                return v
            };
        try {
            Array.prototype.slice.call(x.documentElement.childNodes, 0)
        } catch (mb) {
            Qa = function (v, D) {
                var P = 0,
                    R = D || [];
                if (w.call(v) === "[object Array]") Array.prototype.push.apply(R, v);
                else if (typeof v.length === "number") for (var T = v.length; P < T; P++) R.push(v[P]);
                else for (; v[P]; P++) R.push(v[P]);
                return R
            }
        }
        var db, Ka;
        if (x.documentElement.compareDocumentPosition) db = function (v, D) {
            if (v === D) {
                A = true;
                return 0
            }
            if (!v.compareDocumentPosition || !D.compareDocumentPosition) return v.compareDocumentPosition ? -1 : 1;
            return v.compareDocumentPosition(D) & 4 ? -1 : 1
        };
        else {
            db = function (v, D) {
                if (v === D) {
                    A = true;
                    return 0
                } else if (v.sourceIndex && D.sourceIndex) return v.sourceIndex - D.sourceIndex;
                var P, R, T = [],
                    X = [];
                P = v.parentNode;
                R = D.parentNode;
                var ra = P;
                if (P === R) return Ka(v, D);
                else if (P) {
                    if (!R) return 1
                } else return -1;
                for (; ra;) {
                    T.unshift(ra);
                    ra = ra.parentNode
                }
                for (ra = R; ra;) {
                    X.unshift(ra);
                    ra = ra.parentNode
                }
                P = T.length;
                R = X.length;
                for (ra = 0; ra < P && ra < R; ra++) if (T[ra] !== X[ra]) return Ka(T[ra], X[ra]);
                return ra === P ? Ka(v, X[ra], -1) : Ka(T[ra], D, 1)
            };
            Ka = function (v, D, P) {
                if (v === D) return P;
                for (v = v.nextSibling; v;) {
                    if (v === D) return -1;
                    v = v.nextSibling
                }
                return 1
            }
        }(function () {
            var v = x.createElement("div"),
                D = "script" + (new Date).getTime(),
                P = x.documentElement;
            v.innerHTML = "<a name='" + D + "'/>";
            P.insertBefore(v, P.firstChild);
            if (x.getElementById(D)) {
                ba.find.ID = function (R, T, X) {
                    if (typeof T.getElementById !== "undefined" && !X) return (T = T.getElementById(R[1])) ? T.id === R[1] || typeof T.getAttributeNode !== "undefined" && T.getAttributeNode("id").nodeValue === R[1] ? [T] : f : []
                };
                ba.filter.ID = function (R, T) {
                    var X = typeof R.getAttributeNode !== "undefined" && R.getAttributeNode("id");
                    return R.nodeType === 1 && X && X.nodeValue === T
                }
            }
            P.removeChild(v);
            P = v = null
        })();
        (function () {
            var v = x.createElement("div");
            v.appendChild(x.createComment(""));
            if (v.getElementsByTagName("*").length > 0) ba.find.TAG = function (D, P) {
                var R = P.getElementsByTagName(D[1]);
                if (D[1] === "*") {
                    for (var T = [], X = 0; R[X]; X++) R[X].nodeType === 1 && T.push(R[X]);
                    R = T
                }
                return R
            };
            v.innerHTML = "<a href='#'></a>";
            if (v.firstChild && typeof v.firstChild.getAttribute !== "undefined" && v.firstChild.getAttribute("href") !== "#") ba.attrHandle.href = function (D) {
                return D.getAttribute("href", 2)
            };
            v = null
        })();
        x.querySelectorAll &&
        function () {
            var v = V,
                D = x.createElement("div");
            D.innerHTML = "<p class='TEST'></p>";
            if (!(D.querySelectorAll && D.querySelectorAll(".TEST").length === 0)) {
                V = function (R, T, X, ra) {
                    T = T || x;
                    if (!ra && !V.isXML(T)) {
                        var ia = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(R);
                        if (ia && (T.nodeType === 1 || T.nodeType === 9)) if (ia[1]) return Qa(T.getElementsByTagName(R), X);
                        else if (ia[2] && ba.find.CLASS && T.getElementsByClassName) return Qa(T.getElementsByClassName(ia[2]), X);
                        if (T.nodeType === 9) {
                            if (R === "body" && T.body) return Qa([T.body], X);
                            else if (ia && ia[3]) {
                                var La = T.getElementById(ia[3]);
                                if (La && La.parentNode) {
                                    if (La.id === ia[3]) return Qa([La], X)
                                } else return Qa([], X)
                            }
                            try {
                                return Qa(T.querySelectorAll(R), X)
                            } catch (F) {}
                        } else if (T.nodeType === 1 && T.nodeName.toLowerCase() !== "object") {
                            ia = T;
                            var ca = (La = T.getAttribute("id")) || "__sizzle__",
                                ha = T.parentNode,
                                ea = /^\s*[+~]/.test(R);
                            if (La) ca = ca.replace(/'/g, "\\$&");
                            else T.setAttribute("id", ca);
                            if (ea && ha) T = T.parentNode;
                            try {
                                if (!ea || ha) return Qa(T.querySelectorAll("[id='" + ca + "'] " + R), X)
                            } catch (Fa) {} finally {
                                La || ia.removeAttribute("id")
                            }
                        }
                    }
                    return v(R, T, X, ra)
                };
                for (var P in v) V[P] = v[P];
                D = null
            }
        }();
        (function () {
            var v = x.documentElement,
                D = v.matchesSelector || v.mozMatchesSelector || v.webkitMatchesSelector || v.msMatchesSelector;
            if (D) {
                var P = !D.call(x.createElement("div"), "div"),
                    R = false;
                try {
                    D.call(x.documentElement, "[test!='']:sizzle")
                } catch (T) {
                    R = true
                }
                V.matchesSelector = function (X, ra) {
                    ra = ra.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");
                    if (!V.isXML(X)) try {
                        if (R || !ba.match.PSEUDO.test(ra) && !/!=/.test(ra)) {
                            var ia = D.call(X, ra);
                            if (ia || !P || X.document && X.document.nodeType !== 11) return ia
                        }
                    } catch (La) {}
                    return V(ra, null, null, [X]).length > 0
                }
            }
        })();
        (function () {
            var v = x.createElement("div");
            v.innerHTML = "<div class='test e'></div><div class='test'></div>";
            if (!(!v.getElementsByClassName || v.getElementsByClassName("e").length === 0)) {
                v.lastChild.className = "e";
                if (v.getElementsByClassName("e").length !== 1) {
                    ba.order.splice(1, 0, "CLASS");
                    ba.find.CLASS = function (D, P, R) {
                        if (typeof P.getElementsByClassName !== "undefined" && !R) return P.getElementsByClassName(D[1])
                    };
                    v = null
                }
            }
        })();
        V.contains = x.documentElement.contains ?
        function (v, D) {
            return v !== D && (v.contains ? v.contains(D) : true)
        } : x.documentElement.compareDocumentPosition ?
        function (v, D) {
            return !!(v.compareDocumentPosition(D) & 16)
        } : function () {
            return false
        };
        V.isXML = function (v) {
            return (v = (v ? v.ownerDocument || v : 0).documentElement) ? v.nodeName !== "HTML" : false
        };
        var eb = function (v, D, P) {
                var R, T = [],
                    X = "";
                for (D = D.nodeType ? [D] : D; R = ba.match.PSEUDO.exec(v);) {
                    X += R[0];
                    v = v.replace(ba.match.PSEUDO, "")
                }
                v = ba.relative[v] ? v + "*" : v;
                R = 0;
                for (var ra = D.length; R < ra; R++) V(v, D[R], T, P);
                return V.filter(X, T)
            };
        V.attr = d.attr;
        V.selectors.attrMap = {};
        d.find = V;
        d.expr = V.selectors;
        d.expr[":"] = d.expr.filters;
        d.unique = V.uniqueSort;
        d.text = V.getText;
        d.isXMLDoc = V.isXML;
        d.contains = V.contains
    })();
    var Ea = /Until$/,
        xa = /^(?:parents|prevUntil|prevAll)/,
        Va = /,/,
        Xa = /^.[^:#\[\.,]*$/,
        wa = Array.prototype.slice,
        Aa = d.expr.match.globalPOS,
        Ja = {
            children: true,
            contents: true,
            next: true,
            prev: true
        };
    d.fn.extend({
        find: function (b) {
            var e = this,
                m, o;
            if (typeof b !== "string") return d(b).filter(function () {
                m = 0;
                for (o = e.length; m < o; m++) if (d.contains(e[m], this)) return true
            });
            var r = this.pushStack("", "find", b),
                w, A, B;
            m = 0;
            for (o = this.length; m < o; m++) {
                w = r.length;
                d.find(b, this[m], r);
                if (m > 0) for (A = w; A < r.length; A++) for (B = 0; B < w; B++) if (r[B] === r[A]) {
                    r.splice(A--, 1);
                    break
                }
            }
            return r
        },
        has: function (b) {
            var e = d(b);
            return this.filter(function () {
                for (var m = 0, o = e.length; m < o; m++) if (d.contains(this, e[m])) return true
            })
        },
        not: function (b) {
            return this.pushStack(k(this, b, false), "not", b)
        },
        filter: function (b) {
            return this.pushStack(k(this, b, true), "filter", b)
        },
        is: function (b) {
            return !!b && (typeof b === "string" ? Aa.test(b) ? d(b, this.context).index(this[0]) >= 0 : d.filter(b, this).length > 0 : this.filter(b).length > 0)
        },
        closest: function (b, e) {
            var m = [],
                o, r, w = this[0];
            if (d.isArray(b)) {
                for (r = 1; w && w.ownerDocument && w !== e;) {
                    for (o = 0; o < b.length; o++) d(w).is(b[o]) && m.push({
                        selector: b[o],
                        elem: w,
                        level: r
                    });
                    w = w.parentNode;
                    r++
                }
                return m
            }
            var A = Aa.test(b) || typeof b !== "string" ? d(b, e || this.context) : 0;
            o = 0;
            for (r = this.length; o < r; o++) for (w = this[o]; w;) if (A ? A.index(w) > -1 : d.find.matchesSelector(w, b)) {
                m.push(w);
                break
            } else {
                w = w.parentNode;
                if (!w || !w.ownerDocument || w === e || w.nodeType === 11) break
            }
            m = m.length > 1 ? d.unique(m) : m;
            return this.pushStack(m, "closest", b)
        },
        index: function (b) {
            if (!b) return this[0] && this[0].parentNode ? this.prevAll().length : -1;
            if (typeof b === "string") return d.inArray(this[0], d(b));
            return d.inArray(b.jquery ? b[0] : b, this)
        },
        add: function (b, e) {
            var m = typeof b === "string" ? d(b, e) : d.makeArray(b && b.nodeType ? [b] : b),
                o = d.merge(this.get(), m);
            return this.pushStack(!m[0] || !m[0].parentNode || m[0].parentNode.nodeType === 11 || !o[0] || !o[0].parentNode || o[0].parentNode.nodeType === 11 ? o : d.unique(o))
        },
        andSelf: function () {
            return this.add(this.prevObject)
        }
    });
    d.each({
        parent: function (b) {
            return (b = b.parentNode) && b.nodeType !== 11 ? b : null
        },
        parents: function (b) {
            return d.dir(b, "parentNode")
        },
        parentsUntil: function (b, e, m) {
            return d.dir(b, "parentNode", m)
        },
        next: function (b) {
            return d.nth(b, 2, "nextSibling")
        },
        prev: function (b) {
            return d.nth(b, 2, "previousSibling")
        },
        nextAll: function (b) {
            return d.dir(b, "nextSibling")
        },
        prevAll: function (b) {
            return d.dir(b, "previousSibling")
        },
        nextUntil: function (b, e, m) {
            return d.dir(b, "nextSibling", m)
        },
        prevUntil: function (b, e, m) {
            return d.dir(b, "previousSibling", m)
        },
        siblings: function (b) {
            return d.sibling((b.parentNode || {}).firstChild, b)
        },
        children: function (b) {
            return d.sibling(b.firstChild)
        },
        contents: function (b) {
            return d.nodeName(b, "iframe") ? b.contentDocument || b.contentWindow.document : d.makeArray(b.childNodes)
        }
    }, function (b, e) {
        d.fn[b] = function (m, o) {
            var r = d.map(this, e, m);
            Ea.test(b) || (o = m);
            if (o && typeof o === "string") r = d.filter(o, r);
            r = this.length > 1 && !Ja[b] ? d.unique(r) : r;
            if ((this.length > 1 || Va.test(o)) && xa.test(b)) r = r.reverse();
            return this.pushStack(r, b, wa.call(arguments).join(","))
        }
    });
    d.extend({
        filter: function (b, e, m) {
            if (m) b = ":not(" + b + ")";
            return e.length === 1 ? d.find.matchesSelector(e[0], b) ? [e[0]] : [] : d.find.matches(b, e)
        },
        dir: function (b, e, m) {
            var o = [];
            for (b = b[e]; b && b.nodeType !== 9 && (m === f || b.nodeType !== 1 || !d(b).is(m));) {
                b.nodeType === 1 && o.push(b);
                b = b[e]
            }
            return o
        },
        nth: function (b, e, m) {
            e = e || 1;
            for (var o = 0; b; b = b[m]) if (b.nodeType === 1 && ++o === e) break;
            return b
        },
        sibling: function (b, e) {
            for (var m = []; b; b = b.nextSibling) b.nodeType === 1 && b !== e && m.push(b);
            return m
        }
    });
    var Ra = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        fb = / jQuery\d+="(?:\d+|null)"/g,
        rb = /^\s+/,
        Ua = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,
        Ca = /<([\w:]+)/,
        cb = /<tbody/i,
        sb = /<|&#?\w+;/,
        Wa = /<(?:script|style)/i,
        kb = /<(?:script|object|embed|option|style)/i,
        Za = RegExp("<(?:" + Ra + ")[\\s/>]", "i"),
        nb = /checked\s*(?:[^=]|=\s*.checked.)/i,
        bb = /\/(java|ecma)script/i,
        tb = /^\s*<!(?:\[CDATA\[|\-\-)/,
        Sa = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            area: [1, "<map>", "</map>"],
            _default: [0, "", ""]
        },
        lb = q(x);
    Sa.optgroup = Sa.option;
    Sa.tbody = Sa.tfoot = Sa.colgroup = Sa.caption = Sa.thead;
    Sa.th = Sa.td;
    if (!d.support.htmlSerialize) Sa._default = [1, "div<div>", "</div>"];
    d.fn.extend({
        text: function (b) {
            return d.access(this, function (e) {
                return e === f ? d.text(this) : this.empty().append((this[0] && this[0].ownerDocument || x).createTextNode(e))
            }, null, b, arguments.length)
        },
        wrapAll: function (b) {
            if (d.isFunction(b)) return this.each(function (m) {
                d(this).wrapAll(b.call(this, m))
            });
            if (this[0]) {
                var e = d(b, this[0].ownerDocument).eq(0).clone(true);
                this[0].parentNode && e.insertBefore(this[0]);
                e.map(function () {
                    for (var m = this; m.firstChild && m.firstChild.nodeType === 1;) m = m.firstChild;
                    return m
                }).append(this)
            }
            return this
        },
        wrapInner: function (b) {
            if (d.isFunction(b)) return this.each(function (e) {
                d(this).wrapInner(b.call(this, e))
            });
            return this.each(function () {
                var e = d(this),
                    m = e.contents();
                m.length ? m.wrapAll(b) : e.append(b)
            })
        },
        wrap: function (b) {
            var e = d.isFunction(b);
            return this.each(function (m) {
                d(this).wrapAll(e ? b.call(this, m) : b)
            })
        },
        unwrap: function () {
            return this.parent().each(function () {
                d.nodeName(this, "body") || d(this).replaceWith(this.childNodes)
            }).end()
        },
        append: function () {
            return this.domManip(arguments, true, function (b) {
                this.nodeType === 1 && this.appendChild(b)
            })
        },
        prepend: function () {
            return this.domManip(arguments, true, function (b) {
                this.nodeType === 1 && this.insertBefore(b, this.firstChild)
            })
        },
        before: function () {
            if (this[0] && this[0].parentNode) return this.domManip(arguments, false, function (e) {
                this.parentNode.insertBefore(e, this)
            });
            else if (arguments.length) {
                var b = d.clean(arguments);
                b.push.apply(b, this.toArray());
                return this.pushStack(b, "before", arguments)
            }
        },
        after: function () {
            if (this[0] && this[0].parentNode) return this.domManip(arguments, false, function (e) {
                this.parentNode.insertBefore(e, this.nextSibling)
            });
            else if (arguments.length) {
                var b = this.pushStack(this, "after", arguments);
                b.push.apply(b, d.clean(arguments));
                return b
            }
        },
        remove: function (b, e) {
            for (var m = 0, o;
            (o = this[m]) != null; m++) if (!b || d.filter(b, [o]).length) {
                if (!e && o.nodeType === 1) {
                    d.cleanData(o.getElementsByTagName("*"));
                    d.cleanData([o])
                }
                o.parentNode && o.parentNode.removeChild(o)
            }
            return this
        },
        empty: function () {
            for (var b = 0, e;
            (e = this[b]) != null; b++) for (e.nodeType === 1 && d.cleanData(e.getElementsByTagName("*")); e.firstChild;) e.removeChild(e.firstChild);
            return this
        },
        clone: function (b, e) {
            b = b == null ? false : b;
            e = e == null ? b : e;
            return this.map(function () {
                return d.clone(this, b, e)
            })
        },
        html: function (b) {
            return d.access(this, function (e) {
                var m = this[0] || {},
                    o = 0,
                    r = this.length;
                if (e === f) return m.nodeType === 1 ? m.innerHTML.replace(fb, "") : null;
                if (typeof e === "string" && !Wa.test(e) && (d.support.leadingWhitespace || !rb.test(e)) && !Sa[(Ca.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = e.replace(Ua, "<$1></$2>");
                    try {
                        for (; o < r; o++) {
                            m = this[o] || {};
                            if (m.nodeType === 1) {
                                d.cleanData(m.getElementsByTagName("*"));
                                m.innerHTML = e
                            }
                        }
                        m = 0
                    } catch (w) {}
                }
                m && this.empty().append(e)
            }, null, b, arguments.length)
        },
        replaceWith: function (b) {
            if (this[0] && this[0].parentNode) {
                if (d.isFunction(b)) return this.each(function (e) {
                    var m = d(this),
                        o = m.html();
                    m.replaceWith(b.call(this, e, o))
                });
                if (typeof b !== "string") b = d(b).detach();
                return this.each(function () {
                    var e = this.nextSibling,
                        m = this.parentNode;
                    d(this).remove();
                    e ? d(e).before(b) : d(m).append(b)
                })
            } else return this.length ? this.pushStack(d(d.isFunction(b) ? b() : b), "replaceWith", b) : this
        },
        detach: function (b) {
            return this.remove(b, true)
        },
        domManip: function (b, e, m) {
            var o, r, w, A = b[0],
                B = [];
            if (!d.support.checkClone && arguments.length === 3 && typeof A === "string" && nb.test(A)) return this.each(function () {
                d(this).domManip(b, e, m, true)
            });
            if (d.isFunction(A)) return this.each(function (aa) {
                var V = d(this);
                b[0] = A.call(this, aa, e ? V.html() : f);
                V.domManip(b, e, m)
            });
            if (this[0]) {
                o = A && A.parentNode;
                o = d.support.parentNode && o && o.nodeType === 11 && o.childNodes.length === this.length ? {
                    fragment: o
                } : d.buildFragment(b, this, B);
                w = o.fragment;
                if (r = w.childNodes.length === 1 ? w = w.firstChild : w.firstChild) {
                    e = e && d.nodeName(r, "tr");
                    r = 0;
                    for (var I = this.length, U = I - 1; r < I; r++) m.call(e ? d.nodeName(this[r], "table") ? this[r].getElementsByTagName("tbody")[0] || this[r].appendChild(this[r].ownerDocument.createElement("tbody")) : this[r] : this[r], o.cacheable || I > 1 && r < U ? d.clone(w, true, true) : w)
                }
                B.length && d.each(B, function (aa, V) {
                    V.src ? d.ajax({
                        type: "GET",
                        global: false,
                        url: V.src,
                        async: false,
                        dataType: "script"
                    }) : d.globalEval((V.text || V.textContent || V.innerHTML || "").replace(tb, "/*$0*/"));
                    V.parentNode && V.parentNode.removeChild(V)
                })
            }
            return this
        }
    });
    d.buildFragment = function (b, e, m) {
        var o, r, w, A, B = b[0];
        if (e && e[0]) A = e[0].ownerDocument || e[0];
        A.createDocumentFragment || (A = x);
        if (b.length === 1 && typeof B === "string" && B.length < 512 && A === x && B.charAt(0) === "<" && !kb.test(B) && (d.support.checkClone || !nb.test(B)) && (d.support.html5Clone || !Za.test(B))) {
            r = true;
            if ((w = d.fragments[B]) && w !== 1) o = w
        }
        if (!o) {
            o = A.createDocumentFragment();
            d.clean(b, A, o, m)
        }
        if (r) d.fragments[B] = w ? o : 1;
        return {
            fragment: o,
            cacheable: r
        }
    };
    d.fragments = {};
    d.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (b, e) {
        d.fn[b] = function (m) {
            var o = [];
            m = d(m);
            var r = this.length === 1 && this[0].parentNode;
            if (r && r.nodeType === 11 && r.childNodes.length === 1 && m.length === 1) {
                m[e](this[0]);
                return this
            } else {
                r = 0;
                for (var w = m.length; r < w; r++) {
                    var A = (r > 0 ? this.clone(true) : this).get();
                    d(m[r])[e](A);
                    o = o.concat(A)
                }
                return this.pushStack(o, b, m.selector)
            }
        }
    });
    d.extend({
        clone: function (b, e, m) {
            var o, r, w;
            if (d.support.html5Clone || d.isXMLDoc(b) || !Za.test("<" + b.nodeName + ">")) o = b.cloneNode(true);
            else {
                o = x.createElement("div");
                lb.appendChild(o);
                o.innerHTML = b.outerHTML;
                o = o.firstChild
            }
            var A = o;
            if ((!d.support.noCloneEvent || !d.support.noCloneChecked) && (b.nodeType === 1 || b.nodeType === 11) && !d.isXMLDoc(b)) {
                p(b, A);
                o = n(b);
                r = n(A);
                for (w = 0; o[w]; ++w) r[w] && p(o[w], r[w])
            }
            if (e) {
                s(b, A);
                if (m) {
                    o = n(b);
                    r = n(A);
                    for (w = 0; o[w]; ++w) s(o[w], r[w])
                }
            }
            return A
        },
        clean: function (b, e, m, o) {
            var r, w = [];
            e = e || x;
            if (typeof e.createElement === "undefined") e = e.ownerDocument || e[0] && e[0].ownerDocument || x;
            for (var A = 0, B;
            (B = b[A]) != null; A++) {
                if (typeof B === "number") B += "";
                if (B) {
                    if (typeof B === "string") if (sb.test(B)) {
                        B = B.replace(Ua, "<$1></$2>");
                        r = (Ca.exec(B) || ["", ""])[1].toLowerCase();
                        var I = Sa[r] || Sa._default,
                            U = I[0],
                            aa = e.createElement("div"),
                            V = lb.childNodes;
                        e === x ? lb.appendChild(aa) : q(e).appendChild(aa);
                        for (aa.innerHTML = I[1] + B + I[2]; U--;) aa = aa.lastChild;
                        if (!d.support.tbody) {
                            U = cb.test(B);
                            I = r === "table" && !U ? aa.firstChild && aa.firstChild.childNodes : I[1] === "<table>" && !U ? aa.childNodes : [];
                            for (r = I.length - 1; r >= 0; --r) d.nodeName(I[r], "tbody") && !I[r].childNodes.length && I[r].parentNode.removeChild(I[r])
                        }!d.support.leadingWhitespace && rb.test(B) && aa.insertBefore(e.createTextNode(rb.exec(B)[0]), aa.firstChild);
                        B = aa.childNodes;
                        if (aa) {
                            aa.parentNode.removeChild(aa);
                            if (V.length > 0)(aa = V[V.length - 1]) && aa.parentNode && aa.parentNode.removeChild(aa)
                        }
                    } else B = e.createTextNode(B);
                    var qa;
                    if (!d.support.appendChecked) if (B[0] && typeof (qa = B.length) === "number") for (r = 0; r < qa; r++) z(B[r]);
                    else z(B);
                    if (B.nodeType) w.push(B);
                    else w = d.merge(w, B)
                }
            }
            if (m) {
                b = function (ba) {
                    return !ba.type || bb.test(ba.type)
                };
                for (A = 0; w[A]; A++) {
                    e = w[A];
                    if (o && d.nodeName(e, "script") && (!e.type || bb.test(e.type))) o.push(e.parentNode ? e.parentNode.removeChild(e) : e);
                    else {
                        if (e.nodeType === 1) {
                            B = d.grep(e.getElementsByTagName("script"), b);
                            w.splice.apply(w, [A + 1, 0].concat(B))
                        }
                        m.appendChild(e)
                    }
                }
            }
            return w
        },
        cleanData: function (b) {
            for (var e, m, o = d.cache, r = d.event.special, w = d.support.deleteExpando, A = 0, B;
            (B = b[A]) != null; A++) if (!(B.nodeName && d.noData[B.nodeName.toLowerCase()])) if (m = B[d.expando]) {
                if ((e = o[m]) && e.events) {
                    for (var I in e.events) r[I] ? d.event.remove(B, I) : d.removeEvent(B, I, e.handle);
                    if (e.handle) e.handle.elem = null
                }
                if (w) delete B[d.expando];
                else B.removeAttribute && B.removeAttribute(d.expando);
                delete o[m]
            }
        }
    });
    var Ya = /alpha\([^)]*\)/i,
        gb = /opacity=([^)]*)/,
        ub = /([A-Z]|^ms)/g,
        $b = /^[\-+]?(?:\d*\.)?\d+$/i,
        Ib = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i,
        ac = /^([\-+])=([\-+.\de]+)/,
        bc = /^margin/,
        cc = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        pb = ["Top", "Right", "Bottom", "Left"],
        yb, Pb, Qb;
    d.fn.css = function (b, e) {
        return d.access(this, function (m, o, r) {
            return r !== f ? d.style(m, o, r) : d.css(m, o)
        }, b, e, arguments.length > 1)
    };
    d.extend({
        cssHooks: {
            opacity: {
                get: function (b, e) {
                    if (e) {
                        var m = yb(b, "opacity");
                        return m === "" ? "1" : m
                    } else return b.style.opacity
                }
            }
        },
        cssNumber: {
            fillOpacity: true,
            fontWeight: true,
            lineHeight: true,
            opacity: true,
            orphans: true,
            widows: true,
            zIndex: true,
            zoom: true
        },
        cssProps: {
            "float": d.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function (b, e, m, o) {
            if (!(!b || b.nodeType === 3 || b.nodeType === 8 || !b.style)) {
                var r, w = d.camelCase(e),
                    A = b.style,
                    B = d.cssHooks[w];
                e = d.cssProps[w] || w;
                if (m !== f) {
                    o = typeof m;
                    if (o === "string" && (r = ac.exec(m))) {
                        m = +(r[1] + 1) * +r[2] + parseFloat(d.css(b, e));
                        o = "number"
                    }
                    if (!(m == null || o === "number" && isNaN(m))) {
                        if (o === "number" && !d.cssNumber[w]) m += "px";
                        if (!B || !("set" in B) || (m = B.set(b, m)) !== f) try {
                            A[e] = m
                        } catch (I) {}
                    }
                } else {
                    if (B && "get" in B && (r = B.get(b, false, o)) !== f) return r;
                    return A[e]
                }
            }
        },
        css: function (b, e, m) {
            var o, r;
            e = d.camelCase(e);
            r = d.cssHooks[e];
            e = d.cssProps[e] || e;
            if (e === "cssFloat") e = "float";
            if (r && "get" in r && (o = r.get(b, true, m)) !== f) return o;
            else if (yb) return yb(b, e)
        },
        swap: function (b, e, m) {
            var o = {},
                r;
            for (r in e) {
                o[r] = b.style[r];
                b.style[r] = e[r]
            }
            m = m.call(b);
            for (r in e) b.style[r] = o[r];
            return m
        }
    });
    d.curCSS = d.css;
    if (x.defaultView && x.defaultView.getComputedStyle) Pb = function (b, e) {
        var m, o, r, w = b.style;
        e = e.replace(ub, "-$1").toLowerCase();
        if ((o = b.ownerDocument.defaultView) && (r = o.getComputedStyle(b, null))) {
            m = r.getPropertyValue(e);
            if (m === "" && !d.contains(b.ownerDocument.documentElement, b)) m = d.style(b, e)
        }
        if (!d.support.pixelMargin && r && bc.test(e) && Ib.test(m)) {
            o = w.width;
            w.width = m;
            m = r.width;
            w.width = o
        }
        return m
    };
    if (x.documentElement.currentStyle) Qb = function (b, e) {
        var m, o, r = b.currentStyle && b.currentStyle[e],
            w = b.style;
        if (r == null && w && (m = w[e])) r = m;
        if (Ib.test(r)) {
            m = w.left;
            if (o = b.runtimeStyle && b.runtimeStyle.left) b.runtimeStyle.left = b.currentStyle.left;
            w.left = e === "fontSize" ? "1em" : r;
            r = w.pixelLeft + "px";
            w.left = m;
            if (o) b.runtimeStyle.left = o
        }
        return r === "" ? "auto" : r
    };
    yb = Pb || Qb;
    d.each(["height", "width"], function (b, e) {
        d.cssHooks[e] = {
            get: function (m, o, r) {
                if (o) return m.offsetWidth !== 0 ? E(m, e, r) : d.swap(m, cc, function () {
                    return E(m, e, r)
                })
            },
            set: function (m, o) {
                return $b.test(o) ? o + "px" : o
            }
        }
    });
    if (!d.support.opacity) d.cssHooks.opacity = {
        get: function (b, e) {
            return gb.test((e && b.currentStyle ? b.currentStyle.filter : b.style.filter) || "") ? parseFloat(RegExp.$1) / 100 + "" : e ? "1" : ""
        },
        set: function (b, e) {
            var m = b.style,
                o = b.currentStyle,
                r = d.isNumeric(e) ? "alpha(opacity=" + e * 100 + ")" : "",
                w = o && o.filter || m.filter || "";
            m.zoom = 1;
            if (e >= 1 && d.trim(w.replace(Ya, "")) === "") {
                m.removeAttribute("filter");
                if (o && !o.filter) return
            }
            m.filter = Ya.test(w) ? w.replace(Ya, r) : w + " " + r
        }
    };
    d(function () {
        if (!d.support.reliableMarginRight) d.cssHooks.marginRight = {
            get: function (b, e) {
                return d.swap(b, {
                    display: "inline-block"
                }, function () {
                    return e ? yb(b, "margin-right") : b.style.marginRight
                })
            }
        }
    });
    if (d.expr && d.expr.filters) {
        d.expr.filters.hidden = function (b) {
            var e = b.offsetHeight;
            return b.offsetWidth === 0 && e === 0 || !d.support.reliableHiddenOffsets && (b.style && b.style.display || d.css(b, "display")) === "none"
        };
        d.expr.filters.visible = function (b) {
            return !d.expr.filters.hidden(b)
        }
    }
    d.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function (b, e) {
        d.cssHooks[b + e] = {
            expand: function (m) {
                var o = typeof m === "string" ? m.split(" ") : [m],
                    r = {};
                for (m = 0; m < 4; m++) r[b + pb[m] + e] = o[m] || o[m - 2] || o[0];
                return r
            }
        }
    });
    var dc = /%20/g,
        Zb = /\[\]$/,
        Rb = /\r?\n/g,
        ec = /#.*$/,
        fc = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
        gc = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
        hc = /^(?:GET|HEAD)$/,
        ic = /^\/\//,
        Sb = /\?/,
        jc = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
        kc = /^(?:select|textarea)/i,
        Ob = /\s+/,
        lc = /([?&])_=[^&]*/,
        Tb = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,
        Ub = d.fn.load,
        Jb = {},
        Vb = {},
        vb, wb, Wb = ["*/"] + ["*"];
    try {
        vb = y.href
    } catch (sc) {
        vb = x.createElement("a");
        vb.href = "";
        vb = vb.href
    }
    wb = Tb.exec(vb.toLowerCase()) || [];
    d.fn.extend({
        load: function (b, e, m) {
            if (typeof b !== "string" && Ub) return Ub.apply(this, arguments);
            else if (!this.length) return this;
            var o = b.indexOf(" ");
            if (o >= 0) {
                var r = b.slice(o, b.length);
                b = b.slice(0, o)
            }
            o = "GET";
            if (e) if (d.isFunction(e)) {
                m = e;
                e = f
            } else if (typeof e === "object") {
                e = d.param(e, d.ajaxSettings.traditional);
                o = "POST"
            }
            var w = this;
            d.ajax({
                url: b,
                type: o,
                dataType: "html",
                data: e,
                complete: function (A, B, I) {
                    I = A.responseText;
                    if (A.isResolved()) {
                        A.done(function (U) {
                            I = U
                        });
                        w.html(r ? d("<div>").append(I.replace(jc, "")).find(r) : I)
                    }
                    m && w.each(m, [I, B, A])
                }
            });
            return this
        },
        serialize: function () {
            return d.param(this.serializeArray())
        },
        serializeArray: function () {
            return this.map(function () {
                return this.elements ? d.makeArray(this.elements) : this
            }).filter(function () {
                return this.name && !this.disabled && (this.checked || kc.test(this.nodeName) || gc.test(this.type))
            }).map(function (b, e) {
                var m = d(this).val();
                return m == null ? null : d.isArray(m) ? d.map(m, function (o) {
                    return {
                        name: e.name,
                        value: o.replace(Rb, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: m.replace(Rb, "\r\n")
                }
            }).get()
        }
    });
    d.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function (b, e) {
        d.fn[e] = function (m) {
            return this.on(e, m)
        }
    });
    d.each(["get", "post"], function (b, e) {
        d[e] = function (m, o, r, w) {
            if (d.isFunction(o)) {
                w = w || r;
                r = o;
                o = f
            }
            return d.ajax({
                type: e,
                url: m,
                data: o,
                success: r,
                dataType: w
            })
        }
    });
    d.extend({
        getScript: function (b, e) {
            return d.get(b, f, e, "script")
        },
        getJSON: function (b, e, m) {
            return d.get(b, e, m, "json")
        },
        ajaxSetup: function (b, e) {
            if (e) oa(b, d.ajaxSettings);
            else {
                e = b;
                b = d.ajaxSettings
            }
            oa(b, e);
            return b
        },
        ajaxSettings: {
            url: vb,
            isLocal: /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/.test(wb[1]),
            global: true,
            type: "GET",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            processData: true,
            async: true,
            accepts: {
                xml: "application/xml, text/xml",
                html: "text/html",
                text: "text/plain",
                json: "application/json, text/javascript",
                "*": Wb
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },
            converters: {
                "* text": a.String,
                "text html": true,
                "text json": d.parseJSON,
                "text xml": d.parseXML
            },
            flatOptions: {
                context: true,
                url: true
            }
        },
        ajaxPrefilter: M(Jb),
        ajaxTransport: M(Vb),
        ajax: function (b, e) {
            function m(D, P, R, T) {
                if (Qa !== 2) {
                    Qa = 2;
                    Pa && clearTimeout(Pa);
                    Ia = f;
                    qa = T || "";
                    Ka.readyState = D > 0 ? 4 : 0;
                    var X, ra, ia;
                    T = P;
                    if (R) {
                        var La = o,
                            F = Ka,
                            ca = La.contents,
                            ha = La.dataTypes,
                            ea = La.responseFields,
                            Fa, za, Ta, $a;
                        for (za in ea) if (za in R) F[ea[za]] = R[za];
                        for (; ha[0] === "*";) {
                            ha.shift();
                            if (Fa === f) Fa = La.mimeType || F.getResponseHeader("content-type")
                        }
                        if (Fa) for (za in ca) if (ca[za] && ca[za].test(Fa)) {
                            ha.unshift(za);
                            break
                        }
                        if (ha[0] in R) Ta = ha[0];
                        else {
                            for (za in R) {
                                if (!ha[0] || La.converters[za + " " + ha[0]]) {
                                    Ta = za;
                                    break
                                }
                                $a || ($a = za)
                            }
                            Ta = Ta || $a
                        }
                        if (Ta) {
                            Ta !== ha[0] && ha.unshift(Ta);
                            R = R[Ta]
                        } else R = void 0
                    } else R = f;
                    if (D >= 200 && D < 300 || D === 304) {
                        if (o.ifModified) {
                            if (Fa = Ka.getResponseHeader("Last-Modified")) d.lastModified[U] = Fa;
                            if (Fa = Ka.getResponseHeader("Etag")) d.etag[U] = Fa
                        }
                        if (D === 304) {
                            T = "notmodified";
                            X = true
                        } else try {
                            Fa = o;
                            if (Fa.dataFilter) R = Fa.dataFilter(R, Fa.dataType);
                            var ib = Fa.dataTypes;
                            za = {};
                            var ab, qb, Lb = ib.length,
                                Db, jb = ib[0],
                                Ab, Mb, ob, xb, Bb;
                            for (ab = 1; ab < Lb; ab++) {
                                if (ab === 1) for (qb in Fa.converters) if (typeof qb === "string") za[qb.toLowerCase()] = Fa.converters[qb];
                                Ab = jb;
                                jb = ib[ab];
                                if (jb === "*") jb = Ab;
                                else if (Ab !== "*" && Ab !== jb) {
                                    Mb = Ab + " " + jb;
                                    ob = za[Mb] || za["* " + jb];
                                    if (!ob) {
                                        Bb = f;
                                        for (xb in za) {
                                            Db = xb.split(" ");
                                            if (Db[0] === Ab || Db[0] === "*") if (Bb = za[Db[1] + " " + jb]) {
                                                xb = za[xb];
                                                if (xb === true) ob = Bb;
                                                else if (Bb === true) ob = xb;
                                                break
                                            }
                                        }
                                    }
                                    ob || Bb || d.error("No conversion from " + Mb.replace(" ", " to "));
                                    if (ob !== true) R = ob ? ob(R) : Bb(xb(R))
                                }
                            }
                            ra = R;
                            T = "success";
                            X = true
                        } catch (mc) {
                            T = "parsererror";
                            ia = mc
                        }
                    } else {
                        ia = T;
                        if (!T || D) {
                            T = "error";
                            if (D < 0) D = 0
                        }
                    }
                    Ka.status = D;
                    Ka.statusText = "" + (P || T);
                    X ? A.resolveWith(r, [ra, T, Ka]) : A.rejectWith(r, [Ka, T, ia]);
                    Ka.statusCode(I);
                    I = f;
                    if (mb) w.trigger("ajax" + (X ? "Success" : "Error"), [Ka, o, X ? ra : ia]);
                    B.fireWith(r, [Ka, T]);
                    if (mb) {
                        w.trigger("ajaxComplete", [Ka, o]);
                        --d.active || d.event.trigger("ajaxStop")
                    }
                }
            }
            if (typeof b === "object") {
                e = b;
                b = f
            }
            e = e || {};
            var o = d.ajaxSetup({}, e),
                r = o.context || o,
                w = r !== o && (r.nodeType || r instanceof d) ? d(r) : d.event,
                A = d.Deferred(),
                B = d.Callbacks("once memory"),
                I = o.statusCode || {},
                U, aa = {},
                V = {},
                qa, ba, Ia, Pa, Oa, Qa = 0,
                mb, db, Ka = {
                    readyState: 0,
                    setRequestHeader: function (D, P) {
                        if (!Qa) {
                            var R = D.toLowerCase();
                            D = V[R] = V[R] || D;
                            aa[D] = P
                        }
                        return this
                    },
                    getAllResponseHeaders: function () {
                        return Qa === 2 ? qa : null
                    },
                    getResponseHeader: function (D) {
                        var P;
                        if (Qa === 2) {
                            if (!ba) for (ba = {}; P = fc.exec(qa);) ba[P[1].toLowerCase()] = P[2];
                            P = ba[D.toLowerCase()]
                        }
                        return P === f ? null : P
                    },
                    overrideMimeType: function (D) {
                        if (!Qa) o.mimeType = D;
                        return this
                    },
                    abort: function (D) {
                        D = D || "abort";
                        Ia && Ia.abort(D);
                        m(0, D);
                        return this
                    }
                };
            A.promise(Ka);
            Ka.success = Ka.done;
            Ka.error = Ka.fail;
            Ka.complete = B.add;
            Ka.statusCode = function (D) {
                if (D) {
                    var P;
                    if (Qa < 2) for (P in D) I[P] = [I[P], D[P]];
                    else {
                        P = D[Ka.status];
                        Ka.then(P, P)
                    }
                }
                return this
            };
            o.url = ((b || o.url) + "").replace(ec, "").replace(ic, wb[1] + "//");
            o.dataTypes = d.trim(o.dataType || "*").toLowerCase().split(Ob);
            if (o.crossDomain == null) {
                Oa = Tb.exec(o.url.toLowerCase());
                o.crossDomain = !! (Oa && (Oa[1] != wb[1] || Oa[2] != wb[2] || (Oa[3] || (Oa[1] === "http:" ? 80 : 443)) != (wb[3] || (wb[1] === "http:" ? 80 : 443))))
            }
            if (o.data && o.processData && typeof o.data !== "string") o.data = d.param(o.data, o.traditional);
            ma(Jb, o, e, Ka);
            if (Qa === 2) return false;
            mb = o.global;
            o.type = o.type.toUpperCase();
            o.hasContent = !hc.test(o.type);
            mb && d.active++ === 0 && d.event.trigger("ajaxStart");
            if (!o.hasContent) {
                if (o.data) {
                    o.url += (Sb.test(o.url) ? "&" : "?") + o.data;
                    delete o.data
                }
                U = o.url;
                if (o.cache === false) {
                    Oa = d.now();
                    var eb = o.url.replace(lc, "$1_=" + Oa);
                    o.url = eb + (eb === o.url ? (Sb.test(o.url) ? "&" : "?") + "_=" + Oa : "")
                }
            }
            if (o.data && o.hasContent && o.contentType !== false || e.contentType) Ka.setRequestHeader("Content-Type", o.contentType);
            if (o.ifModified) {
                U = U || o.url;
                d.lastModified[U] && Ka.setRequestHeader("If-Modified-Since", d.lastModified[U]);
                d.etag[U] && Ka.setRequestHeader("If-None-Match", d.etag[U])
            }
            Ka.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + (o.dataTypes[0] !== "*" ? ", " + Wb + "; q=0.01" : "") : o.accepts["*"]);
            for (db in o.headers) Ka.setRequestHeader(db, o.headers[db]);
            if (o.beforeSend && (o.beforeSend.call(r, Ka, o) === false || Qa === 2)) {
                Ka.abort();
                return false
            }
            for (db in {
                success: 1,
                error: 1,
                complete: 1
            }) Ka[db](o[db]);
            if (Ia = ma(Vb, o, e, Ka)) {
                Ka.readyState = 1;
                mb && w.trigger("ajaxSend", [Ka, o]);
                if (o.async && o.timeout > 0) Pa = setTimeout(function () {
                    Ka.abort("timeout")
                }, o.timeout);
                try {
                    Qa = 1;
                    Ia.send(aa, m)
                } catch (v) {
                    if (Qa < 2) m(-1, v);
                    else throw v;
                }
            } else m(-1, "No Transport");
            return Ka
        },
        param: function (b, e) {
            var m = [],
                o = function (w, A) {
                    A = d.isFunction(A) ? A() : A;
                    m[m.length] = encodeURIComponent(w) + "=" + encodeURIComponent(A)
                };
            if (e === f) e = d.ajaxSettings.traditional;
            if (d.isArray(b) || b.jquery && !d.isPlainObject(b)) d.each(b, function () {
                o(this.name, this.value)
            });
            else for (var r in b) Da(r, b[r], e, o);
            return m.join("&").replace(dc, "+")
        }
    });
    d.extend({
        active: 0,
        lastModified: {},
        etag: {}
    });
    var nc = d.now(),
        Gb = /(\=)\?(&|$)|\?\?/i;
    d.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            return d.expando + "_" + nc++
        }
    });
    d.ajaxPrefilter("json jsonp", function (b, e, m) {
        e = typeof b.data === "string" && /^application\/x\-www\-form\-urlencoded/.test(b.contentType);
        if (b.dataTypes[0] === "jsonp" || b.jsonp !== false && (Gb.test(b.url) || e && Gb.test(b.data))) {
            var o, r = b.jsonpCallback = d.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback,
                w = a[r],
                A = b.url,
                B = b.data,
                I = "$1" + r + "$2";
            if (b.jsonp !== false) {
                A = A.replace(Gb, I);
                if (b.url === A) {
                    if (e) B = B.replace(Gb, I);
                    if (b.data === B) A += (/\?/.test(A) ? "&" : "?") + b.jsonp + "=" + r
                }
            }
            b.url = A;
            b.data = B;
            a[r] = function (U) {
                o = [U]
            };
            m.always(function () {
                a[r] = w;
                if (o && d.isFunction(w)) a[r](o[0])
            });
            b.converters["script json"] = function () {
                o || d.error(r + " was not called");
                return o[0]
            };
            b.dataTypes[0] = "json";
            return "script"
        }
    });
    d.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /javascript|ecmascript/
        },
        converters: {
            "text script": function (b) {
                d.globalEval(b);
                return b
            }
        }
    });
    d.ajaxPrefilter("script", function (b) {
        if (b.cache === f) b.cache = false;
        if (b.crossDomain) {
            b.type = "GET";
            b.global = false
        }
    });
    d.ajaxTransport("script", function (b) {
        if (b.crossDomain) {
            var e, m = x.head || x.getElementsByTagName("head")[0] || x.documentElement;
            return {
                send: function (o, r) {
                    e = x.createElement("script");
                    e.async = "async";
                    if (b.scriptCharset) e.charset = b.scriptCharset;
                    e.src = b.url;
                    e.onload = e.onreadystatechange = function (w, A) {
                        if (A || !e.readyState || /loaded|complete/.test(e.readyState)) {
                            e.onload = e.onreadystatechange = null;
                            m && e.parentNode && m.removeChild(e);
                            e = f;
                            A || r(200, "success")
                        }
                    };
                    m.insertBefore(e, m.firstChild)
                },
                abort: function () {
                    if (e) e.onload(0, 1)
                }
            }
        }
    });
    var Nb = a.ActiveXObject ?
    function () {
        for (var b in Cb) Cb[b](0, 1)
    } : false, oc = 0, Cb;
    d.ajaxSettings.xhr = a.ActiveXObject ?
    function () {
        var b;
        if (!(b = !this.isLocal && ya())) a: {
            try {
                b = new a.ActiveXObject("Microsoft.XMLHTTP");
                break a
            } catch (e) {}
            b = void 0
        }
        return b
    } : ya;
    (function (b) {
        d.extend(d.support, {
            ajax: !! b,
            cors: !! b && "withCredentials" in b
        })
    })(d.ajaxSettings.xhr());
    d.support.ajax && d.ajaxTransport(function (b) {
        if (!b.crossDomain || d.support.cors) {
            var e;
            return {
                send: function (m, o) {
                    var r = b.xhr(),
                        w, A;
                    b.username ? r.open(b.type, b.url, b.async, b.username, b.password) : r.open(b.type, b.url, b.async);
                    if (b.xhrFields) for (A in b.xhrFields) r[A] = b.xhrFields[A];
                    b.mimeType && r.overrideMimeType && r.overrideMimeType(b.mimeType);
                    if (!b.crossDomain && !m["X-Requested-With"]) m["X-Requested-With"] = "XMLHttpRequest";
                    try {
                        for (A in m) r.setRequestHeader(A, m[A])
                    } catch (B) {}
                    r.send(b.hasContent && b.data || null);
                    e = function (I, U) {
                        var aa, V, qa, ba, Ia;
                        try {
                            if (e && (U || r.readyState === 4)) {
                                e = f;
                                if (w) {
                                    r.onreadystatechange = d.noop;
                                    Nb && delete Cb[w]
                                }
                                if (U) r.readyState !== 4 && r.abort();
                                else {
                                    aa = r.status;
                                    qa = r.getAllResponseHeaders();
                                    ba = {};
                                    if ((Ia = r.responseXML) && Ia.documentElement) ba.xml = Ia;
                                    try {
                                        ba.text = r.responseText
                                    } catch (Pa) {}
                                    try {
                                        V = r.statusText
                                    } catch (Oa) {
                                        V = ""
                                    }
                                    if (!aa && b.isLocal && !b.crossDomain) aa = ba.text ? 200 : 404;
                                    else if (aa === 1223) aa = 204
                                }
                            }
                        } catch (Qa) {
                            U || o(-1, Qa)
                        }
                        ba && o(aa, V, ba, qa)
                    };
                    if (!b.async || r.readyState === 4) e();
                    else {
                        w = ++oc;
                        if (Nb) {
                            if (!Cb) {
                                Cb = {};
                                d(a).unload(Nb)
                            }
                            Cb[w] = e
                        }
                        r.onreadystatechange = e
                    }
                },
                abort: function () {
                    e && e(0, 1)
                }
            }
        }
    });
    var Kb = {},
        hb, zb, pc = /^(?:toggle|show|hide)$/,
        qc = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,
        Hb, Fb = [
            ["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"],
            ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"],
            ["opacity"]
        ],
        Eb;
    d.fn.extend({
        show: function (b, e, m) {
            if (b || b === 0) return this.animate(Z("show", 3), b, e, m);
            else {
                m = 0;
                for (var o = this.length; m < o; m++) {
                    b = this[m];
                    if (b.style) {
                        e = b.style.display;
                        if (!d._data(b, "olddisplay") && e === "none") e = b.style.display = "";
                        if (e === "" && d.css(b, "display") === "none" || !d.contains(b.ownerDocument.documentElement, b)) d._data(b, "olddisplay", ja(b.nodeName))
                    }
                }
                for (m = 0; m < o; m++) {
                    b = this[m];
                    if (b.style) {
                        e = b.style.display;
                        if (e === "" || e === "none") b.style.display = d._data(b, "olddisplay") || ""
                    }
                }
                return this
            }
        },
        hide: function (b, e, m) {
            if (b || b === 0) return this.animate(Z("hide", 3), b, e, m);
            else {
                m = 0;
                for (var o = this.length; m < o; m++) {
                    b = this[m];
                    if (b.style) {
                        e = d.css(b, "display");
                        e !== "none" && !d._data(b, "olddisplay") && d._data(b, "olddisplay", e)
                    }
                }
                for (m = 0; m < o; m++) if (this[m].style) this[m].style.display = "none";
                return this
            }
        },
        _toggle: d.fn.toggle,
        toggle: function (b, e, m) {
            var o = typeof b === "boolean";
            if (d.isFunction(b) && d.isFunction(e)) this._toggle.apply(this, arguments);
            else b == null || o ? this.each(function () {
                var r = o ? b : d(this).is(":hidden");
                d(this)[r ? "show" : "hide"]()
            }) : this.animate(Z("toggle", 3), b, e, m);
            return this
        },
        fadeTo: function (b, e, m, o) {
            return this.filter(":hidden").css("opacity", 0).show().end().animate({
                opacity: e
            }, b, m, o)
        },
        animate: function (b, e, m, o) {
            function r() {
                w.queue === false && d._mark(this);
                var A = d.extend({}, w),
                    B = this.nodeType === 1,
                    I = B && d(this).is(":hidden"),
                    U, aa, V, qa, ba;
                A.animatedProperties = {};
                for (V in b) {
                    U = d.camelCase(V);
                    if (V !== U) {
                        b[U] = b[V];
                        delete b[V]
                    }
                    if ((aa = d.cssHooks[U]) && "expand" in aa) {
                        qa = aa.expand(b[U]);
                        delete b[U];
                        for (V in qa) V in b || (b[V] = qa[V])
                    }
                }
                for (U in b) {
                    aa = b[U];
                    if (d.isArray(aa)) {
                        A.animatedProperties[U] = aa[1];
                        aa = b[U] = aa[0]
                    } else A.animatedProperties[U] = A.specialEasing && A.specialEasing[U] || A.easing || "swing";
                    if (aa === "hide" && I || aa === "show" && !I) return A.complete.call(this);
                    if (B && (U === "height" || U === "width")) {
                        A.overflow = [this.style.overflow, this.style.overflowX, this.style.overflowY];
                        if (d.css(this, "display") === "inline" && d.css(this, "float") === "none") if (!d.support.inlineBlockNeedsLayout || ja(this.nodeName) === "inline") this.style.display = "inline-block";
                        else this.style.zoom = 1
                    }
                }
                if (A.overflow != null) this.style.overflow = "hidden";
                for (V in b) {
                    B = new d.fx(this, A, V);
                    aa = b[V];
                    if (pc.test(aa)) if (U = d._data(this, "toggle" + V) || (aa === "toggle" ? I ? "show" : "hide" : 0)) {
                        d._data(this, "toggle" + V, U === "show" ? "hide" : "show");
                        B[U]()
                    } else B[aa]();
                    else {
                        U = qc.exec(aa);
                        qa = B.cur();
                        if (U) {
                            aa = parseFloat(U[2]);
                            ba = U[3] || (d.cssNumber[V] ? "" : "px");
                            if (ba !== "px") {
                                d.style(this, V, (aa || 1) + ba);
                                qa *= (aa || 1) / B.cur();
                                d.style(this, V, qa + ba)
                            }
                            if (U[1]) aa = (U[1] === "-=" ? -1 : 1) * aa + qa;
                            B.custom(qa, aa, ba)
                        } else B.custom(qa, aa, "")
                    }
                }
                return true
            }
            var w = d.speed(e, m, o);
            if (d.isEmptyObject(b)) return this.each(w.complete, [false]);
            b = d.extend({}, b);
            return w.queue === false ? this.each(r) : this.queue(w.queue, r)
        },
        stop: function (b, e, m) {
            if (typeof b !== "string") {
                m = e;
                e = b;
                b = f
            }
            if (e && b !== false) this.queue(b || "fx", []);
            return this.each(function () {
                function o(I, U, aa) {
                    U = U[aa];
                    d.removeData(I, aa, true);
                    U.stop(m)
                }
                var r, w = false,
                    A = d.timers,
                    B = d._data(this);
                m || d._unmark(true, this);
                if (b == null) for (r in B) B[r] && B[r].stop && r.indexOf(".run") === r.length - 4 && o(this, B, r);
                else if (B[r = b + ".run"] && B[r].stop) o(this, B, r);
                for (r = A.length; r--;) if (A[r].elem === this && (b == null || A[r].queue === b)) {
                    if (m) A[r](true);
                    else A[r].saveState();
                    w = true;
                    A.splice(r, 1)
                }
                m && w || d.dequeue(this, b)
            })
        }
    });
    d.each({
        slideDown: Z("show", 1),
        slideUp: Z("hide", 1),
        slideToggle: Z("toggle", 1),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function (b, e) {
        d.fn[b] = function (m, o, r) {
            return this.animate(e, m, o, r)
        }
    });
    d.extend({
        speed: function (b, e, m) {
            var o = b && typeof b === "object" ? d.extend({}, b) : {
                complete: m || !m && e || d.isFunction(b) && b,
                duration: b,
                easing: m && e || e && !d.isFunction(e) && e
            };
            o.duration = d.fx.off ? 0 : typeof o.duration === "number" ? o.duration : o.duration in d.fx.speeds ? d.fx.speeds[o.duration] : d.fx.speeds._default;
            if (o.queue == null || o.queue === true) o.queue = "fx";
            o.old = o.complete;
            o.complete = function (r) {
                d.isFunction(o.old) && o.old.call(this);
                if (o.queue) d.dequeue(this, o.queue);
                else r !== false && d._unmark(this)
            };
            return o
        },
        easing: {
            linear: function (b) {
                return b
            },
            swing: function (b) {
                return -Math.cos(b * Math.PI) / 2 + 0.5
            }
        },
        timers: [],
        fx: function (b, e, m) {
            this.options = e;
            this.elem = b;
            this.prop = m;
            e.orig = e.orig || {}
        }
    });
    d.fx.prototype = {
        update: function () {
            this.options.step && this.options.step.call(this.elem, this.now, this);
            (d.fx.step[this.prop] || d.fx.step._default)(this)
        },
        cur: function () {
            if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) return this.elem[this.prop];
            var b, e = d.css(this.elem, this.prop);
            return isNaN(b = parseFloat(e)) ? !e || e === "auto" ? 0 : e : b
        },
        custom: function (b, e, m) {
            function o(A) {
                return r.step(A)
            }
            var r = this,
                w = d.fx;
            this.startTime = Eb || pa();
            this.end = e;
            this.now = this.start = b;
            this.pos = this.state = 0;
            this.unit = m || this.unit || (d.cssNumber[this.prop] ? "" : "px");
            o.queue = this.options.queue;
            o.elem = this.elem;
            o.saveState = function () {
                if (d._data(r.elem, "fxshow" + r.prop) === f) if (r.options.hide) d._data(r.elem, "fxshow" + r.prop, r.start);
                else r.options.show && d._data(r.elem, "fxshow" + r.prop, r.end)
            };
            if (o() && d.timers.push(o) && !Hb) Hb = setInterval(w.tick, w.interval)
        },
        show: function () {
            var b = d._data(this.elem, "fxshow" + this.prop);
            this.options.orig[this.prop] = b || d.style(this.elem, this.prop);
            this.options.show = true;
            b !== f ? this.custom(this.cur(), b) : this.custom(this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur());
            d(this.elem).show()
        },
        hide: function () {
            this.options.orig[this.prop] = d._data(this.elem, "fxshow" + this.prop) || d.style(this.elem, this.prop);
            this.options.hide = true;
            this.custom(this.cur(), 0)
        },
        step: function (b) {
            var e, m = Eb || pa(),
                o = true,
                r = this.elem,
                w = this.options;
            if (b || m >= w.duration + this.startTime) {
                this.now = this.end;
                this.pos = this.state = 1;
                this.update();
                w.animatedProperties[this.prop] = true;
                for (e in w.animatedProperties) if (w.animatedProperties[e] !== true) o = false;
                if (o) {
                    w.overflow != null && !d.support.shrinkWrapBlocks && d.each(["", "X", "Y"], function (A, B) {
                        r.style["overflow" + B] = w.overflow[A]
                    });
                    w.hide && d(r).hide();
                    if (w.hide || w.show) for (e in w.animatedProperties) {
                        d.style(r, e, w.orig[e]);
                        d.removeData(r, "fxshow" + e, true);
                        d.removeData(r, "toggle" + e, true)
                    }
                    if (b = w.complete) {
                        w.complete = false;
                        b.call(r)
                    }
                }
                return false
            } else {
                if (w.duration == Infinity) this.now = m;
                else {
                    b = m - this.startTime;
                    this.state = b / w.duration;
                    this.pos = d.easing[w.animatedProperties[this.prop]](this.state, b, 0, 1, w.duration);
                    this.now = this.start + (this.end - this.start) * this.pos
                }
                this.update()
            }
            return true
        }
    };
    d.extend(d.fx, {
        tick: function () {
            for (var b, e = d.timers, m = 0; m < e.length; m++) {
                b = e[m];
                !b() && e[m] === b && e.splice(m--, 1)
            }
            e.length || d.fx.stop()
        },
        interval: 13,
        stop: function () {
            clearInterval(Hb);
            Hb = null
        },
        speeds: {
            slow: 600,
            fast: 200,
            _default: 400
        },
        step: {
            opacity: function (b) {
                d.style(b.elem, "opacity", b.now)
            },
            _default: function (b) {
                if (b.elem.style && b.elem.style[b.prop] != null) b.elem.style[b.prop] = b.now + b.unit;
                else b.elem[b.prop] = b.now
            }
        }
    });
    d.each(Fb.concat.apply([], Fb), function (b, e) {
        if (e.indexOf("margin")) d.fx.step[e] = function (m) {
            d.style(m.elem, e, Math.max(0, m.now) + m.unit)
        }
    });
    if (d.expr && d.expr.filters) d.expr.filters.animated = function (b) {
        return d.grep(d.timers, function (e) {
            return b === e.elem
        }).length
    };
    var Xb, rc = /^t(?:able|d|h)$/i,
        Yb = /^(?:body|html)$/i;
    Xb = "getBoundingClientRect" in x.documentElement ?
    function (b, e, m, o) {
        try {
            o = b.getBoundingClientRect()
        } catch (r) {}
        if (!o || !d.contains(m, b)) return o ? {
            top: o.top,
            left: o.left
        } : {
            top: 0,
            left: 0
        };
        b = e.body;
        e = C(e);
        return {
            top: o.top + (e.pageYOffset || d.support.boxModel && m.scrollTop || b.scrollTop) - (m.clientTop || b.clientTop || 0),
            left: o.left + (e.pageXOffset || d.support.boxModel && m.scrollLeft || b.scrollLeft) - (m.clientLeft || b.clientLeft || 0)
        }
    } : function (b, e, m) {
        var o, r = b.offsetParent,
            w = e.body;
        o = (e = e.defaultView) ? e.getComputedStyle(b, null) : b.currentStyle;
        for (var A = b.offsetTop, B = b.offsetLeft;
        (b = b.parentNode) && b !== w && b !== m;) {
            if (d.support.fixedPosition && o.position === "fixed") break;
            o = e ? e.getComputedStyle(b, null) : b.currentStyle;
            A -= b.scrollTop;
            B -= b.scrollLeft;
            if (b === r) {
                A += b.offsetTop;
                B += b.offsetLeft;
                if (d.support.doesNotAddBorder && !(d.support.doesAddBorderForTableAndCells && rc.test(b.nodeName))) {
                    A += parseFloat(o.borderTopWidth) || 0;
                    B += parseFloat(o.borderLeftWidth) || 0
                }
                r = b.offsetParent
            }
            if (d.support.subtractsBorderForOverflowNotVisible && o.overflow !== "visible") {
                A += parseFloat(o.borderTopWidth) || 0;
                B += parseFloat(o.borderLeftWidth) || 0
            }
        }
        if (o.position === "relative" || o.position === "static") {
            A += w.offsetTop;
            B += w.offsetLeft
        }
        if (d.support.fixedPosition && o.position === "fixed") {
            A += Math.max(m.scrollTop, w.scrollTop);
            B += Math.max(m.scrollLeft, w.scrollLeft)
        }
        return {
            top: A,
            left: B
        }
    };
    d.fn.offset = function (b) {
        if (arguments.length) return b === f ? this : this.each(function (o) {
            d.offset.setOffset(this, b, o)
        });
        var e = this[0],
            m = e && e.ownerDocument;
        if (!m) return null;
        if (e === m.body) return d.offset.bodyOffset(e);
        return Xb(e, m, m.documentElement)
    };
    d.offset = {
        bodyOffset: function (b) {
            var e = b.offsetTop,
                m = b.offsetLeft;
            if (d.support.doesNotIncludeMarginInBodyOffset) {
                e += parseFloat(d.css(b, "marginTop")) || 0;
                m += parseFloat(d.css(b, "marginLeft")) || 0
            }
            return {
                top: e,
                left: m
            }
        },
        setOffset: function (b, e, m) {
            var o = d.css(b, "position");
            if (o === "static") b.style.position = "relative";
            var r = d(b),
                w = r.offset(),
                A = d.css(b, "top"),
                B = d.css(b, "left"),
                I = {},
                U = {};
            if ((o === "absolute" || o === "fixed") && d.inArray("auto", [A, B]) > -1) {
                U = r.position();
                o = U.top;
                B = U.left
            } else {
                o = parseFloat(A) || 0;
                B = parseFloat(B) || 0
            }
            if (d.isFunction(e)) e = e.call(b, m, w);
            if (e.top != null) I.top = e.top - w.top + o;
            if (e.left != null) I.left = e.left - w.left + B;
            "using" in e ? e.using.call(b, I) : r.css(I)
        }
    };
    d.fn.extend({
        position: function () {
            if (!this[0]) return null;
            var b = this[0],
                e = this.offsetParent(),
                m = this.offset(),
                o = Yb.test(e[0].nodeName) ? {
                    top: 0,
                    left: 0
                } : e.offset();
            m.top -= parseFloat(d.css(b, "marginTop")) || 0;
            m.left -= parseFloat(d.css(b, "marginLeft")) || 0;
            o.top += parseFloat(d.css(e[0], "borderTopWidth")) || 0;
            o.left += parseFloat(d.css(e[0], "borderLeftWidth")) || 0;
            return {
                top: m.top - o.top,
                left: m.left - o.left
            }
        },
        offsetParent: function () {
            return this.map(function () {
                for (var b = this.offsetParent || x.body; b && !Yb.test(b.nodeName) && d.css(b, "position") === "static";) b = b.offsetParent;
                return b
            })
        }
    });
    d.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function (b, e) {
        var m = /Y/.test(e);
        d.fn[b] = function (o) {
            return d.access(this, function (r, w, A) {
                var B = C(r);
                if (A === f) return B ? e in B ? B[e] : d.support.boxModel && B.document.documentElement[w] || B.document.body[w] : r[w];
                if (B) B.scrollTo(!m ? A : d(B).scrollLeft(), m ? A : d(B).scrollTop());
                else r[w] = A
            }, b, o, arguments.length, null)
        }
    });
    d.each({
        Height: "height",
        Width: "width"
    }, function (b, e) {
        var m = "client" + b,
            o = "scroll" + b,
            r = "offset" + b;
        d.fn["inner" + b] = function () {
            var w = this[0];
            return w ? w.style ? parseFloat(d.css(w, e, "padding")) : this[e]() : null
        };
        d.fn["outer" + b] = function (w) {
            var A = this[0];
            return A ? A.style ? parseFloat(d.css(A, e, w ? "margin" : "border")) : this[e]() : null
        };
        d.fn[e] = function (w) {
            return d.access(this, function (A, B, I) {
                if (d.isWindow(A)) {
                    B = A.document;
                    A = B.documentElement[m];
                    return d.support.boxModel && A || B.body && B.body[m] || A
                }
                if (A.nodeType === 9) {
                    B = A.documentElement;
                    if (B[m] >= B[o]) return B[m];
                    return Math.max(A.body[o], B[o], A.body[r], B[r])
                }
                if (I === f) {
                    A = d.css(A, B);
                    B = parseFloat(A);
                    return d.isNumeric(B) ? B : A
                }
                d(A).css(B, I)
            }, e, w, arguments.length, null)
        }
    });
    a.jQuery = a.$ = d;
    typeof define === "function" && define.amd && define.amd.jQuery && define("jquery", [], function () {
        return d
    })
})(window);
(function (a, f) {
    var i, g, h, j, l, c, k, q, s, p, n, u, z, E, M, ma, oa, Da, ya, pa, Ma, Z, ja;
    i = function (C) {
        return new i.prototype.init(C)
    };
    if (typeof require !== "undefined" && typeof exports !== "undefined" && typeof module !== "undefined") module.exports = i;
    else a.Globalize = i;
    i.cultures = {};
    i.prototype = {
        constructor: i,
        init: function (C) {
            this.cultures = i.cultures;
            this.cultureSelector = C;
            return this
        }
    };
    i.prototype.init.prototype = i.prototype;
    i.cultures["default"] = {
        name: "en",
        englishName: "English",
        nativeName: "English",
        isRTL: false,
        language: "en",
        numberFormat: {
            pattern: ["-n"],
            decimals: 2,
            ",": ",",
            ".": ".",
            groupSizes: [3],
            "+": "+",
            "-": "-",
            percent: {
                pattern: ["-n %", "n %"],
                decimals: 2,
                groupSizes: [3],
                ",": ",",
                ".": ".",
                symbol: "%"
            },
            currency: {
                pattern: ["($n)", "$n"],
                decimals: 2,
                groupSizes: [3],
                ",": ",",
                ".": ".",
                symbol: "$"
            }
        },
        calendars: {
            standard: {
                name: "Gregorian_USEnglish",
                "/": "/",
                ":": ":",
                firstDay: 0,
                days: {
                    names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    namesAbbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                    namesShort: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
                },
                months: {
                    names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                    namesAbbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""]
                },
                AM: ["AM", "am", "AM"],
                PM: ["PM", "pm", "PM"],
                eras: [{
                    name: "A.D.",
                    start: null,
                    offset: 0
                }],
                twoDigitYearMax: 2029,
                patterns: {
                    d: "M/d/yyyy",
                    D: "dddd, MMMM dd, yyyy",
                    t: "h:mm tt",
                    T: "h:mm:ss tt",
                    f: "dddd, MMMM dd, yyyy h:mm tt",
                    F: "dddd, MMMM dd, yyyy h:mm:ss tt",
                    M: "MMMM dd",
                    Y: "yyyy MMMM",
                    S: "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
                }
            }
        },
        messages: {}
    };
    i.cultures["default"].calendar = i.cultures["default"].calendars.standard;
    i.cultures.en = i.cultures["default"];
    i.cultureSelector = "en";
    g = /^0x[a-f0-9]+$/i;
    h = /^[+-]?infinity$/i;
    j = /^[+-]?\d*\.?\d*(e[+-]?\d+)?$/;
    l = /^\s+|\s+$/g;
    c = function (C, x) {
        if (C.indexOf) return C.indexOf(x);
        for (var K = 0, y = C.length; K < y; K++) if (C[K] === x) return K;
        return -1
    };
    k = function (C, x) {
        return C.substr(C.length - x.length) === x
    };
    q = function (C) {
        var x, K, y, d, Q, N = arguments[0] || {},
            H = 1,
            O = arguments.length;
        C = false;
        if (typeof N === "boolean") {
            C = N;
            N = arguments[1] || {};
            H = 2
        }
        if (typeof N !== "object" && !p(N)) N = {};
        for (; H < O; H++) if ((x = arguments[H]) != null) for (K in x) {
            y = N[K];
            d = x[K];
            if (N !== d) if (C && d && (n(d) || (Q = s(d)))) {
                if (Q) {
                    Q = false;
                    y = y && s(y) ? y : []
                } else y = y && n(y) ? y : {};
                N[K] = q(C, y, d)
            } else if (d !== f) N[K] = d
        }
        return N
    };
    s = Array.isArray ||
    function (C) {
        return Object.prototype.toString.call(C) === "[object Array]"
    };
    p = function (C) {
        return Object.prototype.toString.call(C) === "[object Function]"
    };
    n = function (C) {
        return Object.prototype.toString.call(C) === "[object Object]"
    };
    u = function (C, x) {
        return C.indexOf(x) === 0
    };
    z = function (C) {
        return (C + "").replace(l, "")
    };
    E = function (C, x, K) {
        var y;
        for (y = C.length; y < x; y += 1) C = K ? "0" + C : C + "0";
        return C
    };
    M = function (C, x) {
        for (var K = 0, y = false, d = 0, Q = C.length; d < Q; d++) {
            var N = C.charAt(d);
            switch (N) {
            case "'":
                if (y) x.push("'");
                else K++;
                y = false;
                break;
            case "\\":
                y && x.push("\\");
                y = !y;
                break;
            default:
                x.push(N);
                y = false
            }
        }
        return K
    };
    ma = function (C, x) {
        x = x || "F";
        var K;
        K = C.patterns;
        var y = x.length;
        if (y === 1) {
            K = K[x];
            if (!K) throw "Invalid date format string '" + x + "'.";
            x = K
        } else if (y === 2 && x.charAt(0) === "%") x = x.charAt(1);
        return x
    };
    oa = function (C, x, K) {
        function y(sa, ta) {
            var Ba;
            Ba = sa + "";
            if (ta > 1 && Ba.length < ta) {
                Ba = S[ta - 2] + Ba;
                return Ba.substr(Ba.length - ta, ta)
            }
            return Ba
        }
        function d() {
            if (ga || la) return ga;
            ga = Ga.test(x);
            la = true;
            return ga
        }
        function Q(sa, ta) {
            if (da) return da[ta];
            switch (ta) {
            case 0:
                return sa.getFullYear();
            case 1:
                return sa.getMonth();
            case 2:
                return sa.getDate()
            }
        }
        var N = K.calendar,
            H = N.convert;
        if (!x || !x.length || x === "i") {
            if (K && K.name.length) if (H) K = oa(C, N.patterns.F, K);
            else {
                K = new Date(C.getTime());
                var O = pa(C, N.eras);
                K.setFullYear(Ma(C, N, O));
                K = K.toLocaleString()
            } else K = C.toString();
            return K
        }
        O = N.eras;
        var Y = x === "s";
        x = ma(N, x);
        K = [];
        var L, S = ["0", "00", "000"],
            ga, la, Ga = /([^d]|^)(d|dd)([^d]|$)/g,
            Na = 0,
            G = ya(),
            da;
        if (!Y && H) da = H.fromGregorian(C);
        for (;;) {
            L = G.lastIndex;
            H = G.exec(x);
            L = x.slice(L, H ? H.index : x.length);
            Na += M(L, K);
            if (!H) break;
            if (Na % 2) K.push(H[0]);
            else {
                L = H[0];
                H = L.length;
                switch (L) {
                case "ddd":
                case "dddd":
                    K.push((H === 3 ? N.days.namesAbbr : N.days.names)[C.getDay()]);
                    break;
                case "d":
                case "dd":
                    ga = true;
                    K.push(y(Q(C, 2), H));
                    break;
                case "MMM":
                case "MMMM":
                    L = Q(C, 1);
                    K.push(N.monthsGenitive && d() ? N.monthsGenitive[H === 3 ? "namesAbbr" : "names"][L] : N.months[H === 3 ? "namesAbbr" : "names"][L]);
                    break;
                case "M":
                case "MM":
                    K.push(y(Q(C, 1) + 1, H));
                    break;
                case "y":
                case "yy":
                case "yyyy":
                    L = da ? da[0] : Ma(C, N, pa(C, O), Y);
                    if (H < 4) L %= 100;
                    K.push(y(L, H));
                    break;
                case "h":
                case "hh":
                    L = C.getHours() % 12;
                    if (L === 0) L = 12;
                    K.push(y(L, H));
                    break;
                case "H":
                case "HH":
                    K.push(y(C.getHours(), H));
                    break;
                case "m":
                case "mm":
                    K.push(y(C.getMinutes(), H));
                    break;
                case "s":
                case "ss":
                    K.push(y(C.getSeconds(), H));
                    break;
                case "t":
                case "tt":
                    L = C.getHours() < 12 ? N.AM ? N.AM[0] : " " : N.PM ? N.PM[0] : " ";
                    K.push(H === 1 ? L.charAt(0) : L);
                    break;
                case "f":
                case "ff":
                case "fff":
                    K.push(y(C.getMilliseconds(), 3).substr(0, H));
                    break;
                case "z":
                case "zz":
                    L = C.getTimezoneOffset() / 60;
                    K.push((L <= 0 ? "+" : "-") + y(Math.floor(Math.abs(L)), H));
                    break;
                case "zzz":
                    L = C.getTimezoneOffset() / 60;
                    K.push((L <= 0 ? "+" : "-") + y(Math.floor(Math.abs(L)), 2) + ":" + y(Math.abs(C.getTimezoneOffset() % 60), 2));
                    break;
                case "g":
                case "gg":
                    N.eras && K.push(N.eras[pa(C, O)].name);
                    break;
                case "/":
                    K.push(N["/"]);
                    break;
                default:
                    throw "Invalid date format pattern '" + L + "'.";
                }
            }
        }
        return K.join("")
    };
    (function () {
        var C;
        C = function (x, K, y) {
            var d = y.groupSizes,
                Q = d[0],
                N = 1,
                H = Math.pow(10, K),
                O = Math.round(x * H) / H;
            isFinite(O) || (O = x);
            H = "";
            H = (O + "").split(/e/i);
            O = H.length > 1 ? parseInt(H[1], 10) : 0;
            x = H[0];
            H = x.split(".");
            x = H[0];
            H = H.length > 1 ? H[1] : "";
            if (O > 0) {
                H = E(H, O, false);
                x += H.slice(0, O);
                H = H.substr(O)
            } else if (O < 0) {
                O = -O;
                x = E(x, O + 1);
                H = x.slice(-O, x.length) + H;
                x = x.slice(0, -O)
            }
            H = K > 0 ? y["."] + (H.length > K ? H.slice(0, K) : E(H, K)) : "";
            K = x.length - 1;
            y = y[","];
            for (O = ""; K >= 0;) {
                if (Q === 0 || Q > K) return x.slice(0, K + 1) + (O.length ? y + O + H : H);
                O = x.slice(K - Q + 1, K + 1) + (O.length ? y + O : "");
                K -= Q;
                if (N < d.length) {
                    Q = d[N];
                    N++
                }
            }
            return x.slice(0, K + 1) + y + O + H
        };
        Da = function (x, K, y) {
            if (!K || K === "i") return y.name.length ? x.toLocaleString() : x.toString();
            K = K || "D";
            y = y.numberFormat;
            var d = Math.abs(x),
                Q = -1;
            if (K.length > 1) Q = parseInt(K.slice(1), 10);
            var N = K.charAt(0).toUpperCase(),
                H;
            switch (N) {
            case "D":
                K = "n";
                if (Q !== -1) d = E("" + d, Q, true);
                if (x < 0) d = -d;
                break;
            case "N":
                H = y;
            case "C":
                H = H || y.currency;
            case "P":
                H = H || y.percent;
                K = x < 0 ? H.pattern[0] : H.pattern[1] || "n";
                if (Q === -1) Q = H.decimals;
                d = C(d * (N === "P" ? 100 : 1), Q, H);
                break;
            default:
                throw "Bad number format specifier: " + N;
            }
            x = /n|\$|-|%/g;
            for (H = "";;) {
                Q = x.lastIndex;
                N = x.exec(K);
                H += K.slice(Q, N ? N.index : K.length);
                if (!N) break;
                switch (N[0]) {
                case "n":
                    H += d;
                    break;
                case "$":
                    H += y.currency.symbol;
                    break;
                case "-":
                    if (/[1-9]/.test(d)) H += y["-"];
                    break;
                case "%":
                    H += y.percent.symbol
                }
            }
            return H
        }
    })();
    ya = function () {
        return /\/|dddd|ddd|dd|d|MMMM|MMM|MM|M|yyyy|yy|y|hh|h|HH|H|mm|m|ss|s|tt|t|fff|ff|f|zzz|zz|z|gg|g/g
    };
    pa = function (C, x) {
        if (!x) return 0;
        for (var K, y = C.getTime(), d = 0, Q = x.length; d < Q; d++) {
            K = x[d].start;
            if (K === null || y >= K) return d
        }
        return 0
    };
    Ma = function (C, x, K, y) {
        C = C.getFullYear();
        if (!y && x.eras) C -= x.eras[K].offset;
        return C
    };
    (function () {
        var C, x, K, y, d, Q, N;
        C = function (H, O) {
            var Y = new Date,
                L = pa(Y);
            if (O < 100) {
                var S = H.twoDigitYearMax;
                S = typeof S === "string" ? (new Date).getFullYear() % 100 + parseInt(S, 10) : S;
                Y = Ma(Y, H, L);
                O += Y - Y % 100;
                if (O > S) O -= 100
            }
            return O
        };
        x = function (H, O, Y) {
            var L = H.days,
                S = H._upperDays;
            if (!S) H._upperDays = S = [N(L.names), N(L.namesAbbr), N(L.namesShort)];
            O = Q(O);
            if (Y) {
                H = c(S[1], O);
                if (H === -1) H = c(S[2], O)
            } else H = c(S[0], O);
            return H
        };
        K = function (H, O, Y) {
            var L = H.months,
                S = H.monthsGenitive || H.months,
                ga = H._upperMonths,
                la = H._upperMonthsGen;
            if (!ga) {
                H._upperMonths = ga = [N(L.names), N(L.namesAbbr)];
                H._upperMonthsGen = la = [N(S.names), N(S.namesAbbr)]
            }
            O = Q(O);
            H = c(Y ? ga[1] : ga[0], O);
            if (H < 0) H = c(Y ? la[1] : la[0], O);
            return H
        };
        y = function (H, O) {
            var Y = H._parseRegExp;
            if (Y) {
                var L = Y[O];
                if (L) return L
            } else H._parseRegExp = Y = {};
            L = ma(H, O).replace(/([\^\$\.\*\+\?\|\[\]\(\)\{\}])/g, "\\\\$1");
            for (var S = ["^"], ga = [], la = 0, Ga = 0, Na = ya(), G;
            (G = Na.exec(L)) !== null;) {
                var da = L.slice(la, G.index);
                la = Na.lastIndex;
                Ga += M(da, S);
                if (Ga % 2) S.push(G[0]);
                else {
                    da = G[0];
                    var sa = da.length;
                    switch (da) {
                    case "dddd":
                    case "ddd":
                    case "MMMM":
                    case "MMM":
                    case "gg":
                    case "g":
                        da = "(\\D+)";
                        break;
                    case "tt":
                    case "t":
                        da = "(\\D*)";
                        break;
                    case "yyyy":
                    case "fff":
                    case "ff":
                    case "f":
                        da = "(\\d{" + sa + "})";
                        break;
                    case "dd":
                    case "d":
                    case "MM":
                    case "M":
                    case "yy":
                    case "y":
                    case "HH":
                    case "H":
                    case "hh":
                    case "h":
                    case "mm":
                    case "m":
                    case "ss":
                    case "s":
                        da = "(\\d\\d?)";
                        break;
                    case "zzz":
                        da = "([+-]?\\d\\d?:\\d{2})";
                        break;
                    case "zz":
                    case "z":
                        da = "([+-]?\\d\\d?)";
                        break;
                    case "/":
                        da = "(\\" + H["/"] + ")";
                        break;
                    default:
                        throw "Invalid date format pattern '" + da + "'.";
                    }
                    da && S.push(da);
                    ga.push(G[0])
                }
            }
            M(L.slice(la), S);
            S.push("$");
            L = {
                regExp: S.join("").replace(/\s+/g, "\\s+"),
                groups: ga
            };
            return Y[O] = L
        };
        d = function (H, O, Y) {
            return H < O || H > Y
        };
        Q = function (H) {
            return H.split("\u00a0").join(" ").toUpperCase()
        };
        N = function (H) {
            for (var O = [], Y = 0, L = H.length; Y < L; Y++) O[Y] = Q(H[Y]);
            return O
        };
        Z = function (H, O, Y) {
            H = z(H);
            Y = Y.calendar;
            O = y(Y, O);
            var L = RegExp(O.regExp).exec(H);
            if (L === null) return null;
            var S = O.groups;
            var ga = O = H = null,
                la = null,
                Ga = null,
                Na = 0,
                G, da = 0,
                sa = 0,
                ta = 0;
            G = null;
            for (var Ba = false, W = 0, na = S.length; W < na; W++) {
                var ua = L[W + 1];
                if (ua) {
                    var va = S[W],
                        J = va.length,
                        fa = parseInt(ua, 10);
                    switch (va) {
                    case "dd":
                    case "d":
                        la = fa;
                        if (d(la, 1, 31)) return null;
                        break;
                    case "MMM":
                    case "MMMM":
                        ga = K(Y, ua, J === 3);
                        if (d(ga, 0, 11)) return null;
                        break;
                    case "M":
                    case "MM":
                        ga = fa - 1;
                        if (d(ga, 0, 11)) return null;
                        break;
                    case "y":
                    case "yy":
                    case "yyyy":
                        O = J < 4 ? C(Y, fa) : fa;
                        if (d(O, 0, 9999)) return null;
                        break;
                    case "h":
                    case "hh":
                        Na = fa;
                        if (Na === 12) Na = 0;
                        if (d(Na, 0, 11)) return null;
                        break;
                    case "H":
                    case "HH":
                        Na = fa;
                        if (d(Na, 0, 23)) return null;
                        break;
                    case "m":
                    case "mm":
                        da = fa;
                        if (d(da, 0, 59)) return null;
                        break;
                    case "s":
                    case "ss":
                        sa = fa;
                        if (d(sa, 0, 59)) return null;
                        break;
                    case "tt":
                    case "t":
                        Ba = Y.PM && (ua === Y.PM[0] || ua === Y.PM[1] || ua === Y.PM[2]);
                        if (!Ba && (!Y.AM || ua !== Y.AM[0] && ua !== Y.AM[1] && ua !== Y.AM[2])) return null;
                        break;
                    case "f":
                    case "ff":
                    case "fff":
                        ta = fa * Math.pow(10, 3 - J);
                        if (d(ta, 0, 999)) return null;
                        break;
                    case "ddd":
                    case "dddd":
                        Ga = x(Y, ua, J === 3);
                        if (d(Ga, 0, 6)) return null;
                        break;
                    case "zzz":
                        va = ua.split(/:/);
                        if (va.length !== 2) return null;
                        G = parseInt(va[0], 10);
                        if (d(G, -12, 13)) return null;
                        va = parseInt(va[1], 10);
                        if (d(va, 0, 59)) return null;
                        G = G * 60 + (u(ua, "-") ? -va : va);
                        break;
                    case "z":
                    case "zz":
                        G = fa;
                        if (d(G, -12, 13)) return null;
                        G *= 60;
                        break;
                    case "g":
                    case "gg":
                        if (!ua || !Y.eras) return null;
                        ua = z(ua.toLowerCase());
                        va = 0;
                        for (J = Y.eras.length; va < J; va++) if (ua === Y.eras[va].name.toLowerCase()) {
                            H = va;
                            break
                        }
                        if (H === null) return null
                    }
                }
            }
            L = new Date;
            S = (W = Y.convert) ? W.fromGregorian(L)[0] : L.getFullYear();
            if (O === null) O = S;
            else if (Y.eras) O += Y.eras[H || 0].offset;
            if (ga === null) ga = 0;
            if (la === null) la = 1;
            if (W) {
                L = W.toGregorian(O, ga, la);
                if (L === null) return null
            } else {
                L.setFullYear(O, ga, la);
                if (L.getDate() !== la) return null;
                if (Ga !== null && L.getDay() !== Ga) return null
            }
            if (Ba && Na < 12) Na += 12;
            L.setHours(Na, da, sa, ta);
            if (G !== null) {
                Y = L.getMinutes() - (G + L.getTimezoneOffset());
                L.setHours(L.getHours() + parseInt(Y / 60, 10), Y % 60)
            }
            return L
        }
    })();
    ja = function (C, x, K) {
        var y = x["-"];
        x = x["+"];
        var d;
        switch (K) {
        case "n -":
            y = " " + y;
            x = " " + x;
        case "n-":
            if (k(C, y)) d = ["-", C.substr(0, C.length - y.length)];
            else if (k(C, x)) d = ["+", C.substr(0, C.length - x.length)];
            break;
        case "- n":
            y += " ";
            x += " ";
        case "-n":
            if (u(C, y)) d = ["-", C.substr(y.length)];
            else if (u(C, x)) d = ["+", C.substr(x.length)];
            break;
        case "(n)":
            if (u(C, "(") && k(C, ")")) d = ["-", C.substr(1, C.length - 2)]
        }
        return d || ["", C]
    };
    i.prototype.findClosestCulture = function (C) {
        return i.findClosestCulture.call(this, C)
    };
    i.prototype.format = function (C, x, K) {
        return i.format.call(this, C, x, K)
    };
    i.prototype.localize = function (C, x) {
        return i.localize.call(this, C, x)
    };
    i.prototype.parseInt = function (C, x, K) {
        return i.parseInt.call(this, C, x, K)
    };
    i.prototype.parseFloat = function (C, x, K) {
        return i.parseFloat.call(this, C, x, K)
    };
    i.prototype.culture = function (C) {
        return i.culture.call(this, C)
    };
    i.addCultureInfo = function (C, x, K) {
        var y = {},
            d = false;
        if (typeof C !== "string") {
            K = C;
            C = this.culture().name;
            y = this.cultures[C]
        } else if (typeof x !== "string") {
            K = x;
            d = this.cultures[C] == null;
            y = this.cultures[C] || this.cultures["default"]
        } else {
            d = true;
            y = this.cultures[x]
        }
        this.cultures[C] = q(true, {}, y, K);
        if (d) this.cultures[C].calendar = this.cultures[C].calendars.standard
    };
    i.findClosestCulture = function (C) {
        var x;
        if (!C) return this.cultures[this.cultureSelector] || this.cultures["default"];
        if (typeof C === "string") C = C.split(",");
        if (s(C)) {
            var K, y = this.cultures,
                d = C,
                Q, N = d.length,
                H = [];
            for (Q = 0; Q < N; Q++) {
                C = z(d[Q]);
                C = C.split(";");
                K = z(C[0]);
                if (C.length === 1) C = 1;
                else {
                    C = z(C[1]);
                    if (C.indexOf("q=") === 0) {
                        C = C.substr(2);
                        C = parseFloat(C);
                        C = isNaN(C) ? 0 : C
                    } else C = 1
                }
                H.push({
                    lang: K,
                    pri: C
                })
            }
            H.sort(function (Y, L) {
                return Y.pri < L.pri ? 1 : -1
            });
            for (Q = 0; Q < N; Q++) {
                K = H[Q].lang;
                if (x = y[K]) return x
            }
            for (Q = 0; Q < N; Q++) {
                K = H[Q].lang;
                do {
                    d = K.lastIndexOf("-");
                    if (d === -1) break;
                    K = K.substr(0, d);
                    if (x = y[K]) return x
                } while (1)
            }
            for (Q = 0; Q < N; Q++) {
                K = H[Q].lang;
                for (var O in y) {
                    d = y[O];
                    if (d.language == K) return d
                }
            }
        } else if (typeof C === "object") return C;
        return x || null
    };
    i.format = function (C, x, K) {
        culture = this.findClosestCulture(K);
        if (C instanceof Date) C = oa(C, x, culture);
        else if (typeof C === "number") C = Da(C, x, culture);
        return C
    };
    i.localize = function (C, x) {
        return this.findClosestCulture(x).messages[C] || this.cultures["default"].messages.key
    };
    i.parseDate = function (C, x, K) {
        K = this.findClosestCulture(K);
        var y, d;
        if (x) {
            if (typeof x === "string") x = [x];
            if (x.length) {
                d = 0;
                for (var Q = x.length; d < Q; d++) {
                    var N = x[d];
                    if (N) if (y = Z(C, N, K)) break
                }
            }
        } else {
            x = K.calendar.patterns;
            for (d in x) if (y = Z(C, x[d], K)) break
        }
        return y || null
    };
    i.parseInt = function (C, x, K) {
        return Math.floor(i.parseFloat(C, x, K))
    };
    i.parseFloat = function (C, x, K) {
        if (typeof x !== "number") {
            K = x;
            x = 10
        }
        var y = this.findClosestCulture(K);
        K = NaN;
        var d = y.numberFormat;
        if (C.indexOf(y.numberFormat.currency.symbol) > -1) {
            C = C.replace(y.numberFormat.currency.symbol, "");
            C = C.replace(y.numberFormat.currency["."], y.numberFormat["."])
        }
        C = z(C);
        if (h.test(C)) K = parseFloat(C);
        else if (!x && g.test(C)) K = parseInt(C, 16);
        else {
            y = ja(C, d, d.pattern[0]);
            x = y[0];
            y = y[1];
            if (x === "" && d.pattern[0] !== "-n") {
                y = ja(C, d, "-n");
                x = y[0];
                y = y[1]
            }
            x = x || "+";
            var Q;
            C = y.indexOf("e");
            if (C < 0) C = y.indexOf("E");
            if (C < 0) {
                Q = y;
                C = null
            } else {
                Q = y.substr(0, C);
                C = y.substr(C + 1)
            }
            var N = d["."],
                H = Q.indexOf(N);
            if (H < 0) {
                y = Q;
                Q = null
            } else {
                y = Q.substr(0, H);
                Q = Q.substr(H + N.length)
            }
            N = d[","];
            y = y.split(N).join("");
            H = N.replace(/\u00A0/g, " ");
            if (N !== H) y = y.split(H).join("");
            x += y;
            if (Q !== null) x += "." + Q;
            if (C !== null) {
                d = ja(C, d, "-n");
                x += "e" + (d[0] || "+") + d[1]
            }
            if (j.test(x)) K = parseFloat(x)
        }
        return K
    };
    i.culture = function (C) {
        if (typeof C !== "undefined") this.cultureSelector = C;
        return this.findClosestCulture(C) || this.culture["default"]
    }
})(this);
(function (a, f) {
    function i(h, j) {
        var l = h.nodeName.toLowerCase();
        if ("area" === l) {
            l = h.parentNode;
            var c = l.name;
            if (!h.href || !c || l.nodeName.toLowerCase() !== "map") return false;
            l = a("img[usemap=#" + c + "]")[0];
            return !!l && g(l)
        }
        return (/input|select|textarea|button|object/.test(l) ? !h.disabled : "a" == l ? h.href || j : j) && g(h)
    }
    function g(h) {
        return !a(h).parents().andSelf().filter(function () {
            return a.curCSS(this, "visibility") === "hidden" || a.expr.filters.hidden(this)
        }).length
    }
    a.ui = a.ui || {};
    if (!a.ui.version) {
        a.extend(a.ui, {
            version: "1.8.18",
            keyCode: {
                ALT: 18,
                BACKSPACE: 8,
                CAPS_LOCK: 20,
                COMMA: 188,
                COMMAND: 91,
                COMMAND_LEFT: 91,
                COMMAND_RIGHT: 93,
                CONTROL: 17,
                DELETE: 46,
                DOWN: 40,
                END: 35,
                ENTER: 13,
                ESCAPE: 27,
                HOME: 36,
                INSERT: 45,
                LEFT: 37,
                MENU: 93,
                NUMPAD_ADD: 107,
                NUMPAD_DECIMAL: 110,
                NUMPAD_DIVIDE: 111,
                NUMPAD_ENTER: 108,
                NUMPAD_MULTIPLY: 106,
                NUMPAD_SUBTRACT: 109,
                PAGE_DOWN: 34,
                PAGE_UP: 33,
                PERIOD: 190,
                RIGHT: 39,
                SHIFT: 16,
                SPACE: 32,
                TAB: 9,
                UP: 38,
                WINDOWS: 91
            }
        });
        a.fn.extend({
            propAttr: a.fn.prop || a.fn.attr,
            _focus: a.fn.focus,
            focus: function (h, j) {
                return typeof h === "number" ? this.each(function () {
                    var l = this;
                    setTimeout(function () {
                        a(l).focus();
                        j && j.call(l)
                    }, h)
                }) : this._focus.apply(this, arguments)
            },
            scrollParent: function () {
                var h;
                h = a.browser.msie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
                    return /(relative|absolute|fixed)/.test(a.curCSS(this, "position", 1)) && /(auto|scroll)/.test(a.curCSS(this, "overflow", 1) + a.curCSS(this, "overflow-y", 1) + a.curCSS(this, "overflow-x", 1))
                }).eq(0) : this.parents().filter(function () {
                    return /(auto|scroll)/.test(a.curCSS(this, "overflow", 1) + a.curCSS(this, "overflow-y", 1) + a.curCSS(this, "overflow-x", 1))
                }).eq(0);
                return /fixed/.test(this.css("position")) || !h.length ? a(document) : h
            },
            zIndex: function (h) {
                if (h !== f) return this.css("zIndex", h);
                if (this.length) {
                    h = a(this[0]);
                    for (var j; h.length && h[0] !== document;) {
                        j = h.css("position");
                        if (j === "absolute" || j === "relative" || j === "fixed") {
                            j = parseInt(h.css("zIndex"), 10);
                            if (!isNaN(j) && j !== 0) return j
                        }
                        h = h.parent()
                    }
                }
                return 0
            },
            disableSelection: function () {
                return this.bind((a.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (h) {
                    h.preventDefault()
                })
            },
            enableSelection: function () {
                return this.unbind(".ui-disableSelection")
            }
        });
        a.each(["Width", "Height"], function (h, j) {
            function l(s, p, n, u) {
                a.each(c, function () {
                    p -= parseFloat(a.curCSS(s, "padding" + this, true)) || 0;
                    if (n) p -= parseFloat(a.curCSS(s, "border" + this + "Width", true)) || 0;
                    if (u) p -= parseFloat(a.curCSS(s, "margin" + this, true)) || 0
                });
                return p
            }
            var c = j === "Width" ? ["Left", "Right"] : ["Top", "Bottom"],
                k = j.toLowerCase(),
                q = {
                    innerWidth: a.fn.innerWidth,
                    innerHeight: a.fn.innerHeight,
                    outerWidth: a.fn.outerWidth,
                    outerHeight: a.fn.outerHeight
                };
            a.fn["inner" + j] = function (s) {
                if (s === f) return q["inner" + j].call(this);
                return this.each(function () {
                    a(this).css(k, l(this, s) + "px")
                })
            };
            a.fn["outer" + j] = function (s, p) {
                if (typeof s !== "number") return q["outer" + j].call(this, s);
                return this.each(function () {
                    a(this).css(k, l(this, s, true, p) + "px")
                })
            }
        });
        a.extend(a.expr[":"], {
            data: function (h, j, l) {
                return !!a.data(h, l[3])
            },
            focusable: function (h) {
                return i(h, !isNaN(a.attr(h, "tabindex")))
            },
            tabbable: function (h) {
                var j = a.attr(h, "tabindex"),
                    l = isNaN(j);
                return (l || j >= 0) && i(h, !l)
            }
        });
        a(function () {
            var h = document.body,
                j = h.appendChild(j = document.createElement("div"));
            a.extend(j.style, {
                minHeight: "100px",
                height: "auto",
                padding: 0,
                borderWidth: 0
            });
            a.support.minHeight = j.offsetHeight === 100;
            a.support.selectstart = "onselectstart" in j;
            h.removeChild(j).style.display = "none"
        });
        a.extend(a.ui, {
            plugin: {
                add: function (h, j, l) {
                    h = a.ui[h].prototype;
                    for (var c in l) {
                        h.plugins[c] = h.plugins[c] || [];
                        h.plugins[c].push([j, l[c]])
                    }
                },
                call: function (h, j, l) {
                    if ((j = h.plugins[j]) && h.element[0].parentNode) for (var c = 0; c < j.length; c++) h.options[j[c][0]] && j[c][1].apply(h.element, l)
                }
            },
            contains: function (h, j) {
                return document.compareDocumentPosition ? h.compareDocumentPosition(j) & 16 : h !== j && h.contains(j)
            },
            hasScroll: function (h, j) {
                if (a(h).css("overflow") === "hidden") return false;
                var l = j && j === "left" ? "scrollLeft" : "scrollTop",
                    c = false;
                if (h[l] > 0) return true;
                h[l] = 1;
                c = h[l] > 0;
                h[l] = 0;
                return c
            },
            isOverAxis: function (h, j, l) {
                return h > j && h < j + l
            },
            isOver: function (h, j, l, c, k, q) {
                return a.ui.isOverAxis(h, l, k) && a.ui.isOverAxis(j, c, q)
            }
        })
    }
})(jQuery);
(function (a, f) {
    if (a.cleanData) {
        var i = a.cleanData;
        a.cleanData = function (h) {
            for (var j = 0, l;
            (l = h[j]) != null; j++) try {
                a(l).triggerHandler("remove")
            } catch (c) {}
            i(h)
        }
    } else {
        var g = a.fn.remove;
        a.fn.remove = function (h, j) {
            return this.each(function () {
                if (!j) if (!h || a.filter(h, [this]).length) a("*", this).add([this]).each(function () {
                    try {
                        a(this).triggerHandler("remove")
                    } catch (l) {}
                });
                return g.call(a(this), h, j)
            })
        }
    }
    a.widget = function (h, j, l) {
        var c = h.split(".")[0],
            k;
        h = h.split(".")[1];
        k = c + "-" + h;
        if (!l) {
            l = j;
            j = a.Widget
        }
        a.expr[":"][k] = function (q) {
            return !!a.data(q, h)
        };
        a[c] = a[c] || {};
        a[c][h] = function (q, s) {
            arguments.length && this._createWidget(q, s)
        };
        j = new j;
        j.options = a.extend(true, {}, j.options);
        a[c][h].prototype = a.extend(true, j, {
            namespace: c,
            widgetName: h,
            widgetEventPrefix: a[c][h].prototype.widgetEventPrefix || h,
            widgetBaseClass: k
        }, l);
        a.widget.bridge(h, a[c][h])
    };
    a.widget.bridge = function (h, j) {
        a.fn[h] = function (l) {
            var c = typeof l === "string",
                k = Array.prototype.slice.call(arguments, 1),
                q = this;
            l = !c && k.length ? a.extend.apply(null, [true, l].concat(k)) : l;
            if (c && l.charAt(0) === "_") return q;
            c ? this.each(function () {
                var s = a.data(this, h),
                    p = s && a.isFunction(s[l]) ? s[l].apply(s, k) : s;
                if (p !== s && p !== f) {
                    q = p;
                    return false
                }
            }) : this.each(function () {
                var s = a.data(this, h);
                s ? s.option(l || {})._init() : a.data(this, h, new j(l, this))
            });
            return q
        }
    };
    a.Widget = function (h, j) {
        arguments.length && this._createWidget(h, j)
    };
    a.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        options: {
            disabled: false
        },
        _createWidget: function (h, j) {
            a.data(j, this.widgetName, this);
            this.element = a(j);
            this.options = a.extend(true, {}, this.options, this._getCreateOptions(), h);
            var l = this;
            this.element.bind("remove." + this.widgetName, function () {
                l.destroy()
            });
            this._create();
            this._trigger("create");
            this._init()
        },
        _getCreateOptions: function () {
            return a.metadata && a.metadata.get(this.element[0])[this.widgetName]
        },
        _create: function () {},
        _init: function () {},
        destroy: function () {
            this.element.unbind("." + this.widgetName).removeData(this.widgetName);
            this.widget().unbind("." + this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass + "-disabled ui-state-disabled")
        },
        widget: function () {
            return this.element
        },
        option: function (h, j) {
            var l = h;
            if (arguments.length === 0) return a.extend({}, this.options);
            if (typeof h === "string") {
                if (j === f) return this.options[h];
                l = {};
                l[h] = j
            }
            this._setOptions(l);
            return this
        },
        _setOptions: function (h) {
            var j = this;
            a.each(h, function (l, c) {
                j._setOption(l, c)
            });
            return this
        },
        _setOption: function (h, j) {
            this.options[h] = j;
            if (h === "disabled") this.widget()[j ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled ui-state-disabled").attr("aria-disabled", j);
            return this
        },
        enable: function () {
            return this._setOption("disabled", false)
        },
        disable: function () {
            return this._setOption("disabled", true)
        },
        _trigger: function (h, j, l) {
            var c, k = this.options[h];
            l = l || {};
            j = a.Event(j);
            j.type = (h === this.widgetEventPrefix ? h : this.widgetEventPrefix + h).toLowerCase();
            j.target = this.element[0];
            if (h = j.originalEvent) for (c in h) c in j || (j[c] = h[c]);
            this.element.trigger(j, l);
            return !(a.isFunction(k) && k.call(this.element[0], j, l) === false || j.isDefaultPrevented())
        }
    }
})(jQuery);
(function (a) {
    var f = false;
    a(document).mouseup(function () {
        f = false
    });
    a.widget("ui.mouse", {
        options: {
            cancel: ":input,option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function () {
            var i = this;
            this.element.bind("mousedown." + this.widgetName, function (g) {
                return i._mouseDown(g)
            }).bind("click." + this.widgetName, function (g) {
                if (true === a.data(g.target, i.widgetName + ".preventClickEvent")) {
                    a.removeData(g.target, i.widgetName + ".preventClickEvent");
                    g.stopImmediatePropagation();
                    return false
                }
            });
            this.started = false
        },
        _mouseDestroy: function () {
            this.element.unbind("." + this.widgetName)
        },
        _mouseDown: function (i) {
            if (!f) {
                this._mouseStarted && this._mouseUp(i);
                this._mouseDownEvent = i;
                var g = this,
                    h = i.which == 1,
                    j = typeof this.options.cancel == "string" && i.target.nodeName ? a(i.target).closest(this.options.cancel).length : false;
                if (!h || j || !this._mouseCapture(i)) return true;
                this.mouseDelayMet = !this.options.delay;
                if (!this.mouseDelayMet) this._mouseDelayTimer = setTimeout(function () {
                    g.mouseDelayMet = true
                }, this.options.delay);
                if (this._mouseDistanceMet(i) && this._mouseDelayMet(i)) {
                    this._mouseStarted = this._mouseStart(i) !== false;
                    if (!this._mouseStarted) {
                        i.preventDefault();
                        return true
                    }
                }
                true === a.data(i.target, this.widgetName + ".preventClickEvent") && a.removeData(i.target, this.widgetName + ".preventClickEvent");
                this._mouseMoveDelegate = function (l) {
                    return g._mouseMove(l)
                };
                this._mouseUpDelegate = function (l) {
                    return g._mouseUp(l)
                };
                a(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate);
                i.preventDefault();
                return f = true
            }
        },
        _mouseMove: function (i) {
            if (a.browser.msie && !(document.documentMode >= 9) && !i.button) return this._mouseUp(i);
            if (this._mouseStarted) {
                this._mouseDrag(i);
                return i.preventDefault()
            }
            if (this._mouseDistanceMet(i) && this._mouseDelayMet(i))(this._mouseStarted = this._mouseStart(this._mouseDownEvent, i) !== false) ? this._mouseDrag(i) : this._mouseUp(i);
            return !this._mouseStarted
        },
        _mouseUp: function (i) {
            a(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
            if (this._mouseStarted) {
                this._mouseStarted = false;
                i.target == this._mouseDownEvent.target && a.data(i.target, this.widgetName + ".preventClickEvent", true);
                this._mouseStop(i)
            }
            return false
        },
        _mouseDistanceMet: function (i) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - i.pageX), Math.abs(this._mouseDownEvent.pageY - i.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function () {
            return this.mouseDelayMet
        },
        _mouseStart: function () {},
        _mouseDrag: function () {},
        _mouseStop: function () {},
        _mouseCapture: function () {
            return true
        }
    })
})(jQuery);
(function (a) {
    a.ui = a.ui || {};
    var f = /left|center|right/,
        i = /top|center|bottom/,
        g = {},
        h = a.fn.position,
        j = a.fn.offset;
    a.fn.position = function (l) {
        if (!l || !l.of) return h.apply(this, arguments);
        l = a.extend({}, l);
        var c = a(l.of),
            k = c[0],
            q = (l.collision || "flip").split(" "),
            s = l.offset ? l.offset.split(" ") : [0, 0],
            p, n, u;
        if (k.nodeType === 9) {
            p = c.width();
            n = c.height();
            u = {
                top: 0,
                left: 0
            }
        } else if (k.setTimeout) {
            p = c.width();
            n = c.height();
            u = {
                top: c.scrollTop(),
                left: c.scrollLeft()
            }
        } else if (k.preventDefault) {
            l.at = "left top";
            p = n = 0;
            u = {
                top: l.of.pageY,
                left: l.of.pageX
            }
        } else {
            p = c.outerWidth();
            n = c.outerHeight();
            u = c.offset()
        }
        a.each(["my", "at"], function () {
            var z = (l[this] || "").split(" ");
            if (z.length === 1) z = f.test(z[0]) ? z.concat(["center"]) : i.test(z[0]) ? ["center"].concat(z) : ["center", "center"];
            z[0] = f.test(z[0]) ? z[0] : "center";
            z[1] = i.test(z[1]) ? z[1] : "center";
            l[this] = z
        });
        if (q.length === 1) q[1] = q[0];
        s[0] = parseInt(s[0], 10) || 0;
        if (s.length === 1) s[1] = s[0];
        s[1] = parseInt(s[1], 10) || 0;
        if (l.at[0] === "right") u.left += p;
        else if (l.at[0] === "center") u.left += p / 2;
        if (l.at[1] === "bottom") u.top += n;
        else if (l.at[1] === "center") u.top += n / 2;
        u.left += s[0];
        u.top += s[1];
        return this.each(function () {
            var z = a(this),
                E = z.outerWidth(),
                M = z.outerHeight(),
                ma = parseInt(a.curCSS(this, "marginLeft", true)) || 0,
                oa = parseInt(a.curCSS(this, "marginTop", true)) || 0,
                Da = E + ma + (parseInt(a.curCSS(this, "marginRight", true)) || 0),
                ya = M + oa + (parseInt(a.curCSS(this, "marginBottom", true)) || 0),
                pa = a.extend({}, u),
                Ma;
            if (l.my[0] === "right") pa.left -= E;
            else if (l.my[0] === "center") pa.left -= E / 2;
            if (l.my[1] === "bottom") pa.top -= M;
            else if (l.my[1] === "center") pa.top -= M / 2;
            if (!g.fractions) {
                pa.left = Math.round(pa.left);
                pa.top = Math.round(pa.top)
            }
            Ma = {
                left: pa.left - ma,
                top: pa.top - oa
            };
            a.each(["left", "top"], function (Z, ja) {
                if (a.ui.position[q[Z]]) a.ui.position[q[Z]][ja](pa, {
                    targetWidth: p,
                    targetHeight: n,
                    elemWidth: E,
                    elemHeight: M,
                    collisionPosition: Ma,
                    collisionWidth: Da,
                    collisionHeight: ya,
                    offset: s,
                    my: l.my,
                    at: l.at
                })
            });
            a.fn.bgiframe && z.bgiframe();
            z.offset(a.extend(pa, {
                using: l.using
            }))
        })
    };
    a.ui.position = {
        fit: {
            left: function (l, c) {
                var k = a(window);
                k = c.collisionPosition.left + c.collisionWidth - k.width() - k.scrollLeft();
                l.left = k > 0 ? l.left - k : Math.max(l.left - c.collisionPosition.left, l.left)
            },
            top: function (l, c) {
                var k = a(window);
                k = c.collisionPosition.top + c.collisionHeight - k.height() - k.scrollTop();
                l.top = k > 0 ? l.top - k : Math.max(l.top - c.collisionPosition.top, l.top)
            }
        },
        flip: {
            left: function (l, c) {
                if (c.at[0] !== "center") {
                    var k = a(window);
                    k = c.collisionPosition.left + c.collisionWidth - k.width() - k.scrollLeft();
                    var q = c.my[0] === "left" ? -c.elemWidth : c.my[0] === "right" ? c.elemWidth : 0,
                        s = c.at[0] === "left" ? c.targetWidth : -c.targetWidth,
                        p = -2 * c.offset[0];
                    l.left += c.collisionPosition.left < 0 ? q + s + p : k > 0 ? q + s + p : 0
                }
            },
            top: function (l, c) {
                if (c.at[1] !== "center") {
                    var k = a(window);
                    k = c.collisionPosition.top + c.collisionHeight - k.height() - k.scrollTop();
                    var q = c.my[1] === "top" ? -c.elemHeight : c.my[1] === "bottom" ? c.elemHeight : 0,
                        s = c.at[1] === "top" ? c.targetHeight : -c.targetHeight,
                        p = -2 * c.offset[1];
                    l.top += c.collisionPosition.top < 0 ? q + s + p : k > 0 ? q + s + p : 0
                }
            }
        }
    };
    if (!a.offset.setOffset) {
        a.offset.setOffset = function (l, c) {
            if (/static/.test(a.curCSS(l, "position"))) l.style.position = "relative";
            var k = a(l),
                q = k.offset(),
                s = parseInt(a.curCSS(l, "top", true), 10) || 0,
                p = parseInt(a.curCSS(l, "left", true), 10) || 0;
            q = {
                top: c.top - q.top + s,
                left: c.left - q.left + p
            };
            "using" in c ? c.using.call(l, q) : k.css(q)
        };
        a.fn.offset = function (l) {
            var c = this[0];
            if (!c || !c.ownerDocument) return null;
            if (l) return this.each(function () {
                a.offset.setOffset(this, l)
            });
            return j.call(this)
        }
    }(function () {
        var l = document.getElementsByTagName("body")[0],
            c = document.createElement("div"),
            k, q;
        k = document.createElement(l ? "div" : "body");
        q = {
            visibility: "hidden",
            width: 0,
            height: 0,
            border: 0,
            margin: 0,
            background: "none"
        };
        l && a.extend(q, {
            position: "absolute",
            left: "-1000px",
            top: "-1000px"
        });
        for (var s in q) k.style[s] = q[s];
        k.appendChild(c);
        q = l || document.documentElement;
        q.insertBefore(k, q.firstChild);
        c.style.cssText = "position: absolute; left: 10.7432222px; top: 10.432325px; height: 30px; width: 201px;";
        c = a(c).offset(function (p, n) {
            return n
        }).offset();
        k.innerHTML = "";
        q.removeChild(k);
        l = c.top + c.left + (l ? 2E3 : 0);
        g.fractions = l > 21 && l < 22
    })()
})(jQuery);
(function (a) {
    a.widget("ui.draggable", a.ui.mouse, {
        widgetEventPrefix: "drag",
        options: {
            addClasses: true,
            appendTo: "parent",
            axis: false,
            connectToSortable: false,
            containment: false,
            cursor: "auto",
            cursorAt: false,
            grid: false,
            handle: false,
            helper: "original",
            iframeFix: false,
            opacity: false,
            refreshPositions: false,
            revert: false,
            revertDuration: 500,
            scope: "default",
            scroll: true,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: false,
            snapMode: "both",
            snapTolerance: 20,
            stack: false,
            zIndex: false
        },
        _create: function () {
            if (this.options.helper == "original" && !/^(?:r|a|f)/.test(this.element.css("position"))) this.element[0].style.position = "relative";
            this.options.addClasses && this.element.addClass("ui-draggable");
            this.options.disabled && this.element.addClass("ui-draggable-disabled");
            this._mouseInit()
        },
        destroy: function () {
            if (this.element.data("draggable")) {
                this.element.removeData("draggable").unbind(".draggable").removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");
                this._mouseDestroy();
                return this
            }
        },
        _mouseCapture: function (f) {
            var i = this.options;
            if (this.helper || i.disabled || a(f.target).is(".ui-resizable-handle")) return false;
            this.handle = this._getHandle(f);
            if (!this.handle) return false;
            if (i.iframeFix) a(i.iframeFix === true ? "iframe" : i.iframeFix).each(function () {
                a('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({
                    width: this.offsetWidth + "px",
                    height: this.offsetHeight + "px",
                    position: "absolute",
                    opacity: "0.001",
                    zIndex: 1E3
                }).css(a(this).offset()).appendTo("body")
            });
            return true
        },
        _mouseStart: function (f) {
            var i = this.options;
            this.helper = this._createHelper(f);
            this._cacheHelperProportions();
            if (a.ui.ddmanager) a.ui.ddmanager.current = this;
            this._cacheMargins();
            this.cssPosition = this.helper.css("position");
            this.scrollParent = this.helper.scrollParent();
            this.offset = this.positionAbs = this.element.offset();
            this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            };
            a.extend(this.offset, {
                click: {
                    left: f.pageX - this.offset.left,
                    top: f.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            });
            this.originalPosition = this.position = this._generatePosition(f);
            this.originalPageX = f.pageX;
            this.originalPageY = f.pageY;
            i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt);
            i.containment && this._setContainment();
            if (this._trigger("start", f) === false) {
                this._clear();
                return false
            }
            this._cacheHelperProportions();
            a.ui.ddmanager && !i.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, f);
            this.helper.addClass("ui-draggable-dragging");
            this._mouseDrag(f, true);
            a.ui.ddmanager && a.ui.ddmanager.dragStart(this, f);
            return true
        },
        _mouseDrag: function (f, i) {
            this.position = this._generatePosition(f);
            this.positionAbs = this._convertPositionTo("absolute");
            if (!i) {
                var g = this._uiHash();
                if (this._trigger("drag", f, g) === false) {
                    this._mouseUp({});
                    return false
                }
                this.position = g.position
            }
            if (!this.options.axis || this.options.axis != "y") this.helper[0].style.left = this.position.left + "px";
            if (!this.options.axis || this.options.axis != "x") this.helper[0].style.top = this.position.top + "px";
            a.ui.ddmanager && a.ui.ddmanager.drag(this, f);
            return false
        },
        _mouseStop: function (f) {
            var i = false;
            if (a.ui.ddmanager && !this.options.dropBehaviour) i = a.ui.ddmanager.drop(this, f);
            if (this.dropped) {
                i = this.dropped;
                this.dropped = false
            }
            if ((!this.element[0] || !this.element[0].parentNode) && this.options.helper == "original") return false;
            if (this.options.revert == "invalid" && !i || this.options.revert == "valid" && i || this.options.revert === true || a.isFunction(this.options.revert) && this.options.revert.call(this.element, i)) {
                var g = this;
                a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
                    g._trigger("stop", f) !== false && g._clear()
                })
            } else this._trigger("stop", f) !== false && this._clear();
            return false
        },
        _mouseUp: function (f) {
            this.options.iframeFix === true && a("div.ui-draggable-iframeFix").each(function () {
                this.parentNode.removeChild(this)
            });
            a.ui.ddmanager && a.ui.ddmanager.dragStop(this, f);
            return a.ui.mouse.prototype._mouseUp.call(this, f)
        },
        cancel: function () {
            this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear();
            return this
        },
        _getHandle: function (f) {
            var i = !this.options.handle || !a(this.options.handle, this.element).length ? true : false;
            a(this.options.handle, this.element).find("*").andSelf().each(function () {
                if (this == f.target) i = true
            });
            return i
        },
        _createHelper: function (f) {
            var i = this.options;
            f = a.isFunction(i.helper) ? a(i.helper.apply(this.element[0], [f])) : i.helper == "clone" ? this.element.clone().removeAttr("id") : this.element;
            f.parents("body").length || f.appendTo(i.appendTo == "parent" ? this.element[0].parentNode : i.appendTo);
            f[0] != this.element[0] && !/(fixed|absolute)/.test(f.css("position")) && f.css("position", "absolute");
            return f
        },
        _adjustOffsetFromHelper: function (f) {
            if (typeof f == "string") f = f.split(" ");
            if (a.isArray(f)) f = {
                left: +f[0],
                top: +f[1] || 0
            };
            if ("left" in f) this.offset.click.left = f.left + this.margins.left;
            if ("right" in f) this.offset.click.left = this.helperProportions.width - f.right + this.margins.left;
            if ("top" in f) this.offset.click.top = f.top + this.margins.top;
            if ("bottom" in f) this.offset.click.top = this.helperProportions.height - f.bottom + this.margins.top
        },
        _getParentOffset: function () {
            this.offsetParent = this.helper.offsetParent();
            var f = this.offsetParent.offset();
            if (this.cssPosition == "absolute" && this.scrollParent[0] != document && a.ui.contains(this.scrollParent[0], this.offsetParent[0])) {
                f.left += this.scrollParent.scrollLeft();
                f.top += this.scrollParent.scrollTop()
            }
            if (this.offsetParent[0] == document.body || this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() == "html" && a.browser.msie) f = {
                top: 0,
                left: 0
            };
            return {
                top: f.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: f.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function () {
            if (this.cssPosition == "relative") {
                var f = this.element.position();
                return {
                    top: f.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: f.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            } else return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function () {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            }
        },
        _cacheHelperProportions: function () {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function () {
            var f = this.options;
            if (f.containment == "parent") f.containment = this.helper[0].parentNode;
            if (f.containment == "document" || f.containment == "window") this.containment = [f.containment == "document" ? 0 : a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, f.containment == "document" ? 0 : a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, (f.containment == "document" ? 0 : a(window).scrollLeft()) + a(f.containment == "document" ? document : window).width() - this.helperProportions.width - this.margins.left, (f.containment == "document" ? 0 : a(window).scrollTop()) + (a(f.containment == "document" ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top];
            if (!/^(document|window|parent)$/.test(f.containment) && f.containment.constructor != Array) {
                f = a(f.containment);
                var i = f[0];
                if (i) {
                    f.offset();
                    var g = a(i).css("overflow") != "hidden";
                    this.containment = [(parseInt(a(i).css("borderLeftWidth"), 10) || 0) + (parseInt(a(i).css("paddingLeft"), 10) || 0), (parseInt(a(i).css("borderTopWidth"), 10) || 0) + (parseInt(a(i).css("paddingTop"), 10) || 0), (g ? Math.max(i.scrollWidth, i.offsetWidth) : i.offsetWidth) - (parseInt(a(i).css("borderLeftWidth"), 10) || 0) - (parseInt(a(i).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (g ? Math.max(i.scrollHeight, i.offsetHeight) : i.offsetHeight) - (parseInt(a(i).css("borderTopWidth"), 10) || 0) - (parseInt(a(i).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom];
                    this.relative_container = f
                }
            } else if (f.containment.constructor == Array) this.containment = f.containment
        },
        _convertPositionTo: function (f, i) {
            if (!i) i = this.position;
            var g = f == "absolute" ? 1 : -1,
                h = this.cssPosition == "absolute" && !(this.scrollParent[0] != document && a.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent,
                j = /(html|body)/i.test(h[0].tagName);
            return {
                top: i.top + this.offset.relative.top * g + this.offset.parent.top * g - (a.browser.safari && a.browser.version < 526 && this.cssPosition == "fixed" ? 0 : (this.cssPosition == "fixed" ? -this.scrollParent.scrollTop() : j ? 0 : h.scrollTop()) * g),
                left: i.left + this.offset.relative.left * g + this.offset.parent.left * g - (a.browser.safari && a.browser.version < 526 && this.cssPosition == "fixed" ? 0 : (this.cssPosition == "fixed" ? -this.scrollParent.scrollLeft() : j ? 0 : h.scrollLeft()) * g)
            }
        },
        _generatePosition: function (f) {
            var i = this.options,
                g = this.cssPosition == "absolute" && !(this.scrollParent[0] != document && a.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent,
                h = /(html|body)/i.test(g[0].tagName),
                j = f.pageX,
                l = f.pageY;
            if (this.originalPosition) {
                var c;
                if (this.containment) {
                    if (this.relative_container) {
                        c = this.relative_container.offset();
                        c = [this.containment[0] + c.left, this.containment[1] + c.top, this.containment[2] + c.left, this.containment[3] + c.top]
                    } else c = this.containment;
                    if (f.pageX - this.offset.click.left < c[0]) j = c[0] + this.offset.click.left;
                    if (f.pageY - this.offset.click.top < c[1]) l = c[1] + this.offset.click.top;
                    if (f.pageX - this.offset.click.left > c[2]) j = c[2] + this.offset.click.left;
                    if (f.pageY - this.offset.click.top > c[3]) l = c[3] + this.offset.click.top
                }
                if (i.grid) {
                    l = i.grid[1] ? this.originalPageY + Math.round((l - this.originalPageY) / i.grid[1]) * i.grid[1] : this.originalPageY;
                    l = c ? !(l - this.offset.click.top < c[1] || l - this.offset.click.top > c[3]) ? l : !(l - this.offset.click.top < c[1]) ? l - i.grid[1] : l + i.grid[1] : l;
                    j = i.grid[0] ? this.originalPageX + Math.round((j - this.originalPageX) / i.grid[0]) * i.grid[0] : this.originalPageX;
                    j = c ? !(j - this.offset.click.left < c[0] || j - this.offset.click.left > c[2]) ? j : !(j - this.offset.click.left < c[0]) ? j - i.grid[0] : j + i.grid[0] : j
                }
            }
            return {
                top: l - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + (a.browser.safari && a.browser.version < 526 && this.cssPosition == "fixed" ? 0 : this.cssPosition == "fixed" ? -this.scrollParent.scrollTop() : h ? 0 : g.scrollTop()),
                left: j - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + (a.browser.safari && a.browser.version < 526 && this.cssPosition == "fixed" ? 0 : this.cssPosition == "fixed" ? -this.scrollParent.scrollLeft() : h ? 0 : g.scrollLeft())
            }
        },
        _clear: function () {
            this.helper.removeClass("ui-draggable-dragging");
            this.helper[0] != this.element[0] && !this.cancelHelperRemoval && this.helper.remove();
            this.helper = null;
            this.cancelHelperRemoval = false
        },
        _trigger: function (f, i, g) {
            g = g || this._uiHash();
            a.ui.plugin.call(this, f, [i, g]);
            if (f == "drag") this.positionAbs = this._convertPositionTo("absolute");
            return a.Widget.prototype._trigger.call(this, f, i, g)
        },
        plugins: {},
        _uiHash: function () {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            }
        }
    });
    a.extend(a.ui.draggable, {
        version: "1.8.18"
    });
    a.ui.plugin.add("draggable", "connectToSortable", {
        start: function (f, i) {
            var g = a(this).data("draggable"),
                h = g.options,
                j = a.extend({}, i, {
                    item: g.element
                });
            g.sortables = [];
            a(h.connectToSortable).each(function () {
                var l = a.data(this, "sortable");
                if (l && !l.options.disabled) {
                    g.sortables.push({
                        instance: l,
                        shouldRevert: l.options.revert
                    });
                    l.refreshPositions();
                    l._trigger("activate", f, j)
                }
            })
        },
        stop: function (f, i) {
            var g = a(this).data("draggable"),
                h = a.extend({}, i, {
                    item: g.element
                });
            a.each(g.sortables, function () {
                if (this.instance.isOver) {
                    this.instance.isOver = 0;
                    g.cancelHelperRemoval = true;
                    this.instance.cancelHelperRemoval = false;
                    if (this.shouldRevert) this.instance.options.revert = true;
                    this.instance._mouseStop(f);
                    this.instance.options.helper = this.instance.options._helper;
                    g.options.helper == "original" && this.instance.currentItem.css({
                        top: "auto",
                        left: "auto"
                    })
                } else {
                    this.instance.cancelHelperRemoval = false;
                    this.instance._trigger("deactivate", f, h)
                }
            })
        },
        drag: function (f, i) {
            var g = a(this).data("draggable"),
                h = this;
            a.each(g.sortables, function () {
                this.instance.positionAbs = g.positionAbs;
                this.instance.helperProportions = g.helperProportions;
                this.instance.offset.click = g.offset.click;
                if (this.instance._intersectsWith(this.instance.containerCache)) {
                    if (!this.instance.isOver) {
                        this.instance.isOver = 1;
                        this.instance.currentItem = a(h).clone().removeAttr("id").appendTo(this.instance.element).data("sortable-item", true);
                        this.instance.options._helper = this.instance.options.helper;
                        this.instance.options.helper = function () {
                            return i.helper[0]
                        };
                        f.target = this.instance.currentItem[0];
                        this.instance._mouseCapture(f, true);
                        this.instance._mouseStart(f, true, true);
                        this.instance.offset.click.top = g.offset.click.top;
                        this.instance.offset.click.left = g.offset.click.left;
                        this.instance.offset.parent.left -= g.offset.parent.left - this.instance.offset.parent.left;
                        this.instance.offset.parent.top -= g.offset.parent.top - this.instance.offset.parent.top;
                        g._trigger("toSortable", f);
                        g.dropped = this.instance.element;
                        g.currentItem = g.element;
                        this.instance.fromOutside = g
                    }
                    this.instance.currentItem && this.instance._mouseDrag(f)
                } else if (this.instance.isOver) {
                    this.instance.isOver = 0;
                    this.instance.cancelHelperRemoval = true;
                    this.instance.options.revert = false;
                    this.instance._trigger("out", f, this.instance._uiHash(this.instance));
                    this.instance._mouseStop(f, true);
                    this.instance.options.helper = this.instance.options._helper;
                    this.instance.currentItem.remove();
                    this.instance.placeholder && this.instance.placeholder.remove();
                    g._trigger("fromSortable", f);
                    g.dropped = false
                }
            })
        }
    });
    a.ui.plugin.add("draggable", "cursor", {
        start: function () {
            var f = a("body"),
                i = a(this).data("draggable").options;
            if (f.css("cursor")) i._cursor = f.css("cursor");
            f.css("cursor", i.cursor)
        },
        stop: function () {
            var f = a(this).data("draggable").options;
            f._cursor && a("body").css("cursor", f._cursor)
        }
    });
    a.ui.plugin.add("draggable", "opacity", {
        start: function (f, i) {
            var g = a(i.helper),
                h = a(this).data("draggable").options;
            if (g.css("opacity")) h._opacity = g.css("opacity");
            g.css("opacity", h.opacity)
        },
        stop: function (f, i) {
            var g = a(this).data("draggable").options;
            g._opacity && a(i.helper).css("opacity", g._opacity)
        }
    });
    a.ui.plugin.add("draggable", "scroll", {
        start: function () {
            var f = a(this).data("draggable");
            if (f.scrollParent[0] != document && f.scrollParent[0].tagName != "HTML") f.overflowOffset = f.scrollParent.offset()
        },
        drag: function (f) {
            var i = a(this).data("draggable"),
                g = i.options,
                h = false;
            if (i.scrollParent[0] != document && i.scrollParent[0].tagName != "HTML") {
                if (!g.axis || g.axis != "x") if (i.overflowOffset.top + i.scrollParent[0].offsetHeight - f.pageY < g.scrollSensitivity) i.scrollParent[0].scrollTop = h = i.scrollParent[0].scrollTop + g.scrollSpeed;
                else if (f.pageY - i.overflowOffset.top < g.scrollSensitivity) i.scrollParent[0].scrollTop = h = i.scrollParent[0].scrollTop - g.scrollSpeed;
                if (!g.axis || g.axis != "y") if (i.overflowOffset.left + i.scrollParent[0].offsetWidth - f.pageX < g.scrollSensitivity) i.scrollParent[0].scrollLeft = h = i.scrollParent[0].scrollLeft + g.scrollSpeed;
                else if (f.pageX - i.overflowOffset.left < g.scrollSensitivity) i.scrollParent[0].scrollLeft = h = i.scrollParent[0].scrollLeft - g.scrollSpeed
            } else {
                if (!g.axis || g.axis != "x") if (f.pageY - a(document).scrollTop() < g.scrollSensitivity) h = a(document).scrollTop(a(document).scrollTop() - g.scrollSpeed);
                else if (a(window).height() - (f.pageY - a(document).scrollTop()) < g.scrollSensitivity) h = a(document).scrollTop(a(document).scrollTop() + g.scrollSpeed);
                if (!g.axis || g.axis != "y") if (f.pageX - a(document).scrollLeft() < g.scrollSensitivity) h = a(document).scrollLeft(a(document).scrollLeft() - g.scrollSpeed);
                else if (a(window).width() - (f.pageX - a(document).scrollLeft()) < g.scrollSensitivity) h = a(document).scrollLeft(a(document).scrollLeft() + g.scrollSpeed)
            }
            h !== false && a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(i, f)
        }
    });
    a.ui.plugin.add("draggable", "snap", {
        start: function () {
            var f = a(this).data("draggable"),
                i = f.options;
            f.snapElements = [];
            a(i.snap.constructor != String ? i.snap.items || ":data(draggable)" : i.snap).each(function () {
                var g = a(this),
                    h = g.offset();
                this != f.element[0] && f.snapElements.push({
                    item: this,
                    width: g.outerWidth(),
                    height: g.outerHeight(),
                    top: h.top,
                    left: h.left
                })
            })
        },
        drag: function (f, i) {
            for (var g = a(this).data("draggable"), h = g.options, j = h.snapTolerance, l = i.offset.left, c = l + g.helperProportions.width, k = i.offset.top, q = k + g.helperProportions.height, s = g.snapElements.length - 1; s >= 0; s--) {
                var p = g.snapElements[s].left,
                    n = p + g.snapElements[s].width,
                    u = g.snapElements[s].top,
                    z = u + g.snapElements[s].height;
                if (p - j < l && l < n + j && u - j < k && k < z + j || p - j < l && l < n + j && u - j < q && q < z + j || p - j < c && c < n + j && u - j < k && k < z + j || p - j < c && c < n + j && u - j < q && q < z + j) {
                    if (h.snapMode != "inner") {
                        var E = Math.abs(u - q) <= j,
                            M = Math.abs(z - k) <= j,
                            ma = Math.abs(p - c) <= j,
                            oa = Math.abs(n - l) <= j;
                        if (E) i.position.top = g._convertPositionTo("relative", {
                            top: u - g.helperProportions.height,
                            left: 0
                        }).top - g.margins.top;
                        if (M) i.position.top = g._convertPositionTo("relative", {
                            top: z,
                            left: 0
                        }).top - g.margins.top;
                        if (ma) i.position.left = g._convertPositionTo("relative", {
                            top: 0,
                            left: p - g.helperProportions.width
                        }).left - g.margins.left;
                        if (oa) i.position.left = g._convertPositionTo("relative", {
                            top: 0,
                            left: n
                        }).left - g.margins.left
                    }
                    var Da = E || M || ma || oa;
                    if (h.snapMode != "outer") {
                        E = Math.abs(u - k) <= j;
                        M = Math.abs(z - q) <= j;
                        ma = Math.abs(p - l) <= j;
                        oa = Math.abs(n - c) <= j;
                        if (E) i.position.top = g._convertPositionTo("relative", {
                            top: u,
                            left: 0
                        }).top - g.margins.top;
                        if (M) i.position.top = g._convertPositionTo("relative", {
                            top: z - g.helperProportions.height,
                            left: 0
                        }).top - g.margins.top;
                        if (ma) i.position.left = g._convertPositionTo("relative", {
                            top: 0,
                            left: p
                        }).left - g.margins.left;
                        if (oa) i.position.left = g._convertPositionTo("relative", {
                            top: 0,
                            left: n - g.helperProportions.width
                        }).left - g.margins.left
                    }
                    if (!g.snapElements[s].snapping && (E || M || ma || oa || Da)) g.options.snap.snap && g.options.snap.snap.call(g.element, f, a.extend(g._uiHash(), {
                        snapItem: g.snapElements[s].item
                    }));
                    g.snapElements[s].snapping = E || M || ma || oa || Da
                } else {
                    g.snapElements[s].snapping && g.options.snap.release && g.options.snap.release.call(g.element, f, a.extend(g._uiHash(), {
                        snapItem: g.snapElements[s].item
                    }));
                    g.snapElements[s].snapping = false
                }
            }
        }
    });
    a.ui.plugin.add("draggable", "stack", {
        start: function () {
            var f = a(this).data("draggable").options;
            f = a.makeArray(a(f.stack)).sort(function (g, h) {
                return (parseInt(a(g).css("zIndex"), 10) || 0) - (parseInt(a(h).css("zIndex"), 10) || 0)
            });
            if (f.length) {
                var i = parseInt(f[0].style.zIndex) || 0;
                a(f).each(function (g) {
                    this.style.zIndex = i + g
                });
                this[0].style.zIndex = i + f.length
            }
        }
    });
    a.ui.plugin.add("draggable", "zIndex", {
        start: function (f, i) {
            var g = a(i.helper),
                h = a(this).data("draggable").options;
            if (g.css("zIndex")) h._zIndex = g.css("zIndex");
            g.css("zIndex", h.zIndex)
        },
        stop: function (f, i) {
            var g = a(this).data("draggable").options;
            g._zIndex && a(i.helper).css("zIndex", g._zIndex)
        }
    })
})(jQuery);
(function (a) {
    a.widget("ui.resizable", a.ui.mouse, {
        widgetEventPrefix: "resize",
        options: {
            alsoResize: false,
            animate: false,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: false,
            autoHide: false,
            containment: false,
            ghost: false,
            grid: false,
            handles: "e,s,se",
            helper: false,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 1E3
        },
        _create: function () {
            var g = this,
                h = this.options;
            this.element.addClass("ui-resizable");
            a.extend(this, {
                _aspectRatio: !! h.aspectRatio,
                aspectRatio: h.aspectRatio,
                originalElement: this.element,
                _proportionallyResizeElements: [],
                _helper: h.helper || h.ghost || h.animate ? h.helper || "ui-resizable-helper" : null
            });
            if (this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)) {
                this.element.wrap(a('<div class="ui-wrapper" style="overflow: hidden;"></div>').css({
                    position: this.element.css("position"),
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight(),
                    top: this.element.css("top"),
                    left: this.element.css("left")
                }));
                this.element = this.element.parent().data("resizable", this.element.data("resizable"));
                this.elementIsWrapper = true;
                this.element.css({
                    marginLeft: this.originalElement.css("marginLeft"),
                    marginTop: this.originalElement.css("marginTop"),
                    marginRight: this.originalElement.css("marginRight"),
                    marginBottom: this.originalElement.css("marginBottom")
                });
                this.originalElement.css({
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0
                });
                this.originalResizeStyle = this.originalElement.css("resize");
                this.originalElement.css("resize", "none");
                this._proportionallyResizeElements.push(this.originalElement.css({
                    position: "static",
                    zoom: 1,
                    display: "block"
                }));
                this.originalElement.css({
                    margin: this.originalElement.css("margin")
                });
                this._proportionallyResize()
            }
            this.handles = h.handles || (!a(".ui-resizable-handle", this.element).length ? "e,s,se" : {
                n: ".ui-resizable-n",
                e: ".ui-resizable-e",
                s: ".ui-resizable-s",
                w: ".ui-resizable-w",
                se: ".ui-resizable-se",
                sw: ".ui-resizable-sw",
                ne: ".ui-resizable-ne",
                nw: ".ui-resizable-nw"
            });
            if (this.handles.constructor == String) {
                if (this.handles == "all") this.handles = "n,e,s,w,se,sw,ne,nw";
                var j = this.handles.split(",");
                this.handles = {};
                for (var l = 0; l < j.length; l++) {
                    var c = a.trim(j[l]),
                        k = a('<div class="ui-resizable-handle ' + ("ui-resizable-" + c) + '"></div>');
                    /sw|se|ne|nw/.test(c) && k.css({
                        zIndex: ++h.zIndex
                    });
                    "se" == c && k.addClass("ui-icon ui-icon-gripsmall-diagonal-se");
                    this.handles[c] = ".ui-resizable-" + c;
                    this.element.append(k)
                }
            }
            this._renderAxis = function (q) {
                q = q || this.element;
                for (var s in this.handles) {
                    if (this.handles[s].constructor == String) this.handles[s] = a(this.handles[s], this.element).show();
                    if (this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i)) {
                        var p = a(this.handles[s], this.element),
                            n = 0;
                        n = /sw|ne|nw|se|n|s/.test(s) ? p.outerHeight() : p.outerWidth();
                        p = ["padding", /ne|nw|n/.test(s) ? "Top" : /se|sw|s/.test(s) ? "Bottom" : /^e$/.test(s) ? "Right" : "Left"].join("");
                        q.css(p, n);
                        this._proportionallyResize()
                    }
                    a(this.handles[s])
                }
            };
            this._renderAxis(this.element);
            this._handles = a(".ui-resizable-handle", this.element).disableSelection();
            this._handles.mouseover(function () {
                if (!g.resizing) {
                    if (this.className) var q = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);
                    g.axis = q && q[1] ? q[1] : "se"
                }
            });
            if (h.autoHide) {
                this._handles.hide();
                a(this.element).addClass("ui-resizable-autohide").hover(function () {
                    if (!h.disabled) {
                        a(this).removeClass("ui-resizable-autohide");
                        g._handles.show()
                    }
                }, function () {
                    if (!h.disabled) if (!g.resizing) {
                        a(this).addClass("ui-resizable-autohide");
                        g._handles.hide()
                    }
                })
            }
            this._mouseInit()
        },
        destroy: function () {
            this._mouseDestroy();
            var g = function (j) {
                    a(j).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
                };
            if (this.elementIsWrapper) {
                g(this.element);
                var h = this.element;
                h.after(this.originalElement.css({
                    position: h.css("position"),
                    width: h.outerWidth(),
                    height: h.outerHeight(),
                    top: h.css("top"),
                    left: h.css("left")
                })).remove()
            }
            this.originalElement.css("resize", this.originalResizeStyle);
            g(this.originalElement);
            return this
        },
        _mouseCapture: function (g) {
            var h = false,
                j;
            for (j in this.handles) if (a(this.handles[j])[0] == g.target) h = true;
            return !this.options.disabled && h
        },
        _mouseStart: function (g) {
            var h = this.options,
                j = this.element.position(),
                l = this.element;
            this.resizing = true;
            this.documentScroll = {
                top: a(document).scrollTop(),
                left: a(document).scrollLeft()
            };
            if (l.is(".ui-draggable") || /absolute/.test(l.css("position"))) l.css({
                position: "absolute",
                top: j.top,
                left: j.left
            });
            this._renderProxy();
            j = f(this.helper.css("left"));
            var c = f(this.helper.css("top"));
            if (h.containment) {
                j += a(h.containment).scrollLeft() || 0;
                c += a(h.containment).scrollTop() || 0
            }
            this.offset = this.helper.offset();
            this.position = {
                left: j,
                top: c
            };
            this.size = this._helper ? {
                width: l.outerWidth(),
                height: l.outerHeight()
            } : {
                width: l.width(),
                height: l.height()
            };
            this.originalSize = this._helper ? {
                width: l.outerWidth(),
                height: l.outerHeight()
            } : {
                width: l.width(),
                height: l.height()
            };
            this.originalPosition = {
                left: j,
                top: c
            };
            this.sizeDiff = {
                width: l.outerWidth() - l.width(),
                height: l.outerHeight() - l.height()
            };
            this.originalMousePosition = {
                left: g.pageX,
                top: g.pageY
            };
            this.aspectRatio = typeof h.aspectRatio == "number" ? h.aspectRatio : this.originalSize.width / this.originalSize.height || 1;
            h = a(".ui-resizable-" + this.axis).css("cursor");
            a("body").css("cursor", h == "auto" ? this.axis + "-resize" : h);
            l.addClass("ui-resizable-resizing");
            this._propagate("start", g);
            return true
        },
        _mouseDrag: function (g) {
            var h = this.helper,
                j = this.originalMousePosition,
                l = this._change[this.axis];
            if (!l) return false;
            j = l.apply(this, [g, g.pageX - j.left || 0, g.pageY - j.top || 0]);
            this._updateVirtualBoundaries(g.shiftKey);
            if (this._aspectRatio || g.shiftKey) j = this._updateRatio(j, g);
            j = this._respectSize(j, g);
            this._propagate("resize", g);
            h.css({
                top: this.position.top + "px",
                left: this.position.left + "px",
                width: this.size.width + "px",
                height: this.size.height + "px"
            });
            !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize();
            this._updateCache(j);
            this._trigger("resize", g, this.ui());
            return false
        },
        _mouseStop: function (g) {
            this.resizing = false;
            var h = this.options;
            if (this._helper) {
                var j = this._proportionallyResizeElements,
                    l = j.length && /textarea/i.test(j[0].nodeName);
                j = l && a.ui.hasScroll(j[0], "left") ? 0 : this.sizeDiff.height;
                l = l ? 0 : this.sizeDiff.width;
                l = {
                    width: this.helper.width() - l,
                    height: this.helper.height() - j
                };
                j = parseInt(this.element.css("left"), 10) + (this.position.left - this.originalPosition.left) || null;
                var c = parseInt(this.element.css("top"), 10) + (this.position.top - this.originalPosition.top) || null;
                h.animate || this.element.css(a.extend(l, {
                    top: c,
                    left: j
                }));
                this.helper.height(this.size.height);
                this.helper.width(this.size.width);
                this._helper && !h.animate && this._proportionallyResize()
            }
            a("body").css("cursor", "auto");
            this.element.removeClass("ui-resizable-resizing");
            this._propagate("stop", g);
            this._helper && this.helper.remove();
            return false
        },
        _updateVirtualBoundaries: function (g) {
            var h = this.options,
                j, l, c;
            h = {
                minWidth: i(h.minWidth) ? h.minWidth : 0,
                maxWidth: i(h.maxWidth) ? h.maxWidth : Infinity,
                minHeight: i(h.minHeight) ? h.minHeight : 0,
                maxHeight: i(h.maxHeight) ? h.maxHeight : Infinity
            };
            if (this._aspectRatio || g) {
                g = h.minHeight * this.aspectRatio;
                l = h.minWidth / this.aspectRatio;
                j = h.maxHeight * this.aspectRatio;
                c = h.maxWidth / this.aspectRatio;
                if (g > h.minWidth) h.minWidth = g;
                if (l > h.minHeight) h.minHeight = l;
                if (j < h.maxWidth) h.maxWidth = j;
                if (c < h.maxHeight) h.maxHeight = c
            }
            this._vBoundaries = h
        },
        _updateCache: function (g) {
            this.offset = this.helper.offset();
            if (i(g.left)) this.position.left = g.left;
            if (i(g.top)) this.position.top = g.top;
            if (i(g.height)) this.size.height = g.height;
            if (i(g.width)) this.size.width = g.width
        },
        _updateRatio: function (g) {
            var h = this.position,
                j = this.size,
                l = this.axis;
            if (i(g.height)) g.width = g.height * this.aspectRatio;
            else if (i(g.width)) g.height = g.width / this.aspectRatio;
            if (l == "sw") {
                g.left = h.left + (j.width - g.width);
                g.top = null
            }
            if (l == "nw") {
                g.top = h.top + (j.height - g.height);
                g.left = h.left + (j.width - g.width)
            }
            return g
        },
        _respectSize: function (g) {
            var h = this._vBoundaries,
                j = this.axis,
                l = i(g.width) && h.maxWidth && h.maxWidth < g.width,
                c = i(g.height) && h.maxHeight && h.maxHeight < g.height,
                k = i(g.width) && h.minWidth && h.minWidth > g.width,
                q = i(g.height) && h.minHeight && h.minHeight > g.height;
            if (k) g.width = h.minWidth;
            if (q) g.height = h.minHeight;
            if (l) g.width = h.maxWidth;
            if (c) g.height = h.maxHeight;
            var s = this.originalPosition.left + this.originalSize.width,
                p = this.position.top + this.size.height,
                n = /sw|nw|w/.test(j);
            j = /nw|ne|n/.test(j);
            if (k && n) g.left = s - h.minWidth;
            if (l && n) g.left = s - h.maxWidth;
            if (q && j) g.top = p - h.minHeight;
            if (c && j) g.top = p - h.maxHeight;
            if ((h = !g.width && !g.height) && !g.left && g.top) g.top = null;
            else if (h && !g.top && g.left) g.left = null;
            return g
        },
        _proportionallyResize: function () {
            if (this._proportionallyResizeElements.length) for (var g = this.helper || this.element, h = 0; h < this._proportionallyResizeElements.length; h++) {
                var j = this._proportionallyResizeElements[h];
                if (!this.borderDif) {
                    var l = [j.css("borderTopWidth"), j.css("borderRightWidth"), j.css("borderBottomWidth"), j.css("borderLeftWidth")],
                        c = [j.css("paddingTop"), j.css("paddingRight"), j.css("paddingBottom"), j.css("paddingLeft")];
                    this.borderDif = a.map(l, function (k, q) {
                        var s = parseInt(k, 10) || 0,
                            p = parseInt(c[q], 10) || 0;
                        return s + p
                    })
                }
                a.browser.msie && (a(g).is(":hidden") || a(g).parents(":hidden").length) || j.css({
                    height: g.height() - this.borderDif[0] - this.borderDif[2] || 0,
                    width: g.width() - this.borderDif[1] - this.borderDif[3] || 0
                })
            }
        },
        _renderProxy: function () {
            var g = this.options;
            this.elementOffset = this.element.offset();
            if (this._helper) {
                this.helper = this.helper || a('<div style="overflow:hidden;"></div>');
                var h = a.browser.msie && a.browser.version < 7,
                    j = h ? 1 : 0;
                h = h ? 2 : -1;
                this.helper.addClass(this._helper).css({
                    width: this.element.outerWidth() + h,
                    height: this.element.outerHeight() + h,
                    position: "absolute",
                    left: this.elementOffset.left - j + "px",
                    top: this.elementOffset.top - j + "px",
                    zIndex: ++g.zIndex
                });
                this.helper.appendTo("body").disableSelection()
            } else this.helper = this.element
        },
        _change: {
            e: function (g, h) {
                return {
                    width: this.originalSize.width + h
                }
            },
            w: function (g, h) {
                return {
                    left: this.originalPosition.left + h,
                    width: this.originalSize.width - h
                }
            },
            n: function (g, h, j) {
                return {
                    top: this.originalPosition.top + j,
                    height: this.originalSize.height - j
                }
            },
            s: function (g, h, j) {
                return {
                    height: this.originalSize.height + j
                }
            },
            se: function (g, h, j) {
                return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [g, h, j]))
            },
            sw: function (g, h, j) {
                return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [g, h, j]))
            },
            ne: function (g, h, j) {
                return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [g, h, j]))
            },
            nw: function (g, h, j) {
                return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [g, h, j]))
            }
        },
        _propagate: function (g, h) {
            a.ui.plugin.call(this, g, [h, this.ui()]);
            g != "resize" && this._trigger(g, h, this.ui())
        },
        plugins: {},
        ui: function () {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            }
        }
    });
    a.extend(a.ui.resizable, {
        version: "1.8.18"
    });
    a.ui.plugin.add("resizable", "alsoResize", {
        start: function () {
            var g = a(this).data("resizable").options,
                h = function (j) {
                    a(j).each(function () {
                        var l = a(this);
                        l.data("resizable-alsoresize", {
                            width: parseInt(l.width(), 10),
                            height: parseInt(l.height(), 10),
                            left: parseInt(l.css("left"), 10),
                            top: parseInt(l.css("top"), 10)
                        })
                    })
                };
            if (typeof g.alsoResize == "object" && !g.alsoResize.parentNode) if (g.alsoResize.length) {
                g.alsoResize = g.alsoResize[0];
                h(g.alsoResize)
            } else a.each(g.alsoResize, function (j) {
                h(j)
            });
            else h(g.alsoResize)
        },
        resize: function (g, h) {
            var j = a(this).data("resizable"),
                l = j.options,
                c = j.originalSize,
                k = j.originalPosition,
                q = {
                    height: j.size.height - c.height || 0,
                    width: j.size.width - c.width || 0,
                    top: j.position.top - k.top || 0,
                    left: j.position.left - k.left || 0
                },
                s = function (p, n) {
                    a(p).each(function () {
                        var u = a(this),
                            z = a(this).data("resizable-alsoresize"),
                            E = {},
                            M = n && n.length ? n : u.parents(h.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                        a.each(M, function (ma, oa) {
                            var Da = (z[oa] || 0) + (q[oa] || 0);
                            if (Da && Da >= 0) E[oa] = Da || null
                        });
                        u.css(E)
                    })
                };
            typeof l.alsoResize == "object" && !l.alsoResize.nodeType ? a.each(l.alsoResize, function (p, n) {
                s(p, n)
            }) : s(l.alsoResize)
        },
        stop: function () {
            a(this).removeData("resizable-alsoresize")
        }
    });
    a.ui.plugin.add("resizable", "animate", {
        stop: function (g) {
            var h = a(this).data("resizable"),
                j = h.options,
                l = h._proportionallyResizeElements,
                c = l.length && /textarea/i.test(l[0].nodeName),
                k = c && a.ui.hasScroll(l[0], "left") ? 0 : h.sizeDiff.height;
            c = {
                width: h.size.width - (c ? 0 : h.sizeDiff.width),
                height: h.size.height - k
            };
            k = parseInt(h.element.css("left"), 10) + (h.position.left - h.originalPosition.left) || null;
            var q = parseInt(h.element.css("top"), 10) + (h.position.top - h.originalPosition.top) || null;
            h.element.animate(a.extend(c, q && k ? {
                top: q,
                left: k
            } : {}), {
                duration: j.animateDuration,
                easing: j.animateEasing,
                step: function () {
                    var s = {
                        width: parseInt(h.element.css("width"), 10),
                        height: parseInt(h.element.css("height"), 10),
                        top: parseInt(h.element.css("top"), 10),
                        left: parseInt(h.element.css("left"), 10)
                    };
                    l && l.length && a(l[0]).css({
                        width: s.width,
                        height: s.height
                    });
                    h._updateCache(s);
                    h._propagate("resize", g)
                }
            })
        }
    });
    a.ui.plugin.add("resizable", "containment", {
        start: function () {
            var g = a(this).data("resizable"),
                h = g.element,
                j = g.options.containment;
            if (h = j instanceof a ? j.get(0) : /parent/.test(j) ? h.parent().get(0) : j) {
                g.containerElement = a(h);
                if (/document/.test(j) || j == document) {
                    g.containerOffset = {
                        left: 0,
                        top: 0
                    };
                    g.containerPosition = {
                        left: 0,
                        top: 0
                    };
                    g.parentData = {
                        element: a(document),
                        left: 0,
                        top: 0,
                        width: a(document).width(),
                        height: a(document).height() || document.body.parentNode.scrollHeight
                    }
                } else {
                    var l = a(h),
                        c = [];
                    a(["Top", "Right", "Left", "Bottom"]).each(function (s, p) {
                        c[s] = f(l.css("padding" + p))
                    });
                    g.containerOffset = l.offset();
                    g.containerPosition = l.position();
                    g.containerSize = {
                        height: l.innerHeight() - c[3],
                        width: l.innerWidth() - c[1]
                    };
                    j = g.containerOffset;
                    var k = g.containerSize.height,
                        q = g.containerSize.width;
                    q = a.ui.hasScroll(h, "left") ? h.scrollWidth : q;
                    k = a.ui.hasScroll(h) ? h.scrollHeight : k;
                    g.parentData = {
                        element: h,
                        left: j.left,
                        top: j.top,
                        width: q,
                        height: k
                    }
                }
            }
        },
        resize: function (g) {
            var h = a(this).data("resizable"),
                j = h.options,
                l = h.containerOffset,
                c = h.position;
            g = h._aspectRatio || g.shiftKey;
            var k = {
                top: 0,
                left: 0
            },
                q = h.containerElement;
            if (q[0] != document && /static/.test(q.css("position"))) k = l;
            if (c.left < (h._helper ? l.left : 0)) {
                h.size.width += h._helper ? h.position.left - l.left : h.position.left - k.left;
                if (g) h.size.height = h.size.width / j.aspectRatio;
                h.position.left = j.helper ? l.left : 0
            }
            if (c.top < (h._helper ? l.top : 0)) {
                h.size.height += h._helper ? h.position.top - l.top : h.position.top;
                if (g) h.size.width = h.size.height * j.aspectRatio;
                h.position.top = h._helper ? l.top : 0
            }
            h.offset.left = h.parentData.left + h.position.left;
            h.offset.top = h.parentData.top + h.position.top;
            j = Math.abs((h._helper ? h.offset.left - k.left : h.offset.left - k.left) + h.sizeDiff.width);
            l = Math.abs((h._helper ? h.offset.top - k.top : h.offset.top - l.top) + h.sizeDiff.height);
            c = h.containerElement.get(0) == h.element.parent().get(0);
            k = /relative|absolute/.test(h.containerElement.css("position"));
            if (c && k) j -= h.parentData.left;
            if (j + h.size.width >= h.parentData.width) {
                h.size.width = h.parentData.width - j;
                if (g) h.size.height = h.size.width / h.aspectRatio
            }
            if (l + h.size.height >= h.parentData.height) {
                h.size.height = h.parentData.height - l;
                if (g) h.size.width = h.size.height * h.aspectRatio
            }
        },
        stop: function () {
            var g = a(this).data("resizable"),
                h = g.options,
                j = g.containerOffset,
                l = g.containerPosition,
                c = g.containerElement,
                k = a(g.helper),
                q = k.offset(),
                s = k.outerWidth() - g.sizeDiff.width;
            k = k.outerHeight() - g.sizeDiff.height;
            g._helper && !h.animate && /relative/.test(c.css("position")) && a(this).css({
                left: q.left - l.left - j.left,
                width: s,
                height: k
            });
            g._helper && !h.animate && /static/.test(c.css("position")) && a(this).css({
                left: q.left - l.left - j.left,
                width: s,
                height: k
            })
        }
    });
    a.ui.plugin.add("resizable", "ghost", {
        start: function () {
            var g = a(this).data("resizable"),
                h = g.options,
                j = g.size;
            g.ghost = g.originalElement.clone();
            g.ghost.css({
                opacity: 0.25,
                display: "block",
                position: "relative",
                height: j.height,
                width: j.width,
                margin: 0,
                left: 0,
                top: 0
            }).addClass("ui-resizable-ghost").addClass(typeof h.ghost == "string" ? h.ghost : "");
            g.ghost.appendTo(g.helper)
        },
        resize: function () {
            var g = a(this).data("resizable");
            g.ghost && g.ghost.css({
                position: "relative",
                height: g.size.height,
                width: g.size.width
            })
        },
        stop: function () {
            var g = a(this).data("resizable");
            g.ghost && g.helper && g.helper.get(0).removeChild(g.ghost.get(0))
        }
    });
    a.ui.plugin.add("resizable", "grid", {
        resize: function () {
            var g = a(this).data("resizable"),
                h = g.options,
                j = g.size,
                l = g.originalSize,
                c = g.originalPosition,
                k = g.axis;
            h.grid = typeof h.grid == "number" ? [h.grid, h.grid] : h.grid;
            var q = Math.round((j.width - l.width) / (h.grid[0] || 1)) * (h.grid[0] || 1);
            h = Math.round((j.height - l.height) / (h.grid[1] || 1)) * (h.grid[1] || 1);
            if (/^(se|s|e)$/.test(k)) {
                g.size.width = l.width + q;
                g.size.height = l.height + h
            } else if (/^(ne)$/.test(k)) {
                g.size.width = l.width + q;
                g.size.height = l.height + h;
                g.position.top = c.top - h
            } else {
                if (/^(sw)$/.test(k)) {
                    g.size.width = l.width + q;
                    g.size.height = l.height + h
                } else {
                    g.size.width = l.width + q;
                    g.size.height = l.height + h;
                    g.position.top = c.top - h
                }
                g.position.left = c.left - q
            }
        }
    });
    var f = function (g) {
            return parseInt(g, 10) || 0
        },
        i = function (g) {
            return !isNaN(parseInt(g, 10))
        }
})(jQuery);
(function (a) {
    a.widget("ui.accordion", {
        options: {
            active: 0,
            animated: "slide",
            autoHeight: true,
            clearStyle: false,
            collapsible: false,
            event: "click",
            fillSpace: false,
            header: "> li > :first-child,> :not(li):even",
            icons: {
                header: "ui-icon-triangle-1-e",
                headerSelected: "ui-icon-triangle-1-s"
            },
            navigation: false,
            navigationFilter: function () {
                return this.href.toLowerCase() === location.href.toLowerCase()
            }
        },
        _create: function () {
            var f = this,
                i = f.options;
            f.running = 0;
            f.element.addClass("ui-accordion ui-widget ui-helper-reset").children("li").addClass("ui-accordion-li-fix");
            f.headers = f.element.find(i.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all").bind("mouseenter.accordion", function () {
                i.disabled || a(this).addClass("ui-state-hover")
            }).bind("mouseleave.accordion", function () {
                i.disabled || a(this).removeClass("ui-state-hover")
            }).bind("focus.accordion", function () {
                i.disabled || a(this).addClass("ui-state-focus")
            }).bind("blur.accordion", function () {
                i.disabled || a(this).removeClass("ui-state-focus")
            });
            f.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom");
            if (i.navigation) {
                var g = f.element.find("a").filter(i.navigationFilter).eq(0);
                if (g.length) {
                    var h = g.closest(".ui-accordion-header");
                    f.active = h.length ? h : g.closest(".ui-accordion-content").prev()
                }
            }
            f.active = f._findActive(f.active || i.active).addClass("ui-state-default ui-state-active").toggleClass("ui-corner-all").toggleClass("ui-corner-top");
            f.active.next().addClass("ui-accordion-content-active");
            f._createIcons();
            f.resize();
            f.element.attr("role", "tablist");
            f.headers.attr("role", "tab").bind("keydown.accordion", function (j) {
                return f._keydown(j)
            }).next().attr("role", "tabpanel");
            f.headers.not(f.active || "").attr({
                "aria-expanded": "false",
                "aria-selected": "false",
                tabIndex: -1
            }).next().hide();
            f.active.length ? f.active.attr({
                "aria-expanded": "true",
                "aria-selected": "true",
                tabIndex: 0
            }) : f.headers.eq(0).attr("tabIndex", 0);
            a.browser.safari || f.headers.find("a").attr("tabIndex", -1);
            i.event && f.headers.bind(i.event.split(" ").join(".accordion ") + ".accordion", function (j) {
                f._clickHandler.call(f, j, this);
                j.preventDefault()
            })
        },
        _createIcons: function () {
            var f = this.options;
            if (f.icons) {
                a("<span></span>").addClass("ui-icon " + f.icons.header).prependTo(this.headers);
                this.active.children(".ui-icon").toggleClass(f.icons.header).toggleClass(f.icons.headerSelected);
                this.element.addClass("ui-accordion-icons")
            }
        },
        _destroyIcons: function () {
            this.headers.children(".ui-icon").remove();
            this.element.removeClass("ui-accordion-icons")
        },
        destroy: function () {
            var f = this.options;
            this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role");
            this.headers.unbind(".accordion").removeClass("ui-accordion-header ui-accordion-disabled ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("tabIndex");
            this.headers.find("a").removeAttr("tabIndex");
            this._destroyIcons();
            var i = this.headers.next().css("display", "").removeAttr("role").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-accordion-disabled ui-state-disabled");
            if (f.autoHeight || f.fillHeight) i.css("height", "");
            return a.Widget.prototype.destroy.call(this)
        },
        _setOption: function (f, i) {
            a.Widget.prototype._setOption.apply(this, arguments);
            f == "active" && this.activate(i);
            if (f == "icons") {
                this._destroyIcons();
                i && this._createIcons()
            }
            if (f == "disabled") this.headers.add(this.headers.next())[i ? "addClass" : "removeClass"]("ui-accordion-disabled ui-state-disabled")
        },
        _keydown: function (f) {
            if (!(this.options.disabled || f.altKey || f.ctrlKey)) {
                var i = a.ui.keyCode,
                    g = this.headers.length,
                    h = this.headers.index(f.target),
                    j = false;
                switch (f.keyCode) {
                case i.RIGHT:
                case i.DOWN:
                    j = this.headers[(h + 1) % g];
                    break;
                case i.LEFT:
                case i.UP:
                    j = this.headers[(h - 1 + g) % g];
                    break;
                case i.SPACE:
                case i.ENTER:
                    this._clickHandler({
                        target: f.target
                    }, f.target);
                    f.preventDefault()
                }
                if (j) {
                    a(f.target).attr("tabIndex", -1);
                    a(j).attr("tabIndex", 0);
                    j.focus();
                    return false
                }
                return true
            }
        },
        resize: function () {
            var f = this.options,
                i;
            if (f.fillSpace) {
                if (a.browser.msie) {
                    var g = this.element.parent().css("overflow");
                    this.element.parent().css("overflow", "hidden")
                }
                i = this.element.parent().height();
                a.browser.msie && this.element.parent().css("overflow", g);
                this.headers.each(function () {
                    i -= a(this).outerHeight(true)
                });
                this.headers.next().each(function () {
                    a(this).height(Math.max(0, i - a(this).innerHeight() + a(this).height()))
                }).css("overflow", "auto")
            } else if (f.autoHeight) {
                i = 0;
                this.headers.next().each(function () {
                    i = Math.max(i, a(this).height("").height())
                }).height(i)
            }
            return this
        },
        activate: function (f) {
            this.options.active = f;
            f = this._findActive(f)[0];
            this._clickHandler({
                target: f
            }, f);
            return this
        },
        _findActive: function (f) {
            return f ? typeof f === "number" ? this.headers.filter(":eq(" + f + ")") : this.headers.not(this.headers.not(f)) : f === false ? a([]) : this.headers.filter(":eq(0)")
        },
        _clickHandler: function (f, i) {
            var g = this.options;
            if (!g.disabled) if (f.target) {
                var h = a(f.currentTarget || i),
                    j = h[0] === this.active[0];
                g.active = g.collapsible && j ? false : this.headers.index(h);
                if (!(this.running || !g.collapsible && j)) {
                    var l = this.active;
                    s = h.next();
                    k = this.active.next();
                    q = {
                        options: g,
                        newHeader: j && g.collapsible ? a([]) : h,
                        oldHeader: this.active,
                        newContent: j && g.collapsible ? a([]) : s,
                        oldContent: k
                    };
                    var c = this.headers.index(this.active[0]) > this.headers.index(h[0]);
                    this.active = j ? a([]) : h;
                    this._toggle(s, k, q, j, c);
                    l.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").children(".ui-icon").removeClass(g.icons.headerSelected).addClass(g.icons.header);
                    if (!j) {
                        h.removeClass("ui-state-default ui-corner-all").addClass("ui-state-active ui-corner-top").children(".ui-icon").removeClass(g.icons.header).addClass(g.icons.headerSelected);
                        h.next().addClass("ui-accordion-content-active")
                    }
                }
            } else if (g.collapsible) {
                this.active.removeClass("ui-state-active ui-corner-top").addClass("ui-state-default ui-corner-all").children(".ui-icon").removeClass(g.icons.headerSelected).addClass(g.icons.header);
                this.active.next().addClass("ui-accordion-content-active");
                var k = this.active.next(),
                    q = {
                        options: g,
                        newHeader: a([]),
                        oldHeader: g.active,
                        newContent: a([]),
                        oldContent: k
                    },
                    s = this.active = a([]);
                this._toggle(s, k, q)
            }
        },
        _toggle: function (f, i, g, h, j) {
            var l = this,
                c = l.options;
            l.toShow = f;
            l.toHide = i;
            l.data = g;
            var k = function () {
                    if (l) return l._completed.apply(l, arguments)
                };
            l._trigger("changestart", null, l.data);
            l.running = i.size() === 0 ? f.size() : i.size();
            if (c.animated) {
                g = {};
                g = c.collapsible && h ? {
                    toShow: a([]),
                    toHide: i,
                    complete: k,
                    down: j,
                    autoHeight: c.autoHeight || c.fillSpace
                } : {
                    toShow: f,
                    toHide: i,
                    complete: k,
                    down: j,
                    autoHeight: c.autoHeight || c.fillSpace
                };
                if (!c.proxied) c.proxied = c.animated;
                if (!c.proxiedDuration) c.proxiedDuration = c.duration;
                c.animated = a.isFunction(c.proxied) ? c.proxied(g) : c.proxied;
                c.duration = a.isFunction(c.proxiedDuration) ? c.proxiedDuration(g) : c.proxiedDuration;
                h = a.ui.accordion.animations;
                var q = c.duration,
                    s = c.animated;
                if (s && !h[s] && !a.easing[s]) s = "slide";
                h[s] || (h[s] = function (p) {
                    this.slide(p, {
                        easing: s,
                        duration: q || 700
                    })
                });
                h[s](g)
            } else {
                if (c.collapsible && h) f.toggle();
                else {
                    i.hide();
                    f.show()
                }
                k(true)
            }
            i.prev().attr({
                "aria-expanded": "false",
                "aria-selected": "false",
                tabIndex: -1
            }).blur();
            f.prev().attr({
                "aria-expanded": "true",
                "aria-selected": "true",
                tabIndex: 0
            }).focus()
        },
        _completed: function (f) {
            this.running = f ? 0 : --this.running;
            if (!this.running) {
                this.options.clearStyle && this.toShow.add(this.toHide).css({
                    height: "",
                    overflow: ""
                });
                this.toHide.removeClass("ui-accordion-content-active");
                if (this.toHide.length) this.toHide.parent()[0].className = this.toHide.parent()[0].className;
                this._trigger("change", null, this.data)
            }
        }
    });
    a.extend(a.ui.accordion, {
        version: "1.8.18",
        animations: {
            slide: function (f, i) {
                f = a.extend({
                    easing: "swing",
                    duration: 300
                }, f, i);
                if (f.toHide.size()) if (f.toShow.size()) {
                    var g = f.toShow.css("overflow"),
                        h = 0,
                        j = {},
                        l = {},
                        c, k = f.toShow;
                    c = k[0].style.width;
                    k.width(k.parent().width() - parseFloat(k.css("paddingLeft")) - parseFloat(k.css("paddingRight")) - (parseFloat(k.css("borderLeftWidth")) || 0) - (parseFloat(k.css("borderRightWidth")) || 0));
                    a.each(["height", "paddingTop", "paddingBottom"], function (q, s) {
                        l[s] = "hide";
                        var p = ("" + a.css(f.toShow[0], s)).match(/^([\d+-.]+)(.*)$/);
                        j[s] = {
                            value: p[1],
                            unit: p[2] || "px"
                        }
                    });
                    f.toShow.css({
                        height: 0,
                        overflow: "hidden"
                    }).show();
                    f.toHide.filter(":hidden").each(f.complete).end().filter(":visible").animate(l, {
                        step: function (q, s) {
                            if (s.prop == "height") h = s.end - s.start === 0 ? 0 : (s.now - s.start) / (s.end - s.start);
                            f.toShow[0].style[s.prop] = h * j[s.prop].value + j[s.prop].unit
                        },
                        duration: f.duration,
                        easing: f.easing,
                        complete: function () {
                            f.autoHeight || f.toShow.css("height", "");
                            f.toShow.css({
                                width: c,
                                overflow: g
                            });
                            f.complete()
                        }
                    })
                } else f.toHide.animate({
                    height: "hide",
                    paddingTop: "hide",
                    paddingBottom: "hide"
                }, f);
                else f.toShow.animate({
                    height: "show",
                    paddingTop: "show",
                    paddingBottom: "show"
                }, f)
            },
            bounceslide: function (f) {
                this.slide(f, {
                    easing: f.down ? "easeOutBounce" : "swing",
                    duration: f.down ? 1E3 : 200
                })
            }
        }
    })
})(jQuery);
(function (a) {
    var f = 0;
    a.widget("ui.autocomplete", {
        options: {
            appendTo: "body",
            autoFocus: false,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null
        },
        pending: 0,
        _create: function () {
            var i = this,
                g = this.element[0].ownerDocument,
                h;
            this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off").attr({
                role: "textbox",
                "aria-autocomplete": "list",
                "aria-haspopup": "true"
            }).bind("keydown.autocomplete", function (j) {
                if (!(i.options.disabled || i.element.propAttr("readOnly"))) {
                    h = false;
                    var l = a.ui.keyCode;
                    switch (j.keyCode) {
                    case l.PAGE_UP:
                        i._move("previousPage", j);
                        break;
                    case l.PAGE_DOWN:
                        i._move("nextPage", j);
                        break;
                    case l.UP:
                        i._move("previous", j);
                        j.preventDefault();
                        break;
                    case l.DOWN:
                        i._move("next", j);
                        j.preventDefault();
                        break;
                    case l.ENTER:
                    case l.NUMPAD_ENTER:
                        if (i.menu.active) {
                            h = true;
                            j.preventDefault()
                        }
                    case l.TAB:
                        if (!i.menu.active) break;
                        i.menu.select(j);
                        break;
                    case l.ESCAPE:
                        i.element.val(i.term);
                        i.close(j);
                        break;
                    default:
                        clearTimeout(i.searching);
                        i.searching = setTimeout(function () {
                            if (i.term != i.element.val()) {
                                i.selectedItem = null;
                                i.search(null, j)
                            }
                        }, i.options.delay)
                    }
                }
            }).bind("keypress.autocomplete", function (j) {
                if (h) {
                    h = false;
                    j.preventDefault()
                }
            }).bind("focus.autocomplete", function () {
                if (!i.options.disabled) {
                    i.selectedItem = null;
                    i.previous = i.element.val()
                }
            }).bind("blur.autocomplete", function (j) {
                if (!i.options.disabled) {
                    clearTimeout(i.searching);
                    i.closing = setTimeout(function () {
                        i.close(j);
                        i._change(j)
                    }, 150)
                }
            });
            this._initSource();
            this.response = function () {
                return i._response.apply(i, arguments)
            };
            this.menu = a("<ul></ul>").addClass("ui-autocomplete").appendTo(a(this.options.appendTo || "body", g)[0]).mousedown(function (j) {
                var l = i.menu.element[0];
                a(j.target).closest(".ui-menu-item").length || setTimeout(function () {
                    a(document).one("mousedown", function (c) {
                        c.target !== i.element[0] && c.target !== l && !a.ui.contains(l, c.target) && i.close()
                    })
                }, 1);
                setTimeout(function () {
                    clearTimeout(i.closing)
                }, 13)
            }).menu({
                focus: function (j, l) {
                    var c = l.item.data("item.autocomplete");
                    false !== i._trigger("focus", j, {
                        item: c
                    }) && /^key/.test(j.originalEvent.type) && i.element.val(c.value)
                },
                selected: function (j, l) {
                    var c = l.item.data("item.autocomplete"),
                        k = i.previous;
                    if (i.element[0] !== g.activeElement) {
                        i.element.focus();
                        i.previous = k;
                        setTimeout(function () {
                            i.previous = k;
                            i.selectedItem = c
                        }, 1)
                    }
                    false !== i._trigger("select", j, {
                        item: c
                    }) && i.element.val(c.value);
                    i.term = i.element.val();
                    i.close(j);
                    i.selectedItem = c
                },
                blur: function () {
                    i.menu.element.is(":visible") && i.element.val() !== i.term && i.element.val(i.term)
                }
            }).zIndex(this.element.zIndex() + 1).css({
                top: 0,
                left: 0
            }).hide().data("menu");
            a.fn.bgiframe && this.menu.element.bgiframe();
            i.beforeunloadHandler = function () {
                i.element.removeAttr("autocomplete")
            };
            a(window).bind("beforeunload", i.beforeunloadHandler)
        },
        destroy: function () {
            this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete").removeAttr("role").removeAttr("aria-autocomplete").removeAttr("aria-haspopup");
            this.menu.element.remove();
            a(window).unbind("beforeunload", this.beforeunloadHandler);
            a.Widget.prototype.destroy.call(this)
        },
        _setOption: function (i, g) {
            a.Widget.prototype._setOption.apply(this, arguments);
            i === "source" && this._initSource();
            if (i === "appendTo") this.menu.element.appendTo(a(g || "body", this.element[0].ownerDocument)[0]);
            i === "disabled" && g && this.xhr && this.xhr.abort()
        },
        _initSource: function () {
            var i = this,
                g, h;
            if (a.isArray(this.options.source)) {
                g = this.options.source;
                this.source = function (j, l) {
                    l(a.ui.autocomplete.filter(g, j.term))
                }
            } else if (typeof this.options.source === "string") {
                h = this.options.source;
                this.source = function (j, l) {
                    i.xhr && i.xhr.abort();
                    i.xhr = a.ajax({
                        url: h,
                        data: j,
                        dataType: "json",
                        context: {
                            autocompleteRequest: ++f
                        },
                        success: function (c) {
                            this.autocompleteRequest === f && l(c)
                        },
                        error: function () {
                            this.autocompleteRequest === f && l([])
                        }
                    })
                }
            } else this.source = this.options.source
        },
        search: function (i, g) {
            i = i != null ? i : this.element.val();
            this.term = this.element.val();
            if (i.length < this.options.minLength) return this.close(g);
            clearTimeout(this.closing);
            if (this._trigger("search", g) !== false) return this._search(i)
        },
        _search: function (i) {
            this.pending++;
            this.element.addClass("ui-autocomplete-loading");
            this.source({
                term: i
            }, this.response)
        },
        _response: function (i) {
            if (!this.options.disabled && i && i.length) {
                i = this._normalize(i);
                this._suggest(i);
                this._trigger("open")
            } else this.close();
            this.pending--;
            this.pending || this.element.removeClass("ui-autocomplete-loading")
        },
        close: function (i) {
            clearTimeout(this.closing);
            if (this.menu.element.is(":visible")) {
                this.menu.element.hide();
                this.menu.deactivate();
                this._trigger("close", i)
            }
        },
        _change: function (i) {
            this.previous !== this.element.val() && this._trigger("change", i, {
                item: this.selectedItem
            })
        },
        _normalize: function (i) {
            if (i.length && i[0].label && i[0].value) return i;
            return a.map(i, function (g) {
                if (typeof g === "string") return {
                    label: g,
                    value: g
                };
                return a.extend({
                    label: g.label || g.value,
                    value: g.value || g.label
                }, g)
            })
        },
        _suggest: function (i) {
            var g = this.menu.element.empty().zIndex(this.element.zIndex() + 1);
            this._renderMenu(g, i);
            this.menu.deactivate();
            this.menu.refresh();
            g.show();
            this._resizeMenu();
            g.position(a.extend({
                of: this.element
            }, this.options.position));
            this.options.autoFocus && this.menu.next(new a.Event("mouseover"))
        },
        _resizeMenu: function () {
            var i = this.menu.element;
            i.outerWidth(Math.max(i.width("").outerWidth() + 1, this.element.outerWidth()))
        },
        _renderMenu: function (i, g) {
            var h = this;
            a.each(g, function (j, l) {
                h._renderItem(i, l)
            })
        },
        _renderItem: function (i, g) {
            return a("<li></li>").data("item.autocomplete", g).append(a("<a></a>").text(g.label)).appendTo(i)
        },
        _move: function (i, g) {
            if (this.menu.element.is(":visible")) if (this.menu.first() && /^previous/.test(i) || this.menu.last() && /^next/.test(i)) {
                this.element.val(this.term);
                this.menu.deactivate()
            } else this.menu[i](g);
            else this.search(null, g)
        },
        widget: function () {
            return this.menu.element
        }
    });
    a.extend(a.ui.autocomplete, {
        escapeRegex: function (i) {
            return i.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
        },
        filter: function (i, g) {
            var h = RegExp(a.ui.autocomplete.escapeRegex(g), "i");
            return a.grep(i, function (j) {
                return h.test(j.label || j.value || j)
            })
        }
    })
})(jQuery);
(function (a) {
    a.widget("ui.menu", {
        _create: function () {
            var f = this;
            this.element.addClass("ui-menu ui-widget ui-widget-content ui-corner-all").attr({
                role: "listbox",
                "aria-activedescendant": "ui-active-menuitem"
            }).click(function (i) {
                if (a(i.target).closest(".ui-menu-item a").length) {
                    i.preventDefault();
                    f.select(i)
                }
            });
            this.refresh()
        },
        refresh: function () {
            var f = this;
            this.element.children("li:not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "menuitem").children("a").addClass("ui-corner-all").attr("tabindex", -1).mouseenter(function (i) {
                f.activate(i, a(this).parent())
            }).mouseleave(function () {
                f.deactivate()
            })
        },
        activate: function (f, i) {
            this.deactivate();
            if (this.hasScroll()) {
                var g = i.offset().top - this.element.offset().top,
                    h = this.element.scrollTop(),
                    j = this.element.height();
                if (g < 0) this.element.scrollTop(h + g);
                else g >= j && this.element.scrollTop(h + g - j + i.height())
            }
            this.active = i.eq(0).children("a").addClass("ui-state-hover").attr("id", "ui-active-menuitem").end();
            this._trigger("focus", f, {
                item: i
            })
        },
        deactivate: function () {
            if (this.active) {
                this.active.children("a").removeClass("ui-state-hover").removeAttr("id");
                this._trigger("blur");
                this.active = null
            }
        },
        next: function (f) {
            this.move("next", ".ui-menu-item:first", f)
        },
        previous: function (f) {
            this.move("prev", ".ui-menu-item:last", f)
        },
        first: function () {
            return this.active && !this.active.prevAll(".ui-menu-item").length
        },
        last: function () {
            return this.active && !this.active.nextAll(".ui-menu-item").length
        },
        move: function (f, i, g) {
            if (this.active) {
                f = this.active[f + "All"](".ui-menu-item").eq(0);
                f.length ? this.activate(g, f) : this.activate(g, this.element.children(i))
            } else this.activate(g, this.element.children(i))
        },
        nextPage: function (f) {
            if (this.hasScroll()) if (!this.active || this.last()) this.activate(f, this.element.children(".ui-menu-item:first"));
            else {
                var i = this.active.offset().top,
                    g = this.element.height(),
                    h = this.element.children(".ui-menu-item").filter(function () {
                        var j = a(this).offset().top - i - g + a(this).height();
                        return j < 10 && j > -10
                    });
                h.length || (h = this.element.children(".ui-menu-item:last"));
                this.activate(f, h)
            } else this.activate(f, this.element.children(".ui-menu-item").filter(!this.active || this.last() ? ":first" : ":last"))
        },
        previousPage: function (f) {
            if (this.hasScroll()) if (!this.active || this.first()) this.activate(f, this.element.children(".ui-menu-item:last"));
            else {
                var i = this.active.offset().top,
                    g = this.element.height();
                result = this.element.children(".ui-menu-item").filter(function () {
                    var h = a(this).offset().top - i + g - a(this).height();
                    return h < 10 && h > -10
                });
                result.length || (result = this.element.children(".ui-menu-item:first"));
                this.activate(f, result)
            } else this.activate(f, this.element.children(".ui-menu-item").filter(!this.active || this.first() ? ":last" : ":first"))
        },
        hasScroll: function () {
            return this.element.height() < this.element[a.fn.prop ? "prop" : "attr"]("scrollHeight")
        },
        select: function (f) {
            this._trigger("selected", f, {
                item: this.active
            })
        }
    })
})(jQuery);
(function (a) {
    var f, i, g, h, j = function () {
            var c = a(this).find(":ui-button");
            setTimeout(function () {
                c.button("refresh")
            }, 1)
        },
        l = function (c) {
            var k = c.name,
                q = c.form,
                s = a([]);
            if (k) s = q ? a(q).find("[name='" + k + "']") : a("[name='" + k + "']", c.ownerDocument).filter(function () {
                return !this.form
            });
            return s
        };
    a.widget("ui.button", {
        options: {
            disabled: null,
            text: true,
            label: null,
            icons: {
                primary: null,
                secondary: null
            }
        },
        _create: function () {
            this.element.closest("form").unbind("reset.button").bind("reset.button", j);
            if (typeof this.options.disabled !== "boolean") this.options.disabled = !! this.element.propAttr("disabled");
            else this.element.propAttr("disabled", this.options.disabled);
            this._determineButtonType();
            this.hasTitle = !! this.buttonElement.attr("title");
            var c = this,
                k = this.options,
                q = this.type === "checkbox" || this.type === "radio",
                s = "ui-state-hover" + (!q ? " ui-state-active" : "");
            if (k.label === null) k.label = this.buttonElement.html();
            this.buttonElement.addClass("ui-button ui-widget ui-state-default ui-corner-all").attr("role", "button").bind("mouseenter.button", function () {
                if (!k.disabled) {
                    a(this).addClass("ui-state-hover");
                    this === f && a(this).addClass("ui-state-active")
                }
            }).bind("mouseleave.button", function () {
                k.disabled || a(this).removeClass(s)
            }).bind("click.button", function (p) {
                if (k.disabled) {
                    p.preventDefault();
                    p.stopImmediatePropagation()
                }
            });
            this.element.bind("focus.button", function () {
                c.buttonElement.addClass("ui-state-focus")
            }).bind("blur.button", function () {
                c.buttonElement.removeClass("ui-state-focus")
            });
            if (q) {
                this.element.bind("change.button", function () {
                    h || c.refresh()
                });
                this.buttonElement.bind("mousedown.button", function (p) {
                    if (!k.disabled) {
                        h = false;
                        i = p.pageX;
                        g = p.pageY
                    }
                }).bind("mouseup.button", function (p) {
                    if (!k.disabled) if (i !== p.pageX || g !== p.pageY) h = true
                })
            }
            if (this.type === "checkbox") this.buttonElement.bind("click.button", function () {
                if (k.disabled || h) return false;
                a(this).toggleClass("ui-state-active");
                c.buttonElement.attr("aria-pressed", c.element[0].checked)
            });
            else if (this.type === "radio") this.buttonElement.bind("click.button", function () {
                if (k.disabled || h) return false;
                a(this).addClass("ui-state-active");
                c.buttonElement.attr("aria-pressed", "true");
                var p = c.element[0];
                l(p).not(p).map(function () {
                    return a(this).button("widget")[0]
                }).removeClass("ui-state-active").attr("aria-pressed", "false")
            });
            else {
                this.buttonElement.bind("mousedown.button", function () {
                    if (k.disabled) return false;
                    a(this).addClass("ui-state-active");
                    f = this;
                    a(document).one("mouseup", function () {
                        f = null
                    })
                }).bind("mouseup.button", function () {
                    if (k.disabled) return false;
                    a(this).removeClass("ui-state-active")
                }).bind("keydown.button", function (p) {
                    if (k.disabled) return false;
                    if (p.keyCode == a.ui.keyCode.SPACE || p.keyCode == a.ui.keyCode.ENTER) a(this).addClass("ui-state-active")
                }).bind("keyup.button", function () {
                    a(this).removeClass("ui-state-active")
                });
                this.buttonElement.is("a") && this.buttonElement.keyup(function (p) {
                    p.keyCode === a.ui.keyCode.SPACE && a(this).click()
                })
            }
            this._setOption("disabled", k.disabled);
            this._resetButton()
        },
        _determineButtonType: function () {
            this.type = this.element.is(":checkbox") ? "checkbox" : this.element.is(":radio") ? "radio" : this.element.is("input") ? "input" : "button";
            if (this.type === "checkbox" || this.type === "radio") {
                var c = this.element.parents().filter(":last"),
                    k = "label[for='" + this.element.attr("id") + "']";
                this.buttonElement = c.find(k);
                if (!this.buttonElement.length) {
                    c = c.length ? c.siblings() : this.element.siblings();
                    this.buttonElement = c.filter(k);
                    if (!this.buttonElement.length) this.buttonElement = c.find(k)
                }
                this.element.addClass("ui-helper-hidden-accessible");
                (c = this.element.is(":checked")) && this.buttonElement.addClass("ui-state-active");
                this.buttonElement.attr("aria-pressed", c)
            } else this.buttonElement = this.element
        },
        widget: function () {
            return this.buttonElement
        },
        destroy: function () {
            this.element.removeClass("ui-helper-hidden-accessible");
            this.buttonElement.removeClass("ui-button ui-widget ui-state-default ui-corner-all ui-state-hover ui-state-active  ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only").removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());
            this.hasTitle || this.buttonElement.removeAttr("title");
            a.Widget.prototype.destroy.call(this)
        },
        _setOption: function (c, k) {
            a.Widget.prototype._setOption.apply(this, arguments);
            if (c === "disabled") k ? this.element.propAttr("disabled", true) : this.element.propAttr("disabled", false);
            else this._resetButton()
        },
        refresh: function () {
            var c = this.element.is(":disabled");
            c !== this.options.disabled && this._setOption("disabled", c);
            if (this.type === "radio") l(this.element[0]).each(function () {
                a(this).is(":checked") ? a(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : a(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false")
            });
            else if (this.type === "checkbox") this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false")
        },
        _resetButton: function () {
            if (this.type === "input") this.options.label && this.element.val(this.options.label);
            else {
                var c = this.buttonElement.removeClass("ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only"),
                    k = a("<span></span>", this.element[0].ownerDocument).addClass("ui-button-text").html(this.options.label).appendTo(c.empty()).text(),
                    q = this.options.icons,
                    s = q.primary && q.secondary,
                    p = [];
                if (q.primary || q.secondary) {
                    if (this.options.text) p.push("ui-button-text-icon" + (s ? "s" : q.primary ? "-primary" : "-secondary"));
                    q.primary && c.prepend("<span class='ui-button-icon-primary ui-icon " + q.primary + "'></span>");
                    q.secondary && c.append("<span class='ui-button-icon-secondary ui-icon " + q.secondary + "'></span>");
                    if (!this.options.text) {
                        p.push(s ? "ui-button-icons-only" : "ui-button-icon-only");
                        this.hasTitle || c.attr("title", k)
                    }
                } else p.push("ui-button-text-only");
                c.addClass(p.join(" "))
            }
        }
    });
    a.widget("ui.buttonset", {
        options: {
            items: ":button, :submit, :reset, :checkbox, :radio, a, :data(button)"
        },
        _create: function () {
            this.element.addClass("ui-buttonset")
        },
        _init: function () {
            this.refresh()
        },
        _setOption: function (c, k) {
            c === "disabled" && this.buttons.button("option", c, k);
            a.Widget.prototype._setOption.apply(this, arguments)
        },
        refresh: function () {
            var c = this.element.css("direction") === "rtl";
            this.buttons = this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function () {
                return a(this).button("widget")[0]
            }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(c ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(c ? "ui-corner-left" : "ui-corner-right").end().end()
        },
        destroy: function () {
            this.element.removeClass("ui-buttonset");
            this.buttons.map(function () {
                return a(this).button("widget")[0]
            }).removeClass("ui-corner-left ui-corner-right").end().button("destroy");
            a.Widget.prototype.destroy.call(this)
        }
    })
})(jQuery);
(function (a, f) {
    var i = {
        buttons: true,
        height: true,
        maxHeight: true,
        maxWidth: true,
        minHeight: true,
        minWidth: true,
        width: true
    },
        g = {
            maxHeight: true,
            maxWidth: true,
            minHeight: true,
            minWidth: true
        },
        h = a.attrFn || {
            val: true,
            css: true,
            html: true,
            text: true,
            data: true,
            width: true,
            height: true,
            offset: true,
            click: true
        };
    a.widget("ui.dialog", {
        options: {
            autoOpen: true,
            buttons: {},
            closeOnEscape: true,
            closeText: "close",
            dialogClass: "",
            draggable: true,
            hide: null,
            height: "auto",
            maxHeight: false,
            maxWidth: false,
            minHeight: 150,
            minWidth: 150,
            modal: false,
            position: {
                my: "center",
                at: "center",
                collision: "fit",
                using: function (j) {
                    var l = a(this).css(j).offset().top;
                    l < 0 && a(this).css("top", j.top - l)
                }
            },
            resizable: true,
            show: null,
            stack: true,
            title: "",
            width: 300,
            zIndex: 1E3
        },
        _create: function () {
            this.originalTitle = this.element.attr("title");
            if (typeof this.originalTitle !== "string") this.originalTitle = "";
            this.options.title = this.options.title || this.originalTitle;
            var j = this,
                l = j.options,
                c = l.title || "&#160;",
                k = a.ui.dialog.getTitleId(j.element),
                q = (j.uiDialog = a("<div></div>")).appendTo(document.body).hide().addClass("ui-dialog ui-widget ui-widget-content ui-corner-all " + l.dialogClass).css({
                    zIndex: l.zIndex
                }).attr("tabIndex", -1).css("outline", 0).keydown(function (n) {
                    if (l.closeOnEscape && !n.isDefaultPrevented() && n.keyCode && n.keyCode === a.ui.keyCode.ESCAPE) {
                        j.close(n);
                        n.preventDefault()
                    }
                }).attr({
                    role: "dialog",
                    "aria-labelledby": k
                }).mousedown(function (n) {
                    j.moveToTop(false, n)
                });
            j.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(q);
            var s = (j.uiDialogTitlebar = a("<div></div>")).addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(q),
                p = a('<a href="#"></a>').addClass("ui-dialog-titlebar-close ui-corner-all").attr("role", "button").hover(function () {
                    p.addClass("ui-state-hover")
                }, function () {
                    p.removeClass("ui-state-hover")
                }).focus(function () {
                    p.addClass("ui-state-focus")
                }).blur(function () {
                    p.removeClass("ui-state-focus")
                }).click(function (n) {
                    j.close(n);
                    return false
                }).appendTo(s);
            (j.uiDialogTitlebarCloseText = a("<span></span>")).addClass("ui-icon ui-icon-closethick").text(l.closeText).appendTo(p);
            a("<span></span>").addClass("ui-dialog-title").attr("id", k).html(c).prependTo(s);
            if (a.isFunction(l.beforeclose) && !a.isFunction(l.beforeClose)) l.beforeClose = l.beforeclose;
            s.find("*").add(s).disableSelection();
            l.draggable && a.fn.draggable && j._makeDraggable();
            l.resizable && a.fn.resizable && j._makeResizable();
            j._createButtons(l.buttons);
            j._isOpen = false;
            a.fn.bgiframe && q.bgiframe()
        },
        _init: function () {
            this.options.autoOpen && this.open()
        },
        destroy: function () {
            this.overlay && this.overlay.destroy();
            this.uiDialog.hide();
            this.element.unbind(".dialog").removeData("dialog").removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body");
            this.uiDialog.remove();
            this.originalTitle && this.element.attr("title", this.originalTitle);
            return this
        },
        widget: function () {
            return this.uiDialog
        },
        close: function (j) {
            var l = this,
                c, k;
            if (false !== l._trigger("beforeClose", j)) {
                l.overlay && l.overlay.destroy();
                l.uiDialog.unbind("keypress.ui-dialog");
                l._isOpen = false;
                if (l.options.hide) l.uiDialog.hide(l.options.hide, function () {
                    l._trigger("close", j)
                });
                else {
                    l.uiDialog.hide();
                    l._trigger("close", j)
                }
                a.ui.dialog.overlay.resize();
                if (l.options.modal) {
                    c = 0;
                    a(".ui-dialog").each(function () {
                        if (this !== l.uiDialog[0]) {
                            k = a(this).css("z-index");
                            isNaN(k) || (c = Math.max(c, k))
                        }
                    });
                    a.ui.dialog.maxZ = c
                }
                return l
            }
        },
        isOpen: function () {
            return this._isOpen
        },
        moveToTop: function (j, l) {
            var c = this.options;
            if (c.modal && !j || !c.stack && !c.modal) return this._trigger("focus", l);
            if (c.zIndex > a.ui.dialog.maxZ) a.ui.dialog.maxZ = c.zIndex;
            if (this.overlay) {
                a.ui.dialog.maxZ += 1;
                this.overlay.$el.css("z-index", a.ui.dialog.overlay.maxZ = a.ui.dialog.maxZ)
            }
            c = {
                scrollTop: this.element.scrollTop(),
                scrollLeft: this.element.scrollLeft()
            };
            a.ui.dialog.maxZ += 1;
            this.uiDialog.css("z-index", a.ui.dialog.maxZ);
            this.element.attr(c);
            this._trigger("focus", l);
            return this
        },
        open: function () {
            if (!this._isOpen) {
                var j = this.options,
                    l = this.uiDialog;
                this.overlay = j.modal ? new a.ui.dialog.overlay(this) : null;
                this._size();
                this._position(j.position);
                l.show(j.show);
                this.moveToTop(true);
                j.modal && l.bind("keydown.ui-dialog", function (c) {
                    if (c.keyCode === a.ui.keyCode.TAB) {
                        var k = a(":tabbable", this),
                            q = k.filter(":first");
                        k = k.filter(":last");
                        if (c.target === k[0] && !c.shiftKey) {
                            q.focus(1);
                            return false
                        } else if (c.target === q[0] && c.shiftKey) {
                            k.focus(1);
                            return false
                        }
                    }
                });
                a(this.element.find(":tabbable").get().concat(l.find(".ui-dialog-buttonpane :tabbable").get().concat(l.get()))).eq(0).focus();
                this._isOpen = true;
                this._trigger("open");
                return this
            }
        },
        _createButtons: function (j) {
            var l = this,
                c = false,
                k = a("<div></div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),
                q = a("<div></div>").addClass("ui-dialog-buttonset").appendTo(k);
            l.uiDialog.find(".ui-dialog-buttonpane").remove();
            typeof j === "object" && j !== null && a.each(j, function () {
                return !(c = true)
            });
            if (c) {
                a.each(j, function (s, p) {
                    p = a.isFunction(p) ? {
                        click: p,
                        text: s
                    } : p;
                    var n = a('<button type="button"></button>').click(function () {
                        p.click.apply(l.element[0], arguments)
                    }).appendTo(q);
                    a.each(p, function (u, z) {
                        if (u !== "click") if (u in h) n[u](z);
                        else n.attr(u, z)
                    });
                    a.fn.button && n.button()
                });
                k.appendTo(l.uiDialog)
            }
        },
        _makeDraggable: function () {
            function j(s) {
                return {
                    position: s.position,
                    offset: s.offset
                }
            }
            var l = this,
                c = l.options,
                k = a(document),
                q;
            l.uiDialog.draggable({
                cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                handle: ".ui-dialog-titlebar",
                containment: "document",
                start: function (s, p) {
                    q = c.height === "auto" ? "auto" : a(this).height();
                    a(this).height(a(this).height()).addClass("ui-dialog-dragging");
                    l._trigger("dragStart", s, j(p))
                },
                drag: function (s, p) {
                    l._trigger("drag", s, j(p))
                },
                stop: function (s, p) {
                    c.position = [p.position.left - k.scrollLeft(), p.position.top - k.scrollTop()];
                    a(this).removeClass("ui-dialog-dragging").height(q);
                    l._trigger("dragStop", s, j(p));
                    a.ui.dialog.overlay.resize()
                }
            })
        },
        _makeResizable: function (j) {
            function l(s) {
                return {
                    originalPosition: s.originalPosition,
                    originalSize: s.originalSize,
                    position: s.position,
                    size: s.size
                }
            }
            j = j === f ? this.options.resizable : j;
            var c = this,
                k = c.options,
                q = c.uiDialog.css("position");
            j = typeof j === "string" ? j : "n,e,s,w,se,sw,ne,nw";
            c.uiDialog.resizable({
                cancel: ".ui-dialog-content",
                containment: "document",
                alsoResize: c.element,
                maxWidth: k.maxWidth,
                maxHeight: k.maxHeight,
                minWidth: k.minWidth,
                minHeight: c._minHeight(),
                handles: j,
                start: function (s, p) {
                    a(this).addClass("ui-dialog-resizing");
                    c._trigger("resizeStart", s, l(p))
                },
                resize: function (s, p) {
                    c._trigger("resize", s, l(p))
                },
                stop: function (s, p) {
                    a(this).removeClass("ui-dialog-resizing");
                    k.height = a(this).height();
                    k.width = a(this).width();
                    c._trigger("resizeStop", s, l(p));
                    a.ui.dialog.overlay.resize()
                }
            }).css("position", q).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")
        },
        _minHeight: function () {
            var j = this.options;
            return j.height === "auto" ? j.minHeight : Math.min(j.minHeight, j.height)
        },
        _position: function (j) {
            var l = [],
                c = [0, 0],
                k;
            if (j) {
                if (typeof j === "string" || typeof j === "object" && "0" in j) {
                    l = j.split ? j.split(" ") : [j[0], j[1]];
                    if (l.length === 1) l[1] = l[0];
                    a.each(["left", "top"], function (q, s) {
                        if (+l[q] === l[q]) {
                            c[q] = l[q];
                            l[q] = s
                        }
                    });
                    j = {
                        my: l.join(" "),
                        at: l.join(" "),
                        offset: c.join(" ")
                    }
                }
                j = a.extend({}, a.ui.dialog.prototype.options.position, j)
            } else j = a.ui.dialog.prototype.options.position;
            (k = this.uiDialog.is(":visible")) || this.uiDialog.show();
            this.uiDialog.css({
                top: 0,
                left: 0
            }).position(a.extend({
                of: window
            }, j));
            k || this.uiDialog.hide()
        },
        _setOptions: function (j) {
            var l = this,
                c = {},
                k = false;
            a.each(j, function (q, s) {
                l._setOption(q, s);
                if (q in i) k = true;
                if (q in g) c[q] = s
            });
            k && this._size();
            this.uiDialog.is(":data(resizable)") && this.uiDialog.resizable("option", c)
        },
        _setOption: function (j, l) {
            var c = this.uiDialog;
            switch (j) {
            case "beforeclose":
                j = "beforeClose";
                break;
            case "buttons":
                this._createButtons(l);
                break;
            case "closeText":
                this.uiDialogTitlebarCloseText.text("" + l);
                break;
            case "dialogClass":
                c.removeClass(this.options.dialogClass).addClass("ui-dialog ui-widget ui-widget-content ui-corner-all " + l);
                break;
            case "disabled":
                l ? c.addClass("ui-dialog-disabled") : c.removeClass("ui-dialog-disabled");
                break;
            case "draggable":
                var k = c.is(":data(draggable)");
                k && !l && c.draggable("destroy");
                !k && l && this._makeDraggable();
                break;
            case "position":
                this._position(l);
                break;
            case "resizable":
                (k = c.is(":data(resizable)")) && !l && c.resizable("destroy");
                k && typeof l === "string" && c.resizable("option", "handles", l);
                !k && l !== false && this._makeResizable(l);
                break;
            case "title":
                a(".ui-dialog-title", this.uiDialogTitlebar).html("" + (l || "&#160;"))
            }
            a.Widget.prototype._setOption.apply(this, arguments)
        },
        _size: function () {
            var j = this.options,
                l, c, k = this.uiDialog.is(":visible");
            this.element.show().css({
                width: "auto",
                minHeight: 0,
                height: 0
            });
            if (j.minWidth > j.width) j.width = j.minWidth;
            l = this.uiDialog.css({
                height: "auto",
                width: j.width
            }).height();
            c = Math.max(0, j.minHeight - l);
            if (j.height === "auto") if (a.support.minHeight) this.element.css({
                minHeight: c,
                height: "auto"
            });
            else {
                this.uiDialog.show();
                j = this.element.css("height", "auto").height();
                k || this.uiDialog.hide();
                this.element.height(Math.max(j, c))
            } else this.element.height(Math.max(j.height - l, 0));
            this.uiDialog.is(":data(resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
        }
    });
    a.extend(a.ui.dialog, {
        version: "1.8.18",
        uuid: 0,
        maxZ: 0,
        getTitleId: function (j) {
            j = j.attr("id");
            if (!j) {
                this.uuid += 1;
                j = this.uuid
            }
            return "ui-dialog-title-" + j
        },
        overlay: function (j) {
            this.$el = a.ui.dialog.overlay.create(j)
        }
    });
    a.extend(a.ui.dialog.overlay, {
        instances: [],
        oldInstances: [],
        maxZ: 0,
        events: a.map("focus,mousedown,mouseup,keydown,keypress,click".split(","), function (j) {
            return j + ".dialog-overlay"
        }).join(" "),
        create: function (j) {
            if (this.instances.length === 0) {
                setTimeout(function () {
                    a.ui.dialog.overlay.instances.length && a(document).bind(a.ui.dialog.overlay.events, function (c) {
                        if (a(c.target).zIndex() < a.ui.dialog.overlay.maxZ) return false
                    })
                }, 1);
                a(document).bind("keydown.dialog-overlay", function (c) {
                    if (j.options.closeOnEscape && !c.isDefaultPrevented() && c.keyCode && c.keyCode === a.ui.keyCode.ESCAPE) {
                        j.close(c);
                        c.preventDefault()
                    }
                });
                a(window).bind("resize.dialog-overlay", a.ui.dialog.overlay.resize)
            }
            var l = (this.oldInstances.pop() || a("<div></div>").addClass("ui-widget-overlay")).appendTo(document.body).css({
                width: this.width(),
                height: this.height()
            });
            a.fn.bgiframe && l.bgiframe();
            this.instances.push(l);
            return l
        },
        destroy: function (j) {
            var l = a.inArray(j, this.instances);
            l != -1 && this.oldInstances.push(this.instances.splice(l, 1)[0]);
            this.instances.length === 0 && a([document, window]).unbind(".dialog-overlay");
            j.remove();
            var c = 0;
            a.each(this.instances, function () {
                c = Math.max(c, this.css("z-index"))
            });
            this.maxZ = c
        },
        height: function () {
            var j, l;
            if (a.browser.msie && a.browser.version < 7) {
                j = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
                l = Math.max(document.documentElement.offsetHeight, document.body.offsetHeight);
                return j < l ? a(window).height() + "px" : j + "px"
            } else return a(document).height() + "px"
        },
        width: function () {
            var j, l;
            if (a.browser.msie) {
                j = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth);
                l = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);
                return j < l ? a(window).width() + "px" : j + "px"
            } else return a(document).width() + "px"
        },
        resize: function () {
            var j = a([]);
            a.each(a.ui.dialog.overlay.instances, function () {
                j = j.add(this)
            });
            j.css({
                width: 0,
                height: 0
            }).css({
                width: a.ui.dialog.overlay.width(),
                height: a.ui.dialog.overlay.height()
            })
        }
    });
    a.extend(a.ui.dialog.overlay.prototype, {
        destroy: function () {
            a.ui.dialog.overlay.destroy(this.$el)
        }
    })
})(jQuery);
(function (a) {
    a.widget("ui.slider", a.ui.mouse, {
        widgetEventPrefix: "slide",
        options: {
            animate: false,
            distance: 0,
            max: 100,
            min: 0,
            orientation: "horizontal",
            range: false,
            step: 1,
            value: 0,
            values: null
        },
        _create: function () {
            var f = this,
                i = this.options,
                g = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
                h = i.values && i.values.length || 1,
                j = [];
            this._mouseSliding = this._keySliding = false;
            this._animateOff = true;
            this._handleIndex = null;
            this._detectOrientation();
            this._mouseInit();
            this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all" + (i.disabled ? " ui-slider-disabled ui-disabled" : ""));
            this.range = a([]);
            if (i.range) {
                if (i.range === true) {
                    if (!i.values) i.values = [this._valueMin(), this._valueMin()];
                    if (i.values.length && i.values.length !== 2) i.values = [i.values[0], i.values[0]]
                }
                this.range = a("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header" + (i.range === "min" || i.range === "max" ? " ui-slider-range-" + i.range : ""))
            }
            for (var l = g.length; l < h; l += 1) j.push("<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>");
            this.handles = g.add(a(j.join("")).appendTo(f.element));
            this.handle = this.handles.eq(0);
            this.handles.add(this.range).filter("a").click(function (c) {
                c.preventDefault()
            }).hover(function () {
                i.disabled || a(this).addClass("ui-state-hover")
            }, function () {
                a(this).removeClass("ui-state-hover")
            }).focus(function () {
                if (i.disabled) a(this).blur();
                else {
                    a(".ui-slider .ui-state-focus").removeClass("ui-state-focus");
                    a(this).addClass("ui-state-focus")
                }
            }).blur(function () {
                a(this).removeClass("ui-state-focus")
            });
            this.handles.each(function (c) {
                a(this).data("index.ui-slider-handle", c)
            });
            this.handles.keydown(function (c) {
                var k = a(this).data("index.ui-slider-handle"),
                    q, s, p;
                if (!f.options.disabled) {
                    switch (c.keyCode) {
                    case a.ui.keyCode.HOME:
                    case a.ui.keyCode.END:
                    case a.ui.keyCode.PAGE_UP:
                    case a.ui.keyCode.PAGE_DOWN:
                    case a.ui.keyCode.UP:
                    case a.ui.keyCode.RIGHT:
                    case a.ui.keyCode.DOWN:
                    case a.ui.keyCode.LEFT:
                        c.preventDefault();
                        if (!f._keySliding) {
                            f._keySliding = true;
                            a(this).addClass("ui-state-active");
                            q = f._start(c, k);
                            if (q === false) return
                        }
                    }
                    p = f.options.step;
                    q = f.options.values && f.options.values.length ? s = f.values(k) : s = f.value();
                    switch (c.keyCode) {
                    case a.ui.keyCode.HOME:
                        s = f._valueMin();
                        break;
                    case a.ui.keyCode.END:
                        s = f._valueMax();
                        break;
                    case a.ui.keyCode.PAGE_UP:
                        s = f._trimAlignValue(q + (f._valueMax() - f._valueMin()) / 5);
                        break;
                    case a.ui.keyCode.PAGE_DOWN:
                        s = f._trimAlignValue(q - (f._valueMax() - f._valueMin()) / 5);
                        break;
                    case a.ui.keyCode.UP:
                    case a.ui.keyCode.RIGHT:
                        if (q === f._valueMax()) return;
                        s = f._trimAlignValue(q + p);
                        break;
                    case a.ui.keyCode.DOWN:
                    case a.ui.keyCode.LEFT:
                        if (q === f._valueMin()) return;
                        s = f._trimAlignValue(q - p)
                    }
                    f._slide(c, k, s)
                }
            }).keyup(function (c) {
                var k = a(this).data("index.ui-slider-handle");
                if (f._keySliding) {
                    f._keySliding = false;
                    f._stop(c, k);
                    f._change(c, k);
                    a(this).removeClass("ui-state-active")
                }
            });
            this._refreshValue();
            this._animateOff = false
        },
        destroy: function () {
            this.handles.remove();
            this.range.remove();
            this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all").removeData("slider").unbind(".slider");
            this._mouseDestroy();
            return this
        },
        _mouseCapture: function (f) {
            var i = this.options,
                g, h, j, l, c;
            if (i.disabled) return false;
            this.elementSize = {
                width: this.element.outerWidth(),
                height: this.element.outerHeight()
            };
            this.elementOffset = this.element.offset();
            g = this._normValueFromMouse({
                x: f.pageX,
                y: f.pageY
            });
            h = this._valueMax() - this._valueMin() + 1;
            l = this;
            this.handles.each(function (k) {
                var q = Math.abs(g - l.values(k));
                if (h > q) {
                    h = q;
                    j = a(this);
                    c = k
                }
            });
            if (i.range === true && this.values(1) === i.min) {
                c += 1;
                j = a(this.handles[c])
            }
            if (this._start(f, c) === false) return false;
            this._mouseSliding = true;
            l._handleIndex = c;
            j.addClass("ui-state-active").focus();
            i = j.offset();
            this._clickOffset = !a(f.target).parents().andSelf().is(".ui-slider-handle") ? {
                left: 0,
                top: 0
            } : {
                left: f.pageX - i.left - j.width() / 2,
                top: f.pageY - i.top - j.height() / 2 - (parseInt(j.css("borderTopWidth"), 10) || 0) - (parseInt(j.css("borderBottomWidth"), 10) || 0) + (parseInt(j.css("marginTop"), 10) || 0)
            };
            this.handles.hasClass("ui-state-hover") || this._slide(f, c, g);
            return this._animateOff = true
        },
        _mouseStart: function () {
            return true
        },
        _mouseDrag: function (f) {
            var i = this._normValueFromMouse({
                x: f.pageX,
                y: f.pageY
            });
            this._slide(f, this._handleIndex, i);
            return false
        },
        _mouseStop: function (f) {
            this.handles.removeClass("ui-state-active");
            this._mouseSliding = false;
            this._stop(f, this._handleIndex);
            this._change(f, this._handleIndex);
            this._clickOffset = this._handleIndex = null;
            return this._animateOff = false
        },
        _detectOrientation: function () {
            this.orientation = this.options.orientation === "vertical" ? "vertical" : "horizontal"
        },
        _normValueFromMouse: function (f) {
            var i;
            if (this.orientation === "horizontal") {
                i = this.elementSize.width;
                f = f.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)
            } else {
                i = this.elementSize.height;
                f = f.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)
            }
            i = f / i;
            if (i > 1) i = 1;
            if (i < 0) i = 0;
            if (this.orientation === "vertical") i = 1 - i;
            f = this._valueMax() - this._valueMin();
            return this._trimAlignValue(this._valueMin() + i * f)
        },
        _start: function (f, i) {
            var g = {
                handle: this.handles[i],
                value: this.value()
            };
            if (this.options.values && this.options.values.length) {
                g.value = this.values(i);
                g.values = this.values()
            }
            return this._trigger("start", f, g)
        },
        _slide: function (f, i, g) {
            var h;
            if (this.options.values && this.options.values.length) {
                h = this.values(i ? 0 : 1);
                if (this.options.values.length === 2 && this.options.range === true && (i === 0 && g > h || i === 1 && g < h)) g = h;
                if (g !== this.values(i)) {
                    h = this.values();
                    h[i] = g;
                    f = this._trigger("slide", f, {
                        handle: this.handles[i],
                        value: g,
                        values: h
                    });
                    this.values(i ? 0 : 1);
                    f !== false && this.values(i, g, true)
                }
            } else if (g !== this.value()) {
                f = this._trigger("slide", f, {
                    handle: this.handles[i],
                    value: g
                });
                f !== false && this.value(g)
            }
        },
        _stop: function (f, i) {
            var g = {
                handle: this.handles[i],
                value: this.value()
            };
            if (this.options.values && this.options.values.length) {
                g.value = this.values(i);
                g.values = this.values()
            }
            this._trigger("stop", f, g)
        },
        _change: function (f, i) {
            if (!this._keySliding && !this._mouseSliding) {
                var g = {
                    handle: this.handles[i],
                    value: this.value()
                };
                if (this.options.values && this.options.values.length) {
                    g.value = this.values(i);
                    g.values = this.values()
                }
                this._trigger("change", f, g)
            }
        },
        value: function (f) {
            if (arguments.length) {
                this.options.value = this._trimAlignValue(f);
                this._refreshValue();
                this._change(null, 0)
            } else return this._value()
        },
        values: function (f, i) {
            var g, h, j;
            if (arguments.length > 1) {
                this.options.values[f] = this._trimAlignValue(i);
                this._refreshValue();
                this._change(null, f)
            } else if (arguments.length) if (a.isArray(arguments[0])) {
                g = this.options.values;
                h = arguments[0];
                for (j = 0; j < g.length; j += 1) {
                    g[j] = this._trimAlignValue(h[j]);
                    this._change(null, j)
                }
                this._refreshValue()
            } else return this.options.values && this.options.values.length ? this._values(f) : this.value();
            else return this._values()
        },
        _setOption: function (f, i) {
            var g, h = 0;
            if (a.isArray(this.options.values)) h = this.options.values.length;
            a.Widget.prototype._setOption.apply(this, arguments);
            switch (f) {
            case "disabled":
                if (i) {
                    this.handles.filter(".ui-state-focus").blur();
                    this.handles.removeClass("ui-state-hover");
                    this.handles.propAttr("disabled", true);
                    this.element.addClass("ui-disabled")
                } else {
                    this.handles.propAttr("disabled", false);
                    this.element.removeClass("ui-disabled")
                }
                break;
            case "orientation":
                this._detectOrientation();
                this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation);
                this._refreshValue();
                break;
            case "value":
                this._animateOff = true;
                this._refreshValue();
                this._change(null, 0);
                this._animateOff = false;
                break;
            case "values":
                this._animateOff = true;
                this._refreshValue();
                for (g = 0; g < h; g += 1) this._change(null, g);
                this._animateOff = false
            }
        },
        _value: function () {
            var f = this.options.value;
            return f = this._trimAlignValue(f)
        },
        _values: function (f) {
            var i, g;
            if (arguments.length) {
                i = this.options.values[f];
                i = this._trimAlignValue(i)
            } else {
                i = this.options.values.slice();
                for (g = 0; g < i.length; g += 1) i[g] = this._trimAlignValue(i[g])
            }
            return i
        },
        _trimAlignValue: function (f) {
            if (f <= this._valueMin()) return this._valueMin();
            if (f >= this._valueMax()) return this._valueMax();
            var i = this.options.step > 0 ? this.options.step : 1,
                g = (f - this._valueMin()) % i;
            f -= g;
            if (Math.abs(g) * 2 >= i) f += g > 0 ? i : -i;
            return parseFloat(f.toFixed(5))
        },
        _valueMin: function () {
            return this.options.min
        },
        _valueMax: function () {
            return this.options.max
        },
        _refreshValue: function () {
            var f = this.options.range,
                i = this.options,
                g = this,
                h = !this._animateOff ? i.animate : false,
                j, l = {},
                c, k, q, s;
            if (this.options.values && this.options.values.length) this.handles.each(function (p) {
                j = (g.values(p) - g._valueMin()) / (g._valueMax() - g._valueMin()) * 100;
                l[g.orientation === "horizontal" ? "left" : "bottom"] = j + "%";
                a(this).stop(1, 1)[h ? "animate" : "css"](l, i.animate);
                if (g.options.range === true) if (g.orientation === "horizontal") {
                    if (p === 0) g.range.stop(1, 1)[h ? "animate" : "css"]({
                        left: j + "%"
                    }, i.animate);
                    if (p === 1) g.range[h ? "animate" : "css"]({
                        width: j - c + "%"
                    }, {
                        queue: false,
                        duration: i.animate
                    })
                } else {
                    if (p === 0) g.range.stop(1, 1)[h ? "animate" : "css"]({
                        bottom: j + "%"
                    }, i.animate);
                    if (p === 1) g.range[h ? "animate" : "css"]({
                        height: j - c + "%"
                    }, {
                        queue: false,
                        duration: i.animate
                    })
                }
                c = j
            });
            else {
                k = this.value();
                q = this._valueMin();
                s = this._valueMax();
                j = s !== q ? (k - q) / (s - q) * 100 : 0;
                l[g.orientation === "horizontal" ? "left" : "bottom"] = j + "%";
                this.handle.stop(1, 1)[h ? "animate" : "css"](l, i.animate);
                if (f === "min" && this.orientation === "horizontal") this.range.stop(1, 1)[h ? "animate" : "css"]({
                    width: j + "%"
                }, i.animate);
                if (f === "max" && this.orientation === "horizontal") this.range[h ? "animate" : "css"]({
                    width: 100 - j + "%"
                }, {
                    queue: false,
                    duration: i.animate
                });
                if (f === "min" && this.orientation === "vertical") this.range.stop(1, 1)[h ? "animate" : "css"]({
                    height: j + "%"
                }, i.animate);
                if (f === "max" && this.orientation === "vertical") this.range[h ? "animate" : "css"]({
                    height: 100 - j + "%"
                }, {
                    queue: false,
                    duration: i.animate
                })
            }
        }
    });
    a.extend(a.ui.slider, {
        version: "1.8.18"
    })
})(jQuery);
(function (a, f) {
    var i = 0,
        g = 0;
    a.widget("ui.tabs", {
        options: {
            add: null,
            ajaxOptions: null,
            cache: false,
            cookie: null,
            collapsible: false,
            disable: null,
            disabled: [],
            enable: null,
            event: "click",
            fx: null,
            idPrefix: "ui-tabs-",
            load: null,
            panelTemplate: "<div></div>",
            remove: null,
            select: null,
            show: null,
            spinner: "<em>Loading&#8230;</em>",
            tabTemplate: "<li><a href='#{href}'><span>#{label}</span></a></li>"
        },
        _create: function () {
            this._tabify(true)
        },
        _setOption: function (h, j) {
            if (h == "selected") this.options.collapsible && j == this.options.selected || this.select(j);
            else {
                this.options[h] = j;
                this._tabify()
            }
        },
        _tabId: function (h) {
            return h.title && h.title.replace(/\s/g, "_").replace(/[^\w\u00c0-\uFFFF-]/g, "") || this.options.idPrefix + ++i
        },
        _sanitizeSelector: function (h) {
            return h.replace(/:/g, "\\:")
        },
        _cookie: function () {
            var h = this.cookie || (this.cookie = this.options.cookie.name || "ui-tabs-" + ++g);
            return a.cookie.apply(null, [h].concat(a.makeArray(arguments)))
        },
        _ui: function (h, j) {
            return {
                tab: h,
                panel: j,
                index: this.anchors.index(h)
            }
        },
        _cleanup: function () {
            this.lis.filter(".ui-state-processing").removeClass("ui-state-processing").find("span:data(label.tabs)").each(function () {
                var h = a(this);
                h.html(h.data("label.tabs")).removeData("label.tabs")
            })
        },
        _tabify: function (h) {
            function j(E, M) {
                E.css("display", "");
                !a.support.opacity && M.opacity && E[0].style.removeAttribute("filter")
            }
            var l = this,
                c = this.options,
                k = /^#.+/;
            this.list = this.element.find("ol,ul").eq(0);
            this.lis = a(" > li:has(a[href])", this.list);
            this.anchors = this.lis.map(function () {
                return a("a", this)[0]
            });
            this.panels = a([]);
            this.anchors.each(function (E, M) {
                var ma = a(M).attr("href"),
                    oa = ma.split("#")[0],
                    Da;
                if (oa && (oa === location.toString().split("#")[0] || (Da = a("base")[0]) && oa === Da.href)) {
                    ma = M.hash;
                    M.href = ma
                }
                if (k.test(ma)) l.panels = l.panels.add(l.element.find(l._sanitizeSelector(ma)));
                else if (ma && ma !== "#") {
                    a.data(M, "href.tabs", ma);
                    a.data(M, "load.tabs", ma.replace(/#.*$/, ""));
                    ma = l._tabId(M);
                    M.href = "#" + ma;
                    oa = l.element.find("#" + ma);
                    if (!oa.length) {
                        oa = a(c.panelTemplate).attr("id", ma).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").insertAfter(l.panels[E - 1] || l.list);
                        oa.data("destroy.tabs", true)
                    }
                    l.panels = l.panels.add(oa)
                } else c.disabled.push(E)
            });
            if (h) {
                this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all");
                this.list.addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
                this.lis.addClass("ui-state-default ui-corner-top");
                this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom");
                if (c.selected === f) {
                    location.hash && this.anchors.each(function (E, M) {
                        if (M.hash == location.hash) {
                            c.selected = E;
                            return false
                        }
                    });
                    if (typeof c.selected !== "number" && c.cookie) c.selected = parseInt(l._cookie(), 10);
                    if (typeof c.selected !== "number" && this.lis.filter(".ui-tabs-selected").length) c.selected = this.lis.index(this.lis.filter(".ui-tabs-selected"));
                    c.selected = c.selected || (this.lis.length ? 0 : -1)
                } else if (c.selected === null) c.selected = -1;
                c.selected = c.selected >= 0 && this.anchors[c.selected] || c.selected < 0 ? c.selected : 0;
                c.disabled = a.unique(c.disabled.concat(a.map(this.lis.filter(".ui-state-disabled"), function (E) {
                    return l.lis.index(E)
                }))).sort();
                a.inArray(c.selected, c.disabled) != -1 && c.disabled.splice(a.inArray(c.selected, c.disabled), 1);
                this.panels.addClass("ui-tabs-hide");
                this.lis.removeClass("ui-tabs-selected ui-state-active");
                if (c.selected >= 0 && this.anchors.length) {
                    l.element.find(l._sanitizeSelector(l.anchors[c.selected].hash)).removeClass("ui-tabs-hide");
                    this.lis.eq(c.selected).addClass("ui-tabs-selected ui-state-active");
                    l.element.queue("tabs", function () {
                        l._trigger("show", null, l._ui(l.anchors[c.selected], l.element.find(l._sanitizeSelector(l.anchors[c.selected].hash))[0]))
                    });
                    this.load(c.selected)
                }
                a(window).bind("unload", function () {
                    l.lis.add(l.anchors).unbind(".tabs");
                    l.lis = l.anchors = l.panels = null
                })
            } else c.selected = this.lis.index(this.lis.filter(".ui-tabs-selected"));
            this.element[c.collapsible ? "addClass" : "removeClass"]("ui-tabs-collapsible");
            c.cookie && this._cookie(c.selected, c.cookie);
            h = 0;
            for (var q; q = this.lis[h]; h++) a(q)[a.inArray(h, c.disabled) != -1 && !a(q).hasClass("ui-tabs-selected") ? "addClass" : "removeClass"]("ui-state-disabled");
            c.cache === false && this.anchors.removeData("cache.tabs");
            this.lis.add(this.anchors).unbind(".tabs");
            if (c.event !== "mouseover") {
                var s = function (E, M) {
                        M.is(":not(.ui-state-disabled)") && M.addClass("ui-state-" + E)
                    };
                this.lis.bind("mouseover.tabs", function () {
                    s("hover", a(this))
                });
                this.lis.bind("mouseout.tabs", function () {
                    a(this).removeClass("ui-state-hover")
                });
                this.anchors.bind("focus.tabs", function () {
                    s("focus", a(this).closest("li"))
                });
                this.anchors.bind("blur.tabs", function () {
                    a(this).closest("li").removeClass("ui-state-focus")
                })
            }
            var p, n;
            if (c.fx) if (a.isArray(c.fx)) {
                p = c.fx[0];
                n = c.fx[1]
            } else p = n = c.fx;
            var u = n ?
            function (E, M) {
                a(E).closest("li").addClass("ui-tabs-selected ui-state-active");
                M.hide().removeClass("ui-tabs-hide").animate(n, n.duration || "normal", function () {
                    j(M, n);
                    l._trigger("show", null, l._ui(E, M[0]))
                })
            } : function (E, M) {
                a(E).closest("li").addClass("ui-tabs-selected ui-state-active");
                M.removeClass("ui-tabs-hide");
                l._trigger("show", null, l._ui(E, M[0]))
            }, z = p ?
            function (E, M) {
                M.animate(p, p.duration || "normal", function () {
                    l.lis.removeClass("ui-tabs-selected ui-state-active");
                    M.addClass("ui-tabs-hide");
                    j(M, p);
                    l.element.dequeue("tabs")
                })
            } : function (E, M) {
                l.lis.removeClass("ui-tabs-selected ui-state-active");
                M.addClass("ui-tabs-hide");
                l.element.dequeue("tabs")
            };
            this.anchors.bind(c.event + ".tabs", function () {
                var E = this,
                    M = a(E).closest("li"),
                    ma = l.panels.filter(":not(.ui-tabs-hide)"),
                    oa = l.element.find(l._sanitizeSelector(E.hash));
                if (M.hasClass("ui-tabs-selected") && !c.collapsible || M.hasClass("ui-state-disabled") || M.hasClass("ui-state-processing") || l.panels.filter(":animated").length || l._trigger("select", null, l._ui(this, oa[0])) === false) {
                    this.blur();
                    return false
                }
                c.selected = l.anchors.index(this);
                l.abort();
                if (c.collapsible) if (M.hasClass("ui-tabs-selected")) {
                    c.selected = -1;
                    c.cookie && l._cookie(c.selected, c.cookie);
                    l.element.queue("tabs", function () {
                        z(E, ma)
                    }).dequeue("tabs");
                    this.blur();
                    return false
                } else if (!ma.length) {
                    c.cookie && l._cookie(c.selected, c.cookie);
                    l.element.queue("tabs", function () {
                        u(E, oa)
                    });
                    l.load(l.anchors.index(this));
                    this.blur();
                    return false
                }
                c.cookie && l._cookie(c.selected, c.cookie);
                if (oa.length) {
                    ma.length && l.element.queue("tabs", function () {
                        z(E, ma)
                    });
                    l.element.queue("tabs", function () {
                        u(E, oa)
                    });
                    l.load(l.anchors.index(this))
                } else throw "jQuery UI Tabs: Mismatching fragment identifier.";
                a.browser.msie && this.blur()
            });
            this.anchors.bind("click.tabs", function () {
                return false
            })
        },
        _getIndex: function (h) {
            if (typeof h == "string") h = this.anchors.index(this.anchors.filter("[href$=" + h + "]"));
            return h
        },
        destroy: function () {
            var h = this.options;
            this.abort();
            this.element.unbind(".tabs").removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible").removeData("tabs");
            this.list.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
            this.anchors.each(function () {
                var j = a.data(this, "href.tabs");
                if (j) this.href = j;
                var l = a(this).unbind(".tabs");
                a.each(["href", "load", "cache"], function (c, k) {
                    l.removeData(k + ".tabs")
                })
            });
            this.lis.unbind(".tabs").add(this.panels).each(function () {
                a.data(this, "destroy.tabs") ? a(this).remove() : a(this).removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active ui-state-hover ui-state-focus ui-state-disabled ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide")
            });
            h.cookie && this._cookie(null, h.cookie);
            return this
        },
        add: function (h, j, l) {
            if (l === f) l = this.anchors.length;
            var c = this,
                k = this.options;
            j = a(k.tabTemplate.replace(/#\{href\}/g, h).replace(/#\{label\}/g, j));
            h = !h.indexOf("#") ? h.replace("#", "") : this._tabId(a("a", j)[0]);
            j.addClass("ui-state-default ui-corner-top").data("destroy.tabs", true);
            var q = c.element.find("#" + h);
            q.length || (q = a(k.panelTemplate).attr("id", h).data("destroy.tabs", true));
            q.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide");
            if (l >= this.lis.length) {
                j.appendTo(this.list);
                q.appendTo(this.list[0].parentNode)
            } else {
                j.insertBefore(this.lis[l]);
                q.insertBefore(this.panels[l])
            }
            k.disabled = a.map(k.disabled, function (s) {
                return s >= l ? ++s : s
            });
            this._tabify();
            if (this.anchors.length == 1) {
                k.selected = 0;
                j.addClass("ui-tabs-selected ui-state-active");
                q.removeClass("ui-tabs-hide");
                this.element.queue("tabs", function () {
                    c._trigger("show", null, c._ui(c.anchors[0], c.panels[0]))
                });
                this.load(0)
            }
            this._trigger("add", null, this._ui(this.anchors[l], this.panels[l]));
            return this
        },
        remove: function (h) {
            h = this._getIndex(h);
            var j = this.options,
                l = this.lis.eq(h).remove(),
                c = this.panels.eq(h).remove();
            if (l.hasClass("ui-tabs-selected") && this.anchors.length > 1) this.select(h + (h + 1 < this.anchors.length ? 1 : -1));
            j.disabled = a.map(a.grep(j.disabled, function (k) {
                return k != h
            }), function (k) {
                return k >= h ? --k : k
            });
            this._tabify();
            this._trigger("remove", null, this._ui(l.find("a")[0], c[0]));
            return this
        },
        enable: function (h) {
            h = this._getIndex(h);
            var j = this.options;
            if (a.inArray(h, j.disabled) != -1) {
                this.lis.eq(h).removeClass("ui-state-disabled");
                j.disabled = a.grep(j.disabled, function (l) {
                    return l != h
                });
                this._trigger("enable", null, this._ui(this.anchors[h], this.panels[h]));
                return this
            }
        },
        disable: function (h) {
            h = this._getIndex(h);
            var j = this.options;
            if (h != j.selected) {
                this.lis.eq(h).addClass("ui-state-disabled");
                j.disabled.push(h);
                j.disabled.sort();
                this._trigger("disable", null, this._ui(this.anchors[h], this.panels[h]))
            }
            return this
        },
        select: function (h) {
            h = this._getIndex(h);
            if (h == -1) if (this.options.collapsible && this.options.selected != -1) h = this.options.selected;
            else return this;
            this.anchors.eq(h).trigger(this.options.event + ".tabs");
            return this
        },
        load: function (h) {
            h = this._getIndex(h);
            var j = this,
                l = this.options,
                c = this.anchors.eq(h)[0],
                k = a.data(c, "load.tabs");
            this.abort();
            if (!k || this.element.queue("tabs").length !== 0 && a.data(c, "cache.tabs")) this.element.dequeue("tabs");
            else {
                this.lis.eq(h).addClass("ui-state-processing");
                if (l.spinner) {
                    var q = a("span", c);
                    q.data("label.tabs", q.html()).html(l.spinner)
                }
                this.xhr = a.ajax(a.extend({}, l.ajaxOptions, {
                    url: k,
                    success: function (s, p) {
                        j.element.find(j._sanitizeSelector(c.hash)).html(s);
                        j._cleanup();
                        l.cache && a.data(c, "cache.tabs", true);
                        j._trigger("load", null, j._ui(j.anchors[h], j.panels[h]));
                        try {
                            l.ajaxOptions.success(s, p)
                        } catch (n) {}
                    },
                    error: function (s, p) {
                        j._cleanup();
                        j._trigger("load", null, j._ui(j.anchors[h], j.panels[h]));
                        try {
                            l.ajaxOptions.error(s, p, h, c)
                        } catch (n) {}
                    }
                }));
                j.element.dequeue("tabs");
                return this
            }
        },
        abort: function () {
            this.element.queue([]);
            this.panels.stop(false, true);
            this.element.queue("tabs", this.element.queue("tabs").splice(-2, 2));
            if (this.xhr) {
                this.xhr.abort();
                delete this.xhr
            }
            this._cleanup();
            return this
        },
        url: function (h, j) {
            this.anchors.eq(h).removeData("cache.tabs").data("load.tabs", j);
            return this
        },
        length: function () {
            return this.anchors.length
        }
    });
    a.extend(a.ui.tabs, {
        version: "1.8.18"
    });
    a.extend(a.ui.tabs.prototype, {
        rotation: null,
        rotate: function (h, j) {
            var l = this,
                c = this.options,
                k = l._rotate || (l._rotate = function (s) {
                    clearTimeout(l.rotation);
                    l.rotation = setTimeout(function () {
                        var p = c.selected;
                        l.select(++p < l.anchors.length ? p : 0)
                    }, h);
                    s && s.stopPropagation()
                }),
                q = l._unrotate || (l._unrotate = !j ?
                function (s) {
                    s.clientX && l.rotate(null)
                } : function () {
                    t = c.selected;
                    k()
                });
            if (h) {
                this.element.bind("tabsshow", k);
                this.anchors.bind(c.event + ".tabs", q);
                k()
            } else {
                clearTimeout(l.rotation);
                this.element.unbind("tabsshow", k);
                this.anchors.unbind(c.event + ".tabs", q);
                delete this._rotate;
                delete this._unrotate
            }
            return this
        }
    })
})(jQuery);
(function (a, f) {
    function i() {
        this.debug = false;
        this._curInst = null;
        this._keyEvent = false;
        this._disabledInputs = [];
        this._inDialog = this._datepickerShowing = false;
        this._mainDivId = "ui-datepicker-div";
        this._inlineClass = "ui-datepicker-inline";
        this._appendClass = "ui-datepicker-append";
        this._triggerClass = "ui-datepicker-trigger";
        this._dialogClass = "ui-datepicker-dialog";
        this._disableClass = "ui-datepicker-disabled";
        this._unselectableClass = "ui-datepicker-unselectable";
        this._currentClass = "ui-datepicker-current-day";
        this._dayOverClass = "ui-datepicker-days-cell-over";
        this.regional = [];
        this.regional[""] = {
            closeText: "Done",
            prevText: "Prev",
            nextText: "Next",
            currentText: "Today",
            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            weekHeader: "Wk",
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ""
        };
        this._defaults = {
            showOn: "focus",
            showAnim: "fadeIn",
            showOptions: {},
            defaultDate: null,
            appendText: "",
            buttonText: "...",
            buttonImage: "",
            buttonImageOnly: false,
            hideIfNoPrevNext: false,
            navigationAsDateFormat: false,
            gotoCurrent: false,
            changeMonth: false,
            changeYear: false,
            yearRange: "c-10:c+10",
            showOtherMonths: false,
            selectOtherMonths: false,
            showWeek: false,
            calculateWeek: this.iso8601Week,
            shortYearCutoff: "+10",
            minDate: null,
            maxDate: null,
            duration: "fast",
            beforeShowDay: null,
            beforeShow: null,
            onSelect: null,
            onChangeMonthYear: null,
            onClose: null,
            numberOfMonths: 1,
            showCurrentAtPos: 0,
            stepMonths: 1,
            stepBigMonths: 12,
            altField: "",
            altFormat: "",
            constrainInput: true,
            showButtonPanel: false,
            autoSize: false,
            disabled: false
        };
        a.extend(this._defaults, this.regional[""]);
        this.dpDiv = g(a('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))
    }
    function g(c) {
        return c.bind("mouseout", function (k) {
            k = a(k.target).closest("button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a");
            k.length && k.removeClass("ui-state-hover ui-datepicker-prev-hover ui-datepicker-next-hover")
        }).bind("mouseover", function (k) {
            k = a(k.target).closest("button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a");
            if (!(a.datepicker._isDisabledDatepicker(l.inline ? c.parent()[0] : l.input[0]) || !k.length)) {
                k.parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover");
                k.addClass("ui-state-hover");
                k.hasClass("ui-datepicker-prev") && k.addClass("ui-datepicker-prev-hover");
                k.hasClass("ui-datepicker-next") && k.addClass("ui-datepicker-next-hover")
            }
        })
    }
    function h(c, k) {
        a.extend(c, k);
        for (var q in k) if (k[q] == null || k[q] == f) c[q] = k[q];
        return c
    }
    a.extend(a.ui, {
        datepicker: {
            version: "1.8.18"
        }
    });
    var j = (new Date).getTime(),
        l;
    a.extend(i.prototype, {
        markerClassName: "hasDatepicker",
        maxRows: 4,
        log: function () {
            this.debug && console.log.apply("", arguments)
        },
        _widgetDatepicker: function () {
            return this.dpDiv
        },
        setDefaults: function (c) {
            h(this._defaults, c || {});
            return this
        },
        _attachDatepicker: function (c, k) {
            var q = null,
                s;
            for (s in this._defaults) {
                var p = c.getAttribute("date:" + s);
                if (p) {
                    q = q || {};
                    try {
                        q[s] = eval(p)
                    } catch (n) {
                        q[s] = p
                    }
                }
            }
            s = c.nodeName.toLowerCase();
            p = s == "div" || s == "span";
            if (!c.id) {
                this.uuid += 1;
                c.id = "dp" + this.uuid
            }
            var u = this._newInst(a(c), p);
            u.settings = a.extend({}, k || {}, q || {});
            if (s == "input") this._connectDatepicker(c, u);
            else p && this._inlineDatepicker(c, u)
        },
        _newInst: function (c, k) {
            return {
                id: c[0].id.replace(/([^A-Za-z0-9_-])/g, "\\\\$1"),
                input: c,
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                drawMonth: 0,
                drawYear: 0,
                inline: k,
                dpDiv: !k ? this.dpDiv : g(a('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))
            }
        },
        _connectDatepicker: function (c, k) {
            var q = a(c);
            k.append = a([]);
            k.trigger = a([]);
            if (!q.hasClass(this.markerClassName)) {
                this._attachments(q, k);
                q.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp).bind("setData.datepicker", function (s, p, n) {
                    k.settings[p] = n
                }).bind("getData.datepicker", function (s, p) {
                    return this._get(k, p)
                });
                this._autoSize(k);
                a.data(c, "datepicker", k);
                k.settings.disabled && this._disableDatepicker(c)
            }
        },
        _attachments: function (c, k) {
            var q = this._get(k, "appendText"),
                s = this._get(k, "isRTL");
            k.append && k.append.remove();
            if (q) {
                k.append = a('<span class="' + this._appendClass + '">' + q + "</span>");
                c[s ? "before" : "after"](k.append)
            }
            c.unbind("focus", this._showDatepicker);
            k.trigger && k.trigger.remove();
            q = this._get(k, "showOn");
            if (q == "focus" || q == "both") c.focus(this._showDatepicker);
            if (q == "button" || q == "both") {
                q = this._get(k, "buttonText");
                var p = this._get(k, "buttonImage");
                k.trigger = a(this._get(k, "buttonImageOnly") ? a("<img/>").addClass(this._triggerClass).attr({
                    src: p,
                    alt: q,
                    title: q
                }) : a('<button type="button"></button>').addClass(this._triggerClass).html(p == "" ? q : a("<img/>").attr({
                    src: p,
                    alt: q,
                    title: q
                })));
                c[s ? "before" : "after"](k.trigger);
                k.trigger.click(function () {
                    if (a.datepicker._datepickerShowing && a.datepicker._lastInput == c[0]) a.datepicker._hideDatepicker();
                    else {
                        a.datepicker._datepickerShowing && a.datepicker._lastInput != c[0] && a.datepicker._hideDatepicker();
                        a.datepicker._showDatepicker(c[0])
                    }
                    return false
                })
            }
        },
        _autoSize: function (c) {
            if (this._get(c, "autoSize") && !c.inline) {
                var k = new Date(2009, 11, 20),
                    q = this._get(c, "dateFormat");
                if (q.match(/[DM]/)) {
                    var s = function (p) {
                            for (var n = 0, u = 0, z = 0; z < p.length; z++) if (p[z].length > n) {
                                n = p[z].length;
                                u = z
                            }
                            return u
                        };
                    k.setMonth(s(this._get(c, q.match(/MM/) ? "monthNames" : "monthNamesShort")));
                    k.setDate(s(this._get(c, q.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - k.getDay())
                }
                c.input.attr("size", this._formatDate(c, k).length)
            }
        },
        _inlineDatepicker: function (c, k) {
            var q = a(c);
            if (!q.hasClass(this.markerClassName)) {
                q.addClass(this.markerClassName).append(k.dpDiv).bind("setData.datepicker", function (s, p, n) {
                    k.settings[p] = n
                }).bind("getData.datepicker", function (s, p) {
                    return this._get(k, p)
                });
                a.data(c, "datepicker", k);
                this._setDate(k, this._getDefaultDate(k), true);
                this._updateDatepicker(k);
                this._updateAlternate(k);
                k.settings.disabled && this._disableDatepicker(c);
                k.dpDiv.css("display", "block")
            }
        },
        _dialogDatepicker: function (c, k, q, s, p) {
            c = this._dialogInst;
            if (!c) {
                this.uuid += 1;
                this._dialogInput = a('<input type="text" id="' + ("dp" + this.uuid) + '" style="position: absolute; top: -100px; width: 0px; z-index: -10;"/>');
                this._dialogInput.keydown(this._doKeyDown);
                a("body").append(this._dialogInput);
                c = this._dialogInst = this._newInst(this._dialogInput, false);
                c.settings = {};
                a.data(this._dialogInput[0], "datepicker", c)
            }
            h(c.settings, s || {});
            k = k && k.constructor == Date ? this._formatDate(c, k) : k;
            this._dialogInput.val(k);
            this._pos = p ? p.length ? p : [p.pageX, p.pageY] : null;
            if (!this._pos) this._pos = [document.documentElement.clientWidth / 2 - 100 + (document.documentElement.scrollLeft || document.body.scrollLeft), document.documentElement.clientHeight / 2 - 150 + (document.documentElement.scrollTop || document.body.scrollTop)];
            this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px");
            c.settings.onSelect = q;
            this._inDialog = true;
            this.dpDiv.addClass(this._dialogClass);
            this._showDatepicker(this._dialogInput[0]);
            a.blockUI && a.blockUI(this.dpDiv);
            a.data(this._dialogInput[0], "datepicker", c);
            return this
        },
        _destroyDatepicker: function (c) {
            var k = a(c),
                q = a.data(c, "datepicker");
            if (k.hasClass(this.markerClassName)) {
                var s = c.nodeName.toLowerCase();
                a.removeData(c, "datepicker");
                if (s == "input") {
                    q.append.remove();
                    q.trigger.remove();
                    k.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)
                } else if (s == "div" || s == "span") k.removeClass(this.markerClassName).empty()
            }
        },
        _enableDatepicker: function (c) {
            var k = a(c),
                q = a.data(c, "datepicker");
            if (k.hasClass(this.markerClassName)) {
                var s = c.nodeName.toLowerCase();
                if (s == "input") {
                    c.disabled = false;
                    q.trigger.filter("button").each(function () {
                        this.disabled = false
                    }).end().filter("img").css({
                        opacity: "1.0",
                        cursor: ""
                    })
                } else if (s == "div" || s == "span") {
                    k = k.children("." + this._inlineClass);
                    k.children().removeClass("ui-state-disabled");
                    k.find("select.ui-datepicker-month, select.ui-datepicker-year").removeAttr("disabled")
                }
                this._disabledInputs = a.map(this._disabledInputs, function (p) {
                    return p == c ? null : p
                })
            }
        },
        _disableDatepicker: function (c) {
            var k = a(c),
                q = a.data(c, "datepicker");
            if (k.hasClass(this.markerClassName)) {
                var s = c.nodeName.toLowerCase();
                if (s == "input") {
                    c.disabled = true;
                    q.trigger.filter("button").each(function () {
                        this.disabled = true
                    }).end().filter("img").css({
                        opacity: "0.5",
                        cursor: "default"
                    })
                } else if (s == "div" || s == "span") {
                    k = k.children("." + this._inlineClass);
                    k.children().addClass("ui-state-disabled");
                    k.find("select.ui-datepicker-month, select.ui-datepicker-year").attr("disabled", "disabled")
                }
                this._disabledInputs = a.map(this._disabledInputs, function (p) {
                    return p == c ? null : p
                });
                this._disabledInputs[this._disabledInputs.length] = c
            }
        },
        _isDisabledDatepicker: function (c) {
            if (!c) return false;
            for (var k = 0; k < this._disabledInputs.length; k++) if (this._disabledInputs[k] == c) return true;
            return false
        },
        _getInst: function (c) {
            try {
                return a.data(c, "datepicker")
            } catch (k) {
                throw "Missing instance data for this datepicker";
            }
        },
        _optionDatepicker: function (c, k, q) {
            var s = this._getInst(c);
            if (arguments.length == 2 && typeof k == "string") return k == "defaults" ? a.extend({}, a.datepicker._defaults) : s ? k == "all" ? a.extend({}, s.settings) : this._get(s, k) : null;
            var p = k || {};
            if (typeof k == "string") {
                p = {};
                p[k] = q
            }
            if (s) {
                this._curInst == s && this._hideDatepicker();
                var n = this._getDateDatepicker(c, true),
                    u = this._getMinMaxDate(s, "min"),
                    z = this._getMinMaxDate(s, "max");
                h(s.settings, p);
                if (u !== null && p.dateFormat !== f && p.minDate === f) s.settings.minDate = this._formatDate(s, u);
                if (z !== null && p.dateFormat !== f && p.maxDate === f) s.settings.maxDate = this._formatDate(s, z);
                this._attachments(a(c), s);
                this._autoSize(s);
                this._setDate(s, n);
                this._updateAlternate(s);
                this._updateDatepicker(s)
            }
        },
        _changeDatepicker: function (c, k, q) {
            this._optionDatepicker(c, k, q)
        },
        _refreshDatepicker: function (c) {
            (c = this._getInst(c)) && this._updateDatepicker(c)
        },
        _setDateDatepicker: function (c, k) {
            var q = this._getInst(c);
            if (q) {
                this._setDate(q, k);
                this._updateDatepicker(q);
                this._updateAlternate(q)
            }
        },
        _getDateDatepicker: function (c, k) {
            var q = this._getInst(c);
            q && !q.inline && this._setDateFromField(q, k);
            return q ? this._getDate(q) : null
        },
        _doKeyDown: function (c) {
            var k = a.datepicker._getInst(c.target),
                q = true,
                s = k.dpDiv.is(".ui-datepicker-rtl");
            k._keyEvent = true;
            if (a.datepicker._datepickerShowing) switch (c.keyCode) {
            case 9:
                a.datepicker._hideDatepicker();
                q = false;
                break;
            case 13:
                q = a("td." + a.datepicker._dayOverClass + ":not(." + a.datepicker._currentClass + ")", k.dpDiv);
                q[0] && a.datepicker._selectDay(c.target, k.selectedMonth, k.selectedYear, q[0]);
                if (c = a.datepicker._get(k, "onSelect")) {
                    q = a.datepicker._formatDate(k);
                    c.apply(k.input ? k.input[0] : null, [q, k])
                } else a.datepicker._hideDatepicker();
                return false;
            case 27:
                a.datepicker._hideDatepicker();
                break;
            case 33:
                a.datepicker._adjustDate(c.target, c.ctrlKey ? -a.datepicker._get(k, "stepBigMonths") : -a.datepicker._get(k, "stepMonths"), "M");
                break;
            case 34:
                a.datepicker._adjustDate(c.target, c.ctrlKey ? +a.datepicker._get(k, "stepBigMonths") : +a.datepicker._get(k, "stepMonths"), "M");
                break;
            case 35:
                if (c.ctrlKey || c.metaKey) a.datepicker._clearDate(c.target);
                q = c.ctrlKey || c.metaKey;
                break;
            case 36:
                if (c.ctrlKey || c.metaKey) a.datepicker._gotoToday(c.target);
                q = c.ctrlKey || c.metaKey;
                break;
            case 37:
                if (c.ctrlKey || c.metaKey) a.datepicker._adjustDate(c.target, s ? 1 : -1, "D");
                q = c.ctrlKey || c.metaKey;
                if (c.originalEvent.altKey) a.datepicker._adjustDate(c.target, c.ctrlKey ? -a.datepicker._get(k, "stepBigMonths") : -a.datepicker._get(k, "stepMonths"), "M");
                break;
            case 38:
                if (c.ctrlKey || c.metaKey) a.datepicker._adjustDate(c.target, -7, "D");
                q = c.ctrlKey || c.metaKey;
                break;
            case 39:
                if (c.ctrlKey || c.metaKey) a.datepicker._adjustDate(c.target, s ? -1 : 1, "D");
                q = c.ctrlKey || c.metaKey;
                if (c.originalEvent.altKey) a.datepicker._adjustDate(c.target, c.ctrlKey ? +a.datepicker._get(k, "stepBigMonths") : +a.datepicker._get(k, "stepMonths"), "M");
                break;
            case 40:
                if (c.ctrlKey || c.metaKey) a.datepicker._adjustDate(c.target, 7, "D");
                q = c.ctrlKey || c.metaKey;
                break;
            default:
                q = false
            } else if (c.keyCode == 36 && c.ctrlKey) a.datepicker._showDatepicker(this);
            else q = false;
            if (q) {
                c.preventDefault();
                c.stopPropagation()
            }
        },
        _doKeyPress: function (c) {
            var k = a.datepicker._getInst(c.target);
            if (a.datepicker._get(k, "constrainInput")) {
                k = a.datepicker._possibleChars(a.datepicker._get(k, "dateFormat"));
                var q = String.fromCharCode(c.charCode == f ? c.keyCode : c.charCode);
                return c.ctrlKey || c.metaKey || q < " " || !k || k.indexOf(q) > -1
            }
        },
        _doKeyUp: function (c) {
            c = a.datepicker._getInst(c.target);
            if (c.input.val() != c.lastVal) try {
                if (a.datepicker.parseDate(a.datepicker._get(c, "dateFormat"), c.input ? c.input.val() : null, a.datepicker._getFormatConfig(c))) {
                    a.datepicker._setDateFromField(c);
                    a.datepicker._updateAlternate(c);
                    a.datepicker._updateDatepicker(c)
                }
            } catch (k) {
                a.datepicker.log(k)
            }
            return true
        },
        _showDatepicker: function (c) {
            c = c.target || c;
            if (c.nodeName.toLowerCase() != "input") c = a("input", c.parentNode)[0];
            if (!(a.datepicker._isDisabledDatepicker(c) || a.datepicker._lastInput == c)) {
                var k = a.datepicker._getInst(c);
                if (a.datepicker._curInst && a.datepicker._curInst != k) {
                    a.datepicker._curInst.dpDiv.stop(true, true);
                    k && a.datepicker._datepickerShowing && a.datepicker._hideDatepicker(a.datepicker._curInst.input[0])
                }
                var q = a.datepicker._get(k, "beforeShow");
                q = q ? q.apply(c, [c, k]) : {};
                if (q !== false) {
                    h(k.settings, q);
                    k.lastVal = null;
                    a.datepicker._lastInput = c;
                    a.datepicker._setDateFromField(k);
                    if (a.datepicker._inDialog) c.value = "";
                    if (!a.datepicker._pos) {
                        a.datepicker._pos = a.datepicker._findPos(c);
                        a.datepicker._pos[1] += c.offsetHeight
                    }
                    var s = false;
                    a(c).parents().each(function () {
                        s |= a(this).css("position") == "fixed";
                        return !s
                    });
                    if (s && a.browser.opera) {
                        a.datepicker._pos[0] -= document.documentElement.scrollLeft;
                        a.datepicker._pos[1] -= document.documentElement.scrollTop
                    }
                    q = {
                        left: a.datepicker._pos[0],
                        top: a.datepicker._pos[1]
                    };
                    a.datepicker._pos = null;
                    k.dpDiv.empty();
                    k.dpDiv.css({
                        position: "absolute",
                        display: "block",
                        top: "-1000px"
                    });
                    a.datepicker._updateDatepicker(k);
                    q = a.datepicker._checkOffset(k, q, s);
                    k.dpDiv.css({
                        position: a.datepicker._inDialog && a.blockUI ? "static" : s ? "fixed" : "absolute",
                        display: "none",
                        left: q.left + "px",
                        top: q.top + "px"
                    });
                    if (!k.inline) {
                        q = a.datepicker._get(k, "showAnim");
                        var p = a.datepicker._get(k, "duration"),
                            n = function () {
                                var u = k.dpDiv.find("iframe.ui-datepicker-cover");
                                if (u.length) {
                                    var z = a.datepicker._getBorders(k.dpDiv);
                                    u.css({
                                        left: -z[0],
                                        top: -z[1],
                                        width: k.dpDiv.outerWidth(),
                                        height: k.dpDiv.outerHeight()
                                    })
                                }
                            };
                        k.dpDiv.zIndex(a(c).zIndex() + 1);
                        a.datepicker._datepickerShowing = true;
                        if (a.effects && a.effects[q]) k.dpDiv.show(q, a.datepicker._get(k, "showOptions"), p, n);
                        else k.dpDiv[q || "show"](q ? p : null, n);
                        if (!q || !p) n();
                        k.input.is(":visible") && !k.input.is(":disabled") && k.input.focus();
                        a.datepicker._curInst = k
                    }
                }
            }
        },
        _updateDatepicker: function (c) {
            this.maxRows = 4;
            var k = a.datepicker._getBorders(c.dpDiv);
            l = c;
            c.dpDiv.empty().append(this._generateHTML(c));
            var q = c.dpDiv.find("iframe.ui-datepicker-cover");
            q.length && q.css({
                left: -k[0],
                top: -k[1],
                width: c.dpDiv.outerWidth(),
                height: c.dpDiv.outerHeight()
            });
            c.dpDiv.find("." + this._dayOverClass + " a").mouseover();
            k = this._getNumberOfMonths(c);
            q = k[1];
            c.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");
            q > 1 && c.dpDiv.addClass("ui-datepicker-multi-" + q).css("width", 17 * q + "em");
            c.dpDiv[(k[0] != 1 || k[1] != 1 ? "add" : "remove") + "Class"]("ui-datepicker-multi");
            c.dpDiv[(this._get(c, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl");
            c == a.datepicker._curInst && a.datepicker._datepickerShowing && c.input && c.input.is(":visible") && !c.input.is(":disabled") && c.input[0] != document.activeElement && c.input.focus();
            if (c.yearshtml) {
                var s = c.yearshtml;
                setTimeout(function () {
                    s === c.yearshtml && c.yearshtml && c.dpDiv.find("select.ui-datepicker-year:first").replaceWith(c.yearshtml);
                    s = c.yearshtml = null
                }, 0)
            }
        },
        _getBorders: function (c) {
            var k = function (q) {
                    return {
                        thin: 1,
                        medium: 2,
                        thick: 3
                    }[q] || q
                };
            return [parseFloat(k(c.css("border-left-width"))), parseFloat(k(c.css("border-top-width")))]
        },
        _checkOffset: function (c, k, q) {
            var s = c.dpDiv.outerWidth(),
                p = c.dpDiv.outerHeight(),
                n = c.input ? c.input.outerWidth() : 0,
                u = c.input ? c.input.outerHeight() : 0,
                z = document.documentElement.clientWidth + a(document).scrollLeft(),
                E = document.documentElement.clientHeight + a(document).scrollTop();
            k.left -= this._get(c, "isRTL") ? s - n : 0;
            k.left -= q && k.left == c.input.offset().left ? a(document).scrollLeft() : 0;
            k.top -= q && k.top == c.input.offset().top + u ? a(document).scrollTop() : 0;
            k.left -= Math.min(k.left, k.left + s > z && z > s ? Math.abs(k.left + s - z) : 0);
            k.top -= Math.min(k.top, k.top + p > E && E > p ? Math.abs(p + u) : 0);
            return k
        },
        _findPos: function (c) {
            for (var k = this._get(this._getInst(c), "isRTL"); c && (c.type == "hidden" || c.nodeType != 1 || a.expr.filters.hidden(c));) c = c[k ? "previousSibling" : "nextSibling"];
            c = a(c).offset();
            return [c.left, c.top]
        },
        _hideDatepicker: function (c) {
            var k = this._curInst;
            if (!(!k || c && k != a.data(c, "datepicker"))) if (this._datepickerShowing) {
                c = this._get(k, "showAnim");
                var q = this._get(k, "duration"),
                    s = this,
                    p = function () {
                        a.datepicker._tidyDialog(k);
                        s._curInst = null
                    };
                if (a.effects && a.effects[c]) k.dpDiv.hide(c, a.datepicker._get(k, "showOptions"), q, p);
                else k.dpDiv[c == "slideDown" ? "slideUp" : c == "fadeIn" ? "fadeOut" : "hide"](c ? q : null, p);
                c || p();
                this._datepickerShowing = false;
                if (c = this._get(k, "onClose")) c.apply(k.input ? k.input[0] : null, [k.input ? k.input.val() : "", k]);
                this._lastInput = null;
                if (this._inDialog) {
                    this._dialogInput.css({
                        position: "absolute",
                        left: "0",
                        top: "-100px"
                    });
                    if (a.blockUI) {
                        a.unblockUI();
                        a("body").append(this.dpDiv)
                    }
                }
                this._inDialog = false
            }
        },
        _tidyDialog: function (c) {
            c.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
        },
        _checkExternalClick: function (c) {
            if (a.datepicker._curInst) {
                c = a(c.target);
                var k = a.datepicker._getInst(c[0]);
                if (c[0].id != a.datepicker._mainDivId && c.parents("#" + a.datepicker._mainDivId).length == 0 && !c.hasClass(a.datepicker.markerClassName) && !c.closest("." + a.datepicker._triggerClass).length && a.datepicker._datepickerShowing && !(a.datepicker._inDialog && a.blockUI) || c.hasClass(a.datepicker.markerClassName) && a.datepicker._curInst != k) a.datepicker._hideDatepicker()
            }
        },
        _adjustDate: function (c, k, q) {
            c = a(c);
            var s = this._getInst(c[0]);
            if (!this._isDisabledDatepicker(c[0])) {
                this._adjustInstDate(s, k + (q == "M" ? this._get(s, "showCurrentAtPos") : 0), q);
                this._updateDatepicker(s)
            }
        },
        _gotoToday: function (c) {
            c = a(c);
            var k = this._getInst(c[0]);
            if (this._get(k, "gotoCurrent") && k.currentDay) {
                k.selectedDay = k.currentDay;
                k.drawMonth = k.selectedMonth = k.currentMonth;
                k.drawYear = k.selectedYear = k.currentYear
            } else {
                var q = new Date;
                k.selectedDay = q.getDate();
                k.drawMonth = k.selectedMonth = q.getMonth();
                k.drawYear = k.selectedYear = q.getFullYear()
            }
            this._notifyChange(k);
            this._adjustDate(c)
        },
        _selectMonthYear: function (c, k, q) {
            c = a(c);
            var s = this._getInst(c[0]);
            s["selected" + (q == "M" ? "Month" : "Year")] = s["draw" + (q == "M" ? "Month" : "Year")] = parseInt(k.options[k.selectedIndex].value, 10);
            this._notifyChange(s);
            this._adjustDate(c)
        },
        _selectDay: function (c, k, q, s) {
            var p = a(c);
            if (!(a(s).hasClass(this._unselectableClass) || this._isDisabledDatepicker(p[0]))) {
                p = this._getInst(p[0]);
                p.selectedDay = p.currentDay = a("a", s).html();
                p.selectedMonth = p.currentMonth = k;
                p.selectedYear = p.currentYear = q;
                this._selectDate(c, this._formatDate(p, p.currentDay, p.currentMonth, p.currentYear))
            }
        },
        _clearDate: function (c) {
            c = a(c);
            this._getInst(c[0]);
            this._selectDate(c, "")
        },
        _selectDate: function (c, k) {
            var q = this._getInst(a(c)[0]);
            k = k != null ? k : this._formatDate(q);
            q.input && q.input.val(k);
            this._updateAlternate(q);
            var s = this._get(q, "onSelect");
            if (s) s.apply(q.input ? q.input[0] : null, [k, q]);
            else q.input && q.input.trigger("change");
            if (q.inline) this._updateDatepicker(q);
            else {
                this._hideDatepicker();
                this._lastInput = q.input[0];
                typeof q.input[0] != "object" && q.input.focus();
                this._lastInput = null
            }
        },
        _updateAlternate: function (c) {
            var k = this._get(c, "altField");
            if (k) {
                var q = this._get(c, "altFormat") || this._get(c, "dateFormat"),
                    s = this._getDate(c),
                    p = this.formatDate(q, s, this._getFormatConfig(c));
                a(k).each(function () {
                    a(this).val(p)
                })
            }
        },
        noWeekends: function (c) {
            c = c.getDay();
            return [c > 0 && c < 6, ""]
        },
        iso8601Week: function (c) {
            c = new Date(c.getTime());
            c.setDate(c.getDate() + 4 - (c.getDay() || 7));
            var k = c.getTime();
            c.setMonth(0);
            c.setDate(1);
            return Math.floor(Math.round((k - c) / 864E5) / 7) + 1
        },
        parseDate: function (c, k, q) {
            if (c == null || k == null) throw "Invalid arguments";
            k = typeof k == "object" ? k.toString() : k + "";
            if (k == "") return null;
            var s = (q ? q.shortYearCutoff : null) || this._defaults.shortYearCutoff;
            s = typeof s != "string" ? s : (new Date).getFullYear() % 100 + parseInt(s, 10);
            for (var p = (q ? q.dayNamesShort : null) || this._defaults.dayNamesShort, n = (q ? q.dayNames : null) || this._defaults.dayNames, u = (q ? q.monthNamesShort : null) || this._defaults.monthNamesShort, z = (q ? q.monthNames : null) || this._defaults.monthNames, E = q = -1, M = -1, ma = -1, oa = false, Da = function (x) {
                    (x = ja + 1 < c.length && c.charAt(ja + 1) == x) && ja++;
                    return x
                }, ya = function (x) {
                    var K = Da(x);
                    x = k.substring(Z).match(RegExp("^\\d{1," + (x == "@" ? 14 : x == "!" ? 20 : x == "y" && K ? 4 : x == "o" ? 3 : 2) + "}"));
                    if (!x) throw "Missing number at position " + Z;
                    Z += x[0].length;
                    return parseInt(x[0], 10)
                }, pa = function (x, K, y) {
                    x = a.map(Da(x) ? y : K, function (Q, N) {
                        return [[N, Q]]
                    }).sort(function (Q, N) {
                        return -(Q[1].length - N[1].length)
                    });
                    var d = -1;
                    a.each(x, function (Q, N) {
                        var H = N[1];
                        if (k.substr(Z, H.length).toLowerCase() == H.toLowerCase()) {
                            d = N[0];
                            Z += H.length;
                            return false
                        }
                    });
                    if (d != -1) return d + 1;
                    else throw "Unknown name at position " + Z;
                }, Ma = function () {
                    if (k.charAt(Z) != c.charAt(ja)) throw "Unexpected literal at position " + Z;
                    Z++
                }, Z = 0, ja = 0; ja < c.length; ja++) if (oa) if (c.charAt(ja) == "'" && !Da("'")) oa = false;
            else Ma();
            else switch (c.charAt(ja)) {
            case "d":
                M = ya("d");
                break;
            case "D":
                pa("D", p, n);
                break;
            case "o":
                ma = ya("o");
                break;
            case "m":
                E = ya("m");
                break;
            case "M":
                E = pa("M", u, z);
                break;
            case "y":
                q = ya("y");
                break;
            case "@":
                var C = new Date(ya("@"));
                q = C.getFullYear();
                E = C.getMonth() + 1;
                M = C.getDate();
                break;
            case "!":
                C = new Date((ya("!") - this._ticksTo1970) / 1E4);
                q = C.getFullYear();
                E = C.getMonth() + 1;
                M = C.getDate();
                break;
            case "'":
                if (Da("'")) Ma();
                else oa = true;
                break;
            default:
                Ma()
            }
            if (Z < k.length) throw "Extra/unparsed characters found in date: " + k.substring(Z);
            if (q == -1) q = (new Date).getFullYear();
            else if (q < 100) q += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (q <= s ? 0 : -100);
            if (ma > -1) {
                E = 1;
                M = ma;
                do {
                    s = this._getDaysInMonth(q, E - 1);
                    if (M <= s) break;
                    E++;
                    M -= s
                } while (1)
            }
            C = this._daylightSavingAdjust(new Date(q, E - 1, M));
            if (C.getFullYear() != q || C.getMonth() + 1 != E || C.getDate() != M) throw "Invalid date";
            return C
        },
        ATOM: "yy-mm-dd",
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        _ticksTo1970: (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)) * 864E9,
        formatDate: function (c, k, q) {
            if (!k) return "";
            var s = (q ? q.dayNamesShort : null) || this._defaults.dayNamesShort,
                p = (q ? q.dayNames : null) || this._defaults.dayNames,
                n = (q ? q.monthNamesShort : null) || this._defaults.monthNamesShort;
            q = (q ? q.monthNames : null) || this._defaults.monthNames;
            var u = function (Da) {
                    (Da = oa + 1 < c.length && c.charAt(oa + 1) == Da) && oa++;
                    return Da
                },
                z = function (Da, ya, pa) {
                    ya = "" + ya;
                    if (u(Da)) for (; ya.length < pa;) ya = "0" + ya;
                    return ya
                },
                E = function (Da, ya, pa, Ma) {
                    return u(Da) ? Ma[ya] : pa[ya]
                },
                M = "",
                ma = false;
            if (k) for (var oa = 0; oa < c.length; oa++) if (ma) if (c.charAt(oa) == "'" && !u("'")) ma = false;
            else M += c.charAt(oa);
            else switch (c.charAt(oa)) {
            case "d":
                M += z("d", k.getDate(), 2);
                break;
            case "D":
                M += E("D", k.getDay(), s, p);
                break;
            case "o":
                M += z("o", Math.round(((new Date(k.getFullYear(), k.getMonth(), k.getDate())).getTime() - (new Date(k.getFullYear(), 0, 0)).getTime()) / 864E5), 3);
                break;
            case "m":
                M += z("m", k.getMonth() + 1, 2);
                break;
            case "M":
                M += E("M", k.getMonth(), n, q);
                break;
            case "y":
                M += u("y") ? k.getFullYear() : (k.getYear() % 100 < 10 ? "0" : "") + k.getYear() % 100;
                break;
            case "@":
                M += k.getTime();
                break;
            case "!":
                M += k.getTime() * 1E4 + this._ticksTo1970;
                break;
            case "'":
                if (u("'")) M += "'";
                else ma = true;
                break;
            default:
                M += c.charAt(oa)
            }
            return M
        },
        _possibleChars: function (c) {
            for (var k = "", q = false, s = function (n) {
                    (n = p + 1 < c.length && c.charAt(p + 1) == n) && p++;
                    return n
                }, p = 0; p < c.length; p++) if (q) if (c.charAt(p) == "'" && !s("'")) q = false;
            else k += c.charAt(p);
            else switch (c.charAt(p)) {
            case "d":
            case "m":
            case "y":
            case "@":
                k += "0123456789";
                break;
            case "D":
            case "M":
                return null;
            case "'":
                if (s("'")) k += "'";
                else q = true;
                break;
            default:
                k += c.charAt(p)
            }
            return k
        },
        _get: function (c, k) {
            return c.settings[k] !== f ? c.settings[k] : this._defaults[k]
        },
        _setDateFromField: function (c, k) {
            if (c.input.val() != c.lastVal) {
                var q = this._get(c, "dateFormat"),
                    s = c.lastVal = c.input ? c.input.val() : null,
                    p, n;
                p = n = this._getDefaultDate(c);
                var u = this._getFormatConfig(c);
                try {
                    p = this.parseDate(q, s, u) || n
                } catch (z) {
                    this.log(z);
                    s = k ? "" : s
                }
                c.selectedDay = p.getDate();
                c.drawMonth = c.selectedMonth = p.getMonth();
                c.drawYear = c.selectedYear = p.getFullYear();
                c.currentDay = s ? p.getDate() : 0;
                c.currentMonth = s ? p.getMonth() : 0;
                c.currentYear = s ? p.getFullYear() : 0;
                this._adjustInstDate(c)
            }
        },
        _getDefaultDate: function (c) {
            return this._restrictMinMax(c, this._determineDate(c, this._get(c, "defaultDate"), new Date))
        },
        _determineDate: function (c, k, q) {
            var s = function (p) {
                    var n = new Date;
                    n.setDate(n.getDate() + p);
                    return n
                };
            if (k = (k = k == null || k === "" ? q : typeof k == "string" ?
            function (p) {
                try {
                    return a.datepicker.parseDate(a.datepicker._get(c, "dateFormat"), p, a.datepicker._getFormatConfig(c))
                } catch (n) {}
                var u = (p.toLowerCase().match(/^c/) ? a.datepicker._getDate(c) : null) || new Date,
                    z = u.getFullYear(),
                    E = u.getMonth();
                u = u.getDate();
                for (var M = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, ma = M.exec(p); ma;) {
                    switch (ma[2] || "d") {
                    case "d":
                    case "D":
                        u += parseInt(ma[1], 10);
                        break;
                    case "w":
                    case "W":
                        u += parseInt(ma[1], 10) * 7;
                        break;
                    case "m":
                    case "M":
                        E += parseInt(ma[1], 10);
                        u = Math.min(u, a.datepicker._getDaysInMonth(z, E));
                        break;
                    case "y":
                    case "Y":
                        z += parseInt(ma[1], 10);
                        u = Math.min(u, a.datepicker._getDaysInMonth(z, E))
                    }
                    ma = M.exec(p)
                }
                return new Date(z, E, u)
            }(k) : typeof k == "number" ? isNaN(k) ? q : s(k) : new Date(k.getTime())) && k.toString() == "Invalid Date" ? q : k) {
                k.setHours(0);
                k.setMinutes(0);
                k.setSeconds(0);
                k.setMilliseconds(0)
            }
            return this._daylightSavingAdjust(k)
        },
        _daylightSavingAdjust: function (c) {
            if (!c) return null;
            c.setHours(c.getHours() > 12 ? c.getHours() + 2 : 0);
            return c
        },
        _setDate: function (c, k, q) {
            var s = !k,
                p = c.selectedMonth,
                n = c.selectedYear;
            k = this._restrictMinMax(c, this._determineDate(c, k, new Date));
            c.selectedDay = c.currentDay = k.getDate();
            c.drawMonth = c.selectedMonth = c.currentMonth = k.getMonth();
            c.drawYear = c.selectedYear = c.currentYear = k.getFullYear();
            if ((p != c.selectedMonth || n != c.selectedYear) && !q) this._notifyChange(c);
            this._adjustInstDate(c);
            if (c.input) c.input.val(s ? "" : this._formatDate(c))
        },
        _getDate: function (c) {
            return !c.currentYear || c.input && c.input.val() == "" ? null : this._daylightSavingAdjust(new Date(c.currentYear, c.currentMonth, c.currentDay))
        },
        _generateHTML: function (c) {
            var k = new Date;
            k = this._daylightSavingAdjust(new Date(k.getFullYear(), k.getMonth(), k.getDate()));
            var q = this._get(c, "isRTL"),
                s = this._get(c, "showButtonPanel"),
                p = this._get(c, "hideIfNoPrevNext"),
                n = this._get(c, "navigationAsDateFormat"),
                u = this._getNumberOfMonths(c),
                z = this._get(c, "showCurrentAtPos"),
                E = this._get(c, "stepMonths"),
                M = u[0] != 1 || u[1] != 1,
                ma = this._daylightSavingAdjust(!c.currentDay ? new Date(9999, 9, 9) : new Date(c.currentYear, c.currentMonth, c.currentDay)),
                oa = this._getMinMaxDate(c, "min"),
                Da = this._getMinMaxDate(c, "max");
            z = c.drawMonth - z;
            var ya = c.drawYear;
            if (z < 0) {
                z += 12;
                ya--
            }
            if (Da) {
                var pa = this._daylightSavingAdjust(new Date(Da.getFullYear(), Da.getMonth() - u[0] * u[1] + 1, Da.getDate()));
                for (pa = oa && pa < oa ? oa : pa; this._daylightSavingAdjust(new Date(ya, z, 1)) > pa;) {
                    z--;
                    if (z < 0) {
                        z = 11;
                        ya--
                    }
                }
            }
            c.drawMonth = z;
            c.drawYear = ya;
            pa = this._get(c, "prevText");
            pa = !n ? pa : this.formatDate(pa, this._daylightSavingAdjust(new Date(ya, z - E, 1)), this._getFormatConfig(c));
            pa = this._canAdjustMonth(c, -1, ya, z) ? '<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery_' + j + ".datepicker._adjustDate('#" + c.id + "', -" + E + ", 'M');\" title=\"" + pa + '"><span class="ui-icon ui-icon-circle-triangle-' + (q ? "e" : "w") + '">' + pa + "</span></a>" : p ? "" : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="' + pa + '"><span class="ui-icon ui-icon-circle-triangle-' + (q ? "e" : "w") + '">' + pa + "</span></a>";
            var Ma = this._get(c, "nextText");
            Ma = !n ? Ma : this.formatDate(Ma, this._daylightSavingAdjust(new Date(ya, z + E, 1)), this._getFormatConfig(c));
            p = this._canAdjustMonth(c, 1, ya, z) ? '<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery_' + j + ".datepicker._adjustDate('#" + c.id + "', +" + E + ", 'M');\" title=\"" + Ma + '"><span class="ui-icon ui-icon-circle-triangle-' + (q ? "w" : "e") + '">' + Ma + "</span></a>" : p ? "" : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="' + Ma + '"><span class="ui-icon ui-icon-circle-triangle-' + (q ? "w" : "e") + '">' + Ma + "</span></a>";
            E = this._get(c, "currentText");
            Ma = this._get(c, "gotoCurrent") && c.currentDay ? ma : k;
            E = !n ? E : this.formatDate(E, Ma, this._getFormatConfig(c));
            n = !c.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + j + '.datepicker._hideDatepicker();">' + this._get(c, "closeText") + "</button>" : "";
            s = s ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (q ? n : "") + (this._isInRange(c, Ma) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + j + ".datepicker._gotoToday('#" + c.id + "');\">" + E + "</button>" : "") + (q ? "" : n) + "</div>" : "";
            n = parseInt(this._get(c, "firstDay"), 10);
            n = isNaN(n) ? 0 : n;
            E = this._get(c, "showWeek");
            Ma = this._get(c, "dayNames");
            this._get(c, "dayNamesShort");
            var Z = this._get(c, "dayNamesMin"),
                ja = this._get(c, "monthNames"),
                C = this._get(c, "monthNamesShort"),
                x = this._get(c, "beforeShowDay"),
                K = this._get(c, "showOtherMonths"),
                y = this._get(c, "selectOtherMonths");
            this._get(c, "calculateWeek");
            for (var d = this._getDefaultDate(c), Q = "", N = 0; N < u[0]; N++) {
                var H = "";
                this.maxRows = 4;
                for (var O = 0; O < u[1]; O++) {
                    var Y = this._daylightSavingAdjust(new Date(ya, z, c.selectedDay)),
                        L = " ui-corner-all",
                        S = "";
                    if (M) {
                        S += '<div class="ui-datepicker-group';
                        if (u[1] > 1) switch (O) {
                        case 0:
                            S += " ui-datepicker-group-first";
                            L = " ui-corner-" + (q ? "right" : "left");
                            break;
                        case u[1] - 1:
                            S += " ui-datepicker-group-last";
                            L = " ui-corner-" + (q ? "left" : "right");
                            break;
                        default:
                            S += " ui-datepicker-group-middle";
                            L = ""
                        }
                        S += '">'
                    }
                    S += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + L + '">' + (/all|left/.test(L) && N == 0 ? q ? p : pa : "") + (/all|right/.test(L) && N == 0 ? q ? pa : p : "") + this._generateMonthYearHeader(c, z, ya, oa, Da, N > 0 || O > 0, ja, C) + '</div><table class="ui-datepicker-calendar"><thead><tr>';
                    var ga = E ? '<th class="ui-datepicker-week-col">' + this._get(c, "weekHeader") + "</th>" : "";
                    for (L = 0; L < 7; L++) {
                        var la = (L + n) % 7;
                        ga += "<th" + ((L + n + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : "") + '><span title="' + Ma[la] + '">' + Z[la] + "</span></th>"
                    }
                    S += ga + "</tr></thead><tbody>";
                    ga = this._getDaysInMonth(ya, z);
                    if (ya == c.selectedYear && z == c.selectedMonth) c.selectedDay = Math.min(c.selectedDay, ga);
                    L = (this._getFirstDayOfMonth(ya, z) - n + 7) % 7;
                    ga = Math.ceil((L + ga) / 7);
                    this.maxRows = ga = M ? this.maxRows > ga ? this.maxRows : ga : ga;
                    la = this._daylightSavingAdjust(new Date(ya, z, 1 - L));
                    for (var Ga = 0; Ga < ga; Ga++) {
                        S += "<tr>";
                        var Na = !E ? "" : '<td class="ui-datepicker-week-col">' + this._get(c, "calculateWeek")(la) + "</td>";
                        for (L = 0; L < 7; L++) {
                            var G = x ? x.apply(c.input ? c.input[0] : null, [la]) : [true, ""],
                                da = la.getMonth() != z,
                                sa = da && !y || !G[0] || oa && la < oa || Da && la > Da;
                            Na += '<td class="' + ((L + n + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (da ? " ui-datepicker-other-month" : "") + (la.getTime() == Y.getTime() && z == c.selectedMonth && c._keyEvent || d.getTime() == la.getTime() && d.getTime() == Y.getTime() ? " " + this._dayOverClass : "") + (sa ? " " + this._unselectableClass + " ui-state-disabled" : "") + (da && !K ? "" : " " + G[1] + (la.getTime() == ma.getTime() ? " " + this._currentClass : "") + (la.getTime() == k.getTime() ? " ui-datepicker-today" : "")) + '"' + ((!da || K) && G[2] ? ' title="' + G[2] + '"' : "") + (sa ? "" : ' onclick="DP_jQuery_' + j + ".datepicker._selectDay('#" + c.id + "'," + la.getMonth() + "," + la.getFullYear() + ', this);return false;"') + ">" + (da && !K ? "&#xa0;" : sa ? '<span class="ui-state-default">' + la.getDate() + "</span>" : '<a class="ui-state-default' + (la.getTime() == k.getTime() ? " ui-state-highlight" : "") + (la.getTime() == ma.getTime() ? " ui-state-active" : "") + (da ? " ui-priority-secondary" : "") + '" href="#">' + la.getDate() + "</a>") + "</td>";
                            la.setDate(la.getDate() + 1);
                            la = this._daylightSavingAdjust(la)
                        }
                        S += Na + "</tr>"
                    }
                    z++;
                    if (z > 11) {
                        z = 0;
                        ya++
                    }
                    S += "</tbody></table>" + (M ? "</div>" + (u[0] > 0 && O == u[1] - 1 ? '<div class="ui-datepicker-row-break"></div>' : "") : "");
                    H += S
                }
                Q += H
            }
            Q += s + (a.browser.msie && parseInt(a.browser.version, 10) < 7 && !c.inline ? '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : "");
            c._keyEvent = false;
            return Q
        },
        _generateMonthYearHeader: function (c, k, q, s, p, n, u, z) {
            var E = this._get(c, "changeMonth"),
                M = this._get(c, "changeYear"),
                ma = this._get(c, "showMonthAfterYear"),
                oa = '<div class="ui-datepicker-title">',
                Da = "";
            if (n || !E) Da += '<span class="ui-datepicker-month">' + u[k] + "</span>";
            else {
                u = s && s.getFullYear() == q;
                var ya = p && p.getFullYear() == q;
                Da += '<select class="ui-datepicker-month" onchange="DP_jQuery_' + j + ".datepicker._selectMonthYear('#" + c.id + "', this, 'M');\" >";
                for (var pa = 0; pa < 12; pa++) if ((!u || pa >= s.getMonth()) && (!ya || pa <= p.getMonth())) Da += '<option value="' + pa + '"' + (pa == k ? ' selected="selected"' : "") + ">" + z[pa] + "</option>";
                Da += "</select>"
            }
            ma || (oa += Da + (n || !(E && M) ? "&#xa0;" : ""));
            if (!c.yearshtml) {
                c.yearshtml = "";
                if (n || !M) oa += '<span class="ui-datepicker-year">' + q + "</span>";
                else {
                    z = this._get(c, "yearRange").split(":");
                    var Ma = (new Date).getFullYear();
                    u = function (Z) {
                        Z = Z.match(/c[+-].*/) ? q + parseInt(Z.substring(1), 10) : Z.match(/[+-].*/) ? Ma + parseInt(Z, 10) : parseInt(Z, 10);
                        return isNaN(Z) ? Ma : Z
                    };
                    k = u(z[0]);
                    z = Math.max(k, u(z[1] || ""));
                    k = s ? Math.max(k, s.getFullYear()) : k;
                    z = p ? Math.min(z, p.getFullYear()) : z;
                    for (c.yearshtml += '<select class="ui-datepicker-year" onchange="DP_jQuery_' + j + ".datepicker._selectMonthYear('#" + c.id + "', this, 'Y');\" >"; k <= z; k++) c.yearshtml += '<option value="' + k + '"' + (k == q ? ' selected="selected"' : "") + ">" + k + "</option>";
                    c.yearshtml += "</select>";
                    oa += c.yearshtml;
                    c.yearshtml = null
                }
            }
            oa += this._get(c, "yearSuffix");
            if (ma) oa += (n || !(E && M) ? "&#xa0;" : "") + Da;
            oa += "</div>";
            return oa
        },
        _adjustInstDate: function (c, k, q) {
            var s = c.drawYear + (q == "Y" ? k : 0),
                p = c.drawMonth + (q == "M" ? k : 0);
            k = Math.min(c.selectedDay, this._getDaysInMonth(s, p)) + (q == "D" ? k : 0);
            s = this._restrictMinMax(c, this._daylightSavingAdjust(new Date(s, p, k)));
            c.selectedDay = s.getDate();
            c.drawMonth = c.selectedMonth = s.getMonth();
            c.drawYear = c.selectedYear = s.getFullYear();
            if (q == "M" || q == "Y") this._notifyChange(c)
        },
        _restrictMinMax: function (c, k) {
            var q = this._getMinMaxDate(c, "min"),
                s = this._getMinMaxDate(c, "max");
            q = q && k < q ? q : k;
            return s && q > s ? s : q
        },
        _notifyChange: function (c) {
            var k = this._get(c, "onChangeMonthYear");
            if (k) k.apply(c.input ? c.input[0] : null, [c.selectedYear, c.selectedMonth + 1, c])
        },
        _getNumberOfMonths: function (c) {
            c = this._get(c, "numberOfMonths");
            return c == null ? [1, 1] : typeof c == "number" ? [1, c] : c
        },
        _getMinMaxDate: function (c, k) {
            return this._determineDate(c, this._get(c, k + "Date"), null)
        },
        _getDaysInMonth: function (c, k) {
            return 32 - this._daylightSavingAdjust(new Date(c, k, 32)).getDate()
        },
        _getFirstDayOfMonth: function (c, k) {
            return (new Date(c, k, 1)).getDay()
        },
        _canAdjustMonth: function (c, k, q, s) {
            var p = this._getNumberOfMonths(c);
            q = this._daylightSavingAdjust(new Date(q, s + (k < 0 ? k : p[0] * p[1]), 1));
            k < 0 && q.setDate(this._getDaysInMonth(q.getFullYear(), q.getMonth()));
            return this._isInRange(c, q)
        },
        _isInRange: function (c, k) {
            var q = this._getMinMaxDate(c, "min"),
                s = this._getMinMaxDate(c, "max");
            return (!q || k.getTime() >= q.getTime()) && (!s || k.getTime() <= s.getTime())
        },
        _getFormatConfig: function (c) {
            var k = this._get(c, "shortYearCutoff");
            k = typeof k != "string" ? k : (new Date).getFullYear() % 100 + parseInt(k, 10);
            return {
                shortYearCutoff: k,
                dayNamesShort: this._get(c, "dayNamesShort"),
                dayNames: this._get(c, "dayNames"),
                monthNamesShort: this._get(c, "monthNamesShort"),
                monthNames: this._get(c, "monthNames")
            }
        },
        _formatDate: function (c, k, q, s) {
            if (!k) {
                c.currentDay = c.selectedDay;
                c.currentMonth = c.selectedMonth;
                c.currentYear = c.selectedYear
            }
            k = k ? typeof k == "object" ? k : this._daylightSavingAdjust(new Date(s, q, k)) : this._daylightSavingAdjust(new Date(c.currentYear, c.currentMonth, c.currentDay));
            return this.formatDate(this._get(c, "dateFormat"), k, this._getFormatConfig(c))
        }
    });
    a.fn.datepicker = function (c) {
        if (!this.length) return this;
        if (!a.datepicker.initialized) {
            a(document).mousedown(a.datepicker._checkExternalClick).find("body").append(a.datepicker.dpDiv);
            a.datepicker.initialized = true
        }
        var k = Array.prototype.slice.call(arguments, 1);
        if (typeof c == "string" && (c == "isDisabled" || c == "getDate" || c == "widget")) return a.datepicker["_" + c + "Datepicker"].apply(a.datepicker, [this[0]].concat(k));
        if (c == "option" && arguments.length == 2 && typeof arguments[1] == "string") return a.datepicker["_" + c + "Datepicker"].apply(a.datepicker, [this[0]].concat(k));
        return this.each(function () {
            typeof c == "string" ? a.datepicker["_" + c + "Datepicker"].apply(a.datepicker, [this].concat(k)) : a.datepicker._attachDatepicker(this, c)
        })
    };
    a.datepicker = new i;
    a.datepicker.initialized = false;
    a.datepicker.uuid = (new Date).getTime();
    a.datepicker.version = "1.8.18";
    window["DP_jQuery_" + j] = a
})(jQuery);
jQuery.effects ||
function (a, f) {
    function i(p) {
        var n;
        if (p && p.constructor == Array && p.length == 3) return p;
        if (n = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(p)) return [parseInt(n[1], 10), parseInt(n[2], 10), parseInt(n[3], 10)];
        if (n = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(p)) return [parseFloat(n[1]) * 2.55, parseFloat(n[2]) * 2.55, parseFloat(n[3]) * 2.55];
        if (n = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(p)) return [parseInt(n[1], 16), parseInt(n[2], 16), parseInt(n[3], 16)];
        if (n = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(p)) return [parseInt(n[1] + n[1], 16), parseInt(n[2] + n[2], 16), parseInt(n[3] + n[3], 16)];
        if (/rgba\(0, 0, 0, 0\)/.exec(p)) return k.transparent;
        return k[a.trim(p).toLowerCase()]
    }
    function g() {
        var p = document.defaultView ? document.defaultView.getComputedStyle(this, null) : this.currentStyle,
            n = {},
            u, z;
        if (p && p.length && p[0] && p[p[0]]) for (var E = p.length; E--;) {
            u = p[E];
            if (typeof p[u] == "string") {
                z = u.replace(/\-(\w)/g, function (M, ma) {
                    return ma.toUpperCase()
                });
                n[z] = p[u]
            }
        } else for (u in p) if (typeof p[u] === "string") n[u] = p[u];
        return n
    }
    function h(p) {
        var n, u;
        for (n in p) {
            u = p[n];
            if (u == null || a.isFunction(u) || n in s || /scrollbar/.test(n) || !/color/i.test(n) && isNaN(parseFloat(u))) delete p[n]
        }
        return p
    }
    function j(p, n) {
        var u = {
            _: 0
        },
            z;
        for (z in n) if (p[z] != n[z]) u[z] = n[z];
        return u
    }
    function l(p, n, u, z) {
        if (typeof p == "object") {
            z = n;
            u = null;
            n = p;
            p = n.effect
        }
        if (a.isFunction(n)) {
            z = n;
            u = null;
            n = {}
        }
        if (typeof n == "number" || a.fx.speeds[n]) {
            z = u;
            u = n;
            n = {}
        }
        if (a.isFunction(u)) {
            z = u;
            u = null
        }
        n = n || {};
        u = u || n.duration;
        u = a.fx.off ? 0 : typeof u == "number" ? u : u in a.fx.speeds ? a.fx.speeds[u] : a.fx.speeds._default;
        z = z || n.complete;
        return [p, n, u, z]
    }
    function c(p) {
        if (!p || typeof p === "number" || a.fx.speeds[p]) return true;
        if (typeof p === "string" && !a.effects[p]) return true;
        return false
    }
    a.effects = {};
    a.each(["backgroundColor", "borderBottomColor", "borderLeftColor", "borderRightColor", "borderTopColor", "borderColor", "color", "outlineColor"], function (p, n) {
        a.fx.step[n] = function (u) {
            if (!u.colorInit) {
                var z;
                z = u.elem;
                var E = n,
                    M;
                do {
                    M = a.curCSS(z, E);
                    if (M != "" && M != "transparent" || a.nodeName(z, "body")) break;
                    E = "backgroundColor"
                } while (z = z.parentNode);
                z = i(M);
                u.start = z;
                u.end = i(u.end);
                u.colorInit = true
            }
            u.elem.style[n] = "rgb(" + Math.max(Math.min(parseInt(u.pos * (u.end[0] - u.start[0]) + u.start[0], 10), 255), 0) + "," + Math.max(Math.min(parseInt(u.pos * (u.end[1] - u.start[1]) + u.start[1], 10), 255), 0) + "," + Math.max(Math.min(parseInt(u.pos * (u.end[2] - u.start[2]) + u.start[2], 10), 255), 0) + ")"
        }
    });
    var k = {
        aqua: [0, 255, 255],
        azure: [240, 255, 255],
        beige: [245, 245, 220],
        black: [0, 0, 0],
        blue: [0, 0, 255],
        brown: [165, 42, 42],
        cyan: [0, 255, 255],
        darkblue: [0, 0, 139],
        darkcyan: [0, 139, 139],
        darkgrey: [169, 169, 169],
        darkgreen: [0, 100, 0],
        darkkhaki: [189, 183, 107],
        darkmagenta: [139, 0, 139],
        darkolivegreen: [85, 107, 47],
        darkorange: [255, 140, 0],
        darkorchid: [153, 50, 204],
        darkred: [139, 0, 0],
        darksalmon: [233, 150, 122],
        darkviolet: [148, 0, 211],
        fuchsia: [255, 0, 255],
        gold: [255, 215, 0],
        green: [0, 128, 0],
        indigo: [75, 0, 130],
        khaki: [240, 230, 140],
        lightblue: [173, 216, 230],
        lightcyan: [224, 255, 255],
        lightgreen: [144, 238, 144],
        lightgrey: [211, 211, 211],
        lightpink: [255, 182, 193],
        lightyellow: [255, 255, 224],
        lime: [0, 255, 0],
        magenta: [255, 0, 255],
        maroon: [128, 0, 0],
        navy: [0, 0, 128],
        olive: [128, 128, 0],
        orange: [255, 165, 0],
        pink: [255, 192, 203],
        purple: [128, 0, 128],
        violet: [128, 0, 128],
        red: [255, 0, 0],
        silver: [192, 192, 192],
        white: [255, 255, 255],
        yellow: [255, 255, 0],
        transparent: [255, 255, 255]
    },
        q = ["add", "remove", "toggle"],
        s = {
            border: 1,
            borderBottom: 1,
            borderColor: 1,
            borderLeft: 1,
            borderRight: 1,
            borderTop: 1,
            borderWidth: 1,
            margin: 1,
            padding: 1
        };
    a.effects.animateClass = function (p, n, u, z) {
        if (a.isFunction(u)) {
            z = u;
            u = null
        }
        return this.queue(function () {
            var E = a(this),
                M = E.attr("style") || " ",
                ma = h(g.call(this)),
                oa, Da = E.attr("class");
            a.each(q, function (ya, pa) {
                if (p[pa]) E[pa + "Class"](p[pa])
            });
            oa = h(g.call(this));
            E.attr("class", Da);
            E.animate(j(ma, oa), {
                queue: false,
                duration: n,
                easing: u,
                complete: function () {
                    a.each(q, function (ya, pa) {
                        if (p[pa]) E[pa + "Class"](p[pa])
                    });
                    if (typeof E.attr("style") == "object") {
                        E.attr("style").cssText = "";
                        E.attr("style").cssText = M
                    } else E.attr("style", M);
                    z && z.apply(this, arguments);
                    a.dequeue(this)
                }
            })
        })
    };
    a.fn.extend({
        _addClass: a.fn.addClass,
        addClass: function (p, n, u, z) {
            return n ? a.effects.animateClass.apply(this, [{
                add: p
            },
            n, u, z]) : this._addClass(p)
        },
        _removeClass: a.fn.removeClass,
        removeClass: function (p, n, u, z) {
            return n ? a.effects.animateClass.apply(this, [{
                remove: p
            },
            n, u, z]) : this._removeClass(p)
        },
        _toggleClass: a.fn.toggleClass,
        toggleClass: function (p, n, u, z, E) {
            return typeof n == "boolean" || n === f ? u ? a.effects.animateClass.apply(this, [n ? {
                add: p
            } : {
                remove: p
            },
            u, z, E]) : this._toggleClass(p, n) : a.effects.animateClass.apply(this, [{
                toggle: p
            },
            n, u, z])
        },
        switchClass: function (p, n, u, z, E) {
            return a.effects.animateClass.apply(this, [{
                add: n,
                remove: p
            },
            u, z, E])
        }
    });
    a.extend(a.effects, {
        version: "1.8.18",
        save: function (p, n) {
            for (var u = 0; u < n.length; u++) n[u] !== null && p.data("ec.storage." + n[u], p[0].style[n[u]])
        },
        restore: function (p, n) {
            for (var u = 0; u < n.length; u++) n[u] !== null && p.css(n[u], p.data("ec.storage." + n[u]))
        },
        setMode: function (p, n) {
            if (n == "toggle") n = p.is(":hidden") ? "show" : "hide";
            return n
        },
        getBaseline: function (p, n) {
            var u, z;
            switch (p[0]) {
            case "top":
                u = 0;
                break;
            case "middle":
                u = 0.5;
                break;
            case "bottom":
                u = 1;
                break;
            default:
                u = p[0] / n.height
            }
            switch (p[1]) {
            case "left":
                z = 0;
                break;
            case "center":
                z = 0.5;
                break;
            case "right":
                z = 1;
                break;
            default:
                z = p[1] / n.width
            }
            return {
                x: z,
                y: u
            }
        },
        createWrapper: function (p) {
            if (p.parent().is(".ui-effects-wrapper")) return p.parent();
            var n = {
                width: p.outerWidth(true),
                height: p.outerHeight(true),
                "float": p.css("float")
            },
                u = a("<div></div>").addClass("ui-effects-wrapper").css({
                    fontSize: "100%",
                    background: "transparent",
                    border: "none",
                    margin: 0,
                    padding: 0
                }),
                z = document.activeElement;
            p.wrap(u);
            if (p[0] === z || a.contains(p[0], z)) a(z).focus();
            u = p.parent();
            if (p.css("position") == "static") {
                u.css({
                    position: "relative"
                });
                p.css({
                    position: "relative"
                })
            } else {
                a.extend(n, {
                    position: p.css("position"),
                    zIndex: p.css("z-index")
                });
                a.each(["top", "left", "bottom", "right"], function (E, M) {
                    n[M] = p.css(M);
                    if (isNaN(parseInt(n[M], 10))) n[M] = "auto"
                });
                p.css({
                    position: "relative",
                    top: 0,
                    left: 0,
                    right: "auto",
                    bottom: "auto"
                })
            }
            return u.css(n).show()
        },
        removeWrapper: function (p) {
            var n, u = document.activeElement;
            if (p.parent().is(".ui-effects-wrapper")) {
                n = p.parent().replaceWith(p);
                if (p[0] === u || a.contains(p[0], u)) a(u).focus();
                return n
            }
            return p
        },
        setTransition: function (p, n, u, z) {
            z = z || {};
            a.each(n, function (E, M) {
                unit = p.cssUnit(M);
                if (unit[0] > 0) z[M] = unit[0] * u + unit[1]
            });
            return z
        }
    });
    a.fn.extend({
        effect: function (p) {
            var n = l.apply(this, arguments),
                u = {
                    options: n[1],
                    duration: n[2],
                    callback: n[3]
                };
            n = u.options.mode;
            var z = a.effects[p];
            if (a.fx.off || !z) return n ? this[n](u.duration, u.callback) : this.each(function () {
                u.callback && u.callback.call(this)
            });
            return z.call(this, u)
        },
        _show: a.fn.show,
        show: function (p) {
            if (c(p)) return this._show.apply(this, arguments);
            else {
                var n = l.apply(this, arguments);
                n[1].mode = "show";
                return this.effect.apply(this, n)
            }
        },
        _hide: a.fn.hide,
        hide: function (p) {
            if (c(p)) return this._hide.apply(this, arguments);
            else {
                var n = l.apply(this, arguments);
                n[1].mode = "hide";
                return this.effect.apply(this, n)
            }
        },
        __toggle: a.fn.toggle,
        toggle: function (p) {
            if (c(p) || typeof p === "boolean" || a.isFunction(p)) return this.__toggle.apply(this, arguments);
            else {
                var n = l.apply(this, arguments);
                n[1].mode = "toggle";
                return this.effect.apply(this, n)
            }
        },
        cssUnit: function (p) {
            var n = this.css(p),
                u = [];
            a.each(["em", "px", "%", "pt"], function (z, E) {
                if (n.indexOf(E) > 0) u = [parseFloat(n), E]
            });
            return u
        }
    });
    a.easing.jswing = a.easing.swing;
    a.extend(a.easing, {
        def: "easeOutQuad",
        swing: function (p, n, u, z, E) {
            return a.easing[a.easing.def](p, n, u, z, E)
        },
        easeInQuad: function (p, n, u, z, E) {
            return z * (n /= E) * n + u
        },
        easeOutQuad: function (p, n, u, z, E) {
            return -z * (n /= E) * (n - 2) + u
        },
        easeInOutQuad: function (p, n, u, z, E) {
            if ((n /= E / 2) < 1) return z / 2 * n * n + u;
            return -z / 2 * (--n * (n - 2) - 1) + u
        },
        easeInCubic: function (p, n, u, z, E) {
            return z * (n /= E) * n * n + u
        },
        easeOutCubic: function (p, n, u, z, E) {
            return z * ((n = n / E - 1) * n * n + 1) + u
        },
        easeInOutCubic: function (p, n, u, z, E) {
            if ((n /= E / 2) < 1) return z / 2 * n * n * n + u;
            return z / 2 * ((n -= 2) * n * n + 2) + u
        },
        easeInQuart: function (p, n, u, z, E) {
            return z * (n /= E) * n * n * n + u
        },
        easeOutQuart: function (p, n, u, z, E) {
            return -z * ((n = n / E - 1) * n * n * n - 1) + u
        },
        easeInOutQuart: function (p, n, u, z, E) {
            if ((n /= E / 2) < 1) return z / 2 * n * n * n * n + u;
            return -z / 2 * ((n -= 2) * n * n * n - 2) + u
        },
        easeInQuint: function (p, n, u, z, E) {
            return z * (n /= E) * n * n * n * n + u
        },
        easeOutQuint: function (p, n, u, z, E) {
            return z * ((n = n / E - 1) * n * n * n * n + 1) + u
        },
        easeInOutQuint: function (p, n, u, z, E) {
            if ((n /= E / 2) < 1) return z / 2 * n * n * n * n * n + u;
            return z / 2 * ((n -= 2) * n * n * n * n + 2) + u
        },
        easeInSine: function (p, n, u, z, E) {
            return -z * Math.cos(n / E * (Math.PI / 2)) + z + u
        },
        easeOutSine: function (p, n, u, z, E) {
            return z * Math.sin(n / E * (Math.PI / 2)) + u
        },
        easeInOutSine: function (p, n, u, z, E) {
            return -z / 2 * (Math.cos(Math.PI * n / E) - 1) + u
        },
        easeInExpo: function (p, n, u, z, E) {
            return n == 0 ? u : z * Math.pow(2, 10 * (n / E - 1)) + u
        },
        easeOutExpo: function (p, n, u, z, E) {
            return n == E ? u + z : z * (-Math.pow(2, -10 * n / E) + 1) + u
        },
        easeInOutExpo: function (p, n, u, z, E) {
            if (n == 0) return u;
            if (n == E) return u + z;
            if ((n /= E / 2) < 1) return z / 2 * Math.pow(2, 10 * (n - 1)) + u;
            return z / 2 * (-Math.pow(2, -10 * --n) + 2) + u
        },
        easeInCirc: function (p, n, u, z, E) {
            return -z * (Math.sqrt(1 - (n /= E) * n) - 1) + u
        },
        easeOutCirc: function (p, n, u, z, E) {
            return z * Math.sqrt(1 - (n = n / E - 1) * n) + u
        },
        easeInOutCirc: function (p, n, u, z, E) {
            if ((n /= E / 2) < 1) return -z / 2 * (Math.sqrt(1 - n * n) - 1) + u;
            return z / 2 * (Math.sqrt(1 - (n -= 2) * n) + 1) + u
        },
        easeInElastic: function (p, n, u, z, E) {
            p = 1.70158;
            var M = 0,
                ma = z;
            if (n == 0) return u;
            if ((n /= E) == 1) return u + z;
            M || (M = E * 0.3);
            if (ma < Math.abs(z)) {
                ma = z;
                p = M / 4
            } else p = M / (2 * Math.PI) * Math.asin(z / ma);
            return -(ma * Math.pow(2, 10 * (n -= 1)) * Math.sin((n * E - p) * 2 * Math.PI / M)) + u
        },
        easeOutElastic: function (p, n, u, z, E) {
            p = 1.70158;
            var M = 0,
                ma = z;
            if (n == 0) return u;
            if ((n /= E) == 1) return u + z;
            M || (M = E * 0.3);
            if (ma < Math.abs(z)) {
                ma = z;
                p = M / 4
            } else p = M / (2 * Math.PI) * Math.asin(z / ma);
            return ma * Math.pow(2, -10 * n) * Math.sin((n * E - p) * 2 * Math.PI / M) + z + u
        },
        easeInOutElastic: function (p, n, u, z, E) {
            p = 1.70158;
            var M = 0,
                ma = z;
            if (n == 0) return u;
            if ((n /= E / 2) == 2) return u + z;
            M || (M = E * 0.3 * 1.5);
            if (ma < Math.abs(z)) {
                ma = z;
                p = M / 4
            } else p = M / (2 * Math.PI) * Math.asin(z / ma);
            if (n < 1) return -0.5 * ma * Math.pow(2, 10 * (n -= 1)) * Math.sin((n * E - p) * 2 * Math.PI / M) + u;
            return ma * Math.pow(2, -10 * (n -= 1)) * Math.sin((n * E - p) * 2 * Math.PI / M) * 0.5 + z + u
        },
        easeInBack: function (p, n, u, z, E, M) {
            if (M == f) M = 1.70158;
            return z * (n /= E) * n * ((M + 1) * n - M) + u
        },
        easeOutBack: function (p, n, u, z, E, M) {
            if (M == f) M = 1.70158;
            return z * ((n = n / E - 1) * n * ((M + 1) * n + M) + 1) + u
        },
        easeInOutBack: function (p, n, u, z, E, M) {
            if (M == f) M = 1.70158;
            if ((n /= E / 2) < 1) return z / 2 * n * n * (((M *= 1.525) + 1) * n - M) + u;
            return z / 2 * ((n -= 2) * n * (((M *= 1.525) + 1) * n + M) + 2) + u
        },
        easeInBounce: function (p, n, u, z, E) {
            return z - a.easing.easeOutBounce(p, E - n, 0, z, E) + u
        },
        easeOutBounce: function (p, n, u, z, E) {
            return (n /= E) < 1 / 2.75 ? z * 7.5625 * n * n + u : n < 2 / 2.75 ? z * (7.5625 * (n -= 1.5 / 2.75) * n + 0.75) + u : n < 2.5 / 2.75 ? z * (7.5625 * (n -= 2.25 / 2.75) * n + 0.9375) + u : z * (7.5625 * (n -= 2.625 / 2.75) * n + 0.984375) + u
        },
        easeInOutBounce: function (p, n, u, z, E) {
            if (n < E / 2) return a.easing.easeInBounce(p, n * 2, 0, z, E) * 0.5 + u;
            return a.easing.easeOutBounce(p, n * 2 - E, 0, z, E) * 0.5 + z * 0.5 + u
        }
    })
}(jQuery);
(function (a) {
    a.effects.highlight = function (f) {
        return this.queue(function () {
            var i = a(this),
                g = ["backgroundImage", "backgroundColor", "opacity"],
                h = a.effects.setMode(i, f.options.mode || "show"),
                j = {
                    backgroundColor: i.css("backgroundColor")
                };
            if (h == "hide") j.opacity = 0;
            a.effects.save(i, g);
            i.show().css({
                backgroundImage: "none",
                backgroundColor: f.options.color || "#ffff99"
            }).animate(j, {
                queue: false,
                duration: f.duration,
                easing: f.options.easing,
                complete: function () {
                    h == "hide" && i.hide();
                    a.effects.restore(i, g);
                    h == "show" && !a.support.opacity && this.style.removeAttribute("filter");
                    f.callback && f.callback.apply(this, arguments);
                    i.dequeue()
                }
            })
        })
    }
})(jQuery);
(function (a) {
    function f(i) {
        return i && i.constructor === Number ? i + "px" : i
    }
    a.fn.bgiframe = a.browser.msie && /msie 6\.0/i.test(navigator.userAgent) ?
    function (i) {
        i = a.extend({
            top: "auto",
            left: "auto",
            width: "auto",
            height: "auto",
            opacity: true,
            src: "javascript:false;"
        }, i);
        var g = '<iframe class="bgiframe"frameborder="0"tabindex="-1"src="' + i.src + '"style="display:block;position:absolute;z-index:-1;' + (i.opacity !== false ? "filter:Alpha(Opacity='0');" : "") + "top:" + (i.top == "auto" ? "expression(((parseInt(this.parentNode.currentStyle.borderTopWidth)||0)*-1)+'px')" : f(i.top)) + ";left:" + (i.left == "auto" ? "expression(((parseInt(this.parentNode.currentStyle.borderLeftWidth)||0)*-1)+'px')" : f(i.left)) + ";width:" + (i.width == "auto" ? "expression(this.parentNode.offsetWidth+'px')" : f(i.width)) + ";height:" + (i.height == "auto" ? "expression(this.parentNode.offsetHeight+'px')" : f(i.height)) + ';"/>';
        return this.each(function () {
            a(this).children("iframe.bgiframe").length === 0 && this.insertBefore(document.createElement(g), this.firstChild)
        })
    } : function () {
        return this
    };
    a.fn.bgIframe = a.fn.bgiframe
})(jQuery);
(function (a) {
    a.toJSON = function (g) {
        if (typeof JSON == "object" && JSON.stringify) return JSON.stringify(g);
        var h = typeof g;
        if (g === null) return "null";
        if (h != "undefined") {
            if (h == "number" || h == "boolean") return g + "";
            if (h == "string") return a.quoteString(g);
            if (h == "object") {
                if (typeof g.toJSON == "function") return a.toJSON(g.toJSON());
                if (g.constructor === Date) {
                    var j = g.getUTCMonth() + 1;
                    if (j < 10) j = "0" + j;
                    var l = g.getUTCDate();
                    if (l < 10) l = "0" + l;
                    h = g.getUTCFullYear();
                    var c = g.getUTCHours();
                    if (c < 10) c = "0" + c;
                    var k = g.getUTCMinutes();
                    if (k < 10) k = "0" + k;
                    var q = g.getUTCSeconds();
                    if (q < 10) q = "0" + q;
                    g = g.getUTCMilliseconds();
                    if (g < 100) g = "0" + g;
                    if (g < 10) g = "0" + g;
                    return '"' + h + "-" + j + "-" + l + "T" + c + ":" + k + ":" + q + "." + g + 'Z"'
                }
                if (g.constructor === Array) {
                    j = [];
                    for (l = 0; l < g.length; l++) j.push(a.toJSON(g[l]) || "null");
                    return "[" + j.join(",") + "]"
                }
                j = [];
                for (l in g) {
                    h = typeof l;
                    if (h == "number") h = '"' + l + '"';
                    else if (h == "string") h = a.quoteString(l);
                    else continue;
                    if (typeof g[l] != "function") {
                        c = a.toJSON(g[l]);
                        j.push(h + ":" + c)
                    }
                }
                return "{" + j.join(", ") + "}"
            }
        }
    };
    a.evalJSON = function (g) {
        if (typeof JSON == "object" && JSON.parse) return JSON.parse(g);
        return eval("(" + g + ")")
    };
    a.secureEvalJSON = function (g) {
        if (typeof JSON == "object" && JSON.parse) return JSON.parse(g);
        var h;
        h = g.replace(/\\["\\\/bfnrtu]/g, "@");
        h = h.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]");
        h = h.replace(/(?:^|:|,)(?:\s*\[)+/g, "");
        if (/^[\],:{}\s]*$/.test(h)) return eval("(" + g + ")");
        else throw new SyntaxError("Error parsing JSON, source is not valid.");
    };
    a.quoteString = function (g) {
        if (g.match(f)) return '"' + g.replace(f, function (h) {
            var j = i[h];
            if (typeof j === "string") return j;
            j = h.charCodeAt();
            return "\\u00" + Math.floor(j / 16).toString(16) + (j % 16).toString(16)
        }) + '"';
        return '"' + g + '"'
    };
    var f = /["\\\x00-\x1f\x7f-\x9f]/g,
        i = {
            "\u0008": "\\b",
            "\t": "\\t",
            "\n": "\\n",
            "\u000c": "\\f",
            "\r": "\\r",
            '"': '\\"',
            "\\": "\\\\"
        }
})(jQuery);
(function (a, f, i) {
    function g() {
        g.history = g.history || [];
        g.history.push(arguments);
        if ("object" === typeof console) {
            var y = console[console.warn ? "warn" : "log"],
                d = Array.prototype.slice.call(arguments);
            if (typeof arguments[0] === "string") d[0] = "qTip2: " + d[0];
            y.apply ? y.apply(console, d) : y(d)
        }
    }
    function h(y) {
        var d;
        if (!y || "object" !== typeof y) return n;
        if (y.metadata === u || "object" !== typeof y.metadata) y.metadata = {
            type: y.metadata
        };
        if ("content" in y) {
            if (y.content === u || "object" !== typeof y.content || y.content.jquery) y.content = {
                text: y.content
            };
            d = y.content.text || n;
            if (!a.isFunction(d) && (!d && !d.attr || d.length < 1 || "object" === typeof d && !d.jquery)) y.content.text = n;
            if ("title" in y.content) {
                if (y.content.title === u || "object" !== typeof y.content.title) y.content.title = {
                    text: y.content.title
                };
                d = y.content.title.text || n;
                if (!a.isFunction(d) && (!d && !d.attr || d.length < 1 || "object" === typeof d && !d.jquery)) y.content.title.text = n
            }
        }
        if ("position" in y) if (y.position === u || "object" !== typeof y.position) y.position = {
            my: y.position,
            at: y.position
        };
        if ("show" in y) if (y.show === u || "object" !== typeof y.show) y.show = y.show.jquery ? {
            target: y.show
        } : {
            event: y.show
        };
        if ("hide" in y) if (y.hide === u || "object" !== typeof y.hide) y.hide = y.hide.jquery ? {
            target: y.hide
        } : {
            event: y.hide
        };
        if ("style" in y) if (y.style === u || "object" !== typeof y.style) y.style = {
            classes: y.style
        };
        a.each(E, function () {
            this.sanitize && this.sanitize(y)
        });
        return y
    }
    function j(y, d, Q, N) {
        function H(J) {
            var fa = 0,
                ka, Ha = d;
            for (J = J.split("."); Ha = Ha[J[fa++]];) if (fa < J.length) ka = Ha;
            return [ka || d, J.pop()]
        }
        function O() {
            var J = d.style.widget;
            W.toggleClass(Da, J).toggleClass(Ma, d.style["default"] && !J);
            ua.content.toggleClass(Da + "-content", J);
            ua.titlebar && ua.titlebar.toggleClass(Da + "-header", J);
            ua.button && ua.button.toggleClass(oa + "-icon", !J)
        }
        function Y(J) {
            if (ua.title) {
                ua.titlebar.remove();
                ua.titlebar = ua.title = ua.button = u;
                J !== n && G.reposition()
            }
        }
        function L() {
            var J = d.content.title.button,
                fa = typeof J === "string" ? J : "Close tooltip";
            ua.button && ua.button.remove();
            ua.button = J.jquery ? J : a("<a />", {
                "class": "ui-state-default ui-tooltip-close " + (d.style.widget ? "" : oa + "-icon"),
                title: fa,
                "aria-label": fa
            }).prepend(a("<span />", {
                "class": "ui-icon ui-icon-close",
                html: "&times;"
            }));
            ua.button.appendTo(ua.titlebar).attr("role", "button").click(function (ka) {
                W.hasClass(ya) || G.hide(ka);
                return n
            });
            G.redraw()
        }
        function S() {
            var J = sa + "-title";
            ua.titlebar && Y();
            ua.titlebar = a("<div />", {
                "class": oa + "-titlebar " + (d.style.widget ? "ui-widget-header" : "")
            }).append(ua.title = a("<div />", {
                id: J,
                "class": oa + "-title",
                "aria-atomic": p
            })).insertBefore(ua.content).delegate(".ui-tooltip-close", "mousedown keydown mouseup keyup mouseout", function (fa) {
                a(this).toggleClass("ui-state-active ui-state-focus", fa.type.substr(-4) === "down")
            }).delegate(".ui-tooltip-close", "mouseover mouseout", function (fa) {
                a(this).toggleClass("ui-state-hover", fa.type === "mouseover")
            });
            if (d.content.title.button) L();
            else G.rendered && G.redraw()
        }
        function ga(J, fa) {
            var ka = ua.title;
            if (!G.rendered || !J) return n;
            if (a.isFunction(J)) J = J.call(y, va.event, G);
            if (J === n) return Y(n);
            else J.jquery && J.length > 0 ? ka.empty().append(J.css({
                display: "block"
            })) : ka.html(J);
            G.redraw();
            fa !== n && G.rendered && W.is(":visible") && G.reposition(va.event)
        }
        function la(J, fa) {
            function ka(Ea) {
                function xa(wa) {
                    if (wa) {
                        delete Xa[wa.src];
                        clearTimeout(G.timers.img[wa.src]);
                        a(wa).unbind(na)
                    }
                    if (a.isEmptyObject(Xa)) {
                        G.redraw();
                        fa !== n && G.reposition(va.event);
                        Ea()
                    }
                }
                var Va, Xa = {};
                if ((Va = Ha.find("img:not([height]):not([width])")).length === 0) return xa();
                Va.each(function (wa, Aa) {
                    if (Xa[Aa.src] === i) {
                        var Ja = 0;
                        (function Ra() {
                            if (Aa.height || Aa.width || Ja > 3) return xa(Aa);
                            Ja += 1;
                            G.timers.img[Aa.src] = setTimeout(Ra, 700)
                        })();
                        a(Aa).bind("error" + na + " load" + na, function () {
                            xa(this)
                        });
                        Xa[Aa.src] = Aa
                    }
                })
            }
            var Ha = ua.content;
            if (!G.rendered || !J) return n;
            if (a.isFunction(J)) J = J.call(y, va.event, G) || "";
            J.jquery && J.length > 0 ? Ha.empty().append(J.css({
                display: "block"
            })) : Ha.html(J);
            if (G.rendered < 0) W.queue("fx", ka);
            else {
                Ba = 0;
                ka(a.noop)
            }
            return G
        }
        function Ga() {
            function J(wa) {
                if (W.hasClass(ya)) return n;
                clearTimeout(G.timers.show);
                clearTimeout(G.timers.hide);
                var Aa = function () {
                        G.toggle(p, wa)
                    };
                if (d.show.delay > 0) G.timers.show = setTimeout(Aa, d.show.delay);
                else Aa()
            }
            function fa(wa) {
                if (W.hasClass(ya) || ta || Ba) return n;
                var Aa = a(wa.relatedTarget || wa.target),
                    Ja = Aa.closest(pa)[0] === W[0];
                Aa = Aa[0] === xa.show[0];
                clearTimeout(G.timers.show);
                clearTimeout(G.timers.hide);
                if (Ea.target === "mouse" && Ja || d.hide.fixed && /mouse(out|leave|move)/.test(wa.type) && (Ja || Aa)) try {
                    wa.preventDefault();
                    wa.stopImmediatePropagation()
                } catch (Ra) {} else if (d.hide.delay > 0) G.timers.hide = setTimeout(function () {
                    G.hide(wa)
                }, d.hide.delay);
                else G.hide(wa)
            }
            function ka(wa) {
                if (W.hasClass(ya)) return n;
                clearTimeout(G.timers.inactive);
                G.timers.inactive = setTimeout(function () {
                    G.hide(wa)
                }, d.hide.inactive)
            }
            function Ha(wa) {
                W.is(":visible") && G.reposition(wa)
            }
            var Ea = d.position,
                xa = {
                    show: d.show.target,
                    hide: d.hide.target,
                    viewport: a(Ea.viewport),
                    document: a(document),
                    body: a(document.body),
                    window: a(f)
                },
                Va = {
                    show: a.trim("" + d.show.event).split(" "),
                    hide: a.trim("" + d.hide.event).split(" ")
                },
                Xa = a.browser.msie && parseInt(a.browser.version, 10) === 6;
            W.bind("mouseenter" + na + " mouseleave" + na, function (wa) {
                var Aa = wa.type === "mouseenter";
                Aa && G.focus(wa);
                W.toggleClass(ja, Aa)
            });
            if (d.hide.fixed) {
                xa.hide = xa.hide.add(W);
                W.bind("mouseover" + na, function () {
                    W.hasClass(ya) || clearTimeout(G.timers.hide)
                })
            }
            if (/mouse(out|leave)/i.test(d.hide.event)) d.hide.leave === "window" && xa.window.bind("mouseout" + na + " blur" + na, function (wa) {
                /select|option/.test(wa.target) && !wa.relatedTarget && G.hide(wa)
            });
            else /mouse(over|enter)/i.test(d.show.event) && xa.hide.bind("mouseleave" + na, function () {
                clearTimeout(G.timers.show)
            });
            ("" + d.hide.event).indexOf("unfocus") > -1 && xa.body.bind("mousedown" + na, function (wa) {
                var Aa = a(wa.target);
                !W.hasClass(ya) && W.is(":visible");
                Aa[0] !== W[0] && Aa.parents(pa).length === 0 && !Aa.closest(y).length && !Aa.attr("disabled") && G.hide(wa)
            });
            if ("number" === typeof d.hide.inactive) {
                xa.show.bind("qtip-" + Q + "-inactive", ka);
                a.each(z.inactiveEvents, function (wa, Aa) {
                    xa.hide.add(ua.tooltip).bind(Aa + na + "-inactive", ka)
                })
            }
            a.each(Va.hide, function (wa, Aa) {
                var Ja = a.inArray(Aa, Va.show),
                    Ra = a(xa.hide);
                if (Ja > -1 && Ra.add(xa.show).length === Ra.length || Aa === "unfocus") {
                    xa.show.bind(Aa + na, function (fb) {
                        W.is(":visible") ? fa(fb) : J(fb)
                    });
                    delete Va.show[Ja]
                } else xa.hide.bind(Aa + na, fa)
            });
            a.each(Va.show, function (wa, Aa) {
                xa.show.bind(Aa + na, J)
            });
            "number" === typeof d.hide.distance && xa.show.add(W).bind("mousemove" + na, function (wa) {
                var Aa = va.origin || {},
                    Ja = d.hide.distance,
                    Ra = Math.abs;
                if (Ra(wa.pageX - Aa.pageX) >= Ja || Ra(wa.pageY - Aa.pageY) >= Ja) G.hide(wa)
            });
            if (Ea.target === "mouse") {
                xa.show.bind("mousemove" + na, function (wa) {
                    M = {
                        pageX: wa.pageX,
                        pageY: wa.pageY,
                        type: "mousemove"
                    }
                });
                if (Ea.adjust.mouse) {
                    if (d.hide.event) {
                        W.bind("mouseleave" + na, function (wa) {
                            if ((wa.relatedTarget || wa.target) !== xa.show[0]) G.hide(wa)
                        });
                        ua.target.bind("mouseenter" + na + " mouseleave" + na, function (wa) {
                            va.onTarget = wa.type === "mouseenter"
                        })
                    }
                    xa.document.bind("mousemove" + na, function (wa) {
                        if (va.onTarget && !W.hasClass(ya) && W.is(":visible")) G.reposition(wa || M)
                    })
                }
            }
            if (Ea.adjust.resize || xa.viewport.length)(a.event.special.resize ? xa.viewport : xa.window).bind("resize" + na, Ha);
            if (xa.viewport.length || Xa && W.css("position") === "fixed") xa.viewport.bind("scroll" + na, Ha)
        }
        function Na() {
            var J = [d.show.target[0], d.hide.target[0], G.rendered && ua.tooltip[0], d.position.container[0], d.position.viewport[0], f, document];
            G.rendered ? a([]).pushStack(a.grep(J, function (fa) {
                return typeof fa === "object"
            })).unbind(na) : d.show.target.unbind(na + "-create")
        }
        var G = this,
            da = document.body,
            sa = oa + "-" + Q,
            ta = 0,
            Ba = 0,
            W = a(),
            na = ".qtip-" + Q,
            ua, va;
        G.id = Q;
        G.rendered = n;
        G.elements = ua = {
            target: y
        };
        G.timers = {
            img: {}
        };
        G.options = d;
        G.checks = {};
        G.plugins = {};
        G.cache = va = {
            event: {},
            target: a(),
            disabled: n,
            attr: N,
            onTarget: n
        };
        G.checks.builtin = {
            "^id$": function (J, fa, ka) {
                J = ka === p ? z.nextid : ka;
                fa = oa + "-" + J;
                if (J !== n && J.length > 0 && !a("#" + fa).length) {
                    W[0].id = fa;
                    ua.content[0].id = fa + "-content";
                    ua.title[0].id = fa + "-title"
                }
            },
            "^content.text$": function (J, fa, ka) {
                la(ka)
            },
            "^content.title.text$": function (J, fa, ka) {
                if (!ka) return Y();
                !ua.title && ka && S();
                ga(ka)
            },
            "^content.title.button$": function (J, fa, ka) {
                J = ua.button;
                fa = ua.title;
                if (G.rendered) if (ka) {
                    fa || S();
                    L()
                } else J.remove()
            },
            "^position.(my|at)$": function (J, fa, ka) {
                if ("string" === typeof ka) J[fa] = new E.Corner(ka)
            },
            "^position.container$": function (J, fa, ka) {
                G.rendered && W.appendTo(ka)
            },
            "^show.ready$": function () {
                G.rendered ? G.toggle(p) : G.render(1)
            },
            "^style.classes$": function (J, fa, ka) {
                W.attr("class", oa + " qtip ui-helper-reset " + ka)
            },
            "^style.widget|content.title": O,
            "^events.(render|show|move|hide|focus|blur)$": function (J, fa, ka) {
                W[(a.isFunction(ka) ? "" : "un") + "bind"]("tooltip" + fa, ka)
            },
            "^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)": function () {
                var J = d.position;
                W.attr("tracking", J.target === "mouse" && J.adjust.mouse);
                Na();
                Ga()
            }
        };
        a.extend(G, {
            render: function (J) {
                if (G.rendered) return G;
                var fa = d.content.text,
                    ka = d.content.title.text,
                    Ha = d.position,
                    Ea = a.Event("tooltiprender");
                a.attr(y[0], "aria-describedby", sa);
                W = ua.tooltip = a("<div/>", {
                    id: sa,
                    "class": oa + " qtip ui-helper-reset " + Ma + " " + d.style.classes + " " + oa + "-pos-" + d.position.my.abbrev(),
                    width: d.style.width || "",
                    height: d.style.height || "",
                    tracking: Ha.target === "mouse" && Ha.adjust.mouse,
                    role: "alert",
                    "aria-live": "polite",
                    "aria-atomic": n,
                    "aria-describedby": sa + "-content",
                    "aria-hidden": p
                }).toggleClass(ya, va.disabled).data("qtip", G).appendTo(d.position.container).append(ua.content = a("<div />", {
                    "class": oa + "-content",
                    id: sa + "-content",
                    "aria-atomic": p
                }));
                G.rendered = -1;
                ta = Ba = 1;
                if (ka) {
                    S();
                    a.isFunction(ka) || ga(ka, n)
                }
                a.isFunction(fa) || la(fa, n);
                G.rendered = p;
                O();
                a.each(d.events, function (xa, Va) {
                    if (a.isFunction(Va)) W.bind(xa === "toggle" ? "tooltipshow tooltiphide" : "tooltip" + xa, Va)
                });
                a.each(E, function () {
                    this.initialize === "render" && this(G)
                });
                Ga();
                W.queue("fx", function (xa) {
                    Ea.originalEvent = va.event;
                    W.trigger(Ea, [G]);
                    ta = Ba = 0;
                    G.redraw();
                    if (d.show.ready || J) G.toggle(p, va.event, n);
                    xa()
                });
                return G
            },
            get: function (J) {
                switch (J.toLowerCase()) {
                case "dimensions":
                    J = {
                        height: W.outerHeight(),
                        width: W.outerWidth()
                    };
                    break;
                case "offset":
                    J = E.offset(W, d.position.container);
                    break;
                default:
                    J = H(J.toLowerCase());
                    J = J[0][J[1]];
                    J = J.precedance ? J.string() : J
                }
                return J
            },
            set: function (J, fa) {
                var ka = /^position\.(my|at|adjust|target|container)|style|content|show\.ready/i,
                    Ha = /^content\.(title|attr)|style/i,
                    Ea = n,
                    xa = n,
                    Va = G.checks,
                    Xa;
                if ("string" === typeof J) {
                    Xa = J;
                    J = {};
                    J[Xa] = fa
                } else J = a.extend(p, {}, J);
                a.each(J, function (wa, Aa) {
                    var Ja = H(wa.toLowerCase()),
                        Ra;
                    Ra = Ja[0][Ja[1]];
                    Ja[0][Ja[1]] = "object" === typeof Aa && Aa.nodeType ? a(Aa) : Aa;
                    J[wa] = [Ja[0], Ja[1], Aa, Ra];
                    Ea = ka.test(wa) || Ea;
                    xa = Ha.test(wa) || xa
                });
                h(d);
                ta = Ba = 1;
                a.each(J, function (wa, Aa) {
                    var Ja, Ra, fb;
                    for (Ja in Va) for (Ra in Va[Ja]) if (fb = RegExp(Ra, "i").exec(wa)) {
                        Aa.push(fb);
                        Va[Ja][Ra].apply(G, Aa)
                    }
                });
                ta = Ba = 0;
                if (W.is(":visible") && G.rendered) {
                    if (Ea) G.reposition(d.position.target === "mouse" ? u : va.event);
                    xa && G.redraw()
                }
                return G
            },
            toggle: function (J, fa, ka) {
                function Ha() {
                    if (J) {
                        a.browser.msie && W[0].style.removeAttribute("filter");
                        W.css("overflow", "");
                        "string" === typeof xa.autofocus && a(xa.autofocus, W).focus();
                        Ja = a.Event("tooltipvisible");
                        Ja.originalEvent = fa ? va.event : u;
                        W.trigger(Ja, [G]);
                        xa.target.trigger("qtip-" + Q + "-inactive")
                    } else W.css({
                        display: "",
                        visibility: "",
                        opacity: "",
                        left: "",
                        top: ""
                    })
                }
                if (!G.rendered) return J ? G.render(1) : G;
                var Ea = J ? "show" : "hide",
                    xa = d[Ea],
                    Va = W.is(":visible"),
                    Xa = !fa || d[Ea].target.length < 2 || va.target[0] === fa.target,
                    wa = d.position,
                    Aa = d.content,
                    Ja;
                if ((typeof J).search("boolean|number")) J = !Va;
                if (!W.is(":animated") && Va === J && Xa) return G;
                if (fa) {
                    if (/over|enter/.test(fa.type) && /out|leave/.test(va.event.type) && fa.target === d.show.target[0] && W.has(fa.relatedTarget).length) return G;
                    va.event = a.extend({}, fa)
                }
                Ja = a.Event("tooltip" + Ea);
                Ja.originalEvent = fa ? va.event : u;
                W.trigger(Ja, [G, 90]);
                if (Ja.isDefaultPrevented()) return G;
                a.attr(W[0], "aria-hidden", !J);
                if (J) {
                    va.origin = a.extend({}, M);
                    G.focus(fa);
                    a.isFunction(Aa.text) && la(Aa.text, n);
                    a.isFunction(Aa.title.text) && ga(Aa.title.text, n);
                    if (!K && wa.target === "mouse" && wa.adjust.mouse) {
                        a(document).bind("mousemove.qtip", function (Ra) {
                            M = {
                                pageX: Ra.pageX,
                                pageY: Ra.pageY,
                                type: "mousemove"
                            }
                        });
                        K = p
                    }
                    G.reposition(fa, ka);
                    if (Ja.solo = !! xa.solo) a(pa, xa.solo).not(W).qtip("hide", Ja)
                } else {
                    clearTimeout(G.timers.show);
                    delete va.origin;
                    if (K && !a(pa + '[tracking="true"]:visible', xa.solo).not(W).length) {
                        a(document).unbind("mousemove.qtip");
                        K = n
                    }
                    G.blur(fa)
                }
                Xa && W.stop(0, 1);
                if (xa.effect === n) {
                    W[Ea]();
                    Ha.call(W)
                } else if (a.isFunction(xa.effect)) {
                    xa.effect.call(W, G);
                    W.queue("fx", function (Ra) {
                        Ha();
                        Ra()
                    })
                } else W.fadeTo(90, J ? 1 : 0, Ha);
                J && xa.target.trigger("qtip-" + Q + "-inactive");
                return G
            },
            show: function (J) {
                return G.toggle(p, J)
            },
            hide: function (J) {
                return G.toggle(n, J)
            },
            focus: function (J) {
                if (!G.rendered) return G;
                var fa = a(pa),
                    ka = parseInt(W[0].style.zIndex, 10),
                    Ha = z.zindex + fa.length;
                J = a.extend({}, J);
                var Ea;
                if (!W.hasClass(Z)) {
                    Ea = a.Event("tooltipfocus");
                    Ea.originalEvent = J;
                    W.trigger(Ea, [G, Ha]);
                    if (!Ea.isDefaultPrevented()) {
                        if (ka !== Ha) {
                            fa.each(function () {
                                if (this.style.zIndex > ka) this.style.zIndex -= 1
                            });
                            fa.filter("." + Z).qtip("blur", J)
                        }
                        W.addClass(Z)[0].style.zIndex = Ha
                    }
                }
                return G
            },
            blur: function (J) {
                J = a.extend({}, J);
                var fa;
                W.removeClass(Z);
                fa = a.Event("tooltipblur");
                fa.originalEvent = J;
                W.trigger(fa, [G]);
                return G
            },
            reposition: function (J, fa) {
                if (!G.rendered || ta) return G;
                ta = 1;
                var ka = d.position.target,
                    Ha = d.position,
                    Ea = Ha.my,
                    xa = Ha.at,
                    Va = Ha.adjust,
                    Xa = Va.method.split(" "),
                    wa = W.outerWidth(),
                    Aa = W.outerHeight(),
                    Ja = 0,
                    Ra = 0,
                    fb = a.Event("tooltipmove"),
                    rb = W.css("position") === "fixed",
                    Ua = Ha.viewport,
                    Ca = {
                        left: 0,
                        top: 0
                    },
                    cb = Ha.container,
                    sb = n,
                    Wa = G.plugins.tip,
                    kb = {
                        horizontal: Xa[0],
                        vertical: Xa[1] = Xa[1] || Xa[0],
                        enabled: Ua.jquery && ka[0] !== f && ka[0] !== da && Va.method !== "none",
                        left: function (Za) {
                            var nb = kb.horizontal === "shift",
                                bb = -cb.offset.left + Ua.offset.left + Ua.scrollLeft,
                                tb = Ea.x === "left" ? wa : Ea.x === "right" ? -wa : -wa / 2,
                                Sa = xa.x === "left" ? Ja : xa.x === "right" ? -Ja : -Ja / 2,
                                lb = Wa && Wa.size ? Wa.size.width || 0 : 0,
                                Ya = Wa && Wa.corner && Wa.corner.precedance === "x" && !nb ? lb : 0,
                                gb = bb - Za + Ya,
                                ub = Za + wa - Ua.width - bb + Ya;
                            Sa = tb - (Ea.precedance === "x" || Ea.x === Ea.y ? Sa : 0) - (xa.x === "center" ? Ja / 2 : 0);
                            Ya = Ea.x === "center";
                            if (nb) {
                                Ya = Wa && Wa.corner && Wa.corner.precedance === "y" ? lb : 0;
                                Sa = (Ea.x === "left" ? 1 : -1) * tb - Ya;
                                Ca.left += gb > 0 ? gb : ub > 0 ? -ub : 0;
                                Ca.left = Math.max(-cb.offset.left + Ua.offset.left + (Ya && Wa.corner.x === "center" ? Wa.offset : 0), Za - Sa, Math.min(Math.max(-cb.offset.left + Ua.offset.left + Ua.width, Za + Sa), Ca.left))
                            } else {
                                if (gb > 0 && (Ea.x !== "left" || ub > 0)) Ca.left -= Sa;
                                else if (ub > 0 && (Ea.x !== "right" || gb > 0)) Ca.left -= Ya ? -Sa : Sa;
                                if (Ca.left !== Za && Ya) Ca.left -= Va.x;
                                if (Ca.left < bb && -Ca.left > ub) Ca.left = Za
                            }
                            return Ca.left - Za
                        },
                        top: function (Za) {
                            var nb = kb.vertical === "shift",
                                bb = -cb.offset.top + Ua.offset.top + Ua.scrollTop,
                                tb = Ea.y === "top" ? Aa : Ea.y === "bottom" ? -Aa : -Aa / 2,
                                Sa = xa.y === "top" ? Ra : xa.y === "bottom" ? -Ra : -Ra / 2,
                                lb = Wa && Wa.size ? Wa.size.height || 0 : 0,
                                Ya = Wa && Wa.corner && Wa.corner.precedance === "y" && !nb ? lb : 0,
                                gb = bb - Za + Ya;
                            bb = Za + Aa - Ua.height - bb + Ya;
                            Sa = tb - (Ea.precedance === "y" || Ea.x === Ea.y ? Sa : 0) - (xa.y === "center" ? Ra / 2 : 0);
                            Ya = Ea.y === "center";
                            if (nb) {
                                Ya = Wa && Wa.corner && Wa.corner.precedance === "x" ? lb : 0;
                                Sa = (Ea.y === "top" ? 1 : -1) * tb - Ya;
                                Ca.top += gb > 0 ? gb : bb > 0 ? -bb : 0;
                                Ca.top = Math.max(-cb.offset.top + Ua.offset.top + (Ya && Wa.corner.x === "center" ? Wa.offset : 0), Za - Sa, Math.min(Math.max(-cb.offset.top + Ua.offset.top + Ua.height, Za + Sa), Ca.top))
                            } else {
                                if (gb > 0 && (Ea.y !== "top" || bb > 0)) Ca.top -= Sa;
                                else if (bb > 0 && (Ea.y !== "bottom" || gb > 0)) Ca.top -= Ya ? -Sa : Sa;
                                if (Ca.top !== Za && Ya) Ca.top -= Va.y;
                                if (Ca.top < 0 && -Ca.top > bb) Ca.top = Za
                            }
                            return Ca.top - Za
                        }
                    };
                if (a.isArray(ka) && ka.length === 2) {
                    xa = {
                        x: "left",
                        y: "top"
                    };
                    Ca = {
                        left: ka[0],
                        top: ka[1]
                    }
                } else if (ka === "mouse" && (J && J.pageX || va.event.pageX)) {
                    xa = {
                        x: "left",
                        y: "top"
                    };
                    J = (J && (J.type === "resize" || J.type === "scroll") ? va.event : J && J.pageX && J.type === "mousemove" ? J : M && M.pageX && (Va.mouse || !J || !J.pageX) ? {
                        pageX: M.pageX,
                        pageY: M.pageY
                    } : !Va.mouse && va.origin && va.origin.pageX && d.show.distance ? va.origin : J) || J || va.event || M || {};
                    Ca = {
                        top: J.pageY,
                        left: J.pageX
                    }
                } else {
                    ka = ka === "event" ? J && J.target && J.type !== "scroll" && J.type !== "resize" ? va.target = a(J.target) : va.target : va.target = a(ka.jquery ? ka : ua.target);
                    ka = a(ka).eq(0);
                    if (ka.length === 0) return G;
                    else if (ka[0] === document || ka[0] === f) {
                        Ja = E.iOS ? f.innerWidth : ka.width();
                        Ra = E.iOS ? f.innerHeight : ka.height();
                        if (ka[0] === f) Ca = {
                            top: (Ua || ka).scrollTop(),
                            left: (Ua || ka).scrollLeft()
                        }
                    } else if (ka.is("area") && E.imagemap) Ca = E.imagemap(ka, xa, kb.enabled ? Xa : n);
                    else if (ka[0].namespaceURI === "http://www.w3.org/2000/svg" && E.svg) Ca = E.svg(ka, xa);
                    else {
                        Ja = ka.outerWidth();
                        Ra = ka.outerHeight();
                        Ca = E.offset(ka, cb)
                    }
                    if (Ca.offset) {
                        Ja = Ca.width;
                        Ra = Ca.height;
                        sb = Ca.flipoffset;
                        Ca = Ca.offset
                    }
                    if (E.iOS < 4.1 && E.iOS > 3.1 || E.iOS == 4.3 || !E.iOS && rb) {
                        Xa = a(f);
                        Ca.left -= Xa.scrollLeft();
                        Ca.top -= Xa.scrollTop()
                    }
                    Ca.left += xa.x === "right" ? Ja : xa.x === "center" ? Ja / 2 : 0;
                    Ca.top += xa.y === "bottom" ? Ra : xa.y === "center" ? Ra / 2 : 0
                }
                Ca.left += Va.x + (Ea.x === "right" ? -wa : Ea.x === "center" ? -wa / 2 : 0);
                Ca.top += Va.y + (Ea.y === "bottom" ? -Aa : Ea.y === "center" ? -Aa / 2 : 0);
                if (kb.enabled) {
                    Ua = {
                        elem: Ua,
                        height: Ua[(Ua[0] === f ? "h" : "outerH") + "eight"](),
                        width: Ua[(Ua[0] === f ? "w" : "outerW") + "idth"](),
                        scrollLeft: rb ? 0 : Ua.scrollLeft(),
                        scrollTop: rb ? 0 : Ua.scrollTop(),
                        offset: Ua.offset() || {
                            left: 0,
                            top: 0
                        }
                    };
                    cb = {
                        elem: cb,
                        scrollLeft: cb.scrollLeft(),
                        scrollTop: cb.scrollTop(),
                        offset: cb.offset() || {
                            left: 0,
                            top: 0
                        }
                    };
                    Ca.adjusted = {
                        left: kb.horizontal !== "none" ? kb.left(Ca.left) : 0,
                        top: kb.vertical !== "none" ? kb.top(Ca.top) : 0
                    };
                    Ca.adjusted.left + Ca.adjusted.top && W.attr("class", W[0].className.replace(/ui-tooltip-pos-\w+/i, oa + "-pos-" + Ea.abbrev()));
                    if (sb && Ca.adjusted.left) Ca.left += sb.left;
                    if (sb && Ca.adjusted.top) Ca.top += sb.top
                } else Ca.adjusted = {
                    left: 0,
                    top: 0
                };
                fb.originalEvent = a.extend({}, J);
                W.trigger(fb, [G, Ca, Ua.elem || Ua]);
                if (fb.isDefaultPrevented()) return G;
                delete Ca.adjusted;
                if (fa === n || isNaN(Ca.left) || isNaN(Ca.top) || ka === "mouse" || !a.isFunction(Ha.effect)) W.css(Ca);
                else if (a.isFunction(Ha.effect)) {
                    Ha.effect.call(W, G, a.extend({}, Ca));
                    W.queue(function (Za) {
                        a(this).css({
                            opacity: "",
                            height: ""
                        });
                        a.browser.msie && this.style.removeAttribute("filter");
                        Za()
                    })
                }
                ta = 0;
                return G
            },
            redraw: function () {
                if (G.rendered < 1 || Ba) return G;
                var J = d.position.container,
                    fa, ka, Ha;
                Ba = 1;
                d.style.height && W.css("height", d.style.height);
                if (d.style.width) W.css("width", d.style.width);
                else {
                    W.css("width", "").addClass(C);
                    fa = W.width() + 1;
                    ka = W.css("max-width") || "";
                    Ha = W.css("min-width") || "";
                    J = (ka + Ha).indexOf("%") > -1 ? J.width() / 100 : 0;
                    ka = (ka.indexOf("%") > -1 ? J : 1) * parseInt(ka, 10) || fa;
                    Ha = (Ha.indexOf("%") > -1 ? J : 1) * parseInt(Ha, 10) || 0;
                    fa = ka + Ha ? Math.min(Math.max(fa, Ha), ka) : fa;
                    W.css("width", Math.round(fa)).removeClass(C)
                }
                Ba = 0;
                return G
            },
            disable: function (J) {
                if ("boolean" !== typeof J) J = !(W.hasClass(ya) || va.disabled);
                if (G.rendered) {
                    W.toggleClass(ya, J);
                    a.attr(W[0], "aria-disabled", J)
                } else va.disabled = !! J;
                return G
            },
            enable: function () {
                return G.disable(n)
            },
            destroy: function () {
                var J = y[0],
                    fa = a.attr(J, x),
                    ka = y.data("qtip");
                if (G.rendered) {
                    W.remove();
                    a.each(G.plugins, function () {
                        this.destroy && this.destroy()
                    })
                }
                clearTimeout(G.timers.show);
                clearTimeout(G.timers.hide);
                Na();
                if (!ka || G === ka) {
                    a.removeData(J, "qtip");
                    if (d.suppress && fa) {
                        a.attr(J, "title", fa);
                        y.removeAttr(x)
                    }
                    y.removeAttr("aria-describedby")
                }
                y.unbind(".qtip-" + Q);
                delete ma[G.id];
                return y
            }
        })
    }
    function l(y, d) {
        var Q, N, H, O, Y, L = a(this),
            S = a(document.body),
            ga = this === document ? S : L;
        N = L.metadata ? L.metadata(d.metadata) : u;
        O = d.metadata.type === "html5" && N ? N[d.metadata.name] : u;
        var la = L.data(d.metadata.name || "qtipopts");
        try {
            la = typeof la === "string" ? (new Function("return " + la))() : la
        } catch (Ga) {
            g("Unable to parse HTML5 attribute data: " + la)
        }
        O = a.extend(p, {}, z.defaults, d, typeof la === "object" ? h(la) : u, h(O || N));
        N = O.position;
        O.id = y;
        if ("boolean" === typeof O.content.text) {
            H = L.attr(O.content.attr);
            if (O.content.attr !== n && H) O.content.text = H;
            else {
                g("Unable to locate content for tooltip! Aborting render of tooltip on element: ", L);
                return n
            }
        }
        if (!N.container.length) N.container = S;
        if (N.target === n) N.target = ga;
        if (O.show.target === n) O.show.target = ga;
        if (O.show.solo === p) O.show.solo = S;
        if (O.hide.target === n) O.hide.target = ga;
        if (O.position.viewport === p) O.position.viewport = N.container;
        N.at = new E.Corner(N.at);
        N.my = new E.Corner(N.my);
        if (a.data(this, "qtip")) if (O.overwrite) L.qtip("destroy");
        else if (O.overwrite === n) return n;
        if (O.suppress && (Y = a.attr(this, "title"))) a(this).removeAttr("title").attr(x, Y);
        Q = new j(L, O, y, !! H);
        a.data(this, "qtip", Q);
        L.bind("remove.qtip-" + y, function () {
            Q.destroy()
        });
        return Q
    }
    function c(y) {
        var d = this,
            Q = y.elements.tooltip,
            N = y.options.content.ajax,
            H = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
        y.checks.ajax = {
            "^content.ajax": function (O, Y, L) {
                if (Y === "ajax") N = L;
                if (Y === "once") d.init();
                else N && N.url ? d.load() : Q.unbind(".qtip-ajax")
            }
        };
        a.extend(d, {
            init: function () {
                if (N && N.url) Q.unbind(".qtip-ajax")[N.once ? "one" : "bind"]("tooltipshow.qtip-ajax", d.load);
                return d
            },
            load: function (O, Y) {
                var L = N.url.indexOf(" "),
                    S = N.url,
                    ga, la = N.once && !N.loading && Y;
                if (la) try {
                    O.preventDefault()
                } catch (Ga) {} else if (O && O.isDefaultPrevented()) return d;
                if (L > -1) {
                    ga = S.substr(L);
                    S = S.substr(0, L)
                }
                a.ajax(a.extend({
                    success: function (Na) {
                        if (ga) Na = a("<div/>").append(Na.replace(H, "")).find(ga);
                        y.set("content.text", Na)
                    },
                    error: function (Na, G, da) {
                        Na.status !== 0 && y.set("content.text", G + ": " + da)
                    },
                    context: y
                }, N, {
                    url: S,
                    complete: function () {
                        if (la) {
                            y.show(O.originalEvent);
                            Y = n
                        }
                        a.isFunction(N.complete) && N.complete.apply(this, arguments)
                    }
                }))
            }
        });
        d.init()
    }
    function k(y, d, Q) {
        var N = Math.ceil(d / 2),
            H = Math.ceil(Q / 2);
        d = {
            bottomright: [
                [0, 0],
                [d, Q],
                [d, 0]
            ],
            bottomleft: [
                [0, 0],
                [d, 0],
                [0, Q]
            ],
            topright: [
                [0, Q],
                [d, 0],
                [d, Q]
            ],
            topleft: [
                [0, 0],
                [0, Q],
                [d, Q]
            ],
            topcenter: [
                [0, Q],
                [N, 0],
                [d, Q]
            ],
            bottomcenter: [
                [0, 0],
                [d, 0],
                [N, Q]
            ],
            rightcenter: [
                [0, 0],
                [d, H],
                [0, Q]
            ],
            leftcenter: [
                [d, 0],
                [d, Q],
                [0, H]
            ]
        };
        d.lefttop = d.bottomright;
        d.righttop = d.bottomleft;
        d.leftbottom = d.topright;
        d.rightbottom = d.topleft;
        return d[y.string()]
    }
    function q(y) {
        function d(G, da, sa) {
            if (Y.tip) {
                G = H.corner.clone();
                da = sa.adjusted;
                var ta = y.options.position.adjust.method.split(" "),
                    Ba = ta[0];
                ta = ta[1] || ta[0];
                var W = {
                    left: n,
                    top: n,
                    x: 0,
                    y: 0
                },
                    na, ua = {},
                    va;
                if (H.corner.fixed !== p) {
                    if (Ba === "shift" && G.precedance === "x" && da.left && G.y !== "center") G.precedance = G.precedance === "x" ? "y" : "x";
                    else if (Ba === "flip" && da.left) G.x = G.x === "center" ? da.left > 0 ? "left" : "right" : G.x === "left" ? "right" : "left";
                    if (ta === "shift" && G.precedance === "y" && da.top && G.x !== "center") G.precedance = G.precedance === "y" ? "x" : "y";
                    else if (ta === "flip" && da.top) G.y = G.y === "center" ? da.top > 0 ? "top" : "bottom" : G.y === "top" ? "bottom" : "top";
                    if (G.string() !== S.corner.string() && (S.top !== da.top || S.left !== da.left)) H.update(G, n)
                }
                na = H.position(G, da);
                if (na.right !== i) na.left = -na.right;
                if (na.bottom !== i) na.top = -na.bottom;
                na.user = Math.max(0, O.offset);
                if (W.left = Ba === "shift" && !! da.left) if (G.x === "center") ua["margin-left"] = W.x = na["margin-left"] - da.left;
                else {
                    va = na.right !== i ? [da.left, -na.left] : [-da.left, na.left];
                    if ((W.x = Math.max(va[0], va[1])) > va[0]) {
                        sa.left -= da.left;
                        W.left = n
                    }
                    ua[na.right !== i ? "right" : "left"] = W.x
                }
                if (W.top = ta === "shift" && !! da.top) if (G.y === "center") ua["margin-top"] = W.y = na["margin-top"] - da.top;
                else {
                    va = na.bottom !== i ? [da.top, -na.top] : [-da.top, na.top];
                    if ((W.y = Math.max(va[0], va[1])) > va[0]) {
                        sa.top -= da.top;
                        W.top = n
                    }
                    ua[na.bottom !== i ? "bottom" : "top"] = W.y
                }
                Y.tip.css(ua).toggle(!(W.x && W.y || G.x === "center" && W.y || G.y === "center" && W.x));
                sa.left -= na.left.charAt ? na.user : Ba !== "shift" || W.top || !W.left && !W.top ? na.left : 0;
                sa.top -= na.top.charAt ? na.user : ta !== "shift" || W.left || !W.left && !W.top ? na.top : 0;
                S.left = da.left;
                S.top = da.top;
                S.corner = G.clone()
            }
        }
        function Q(G, da, sa) {
            da = !da ? G[G.precedance] : da;
            var ta = L.hasClass(C);
            G = Y.titlebar && G.y === "top" ? Y.titlebar : Y.content;
            da = "border-" + da + "-width";
            L.addClass(C);
            G = parseInt(G.css(da), 10);
            G = (sa ? G || parseInt(L.css(da), 10) : G) || 0;
            L.toggleClass(C, ta);
            return G
        }
        function N(G) {
            var da = G.precedance === "y",
                sa = ga[da ? "width" : "height"],
                ta = ga[da ? "height" : "width"],
                Ba = G.string().indexOf("center") > -1,
                W = sa * (Ba ? 0.5 : 1),
                na = Math.pow;
            G = Math.round;
            var ua = Math.sqrt(na(W, 2) + na(ta, 2));
            W = [Ga / W * ua, Ga / ta * ua];
            W[2] = Math.sqrt(na(W[0], 2) - na(Ga, 2));
            W[3] = Math.sqrt(na(W[1], 2) - na(Ga, 2));
            Ba = (ua + W[2] + W[3] + (Ba ? 0 : W[0])) / ua;
            sa = [G(Ba * ta), G(Ba * sa)];
            return {
                height: sa[da ? 0 : 1],
                width: sa[da ? 1 : 0]
            }
        }
        var H = this,
            O = y.options.style.tip,
            Y = y.elements,
            L = Y.tooltip,
            S = {
                top: 0,
                left: 0
            },
            ga = {
                width: O.width,
                height: O.height
            },
            la = {},
            Ga = O.border || 0,
            Na = !! (a("<canvas />")[0] || {}).getContext;
        H.corner = u;
        H.mimic = u;
        H.border = Ga;
        H.offset = O.offset;
        H.size = ga;
        y.checks.tip = {
            "^position.my|style.tip.(corner|mimic|border)$": function () {
                H.init() || H.destroy();
                y.reposition()
            },
            "^style.tip.(height|width)$": function () {
                ga = {
                    width: O.width,
                    height: O.height
                };
                H.create();
                H.update();
                y.reposition()
            },
            "^content.title.text|style.(classes|widget)$": function () {
                Y.tip && H.update()
            }
        };
        a.extend(H, {
            init: function () {
                var G = H.detectCorner() && (Na || a.browser.msie);
                if (G) {
                    H.create();
                    H.update();
                    L.unbind(".qtip-tip").bind("tooltipmove.qtip-tip", d)
                }
                return G
            },
            detectCorner: function () {
                var G = O.corner,
                    da = y.options.position,
                    sa = da.at;
                da = da.my.string ? da.my.string() : da.my;
                if (G === n || da === n && sa === n) return n;
                else if (G === p) H.corner = new E.Corner(da);
                else if (!G.string) {
                    H.corner = new E.Corner(G);
                    H.corner.fixed = p
                }
                S.corner = new E.Corner(H.corner.string());
                return H.corner.string() !== "centercenter"
            },
            detectColours: function (G) {
                var da, sa, ta = Y.tip.css("cssText", "");
                da = G || H.corner;
                var Ba = da[da.precedance];
                G = "border-" + Ba + "-color";
                sa = "border" + Ba.charAt(0) + Ba.substr(1) + "Color";
                Ba = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i;
                var W = a(document.body).css("color");
                y.elements.content.css("color");
                var na = Y.titlebar && (da.y === "top" || da.y === "center" && ta.position().top + ga.height / 2 + O.offset < Y.titlebar.outerHeight(1)) ? Y.titlebar : Y.content;
                L.addClass(C);
                la.fill = da = ta.css("background-color");
                la.border = sa = ta[0].style[sa] || ta.css(G) || L.css(G);
                if (!da || Ba.test(da)) {
                    la.fill = na.css("background-color") || "transparent";
                    if (Ba.test(la.fill)) la.fill = L.css("background-color") || da
                }
                if (!sa || Ba.test(sa) || sa === W) {
                    la.border = na.css(G) || "transparent";
                    if (Ba.test(la.border)) la.border = sa
                }
                a("*", ta).add(ta).css("cssText", "background-color:transparent !important;border:0 !important;");
                L.removeClass(C)
            },
            create: function () {
                var G = ga.width,
                    da = ga.height;
                Y.tip && Y.tip.remove();
                Y.tip = a("<div />", {
                    "class": "ui-tooltip-tip"
                }).css({
                    width: G,
                    height: da
                }).prependTo(L);
                if (Na) a("<canvas />").appendTo(Y.tip)[0].getContext("2d").save();
                else {
                    Y.tip.html('<vml:shape coordorigin="0,0" style="display:inline-block; position:absolute; behavior:url(#default#VML);"></vml:shape><vml:shape coordorigin="0,0" style="display:inline-block; position:absolute; behavior:url(#default#VML);"></vml:shape>');
                    a("*", Y.tip).bind("click mousedown", function (sa) {
                        sa.stopPropagation()
                    })
                }
            },
            update: function (G, da) {
                var sa = Y.tip,
                    ta = sa.children(),
                    Ba = ga.width,
                    W = ga.height,
                    na = O.mimic,
                    ua = Math.round,
                    va, J, fa;
                G || (G = S.corner || H.corner);
                if (na === n) na = G;
                else {
                    na = new E.Corner(na);
                    na.precedance = G.precedance;
                    if (na.x === "inherit") na.x = G.x;
                    else if (na.y === "inherit") na.y = G.y;
                    else if (na.x === na.y) na[G.precedance] = G[G.precedance]
                }
                va = na.precedance;
                H.detectColours(G);
                if (la.border !== "transparent" && la.border !== "#123456") {
                    Ga = Q(G, u, p);
                    if (O.border === 0 && Ga > 0) la.fill = la.border;
                    H.border = Ga = O.border !== p ? O.border : Ga
                } else H.border = Ga = 0;
                J = k(na, Ba, W);
                H.size = fa = N(G);
                sa.css(fa);
                sa = G.precedance === "y" ? [ua(na.x === "left" ? Ga : na.x === "right" ? fa.width - Ba - Ga : (fa.width - Ba) / 2), ua(na.y === "top" ? fa.height - W : 0)] : [ua(na.x === "left" ? fa.width - Ba : 0), ua(na.y === "top" ? Ga : na.y === "bottom" ? fa.height - W - Ga : (fa.height - W) / 2)];
                if (Na) {
                    ta.attr(fa);
                    ta = ta[0].getContext("2d");
                    ta.restore();
                    ta.save();
                    ta.clearRect(0, 0, 3E3, 3E3);
                    ta.translate(sa[0], sa[1]);
                    ta.beginPath();
                    ta.moveTo(J[0][0], J[0][1]);
                    ta.lineTo(J[1][0], J[1][1]);
                    ta.lineTo(J[2][0], J[2][1]);
                    ta.closePath();
                    ta.fillStyle = la.fill;
                    ta.strokeStyle = la.border;
                    ta.lineWidth = Ga * 2;
                    ta.lineJoin = "miter";
                    ta.miterLimit = 100;
                    Ga && ta.stroke();
                    ta.fill()
                } else {
                    J = "m" + J[0][0] + "," + J[0][1] + " l" + J[1][0] + "," + J[1][1] + " " + J[2][0] + "," + J[2][1] + " xe";
                    sa[2] = Ga && /^(r|b)/i.test(G.string()) ? parseFloat(a.browser.version, 10) === 8 ? 2 : 1 : 0;
                    ta.css({
                        antialias: "" + (na.string().indexOf("center") > -1),
                        left: sa[0] - sa[2] * Number(va === "x"),
                        top: sa[1] - sa[2] * Number(va === "y"),
                        width: Ba + Ga,
                        height: W + Ga
                    }).each(function (ka) {
                        var Ha = a(this);
                        Ha[Ha.prop ? "prop" : "attr"]({
                            coordsize: Ba + Ga + " " + (W + Ga),
                            path: J,
                            fillcolor: la.fill,
                            filled: !! ka,
                            stroked: !ka
                        }).css({
                            display: Ga || ka ? "block" : "none"
                        });
                        !ka && Ha.html() === "" && Ha.html('<vml:stroke weight="' + Ga * 2 + 'px" color="' + la.border + '" miterlimit="1000" joinstyle="miter"  style="behavior:url(#default#VML); display:inline-block;" />')
                    })
                }
                da !== n && H.position(G)
            },
            position: function (G) {
                var da = Y.tip,
                    sa = {},
                    ta = Math.max(0, O.offset),
                    Ba, W, na;
                if (O.corner === n || !da) return n;
                G = G || H.corner;
                Ba = G.precedance;
                W = N(G);
                na = [G.x, G.y];
                Ba === "x" && na.reverse();
                a.each(na, function (ua, va) {
                    var J, fa;
                    if (va === "center") {
                        J = Ba === "y" ? "left" : "top";
                        sa[J] = "50%";
                        sa["margin-" + J] = -Math.round(W[Ba === "y" ? "width" : "height"] / 2) + ta
                    } else {
                        J = Q(G, va, p);
                        fa = a.browser.mozilla;
                        var ka = G.y + (fa ? "" : "-") + G.x;
                        fa = (fa ? "-moz-" : a.browser.webkit ? "-webkit-" : "") + (fa ? "border-radius-" + ka : "border-" + ka + "-radius");
                        fa = parseInt((Y.titlebar && G.y === "top" ? Y.titlebar : Y.content).css(fa), 10) || parseInt(L.css(fa), 10) || 0;
                        sa[va] = ua ? Ga ? Q(G, va) : 0 : ta + (fa > J ? fa : 0)
                    }
                });
                sa[G[Ba]] -= W[Ba === "x" ? "width" : "height"];
                da.css({
                    top: "",
                    bottom: "",
                    left: "",
                    right: "",
                    margin: ""
                }).css(sa);
                return sa
            },
            destroy: function () {
                Y.tip && Y.tip.remove();
                L.unbind(".qtip-tip")
            }
        });
        H.init()
    }
    function s(y) {
        var d = this,
            Q = y.options.show.modal,
            N = y.elements,
            H = N.tooltip,
            O = ".qtipmodal" + y.id,
            Y = a(document.body),
            L;
        y.checks.modal = {
            "^show.modal.(on|blur)$": function () {
                d.init();
                N.overlay.toggle(H.is(":visible"))
            }
        };
        a.extend(d, {
            init: function () {
                if (!Q.on) return d;
                L = d.create();
                H.attr("is-modal-qtip", p).css("z-index", E.modal.zindex + a(pa + "[is-modal-qtip]").length).unbind(".qtipmodal").unbind(O).bind("tooltipshow.qtipmodal tooltiphide.qtipmodal", function (S, ga, la) {
                    ga = S.originalEvent;
                    if (S.target === H[0]) if (ga && S.type === "tooltiphide" && /mouse(leave|enter)/.test(ga.type) && a(ga.relatedTarget).closest(L[0]).length) try {
                        S.preventDefault()
                    } catch (Ga) {} else if (!ga || ga && !ga.solo) d[S.type.replace("tooltip", "")](S, la)
                }).bind("tooltipfocus.qtipmodal", function (S) {
                    if (!(S.isDefaultPrevented() || S.target !== H[0])) {
                        var ga = a(pa).filter("[is-modal-qtip]"),
                            la = E.modal.zindex + ga.length,
                            Ga = parseInt(H[0].style.zIndex, 10);
                        L[0].style.zIndex = la - 1;
                        ga.each(function () {
                            if (this.style.zIndex > Ga) this.style.zIndex -= 1
                        });
                        ga.end().filter("." + Z).qtip("blur", S.originalEvent);
                        H.addClass(Z)[0].style.zIndex = la;
                        try {
                            S.preventDefault()
                        } catch (Na) {}
                    }
                }).bind("tooltiphide.qtipmodal", function (S) {
                    S.target === H[0] && a("[is-modal-qtip]").filter(":visible").not(H).last().qtip("focus", S)
                });
                Q.escape && a(f).unbind(O).bind("keydown" + O, function (S) {
                    S.keyCode === 27 && H.hasClass(Z) && y.hide(S)
                });
                Q.blur && N.overlay.unbind(O).bind("click" + O, function (S) {
                    H.hasClass(Z) && y.hide(S)
                });
                return d
            },
            create: function () {
                function S() {
                    L.css({
                        height: a(f).height(),
                        width: a(f).width()
                    })
                }
                var ga = a("#qtip-overlay");
                if (ga.length) return N.overlay = ga.insertAfter(a(pa).last());
                L = N.overlay = a("<div />", {
                    id: "qtip-overlay",
                    html: "<div></div>",
                    mousedown: function () {
                        return n
                    }
                }).insertAfter(a(pa).last());
                a(f).unbind(".qtipmodal").bind("resize.qtipmodal", S);
                S();
                return L
            },
            toggle: function (S, ga, la) {
                if (S && S.isDefaultPrevented()) return d;
                S = Q.effect;
                var Ga = ga ? "show" : "hide",
                    Na = L.is(":visible"),
                    G = a("[is-modal-qtip]").filter(":visible").not(H);
                L || (L = d.create());
                if (L.is(":animated") && Na === ga || !ga && G.length) return d;
                if (ga) {
                    L.css({
                        left: 0,
                        top: 0
                    });
                    L.toggleClass("blurs", Q.blur);
                    Y.bind("focusin" + O, function (da) {
                        var sa = a(da.target).closest(".qtip");
                        !(sa.length < 1 ? n : parseInt(sa[0].style.zIndex, 10) > parseInt(H[0].style.zIndex, 10)) && a(da.target).closest(pa)[0] !== H[0] && H.find("input:visible").filter(":first").focus()
                    })
                } else Y.undelegate("*", "focusin" + O);
                L.stop(p, n);
                if (a.isFunction(S)) S.call(L, ga);
                else if (S === n) L[Ga]();
                else L.fadeTo(parseInt(la, 10) || 90, ga ? 1 : 0, function () {
                    ga || a(this).hide()
                });
                ga || L.queue(function (da) {
                    L.css({
                        left: "",
                        top: ""
                    });
                    da()
                });
                return d
            },
            show: function (S, ga) {
                return d.toggle(S, p, ga)
            },
            hide: function (S, ga) {
                return d.toggle(S, n, ga)
            },
            destroy: function () {
                var S = L;
                if (S) {
                    if (S = a("[is-modal-qtip]").not(H).length < 1) {
                        N.overlay.remove();
                        a(f).unbind(".qtipmodal")
                    } else N.overlay.unbind(".qtipmodal" + y.id);
                    Y.undelegate("*", "focusin" + O)
                }
                return H.removeAttr("is-modal-qtip").unbind(".qtipmodal")
            }
        });
        d.init()
    }
    var p = true,
        n = false,
        u = null,
        z, E, M, ma = {},
        oa = "ui-tooltip",
        Da = "ui-widget",
        ya = "ui-state-disabled",
        pa = "div.qtip." + oa,
        Ma = oa + "-default",
        Z = oa + "-focus",
        ja = oa + "-hover",
        C = oa + "-fluid",
        x = "oldtitle",
        K;
    z = a.fn.qtip = function (y, d, Q) {
        var N = ("" + y).toLowerCase(),
            H = u,
            O = a.makeArray(arguments).slice(1),
            Y = O[O.length - 1],
            L = this[0] ? a.data(this[0], "qtip") : u;
        if (!arguments.length && L || N === "api") return L;
        else if ("string" === typeof y) {
            this.each(function () {
                var S = a.data(this, "qtip");
                if (!S) return p;
                if (Y && Y.timeStamp) S.cache.event = Y;
                if ((N === "option" || N === "options") && d) if (a.isPlainObject(d) || Q !== i) S.set(d, Q);
                else {
                    H = S.get(d);
                    return n
                } else S[N] && S[N].apply(S[N], O)
            });
            return H !== u ? H : this
        } else if ("object" === typeof y || !arguments.length) {
            L = h(a.extend(p, {}, y));
            return z.bind.call(this, L, Y)
        }
    };
    z.bind = function (y, d) {
        return this.each(function (Q) {
            function N(ga) {
                function la() {
                    S.render(typeof ga === "object" || H.show.ready);
                    O.show.add(O.hide).unbind(L)
                }
                if (S.cache.disabled) return n;
                S.cache.event = a.extend({}, ga);
                S.cache.target = ga ? a(ga.target) : [i];
                if (H.show.delay > 0) {
                    clearTimeout(S.timers.show);
                    S.timers.show = setTimeout(la, H.show.delay);
                    Y.show !== Y.hide && O.hide.bind(Y.hide, function () {
                        clearTimeout(S.timers.show)
                    })
                } else la()
            }
            var H, O, Y, L, S;
            Q = a.isArray(y.id) ? y.id[Q] : y.id;
            Q = !Q || Q === n || Q.length < 1 || ma[Q] ? z.nextid++ : ma[Q] = Q;
            L = ".qtip-" + Q + "-create";
            S = l.call(this, Q, y);
            if (S === n) return p;
            H = S.options;
            a.each(E, function () {
                this.initialize === "initialize" && this(S)
            });
            O = {
                show: H.show.target,
                hide: H.hide.target
            };
            Y = {
                show: a.trim("" + H.show.event).replace(/ /g, L + " ") + L,
                hide: a.trim("" + H.hide.event).replace(/ /g, L + " ") + L
            };
            if (/mouse(over|enter)/i.test(Y.show) && !/mouse(out|leave)/i.test(Y.hide)) Y.hide += " mouseleave" + L;
            O.show.bind("mousemove" + L, function (ga) {
                M = {
                    pageX: ga.pageX,
                    pageY: ga.pageY,
                    type: "mousemove"
                };
                S.cache.onTarget = p
            });
            O.show.bind(Y.show, N);
            if (H.show.ready || H.prerender) N(d)
        })
    };
    E = z.plugins = {
        Corner: function (y) {
            y = ("" + y).replace(/([A-Z])/, " $1").replace(/middle/gi, "center").toLowerCase();
            this.x = (y.match(/left|right/i) || y.match(/center/) || ["inherit"])[0].toLowerCase();
            this.y = (y.match(/top|bottom|center/i) || ["inherit"])[0].toLowerCase();
            y = y.charAt(0);
            this.precedance = y === "t" || y === "b" ? "y" : "x";
            this.string = function () {
                return this.precedance === "y" ? this.y + this.x : this.x + this.y
            };
            this.abbrev = function () {
                var d = this.x.substr(0, 1),
                    Q = this.y.substr(0, 1);
                return d === Q ? d : d === "c" || d !== "c" && Q !== "c" ? Q + d : d + Q
            };
            this.clone = function () {
                return {
                    x: this.x,
                    y: this.y,
                    precedance: this.precedance,
                    string: this.string,
                    abbrev: this.abbrev,
                    clone: this.clone
                }
            }
        },
        offset: function (y, d) {
            function Q(ga, la) {
                N.left += la * ga.scrollLeft();
                N.top += la * ga.scrollTop()
            }
            var N = y.offset(),
                H = document.body,
                O = d,
                Y, L, S;
            if (O) {
                do {
                    if (O.css("position") !== "static") {
                        L = O.position();
                        N.left -= L.left + (parseInt(O.css("borderLeftWidth"), 10) || 0) + (parseInt(O.css("marginLeft"), 10) || 0);
                        N.top -= L.top + (parseInt(O.css("borderTopWidth"), 10) || 0) + (parseInt(O.css("marginTop"), 10) || 0);
                        if (!Y && (S = O.css("overflow")) !== "hidden" && S !== "visible") Y = O
                    }
                    if (O[0] === H) break
                } while (O = O.offsetParent());
                Y && Y[0] !== H && Q(Y, 1)
            }
            return N
        },
        iOS: parseFloat(("" + (/CPU.*OS ([0-9_]{1,3})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ""])[1]).replace("undefined", "3_2").replace("_", ".")) || n,
        fn: {
            attr: function (y, d) {
                if (this.length) {
                    var Q = this[0],
                        N = a.data(Q, "qtip");
                    if (y === "title" && N && "object" === typeof N && N.options.suppress) if (arguments.length < 2) return a.attr(Q, x);
                    else {
                        N && N.options.content.attr === "title" && N.cache.attr && N.set("content.text", d);
                        return this.attr(x, d)
                    }
                }
                return a.fn.attr_replacedByqTip.apply(this, arguments)
            },
            clone: function (y) {
                a([]);
                var d = a.fn.clone_replacedByqTip.apply(this, arguments);
                y || d.filter("[" + x + "]").attr("title", function () {
                    return a.attr(this, x)
                }).removeAttr(x);
                return d
            },
            remove: a.ui ? u : function (y, d) {
                a.ui || a(this).each(function () {
                    if (!d) if (!y || a.filter(y, [this]).length) a("*", this).add(this).each(function () {
                        a(this).triggerHandler("remove")
                    })
                })
            }
        }
    };
    a.each(E.fn, function (y, d) {
        if (!d || a.fn[y + "_replacedByqTip"]) return p;
        var Q = a.fn[y + "_replacedByqTip"] = a.fn[y];
        a.fn[y] = function () {
            return d.apply(this, arguments) || Q.apply(this, arguments)
        }
    });
    z.version = "nightly";
    z.nextid = 0;
    z.inactiveEvents = "click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" ");
    z.zindex = 15E3;
    z.defaults = {
        prerender: n,
        id: n,
        overwrite: p,
        suppress: p,
        content: {
            text: p,
            attr: "title",
            title: {
                text: n,
                button: n
            }
        },
        position: {
            my: "top left",
            at: "bottom right",
            target: n,
            container: n,
            viewport: n,
            adjust: {
                x: 0,
                y: 0,
                mouse: p,
                resize: p,
                method: "flip flip"
            },
            effect: function (y, d) {
                a(this).animate(d, {
                    duration: 200,
                    queue: n
                })
            }
        },
        show: {
            target: n,
            event: "mouseenter",
            effect: p,
            delay: 90,
            solo: n,
            ready: n,
            autofocus: n
        },
        hide: {
            target: n,
            event: "mouseleave",
            effect: p,
            delay: 0,
            fixed: n,
            inactive: n,
            leave: "window",
            distance: n
        },
        style: {
            classes: "",
            widget: n,
            width: n,
            height: n,
            "default": p
        },
        events: {
            render: u,
            move: u,
            show: u,
            hide: u,
            toggle: u,
            visible: u,
            focus: u,
            blur: u
        }
    };
    E.ajax = function (y) {
        var d = y.plugins.ajax;
        return "object" === typeof d ? d : y.plugins.ajax = new c(y)
    };
    E.ajax.initialize = "render";
    E.ajax.sanitize = function (y) {
        var d = y.content;
        if (d && "ajax" in d) {
            d = d.ajax;
            if (typeof d !== "object") d = y.content.ajax = {
                url: d
            };
            if ("boolean" !== typeof d.once && d.once) d.once = !! d.once
        }
    };
    a.extend(p, z.defaults, {
        content: {
            ajax: {
                loading: p,
                once: p
            }
        }
    });
    E.tip = function (y) {
        var d = y.plugins.tip;
        return "object" === typeof d ? d : y.plugins.tip = new q(y)
    };
    E.tip.initialize = "render";
    E.tip.sanitize = function (y) {
        var d = y.style;
        if (d && "tip" in d) {
            d = y.style.tip;
            if (typeof d !== "object") y.style.tip = {
                corner: d
            };
            if (!/string|boolean/i.test(typeof d.corner)) d.corner = p;
            typeof d.width !== "number" && delete d.width;
            typeof d.height !== "number" && delete d.height;
            typeof d.border !== "number" && d.border !== p && delete d.border;
            typeof d.offset !== "number" && delete d.offset
        }
    };
    a.extend(p, z.defaults, {
        style: {
            tip: {
                corner: p,
                mimic: n,
                width: 6,
                height: 6,
                border: p,
                offset: 0
            }
        }
    });
    E.modal = function (y) {
        var d = y.plugins.modal;
        return "object" === typeof d ? d : y.plugins.modal = new s(y)
    };
    E.modal.initialize = "render";
    E.modal.sanitize = function (y) {
        if (y.show) if (typeof y.show.modal !== "object") y.show.modal = {
            on: !! y.show.modal
        };
        else if (typeof y.show.modal.on === "undefined") y.show.modal.on = p
    };
    E.modal.zindex = z.zindex + 1E3;
    a.extend(p, z.defaults, {
        show: {
            modal: {
                on: n,
                effect: p,
                blur: p,
                escape: p
            }
        }
    })
})(jQuery, window);
(function (a) {
    function f(Z, ja, C, x) {
        x = {
            data: x || x === 0 || x === false ? x : ja ? ja.data : {},
            _wrap: ja ? ja._wrap : null,
            tmpl: null,
            parent: ja || null,
            nodes: [],
            calls: k,
            nest: q,
            wrap: s,
            html: p,
            update: n
        };
        Z && a.extend(x, Z, {
            nodes: [],
            parent: ja
        });
        if (C) {
            x.tmpl = C;
            x._ctnt = x._ctnt || x.tmpl(a, x);
            x.key = ++ya;
            (Ma.length ? ma : M)[ya] = x
        }
        return x
    }
    function i(Z, ja, C) {
        var x;
        C = C ? a.map(C, function (K) {
            return typeof K === "string" ? Z.key ? K.replace(/(<\w+)(?=[\s>])(?![^>]*_tmplitem)([^>]*)/g, "$1 " + z + '="' + Z.key + '" $2') : K : i(K, Z, K._ctnt)
        }) : Z;
        if (ja) return C;
        C = C.join("");
        C.replace(/^\s*([^<\s][^<]*)?(<[\w\W]+>)([^>]*[^>\s])?\s*$/, function (K, y, d, Q) {
            x = a(d).get();
            c(x);
            if (y) x = g(y).concat(x);
            if (Q) x = x.concat(g(Q))
        });
        return x ? x : g(C)
    }
    function g(Z) {
        var ja = document.createElement("div");
        ja.innerHTML = Z;
        return a.makeArray(ja.childNodes)
    }
    function h(Z) {
        return new Function("jQuery", "$item", "var $=jQuery,call,__=[],$data=$item.data;with($data){__.push('" + a.trim(Z).replace(/([\\'])/g, "\\$1").replace(/[\r\t\n]/g, " ").replace(/\$\{([^\}]*)\}/g, "{{= $1}}").replace(/\{\{(\/?)(\w+|.)(?:\(((?:[^\}]|\}(?!\}))*?)?\))?(?:\s+(.*?)?)?(\(((?:[^\}]|\}(?!\}))*?)\))?\s*\}\}/g, function (ja, C, x, K, y, d, Q) {
            ja = a.tmpl.tag[x];
            if (!ja) throw "Unknown template tag: " + x;
            x = ja._default || [];
            if (d && !/\w$/.test(y)) {
                y += d;
                d = ""
            }
            if (y) {
                y = l(y);
                Q = Q ? "," + l(Q) + ")" : d ? ")" : "";
                Q = d ? y.indexOf(".") > -1 ? y + l(d) : "(" + y + ").call($item" + Q : y;
                d = d ? Q : "(typeof(" + y + ")==='function'?(" + y + ").call($item):(" + y + "))"
            } else d = Q = x.$1 || "null";
            K = l(K);
            return "');" + ja[C ? "close" : "open"].split("$notnull_1").join(y ? "typeof(" + y + ")!=='undefined' && (" + y + ")!=null" : "true").split("$1a").join(d).split("$1").join(Q).split("$2").join(K || x.$2 || "") + "__.push('"
        }) + "');}return __;")
    }
    function j(Z, ja) {
        Z._wrap = i(Z, true, a.isArray(ja) ? ja : [E.test(ja) ? ja : a(ja).html()]).join("")
    }
    function l(Z) {
        return Z ? Z.replace(/\\'/g, "'").replace(/\\\\/g, "\\") : null
    }
    function c(Z) {
        function ja(H) {
            function O(la) {
                la += C;
                S = y[la] = y[la] || f(S, M[S.parent.key + C] || S.parent)
            }
            var Y, L = H,
                S, ga;
            if (ga = H.getAttribute(z)) {
                for (; L.parentNode && (L = L.parentNode).nodeType === 1 && !(Y = L.getAttribute(z)););
                if (Y !== ga) {
                    L = L.parentNode ? L.nodeType === 11 ? 0 : L.getAttribute(z) || 0 : 0;
                    if (!(S = M[ga])) {
                        S = ma[ga];
                        S = f(S, M[L] || ma[L]);
                        S.key = ++ya;
                        M[ya] = S
                    }
                    pa && O(ga)
                }
                H.removeAttribute(z)
            } else if (pa && (S = a.data(H, "tmplItem"))) {
                O(S.key);
                M[S.key] = S;
                L = (L = a.data(H.parentNode, "tmplItem")) ? L.key : 0
            }
            if (S) {
                for (Y = S; Y && Y.key != L;) {
                    Y.nodes.push(H);
                    Y = Y.parent
                }
                delete S._ctnt;
                delete S._wrap;
                a.data(H, "tmplItem", S)
            }
        }
        var C = "_" + pa,
            x, K, y = {},
            d, Q, N;
        d = 0;
        for (Q = Z.length; d < Q; d++) if ((x = Z[d]).nodeType === 1) {
            K = x.getElementsByTagName("*");
            for (N = K.length - 1; N >= 0; N--) ja(K[N]);
            ja(x)
        }
    }
    function k(Z, ja, C, x) {
        if (!Z) return Ma.pop();
        Ma.push({
            _: Z,
            tmpl: ja,
            item: this,
            data: C,
            options: x
        })
    }
    function q(Z, ja, C) {
        return a.tmpl(a.template(Z), ja, C, this)
    }
    function s(Z, ja) {
        var C = Z.options || {};
        C.wrapped = ja;
        return a.tmpl(a.template(Z.tmpl), Z.data, C, Z.item)
    }
    function p(Z, ja) {
        var C = this._wrap;
        return a.map(a(a.isArray(C) ? C.join("") : C).filter(Z || "*"), function (x) {
            if (ja) x = x.innerText || x.textContent;
            else {
                var K;
                if (!(K = x.outerHTML)) {
                    K = document.createElement("div");
                    K.appendChild(x.cloneNode(true));
                    K = K.innerHTML
                }
                x = K
            }
            return x
        })
    }
    function n() {
        var Z = this.nodes;
        a.tmpl(null, null, null, this).insertBefore(Z[0]);
        a(Z).remove()
    }
    var u = a.fn.domManip,
        z = "_tmplitem",
        E = /^[^<]*(<[\w\W]+>)[^>]*$|\{\{\! /,
        M = {},
        ma = {},
        oa, Da = {
            key: 0,
            data: {}
        },
        ya = 0,
        pa = 0,
        Ma = [];
    a.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (Z, ja) {
        a.fn[Z] = function (C) {
            var x = [];
            C = a(C);
            var K, y, d;
            K = this.length === 1 && this[0].parentNode;
            oa = M || {};
            if (K && K.nodeType === 11 && K.childNodes.length === 1 && C.length === 1) {
                C[ja](this[0]);
                x = this
            } else {
                y = 0;
                for (d = C.length; y < d; y++) {
                    pa = y;
                    K = (y > 0 ? this.clone(true) : this).get();
                    a(C[y])[ja](K);
                    x = x.concat(K)
                }
                pa = 0;
                x = this.pushStack(x, Z, C.selector)
            }
            C = oa;
            oa = null;
            a.tmpl.complete(C);
            return x
        }
    });
    a.fn.extend({
        tmpl: function (Z, ja, C) {
            return a.tmpl(this[0], Z, ja, C)
        },
        tmplItem: function () {
            return a.tmplItem(this[0])
        },
        template: function (Z) {
            return a.template(Z, this[0])
        },
        domManip: function (Z, ja, C) {
            if (Z[0] && a.isArray(Z[0])) {
                for (var x = a.makeArray(arguments), K = Z[0], y = K.length, d = 0, Q; d < y && !(Q = a.data(K[d++], "tmplItem")););
                if (Q && pa) x[2] = function (N) {
                    a.tmpl.afterManip(this, N, C)
                };
                u.apply(this, x)
            } else u.apply(this, arguments);
            pa = 0;
            oa || a.tmpl.complete(M);
            return this
        }
    });
    a.extend({
        tmpl: function (Z, ja, C, x) {
            var K = !x;
            if (K) {
                x = Da;
                Z = a.template[Z] || a.template(null, Z);
                ma = {}
            } else if (!Z) {
                Z = x.tmpl;
                M[x.key] = x;
                x.nodes = [];
                x.wrapped && j(x, x.wrapped);
                return a(i(x, null, x.tmpl(a, x)))
            }
            if (!Z) return [];
            if (typeof ja === "function") ja = ja.call(x || {});
            C && C.wrapped && j(C, C.wrapped);
            ja = a.isArray(ja) ? a.map(ja, function (y) {
                return y ? f(C, x, Z, y) : null
            }) : [f(C, x, Z, ja)];
            return K ? a(i(x, null, ja)) : ja
        },
        tmplItem: function (Z) {
            var ja;
            if (Z instanceof
            a) Z = Z[0];
            for (; Z && Z.nodeType === 1 && !(ja = a.data(Z, "tmplItem")) && (Z = Z.parentNode););
            return ja || Da
        },
        template: function (Z, ja) {
            if (ja) {
                if (typeof ja === "string") ja = h(ja);
                else if (ja instanceof a) ja = ja[0] || {};
                if (ja.nodeType) ja = a.data(ja, "tmpl") || a.data(ja, "tmpl", h(ja.innerHTML));
                return typeof Z === "string" ? a.template[Z] = ja : ja
            }
            return Z ? typeof Z !== "string" ? a.template(null, Z) : a.template[Z] || a.template(null, E.test(Z) ? Z : a(Z)) : null
        },
        encode: function (Z) {
            return ("" + Z).split("<").join("&lt;").split(">").join("&gt;").split('"').join("&#34;").split("'").join("&#39;")
        }
    });
    a.extend(a.tmpl, {
        tag: {
            tmpl: {
                _default: {
                    $2: "null"
                },
                open: "if($notnull_1){__=__.concat($item.nest($1,$2));}"
            },
            wrap: {
                _default: {
                    $2: "null"
                },
                open: "$item.calls(__,$1,$2);__=[];",
                close: "call=$item.calls();__=call._.concat($item.wrap(call,__));"
            },
            each: {
                _default: {
                    $2: "$index, $value"
                },
                open: "if($notnull_1){$.each($1a,function($2){with(this){",
                close: "}});}"
            },
            "if": {
                open: "if(($notnull_1) && $1a){",
                close: "}"
            },
            "else": {
                _default: {
                    $1: "true"
                },
                open: "}else if(($notnull_1) && $1a){"
            },
            html: {
                open: "if($notnull_1){__.push($1a);}"
            },
            "=": {
                _default: {
                    $1: "$data"
                },
                open: "if($notnull_1){__.push($.encode($1a));}"
            },
            "!": {
                open: ""
            }
        },
        complete: function () {
            M = {}
        },
        afterManip: function (Z, ja, C) {
            var x = ja.nodeType === 11 ? a.makeArray(ja.childNodes) : ja.nodeType === 1 ? [ja] : [];
            C.call(Z, ja);
            c(x);
            pa++
        }
    })
})(jQuery);
(function (a) {
    function f() {
        if (j.jStorage) try {
            h = q(String(j.jStorage))
        } catch (n) {
            j.jStorage = "{}"
        } else j.jStorage = "{}";
        c = j.jStorage ? String(j.jStorage).length : 0
    }
    function i() {
        try {
            j.jStorage = k(h);
            if (l) {
                l.setAttribute("jStorage", j.jStorage);
                l.save("jStorage")
            }
            c = j.jStorage ? String(j.jStorage).length : 0
        } catch (n) {}
    }
    function g(n) {
        if (!n || typeof n != "string" && typeof n != "number") throw new TypeError("Key name must be string or numeric");
        return true
    }
    if (!a || !(a.toJSON || Object.toJSON || window.JSON)) throw Error("jQuery, MooTools or Prototype needs to be loaded before jStorage!");
    var h = {},
        j = {
            jStorage: "{}"
        },
        l = null,
        c = 0,
        k = a.toJSON || Object.toJSON || window.JSON && (JSON.encode || JSON.stringify),
        q = a.evalJSON || window.JSON && (JSON.decode || JSON.parse) ||
    function (n) {
        return String(n).evalJSON()
    }, s = false, p = {
        isXML: function (n) {
            return (n = (n ? n.ownerDocument || n : 0).documentElement) ? n.nodeName !== "HTML" : false
        },
        encode: function (n) {
            if (!this.isXML(n)) return false;
            try {
                return (new XMLSerializer).serializeToString(n)
            } catch (u) {
                try {
                    return n.xml
                } catch (z) {}
            }
            return false
        },
        decode: function (n) {
            var u = "DOMParser" in window && (new DOMParser).parseFromString || window.ActiveXObject &&
            function (z) {
                var E = new ActiveXObject("Microsoft.XMLDOM");
                E.async = "false";
                E.loadXML(z);
                return E
            };
            if (!u) return false;
            n = u.call("DOMParser" in window && new DOMParser || window, n, "text/xml");
            return this.isXML(n) ? n : false
        }
    };
    a.jStorage = {
        version: "0.1.5.3",
        set: function (n, u) {
            g(n);
            if (p.isXML(u)) u = {
                _is_xml: true,
                xml: p.encode(u)
            };
            h[n] = u;
            i();
            return u
        },
        get: function (n, u) {
            g(n);
            if (n in h) return h[n] && typeof h[n] == "object" && h[n]._is_xml && h[n]._is_xml ? p.decode(h[n].xml) : h[n];
            return typeof u == "undefined" ? null : u
        },
        deleteKey: function (n) {
            g(n);
            if (n in h) {
                delete h[n];
                i();
                return true
            }
            return false
        },
        flush: function () {
            h = {};
            i();
            return true
        },
        storageObj: function () {
            function n() {}
            n.prototype = h;
            return new n
        },
        index: function () {
            var n = [],
                u;
            for (u in h) h.hasOwnProperty(u) && n.push(u);
            return n
        },
        storageSize: function () {
            return c
        },
        currentBackend: function () {
            return s
        },
        storageAvailable: function () {
            return !!s
        },
        reInit: function () {
            var n;
            if (l && l.addBehavior) {
                n = document.createElement("link");
                l.parentNode.replaceChild(n, l);
                l = n;
                l.style.behavior = "url(#default#userData)";
                document.getElementsByTagName("head")[0].appendChild(l);
                l.load("jStorage");
                n = "{}";
                try {
                    n = l.getAttribute("jStorage")
                } catch (u) {}
                j.jStorage = n;
                s = "userDataBehavior"
            }
            f()
        }
    };
    (function () {
        var n = false;
        if ("localStorage" in window) try {
            window.localStorage.setItem("_tmptest", "tmpval");
            n = true;
            window.localStorage.removeItem("_tmptest")
        } catch (u) {}
        if (n) try {
            if (window.localStorage) {
                j = window.localStorage;
                s = "localStorage"
            }
        } catch (z) {} else if ("globalStorage" in window) try {
            if (window.globalStorage) {
                j = window.globalStorage[window.location.hostname];
                s = "globalStorage"
            }
        } catch (E) {} else {
            l = document.createElement("link");
            if (l.addBehavior) {
                l.style.behavior = "url(#default#userData)";
                document.getElementsByTagName("head")[0].appendChild(l);
                l.load("jStorage");
                n = "{}";
                try {
                    n = l.getAttribute("jStorage")
                } catch (M) {}
                j.jStorage = n;
                s = "userDataBehavior"
            } else {
                l = null;
                return
            }
        }
        f()
    })()
})(window.jQuery || window.$);
var WDS_JSLIBRARY_VERSION = "@@@LABEL@@@";
$.wdk = {};
var wdk = {
    validations: {},
    utils: {},
    annotations: {}
};
jQuery(document).ready(function () {
    wdk.initDynamicAnnotations();
    wdk.initStaticAnnotations();
    wdk.initValidationShortcuts()
});
(function (a, f, i) {
    function g(l, c) {
        var k = l.data(c);
        if (typeof k === "string") return a(k);
        return k
    }
    function h(l, c, k) {
        if (l.length != 0) if (a.isNumeric(c)) {
            c = +c;
            k = a.isNumeric(k) ? +k : c;
            return l.each(function () {
                if (this.setSelectionRange) this.setSelectionRange(c, k);
                else if (this.createTextRange) {
                    var q = this.createTextRange();
                    q.collapse(true);
                    q.moveEnd("character", k);
                    q.moveStart("character", c);
                    q.select()
                }
            })
        } else {
            if (l[0].setSelectionRange) {
                c = l[0].selectionStart;
                k = l[0].selectionEnd
            } else if (document.selection && document.selection.createRange) {
                l = document.selection.createRange();
                c = 0 - l.duplicate().moveStart("character", -1E5);
                k = c + l.text.length
            }
            return {
                begin: c,
                end: k
            }
        }
    }
    var j = wdk.annotations.autofocus = function (l) {
            l.off("focus.autofocus keydown.autofocus keypress.autofocus paste.autofocus").on({
                "keydown.autofocus": j.keydown,
                "keypress.autofocus": j.keypress,
                "paste.autofocus": j.paste
            }, "." + j.className)
        };
    j.className = "wdk-autofocus";
    j.live = true;
    j.maxlength = function (l) {
        var c = l.attr("maxlength");
        if (c !== i) {
            l.removeAttr("maxlength");
            l.data("wdkAutofocusMaxLength", +c)
        }
        return l.data("wdkAutofocusMaxLength")
    };
    j.prevField = function (l) {
        var c = l.data("wdkAutofocusPrevField");
        if (c === i) {
            c = a("." + j.className).filter(function () {
                return l.is(g(a(this), "wdkAutofocusNextField"))
            }).first();
            l.data("wdkAutofocusPrevField", c)
        } else if (!c.jquery) {
            c = a(c);
            l.data("wdkAutofocusPrevField", c)
        }
        return c
    };
    j.nextField = function (l) {
        var c = l.data("wdkAutofocusNextField");
        if (c === i) {
            c = a("." + j.className).filter(function () {
                return l.is(g(a(this), "wdkAutofocusPrevField"))
            }).first();
            l.data("wdkAutofocusNextField", c)
        } else if (!c.jquery) {
            c = a(c);
            l.data("wdkAutofocusNextField", c)
        }
        return c
    };
    j.paste = function () {
        var l = a(this);
        setTimeout(function () {
            var c = l.val();
            (function k(q) {
                var s = j.nextField(q),
                    p = j.maxlength(q),
                    n = c.substr(0, p);
                if (n !== "") {
                    q.val(n).change();
                    c = c.substring(p);
                    if (c !== "" && s.length > 0) k(s);
                    else {
                        s = q.val().length;
                        h(q, s, s)
                    }
                }
            })(l)
        }, 0)
    };
    j.keydown = function (l) {
        var c = l.keyCode,
            k = a(this),
            q = h(k),
            s = j.prevField(k),
            p = j.nextField(k),
            n = j.maxlength(k);
        if ((c === 37 || c === 8) && s.length > 0 && q.begin === 0 && q.end === 0) {
            l.preventDefault();
            k.change().blur();
            s.focus();
            h(s, 0, s.val().length)
        }
        if (c === 39 && p.length > 0 && q.begin === n && q.end === n) {
            l.preventDefault();
            k.change().blur();
            p.focus();
            h(p, 0, p.val().length)
        }
    };
    j.keypress = function (l) {
        var c = l.which;
        if (a.inArray(c, [0, 8, 17]) === -1 && !l.altKey && !l.ctrlKey) {
            var k = a(this),
                q = h(k),
                s = k.val(),
                p = j.maxlength(k),
                n = j.nextField(k);
            c = String.fromCharCode(c);
            q = s.substring(0, q.begin) + c + s.substring(q.end);
            if (q.length === p && n.length > 0) {
                l.preventDefault();
                k.val(q).change().blur();
                n.focus();
                h(n, 0, n.val().length)
            }
            q.length > p && l.preventDefault()
        }
    }
})(jQuery, this);
(function (a) {
    wdk.initDynamicAnnotations = function () {
        var f = a("body");
        a.each(wdk.annotations, function () {
            a.isFunction(this) && this.live && this(f)
        })
    };
    wdk.initStaticAnnotations = function () {
        a("body").initStaticAnnotations()
    };
    a.fn.initStaticAnnotations = function () {
        var f = this,
            i = [];
        a.each(wdk.annotations, function () {
            a.isFunction(this) && !this.live && i.push(this.selector || "." + this.className)
        });
        var g = f.find(i.join(","));
        a.each(wdk.annotations, function () {
            a.isFunction(this) && !this.live && this(f, g.filter(this.selector || "." + this.className))
        });
        return f
    }
})(jQuery);
(function (a) {
    var f = wdk.annotations.placeholder = function (i, g) {
            if (!f.supported) {
                g = g || i.find(f.selector);
                g.off("focus.placeholder blur.placeholder").on({
                    "focus.placeholder": function () {
                        var l = a(this);
                        l.attr("placeholder") !== "" && l.val() === l.attr("placeholder") && l.val("").removeClass(f.options.className)
                    },
                    "blur.placeholder": function () {
                        var l = a(this);
                        if (l.attr("aplaceholder") !== "" && (l.val() === "" || l.val() === l.attr("placeholder"))) l.val(l.attr("placeholder")).addClass(f.options.className)
                    }
                });
                try {
                    var h = a(document.activeElement).focus();
                    g = g.not(h)
                } catch (j) {}
                g.blur();
                g.closest("form").off("submit.placeholder").on("submit.placeholder", function () {
                    a(f.selector, this).val(function (l, c) {
                        var k = this.placeholder;
                        return k !== "" && c === k ? "" : c
                    })
                })
            }
        };
    f.live = false;
    f.supported = "placeholder" in document.createElement("input");
    f.selector = "*[placeholder]";
    f.options = {
        className: "hasPlaceholder"
    }
})(jQuery);
(function (a) {
    var f = wdk.annotations.popin = function (i) {
            i.off("click.popin");
            i.on("click.popin", "." + f.className, f.onClick)
        };
    f.className = "wdk-popin";
    f.live = true;
    f.onClick = function (i) {
        var g = a(this),
            h = a.extend({}, f.options, g.dataPrefix("wdkPopin")),
            j = a("div.wdk-popin-holder").dialog("option", h);
        if (j.length === 0) {
            var l = a("<div class='wdk-popin-loading'></div>"),
                c = a("<iframe class='wdk-popin-iframe' frameborder='0'></iframe>");
            j = a("<div class='wdk-popin-holder'></div>").appendTo("body").append(l, c).dialog(h);
            c.load(function () {
                l.hide();
                c.css({
                    height: j.height(),
                    width: j.width()
                }).fadeIn("normal")
            })
        }
        j.dialog("open");
        a("div.wdk-popin-loading", j).show();
        a("iframe.wdk-popin-iframe", j).hide().attr("src", h.href || g.attr("href"));
        i.preventDefault()
    };
    f.options = {
        title: "Information",
        modal: true,
        width: 500,
        height: 500,
        autoOpen: false,
        resizable: false
    }
})(jQuery);
(function (a) {
    var f = wdk.annotations.popup = function (i) {
            i.off("click.popup");
            i.on("click.popup", "." + f.className, f.onClick)
        };
    f.className = "wdk-popup";
    f.live = true;
    f.onClick = function (i) {
        var g = a(this),
            h = g.data(f.options.nameProperty) || f.options.name,
            j = decodeURIComponent(g.data(f.options.urlProperty) || g.attr("href")),
            l = j.substring(0, j.indexOf("?")),
            c = j.substring(l.length + 1),
            k = g.data(f.options.widthProperty) || f.options.width,
            q = g.data(f.options.heightProperty) || f.options.height,
            s = g.data(f.options.centerProperty),
            p = s === null ? f.options.center : s !== "false";
        s = g.data(f.options.leftProperty) || (p ? (window.screen.width - k) / 2 : f.options.left);
        p = g.data(f.options.topProperty) || (p ? (window.screen.height - q) / 2 : f.options.top);
        var n = g.data(f.options.scrollbarsProperty) || f.options.scrollbars,
            u = g.data(f.options.resizableProperty) || f.options.resizable;
        g = g.data(f.options.methodProperty) || f.options.method;
        j = g.toLowerCase() === "post" ? "about:blank" : j;
        k = ["scrollbars=", n, ",resizable=", u, ",width=", k, ",height=", q, ",left=", s, ",top=", p].join("");
        window.open(j, h, k);
        g === "post" && a("<form>", {
            method: "post",
            action: l,
            target: h
        }).append(a.map(c.split("&"), function (z) {
            return "<input type='hidden' name='" + z.split("=")[0] + "' value='" + (z.split("=")[1] || "") + "' />"
        }).join("")).appendTo("body").submit().remove();
        i.preventDefault()
    };
    f.options = {
        width: 600,
        height: 500,
        left: 50,
        top: 50,
        center: true,
        method: "get",
        name: "_blank",
        scrollbars: "no",
        resizable: "no",
        nameProperty: "wdk-popup-name",
        centerProperty: "wdk-popup-center",
        urlProperty: "wdk-popup-url",
        widthProperty: "wdk-popup-width",
        heightProperty: "wdk-popup-height",
        leftProperty: "wdk-popup-left",
        topProperty: "wdk-popup-top",
        scrollbarsProperty: "wdk-popup-scollbars",
        resizableProperty: "wdk-popup-resizable",
        methodProperty: "wdk-popup-method"
    }
})(jQuery);
(function (a) {
    var f = wdk.annotations.toggle = function (i) {
            i.off("click.toggle");
            i.on("click.Toggle", "." + f.className, f.onClick)
        };
    f.className = "wdk-toggle";
    f.live = true;
    f.onClick = function () {
        var i = a(this),
            g = a.extend({}, f.options, i.dataPrefix("wdkToggle")),
            h = a(g.target || ""),
            j = i.is("." + g.triggerOpenedClass);
        i.toggleClass(g.triggerClosedClass, j);
        i.toggleClass(g.triggerOpenedClass, !j);
        h.each(function () {
            function l() {
                k.trigger(g.openEvent)
            }
            function c() {
                k.trigger(g.closeEvent)
            }
            var k = a(this),
                q = a.extend({}, f.options, k.dataPrefix("wdkToggle")),
                s = k.is("." + g.targetOpenedClass);
            k.toggleClass(g.targetOpenedClass, !s);
            k.toggleClass(g.targetClosedClass, s);
            k.stop(true, true);
            if (q.transition === "slide") s ? k.slideUp(q.duration, c) : k.slideDown(q.duration, l);
            else if (q.transition === "fade") s ? k.fadeOut(q.duration, c) : k.fadeIn(q.duration, l);
            else if (q.transition !== "none") s ? k.hide(0, c) : k.show(0, l)
        })
    };
    f.options = {
        duration: "normal",
        transition: "normal",
        targetProperty: "wdk-toggle-target",
        closeEvent: "close",
        openEvent: "open",
        triggerOpenedClass: "wdk-toggle-opened",
        triggerClosedClass: "wdk-toggle-closed",
        targetOpenedClass: "wdk-toggle-opened",
        targetClosedClass: "wdk-toggle-closed"
    }
})(jQuery);
(function (a) {
    var f = wdk.annotations.tooltip = function (i, g) {
            g = g || i.find("." + f.className);
            g.each(function () {
                var h = a(this);
                h.data("qtip") || f.initWidget(h)
            })
        };
    f.className = "wdk-tooltip";
    f.live = false;
    f.initWidget = function (i) {
        var g = i.data("wdkTooltipContent"),
            h = i.data("wdkTooltipClasses"),
            j = i.data("wdkTooltipMy"),
            l = i.data("wdkTooltipAt");
        g = {
            prerender: true,
            style: {
                widget: true
            },
            show: {
                deplay: 0
            },
            content: wdk.utils.contentOrSelection(g || i.attr("title") || i.attr("alt")) || "nothing",
            position: {
                my: j || "top left",
                at: l || "bottom right",
                viewport: a(window),
                adjust: {
                    method: "shift flip"
                }
            }
        };
        if (h) g.style.classes = h;
        i.qtip(g)
    }
})(jQuery);
(function (a) {
    var f = wdk.annotations.waitingLink = function (i) {
            i.off("click.waitingLink").on("click.waitingLink", "." + f.className, f.click)
        };
    f.className = "wdk-waiting-link";
    f.live = true;
    f.click = function () {
        var i = a(this).data("wdkWaitingLinkPanel"),
            g = a(":wdk-waitingPanel").filter(function () {
                return a(this).waitingPanel("option", "isPrincipal")
            });
        if (typeof i !== "undefined") g = a(i);
        g.waitingPanel("pleaseWait")
    }
})(jQuery, this);
(function (a) {
    a.wdk.errorPanel = function () {};
    a.widget("wdk.errorPanel", {
        options: {
            isPrincipal: false,
            scrollToPanelOnError: true,
            widgetClass: "wdk-errorpanel",
            textClass: "wdk-errorpanel-field",
            linkClass: "wdk-errorpanel-link",
            itemClass: "wdk-errorpanel-item",
            listClass: "wdk-errorpanel-list",
            strongClass: "wdk-errorpanel-strong",
            errorClass: "wdk-errorpanel-fieldOnError",
            fieldContainerClass: "wdk-errorpanel-fieldContainer",
            errorContainerClass: "wdk-errorpanel-containerOnError",
            linkTitleScrollTo: "Scroll to the input"
        },
        default_options: {
            avoidAddingToPanel: false,
            avoidAddingTooltip: false
        },
        _init: function () {
            this.errors = {};
            this.element.addClass(this.options.widgetClass);
            this.element.hide();
            this.element.empty().append(a("<ul>").addClass(this.options.listClass))
        },
        addError: function (f, i, g, h) {
            if (typeof g === "string" && g !== "") {
                var j = this.errors[g];
                h = a.extend({}, this.default_options, h);
                if (j) {
                    a.isFunction(i) && a.inArray(i, j.checks) === -1 && j.checks.push(i);
                    if (f) {
                        var l = f.not(j.$input);
                        if (l.length > 0) {
                            h.avoidAddingTooltip || this._addTooltip(l, g, h);
                            j.$input = j.$input.add(l)
                        }
                    }
                    if (!h.avoidAddingToPanel && !j.$li) j.$li = this.addErrorElement(f, i, g, h)
                } else {
                    j = {
                        checks: [],
                        $input: a(),
                        options: h
                    };
                    a.isFunction(i) && j.checks.push(i);
                    if (!h || !h.avoidAddingToPanel) j.$li = this.addErrorElement(f, i, g, h);
                    if (f) {
                        if (!h || !h.avoidAddingTooltip) this._addTooltip(f, g, h);
                        j.$input = f
                    }
                    this.errors[g] = j
                }
            }
        },
        removeError: function (f, i, g, h) {
            if (typeof g === "string" && g !== "") {
                var j = this.errors[g];
                if (j) {
                    if (a.inArray(i, j.checks) !== -1 && f) {
                        this._removeTooltip(f, h, g);
                        j.$input = j.$input.not(f)
                    }
                    if (j.checks.length === 0 || f && j.$input.length === 0) {
                        j.$li && j.$li.remove();
                        delete this.errors[g];
                        this.hasError() || this.element.hide()
                    }
                }
            }
        },
        hasError: function () {
            return a.map(this.errors, function (f) {
                return f
            }).length > 0
        },
        reset: function () {
            var f = this;
            a.each(this.errors, function (i) {
                var g = this;
                g.checks.length === 0 && f.removeError(g.$input, null, i, g.options);
                a.each(g.checks, function () {
                    f.removeError(g.$input, this, i, g.options)
                })
            })
        },
        addErrorElement: function (f, i, g, h) {
            var j = this.element.find("ul");
            i = a("<span>").addClass(this.options.textClass).html(g);
            var l = i.find("a");
            j = a("<li>").addClass(this.options.itemClass).append(i).appendTo(j);
            if (l.length === 0) l = a("<a>").addClass(this.options.linkClass).html(g).appendTo(i.empty());
            if (f) {
                if (h && h.$linkedInputs) f = f.add(h.$linkedInputs);
                l.click(function (c) {
                    f.closest(":visible").first().scrollTo({
                        complete: function () {
                            setTimeout(function () {
                                f.effect("highlight", {
                                    color: "red"
                                })
                            }, 200)
                        }
                    });
                    c.preventDefault()
                }).attr("href", "#")
            }
            this.element.show();
            return j
        },
        _tooltipContent: function (f) {
            return f.length === 1 ? f[0] : a("<ul>").html(a.map(f, function (i) {
                return "<li>" + i + "</li>"
            }).join(""))
        },
        _addTooltip: function (f, i, g) {
            var h = this;
            if (f) {
                if (g && g.$linkedInputs) f = f.add(g.$linkedInputs);
                f.each(function () {
                    var j = a(this),
                        l = j.closest(g.fieldContainer || "." + h.options.fieldContainerClass),
                        c = l.length > 0 ? l : j,
                        k = j.data("_messages") || [];
                    if (c.data("qtip")) {
                        a.inArray(i, k) === -1 && k.push(i);
                        j.hasClass(h.options.errorClass) || j.addClass(h.options.errorClass);
                        c.qtip("option", "content.text", h._tooltipContent(k))
                    } else {
                        k.push(i);
                        l.addClass(h.options.errorContainerClass);
                        j.addClass(h.options.errorClass);
                        c.qtip({
                            content: {
                                text: h._tooltipContent(k)
                            },
                            killTitle: false,
                            position: {
                                my: "left center",
                                at: "right center",
                                adjust: {
                                    method: "flip none"
                                }
                            },
                            style: {
                                classes: "ui-tooltip-red",
                                widget: true
                            },
                            show: {
                                deplay: 0,
                                event: "mouseenter focus"
                            },
                            hide: {
                                event: "mouseleave blur"
                            },
                            events: {
                                show: function (q) {
                                    var s = a(":focus");
                                    q.originalEvent.type === "mouseenter" && s.length > 0 && s.data("qtip") !== undefined && q.preventDefault()
                                },
                                hide: function (q, s) {
                                    s.elements.target.is(":focus") && q.preventDefault()
                                }
                            }
                        })
                    }
                    j.data("_messages", k)
                })
            }
        },
        _removeTooltip: function (f, i, g) {
            var h = this;
            if (f) {
                if (i && i.$linkedInputs) f = f.add(i.$linkedInputs);
                f.each(function () {
                    var j = a(this),
                        l = j.closest(i.fieldContainer || "." + h.options.fieldContainerClass),
                        c = l.length > 0 ? l : j,
                        k = j.data("_messages") || [];
                    k = a.grep(k, function (q) {
                        return q !== g
                    });
                    if (k.length === 0) {
                        l.removeClass(h.options.errorContainerClass);
                        j.removeClass(h.options.errorClass);
                        c.qtip("destroy")
                    } else c.qtip("option", "content.text", h._tooltipContent(k));
                    j.data("_messages", k)
                })
            }
        }
    })
})(jQuery);
(function (a) {
    a.fn.populate = function (f) {
        var i = [];
        a.each(f, function (g, h) {
            h = typeof g === "string" ? h : g;
            i.push('<option value="' + (g === "EMPTY" ? "" : g) + '">' + h + "</option>")
        });
        return this.html(i.join(""))
    };
    a.fn.extract = function () {
        var f = {};
        this.filter("select").first().children("option").each(function () {
            var i = a(this);
            f[i.attr("value")] = i.text()
        });
        return f
    };
    a.scrollToElement = function (f, i) {
        i = a.extend({
            speed: 750
        }, i);
        a("html, body").animate({
            scrollTop: f.offset().top,
            scrollLeft: f.offset().left
        }, i);
        return f
    };
    a.fn.scrollTo = function (f) {
        return a.scrollToElement(this, f)
    };
    a.fn.input = function (f) {
        if (!this.length) return this;
        var i = this[0];
        f = f.split(":");
        var g = null,
            h = null,
            j = 0,
            l = [],
            c = null;
        for (g = g = null; j < f.length; j += 1) {
            g = f[j];
            h = g.charAt(0);
            if (h === "$" || h === "^") {
                g = g.substring(1);
                l.push(":input[name" + h + "=" + g + "]")
            } else {
                g = i.elements[g];
                c = c ? c.add(g) : a(g)
            }
        }
        if (l.length > 0) {
            g = this.find(l.join(","));
            c = c ? c.add(g) : g
        }
        return c || a()
    };
    a.fn.value = function (f) {
        if (typeof f !== "undefined") {
            this.filter(":radio, :checkbox").val([f]);
            this.filter(":input:not(:radio, :checkbox)").val(f);
            return this
        } else return this.first().is(":radio, :checkbox") ? a("input[type=" + this.attr("type") + "][name=" + this.attr("name") + "]:checked").val() : this.val()
    };
    a.fn.dataPrefix = function (f) {
        var i = RegExp("^" + wdk.utils.escapeForRegExp(f) + "([a-zA-Z0-9]+)$"),
            g = {};
        a.each(this.data(), function (h, j) {
            var l = h.match(i);
            if (l !== null) g[wdk.utils.camelize(l[1])] = j
        });
        return g
    }
})(jQuery);
(function (a) {
    a.wdk.waitingPanel = function () {};
    a.widget("wdk.waitingPanel", {
        options: {
            isPrincipal: false,
            panel: a()
        },
        _init: function () {
            var f = this.element.find("img").each(function () {
                (new Image).src = this.src
            });
            a(window).bind("beforeunload.fixImg", function () {
                f.each(function () {
                    this.src = this.src
                })
            })
        },
        pleaseWait: function () {
            this.options.panel.show().css("visibility", "hidden");
            this.element.show()
        },
        stopWait: function () {
            this.element.hide();
            this.options.panel.show().css("visibility", "visible")
        }
    })
})(jQuery);
(function (a) {
    function f() {
        this.nbPromises = 0;
        this.dfd = null
    }
    f.prototype._resolve = function () {
        this.nbPromises === 0 && this.dfd !== null && this.dfd.resolve()
    };
    f.prototype.add = function (i) {
        var g = this;
        this.nbPromises += 1;
        return i.always(function () {
            g.nbPromises -= 1;
            g._resolve()
        })
    };
    f.prototype.done = function (i) {
        var g = this;
        this.dfd = a.Deferred().done(i).then(function () {
            g.dfd = null
        });
        this._resolve()
    };
    a.wdk.wdkform = function () {};
    a.widget("wdk.wdkform", {
        options: {
            queue: new f,
            errorPanel: null,
            waitingPanel: null,
            check: null,
            prepareSubmit: null,
            ajaxSubmit: false,
            ajaxAdditionnalData: null,
            ajaxOptions: {}
        },
        _initWaitingPanel: function () {
            this.option("waitingPanel", (this.options.waitingPanel || a(":wdk-waitingPanel").filter(function () {
                return a(this).waitingPanel("option", "isPrincipal")
            })).waitingPanel())
        },
        _initErrorPanel: function () {
            this.option("errorPanel", (this.options.errorPanel || a(":wdk-errorPanel").filter(function () {
                return a(this).errorPanel("option", "isPrincipal")
            })).errorPanel())
        },
        _init: function () {
            var i = this;
            this.isSubmited = false;
            this._initErrorPanel();
            this._initWaitingPanel();
            this.element.bind("submit", a.proxy(this, "_submit"));
            this.element.delegate("input[type=radio], input[type=checkbox]", "click.wdkform", function () {
                i._revalidate(a(this))
            });
            this.element.delegate("input:not([type=radio]):not([type=checkbox]), select, textarea", "autocompletechange.wdkform change.wdkform", function () {
                i._revalidate(a(this))
            })
        },
        _revalidate: function (i) {
            var g = this,
                h = i.findCheck(!this.isSubmited);
            (function j(l) {
                var c = h[l];
                if (c) {
                    var k = c.check.isBlocker || c.options.isBlocker,
                        q = a.extend({}, c.options, {
                            avoidAddingToPanel: !g.isSubmited
                        }),
                        s = c.options.triggerFor;
                    if (typeof s === "string") s = a(s);
                    s = s || i;
                    c = s.check.apply(s, a.merge([c.check, c.options.error, q], c.args));
                    if (a.isDeferred(c)) k ? c.done(function () {
                        j(l + 1)
                    }) : c.always(function () {
                        j(l + 1)
                    });
                    else if (c || !k) j(l + 1)
                }
            })(0)
        },
        _submit: function (i) {
            var g = this;
            this.isSubmited = true;
            this.options.errorPanel.errorPanel("reset");
            this._check();
            this._validate();
            this.options.queue.done(function () {
                if (g.options.errorPanel.errorPanel("hasError")) {
                    i.preventDefault();
                    i.stopImmediatePropagation();
                    g.options.errorPanel.show().scrollTo()
                } else {
                    g.options.waitingPanel.waitingPanel("pleaseWait");
                    g._prepareSubmit();
                    if (g.options.ajaxSubmit) {
                        i.preventDefault();
                        g._ajaxSubmit()
                    }
                }
            })
        },
        addHiddenInput: function (i, g) {
            this.element.append('<input type="hidden" name="' + i + '" value="' + g + '" />')
        },
        addHiddenInputs: function (i) {
            var g = this,
                h = a.isPlainObject(i);
            a.each(i || [], function (j, l) {
                j = h ? j : l.name;
                l = h ? l : l.value;
                g.addHiddenInput(j, l)
            })
        },
        addError: function (i) {
            this.options.errorPanel.errorPanel("addError", null, null, i)
        },
        _ajaxSubmit: function () {
            var i = this,
                g = this.element.serializeArray(),
                h = this.options.ajaxAdditionnalData,
                j = this.options.ajaxData,
                l = this.element.attr("action"),
                c = this.element.attr("method");
            if (a.isArray(j)) g = j;
            else if (a.isFunction(j)) g = j(g, this.element);
            else {
                j = [];
                if (a.isArray(h)) j = h;
                else if (a.isFunction(h)) j = h(g, this.element);
                a.each(j, function () {
                    this.name !== undefined && this.value !== undefined && g.push({
                        name: this.name,
                        value: this.value
                    })
                })
            }
            a.ajax(l, a.extend({
                data: g,
                method: c
            }, this.options.ajaxOptions)).complete(function () {
                i.options.waitingPanel.waitingPanel("stopWait")
            })
        },
        _check: function () {
            a.isFunction(this.options.check) && this.options.check.apply(this.element, [this.element, this])
        },
        _validate: function () {
            var i = this;
            this.element.find(":input:enabled").each(function () {
                i._revalidate(a(this))
            })
        },
        _prepareSubmit: function () {
            a.isFunction(this.options.prepareSubmit) && this.options.prepareSubmit.apply(this.element, [this.element, this])
        }
    });
    wdk.form = function (i, g) {
        var h = wdk.form._cache[i] || (wdk.form._cache[i] = a(document.forms[i]));
        return g === undefined || h.is(":wdk-wdkform") ? h : h.wdkform(g)
    };
    wdk.form._cache = {}
})(jQuery);
(function (a, f) {
    wdk.message = function (i, g) {
        var h = window.clientMessages || {},
            j = a.isArray(g) ? g : a.makeArray(arguments).slice(1);
        h = h[typeof i === "string" ? i : ""];
        if (h === f) return wdk.formatMessage(wdk.message.options.notFound, [i]);
        return wdk.formatMessage(h, j)
    };
    wdk.message.options = {
        notFound: "{0} (not localized)"
    };
    window._ = wdk.message;
    wdk.messageExists = function (i) {
        return i !== f && i !== null && window.clientMessages !== f && window.clientMessages[i] !== f
    };
    wdk.formatMessage = function (i, g) {
        a.each(g || [], function (h) {
            i = i.replace(RegExp("\\{" + h + "\\}", "g"), this)
        });
        return i
    }
})(jQuery);
(function (a) {
    wdk.utils.asPlainObject = function (f) {
        f = f || {};
        if (a.isArray(f)) {
            var i = f;
            f = {};
            a.each(i, function () {
                f[this] = this
            })
        }
        return f
    };
    wdk.utils.contentOrSelection = function (f) {
        var i = f;
        try {
            var g = a(f);
            i = g.length > 0 ? g.html() : f
        } catch (h) {}
        return i
    };
    (wdk.utils.convertUnderscoredArray = function (f) {
        var i = [];
        a.each(f, function (g, h) {
            var j = g.match(wdk.utils.convertUnderscoredArray.REGEXP);
            if (j !== null && !isNaN(parseInt(j[2], 10))) {
                var l = parseInt(j[2], 10) - 1;
                i[l] = i[l] || {};
                i[l][j[1]] = h
            }
        });
        return i
    }).REGEXP = /^(\w+)_(\d+)$/;
    wdk.utils.camelize = function (f) {
        f = (f || "").replace(/-+(.)?/g, function (i, g) {
            return g ? g.toUpperCase() : ""
        });
        return f.charAt(0).toLowerCase() + f.substring(1)
    };
    wdk.utils.escapeForRegExp = function (f) {
        return f.replace(/[\-\[\]{}()*+?\.,\\\^$|#\s]/g, "\\$&")
    };
    wdk.utils.argumentList = function (f) {
        var i = 0,
            g = [],
            h = [];
        f = Function.prototype.toString.call(f).match(wdk.utils.argumentList.REGEXP);
        if (f !== null) for (h = f[1].split(","); i < h.length; i += 1) g.push(h[i]);
        return g
    };
    wdk.utils.argumentList.REGEXP = /^\s*function[^\(]*\(([a-zA-Z0-9\s,]*)\)/;
    wdk.utils.findPathOfScript = function (f) {
        var i = "",
            g = RegExp(wdk.utils.escapeForRegExp(f) + "(\\?.*)?$");
        a("script[src]").each(function () {
            var h = this.src.toString();
            if (h.match(g)) i = h
        });
        return i.substring(0, i.length - f.length)
    };
    wdk.utils.size = a.size = function (f) {
        var i = 0;
        if (a.isArray(f)) i = f.length;
        else if (a.isPlainObject(f)) for (var g in f) i += 1;
        return i
    };
    a.isDate = function (f) {
        return f instanceof Date && isFinite(f)
    };
    a.isDeferred = function (f) {
        return a.isPlainObject(f) && f.then && f.done && f.fail && f.always && f.pipe && f.progress && f.state
    }
})(jQuery);
(function (a) {
    wdk.validations.luhn = function (f) {
        if (/[^0-9\-]+/.test(f)) return false;
        f = f.replace(/\D/g, "");
        for (var i = 0, g, h = f.length - 1, j = false; h >= 0; h -= 1) {
            g = parseInt(f.charAt(h), 10);
            if (j && (g *= 2) > 9) g -= 9;
            i += g;
            j = !j
        }
        return i % 10 === 0
    };
    wdk.validations.email = function (f) {
        return wdk.validations.regexp(f, wdk.validations.email.REGEXP)
    };
    wdk.validations.email.REGEXP = /^[A-Z0-9\._%+\-]+@[A-Z0-9\.\-]+\.[A-Z]{2,4}$/i;
    wdk.validations.fieldEqual = function (f, i) {
        if (typeof i === "string") i = a(i);
        return f === i.val()
    };
    wdk.validations.fieldDifferent = function (f, i) {
        return !wdk.validations.fieldEqual(f, i)
    }
})(jQuery);
(function (a) {
    wdk.check = function (f) {
        if (typeof f === "string") f = wdk.validations[wdk.utils.camelize(f)];
        if (!a.isFunction(f)) return function () {
            return !!f
        };
        return f
    };
    wdk.initValidationShortcuts = function () {
        a.each(wdk.validations, function (f) {
            var i = f.charAt(0).toUpperCase() + f.substr(1);
            jQuery.fn["check" + i] = function () {
                return this.check.apply(this, a.merge([f], a.makeArray(arguments)))
            };
            jQuery.fn["attachCheck" + i] = function () {
                return this.attachCheck.apply(this, a.merge([f], a.makeArray(arguments)))
            }
        })
    };
    wdk.error = function (f) {
        if (a.isFunction(f)) f = f();
        if (typeof f === "string" && wdk.messageExists(f)) f = wdk.message(f);
        return f
    };
    wdk.checkExists = function (f) {
        return a.isFunction(wdk.validations[wdk.utils.camelize(f)])
    };
    a.fn.check = function (f, i, g) {
        function h(E, M) {
            if (E) {
                i && n.errorPanel("removeError", q, c, k, g);
                M && j.resolveWith(q, [q])
            } else {
                i && n.errorPanel("addError", q, c, k, g);
                M && j.rejectWith(q, [q])
            }
        }
        var j = a.Deferred(),
            l = a.makeArray(arguments).slice(3),
            c = wdk.check(f),
            k = wdk.error(i),
            q = this.filter(":input").not(":disabled"),
            s = q.first(),
            p = s.closest("form"),
            n = p.wdkform("option", "errorPanel");
        p = p.wdkform("option", "queue");
        var u = s.value(),
            z = c.apply(s, a.merge([u], l));
        if (a.isDeferred(z)) {
            z.always(function () {
                h(z.isResolved(), true)
            });
            return p.add(j.promise())
        } else {
            h(z);
            return z
        }
    };
    a.fn.hasErrors = function () {
        return this.hasClass(this.closest("form").wdkform("option", "errorPanel").errorPanel("option", "errorClass"))
    };
    a.fn.attachCheck = function (f, i, g) {
        var h = wdk.check(f),
            j = a.makeArray(arguments).slice(3),
            l = this;
        g = a.extend({
            error: i,
            onlyOnSubmit: false
        }, g);
        return this.each(function () {
            var c = a(this),
                k = c.data(a.fn.attachCheck.DATA_NAME) || [],
                q = l.not(c),
                s = g.additionalTrigger;
            q = a.extend({}, {
                $linkedInputs: q
            }, g);
            var p = {
                check: h,
                args: j,
                options: q
            };
            k.push(p);
            c.data(a.fn.attachCheck.DATA_NAME, k);
            if (s) {
                if (typeof s === "string") s = a(s);
                s.each(function () {
                    var n = a(this),
                        u = n.data(a.fn.attachCheck.DATA_NAME) || [];
                    u.push(a.extend(true, p, {
                        options: {
                            triggerFor: c
                        }
                    }));
                    n.data(a.fn.attachCheck.DATA_NAME, u)
                })
            }
        })
    };
    a.fn.attachCheck.DATA_NAME = "additionnalChecks";
    a.fn.findCheck = function (f) {
        var i = [];
        this.filter(":input").each(function () {
            function g(k, q) {
                if (wdk.checkExists(k)) {
                    var s = wdk.check(k),
                        p = a.extend({}, j, h.dataPrefix("wdkValidate" + k.charAt(0).toUpperCase() + k.substring(1))),
                        n = [],
                        u = wdk.utils.argumentList(s),
                        z = parseInt(s.priority, 10) || parseInt(p.priority, 10) || i.length + 1;
                    u.length === 2 && n.push(q);
                    u.length > 2 && a.each(q.split(","), function () {
                        n.push(a.trim(this))
                    });
                    if (!f || !p.onlyOnSubmit) i.push({
                        $input: h,
                        check: s,
                        args: n,
                        options: a.extend(p, j),
                        priority: z
                    })
                }
            }
            for (var h = a(this), j = a.extend({
                onlyOnSubmit: false
            }, h.dataPrefix("wdkValidate")), l = h.data("wdkValidate") || "", c = a.fn.findCheck.INLINE_CHECK_REGEXP.exec(l); c !== null;) {
                g(c[1], c[2]);
                c = a.fn.findCheck.INLINE_CHECK_REGEXP.exec(l)
            }
            a.each(h.dataPrefix("wdkValidate"), function (k, q) {
                g(k, q)
            });
            a.each(h.data(a.fn.attachCheck.DATA_NAME) || [], function () {
                if (!f || !this.options.onlyOnSubmit) {
                    var k = parseInt(this.check.priority, 10) || parseInt(this.options.priority, 10) || i.length + 1;
                    i.push({
                        $input: h,
                        check: this.check,
                        args: this.args,
                        options: this.options,
                        priority: k
                    })
                }
            })
        });
        i.sort(function (g, h) {
            return h.priority - g.priority
        });
        return i
    };
    a.fn.findCheck.INLINE_CHECK_REGEXP = /([a-zA-Z_$][0-9a-zA-Z_$]*)(?:\(([^\)]*)\))?/g
})(jQuery);
(function (a) {
    var f = wdk.validations.number = function (h) {
            return a.isNumeric(h)
        };
    f.isBlocker = true;
    f.priority = 99998;
    var i = wdk.validations.minValue = function (h, j) {
            return +h >= j
        },
        g = wdk.validations.maxValue = function (h, j) {
            return +h <= j
        };
    wdk.validations.valueBetween = function (h, j, l) {
        return i(h, j) && g(h, l)
    }
})(jQuery);
(function () {
    wdk.validations.mandatory = function (a) {
        return typeof a === "string" && a !== ""
    };
    wdk.validations.mandatory.isBlocker = true;
    wdk.validations.mandatory.priority = 99999;
    wdk.validations.regexp = function (a, f) {
        if (typeof f !== "function" || f.constructor.toString().match(/regexp/i) === null) f = RegExp(f);
        return typeof a === "string" && f.test(a)
    };
    wdk.validations.lengthBetween = function (a, f, i) {
        return wdk.validations.minLength(a, f) && wdk.validations.maxLength(a, i)
    };
    wdk.validations.lengthExact = function (a, f) {
        return wdk.validations.mandatory(a) && a.length === f
    };
    wdk.validations.minLength = function (a, f) {
        return typeof a === "string" && a.length >= f
    };
    wdk.validations.maxLength = function (a, f) {
        return typeof a === "string" && a.length <= f
    };
    wdk.validations.regexpOrEmpty = function (a, f) {
        return !wdk.validations.mandatory(a) || wdk.validations.regexp(a, f)
    }
})(jQuery);