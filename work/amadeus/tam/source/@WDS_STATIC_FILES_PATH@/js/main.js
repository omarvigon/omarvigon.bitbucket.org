$(document).ready(function()
{
	var browser = jQuery.uaMatch(navigator.userAgent).browser;
	var cssLink = $('<link />');
	cssLink.attr("rel","stylesheet")

	switch(browser)
	{				 
	 	case 'webkit':
		//cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/webkit.css");
		
		if((navigator.userAgent.indexOf("Safari")>0) && (navigator.userAgent.indexOf("Chrome")==-1))
			cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/safari.css");
	 	break;
	 	
		case 'opera':
		$("table.bound th:nth-child(2),table.bound th:nth-child(3),table.bound th:nth-child(4)").css("border-left-color","#fff");
		$("tr.details .right").css("border-left-color","#fff");
		$("#alpi input[size='24']").attr("size","22");
		$("#purf #purf_inst").attr("size","62");
		cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/opera.css");
		break;
	 	
		case 'mozilla':
		//cssLink.attr("href","../@WDS_STATIC_FILES_PATH@/css/mozilla.css");
	 	break;
		
		case 'msie':
		var nav=$("nav.nav");
		nav.find("a:first").addClass("first"); //<small style='position:absolute;left:0;top:0;width:16px;height:35px;background:#f00;border-radius:0 0 16px 0'></small>");
		nav.find("a:last").addClass("last");
		
		if($.browser.version==8.0)
		{
			$(".icoa.select").append("<small class='ico' style='position:absolute;background-position:0 -122px;right:3px;top:3px;z-index:2;cursor:pointer'></small>");
			//$("button.main.left").after("<small style='width:14px;height:30px;float:left;background:url(../@WDS_STATIC_FILES_PATH@/img/sprites.png) -281px 0'></small>");
			//$("button.main.right").append("<small style='width:14px;height:30px;position:absolute;left:-12px;zoom:1;background:#0f0'></small>");
			//position:absolute;background:url(../img/sprites.png) -281px 0;top:-1px;right:-14px
		}
		break;
	}
	$("head").append(cssLink);
	
	//lists in red
	var list_ul = $("section.list ul");
	list_ul.click( function()
	{
		list_ul.removeClass("selected").addClass("unselect").find(".slope").addClass("none");
		$(this).addClass("selected").removeClass("unselect").find(".slope").removeClass("none");
	});
	
	//purf payment module
	var purf_label = $("fieldset.form2 label");
	var purf_div = $("fieldset.form2 + fieldset div");
	purf_label.click( function()
	{
		purf_label.removeClass("active").addClass("caption");
		$(this).removeClass("caption").addClass("active");
		purf_div.addClass("none");
		$("#target_"+$(this).attr("id")).removeClass("none");
	});
	
	/* Initialization of variable*/
	/*Login Panel*/
	var logoutLink = $("#logoutLink");
	var loginLoggedState = $("#loginLoggedState");
	var loginKnownNotLoggedState = $("#loginKnownNotLoggedState");
	var loginNotLoggedState = $("#loginNotLoggedState");
	var notMeLogoutLink = $("#notMeLogoutLink");
	var loginButton = $("#logginButton1,#logginButton2");
	
	logoutLink.unbind().click( function()
	{
		loginLoggedState.addClass("none").removeClass("active");
		loginNotLoggedState.removeClass("none").addClass("active");
		$(this).addClass("none");
	});
	
	notMeLogoutLink.unbind().click( function()
	{
		loginKnownNotLoggedState.addClass("none").removeClass("active");
		loginNotLoggedState.removeClass("none").addClass("active");
		$(this).addClass("none");
	});
	
	loginButton.unbind().click( function()
	{
		loginNotLoggedState.addClass("none").removeClass("active");
		loginKnownNotLoggedState.addClass("none").removeClass("active");
		loginLoggedState.removeClass("none").addClass("active");
		logoutLink.removeClass("none");
	});
	
	/* WDS SEARCH*/
	var onlineCheckin = $("#onlineCheckin");
	var manageBooking = $("#manageBooking");
	var onlineSearch = $("#onlineSearch");
	
	$("#onlineCheckinButton").click(function(){
		onlineCheckin.removeClass("none").next().removeClass("none");
		manageBooking.addClass("none").next().addClass("none");
		onlineSearch.addClass("none").next().addClass("none");
	});
	
	$("#manageBookingButton").click(function(){
		manageBooking.removeClass("none").next().removeClass("none");
		onlineCheckin.addClass("none").next().addClass("none");
		onlineSearch.addClass("none").next().addClass("none");
	});
	
	$("#onlineSearchButton").click(function(){
		onlineSearch.removeClass("none").next().removeClass("none");
		manageBooking.addClass("none").next().addClass("none");
		onlineCheckin.addClass("none").next().addClass("none");
	});
});