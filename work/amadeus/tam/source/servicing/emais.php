<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="fr" />
        <meta name="Description" content="description"/>
        <meta name="Keywords" content="keywords"/>
        <title>EMAI</title>
    </head>
    <body style="font-family:Arial, Helvetica, sans-serif; font-size:11px;margin:0;">
        <table border="0" summary="" cellpadding="0" cellspacing="0" bgcolor="#dadada" style="width:100%;background-color:#dadada;margin:0;border-spacing:0">
            <tr>
                <td bgcolor="#dadada" style="width:100%;background-color:#dadada">
                    <?php include("../emails/emai_header.php") ?>
                    <table align="center" summary="" border="0" cellpadding="0" cellspacing="0" width="720" bgcolor="#ffffff" style="background-color:#fff;border-spacing:0">
                        <tr>
                            <td colspan="2" width="712" valign="top" style="padding-left:8px">
                                <table border="0" summary="" cellspacing="0" cellpadding="0" width="712" style="border-collapse:collapse;">
                                    <tr>
                                        <td><font style="font-size:22px;color:#000;">// EMAIL - MANAGE MY BOOKING</font></td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="left" style="height:20px;padding:0;font-size:0;margin:0">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>                    
                    <table align="center" summary="" border="0" cellpadding="0" cellspacing="0" width="720" bgcolor="#ffffff" style="background-color:#fff;border-spacing:0">
                        <tr>
                            <td bgcolor="#ffffff" width="512" valign="top" style="padding-left:8px;background-color:#ffffff">
                                <?php include("../emails/emai_booking_thanks.php"); ?>
                                <?php include("../emails/emai_eticket_refund.php") ?>                               
                                <?php include("../emails/emai_pics_panel.php") ?> 
                                <?php include("../emails/emai_passengers.php") ?>                              
                                <?php include("../emails/emai_flight_summary.php") ?>                                 
                                <?php include("../emails/emai_seat_price.php") ?>                                   
                                <?php include("../emails/emai_meal.php") ?>                                   
                                <?php include("../emails/emai_luggage.php") ?>                                
                                <?php include("../emails/emai_requests.php") ?>                                
                                <?php include("../emails/emai_price_recap.php") ?>                                
                                <?php include("../emails/emai_economy.php") ?>
                            </td>
                            <td width="175" valign="top" align="right" style="padding-right:8px;">
                                <?php include("../emails/emai_aside_checkin.php"); ?>                                    
                                <?php include("../emails/emai_aside_fidelity.php"); ?>                                  
                                <?php include("../emails/emai_aside_doc.php"); ?>   
                                <table summary="" width="175" border="0" cellspacing="0" cellpadding="0" align="right" style="padding:0;font-size:11px;text-align:left">  
                                    <tr>
                                        <td colspan="2" style="text-align: center;"><img border="0" src="../emails/img/emai_logo_aside.gif" alt="TAM Tips" width="123" height="68" /></td>
                                    </tr>
                                </table>
                            </td> 
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#dadada" style="background-color:#dadada">                                
                    <?php include("../emails/emai_footer.php") ?>                    
                </td>
            </tr>
        </table>    
    </body>
</html>
