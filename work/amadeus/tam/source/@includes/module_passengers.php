        <header class="caption4 hr">
        	<small class="fl ico guser"></small>
            <h3 class="h6">&nbsp;PASSENGERS</h3>
        </header>
        <article>
            <dl class="fr boxg middle">
                <dt><small class="ico gray1"></small></dt><dd><b class="em3">Contact information</b></dd>
                <dt class="fl grid0">E-mail: </dt><dd><b>johnsmithname@gmail.com</b></dd>
                <dt class="fl grid0">Mobile phone: </dt><dd><b>+33 0665845125</b></dd>
                <dt class="fl grid0">Type: </dt><dd><b>TIM</b></dd>
                <dt class="fl grid0">Country: </dt><dd><b>Brazil</b></dd>
            </dl>
            <dl class="boxg middle">
                <dt></dt><dd><b>Mr John Smith</b></dd>
                <dt class="fl grid3">User: </dt><dd><b>BA 45875865</b></dd>
                <dt></dt><dd><b class="em2">Secure flight information</b></dd>
                <dt class="fl grid3">Middle Name: </dt><dd><b>Joshua</b></dd>
                <dt class="fl grid3">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
                <dt class="fl grid3">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
                <dt class="fl grid3">Redress Number: </dt><dd><b>123-456-789-789</b></dd>
            </dl>
            <dl class="boxg middle br">
                <dt></dt><dd><small class="ico minus2"></small> <b>Mss Carolina Smith</b></dd>
                <dt class="fl grid3">User: </dt><dd><b>BA 45875865</b></dd>
                <dt></dt><dd><b class="em2">Secure flight information</b></dd>
                <dt class="fl grid3">Middle Name: </dt><dd><b>Carolina</b></dd>
                <dt class="fl grid3">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
                <dt class="fl grid3">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
                <dt class="fl grid3">Redress Number: </dt><dd><b>123-456-789-789</b></dd>
            </dl>
            <dl class="boxg middle br">
                <dt></dt><dd><small class="ico minus2"></small> <b>Mss Carolina Smith</b></dd>
                <dt class="fl grid3">User: </dt><dd><b>BA 45875865</b></dd>
            </dl>            
            <dl class="boxg middle br">
                <dt></dt><dd><small class="ico minus2"></small> <b>Ms Yelena Valgdimir ovna Afansyeva</b></dd>
                <dt class="fl grid3">User: </dt><dd><b>BA 45875865</b></dd>
                <dt></dt><dd><b class="em2">Secure flight information</b></dd>
                <dt class="fl grid3">Middle Name: </dt><dd><b>Yelena</b></dd>
                <dt class="fl grid3">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
                <dt class="fl grid3">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
                <dt class="fl grid3">Redress Number: </dt><dd><b>123-456-789-789</b></dd>
            </dl>
            <dl class="boxg middle br">
                <dt><small class="ico gray2"></small></dt><dd><b>Ms Ahmethov Spartak Galeevic</b></dd>
                <dt class="fl grid3 em3"><b>Infant travelling with:</b></dt><dd><b>Yelena Valgdimir</b></dd>
                <dt class="fl grid3">User: </dt><dd><b>BA 45875865</b></dd>
                <dt></dt><dd><b class="em2">Secure flight information</b></dd>
                <dt class="fl grid3">Middle Name: </dt><dd><b>Ahmethov</b></dd>
                <dt class="fl grid3">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
                <dt class="fl grid3">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
            </dl>
            <aside class="boxg middle br"><button class="fr">Show all details</button><button>Hide all details</button></aside>
		<?php if($page=="bkgd") { ?>
            <div class="tr"><button>Change passengers information</button></div>
        <?php } ?>            
		</article>