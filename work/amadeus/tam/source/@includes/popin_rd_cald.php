	<?php
    include("header_change_dates.php");
    include("tdefault_in_out.php");
	?>
        <table class="cald br tc">
        <thead>
        <tr>
        	<th class="caption" colspan="7"><b>Please select day in for OUTBOUND ( BSB- RIO)</b></th>
            <th class="bottomF topF">&nbsp;</th>
           	<th class="caption" colspan="7"><b>Please select day in for INBOUND ( RIO- BSB)</b></th>
		</tr>
        <tr>
            <th class="caption">MON</th>
            <th class="caption">TUE</th>
            <th class="caption">WED</th>
            <th class="caption2">THU</th>
            <th class="caption">FRI</th>
            <th class="caption">SAT</th>
            <th class="caption">SUN</th> 
            <td class="bottomF">&nbsp;</td>
            <th class="caption">MON</th>
            <th class="caption">TUE</th>
            <th class="caption">WED</th>
            <th class="caption">THU</th>
            <th class="caption2">FRI</th>
            <th class="caption">SAT</th>
            <th class="caption">SUN</th>             
        </tr>            
        </thead>
        <tbody>
        <tr class="grid0">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><b class="block">23 JUN</b>12.950</td>
            <td><b class="block">24 JUN</b>13.950</td>
            <td><b class="block">25 JUN</b>14.950</td>
            <td><b class="block">26 JUN</b>15.950</td>
            <td class="bottomF"></td>
            <td>&nbsp;</td>
            <td class="lowest em3"><small class="fr"><small class="ico cheap"></small></small><b>14 SEP</b><br />3.950</td>
            <td><b class="block">15 SEP</b>14.950</td>
            <td><b class="block">16 SEP</b>15.950</td>
            <td><b class="block">17 SEP</b>12.950</td>
            <td><b class="block">18 SEP</b>13.950</td>
            <td><b class="block">19 SEP</b>14.950</td>                                                                                           
        </tr>
        <tr>
            <td><b class="block">27 JUN</b>12.950</td>
            <td><b class="block">28 JUN</b>12.950</td>
            <td><b class="block">29 JUN</b>12.950</td>
            <td><b class="block">30 JUN</b>12.950</td>
            <td><b class="block">31 JUN</b>13.950</td>
            <th class="selected"><b class="block">01 JUL</b>14.950</th>
            <td><b class="block">02 JUL</b>15.950</td>
            <td class="bottomF"></td>
            <td><b class="block">20 SEP</b>12.950</td>
            <td><b class="block">21 SEP</b>13.950</td>
            <td><b class="block">22 SEP</b>14.950</td>
            <td><b class="block">23 SEP</b>15.950</td>
            <td><b class="block">24 SEP</b>12.950</td>
            <td><b class="block">25 SEP</b>13.950</td>
            <td><b class="block">26 SEP</b>14.950</td>                                                                                           
        </tr>
        <tr>
            <td><b class="block">03 JUL</b>12.950</td>
            <td><b class="block">04 JUL</b>12.950</td>
            <td><b class="block">05 JUL</b>12.950</td>
            <td class="caption3"><b class="block">06 JUL</b>12.950</td>
            <td><b class="block">07 JUL</b>13.950</td>
            <td><b class="block">08 JUL</b>14.950</td>
            <td><b class="block">09 JUL</b>15.950</td>
            <td class="bottomF"></td>
            <td><b class="block">27 SEP</b>12.950</td>
            <td><b class="block">28 SEP</b>13.950</td>
            <td><b class="block">29 SEP</b>14.950</td>
            <td><b class="block">30 SEP</b>15.950</td>
            <td><b class="block">31 SEP</b>12.950</td>
            <td><b class="block">01 OCT</b>13.950</td>
            <td><b class="block">02 OCT</b>14.950</td>                                                                                           
        </tr>
        <tr>
            <td><b class="block">10 JUL</b>12.950</td>
            <td><b class="block">11 JUL</b>12.950</td>
            <td><b class="block">12 JUL</b>12.950</td>
            <td><b class="block">13 JUL</b>12.950</td>
            <td><b class="block">14 JUL</b>13.950</td>
            <td><b class="block">15 JUL</b>14.950</td>
            <td><b class="block">16 JUL</b>15.950</td>
            <td class="bottomF"></td>
            <td><b class="block">03 OCT</b>12.950</td>
            <td><b class="block">04 OCT</b>13.950</td>
            <td><b class="block">05 OCT</b>14.950</td>
            <td><b class="block">06 OCT</b>15.950</td>
            <td class="caption3"><b class="block">07 OCT</b>12.950</td>
            <td><b class="block">08 OCT</b>13.950</td>
            <td><b class="block">09 OCT</b>14.950</td>                                                                                           
        </tr>
        <tr>
            <td><b class="block">17 JUL</b>12.950</td>
            <td><b class="block">18 JUL</b>12.950</td>
            <td><b class="block">19 JUL</b>12.950</td>
            <td><b class="block">20 JUL</b>12.950</td>
            <td><b class="block">21 JUL</b>13.950</td>
            <td><b class="block">22 JUL</b>14.950</td>
            <td><b class="block">23 JUL</b>15.950</td>
            <td class="bottomF"></td>
            <td><b class="block">10 OCT</b>12.950</td>
            <td><b class="block">11 OCT</b>13.950</td>
            <td><b class="block">12 OCT</b>14.950</td>
            <td><b class="block">13 OCT</b>15.950</td>
            <td><b class="block">14 OCT</b>12.950</td>
            <th class="selected"><b class="block">15 OCT</b>13.950</th>
            <td><b class="block">16 OCT</b>14.950</td>                                                                                           
        </tr>
        <tr>
            <td><b class="block">24 JUL</b>12.950</td>
            <td><b class="block">25 JUL</b>12.950</td>
            <td><b class="block">26 JUL</b>12.950</td>
            <td class="em1">27 JUL<br />No flight available</td>
            <td class="em1">28 JUL</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="bottomF"></td>
            <td><b class="block">17 OCT</b>12.950</td>
            <td><b class="block">18 OCT</b>13.950</td>
            <td><b class="block">19 OCT</b>14.950</td>
            <td><b class="block">20 OCT</b>15.950</td>
            <td><b class="block">21 OCT</b>12.950</td>
            <td class="em1">22 OCT</td>
            <td>&nbsp;</td>                                                                                           
        </tr>                   
        </tbody>
		</table>
        <p class="tr h5 em br">Total: 14,000,000 pts</p>
	</section>         
    <footer class="wrap caption4 default">
 	   <button class="fl main left">BACK TO MONT SELECTION</button>
       <button class="fr main right">NEXT</button>
    </footer>       