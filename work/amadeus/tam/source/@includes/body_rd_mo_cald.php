		<h1>Select Month</h1>
        <p>Please select your flight. Price is for 1 adult, excluding taxes and service fee. All prices are in Points.</p>
        <?php include("header_outbound_inbound.php");?>
        <table class="cald tc br">
        <caption class="h2 caption">Please select day in for OUTBOUND (BSB-RIO)</caption>
        <thead>
        <tr class="caption grid0">
           	<td><b>SEPTEMBER</b></td>
            <td><b>OCTOBER</b></td>
            <td><b>NOVEMBER</b></td>
            <td><b>DECEMBER</b></td>
            <td><b>JANUARY</b></td>
            <td><b>FEBRUARY</b></td>
        </tr>
        </thead>
        <tbody>
        <tr>
           	<td>310.111</td>
            <th class="em1">No flight available</th>
            <td>313.333</td>
            <td>314.444</td>
            <th class="selected">314.555</th>
            <td class="lowest em3"><small class="fr"><small class="ico cheap"></small></small><b>314.666</b></td>
        </tr>  
        </tbody>      
        </table>
        
        <table class="cald tc br">
        <caption class="h2 caption">Please select day in for INBOUND (RIO-BSB)</caption>
        <thead>
        <tr class="caption grid0">
           	<td><b>SEPTEMBER</b></td>
            <td><b>OCTOBER</b></td>
            <td><b>NOVEMBER</b></td>
            <td><b>DECEMBER</b></td>
            <td><b>JANUARY</b></td>
            <td><b>FEBRUARY</b></td>
        </tr>
        </thead>
        <tbody>
        <tr>
           	<td>310.111</td>
            <td>313.333</td>
            <td>314.444</td>
            <th class="selected">314.555</th>
            <td>314.666</td>
            <th class="em1">No flight available</th>
        </tr>
        </tbody>       
        </table>
        
        <table class="cald tc br">
        <caption class="h2 caption">Please select day in for OUTBOUND (BSB-RIO)</caption>
        <thead>
        <tr class="caption grid0">
           	<td><b>SEPTEMBER</b></td>
            <td><b>OCTOBER</b></td>
            <td><b>NOVEMBER</b></td>
        </tr>
        </thead>
        <tbody>
        <tr>
           	<td>310.111</td>
            <td>313.333</td>
            <th class="em1">No flight available</th>            
        </tr>
        </tbody>      
        </table>
        
        <table class="cald tc br">
        <caption class="h2 caption">Please select day in for INBOUND (RIO-BSB)</caption>
        <thead>
        <tr class="caption grid0">
           	<td><b>DECEMBER</b></td>
            <td><b>JANUARY</b></td>
            <td><b>FEBRUARY</b></td>
        </tr>
        </thead>
        <tbody>
        <tr>
           	<td>410.111</td>
            <th class="em1">No flight available</th>   
            <td>414.444</td>                     
        </tr>
        </tbody>      
        </table>
        <?php include("footer_prev_next.php");?>
    </div>
    <aside class="main fr">  
    	<?php include("aside_search.php");?>
    </aside>