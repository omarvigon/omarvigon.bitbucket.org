    <header class="caption4">
        <h3 class="h6">PRICE DETAILS</h3>
    </header>  
    <section>     
    <h2 class="h5">Confirm rebooking</h2>
    <p class="em"><b>Nothing has to be paid for this rebooking</b></p>
    <p>Clicking on proceed will confirm your modifications</p>            

    <table class="default boxw br">
        <thead>
            <tr class="caption boxb tc">
                <td>&nbsp;</td>
                <td class="h2" colspan="2">2 Adult</td>
                <td class="h2" colspan="2">2 Children</td>
                <td class="h2" colspan="2">1 Infant</td>
                <td>&nbsp;</td>
            </tr>
        </thead>
        <tfoot>
            <tr class="caption3 boxb">
                <th colspan="7"><b>Total paid</b></th>
                <td class="tr"><b>0,00$</b></td>
            </tr>
            <tr class="caption3 boxb">
                <th colspan="7"><b>Total refunded</b></th>
                <td class="tr em3"><b>-55.000,00$</b></td>
            </tr>
            <tr class="caption3">
                <td colspan="5"><b class="h5 em3">TOTAL Balance</b></td>
                <td class="h5 tr em3" colspan="3"><b>-15.000,00$</b></td>
            </tr>       
        </tfoot>  
        <tbody>
            <tr>
                <th colspan="8" class="h1 em"><b>NEW PRICE</b></th>
            </tr>  
            <tr>
                <th colspan="8" class="h4">TRAVELLERS</th>
            </tr>  
            <tr>
                <th><b>Total travellers</b></th>
                <td class="tr"><b>10.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>20.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>15.000,00</b></td>
                <td class="tc">=</td>
                <td class="tr"><b>45.000,00$</b></td>
            </tr> 
            <tr>
                <td class="h4 toggle minus" colspan="8"><small class="ico sign"></small> TAXES</td>
            </tr>  
            <tr>
                <th>Taxa de Combustivel e Seguranca</th>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th>Seguranca Aeroportuaria</th>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th>Service Taxes</th>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td class="tr">4.000,00</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th><b>Total taxes</b></th>
                <td class="tr"><b>12.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>12.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>12.000,00</b></td>
                <td class="tc">=</td>
                <td class="tr"><b>46.000,00$</b></td>
            </tr> 
            <tr>
                <th colspan="6"><b>Rebooking fee</b></th>
                <td class="tc"><b>=</b></td>
                <td class="tr boxb"><b>10.000,00$</b></td>
            </tr>
            <tr class="boxb">
                <th class="h4verd em"><b>Total New price</b></th>
                <td class="tr em"><b>22.000,00</b></td>
                <td class="tc em">+</td>
                <td class="tr em"><b>32.000,00</b></td>
                <td class="tc em">+</td>
                <td class="tr em"><b>27.000,00</b></td>
                <td class="tc em">=</td>
                <td class="tr em"><b>81.000,00$</b></td>
            </tr> 
            <tr>
                <th colspan="8" class="h1 em3">ORIGINAL PRICE</th>
            </tr>
            <tr>
                <th><b>Total travellers</b></th>
                <td class="tr"><b>10.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>7.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>3.000,00</b></td>
                <td class="tc">=</td>
                <td class="tr"><b>20.000,00$</b></td>
            </tr>   
            <tr>
                <th><b>Total taxes</b></th>
                <td class="tr"><b>12.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>12.000,00</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>12.000,00</b></td>
                <td class="tc">=</td>
                <td class="tr boxb"><b>46.000,00$</b></td>
            </tr> 
            <tr class="boxb">
                <th class="h4verd em3"><b>Total Original price</b></th>
                <td class="tr em3"><b>10.000,00</b></td>
                <td class="tc em3">+</td>
                <td class="tr em3"><b>11.000,00</b></td>
                <td class="tc em3">+</td>
                <td class="tr em3"><b>12.000,00</b></td>
                <td class="tc em3">=</td>
                <td class="tr em3"><b>33.000,00$</b></td>
            </tr> 
        </tbody>
    </table>
</section> 
<footer class="wrap caption4 default">
    <button class="fl main left">CANCEL</button>            
    <button class="fr main right">Yes, confirm modifications</button>
</footer>