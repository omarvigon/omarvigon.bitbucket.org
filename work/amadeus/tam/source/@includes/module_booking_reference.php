    <?php if($page!="atc_conf") { ?>    
        <header class="caption4 hr">
            <h3 class="h6">YOUR BOOKING REFERENCE NUMBER IS GDT523</h3>
        </header>
        <article>
            <a href="#" class="fr em tc"><b class="block">bolero barcode</b> <img src="../@WDS_STATIC_FILES_PATH@/img/barcode.png" align="Barcode" /></a>
            <h2>Your booking is on hold!</h2>            
            <p>The booking reference number is needed whenever in contact with TAM.</p>
			<p>Transaction number: <b>1234656789</b></p>            
            <p class="br"><a href="#" class="em"><b>Click here to pay your transaction</b></a></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ut lorem sem <b>(Thursday, september 2, 2011)</b>, sed tincidunt arcu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec quis lectus eros. Vivamus eget arcu quam, non viverra elit.</p>
        </article>
        
        <header class="caption4 hr">
            <h3 class="h6">YOUR BOOKING REFERENCE NUMBER IS GDT523</h3>
        </header>
        <article>
            <h2>Thank you! your booking is completed</h2>
            <p class="br">Your booking reference is <b class="h5 em">GDT523</b></p>
            <p>Please keep this number!</p>
            <p>The booking reference number is needed whenever in contact with TAM.</p>
        </article>
    <?php } else { ?>    
        <header class="caption4 hr">   
            <h3>YOUR BOOKING REFERENCE NUMBER IS GDT523</h3>
        </header>
        <article>
            <h2><b class="em">GDT523</b> Keep this number !</h2>            
            <p>The booking reference number is needed whenever in contact with TAM</p>            
        </article>
    <?php } ?>