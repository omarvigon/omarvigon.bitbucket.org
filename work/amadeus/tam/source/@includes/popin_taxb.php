<header class="caption4">
    <h3 class="h6">TAXES &amp; SERVICES</h3>
</header>
<section>
<table class="default boxw br">
    <thead>
        <tr class="caption boxw tc">
            <td>&nbsp;</td>
            <td colspan="2" class="h2">2 Adult</td>
            <td colspan="2" class="h2">2 Children</td>
            <td colspan="2" class="h2">1 Infant</td>
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tfoot>
    <tr class="caption3">
        <td class="h5 tr em" colspan="8"><b>TOTAL &emsp; 45.000,00$</b></td>
    </tr>  
    </tfoot>
    <tbody>
    <tr>
        <th>Taxa de Combustivel e Seguranca</th>
        <td class="tr">10.000,00</td>
        <td>&nbsp;</td>
        <td class="tr">10.000,00</td>
        <td>&nbsp;</td>
        <td class="tr">10.000,00</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>  
    <tr>
        <th>Seguranca Aeroportuaria</th>
        <td class="tr">5.000,00</td>
        <td>&nbsp;</td>
        <td class="tr">5.000,00</td>
        <td>&nbsp;</td>
        <td class="tr">5.000,00</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>                       
    <tr class="boxb">
        <th><b>Total Tax</b></th>
        <td class="tr"><b>15.000,00</b></td>
        <td class="tc"><b>+</b></td>
        <td class="tr"><b>15.000,00</b></td>
        <td class="tc"><b>+</b></td>
        <td class="tr"><b>15.000,00</b></td>
        <td class="tc"><b>=</b></td>
        <td class="tr"><b>45.000,00$</b></td>
    </tr>         
    </tbody>
    </table>
</section>