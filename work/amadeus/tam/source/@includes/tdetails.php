<?php if($page=="rd_upsl") { ?>
    <tr class="details dotted first">
        <td colspan="2" rowspan="3" class="rowspan">
        	<button class="short block midst wdk-popin" data-wdk-popin-href="../popins/seat_ro.php" data-wdk-popin-width="700" data-wdk-popin-height="840"><small class="fl ico gseat"></small> View seatmap</button>
            <button class="short block midst br"><small class="fl ico zoom"></small> Compare fare</button>
            <button class="short block midst br"><small class="fl ico book"></small> Legal notice</button>
		</td>
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_plane.png" class="fl" alt="" />&nbsp;<b class="h6">Rebooking</b></td>
        <td class="tc f2b">No</td>
        <td class="tc f2b f2c">No</td>
        <td class="tc">No</td>
        <td class="tc"><small class="ico ok"></small><br />From 20</td>
        <td class="right tc"><small class="ico ok"></small><br />From 20</td>
    </tr>
<?php } else { ?>
    <tr class="details dotted first">
        <td colspan="2" rowspan="4" class="rowspan">
            <button class="short block midst wdk-popin" data-wdk-popin-href="../popins/seat_ro.php" data-wdk-popin-width="700" data-wdk-popin-height="840"><small class="fl ico gseat"></small> View seatmap</button>
            <button class="short block midst br"><small class="fl ico zoom"></small> Compare fare</button>
            <button class="short block midst br"><small class="fl ico book"></small> Legal notice</button>
		</td>
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_tam.png" class="fl"  alt="" />&nbsp;<b class="h6">Fidelidade Points</b></td>
        <td class="tc f2b">50% miles</td>
        <td class="tc f2b f2c">75% miles</td>
        <td class="tc">85% miles</td>
        <td class="tc">100% miles</td>
        <td class="right tc">100% miles</td>
    </tr>
    <tr class="details dotted">
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_plane.png" class="fl" alt="" />&nbsp;<b class="h6">Rebooking</b></td>
        <td class="tc f2b">No</td>
        <td class="tc f2b f2c">No</td>
        <td class="tc">No</td>
        <td class="tc"><small class="ico ok"></small><br />From 20</td>
        <td class="right tc"><small class="ico ok"></small><br />From 20</td>
    </tr>
<?php } ?>
    <tr class="details dotted">
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_bag.png" class="fl" alt="" />&nbsp;<b class="h6">Baggage allowance</b> <small class="ico info"></small></td>
        <td class="tc f2b">25kg</td>
        <td class="tc f2b f2c">30kg</td>
        <td class="tc">35kg</td>
        <td class="tc">40kg</td>
        <td class="right tc">50kg</td>
    </tr>
    <tr class="details dotted">
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_man.png" class="fl" alt="" />&nbsp;<b class="h6">Priority Services</b></td>
        <td class="tc f2b">No</td>
        <td class="tc f2b f2c">No</td>
        <td class="tc">No</td>
        <td class="tc">Check-in, boarding, baggage</td>
        <td class="right tc">Check-in, boarding, baggage</td>
    </tr>
    <tr class="details mark">
        <td class="rightF" colspan="2">&nbsp;</td>
        <td class="right" colspan="2">&nbsp;</td>
        <td class="tc f2b"><span class="slope"></span></td>
        <td class="tc f2b f2c"><span class="slope"></span></td>
        <td class="tc right"><span class="slope family f3"></span></td>
        <td class="tc right"><span class="slope"></span></td>
        <td class="tc"><span class="slope"></span></td>
    </tr>
    <tr class="details last">
        <td colspan="9" class="family f3">
			<p class="hr"><button class="fr">Upgrade</button> <b>Did you know that for 195 more you could upgrade to FLEX Fare?</b></p>
        	<p class="caption3 tr"><b class="em">Hide <small class="ico hide"></small></b> &nbsp;</p>    
		</td>
    </tr>