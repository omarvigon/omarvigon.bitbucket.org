        <h3 class="caption4 hr">FLIGHT SUMMARY</h3>
        <article>
            <h4 class="toggle plus" data-wdk-toggle-target="#bound0NoDetails,#bound0WithDetails"><small class="ico sign"></small> OUTBOUND</h4>
            <section class="boxg wrap column" id="bound0NoDetails" data-wdk-toggle-transition="none">
            	<aside class="fl">
                <?php include("aside_date.php");?>
                </aside>
                <div class="fr">
	            	<p><b>8:30</b> Sao Paulo, Congonhas</p>
    	            <p><b>10:55</b> Rio de Janerio, Santos Dumont</p>
                </div>
                <?php
				if(($page=="fsr") || ($page=="purf"))
					include("aside_luggage.php");
				if($page=="fsrs")
					include("aside_luggage_fsrs.php");	
				?>
            </section>
            <section class="boxg wrap column none" id="bound0WithDetails" data-wdk-toggle-transition="none">
            <?php
            include("aside_outbound.php");
			if(($page=="fsr") || ($page=="purf"))
				include("aside_luggage.php");
			if($page=="fsrs")
				include("aside_luggage_fsrs.php");	
			?>
			</section>