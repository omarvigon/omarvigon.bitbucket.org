        <h2>JJ412 - Airbus Industrie A319</h2>
        <div class="seat br">
        <table>
        <thead>
        <tr class="tc em1">
            <td>&nbsp;</td>
	        <th>A</th>
            <th>B</th>
            <th>C</th>
            <td class="grid-1">&nbsp;</td>
            <th>D</th>
            <th>E</th>
            <th>F</th>
            <th>G</th>
            <th>H</th>
            <td class="grid-1">&nbsp;</td>
            <th>I</th>
            <th>J</th>
            <th>K</th>
            <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody class="tc">
        <tr>
        	<td></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">1</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seat2"><b class="h6">3</b></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
			</td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seat2"><b class="h6">2</b></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
            </td>
            <td class="em1">1</td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seatxl"></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
			</td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seatxl"></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
			</td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seatxl"></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
			</td>
            <td></td>
        </tr>
        <tr>
        	<td class="caption3"><small class="block wing1"></small></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">2</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seat2x"><b class="h6">1</b></small
                ><?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
            </td>
            <td>
               	<?php if($page=="seat") { ?>
            	<small class="ico seat2x"><b class="h6">2</b></small
                ><?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
            </td>
            <td class="em1">2</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="caption3"><small class="block wing2"></small></td>
        </tr>
        <tr>
        	<td class="caption3"></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">3</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">3</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="caption3"></td>
        </tr>
        <tr>
        	<td class="caption3"><small class="ico seate"></small></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">4</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">4</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="caption3"><small class="ico seate"></small></td>
        </tr>
        <tr>
        	<td class="caption3"></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">5</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">5</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="caption3"></td>
        </tr>
		<tr>
        	<td class="caption3"><small class="ico seate"></small></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">6</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">6</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="caption3"><small class="ico seate"></small></td>
        </tr>
		<tr>
        	<td class="caption3"></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">7</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">7</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="caption3"></td>
        </tr>
        <tr>
        	<td class="caption3">&nbsp;</td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seatx"></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
			</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">8</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">8</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td>
            	<?php if($page=="seat") { ?>
            	<small class="ico seatx"></small>
                <?php } else { ?>
                <small class="ico seat1"></small>
                <?php } ?>
			</td>
            <td class="caption3">&nbsp;</td>
        </tr>
        <tr>
        	<td></td>
            <td colspan="13"><p>&nbsp;</p></td>
            <td></td>
        </tr>
		<tr>
        	<td></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seatt"></small></td>
            <td class="em1">9</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">9</td>
            <td><small class="ico seatt"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td></td>
        </tr>
		<tr>
        	<td></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seatg"></small></td>
            <td class="em1">10</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">10</td>
            <td><small class="ico seatg"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td></td>
        </tr>
        <tr>
        	<td></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">11</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">11</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td></td>
        </tr>
        <tr>
        	<td></td>
	        <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">12</td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat0"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td class="em1">12</td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td><small class="ico seat1"></small></td>
            <td></td>
        </tr>                                                                 
        </tbody>
        </table>
        </div>