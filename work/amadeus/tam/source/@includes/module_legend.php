<aside class="wrap hr">
    <p class="fl"><b>Legend:</b></p>
    <ul class="fl grid23 list2">
    	<li><small class="ico tam2 midst"></small>Flight operated by TAM</li>
        <li><small class="ico lan midst"></small>Flight operated by LAN</li>
        <li><small class="ico star midst"></small>Flight operated by partner airline</li>
        <li><small class="ico flight2 midst"></small>Flight operated by other airlin</li>
        <li><small class="ico seat"></small> Hurry! Last seat availability</li>
        <?php if(($page=="od_upsl")) {?>
        <li><small class="ico uncomb midst"></small>Uncombinable flight</li>
		<?php } ?>
        <li><small class="em">&nbsp;+1</small> Next day arrival</li>
    </ul>
</aside>