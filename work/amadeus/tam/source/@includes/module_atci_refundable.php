        <header class="caption4 hr">
            <h3 class="h6">YOUR REFUNDS DETAILS</h3>
        </header>

        <table class="default boxw">
        <caption class="h2 br caption">&nbsp;Your Total refund summary</caption>
        <tfoot>
        <tr class="caption boxw">
			<td><b class="em">Sub-total to be refunded for the bound</b></td>
            <td class="tr"><b class="em">2500.05$</b></td>
            <td class="grid5">&nbsp;</td>
    	</tr>
        </tfoot>
        <tbody>
        <tr>
            <th>Paid Fare</th>
            <td class="tr"><b>2,720.05$</b></td>
            <td rowspan="4">&nbsp;</td>    
        </tr>
        <tr>
            <th>Used fare</th>
            <td class="tr"><b>-200.00$</b></td>
		</tr>
        <tr>
            <th>Tax to be refunded</th>
            <td class="tr"><b>80.00$</b></td>
		</tr>
        <tr>
            <th>Penalty</th>
            <td class="tr"><b>-100.00$</b></td>
		</tr>        
        </tbody>      
        </table>
        <p class="em1 br">* NB: Some of the data realted to the total to be refund were missing.</p>