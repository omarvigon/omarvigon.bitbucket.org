<header class="caption4">
    <h3 class="h6">INSURANCE</h3>
</header>
<section>
	<h2>Mondial Assistance for 95,00$*</h2>
	<p>This provides protection for the non-refundable cost of your TAM ticket (up to R$500 per person) if you need to cancel your trip for a number of specified reasons such as illness or redundancy.</p>
	<p class="em1">* For one passenger and for the whole trip</p>
</section>    
<footer class="wrap caption4 default">
	<button class="fl main left">YES, I WANT TO BUY INSURANCE WITH MONDIAL ASSISTANCE *</button>            
    <button class="fr main right">NO, THANK YOU</button>
</footer>