<header class="caption4">
    <h3 class="h6">CONFIRMATION</h3>
</header>
<section>
	<p class="hr">Localized text about the confirmation ask</p>
</section>
<footer class="wrap caption4 default">
	<button class="fl main left"> NO</button>            
    <button class="fr main right">YES</button>
</footer>