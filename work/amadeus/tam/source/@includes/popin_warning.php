<header class="caption4">
    <h3 class="h6">WARNING</h3>
</header>
<section>
	<p class="hr">Your session will expire in 2 minutes and 30 seconds, you're going to be redirect to the home page, would you like to extend the session ?</p>
</section>    
<footer class="caption4 default tr">
    <button class="main right">EXTEND SESION</button>
</footer>