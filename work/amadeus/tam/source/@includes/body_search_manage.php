<?php if($page=="wds_srch") { ?>
    <fieldset>
        <legend class="fl boxw caption h2 em3 br">Search your flight. Lorem ipsum dolor sit amet consectetur adipiscing elit.</legend>
        <fieldset class="boxw">
			<ul class="default">
                <li>Lorem ipsum dolor sit amet, consectetur</li>
                <li>Curabitur lectus sem, commodo vel varius id, posuere consectetur sapien</li>
                <li>Nam tincidunt vestibulum consectetur</li>
            </ul>
            <p class="br">Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur:</p>
        </fieldset>
    </fieldset>
    <fieldset>
        <legend class="fl boxw caption h2 em3 br">Reference</legend>
        <fieldset class="boxw">
            <p class="wrap br">
                <label class="fl grid3" for="search_code">Bookin reference code: </label>
                <input type="text" class="fl input" id="search_code" size="14" />
            </p>
            <p class="wrap">
                <label class="fl grid3" for="search_family">Passenger family name: </label>
                <input type="text" class="fl input" id="search_family" size="14" />
            </p>
        </fieldset>
    </fieldset>
    <footer class="caption4 tr br">			             
        <button class="main right">SEARCH</button>
    </footer>
<?php } else { ?>
    <fieldset>
        <legend class="fl br mr">Search your flight. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</legend>
        <ul class="default">
            <li>Lorem ipsum dolor sit amet, consectetur</li>
            <li>Curabitur lectus sem, commodo vel varius id, posuere consectetur sapien</li>
            <li>Nam tincidunt vestibulum consectetur</li>
        </ul>
        <p class="br">Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur:</p>
        <p class="wrap br">
            <label class="fl grid3" for="search_code">Bookin reference code: </label>
            <input type="text" class="fl input"  id="search_code" size="14" />
        </p>
        <p class="wrap">
            <label class="fl grid3" for="search_family">Passenger family name: </label>
            <input type="text" class="fl input" id="search_family" size="14" />
        </p>
        <footer class="tr br">
        	<button class="main right">SEARCH</button>
		</footer>
    </fieldset>
<?php } ?>