</section>
<footer class="home em2">
	<nav class="main wrap">
    	<a href="#" class="fl"><small class="fl ico facebook"></small><b class="block">TAM on</b>Facebook</a>
    	<a href="#" class="fl"><small class="fl ico twitter"></small><b class="block">Follow us on</b>Twitter</a>
      	<a href="#" class="fl"><small class="fl ico youtube"></small><b class="block">TAM channel on</b>YouTube</a>
       	<a href="#" class="fl"><small class="fl ico flickr"></small><b class="block">TAM on</b>Flickr</a>
       	<a href="#" class="fl"><small class="fl ico blog"></small><b class="block">TAM's blog</b>Runway</a>
    </nav>
	<nav class="main wrap">
   		<div class="fl">
			<a href="#" class="block">Institutional</a>
            <a href="#" class="block">Services</a>
            <a href="#" class="block">TAM Fidelidade FFP Program</a>
            <a href="#" class="block">Experience</a>
            <a href="#" class="block">Contacts</a>
       	</div>
    	<div class="fl">
            <a href="#" class="block">Register now in our TAM</a>
            <a href="#" class="block">Fidelidade FFP Program</a>
            <a href="#" class="block">Webcheck-in</a> 
   	    </div>
    	<div class="fl">
            <a href="#" class="block">Air Transportation Contract</a>
            <a href="#" class="block">Terms of Service</a>
            <a href="#" class="block">Privacy and Security Policy</a>
		</div>
    	<div class="fl">
        	<a href="#" class="block">Investor relations</a>
            <a href="#" class="block">About TAM</a>
            <a href="#" class="block"><img src="../@WDS_STATIC_FILES_PATH@/img/feedback.gif" alt="feedback" />Feedback</a>            
		</div>                        
    	<div class="fl">
        	<a href="#" class="block">TOP OF PAGE <small class="ico top"></small></a>
		</div>
	</nav>
	<nav class="main wrap">
   		<div class="fl">
        	<small class="fl ico secured"></small>
            <span class="fl midst em2"><b class="block">Secured</b>payment</span>
		</div>
       	<div class="fl doble">
        	<small class="fl ico payment"></small>
            <span class="fl em2 midst br">Forms of <b>payment</b> allowed</span>
		</div>
        <small class="fr ico trustsign" title="TrustSign"></small>
   	</nav>
	<p class="main em2 tc">&copy; TAM Linhas A&eacute;reas S.A. Total or Partial reproduction forbidden without previous authorization</p>  
</footer>
<?php include("foot_panel.php");?>