        <fieldset class="fl formadv">
			<legend class="fl"><b><small class="ico arrow"></small> Advance Passenger Information</b></legend>
			<p class="hr"><small class="ico warn tc"><b>!</b></small> Your selected flight requires further information.</p>
			<?php if($i!=2) { ?>
			<p class="wrap">
				<span class="fl grid1">Date of birth</span>
				<?php include("form_year.php");?>
			</p>  
			<?php } ?>             
			<p class="wrap">
				<label class="fl grid1" for="alpi_passport<?php echo $i?>">Passport number</label>
				<input type="text" class="fl input" id="alpi_passport<?php echo $i?>" size="15" />
				<span class="fl midst em1">(optional)</span>
			</p>
			<p class="wrap">
				<span class="fl grid1">Date of issue</span>
				<?php include("form_year.php");?>
			</p> 
			<p class="wrap">
				<label class="fl grid1" for="alpi_country<?php echo $i?>">Country of issue</label>
				<span class="fl icoa select"><input type="text" class="input" id="alpi_country<?php echo $i?>" size="26" placeholder="Select" /></span>
			</p>
			<p class="wrap">
				<label class="fl grid1" for="alpi_redress<?php echo $i?>">Redress number</label>
				<input type="text" class="fl input" id="alpi_redress<?php echo $i?>" size="15" />
				<span class="fl midst em1">(optional)</span>
			</p>
		</fieldset>