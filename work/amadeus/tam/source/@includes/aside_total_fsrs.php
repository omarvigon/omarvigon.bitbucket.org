        <?php if($page=="pnrs") { ?>
            <h4 class="br">
            	<em id="totalClosed" class="fr h4 em">200,000 $</em> TOTAL
			</h4>
            <section class="boxg none" id="totalOpened" data-wdk-toggle-transition="none">
		<?php } else { ?>
            <h4 class="br toggle minus" data-wdk-toggle-target="#totalOpened,#totalClosed">
            	<em id="totalClosed" data-wdk-toggle-transition="none" class="fr h4 em none">200,000 $</em><small class="ico sign"></small> TOTAL
			</h4>
            <section class="boxg" id="totalOpened" data-wdk-toggle-transition="none">
        <?php } ?>
                <dl>
                    <dt class="fl"><b>Total to be paid</b></dt><dd class="tr"><b>200,000 $</b></dd>
                    <dt class="fl h5 em">Total</dt><dd class="h5 em tr">200,000 $</dd>
                </dl> 
           	 	<?php include("aside_total_currency.php");?>
			</section>