        <header class="caption4 hr">
            <small class="fl ico insurance"></small>
            <h3 class="h6">&nbsp;TRAVEL INSURANCE</h3>
        </header>
        <article>
        	<p class="em1">This provides protection for the non-refundable cost of your TAM ticket (up to R$500 per person) if you need to cancel your trip for a number of  specified reasons such as illness or redundancy... <a href="#" class="em">More info</a></p>
        	<div class="wrap hr">
				<label class="fl grid9"><input type="radio" class="fl" name="radio1" /> <b>Cancellation Insurance</b> <b class="em">50,00*</b></label>
           		<label class="fl grid9"><input type="radio" class="fl" name="radio1" /> <b>No Insurance</b></label>
            </div>
            <p class="small em1 hr">* For one passenger and for the whole trip</p>
        </article>