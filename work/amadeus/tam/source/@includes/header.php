<?php include("head.php");?>
<body id="<?php echo $page;?>">
<header class="main hr caption">
	<a href="#"><img class="fl" src="../@WDS_STATIC_FILES_PATH@/img/tam_logo.png" alt="TAM" /></a>
    
    <?php if($page!="atci") { ?>
    <aside class="fr" id="loginDiv">
    	<h5 class="button em toggle minus" data-wdk-toggle-target="#loginDiv .active"><small class="ico sign"></small>LOGIN</h5>
        <a href="#" id="logoutLink" class="fr em none">Log out</a>
        
		<article class="none" data-wdk-toggle-transition="none" id="loginLoggedState">
            <p class="fr em2"><span class="h1">90500</span> pts</p>
            <h1>Welcome Mr. Jhon Smith</h1>
            <p class="em2">Account: <b>b65G54B889</b></p>
            <p class="em2">Fidelidade program: <b>black</b></p>            
    	</article>
        <article class="none" data-wdk-toggle-transition="none" id="loginNotLoggedState">
            <label class="fl">
            	<b class="em2">Fidelidade Number</b>
                <input type="text" class="block input" />
                <a href="#" class="small em">Forgot your number</a>
			</label>
            <label class="fl">
              	<b class="em2">Password</b>
                <input type="password" class="block input" />
                <a href="#" class="small em">Forgot your password</a>
			</label>
            <button class="fr main br" id="logginButton1">LOGIN</button>
        </article>
        <article class="active" data-wdk-toggle-transition="none" id="loginKnownNotLoggedState">
            <p><b>Hello,</b> <b class="em3">Jhon Smithsmith Longsurname</b></p>
            <button class="fr main br" id="logginButton2">LOGIN</button>
            <label><b class="em2">Password</b> <input type="password" class="block input" /> <a href="#" class="small em" id="notMeLogoutLink">Not Jhon Smith Longsurname?</a></label>
        </article>
    </aside>
    <?php } ?>
</header>

<?php if($page!="bkgd") { ?>
<nav class="main nav">
	<?php switch($page)	{
		
	case "wds_srch": ?>
    	<a href="#" class="fl unselect">SEARCH</a>
    	<a href="#" class="fl unselect">FLIGHTS</a>
  	  	<a href="#" class="fl selected">PASSENGERS</a>
    	<a href="#" class="fl caption2">FLIGHT OPTIONS</a>
   		<a href="#" class="fl caption2">PAYMENT</a>
   		<a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a>    
	<?php break;

	case "alpi": ?>
	    <a href="#" class="fl unselect">SEARCH</a>
		<a href="#" class="fl unselect">FLIGHTS</a>
	    <a href="#" class="fl selected">PASSENGERS</a>
	    <a href="#" class="fl caption2">FLIGHT OPTIONS</a>
	    <a href="#" class="fl caption2">PAYMENT</a>
    	<a href="#" class="fl caption2">CONFIRMATION</a> 
	<?php break;
	
	case "fsr": ?>
	    <a href="#" class="fl unselect">SEARCH</a>
		<a href="#" class="fl unselect">FLIGHTS</a>
	    <a href="#" class="fl unselect">PASSENGERS</a>
	    <a href="#" class="fl selected">FLIGHT OPTIONS</a>
	    <a href="#" class="fl caption2">PAYMENT</a>
    	<a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a> 
	<?php break;
	
	case "purf": ?>
	    <a href="#" class="fl unselect">SEARCH</a>
		<a href="#" class="fl unselect">FLIGHTS</a>
	    <a href="#" class="fl unselect">PASSENGERS</a>
	    <a href="#" class="fl unselect">FLIGHT OPTIONS</a>
	    <a href="#" class="fl selected">PAYMENT</a>
    	<a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a> 
	<?php break;
	
	case "conf": ?>
	    <a href="#" class="fl caption2">SEARCH</a>
		<a href="#" class="fl caption2">FLIGHTS</a>
	    <a href="#" class="fl caption2">PASSENGERS</a>
	    <a href="#" class="fl caption2">FLIGHT OPTIONS</a>
	    <a href="#" class="fl caption2">PAYMENT</a>
    	<a href="#" class="fl selected"><small class="fr ico mail midst br"></small>CONFIRMATION</a> 
	<?php break;

    case "atc_srch": ?>   
        <a href="#" class="fl unselect">MANAGE MY BOOKING</a>    
        <a href="#" class="fl selected">SEARCH</a> 
        <a href="#" class="fl caption2">FLIGHTS</a>
        <a href="#" class="fl caption2">PAYMENT</a>
        <a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a> 
    <?php break;

    case "atc_upsl":
    case "atc_cald": ?>   
        <a href="#" class="fl unselect">MANAGE MY BOOKING</a>    
        <a href="#" class="fl unselect">SEARCH</a> 
        <a href="#" class="fl selected">FLIGHTS</a>
        <a href="#" class="fl caption2">PAYMENT</a>
        <a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a>   
    <?php break; 

    case "atc_purf": ?>   
        <a href="#" class="fl caption2">MANAGE MY BOOKING</a>    
        <a href="#" class="fl caption2">SEARCH</a> 
        <a href="#" class="fl caption2">FLIGHTS</a>
        <a href="#" class="fl selected">PAYMENT</a>
        <a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a>   
    <?php break; 

   	case "atc_conf": ?>   
        <a href="#" class="fl unselect">MANAGE MY BOOKING</a>    
        <a href="#" class="fl unselect">SEARCH</a> 
        <a href="#" class="fl unselect">FLIGHTS</a>
        <a href="#" class="fl unselect">PAYMENT</a>
        <a href="#" class="fl selected"><small class="fr ico mail midst br"></small>CONFIRMATION</a>   
    <?php break; 
	
	case "atci": ?>   
    	<a href="#" class="fl unselect">MANAGE MY BOOKING</a>
    	<a href="#" class="fl selected">CANCEL AND REFUND ITINERARY</a>
    <?php break;
	
	case "fsrs": ?>   
    	<a href="#" class="fl unselect">MANAGE MY BOOKING</a>
    	<a href="#" class="fl selected">FLIGHT OPTIONS</a>
    <?php break;
	
	
	case "pnrs": ?>   
    	<a href="#" class="fl unselect">MANAGE MY BOOKING</a>
    	<a href="#" class="fl selected">PASSENGERS</a>
    <?php break; 

	case "rd_upsl": ?>
	    <a href="#" class="fl unselect">SEARCH</a>
        <a href="#" class="fl unselect">CALENDAR</a>
		<a href="#" class="fl selected">FLIGHTS</a>
	    <a href="#" class="fl caption2">PASSENGERS</a>
	    <a href="#" class="fl caption2">FLIGHT OPTIONS</a>
	    <a href="#" class="fl caption2">PAYMENT</a>
    	<a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a>    
	<?php break;
	
	case "rd_mo_cald": ?>
	    <a href="#" class="fl unselect">SEARCH</a>
        <a href="#" class="fl selected">CALENDAR</a>
		<a href="#" class="fl caption2">FLIGHTS</a>
	    <a href="#" class="fl caption2">PASSENGERS</a>
	    <a href="#" class="fl caption2">FLIGHT OPTIONS</a>
	    <a href="#" class="fl caption2">PAYMENT</a>
    	<a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a>    
	<?php break;

	default: ?>
	    <a href="#" class="fl unselect">SEARCH</a>
		<a href="#" class="fl selected">FLIGHTS</a>
	    <a href="#" class="fl caption2">PASSENGERS</a>
	    <a href="#" class="fl caption2">FLIGHT OPTIONS</a>
	    <a href="#" class="fl caption2">PAYMENT</a>
    	<a href="#" class="fl caption2"><small class="fr ico mail midst br"></small>CONFIRMATION</a>    
	<?php break;
	
	}?>
</nav>
<?php } ?>
<section class="main wrap">
	<div class="main fl">
    <?php include("aside_warning.php");?>