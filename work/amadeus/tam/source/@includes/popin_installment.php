<header class="caption4">
    <h3 class="h6">INSTALLMENT PLAN</h3>
</header>
<section>
	<table class="default boxw">
    <caption class="boxw caption h2">Plan 7x</caption>
    <thead>
	<tr>
    	<td colspan="4">Your selected installment plan:</td>
	</tr>         
    </thead>
    <tfoot class="caption3 boxw">
   	<tr>
    	<td colspan="4" class="h5 em tr">TOTAL &emsp; 460,50$</td>
	</tr>
    </tfoot>
    <tbody>
    <tr class="boxb">
    	<th class="grid7">Downpayment (1x):</th>
        <td>1 x <b>100,50</b></td>
        <td class="tc"><b>=</b></td>
        <td class="tr"><b>100,50 $</b></td>
    </tr>
    <tr class="boxb">
    	<th class="grid7">Installments (6x):</th>
        <td>6 x <b>60,00</b></td>
        <td class="tc"><b>=</b></td>
        <td class="tr"><b>360,00 $</b></td>
    </tr>    
	</tbody>
	</table>
</section>