        <fieldset class="br">
            <legend class="boxw caption h2 em3">
            	Passenger <?php echo $i; if($i==2) echo " (Child)";?>
                <output class="fr"><small class="ico user"></small> Passenger <?php echo $i;?> of 4 <small class="ico ok"></small></output>
            </legend>
            <fieldset class="boxw">
           		<p class="em2">Informe somente o ultimo sobrenombre e ainda o seguimiento familiar (Junior/Filho/Neto) caso possua. Examplo: "Jose Pererira da Silva Junior", insira apenas "Jose Silva Junior"</p>
            	<div class="wrap br">
                    <fieldset class="fl grid9">
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_title<?php echo $i?>">Title</label>
                            <span class="fl icoa select"><input type="text" class="input<?php if($i==1) { ?> em3<?php } ?>"  id="alpi_title<?php echo $i?>" size="10" placeholder="Select" /></span>
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_name<?php echo $i?>">Name</label>
                            <input type="text" class="fl input<?php if($i==1) { ?> em3<?php } ?>" id="alpi_name<?php echo $i?>" size="24" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_middle<?php echo $i?>">Middle Name</label>
                            <input type="text" class="fl input<?php if($i==1) { ?> em3<?php } ?>" id="alpi_middle<?php echo $i?>" size="24" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_last<?php echo $i?>">Last Name</label>
                            <input type="text" class="fl input<?php if($i==1) {?> em3<?php } ?>" id="alpi_last<?php echo $i?>" size="24" />
                        </p>
                        
                        <?php if($i==2) { ?>
                        <p class="wrap">
                            <span class="fl grid1">Date of birth</span>
                            <?php include("form_year.php");?>
                        </p>  
                        <?php } else { ?>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_ff<?php echo $i?>">F.F. Program</label>
                            <span class="fl icoa select"><input type="text" class="input<?php if($i==1) echo " em3"?>" id="alpi_ff<?php echo $i?>" size="10" placeholder="Select" /></span>
                            <span class="fl midst em1">(optional)</span>
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_number<?php echo $i?>">Number</label>
                            <input type="text" class="fl input<?php if($i==1) echo " em3"?>" id="alpi_number<?php echo $i?>" size="10" />
                            <span class="fl midst em1">(optional)</span>
                        </p>
                        <?php } ?>
                    </fieldset>
                    <?php include("form_advanced.php");?>
                </div>
                
            	<p><small class="ico <?php if($i==1) echo "minus2"; else echo "plus2";?>"></small> Show saved passengers</p>
      
                <div class="wrap boxg <?php if($i>1) echo " none";?>">
                    <button class="fl grid2x">
                        <small class="fl ico symbol br" title="Add">+</small>
                        <small class="fr ico close" title="Delete"></small>
                        <b class="fl grid0">Guillerme Goncalves</b>
                    </button>
                    <button class="fl grid2x">
                        <small class="fl ico symbol br" title="Add">+</small>
                        <small class="fr ico close" title="Delete"></small>
                        <b class="fl grid0">Geraldo Teixeiras</b>
                    </button>
                    <button class="fl grid2x">
                        <small class="fl ico symbol bem" title="Clear">x</small>
                        <b class="fl grid0">Clear form</b>
                    </button>
                    <button class="fl grid2x">
                        <small class="fl ico symbol bem1 br" title="Add">+</small>
                        <b class="fl grid0 midst">Add Passenger information</b>
                    </button>
                </div>
        	</fieldset> 
		</fieldset>
        <?php 
		if($i==1)
			include("form_contact_info.php");
		?>