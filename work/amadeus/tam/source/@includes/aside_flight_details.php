    <?php if(($page=="conf") || ($page=="bkgd")) { ?> 		
        <dl>
            <dt class="fl grid2">Flight number: </dt><dd>&nbsp;<small class="ico tam2"></small> <a href="#" class="em">JJ3120</a></dd>
	        <dt class="fl grid2">Operated by: </dt><dd>&nbsp;<b>TAM</b></dd>
            <dt class="fl grid2">Departure: </dt><dd>&nbsp;<b>Brasilia International Airport</b></dd>
            <dt class="fl grid2">Time: </dt><dd>&nbsp;<b>06:30</b> 
            <dt class="fl grid2">Arrival: </dt><dd>&nbsp;<b>Rio de Janeiro Airport</b></dd>
            <dt class="fl grid2">Time: </dt><dd>&nbsp;<b>08:30</b> <?php if($x===true) { ?><b class="em3">Mon 21 Jan</b><?php } ?></dd>
            <dt class="fl grid2">Flight Duration: </dt><dd>&nbsp;<b>2h 15m</b></dd>
            <dt class="fl grid2">Aircraft: </dt><dd>&nbsp;<b>Airbus A319</b></dd>
            <dt class="fl grid2">Cabin: </dt><dd>&nbsp;<b>Economy</b></dd>
            <dt class="fl grid2">Baggage Allowance: </dt><dd>&nbsp;<b>20 kg</b></dd>
        </dl>
    <?php } else { ?>
        <dl>
            <dt class="fl">Flight number: </dt><dd>&nbsp;<small class="ico tam2"></small> <a href="#" class="em">JJ3120</a></dd>
            <dt class="fl">Operated by: </dt><dd>&nbsp;<b>TAM</b></dd>
            <dt class="fl">Departure: </dt><dd>&nbsp;<b>Brasilia International Airport</b></dd>
            <dt class="fl">Time: </dt><dd>&nbsp;<b>06:30</b> 
            <dt class="fl">Arrival: </dt><dd>&nbsp;<b>Rio de Janeiro Airport</b></dd>
            <dt class="fl">Time: </dt><dd>&nbsp;<b>08:30</b> <?php if($x===true) { ?><b class="em3">Mon 21 Jan</b><?php } ?></dd>
            <?php if($page!="avai") { ?>
            <dt class="fl">Flight Duration: </dt><dd>&nbsp;<b>2h 15m</b></dd>
            <?php  } ?>         
            <dt class="fl">Aircraft: </dt><dd>&nbsp;<b>Airbus A319</b></dd>
            <dt class="fl">Cabin: </dt><dd>&nbsp;<b>Economy</b></dd>
            <dt class="fl">Baggage Allowance: </dt><dd>&nbsp;<b>20 kg</b></dd>
        </dl>
    <?php } ?>