        <?php include("header_price_details.php");?>
        <tfoot>
        <tr class="boxb">
            <th><b>INSTALLMENT FEE</b></th>
            <td colspan="5" class="tr"><b>Installments plan 5 (1 x 1.000 + 4 x 2.000)</b></td>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>9.000,00$</b></td>
        </tr>
        <tr class="caption3 boxb">
            <th colspan="6"><b>SUB TOTAL</b></th>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>34.000,00$</b></td>
        </tr>
        <tr class="caption3">
            <th colspan="6"><b>INSURANCE</b></th>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>55.000,00$</b></td>
        </tr>
        <tr class="caption3">
            <td colspan="8">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus nunc vel orci aliquet eget imperdiet nulla tempus. Donec at ligula sed odio elementum auctor</td>
        </tr>
        <tr class="caption3">
            <td colspan="8" class="h5 tr em"><b>TOTAL 15.000,00$</b><p class="em1">For all passengers including taxes</p></td>
        </tr>  
        <tr class="caption3">
            <td class="tr" colspan="8"><small class="ico visa"></small></td>
        </tr>         
        </tfoot>
        <tbody>
        <tr>
            <td class="h4" colspan="8">TRAVELLERS</td>
        </tr>                 
        <tr>
            <th>Travellers</th>
            <td class="tr">15.000,00</b></td>
            <td>&nbsp;</td>
            <td class="tr">20.000,00</td>
            <td>&nbsp;</td>
            <td class="tr">25.000,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>  
        <tr>
            <th class="em3">Discount</th>
            <td class="tr em3">-5.000,00</td>
            <td>&nbsp;</td>
            <td class="tr em3">-5.000,00</td>
            <td>&nbsp;</td>
            <td class="tr em3">-10,000.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>            
        <tr class="boxb">
            <th><b>Total travellers</b></th>
            <td class="tr"><b>10.000,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>15.000,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>15.000,00</b></td>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>40.000,00$</b></td>
        </tr>
        <tr>
            <td class="h4 toggle" colspan="8"><small class="ico sign"></small> AIRPORT TAXES</td>
        </tr> 
        <tr>
            <th>Taxa de Combustivel e Seguranca</th>
            <td class="tr">4.000,00</td>
            <td>&nbsp;</td>
            <td class="tr">4.000,00</td>
            <td>&nbsp;</td>
            <td class="tr">4.000,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Seguranca Aeroportuaria</th>
            <td class="tr">4.000,00</td>
            <td>&nbsp;</td>
            <td class="tr">4.000,00</td>
            <td>&nbsp;</td>
            <td class="tr">4.000,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>         
        <tr>
            <th class="boxb"><b>Total airport taxes</b></th>
            <td class="tr"><b>8.000,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>8.000,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>8.000,00</b></td>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>24.000,00$</b></td>
        </tr>
        <tr>
            <td class="h4" colspan="8">DU TAXES</td>
        </tr>
        <tr class="boxb">
            <th><b>Total du taxes</b></th>
            <td class="tr"><b>2.000,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>4.000,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>5.000,00</b></td>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>11.000,00$</b></td>
        </tr>
        <tr>
            <td class="h4 toggle minus" colspan="8"><small class="ico sign"></small> FLIGHT OPTIONS</td>
        </tr>
        <tr class="boxb">
            <th><b>CGH - SDU</b></th>
            <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
            <th class="top" rowspan="2">Excess luggages</th>
            <td class="tr"><span class="em1">4 pièces</span><br />50,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">4 pièces</span><br />50,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">4 pièces</span><br />50,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr> 
        <tr>
            <td class="tr"><span class="em1">2 pièces</span><br />25,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">2 pièces</span><br />25,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th class="top">Seat luggages</th>
            <td class="tr"><span class="em1">2 XL seats</span><br />50,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">1 exit seat</span><br />50,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr> 
        <tr class="boxb">
            <th><b>SDU - CGH</b></th>
            <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
            <th class="top" rowspan="2">Excess luggages</th>
            <td class="tr"><span class="em1">4 pièces</span><br />50,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">4 pièces</span><br />50,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">4 pièces</span><br />50,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr> 
        <tr>
            <td class="tr"><span class="em1">2 pièces</span><br />25,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">2 pièces</span><br />25,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th class="top">Seat luggages</th>
            <td class="tr"><span class="em1">2 XL seats</span><br />50,00</td>
            <td>&nbsp;</td>
            <td class="tr"><span class="em1">1 exit seat</span><br />50,00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>                       
        <tr class="boxb">
            <th><b>Total flight options</b></th>
            <td class="tr"><b>250,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>250,00</b></td>
            <td class="tc"><b>+</b></td>
            <td class="tr"><b>100,00</b></td>
            <td class="tc"><b>=</b></td>
            <td class="tr"><b>600,00$</b></td>
        </tr>         
        </tbody>
        </table>