<div class="boxl boxw wrap">
    <div class="wrap">
        <small class="fl ico bag"></small>
        <div class="fl grid4">
            
            <p class="em3"><b>Confirmed luggage:</b></p>
            <p>3 luggages <em class="fr em">1500,00</em></p>
            <p class="toggle"><small class="ico plus2"></small> <b>Show details</b></p>
            <dl class="none">
                <dt class="fl">Mr John Smith</dt><dd class="tr em">20kg</dd>
                <dt class="fl">Mss Carolina Smith</dt><dd class="tr em">30kg</dd>
                <dt class="fl">Mss Barbara Smith</dt><dd class="tr em">40kg</dd>
            </dl>
        </div>
    </div>
    <div class="wrap boxl">
        <small class="fl ico bag"></small>
        <div class="fl grid4">
            
            <p class="em3"><small title="Delete" class="fr wdk-popin toggle ico trash" data-wdk-popin-href="../popins/confirm.php" data-wdk-popin-width="640" data-wdk-popin-height="200"></small> <b>Added luggage:</b></p>
            <p>3 luggages <em class="fr em">1500,00</em></p>
            <p class="toggle"><small class="ico minus2"></small> <b>Hide all details</b></p>
            <dl>
                <dt class="fl">Mr John Smith</dt><dd class="tr em">20kg</dd>
                <dt class="fl">Mss Carolina Smith</dt><dd class="tr em">30kg</dd>
                <dt class="fl">Mss Barbara Smith</dt><dd class="tr em">40kg</dd>
            </dl>
        </div>
    </div>
</div>
<div class="boxl boxw wrap br">
    <small class="fl ico gseat br"></small>
    <div class="fl grid4">
        <small title="Delete" class="fr wdk-popin toggle ico trash" data-wdk-popin-href="../popins/confirm.php" data-wdk-popin-width="640" data-wdk-popin-height="200"></small>
    	<p>2 XL seats <em class="block em">1500,00</em></p>
    </div>
</div>