       			<fieldset class="fl formadv">
                <legend class="fl"><b><small class="ico arrow"></small> Advance Passenger Information</b></legend>
                	<p class="hr"><small class="ico warn tc"><b>!</b></small> Your selected flight requires further information.</p>               
                    <p class="wrap">
                	    <label class="fl grid1" for="alpi_passport4">Passport number</label>
                        <input type="text" class="fl input" id="alpi_passport4" size="15" />
                        <span class="fl midst em1">(optional)</span>
                    </p>
                    <p class="wrap">
                        <span class="fl grid1">Date of issue</span>
                        <?php include("form_year.php");?>
                    </p> 
                    <p class="wrap">
                        <label class="fl grid1" for="alpi_country4">Country of issue</label>
                        <span class="fl icoa select"><input type="text" class="input em" id="alpi_country4" size="26" placeholder="Select" /></span>
                    </p>
				</fieldset>