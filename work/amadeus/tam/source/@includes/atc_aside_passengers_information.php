<h3 class="toggle hr caption4 <?php if($page=="atc_upsl") echo 'minus'; else echo 'plus'; ?>"  data-wdk-toggle-target="#searchPaneInfo"><span class="fl sign"></span>PASSENGERS INFORMATION</h3>
<article id="searchPaneInfo" class="<?php if($page=="atc_upsl") echo 'none'; ?>" data-wdk-toggle-transition="none"> 
    <dl class="boxg br">
        <dt></dt><dd><b>Mr John Smith</b></dd>
        <dt class="fl">User: </dt><dd><b>BA 45875865</b></dd>
        <dt></dt><dd><b class="em2">Secure flight information</b></dd>
        <dt class="fl">Middle Name: </dt><dd><b>Joshua</b></dd>
        <dt class="fl">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
        <dt class="fl">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
        <dt class="fl">Redress Number: </dt><dd><b>123-456-789-789</b></dd>
    </dl>
	<dl class="boxg br">
        <dt><small class="ico gray2"></small></dt><dd><b class="em3">Contact information</b></dd>
        <dt class="fl">E-mail: </dt><dd><b>johnsmithname@gmail.com</b></dd>
        <dt class="fl">Mobile phone: </dt><dd><b>+33 0665845125</b></dd>
        <dt class="fl">Type: </dt><dd><b>TIM</b></dd>
        <dt class="fl">Country: </dt><dd><b>Brazil</b></dd>
    </dl>
    <dl class="boxg br">
        <dt></dt><dd><small class="ico minus2"></small> <b>Mss Carolina Smith</b></dd>
        <dt class="fl">User: </dt><dd><b>BA 45875865</b></dd>
        <dt></dt><dd><b class="em2">Secure flight information</b></dd>
        <dt class="fl">Middle Name: </dt><dd><b>Carolina</b></dd>
        <dt class="fl">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
        <dt class="fl">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
        <dt class="fl">Redress Number: </dt><dd><b>123-456-789-789</b></dd>
    </dl>
    <dl class="boxg br">
        <dt></dt><dd><small class="ico minus2"></small> <b>Mss Carolina Smith</b></dd>
        <dt class="fl">User: </dt><dd><b>BA 45875865</b></dd>
    </dl>            
    <dl class="boxg br">
        <dt></dt><dd><small class="ico minus2"></small> <b>Ms Yelena Valgdimir ovna Afansyeva</b></dd>
        <dt class="fl">User: </dt><dd><b>BA 45875865</b></dd>
        <dt></dt><dd><b class="em2">Secure flight information</b></dd>
        <dt class="fl">Middle Name: </dt><dd><b>Yelena</b></dd>
        <dt class="fl">Data de nacimento: </dt><dd><b>05-02-1985</b></dd>
        <dt class="fl">Passport Number.: </dt><dd><b>050-654-321-987</b></dd>
        <dt class="fl">Redress Number: </dt><dd><b>123-456-789-789</b></dd>
    </dl>
    <?php include("footer_next.php"); ?>
</article>