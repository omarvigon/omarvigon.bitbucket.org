<header class="caption4">
    <h3 class="h6">UPDATE MAIL AND PHONE</h3>
</header>
<section>
    <fieldset>
    <legend><b>Please enter your contact details</b></legend>
    <p class="wrap br">
      	<label class="fl grid2" for="popin_email">Email address</label>
        <input class="fl input" type="text" id="popin_email" size="54" />
	</p>
    <p class="wrap">
       	<label class="fl grid2" for="popin_tel">Telephone</label>
        <span class="fl icoa select"><input type="text" placeholder="Country" size="24" class="input"></span>
        <input class="fl input midst" type="text" placeholder="00000000000" size="11" id="popin_tel" /> 
        <span class="fl icoa select"><input type="text" placeholder="Home" size="10" class="input"></span>
	</p>
    </fieldset>
</section>
<footer class="caption4 default tr">         
    <button class="main right">SAVE</button>
</footer>