            <h4 class="br toggle minus" data-wdk-toggle-target="#totalOpened,#totalClosed">
            	<em id="totalClosed" data-wdk-toggle-transition="none" class="fr h4 em none">14,000,000 $</em>
                <small class="ico sign"></small> TOTAL
			</h4>

            <section class="boxg" id="totalOpened" data-wdk-toggle-transition="none">
            <dl>
			<?php if(($page=="atc_upsl") || ($page=="atc_purf")) { ?>
            	<dt class="fl">Total to collect</dt><dd class="tr"><b>2,000,000 $</b></dd>
            	<dt class="fl">Total to refund</dt><dd class="tr"><b>-1,000 $</b></dd>
            	<dt class="fl h3 em">TOTAL Balance</dt><dd class="tr"><span class="boxl h3 em">14,000,000 $</span></dd>
        	<?php } else { ?>
				<dt class="fl">Adult <em class="em1">x2</em></dt><dd class="tr">2,000,000 $</dd>
           	 	<dt class="fl"><b>Discount</b></dt><dd class="tr"><b>-1,000 $</b></dd>
            	<dt class="fl">Children <em class="em1">x1</em></dt><dd class="tr">3,000,000 $</dd>
            	<dt class="fl">Infant <em class="em1">x1</em></dt><dd class="tr">4,000,000 $</dd>
		
        		<?php if(($page=="alpi") || ($page=="fsr") || ($page=="purf")) { ?>
            	<dt class="fl toggle"><u class="em">Tax &amp; Service Fee</u> <?php include("aside_info.php");?></dt><dd class="tr">5,100,000 $</dd>
          		<?php } else { ?>
            	<dt class="fl">Tax &amp; Service Fee <?php include("aside_info.php");?></dt><dd class="boxb tr">5,000,000 $</dd>
          		<?php } ?> 
            
				<?php if(($page=="purf") || ($page=="fsr")) { ?>
            	<dt class="fl"><b>Total flight options</b></dt><dd class="boxb tr"><b>400,000 $</b></dd>    
           		<?php } ?>
			
				<?php if($page=="purf") { ?>
            	<dt class="fl"><a href="#" class="em">Installments</a></dt><dd class="tr">255,000 $</dd>
            	<dt class="fl">Insurance</dt><dd class="boxb tr">255,000 $</dd>    
       			<?php } ?>                     
		
        		<dt class="fl h5 em">Total</dt><dd class="h5 em tr">14,000,000 $</dd>
            </dl>
			<?php } ?>
                
            <?php include("aside_total_currency.php");?>
			</section>