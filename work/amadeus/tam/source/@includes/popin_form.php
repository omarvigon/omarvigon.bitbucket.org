<header class="caption4">
    <?php if($page=="send") { ?>
    <h3 class="h6">SEND THIS PAGE TO YOUR FRIEND</h3>
    <?php } else if($page=="email") { ?>
    <h3 class="h6">EMAIL THIS PAGE</h3>
    <?php } ?>
</header>
<section>
    <fieldset>
        <p class="wrap">
            <label class="fl grid0" for="popin_from">From</label>
            <input class="fl input" type="text" id="popin_from" size="36" />
        </p>
        <p class="wrap">
            <label class="fl grid0" for="popin_to">To</label>
            <input class="fl input" type="text" id="popin_to" size="36" />
        </p>
        <p class="wrap">
            <label class="fl grid0" for="popin_subject">Subject</label>
            <input class="fl input" type="text" id="popin_subject" size="36" />
        </p>
        <p class="wrap">
            <label class="fl grid0" for="popin_comment">Comment</label>
            <textarea class="fl input" cols="48" rows="8" id="popin_comment"></textarea>
        </p>
        <p>Note: Some note about aditional information</p>
    </fieldset>
<?php include("footer_next_cancel.php");?>