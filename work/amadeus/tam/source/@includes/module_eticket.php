       	<header class="caption4 hr">
            <h3 class="h6">E-TICKET</h3>
        </header>
        <article class="wrap">
            <p>Electronic tickets numbers are shown during the emission.</p>
			
            <div class="fr middle midst br">
              	<p><b class="em3">Outbound:</b></p>
                <p>From: Sao jose do Rio Prieto <b>(SJP)</b></p>
                <p>To: Santa Isabel Rio Negro <b>(IRZ)</b></p>
                <p><b class="em3">Inbound:</b></p>
                <p>From: Vitoria de conquista <b>(VCD)</b></p>
                <p>To: Sao jose do Rio Prieto <b>(SJP)</b></p>
			</div>
            <ul>
            	<li class="boxg middle br">
                	<b>Mr. Jhon Smith</b>
                    <b class="block pr em3">E-Ticket: 957-242-811-169-8</b>
                    <?php if($page=="atci" || $page=="bkgd") { ?>
                    <b class="em">No refundable</b>
					<?php } ?>
				</li>
                <li class="boxg middle br">
                	<b>Mr. Archivald Wolfeschlegelsteinhausenbergerdorff</b>
                    <b class="block pr em3">E-Ticket: 957-242-811-169-8</b>
                    <?php if($page=="atci" || $page=="bkgd") { ?>
                    <b>Refundable</b>
					<?php } ?>
				</li>
                <li class="boxg middle br">
                	<b>Mr. Gaddigoudar Shri Parvatagouda Chandanagouda</b>
                    <b class="block pr em3">E-Ticket: 957-242-811-169-8</b>
                    <?php if($page=="atci" || $page=="bkgd") { ?>
                    <b class="em">No refundable</b>
					<?php } ?>
				</li>
            </ul>               
        </article>