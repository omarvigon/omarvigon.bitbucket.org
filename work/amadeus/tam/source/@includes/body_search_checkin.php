<?php if($page=="wds_srch") { ?>
    <fieldset>
        <legend class="fl boxw caption h2 em3 br">1. Select the airline you want to do the chek-in</legend>
        <fieldset class="boxw">
            <p>Search your flight. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <p class="wrap br">
                <label class="fl"><input type="radio" class="fl" name="radio1" /> TAM</label>
                <label class="fl column2"><input type="radio" class="fl" name="radio1" checked="checked" /> LAN</label>
            </p>
        </fieldset>
    </fieldset>
    <fieldset>
        <legend class="fl boxw caption h2 em3 br">2. Select the trip date</legend>
        <fieldset class="boxw wrap">
            <label class="fl grid2" for="search_date">Date: </label>
            <input type="text" class="fl input" id="search_date" size="12" />
            <small class="ico cald midst"></small>
        </fieldset>
    </fieldset>
    <fieldset>
        <legend class="fl boxw caption h2 em3 br">3. Select flight information</legend>
        <fieldset class="boxw">
            <p class="wrap">
                <label class="fl grid2" for="search_local">Locatization: </label>
                <input type="text" class="fl input" id="search_local" size="16" />
            </p>
            <p class="em1">Or</p>
            <p class="wrap">
                <label class="fl grid2" for="search_eticket">E-ticket number: </label>
                <input type="text" class="fl input" id="search_eticket" size="16" />
            </p>
            <p class="br"><label><input type="checkbox" class="fl" /> I agree with the <a href="#" class="em">Terms &amp; Conditions</a></label></p>
        </fieldset>
    </fieldset>
    <footer class="caption4 tr br">			             
        <button class="main right">SEARCH</button>
    </footer>
<?php } else { ?>
    <fieldset>
        <legend class="fl br mr">Search your flight. Lorem ipsum dolor sit amet consectetur adipiscing elit.</legend>
        <fieldset class="wrap">
            <label class="fl"><input type="radio" class="fl" name="radio1" /> TAM</label>
            <label class="fl column2"><input type="radio" class="fl" name="radio1" /> LAN</label>
        </fieldset>
        <p class="wrap br">
            <label class="fl grid2" for="search_date">Date: </label>
            <input type="text" class="fl input"  id="search_date" size="12" />
            <small class="ico cald midst"></small>
        </p>
        <p class="wrap">
            <label class="fl grid2" for="search_local">Locatization: </label>
            <input type="text" class="fl input" id="search_local" size="16" />
        </p>
        <p class="em1">Or</p>
        <p class="wrap">
            <label class="fl grid2" for="search_eticket">E-ticket number: </label>
            <input type="text" class="fl input" id="search_eticket" size="16" />
        </p>
        <p class="hr"><label><input type="checkbox" class="fl" /> I agree with the <a href="#" class="em">Terms &amp; Conditions</a></label></p>
        <footer class="tr br">
        	<button class="main right">SEARCH</button>
		</footer>
    </fieldset>
<?php } ?>