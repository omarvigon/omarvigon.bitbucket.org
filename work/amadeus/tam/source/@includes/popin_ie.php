  	<div class="popin-ie">
		<h1>You are using an insecure and outdated browser!</h1>
		<p>We no longer support Internet Explorer 6. We recommend an upgrade to a newer version of Internet Explorer or another web browser. A list of the most popular browsers can be found below.</p>
		<p>Please upgrade to a recent browser:</p>
		<ul>
			<li><a href="http://www.mozilla.org/fr/download/?product=firefox-11.0&amp;os=win"><span class="ico ff-browser"></span>Firefox</a></li>
			<li><a href="https://www.google.com/chrome/eula.html"><span class="ico chrome-browser"></span>Chrome</a></li>
			<li><a href="http://www.apple.com/fr/safari/download/"><span class="ico safari-browser"></span>Safari</a></li>
			<li><a href="http://view.atdmt.com/action/mrtiex_FY12IE9StaPrdIE8ProductpageforXPFNL_1?href=http://download.microsoft.com/download/B/B/F/BBFCC870-E61D-4515-B81F-69D4D2F2B23B/IE8-WindowsXP-x86-FRA.exe"><span class="ico ie-browser"></span>Internet Explorer</a></li>
		</ul>
	</div>