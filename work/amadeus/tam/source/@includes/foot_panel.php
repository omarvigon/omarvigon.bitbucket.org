<fieldset id="panel" class="boxg grid2" style="position:fixed;top:5px;right:5px;z-index:3">       
    <legend class="fl h4 toggle minus"><small class="ico sign"></small> PANEL</legend>
    <div>
        <label class="block" id="_warning"><input type="checkbox" /> Warning messages</label>
        <label class="block" id="_vcp"><input type="checkbox" /> Warning vcp</label>
        <label class="block" id="_countries"><input type="checkbox" /> Tooltip countries</label>
        <label class="block" id="_family"><input type="checkbox" /> Tooltip family</label>
        <label class="block" id="_flif"><input type="checkbox" /> Tooltip flif</label>
        <label class="block" id="_seatxl" style="text-decoration:line-through"><input type="checkbox" /> Tooltip seatxl</label>
        <label class="block" id="_taxes"><input type="checkbox" /> Tooltip taxes</label>
        <label class="block" id="_interstice"><input type="checkbox" /> Interstice</label>
        <label class="block br" ><input type="radio" name="panel_log" value="loginLoggedState" />Logged</label>
        <label class="block" ><input type="radio" name="panel_log" value="loginNotLoggedState" />Not Logged</label>
        <label class="block mr" ><input type="radio" name="panel_log" value="loginKnownNotLoggedState" />Known Not Logged</label>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/seat.php" data-wdk-popin-width="960" data-wdk-popin-height="960">Popin SEAT</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/seat_ro.php" data-wdk-popin-width="700" data-wdk-popin-height="840">Popin SEAT-RO</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../booking/co_cald.php" data-wdk-popin-width="700" data-wdk-popin-height="600">Popin CO-CALD</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../booking/it_cald.php" data-wdk-popin-width="800" data-wdk-popin-height="600">Popin IT-CALD</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../booking/od_cald.php" data-wdk-popin-width="800" data-wdk-popin-height="840">Popin OD-CALD</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../redemption/rd_cald.php" data-wdk-popin-width="960" data-wdk-popin-height="840">Popin RD-CALD</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/confirm.php" data-wdk-popin-width="640" data-wdk-popin-height="240">Popin confirm</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/email.php" data-wdk-popin-width="640" data-wdk-popin-height="480">Popin email</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/inst.php" data-wdk-popin-width="480" data-wdk-popin-height="480">Popin installment</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/insu.php" data-wdk-popin-width="800" data-wdk-popin-height="260">Popin insurance</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/password.php" data-wdk-popin-width="640" data-wdk-popin-height="260">Popin password</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/send.php" data-wdk-popin-width="640" data-wdk-popin-height="400">Popin send page</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/sext.php" data-wdk-popin-width="640" data-wdk-popin-height="200">Popin warning</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/taxb.php" data-wdk-popin-width="640" data-wdk-popin-height="520">Popin taxes</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/wait.php" data-wdk-popin-width="640" data-wdk-popin-height="160">Popin wait</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/update_phone.php" data-wdk-popin-width="640" data-wdk-popin-height="400">Popin update phone</em>
        <em class="wdk-popin toggle block" data-wdk-popin-href="../popins/atc_ntp.php" data-wdk-popin-width="900" data-wdk-popin-height="900">Popin ntp</em>
    </div>
</fieldset>
<script>
function toggle(obj,target)
{
	if(obj.find("input:checked").length>0)
		target.removeClass("none");
	else
		target.addClass("none");
}
$("#panel legend").click( function(){

	$(this).next().toggleClass("none");
});
$("#_warning").click( function(){

	toggle($(this),$("div.warning"));
});
$("#_vcp").click( function(){

	toggle($(this),$("tr.vcp"));
});
$("#_countries,#_family,#_flif,#_taxes").click( function(){

	toggle($(this),$("#tooltip"+$(this).attr("id")));
});
$("#_interstice").click( function(){

	$("div.main").toggleClass("interstice");
});
$("#panel label:contains('Logged')").click( function(){

	var article = $("#loginDiv");
	article.find("article").addClass("none");
	article.find( "#"+$(this).find("input").val() ).removeClass("none");
});
</script>