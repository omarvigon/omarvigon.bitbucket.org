            <?php include("header_price_details.php");?>
            <tfoot>
                <tr class="caption3 boxb">
                    <th><b>Installment fee</b></th>
                        <td class="tr" colspan="6"><b>Installments plan 5 (1 x 1.000 + 4 x 2.000)</b></td>
                    <td class="tr em"><b>15.000,00$</b></td>
                </tr>
                <tr class="caption3 boxb">
                    <th colspan="7"><b>Total paid</b></th>
                    <td class="tr em"><b>34.000,00$</b></td>
                </tr>
                <tr class="caption3 boxb">
                    <th colspan="7"><b>Total refunded</b></th>
                    <td class="tr em3"><b>-55.000,00$</b></td>
                </tr>
                <tr class="caption3">
                    <td colspan="5"><b class="h5 em3">TOTAL Balance</b></td>
                    <td class="h5 tr em3" colspan="3"><b>-15.000,00$</b></td>
                </tr>  
                <tr class="caption3">
                    <td class="tr" colspan="8"><small class="ico visa"></small></td>
                </tr>         
            </tfoot>  
            <tbody>
                <tr>
                    <th colspan="8" class="h1 em"><b>NEW PRICE</b></th>
                </tr>  
                <tr>
                    <th colspan="8" class="h4">TRAVELLERS</th>
                </tr>  
                <tr>
                    <th><b>Total travellers</b></th>
                    <td class="tr"><b>10.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>20.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>15.000,00</b></td>
                    <td class="tc">=</td>
                    <td class="tr"><b>45.000,00$</b></td>
                </tr> 
                <tr>
                    <td class="h4 toggle minus" colspan="8"><small class="ico sign"></small> TAXES</td>
                </tr>  
                <tr>
                    <th>Taxa de Combustivel e Seguranca</th>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>Seguranca Aeroportuaria</th>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>Service Taxes</th>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td class="tr">4.000,00</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th><b>Total taxes</b></th>
                    <td class="tr"><b>12.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>12.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>12.000,00</b></td>
                    <td class="tc">=</td>
                    <td class="tr"><b>46.000,00$</b></td>
                </tr> 
                <tr>
                    <th colspan="6"><b>Rebooking fee</b></th>
                    <td class="tc"><b>=</b></td>
                    <td class="tr boxb"><b>10.000,00$</b></td>
                </tr>
                <tr class="boxb">
                    <th class="h4verd em"><b>Total New price</b></th>
                    <td class="tr em"><b>22.000,00</b></td>
                    <td class="tc em">+</td>
                    <td class="tr em"><b>32.000,00</b></td>
                    <td class="tc em">+</td>
                    <td class="tr em"><b>27.000,00</b></td>
                    <td class="tc em">=</td>
                    <td class="tr em"><b>81.000,00$</b></td>
                </tr> 
                <tr>
                    <th colspan="8" class="h1 em3">ORIGINAL PRICE</th>
                </tr>
                <tr>
                    <th><b>Total travellers</b></th>
                    <td class="tr"><b>10.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>7.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>3.000,00</b></td>
                    <td class="tc">=</td>
                    <td class="tr"><b>20.000,00$</b></td>
                </tr>   
                <tr>
                    <th><b>Total taxes</b></th>
                    <td class="tr"><b>12.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>12.000,00</b></td>
                    <td class="tc">+</td>
                    <td class="tr"><b>12.000,00</b></td>
                    <td class="tc">=</td>
                    <td class="tr boxb"><b>46.000,00$</b></td>
                </tr> 
                <tr class="boxb">
                    <th class="h4verd em3"><b>Total Original price</b></th>
                    <td class="tr em3"><b>10.000,00</b></td>
                    <td class="tc em3">+</td>
                    <td class="tr em3"><b>11.000,00</b></td>
                    <td class="tc em3">+</td>
                    <td class="tr em3"><b>12.000,00</b></td>
                    <td class="tc em3">=</td>
                    <td class="tr em3"><b>33.000,00$</b></td>
                </tr> 
            </tbody>
        </table>
