<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title><?php echo $page;?> - TAM12</title>
<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/main.css" />
<?php if($page=="atc_cald" || $page=="atc_conf" || $page=="atc_purf" || $page=="atc_srch" || $page=="atc_upsl" || $page=="atc_ntp" ) { ?>
	<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/atc.css" />
<?php } ?>
<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/jquery-ui/jquery-ui.css" />
<!--[if lt IE 10]><link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/ie.css"/><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/ie9.css"/><![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/pie.css"/>
	<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/ie8.css"/>
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/pie.css"/>
	<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/ie7.css"/>
<![endif]-->
<!--[if IE 6]><link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/ie6.css"/><![endif]-->

<?php if(($page=="conf") || ($page=="bkgd")) { ?>
<link rel="stylesheet" href="../@WDS_STATIC_FILES_PATH@/css/print.css" media="print" />
<?php } ?>
<script src="../@WDS_STATIC_FILES_PATH@/js/wdk.js"></script>
<script src="../@WDS_STATIC_FILES_PATH@/js/wdk_config.js"></script>
<!--[if lt IE 9]>
<script src="../@WDS_STATIC_FILES_PATH@/js/html5.js"></script>
<script src="../@WDS_STATIC_FILES_PATH@/js/IE9.js"></script>
<![endif]-->
</head>