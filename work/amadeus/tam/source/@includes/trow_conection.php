    <tr>
        <th class="rightF bottomF"><b>08:30</b> CGH</th>
        <th class="rightF bottomF"><b>10:55</b> GIG</th>
        <th class="rightF bottomF"><small class="ico tam2"></small> <u class="em">JJ5122</u> <small class="op block em1 tr">Operated by TAM</small></th>
        <th class="bottomF">2h15m</th>
        <td rowspan="2" class="rightE tc lowest em3"><small class="fr"><small class="ico cheap"></small></small><b>450</b></td>
        <td rowspan="2" class="rightE tc">550</td>
        <td rowspan="2" class="rightE tc">650</td>
        <td rowspan="2" class="rightE tc">750</td>
        <td rowspan="2" class="tc">850</td>
    </tr>
    <tr>
        <th class="rightF"><b>10:30</b> CGH</th>
        <th class="rightF"><b>12:55</b> GIG <small class="em">+1</small></th>
        <th class="rightF"><small class="ico flight2 midst"></small> <u class="em">JJ5122</u> <small class="op block em1 tr">Operated by American Airlines</small></th>
        <th>2h15m</th>
    </tr>