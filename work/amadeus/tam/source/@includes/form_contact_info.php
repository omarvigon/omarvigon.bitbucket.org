        <fieldset class="article caption grid20">
            <legend class="fl"><b>Your contact information</b></legend>
            <fieldset>
            <p class="wrap br">
            	<label class="fl grid1" for="alpi_email1">Email address</label>
                <input type="text" class="fl input" id="alpi_email1" size="54" />
			</p>
            <p class="wrap">
            	<label class="fl grid1" for="alpi_tel1">Telephone</label>
                <span class="fl icoa select"><input type="text" class="input" size="24" placeholder="Country" /></span>
                <input type="text" class="fl input midst" id="alpi_tel1" size="11" placeholder="00000000000" /> 
            <?php if($page=="pnrs") { ?>
				<span class="fl icoa select"><input type="text" class="input inactive" size="9" readonly="readonly"  /></span>
            <?php } else { ?>
                <span class="fl icoa select"><input type="text" class="input" size="9" placeholder="Home" /></span>
			<?php } ?>
            </p>
            <p class="em1"><span class="fl grid1">&nbsp;</span>(Landline phone won't be able to receive SMS message)</p>
            </fieldset>
        </fieldset>