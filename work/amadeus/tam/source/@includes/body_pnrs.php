		<?php include("module_share_trip.php");?>
        <h1>Step 2 - Specify your travelers</h1>        
        <p>Please review the fare, fill the passenger details and ensure names entered below are the same as displayed on your passport.</p>
		<?php
		include("header_passengers.php");
        
		for($i=1;$i<=3;$i++)
			include("form_pnrs.php");
		
		include("form_pnrs_infant.php");
		include("module_meal_reservation.php");
		include("module_other_request_pnrs.php");
		include("footer_prev_next.php");
		?>
    </div>
    <aside class="main fr">
   		<?php
        include("aside_flight_outbound.php");
		include("aside_flight_inbound.php");
		include("aside_total_fsrs.php");
		include("footer_next.php");
		?>
        </article>  
    </aside>