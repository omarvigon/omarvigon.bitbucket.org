		<?php include("module_share_trip.php");?>
        <h1>Step 1 - Manage my booking</h1>        
        <p>Thank you, your booking is completed!</p>
        <?php
        include("module_booking_reference.php");
		include("module_eticket.php");
		include("module_figure.php");
		include("module_passengers.php");
		include("module_flight_summary.php");
		include("header_seat_reservation.php");
		include("module_seat_reservation.php");
		include("module_meal_reservation.php");
		include("module_luggage.php");
		include("module_other_request.php");
		include("module_price_recap.php");
		include("module_more_services.php");
		include("module_insurance_teaser.php");
		?>
        <footer class="caption4 tr hr">          
            <button class="main right">CHANGE MY BOOKING</button>
        </footer>
    </div>
    <aside class="main fr">
   		<?php
		include("aside_booking.php");
		include("aside_web_chekin.php");
		include("aside_tam_fidelidad.php");
		include("aside_usefull_doc.php");
		?>
    </aside>