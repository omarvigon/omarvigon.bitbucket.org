        <header class="caption4 hr">
            <small class="fl ico luggage"></small>
            <h3 class="h6">&nbsp;LUGGAGE</h3>
        </header>
        
        <table class="default boxw br">
        <tbody>
    	<tr class="caption boxw">
        	<td colspan="2">
            <?php include("tdefault_outbound.php");?>
			</td>
		</tr>
        <tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2 grid1">Weight</td>
        </tr>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td>40kg</td>
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td>20kg</td>
        </tr>
        <tr>
            <th>Mr. Steingrimur J. Sigfusson <em class="em3">(Infant)</em></th>
            <td>n/a</td>
        </tr>     
        </tbody>

		<tbody>
        <tr class="caption boxw">
        	<td colspan="2">
           		<?php include("tdefault_inbound.php");?>
			</td>
        </tr>                                                 
        <tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2 grid1">Weight</td>
        </tr>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td>40kg</td>
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td>20kg</td>
        </tr>
        <tr>
            <th>Mr. Steingrimur J. Sigfusson <em class="em3">(Infant)</em></th>
            <td>n/a</td>
        </tr>                               
        </tbody>
        </table>