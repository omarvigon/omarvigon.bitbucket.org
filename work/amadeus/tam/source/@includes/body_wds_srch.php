		<h1>Step 1 - Search</h1>
        <p>Localized text.</p>
        <?php if($page!="atc_srch") { ?>
            <section class="list br">
    			<ul class="fl selected" id="onlineSearchButton">
                    <li class="h6"><b>ONLINE SEARCH</b></li>
                    <li><small class="ico slope"></small></li>
                </ul>
                <ul class="fl unselect" id="manageBookingButton">
                    <li class="h6"><b>MANAGE BOOKING</b></li>
                    <li><small class="none ico slope"></small></li>
                </ul>
                <ul class="fl unselect" id="onlineCheckinButton">
                    <li class="h6"><b>ONLINE CHECKIN</b></li>
                    <li><small class="none ico slope"></small></li>
                </ul>
            </section>
        <?php } ?>
        
        <header class="caption4" id="onlineSearch">
            <h3 class="h6">ONLINE SEARCH</h3>
        </header>
        <div>
            <fieldset>
                <legend class="fl boxw caption h2 em3 br">1. Select the type of trip</legend>
                <fieldset class="boxw">
                <?php include("form_trip.php");?>
                </fieldset>
            </fieldset>
            
            <fieldset>
                <legend class="fl boxw caption h2 em3 br">Multi-destination</legend>
                <fieldset class="boxw">
                	<p class="wrap br">
                        <label class="fl grid-1" for="search_from">From:</label>
                        <span class="fl icoa select"><input type="text" class="input grid3" id="search_from" size="24"  /></span>
                        <label class="fl grid-1 column2" for="search_to">To:</label>
                        <span class="fl icoa select"><input type="text" class="input grid3" id="search_to" size="24" /></span>
                    </p>
                    <p class="wrap br">
                    	<label class="fl grid-1" for="search_from">From:</label>
                        <span class="fl icoa select"><input type="text" class="input grid3" id="search_from" size="24"  /></span>
                        <label class="fl grid-1 column2" for="search_to">To:</label>
                        <span class="fl icoa select"><input type="text" class="input grid3" id="search_to" size="24" /></span>
                    </p>
                    <p class="wrap br">
                        <label class="fl grid-1" for="search_from">From:</label>
                        <span class="fl icoa select"><input type="text" class="input grid3" id="search_from" size="24"  /></span>
                        <label class="fl grid-1 column2" for="search_to">To:</label>
                        <span class="fl icoa select"><input type="text" class="input grid3" id="search_to" size="24" /></span>
                        <button class="fl column2">Remove flight</button>
                    </p>	
                    <p class="wrap">
                     	<label class="fl grid-1">&nbsp;</label>
                     	<button class="fl">Add flight</button>
                    </p>
                     
					<div class="boxl br">
                        <fieldset class="wrap">
                            <p class="fl">
                                <label for="search_adults">Adults:<em class="block em1">&nbsp;</em></label>
                                <span class="fl icoa select"><input type="text" class="input" id="search_adults" size="5" /></span>
                            </p>
                            <p class="fl midst">
                                <label for="search_children">Children:<em class="block em1">(2-11 years)</em></label>
                                <span class="fl icoa select"><input type="text" class="input" id="search_children" size="5" /></span>
                            </p>
                            <p class="fl">
                                <label for="search_infants">Infants:<em class="block em1">(0-23 months)</em></label>
                                <span class="fl icoa select"><input type="text" class="input" id="search_infants" size="5" /></span>
                            </p>
                        </fieldset>
                        <p><label><input type="checkbox" class="fl" /> Are you flexible with dates?</label></p>
                		<?php include("form_cabin.php");?>
                	</div>
                </fieldset>
            </fieldset>
            
            <fieldset>
                <legend class="fl boxw caption h2 em3 br">2. Select origin and destination</legend>
                <fieldset class="boxw">
                <?php include("form_from_to.php");?>
                </fieldset>
            </fieldset>
            <fieldset>
                <legend class="fl boxw caption h2 em3 br">3. Select the trip dates</legend>
                <fieldset class="boxw">
                <?php include("form_dates.php");?>
                </fieldset>
            </fieldset>
            
            <?php if($page!=="atc_srch") { ?>
            <fieldset>
                <legend class="fl boxw caption h2 em3 br">4. Select number of passengers</legend>
                <fieldset class="boxw">
                <?php
                include("form_passengers.php");
                include("form_cabin.php");
                include("form_promocode.php");
    			?>
                </fieldset>
            </fieldset>
            <?php } ?>
          
            <footer class="caption4 tr br">			             
        		<button class="main right">SEARCH</button>
			</footer>
        </div>
        
        <header class="caption4 none" id="manageBooking">
            <h3 class="h6">MANAGE BOOKING</h3>
        </header>
        <div class="none">
			<?php include("body_search_manage.php");?>
        </div>
        
        <header class="caption4 none" id="onlineCheckin">
            <h3 class="h6">ONLINE CHECKIN</h3>
        </header>
        <div class="none">
			<?php include("body_search_checkin.php");?>
        </div>        
    </div>
    <aside class="main fr">
		<h3 class="caption4 hr">PLACEHOLDER</h3>
		<article>        	
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
            <p>placeholder</p>
		</article>
    </aside>