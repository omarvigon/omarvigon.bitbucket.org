        <aside class="fr menu column2 pr tr">
            <?php if(($page!="atc_upsl") && ($page!="atc_conf") && ($page!="atc_purf") && ($page!="avai")) { ?>
                <button class="toggle grid2" data-wdk-toggle-target="#menuShare" data-wdk-toggle-trigger-opened-class="opened" data-wdk-toggle-trigger-closed-class="closed">
                    <small class="fl ico share"></small> Share this trip
                </button>
            <?php } ?>
            <ul id="menuShare" data-wdk-toggle-transition="none" class="none">
                <li><small class="fl midst ico facebook"></small> Facebook</li>
                <li><small class="fl midst ico twitter"></small> Twitter</li>
                <li><small class="fl midst ico google"></small> Bookmark</li>
                <li><small class="fl midst ico mail"></small> Email</li>
            </ul>
            <?php if(($page=="conf") || ($page=="bkgd")) { ?>
            <p><button>Print</button> <button>Add to calendar</button> <button>Send by email</button></p>
            <?php } ?>
        </aside>