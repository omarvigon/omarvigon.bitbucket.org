	<?php include("header_seat_reservation.php");?>
	<section>
        <table class="default boxw">
        <caption class="caption boxw">
        	<?php include("tdefault_outbound.php");?>
            <output class="fr"><small class="fl ico gseat"></small> Seat selected 4 of 4 <small class="ico ok"></small></output>
        </caption>
        <thead>
        <tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2">Seat</td>
            <td class="h2">Price</td>
            <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td><b>23F</b></td>
            <td>22$</td>
            <td class="tc" rowspan="2"><button class="h6 grid0">Change seats</button></td>
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td><b>24F</b> XL seat</td>
            <td>22$</td>
        </tr>
        </tbody>   
		</table>
        
        <header class="caption boxw br">
        	<?php include("tdefault_inbound.php");?>
        	<output class="fr"><small class="fl ico gseat"></small> Seat selected 0 of 4 <small class="ico okno"></small></output>
        </header>
        <section class="wrap">
        	<fieldset class="fl grid9">
            	<label class="block wrap seat br">
                	<input type="radio" class="fl" name="radio1" />
                    <small class="fl h6 ico seat2 tc"><b>1</b></small>
                    <p class="fl grid5 midst">Mr. Sigmundur David Gunnlaugsson <span class="block"><b>Seat:</b> 23F</span> <span class="block"><b>Price:</b> 22$</span></p>
				</label>
                <label class="block wrap seat br">
                	<input type="radio" class="fl" name="radio1" />
                    <small class="fl h6 ico seat2x tc"><b>2</b></small>
                    <p class="fl grid5 midst">Mrs. Jonina Ros Guomundsdottir <span class="block"><b>Seat:</b> 24F</span> <span class="block"><b>Price:</b> 22$</span> <span class="block"><b class="em3">Travelling with infant:</b> Yelena Valgdimir</span> </p>
				</label>
            	
                <div class="boxg hr">
                    <h2>Legal notice</h2>
                    <p>Please note the following item(s) and check to indicate that you accept in order to continue.</p>
                    <p>I agree with the purchase information above, <a href="#" class="em">Agreement Conditions</a> and <a href="#" class="em">User Agreement</a>.</p>
                </div>
                
                <p class="br"><button class="fr h6 grid0">Next</button> <small class="ico trash"></small> <a href="#" class="em">Reset seat</a></p>
            </fieldset>
            <div class="fr">
            	<?php include("popin_seat_table.php");?>
            </div>
        </section>
        <?php
        include("popin_seat_legend.php");
		include("footer_next_cancel.php");
		?>