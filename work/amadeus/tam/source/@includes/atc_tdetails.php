    <tr class="details dotted first">
        <td colspan="2" rowspan="3" class="rowspan">
            <button class="short block midst br"><small class="fl ico zoom"></small> Compare fare</button>
		</td>
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_tam.png" class="fl"  alt="" />&nbsp;<b class="h6">Fidelidade Points</b></td>
        <td class="tc f2b">50% miles</td>
        <td class="tc f2b f2c">75% miles</td>
        <td class="tc">85% miles</td>
        <td class="tc">100% miles</td>
        <td class="right tc">100% miles</td>
    </tr>
    <tr class="details dotted">
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_bag.png" class="fl" alt="" />&nbsp;<b class="h6">Baggage allowance</b> <small class="ico info"></small></td>
        <td class="tc f2b">25kg</td>
        <td class="tc f2b f2c">30kg</td>
        <td class="tc">35kg</td>
        <td class="tc">40kg</td>
        <td class="right tc">50kg</td>
    </tr>
    <tr class="details dotted">
        <td class="right" colspan="2"><img src="../@WDS_STATIC_FILES_PATH@/img/icon_man.png" class="fl" alt="" />&nbsp;<b class="h6">Priority Services</b></td>
        <td class="tc f2b">No</td>
        <td class="tc f2b f2c">No</td>
        <td class="tc">No</td>
        <td class="tc">Check-in, boarding, baggage</td>
        <td class="right tc">Check-in, boarding, baggage</td>
    </tr>