        <table class="default boxw br">
        <thead>
        <tr>
        	<td class="caption boxw br">
        	<?php include("tdefault_outbound.php");?>
			</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>No seat selection available on this flight</th>          
        </tr>
        </tbody>
        </table>
        
        <table class="default boxw br">
        <tfoot>
        <tr class="caption boxw">
		<?php if(($page=="purf") || ($page=="bkgd") || ($page=="atci")) { ?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="h5 em tr"><b>TOTAL 110,00</b></td>
    	<?php } else if($page=="conf") { ?>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>                   
		<?php } else { ?>
        	<td><button>Cancel</button></td>
            <td>&nbsp;</td>
            <td class="h5 em tr"><b>TOTAL 110,00</b></td>
    	<?php } ?>
        </tr>
        </tfoot>
        
        <tbody>
        <tr>
        	<td class="caption boxw br" colspan="3">
        	<?php include("tdefault_outbound.php");?>
			</td>
        </tr>
		<tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2 grid1">Seat</td>
            <td class="h2"><?php if($page!="conf") { ?>Price<?php } ?></td>
        </tr>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td><b>23F</b></td>
            <td>&nbsp;</td>        
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td><b>24F</b> Exit seat</td>
            <td>
		<?php if($page=="fsrs") { ?>
           	Already paid
		<?php } else if($page=="conf") { ?>
        	&nbsp;
		<?php } else { ?>125<?php } ?>
			</td>
        </tr>
        
        <?php if(($page!="conf") && ($page!="atci")) { ?>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="tr"><button>Change seats</button></td>
        </tr>
        <?php } ?>  
        </tbody>

		<tbody>
        <tr>
        	<td class="caption boxw br" colspan="3">
        	<?php include("tdefault_inbound.php");?>
			</td>
        </tr>
        <tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2 grid1">Seat</td>
            <td class="h2"><?php if($page!="conf") { ?>Price<?php } ?></td>
        </tr>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td><b>23F</b></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td><b>24F</b> Exit seat</td>
            <td>
		<?php if($page=="fsrs") { ?>
           	Already paid
		<?php } else if($page=="conf") { ?>
        	&nbsp;
		<?php } else { ?>125<?php } ?>
			</td>
        </tr>
        
        <?php if(($page!="conf") && ($page!="atci")) { ?>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="tr"><button>Change seats</button></td>
        </tr>
        <?php } ?>
        </tbody>       
        </table>