                    <p class="wrap br">
                        <label class="fl grid2" for="purf_name">Card holder name</label>
                        <input class="fl input" id="purf_name" type="text" size="26" />
                    </p>
                    <p class="wrap">
                        <label class="fl grid2" for="purf_card">Card Number</label>
                        <span class="fl">
                            <input type="text" class="input" id="purf_card" size="26" value=" 00000 00000 00000 00000 " />
                            <small class="card">&nbsp;</small><small class="card">&nbsp;</small><small class="card">&nbsp;</small>
                        </span>
                    </p>
                    <p class="wrap">
                        <span class="fl grid2">Expiry date</span>
                        <label class="fl icoa select"><input type="text" class="input" size="6" placeholder="MM" /></label>
                        <label class="fl icoa select midst"><input type="text" class="input" size="6" placeholder="YYYY" /></label>
                    </p>
                    <p class="wrap boxl br">
                        <label class="block br" for="purf_inst"><b>Installment plan</b></label>
                        <span class="fl icoa select"><input type="text" class="input" id="purf_inst" size="72" /></span>
                    </p>
                    <p class="toggle"><u class="em">More info</u> <small class="ico info"></small></p>
                    <p class="wrap boxl br">
                        <label class="fl grid2 br"><b>Security Code</b><input type="text" class="input block" size="6" maxlength="3" /></label>
                        <img class="fl midst br" src="../@WDS_STATIC_FILES_PATH@/img/purf_security.png" alt="Security Code" />
                        <span class="fr grid5 em1 br">Your credit card security number is required. It is composed of 3 numbers that appear on the back of your credit card</span>
                    </p>

                    <fieldset class="br">
                        <legend class="h6 boxl"><b>Credit card billing address</b></legend>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_street1">Street adress 1</label>
                            <input class="fl input" id="purf_street1" type="text"  size="26" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_street2">Street adress 2</label>
                            <input class="fl input" id="purf_street2" type="text" size="26" />
                            <span class="fl midst em1">(optional)</span>
                        </p>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_postal">Postal code</label>
                            <input class="fl input" id="purf_postal" type="text"  size="26" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_city">City</label>
                            <input class="fl input" id="purf_city" type="text" size="26" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_state">State/province</label>
                            <input class="fl input" id="purf_state" type="text" size="26" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_country">Country</label>
                            <input class="fl input" id="purf_country" type="text" size="26" />
                        </p>
                    </fieldset>
                    
                    <fieldset class="br">
                        <legend class="h6 boxl"><b>Social Security Card information</b></legend>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_names">Name</label>
                            <input class="fl input" id="purf_names" type="text" size="26" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid2" for="purf_cpf">CPF</label>
                            <input class="fl input" id="purf_cpf" type="text" size="26" />
                        </p>
                        <p class="wrap">
                            <span class="fl grid2">Date of birth</span>
                            <?php include("form_year.php");?>
                        </p>
                    </fieldset>
                    <p class="small em1 hr"><b>Note:</b> Your card will be debited on confirmation</p>