        <section class="list br">        	
        	<div class="fl caption tc">
            	<h2 class="caption"><b>Your search</b></h2>
                <ul class="unselect">
                	<li class="tc"><b>500</b></li>
                	<li class="small"><small class="ico plane3"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane4"></small> Mon 23 JUL</li>
                    <li><small class="none ico slope"></small></li>
                </ul>
			</div>
            <div class="fl caption tc">
            	<h2 class="caption"><b>Closest dates around your search</b></h2>
                <ul class="selected">
                	<li class="tc"><b>425</b></li>
                	<li class="small"><small class="ico plane3"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane4"></small> Mon 23 JUL</li>
                    <li><small class="ico slope"></small></li>
                </ul>
                <ul class="unselect">
                	<li class="tc"><b>450</b></li>
                	<li class="small"><small class="ico plane4"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane3"></small> Mon 23 JUL</li>
                    <li><small class="none ico slope"></small></li>
                </ul>
                <ul class="unselect">
                	<li class="tc"><b>475</b></li>
                	<li class="small"><small class="ico plane4"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane3"></small> Mon 23 JUL</li>
                    <li><small class="none ico slope"></small></li>
                </ul>
			</div>
            <div class="fl caption tc">
            	<h2 class="caption"><b>Lowest</b></h2>
                <ul class="unselect">
                	<li class="tc"><b>300</b></li>
                	<li class="small"><small class="ico plane4"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane3"></small> Mon 23 JUL</li>
                    <li><small class="none ico slope"></small></li>
                </ul>
                <ul class="unselect">
                	<li class="tc"><b>325</b></li>
                	<li class="small"><small class="ico plane4"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane3"></small> Mon 23 JUL</li>
                    <li><small class="none ico slope"></small></li>
                </ul>
                <ul class="unselect">
                	<li class="tc"><b>350</b></li>
                	<li class="small"><small class="ico plane4"></small> Sun 20 JUL</li>
                	<li class="small"><small class="ico plane3"></small> Mon 23 JUL</li>
                    <li><small class="none ico slope"></small></li>
                </ul>
			</div>                            
        </section>