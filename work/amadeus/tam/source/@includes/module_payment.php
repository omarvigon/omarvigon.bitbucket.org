		<header class="caption4 hr">
            <small class="fl ico purf"></small>
            <h3 class="h6">&nbsp;PAYMENT</h3>
        </header>
        <article class="wrap">
        	<fieldset class="fl form2 grid2">
				<label class="block boxw active" id="d1"><input type="radio" class="fl" name="radio2" checked="checked" /> TAM Credit Card</label>
                <label class="block boxw caption" id="d2"><input type="radio" class="fl" name="radio2" /> Credit Card</label>
                <?php if($page!="atc_purf") { ?>
               	 	<label class="block boxw caption" id="d3"><input type="radio" class="fl" name="radio2" /> Online Debit</label>
                	<label class="block boxw caption" id="d4"><input type="radio" class="fl" name="radio2" /> 36x installments</label>
                	<label class="block boxw caption" id="d5"><input type="radio" class="fl" name="radio2" /> Boletto</label>
                	<label class="block boxw caption" id="d6"><input type="radio" class="fl" name="radio2" /> Lotteries</label>
                    <label class="block boxw caption inactive" id="d7"><input type="radio" class="fl" name="radio2" /> Credit Card</label>
                <?php } ?>
            </fieldset>
            <fieldset class="fl">
                <div id="target_d1">
                	<p class="boxg"><small class="fl ico tam0"></small>You may register a TAM Credit card to benefits some In ac lectus rhoncus massa volutpat congue viverra at turpis. Proin nisl sem, auctor non varius eu, molestie faucibus ligula. Curabitur placerat aliquet scelerisquem, <a href="#" class="em">register a card</a></p>
                	<ul class="wrap list2 tc br">
                        <li class="boxg selected"><small class="ico visa"></small></li>
                        <li class="boxg caption"><small class="ico master"></small></li>
                    </ul>
					<?php include("form_card.php");?>
                </div>
                <div id="target_d2" class="none">
                	<ul class="wrap list2 tc br">
                        <li class="boxg selected"><small class="ico visa"></small></li>
                        <li class="boxg caption"><small class="ico american"></small></li>
                        <li class="boxg caption"><small class="ico master"></small></li>
                        <li class="boxg caption"><small class="ico diners"></small></li>
                    </ul>
                	<?php include("form_card.php");?>
                </div>
                <div id="target_d3" class="none">
                	<ul class="wrap list2 tc">
                        <li class="boxg selected"><small class="ico p_bb"></small></li>
                        <li class="boxg caption"><small class="ico p_bradesco"></small></li>
                        <li class="boxg caption"><small class="ico p_itau"></small></li>
                    </ul>
                    <p>Pague sua passagem com d&eacute;bito em conta corrente nos bancos acima.</p>
                    <p>Escolhendo esta op&ccedil;&atilde;o, voc&ecirc; ser&aacute; direcionado para o site do banco para finalizar a compra de sua passagem.</p>
                    <p>Certifique-se de possuir acesso ao site do banco e que seu computador esteja habilitado para realizar esta transa&ccedil;&atilde;o.</p>
                </div>
                <div id="target_d4" class="none">
                	<ul class="wrap list2 tc">
                        <li class="boxg selected"><small class="ico p_bb"></small></li>
                        <li class="boxg caption"><small class="ico p_bradesco"></small></li>
                        <li class="boxg caption"><small class="ico p_itau"></small></li>
                    </ul>
                    <p>Pague sua passagem com d&eacute;bito em conta corrente nos bancos acima.</p>
                    <p>Escolhendo esta op&ccedil;&atilde;o, voc&ecirc; ser&aacute; direcionado para o site do banco para finalizar a compra de sua passagem.</p>
                    <p>Certifique-se de possuir acesso ao site do banco e que seu computador esteja habilitado para realizar esta transa&ccedil;&atilde;o.</p>
                </div>
                <div id="target_d5" class="none">
                	<img src="../@WDS_STATIC_FILES_PATH@/img/barcode.png" align="Barcode" />
                    <p><b>Leia com aten&ccedil;&atilde;o as regras para pagamento com BOLETO BANC&Aacute;RIO</b></p>
                    <ol class="default">
                    <li>1. Esta op&ccedil;&atilde;o de pagamento n&atilde;o permite parcelamento e &eacute; v&aacute;lida somente para pagamento &agrave; vista.</li>
                    <li>2. Ao concluir o pedido de compra e realizar a reserva da passagem, o sistema gerar&aacute; na sua tela um boleto banc&aacute;rio, que dever&aacute; ser impresso e pago em qualquer institui&ccedil;&atilde;o banc&aacute;ria ou Internet Banking dentro do prazo de vencimento.</li>
                    <li>3. A libera&ccedil;&atilde;o do pedido de compra pela TAM s&oacute; ocorrer&aacute; ap&oacute;s o pagamento integral do boleto banc&aacute;rio e confirma&ccedil;&atilde;o de pagamento pela institui&ccedil;&atilde;o banc&aacute;ria &agrave; TAM.</li>
                    <li>4. O processo de confirma&ccedil;&atilde;o pelos bancos &agrave; TAM para boletos banc&aacute;rios &eacute; mais demorado do que o prazo normalmente estabelecido para os pedidos pagos com cart&atilde;o de cr&eacute;dito ou D&eacute;bito Online e leva, em m&eacute;dia, 3 (tr&ecirc;s) dias &uacute;teis.</li>
                    <li>5. N&atilde;o ser&atilde;o aceitos pagamentos ap&oacute;s o prazo de vencimento descrito no boleto banc&aacute;rio, que &eacute; de 02 (dois) dias ap&oacute;s a conclus&atilde;o do pedido de compra de passagem a&eacute;rea. Ap&oacute;s este prazo, as reservas de passagens a&eacute;reas ser&atilde;o automaticamente canceladas e o boleto banc&aacute;rio perder&aacute; validade.</li>
                    <li>6. Somente ser&atilde;o aceitos cheques para pagamentos de boleto banc&aacute;rio desde que emitidos pelo pr&oacute;prio sacado e da mesma pra&ccedil;a onde o pagamento do boleto banc&aacute;rio estiver sendo realizado.</li>
                    <li>7. Cheques devolvidos pelo banco por qualquer motivo n&atilde;o ser&atilde;o reapresentados, ser&atilde;o destru&iacute;dos pelo banco e o pedido de compra correspondente ser&aacute; automaticamente cancelado pela TAM. Caso o cliente necessite ou deseje, a TAM emitir&aacute; uma 'Declara&ccedil;&atilde;o de Destrui&ccedil;&atilde;o do Cheque' informando que n&atilde;o h&aacute; restri&ccedil;&otilde;es por parte da TAM com rela&ccedil;&atilde;o cobran&ccedil;a desse t&iacute;tulo.</li>
                    <li>8. Confirmado o pagamento pelo banco e desde que o cliente informe previamente seu e-mail e/ou n&uacute;mero de celular, a TAM enviar&aacute; uma mensagem contendo o n&uacute;mero do seu e-ticket.</li>
                    <li>9. No dia e hor&aacute;rio marcados para o seu v&ocirc;o basta o cliente comparecer com 1 hora de anteced&ecirc;ncia (v&ocirc;os nacionais) ou 2h30 (v&ocirc;os internacionais) ao embarque com o n&uacute;mero de seu e-ticket e os documentos e vistos necess&aacute;rios. Para saber mais acesse o menu &quot;Servi&ccedil;os&quot; e &quot;Documenta&ccedil;&atilde;o para Embarque&quot; no site <a href="#" class="em">www.tam.com.br</a></li>
                    <li>10. Para mais informa&ccedil;&otilde;es contate nossa Central de Atendimento ou acesse o menu &quot;Servi&ccedil;os&quot; e &quot;Como Comprar e Formas de Pagamento&quot; no site <a href="#" class="em">www.tam.com.br</a>.</li>
                    </ol>
                </div>
                <div id="target_d6" class="none">
                	<p><small class="ico lott" title="Caixa"></small></p>
					<p><b>Escolha essa op&ccedil;&atilde;o se voc&ecirc; quiser pagar apenas com o c&oacute;digo do pedido em lot&eacute;ricas ou qualquer posto Caixa. Para pedidos acima de R$1.000,00 o pagamento somente poder&aacute; ser realizado em ag&ecirc;ncias ou postos da Caixa.</b></p>
                    <p>Leia com aten&ccedil;&atilde;o as regras para pagamento em LOT&Eacute;RICAS.</p>
                    <ol class="default">
                    <li>1. Clique em finalizar e anote o <b>N&uacute;mero do Pagamento Eletr&ocirc;nico da Caixa</b> que ser&aacute; apresentado na pr&oacute;xima p&aacute;gina. Apresente este n&uacute;mero para pagar nas Lot&eacute;ricas e CEF.</li>
                    <li>2. Acesse o site <a href="#" class="em">www.caixa.gov.br/loterias</a> e localize a lot&eacute;rica mais pr&oacute;xima de voc&ecirc;.</li>
                    <li>3. Dirija-se a qualquer lot&eacute;rica (bilhetes at&eacute; R$ 1.000,00) at&eacute; a data limite para pagamento. Solicite ao agente que o pagamento ser&aacute; atrav&eacute;s da PEC (Pagamento Eletr&ocirc;nico da Caixa). IMPORTANTE: para bilhetes com valores superiores a R$ 1.000,00, solicitamos que dirija-se somente as ag&ecirc;ncias da Caixa Econ&ocirc;mica Federal para efetuar o pagamento.</li>
                    <li>4. Efetuado o pagamento, voc&ecirc; receber&aacute; no e-mail e no celular informado durante o seu processo de compra o n&uacute;mero de seu(s) e-ticket(s) e o c&oacute;digo de sua reserva.</li>
                    <li>5. Em caso de n&atilde;o pagamento at&eacute; a data limite, sua reserva ser&aacute; cancelada e os assentos reservados ficar&atilde;o dispon&iacute;veis para utiliza&ccedil;&atilde;o por outros passageiros.</li>
                    <li>6. No dia e hor&aacute;rio marcados para o seu v&ocirc;o basta o cliente comparecer com 1 hora de anteced&ecirc;ncia (v&ocirc;os nacionais) ou 2h30 (v&ocirc;os internacionais) ao embarque com o n&uacute;mero de seu e-ticket e os documentos e vistos necess&aacute;rios. Para saber mais acesse o menu Servi&ccedil;os e Documenta&ccedil;&atilde;o para Embarque no site <a href="#" class="em">www.tam.com.br</a></li>
                    <li>7. Para mais informa&ccedil;&otilde;es, contate nossa central de atendimento ou acesse o menu Servi&ccedil;os e Como Comprar e Formas de Pagamento no site <a href="#" class="em">www.tam.com.br</a></li>
                    </ol>
                </div>
                <div id="target_d7" class="none">
                	<ul class="warning w4 icob mr">
                    	<li>Credit card payment is not available for this purchase.</li>
					</ul>
                </div>
            </fieldset>
        </article>
        
        <div class="wrap boxg br">
           	<h2>Terms and Conditions</h2>
            <label class="fr caption boxg grid7 tc"><b class="block">I agree to all terms and conditions</b><input type="checkbox" /></label>
            <p>Please note the following item(s) and check to indicate that you accept in order to continue. Concordo com os termos da minha compra, <a href="#" class="em">Condioes de Contrato</a> e <a href="#" class="em">Acknowledgements</a>. Concordo tambem, no caso de resgate de pontos, com as <a href="#" class="em">TAM Fidelidade Program Rules</a>.</p>
		</div>