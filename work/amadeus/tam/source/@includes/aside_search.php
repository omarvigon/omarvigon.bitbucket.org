    	<h3 class="caption4 toggle minus hr" data-wdk-toggle-target="#searchPanel"><span class="fl sign"></span> MODIFY SEARCH</h3>
		<article id="searchPanel" class="none" data-wdk-toggle-transition="none">        	
            <fieldset>
            <?php
            include("form_trip.php");
            include("form_from_to.php");
            include("form_dates.php");
            include("form_passengers.php");
			include("form_cabin.php");
			?>
            </fieldset>
            <footer class="caption4 tr br">           
                <button type="submit" class="main right">SEARCH AND BUY</button>
            </footer>
		</article>