    <fieldset class="wrap">
    <p class="fl">
		<label for="search_adults">Adults:<em class="block em1">&nbsp;</em></label>
        <span class="fl icoa select"><input type="text" class="input" id="search_adults" size="5" /></span>
	</p>
    <p class="fl midst">
		<label for="search_children">Children:<em class="block em1">(2-11 years)</em></label>
        <span class="fl icoa select"><input type="text" class="input" id="search_children" size="5" /></span>
	</p>
    <p class="fl">
		<label for="search_infants">Infants:<em class="block em1">(0-23 months)</em></label>
        <span class="fl icoa select"><input type="text" class="input" id="search_infants" size="5" /></span>
	</p>
    </fieldset>

<?php if($page!="co_upsl" && $page!="od_upsl" && $page!="rd_upsl" ) { ?>
	<p><a href="#" class="em">More than 9 passengers?</a></p>
<?php } ?>
    
<?php if($page=="home_online") { ?>
   	<p><label><input type="checkbox" class="fl" /> Use my points</label></p>
<?php } else { ?>
   	<p class="br"><label><input type="checkbox" class="fl" /> Use my points</label></p>
<?php } ?>    