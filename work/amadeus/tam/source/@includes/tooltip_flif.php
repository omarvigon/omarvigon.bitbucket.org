<div id="tooltip_flif" class="none tooltip boxw grid7 icoa">
	<small title="Delete" class="fr ico close"></small>
    <p><b>Airline company:</b> TAM Linhas Areas</p>
    <p><b>Aircraft:</b> Airbut Industrie A319</p>
    <a href="#" class="em">Aircraft classification</a> <small class="ico info"></small>
    <p><b>Weight:</b> 40kg</p>
    <a href="#" class="em">Baggage allowance</a> <small class="ico info"></small>
    <p><b>Flight number:</b> JJ5122</p>
    <p><b>Operated by:</b> American Airlines</p>
    <a href="#" class="em">Online performance</a> <small class="ico info"></small>
</div>