        <?php if(($page!="fsr") && ($page!="fsrs")) { ?>
        <header class="caption4 hr">
            <small class="fl ico luggage"></small>
            <h3 class="h6">&nbsp;LUGGAGE<?php if($page=="atci") { ?> *<?php } ?></h3>
        </header>
        <?php } ?>
        
        <table class="default boxw br">
        <tfoot>
        <tr class="caption boxw">
          	<td><?php if(($page=="fsr") || ($page=="fsrs")) { ?><button>Cancel</button><?php } ?></td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="h5 em tr"><b>TOTAL 110,00</b></td>
        </tr>         
        </tfoot>
        
        <tbody>
    	<tr class="caption boxw">
        	<td colspan="4">
        	<?php if(($page=="fsr") || ($page=="fsrs")) { ?>
            <output class="fr none">Luggage added <small class="ico ok"></small></output>
            <?php } ?> 
            <?php include("tdefault_outbound.php");?>
			</td>
		</tr>
        <tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2 grid1">Weight</td>
            <td class="h2">Price</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td>
            	<?php if(($page=="fsr") || ($page=="fsrs")) { ?>
            	<span class="none">40kg</span> <label class="fl icoa select"><input type="text" size="14" class="input grid0" value="40kg (80lbs)"></label>
                <?php } else { ?>
                <span>40kg</span>
                <?php } ?> 
			</td>
            <td colspan="2">35,00</td>
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td>
            	<?php if(($page=="fsr") || ($page=="fsrs")) { ?>
            	<span class="none">20kg</span> <label class="fl icoa select"><input type="text" size="14" class="input grid0" value="40kg (80lbs)"></label>
                <?php } else { ?>
                <span>20kg</span>
                <?php } ?> 
			</td>
            <td colspan="2">Already paid</td>
        </tr>
        <tr>
            <th>Mr. Steingrimur J. Sigfusson <em class="em3">(Infant)</em></th>
            <td>n/a</td>
            <td colspan="2">n/a</td>
        </tr>
        <?php if(($page=="fsr") || ($page=="fsrs")) { ?>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="tr">
                <button>Confirm luggages</button> <button class="none">Change luggage</button>
            </td>
        </tr>   
       	<?php } ?>
        <?php if($page=="bkgd") { ?>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="tr"><button>Change luggages</button></td>
        </tr>   
       	<?php } ?>        
        </tbody>

		<tbody>
        <tr class="caption boxw">
        	<td colspan="4">
       	 	<?php if(($page=="fsr") || ($page=="fsrs")) { ?>
           		<output class="fr">Luggage added <small class="ico ok"></small></output>
        	<?php } ?>  
           		<?php include("tdefault_inbound.php");?>
			</td>
        </tr>                                                 
        <tr>
            <th class="h2 middle">Passenger</th>
            <td class="h2 grid1">Weight</td>
            <td class="h2">Price</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>Mr. Sigmundur David Gunnlaugsson</th>
            <td>
            	<?php if(($page=="fsr") || ($page=="fsrs")) { ?>
                <span>40kg</span> <label class="none fl icoa select"><input type="text" size="14" class="input grid0" value="40kg (80lbs)"></label>
                <?php } else { ?>
                <span>40kg</span>
                <?php } ?> 
			</td>
            <td colspan="2">35,00</td>
        </tr>
        <tr>
            <th>Mrs. Jonina Ros Guomundsdottir</th>
            <td>
            	<?php if(($page=="fsr") || ($page=="fsrs")) { ?>
            	<span>20kg</span> <label class="none fl icoa select"><input type="text" size="14" class="input grid0" value="40kg (80lbs)"></label>
                <?php } else { ?>
                <span>20kg</span>
                <?php } ?> 
			</td>
            <td colspan="2">20,00</td>
        </tr>
        <tr>
            <th>Mr. Steingrimur J. Sigfusson <em class="em3">(Infant)</em></th>
            <td>n/a</td>
            <td colspan="2">n/a</td>
        </tr>
        <?php if(($page=="fsr") || ($page=="fsrs")) { ?>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="tr"><button class="none">Confirm luggages</button> <button>Change luggage</button></td>
        </tr>    
       	<?php } ?>
        <?php if($page=="bkgd") { ?>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="tr"><button>Change luggages</button></td>
        </tr>   
       	<?php } ?>                                
        </tbody>
        </table>