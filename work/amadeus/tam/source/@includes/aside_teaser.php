            <section class="teaser hr family f5">
            	<h2>Did you know that for 50$ more, you could upgrade to MAX Fare?</h2>
                <ul class="br">
                	<li>&#10004; Earn 100% miles</li>
                	<li>&#10004; Ability to rebook and refund your booking</li>
                </ul>
                <p class="br"><button class="fr">Upgrade</button><a href="#">No, thank you</a></p>
            </section>