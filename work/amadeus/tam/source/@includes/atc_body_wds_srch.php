        <h1>Select flights to change</h1>
        <?php include("header_price_details.php");?>

        <tbody>
            <tr>
                <th class="h4">ORIGINAL PRICE</th>
                <td class="tr">15.000,00$</td>
                <td class="tc">+</td>
                <td class="tr">16.000,00$</td>
                <td class="tc">+</td>
                <td class="tr">22.000,00$</td>
                <td class="tc">=</td>
                <td class="tr em3"><b>42.000,00$</b></td>
            </tr>
            <tr class="boxb">
                <th><b>Tax</b></th>
                <td class="tr"><b>12.000,00$</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>12.000,00$</b></td>
                <td class="tc">+</td>
                <td class="tr"><b>12.000,00$</b></td>
                <td class="tc">=</td>
                <td class="tr"><b>36.000,00$</b></td>
            </tr>      
        </tbody>
        </table>
        <header class="wrap caption4 hr">
            <h3 class="h6">YOUR ORIGINAL OUTBOUND SEARCH</h3>
        </header>
        <section>
            <div class="wrap boxw">
                <aside class="fl">
                        <?php include("aside_date.php");?>
                </aside>
                <div class="fl column2">
                    <p><a class="em" href="#">JJ3120</a> <small class="ico tam2"></small></p>
                        <p><span class="em2">Operated by TAM</span></p>
                    <p><b>06:30</b> Brasilia International Airport</p>
                    <p><b>08:30</b> Rio de Janeiro Airport</p>
                </div> 
            </div>
            <div class="wrap atc-box-bt"> 
                <div class="boxw1 tr"><button class="selected warn">Undo changes</button></div>
                <fieldset class="wrap">
                    <legend class="boxw atc_nobt"><b>Change your outbound</b></legend>                
                    <fieldset class="article caption middle">
                        <p class="wrap">
                            <label class="fl grid0">From</label>
                            <span class="fl icoa select"><input type="text" placeholder="Brasilia" size="27" class="input"></span>
                        </p>
                        <p class="wrap">
                            <label class="fl grid0">To</label>
                            <span class="fl icoa select"><input type="text" placeholder="Rio de janeiro" size="27" class="input"></span>
                        </p>
                        <p class="wrap">
                            <label class="fl grid0">Date</label>
                            <span class="fl icoa select"><input type="text" placeholder="23/04/2012" size="16" class="input"></span>
                        </p>
                    </fieldset>
                </fieldset> 
            </div>
        </section>  
        <header class="caption4 hr">
            <h3 class="h6">YOUR ORIGINAL INBOUND</h3>
        </header>
        <section>
            <div class="wrap boxw">
                <aside class="fl">
                        <?php include("aside_date.php");?>
                </aside>
                <div class="fl column2">
                    <p><a class="em" href="#">JJ3120</a> <small class="ico tam2"></small></p>
                    <p><span class="em2">Operated by TAM</span></p> 
                    <p><b>06:30</b> Brasilia International Airport</p>
                    <p><b>08:30</b> Rio de Janeiro Airport</p>
                    <p><span class="em2">Connection:</span> <b>1</b></p>
                </div> 
            </div>
            <div class="boxw atc_nobt caption tr">
                <button>Changes</button>
            </div>
        </section> 
        <header class="caption4 hr">
            <h3 class="h6">YOUR ORIGINAL OUTBOUND SEARCH</h3>
        </header>
        <section>
            <div class="wrap boxw disabled">
                <aside class="fl">
                        <?php include("aside_date.php");?>
                </aside>
                <div class="fl column2">
                    <p><a class="em" href="#">JJ3120</a> <small class="ico tam2"></small></p>
                    <p><b>06:30</b> Brasilia International Airport</p>
                    <p><b>08:30</b> Rio de Janeiro Airport</p>
                    <p><span class="em2">Connection:</span> <b>1</b></p>
                </div> 
                <div class="fr hr">
                    <p class="boxw"><small class="ico warn tc"><b>!</b></small> Change not permitted</p>
                </div> 
            </div>
        </section>     
        <fieldset class="boxg caption hr form1">
            <legend class="fl"><b>Change action</b></legend>
            <?php include("form_cabin.php"); ?>
        </fieldset>    
        <header class="caption4 none" id="manageBooking">
            <h3 class="h6">MANAGE BOOKING</h3>
        </header>
        <article class="none">
            <?php include("body_search_manage.php");?>
        </article>        
        <header class="caption4 none" id="onlineCheckin">
            <h3 class="h6">ONLINE CHECKIN</h3>
        </header>
        <article class="none">
            <?php include("body_search_checkin.php");?>
        </article>
        <?php include("footer_prev_next.php");?>     
    </div>
    <aside class="main fr">
        <?php 
            include("atc_aside_passengers_information.php"); 
        ?>
    </aside>          