		<?php include("module_share_trip.php");?>
        <h1>Step 5 - Review the purchase details</h1>        
        <p>Please check your purchase details.</p>
        <?php
        if($page=="atc_purf")
		{
        	include("module_passengers.php");     	
			include("module_payment.php");
			include("footer_next.php");
		} 
		else
		{
        	include("module_passengers.php");
			include("header_seat_reservation.php");
			include("module_seat_reservation.php");
			include("module_insurance.php");		
			include("module_payment.php");
			include("footer_prev_next.php");			
		}
		?>
    </div>
    <aside class="main fr">
       	<?php
        include("aside_flight_outbound.php");
		include("aside_flight_inbound.php");
		if($page=="atc_purf")
			include("atc_aside_total.php");
		else
			include("aside_total.php");
		include("footer_next.php");
		?>
        </article>    
    </aside>