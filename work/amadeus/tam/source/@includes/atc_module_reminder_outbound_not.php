            <section class="wrap boxw caption hr disabled">
                <div class="fl">
                    <h2>Reminder : Original inbound</h2>
                    <aside class="fl br">
                            <?php include("aside_date.php");?>
                    </aside>
                    <div class="fl column2 br">
                        <p><a class="em" href="#">JJ3120</a> <small class="ico tam2"></small></p>
                        <p><span class="em2">Operated by TAM</span></p>
                        <p><b>06:30</b> Brasilia International Airport</p>
                        <p><b>08:30</b> Rio de Janeiro Airport</p>
                    </div> 
                </div>
                <div class="fr hr">
                    <p class="boxw"><small class="ico warn tc"><b>!</b></small> Change not permitted</p>
                </div> 
            </section>