            <h4 class="br toggle minus" data-wdk-toggle-target="#totalOpened,#totalClosed">
            	<em id="totalClosed" data-wdk-toggle-transition="none" class="fr h4verd em3 none">-9.980,00 $</em><small class="ico sign"></small> TOTAL</h4>
            <section id="totalOpened" class="atc_boxg" data-wdk-toggle-transition="none">
                <div class="atc_boxp">
                    <dl class="">
                        <dt class="fl"></dt><dd class="toggle boxb h2"><small class="ico minus2"></small> <b>New price</b></dd>
                        <dt class="fl">Adult <span class="em1">x2</span></dt><dd class="tr">2.240.000,00 $</dd>
                        <dt class="fl">Children <span class="em1">x1</span></dt><dd class="tr">2.240.000,00 $</dd>
                        <dt class="fl">Infant <span class="em1">x1</span></dt><dd class="tr">560.000,00 $</dd>
                        <dt class="fl grid2">
                            <?php if($page=="atc_purf") { ?>
                                <a class="em wdk-popin" href="../popins/taxb.php" data-wdk-popin-width="700" data-wdk-popin-height="840">Tax et Service Fee</a>
                            <?php } else { ?>
                            	Tax et Service Fee
							<?php } ?>
							<?php include("aside_info.php") ?>
                        </dt><dd class="tr">50.000,00 $</dd>
                        <dt class="fl">Rebooking fee</dt><dd class="tr">20.700,00 $</dd>
                        <dt></dt><dd class="tr"><b class="em boxl">2.230.000,00 $</b></dd>
                    </dl>
                    <dl class="br">
                        <dt class="fl"></dt><dd class="toggle boxb h2"><small class="ico minus2"></small> <b>Original price</b></dd>
                        <dt class="fl">Adult x2</dt><dd class="tr">2.240.000,00 $</dd>
                        <dt class="fl">Children x1</dt><dd class="tr">2.240.000,00 $</dd>
                        <dt class="fl">Infant x1</dt><dd class="tr">560.000,00 $</dd>
                        <dt class="fl grid2">Tax et Service Fee</dt><dd class="tr">56.000,00 $</dd>
                        <dt></dt><dd class="tr"><b class="em3 boxl">-2.230.000,00 $</b></dd>
                    </dl>
                </div>
                <div class="atc_boxp">
                    <dl class="wrap">
                        <?php if($page!="atc_upsl") { ?>
                            <dt class="fl"><a class="em" href="#"><b>Installments</b></a></dt><dd class="tr"><b class="em">20,00 $</b></dd>
                        <?php } ?>
                        <dt class="fl"><b>Total paid</b> <?php include("aside_info.php") ?></dt><dd class="tr"><b class="em">20,00 $</b></dd>
                        <dt class="fl"><b>Total refunded</b> <?php include("aside_info.php") ?></dt><dd class="tr"><b class="em3">-10.000,00 $</b></dd>
                        <dt class="fl h6 em3 h4verd"><b>TOTAL Balance</b></dt><dd class="tr h6 h4verd em3"><b class="boxl">-9.980,00 $</b></dd>
                    </dl>    
                    <?php include("aside_total_currency.php");?>    
                </div>      
    		</section>