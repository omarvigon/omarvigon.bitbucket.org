		<fieldset class="br">
            <legend class="boxw caption h2 em3">
            	Passenger 4 (Infant)
                <output class="fr"><small class="ico user"></small> Passenger 4 of 4 <small class="ico okno"></small></output>
            </legend>
            <fieldset class="boxw">
            	<div class="warning em icob mr">
                    <ul class="em">
                    <li><b>Error:</b> These fields are required</li>
                    <li>- Name</li>
                    <li>- Middle Name</li>
                    <li>- Country of issue</li>
                    </ul>
                </div>
            
	            <p><i>This infant is traveling with Passenger 3</i></p>
    	        <p class="em2">Informe somente o ultimo sobrenombre e ainda o seguimiento familiar (Junior/Filho/Neto) caso possua. Examplo: "Jose Pererira da Silva Junior", insira apenas "Jose Silva Junior"</p>
        	    <div class="wrap br">
                    <fieldset class="fl grid9">
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_title4">Title</label>
                            <span class="fl icoa select"><input type="text" class="input" id="alpi_title4" size="10" placeholder="Select" /></span>
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_name4">Name</label>
                            <input type="text" class="fl input em" id="alpi_name4" size="24" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_middle4">Middle Name</label>
                            <input type="text" class="fl input em" id="alpi_middle4" size="24" />
                        </p>
                        <p class="wrap">
                            <label class="fl grid1" for="alpi_last4">Last Name</label>
                            <input type="text" class="fl input" id="alpi_last4" size="24" />
                        </p>
                        <p class="wrap">
                            <span class="fl grid1">Date of birth</span>
                            <?php include("form_year.php");?>
                        </p> 
                    </fieldset>
                    <?php include("form_advanced_infant.php");?>
            	</div>
			</fieldset>
		</fieldset>