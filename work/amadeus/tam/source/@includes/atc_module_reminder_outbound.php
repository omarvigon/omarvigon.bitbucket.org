            <section class="wrap boxw caption hr">
                <div class="fl">
                    <h2>Reminder : Original outbound</h2>
                    <aside class="fl br">
                            <?php include("aside_date.php");?>
                    </aside>
                    <div class="fl column2 br">
                        <p><a class="em" href="#">JJ3120</a> <small class="ico tam2"></small></p>
                        <p><span class="em2">Operated by TAM</span></p>
                        <p><b>06:30</b> Brasilia International Airport</p>
                        <p><b>08:30</b> Rio de Janeiro Airport</p>
                    </div> 
                </div>
                <div class="fr boxw boxnb grid7 br">
                    <p><?php include("aside_info.php") ?> <span class="fr atc_grid6">Your original ticket will not be changed until 
the purchase step is done</span></p>
                </div> 
            </section>