			<h4 class="toggle plus br" data-wdk-toggle-target="#bound1NoDetails,#bound1WithDetails"><small class="ico sign"></small> INBOUND</h4>
			<section class="boxg column" id="bound1NoDetails" data-wdk-toggle-transition="none">
			<div class="wrap">                
	        	<aside class="fl">
    	        <?php include("aside_date.php");?>
        	    </aside>
	            <div class="fr">
    	        	<p><b>9:30</b> Sao Paulo, Congonhas</p>
            		<p><b>11:55</b> Rio de Janerio, Santos Dumont</p>
                    <p>Connection: <b>1</b> | Stop: <b>1</b></p>
                    <p><b class="em3">Mon 21 Jan</b></p>
				</div>
                <?php
				if(($page=="fsr") || ($page=="purf"))
					include("aside_luggage.php");
				if($page=="fsrs")
					include("aside_luggage_fsrs.php");
				?>
            </div>
			</section>
            <section class="boxg column none" id="bound1WithDetails" data-wdk-toggle-transition="none">
			<?php
            include("aside_inbound.php");
			if(($page=="fsr") || ($page=="purf"))
				include("aside_luggage.php");
			if($page=="fsrs")
					include("aside_luggage_fsrs.php");	
			?>	
			<?php if(($page=="avai")) {	?>
            	<div class="wrap boxl br">
	            	<aside class="fl br tr">
    	            	&nbsp;
        	        </aside>
            	    <div class="fr br">
                       	<p>Flight Duration: <b>2h 15m</b></p>                            
            	    </div>
                </div>
            <?php } ?>
            </section>