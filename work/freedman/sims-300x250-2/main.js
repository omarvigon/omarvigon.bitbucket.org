// JavaScript Document
//HTML5 Ad Template JS from DoubleClick by Google

//Declaring elements from the HTML i.e. Giving them Instance Names like in Flash - makes it easier
var container;
var playGun = true;

var SITE = {

    initEB : function (){

        //Bring in listeners i.e. if a user clicks or rollsover

        //$("#background_exit").on( "tap", SITE.addListeners.bgExitHandler);
        //bgExit = document.getElementById("background_exit");

        //bgExit.addEventListener('click', SITE.addListeners.bgExitHandler);

        SITE.init();


    },

    init : function (){

        //change position
        TweenLite.delayedCall(0, SITE.animate,[0]);

        // $( "#background_exit" ).mouseenter(function() {
        //     TweenLite.to('.cta2',0.2,{alpha:1});
        //     TweenLite.to('.cta',0.2,{alpha:0});
        // });

        // $( "#background_exit" ).mouseleave(function() {
        //    TweenLite.to('.cta2',0.2,{alpha:0});
        //    TweenLite.to('.cta',0.2,{alpha:1});
        // });

        // $( ".header" ).click(function(){
        //     //Enabler.exit('HTML5_Background_Clickthrough'); 
        // })
    },

    animate : function (whichStep) {

            switch (whichStep) {

                case 0:
                    TweenLite.to('#copy0',0,{scaleX:0,alpha:1});
                    TweenLite.to('#copy0',0.4,{scaleX:1,alpha:1});
                    TweenLite.to('#copy0',0.2,{top:-160, left:-160,delay:1.3});
                    TweenLite.to('#c1',0.2,{top:-40, left:-40,delay:1.3});
                    TweenLite.to('#c1',0.3,{top:0, left:0,delay:1.5});                                        
                    TweenLite.delayedCall(3.3, SITE.animate,[1]);
                break;

                case 1:
                    TweenLite.to('#c1',0.3,{alpha:0});
                    TweenLite.to('#copy1',0,{scaleX:0,alpha:1});
                    TweenLite.to('#copy1',0.2,{scaleX:1,alpha:1,delay:0.3}); 
                    TweenLite.to('#c2',0.5,{top:-30,delay:1.3});
                    TweenLite.to('#c2',0.4,{top:0,delay:1.8});
                    TweenLite.to('#copy1',0.3,{top:-200,alpha:1,delay:1.5});                     
                    TweenLite.delayedCall(2.5, SITE.animate,[2]);
                break;

                case 2:
                    TweenLite.to('#c2',0.3,{alpha:0});                    
                    TweenLite.to('#copy2',0,{scaleX:0,alpha:1,delay:0.4});                
                    TweenLite.to('#copy2',0.4,{scaleX:1,alpha:1,delay:0.5});
                    TweenLite.delayedCall(1, function(){
                    TweenLite.to('#copy2',0.4,{left:251,delay:1.3});
                    TweenLite.to('#c3',0.3,{left:25,delay:1.3});
                    TweenLite.to('#c3',0.3,{alpha:0,delay:2.8});  
                    //TweenLite.to('#c3',0.1,{left:0,delay:1.6});
                })
                   TweenLite.delayedCall(3.8, SITE.animate,[3]);
                break;
                case 3:
                                      
                    TweenLite.to('#c2',0.3,{alpha:0,left:0,top:300});
                    TweenLite.to('#logo',0,{scaleX:0,alpha:1});
                    TweenLite.to('#logo',0.4,{scaleX:1,alpha:1,delay:0.3});  
                    TweenLite.to('#c2',0.3,{alpha:1,scaleX:0.82, scaleY:0.82, top:-10, left:-94,delay:1}); 
                    TweenLite.to('#copy3',0.3,{alpha:1,delay:1.5});
                    TweenLite.to('#copy4',0.3,{alpha:1,delay:2});
                    TweenLite.to('#copy5',0.3,{alpha:1,delay:2.5});                                     
                    TweenLite.to('#cta',0,{scaleX:0,alpha:1,delay:2.8});                
                    TweenLite.to('#cta',0.5,{scaleX:1,alpha:1,delay:2.9});


                break;

            }
    },

    addListeners : {

        bgExitHandler : function() {
            //console.log("exitHit");
            //Call Exits
            //Doubleclick
            //Enabler.exit('HTML5_Background_Clickthrough');    

        },
    }
}

SITE.initEB();