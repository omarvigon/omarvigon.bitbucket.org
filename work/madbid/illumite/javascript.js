function init()
{
	if( $( "#slide" ).length )
		var slide = new Gallery();
}

function $( selector, object )
{
	var nodelist;
	var context = object || document;
	
	if( context.querySelector )
		nodelist = context.querySelectorAll( selector );
	/*
	else
		nodelist = Selector( selector, context ); //http://www.llamalab.com/js/selector/	
	*/
	
	return nodelist;
	//return ( nodelist.length == 1 )? nodelist[ 0 ] : nodelist;
	
	//when the object not exists returns NodeList, when exist return HtlmObject
	//if ( element.innerHTML ) if is an single object
	//if ( input.value ) the inputs doesn't have innerHTML
	//if ( array.length)	if is a nodelist
}

function Gallery()
{
	var position = 0;
	var time;
	var tag = $( "#slide div" )[ 0 ];
	var numImgs = $( "img", tag ).length;
	var width = 90;
	var maxPosition = ( numImgs - 7 ) * width * -1;
	var img;

	for( var i = 0 ; i < numImgs ; i++ )
	{
		img = $( "img", tag )[ i ];
		
		img.style.left = ( width * i ) + "px";
		img.onclick = function()
		{
			$( "img.pic_gallery" )[ 0 ].src = this.src;
			$( "h2.txtc" )[ 0 ].innerHTML = this.alt;
			$( "div.scroll" )[ 0 ].innerHTML = $( "#" + this.id + "_text" )[ 0 ].innerHTML;
		};
	}
	
	$( "#slide_left" )[0].onclick = function()
	{
		slide.move( 1 );
		return false;
	};
	
	$( "#slide_right" )[0].onclick = function()
	{
		slide.move( -1 );
		return false;
	};
		
	this.move = function( side )
	{
		if( ( ( position < 0 ) && ( side == 1 ) ) || ( ( position >= maxPosition ) && ( side == -1 ) ) )
		{
			clearTimeout( time );
			this.moving( position + ( width * side ), side );	
		}	
	};
	
	this.moving = function( finalPosition, side )
	{
		if( ( position == finalPosition ) || ( position >= 0 && side == 1 ) || ( position <= maxPosition && side == -1 ) )
		{
			clearTimeout( time );
			return;
		}
		else
		{
			position = position + ( 5 * side );
			tag.style.left = position + "px";
			time = setTimeout( "slide.moving(" + finalPosition + "," + side + ")" );
		}
	};
}

init();
