function initDOM()
{			
	var url = window.location.pathname;
	var href = window.location.href;
	
	ie6_test();
	
	link_iconMinimize();
	
	if( document.getElementById( "mymad" ) != null ) 		//if the page is one of the "my madbid sections"
		highlight_mymadbidLink( url );
		
	if( document.getElementById( "bar_tab" ) != null ) 		//if the page contains the "bar_tab"	
		highlight_barTab( url );
	
	if( document.getElementById( "howimg" ) != null )		 //if the page is "how it works", start the countdown simulator
		how_demoAuction( "auto" );
	
	if( document.getElementById( "confirmed" ) != null ) 	//if the page is "confirmed_first"
		updating_confirmed_first();
	
	if( document.getElementById( "sorting_btn" ) != null )	//if the page contains the sorting form ( in easy view )
		sortingAuctions();
	
	if( navigator.userAgent.indexOf( "AOL" ) != -1 )		//if the browser is AOL, show a warning popup
		prompt_showPage( "pg_prompt_aol" );
	
	if( document.getElementById( "auction_product_image_bid_detail" ) != null ) //if is a singlebid page
		updateImage();
	
	if( ( url.indexOf( "/live/" ) != -1 ) || ( url.indexOf( "/easy/" ) != -1 ) )	//if the page is 'live' or 'easy'
	{	
		//auction_highlight( 0 );
		//auction_rookies();
	}
	if( ( url.indexOf( "/confirm_mobile/" ) != -1 ) || url.indexOf( "/confirm/" ) != -1 )		//if the page is 'confirm_mobile' or 'confirm'
	{	
		removeSub_bar();
	}
	if( ( url.indexOf( "/buy/" ) != -1 ) || url.indexOf( "/order/" ) != -1 ) //if the page is 'buy'
	{
		$(".bar_top p").remove();
		$(".bar_top .bar_yellow").remove();
		$(".bar_top iframe").remove();
		$(".tb_container").remove();
		$(".bar_bottom").remove();
		$(".bar_top a").removeAttr("href").removeAttr("onclick").prop("onclick", null).css("cursor","default");
		
		if( typeof timestamp_count_to != 'undefined' )
		{
			if( timestamp_count_to != 0 )
			{			
				countdown_timer();
			}
		}
	}
	
	
	if( typeof upsellitStatus != 'undefined' )
	{
		if( upsellitStatus==1 )
		{
			if( typeof loggedIn != 'undefined' )
			{
				if( loggedIn == 1 )
				{
					// check for order status
					if( typeof firstOrderStatus != 'undefined' )
					{
						if( firstOrderStatus == 1 )
						{
							USI.init();
						}
					}
				}	
			}
			else
			{
				// not logged in so either registration or home page
				USI.init();
			}
		}
	}
	
}

var USI = {
	init: function(){
		
		var href = window.location.href;
		var params = null;
		
		if (href.match(/buy/))
		{
			params = 'qs=207227220222333306302294302345322298345345322312331272333335&siteID=6719';
		}
		else if(href.match(/register/))
		{
			params = 'qs=223207205207281326311343309309324272343294330337326334331306&siteID=6718';
		}
		/*else if(href.match(/com\/test_release\/$/))
		{
			params = 'qs=210220258237308336278341306343297328313331322296329289334333&siteID=6730';
		}*/;
		
		if (params !== null) 
		{
			this.install(params);
		};
	},
	install: function(params){
		var USI_headID = document.getElementsByTagName("head")[0];
		var USI_installID = document.createElement('script');
		USI_installID.type = 'text/javascript';
		USI_installID.src = 'http'+ (document.location.protocol=='https:'?'s://www':'://www')+ '.upsellit.com/launch.jsp?' + params;
		USI_headID.appendChild(USI_installID);
	}
}

/* Redirect user to given url after given time*/
var Redirect = {
	timeLimit: null,
	timeLeft: null,
	redirectTo: null,
	init: function(args){
		this.timeLimit = args.timeFrameSeconds;
		this.timeLeft = args.timeFrameSeconds;
		this.redirectTo = args.redirectTo;
		return this;
	},
	reset: function(){
		this.timeLeft = this.timeLimit;
	},
	isTimeToRedirect: function(){
		if(this.timeLeft === 0){
			window.location.href = this.redirectTo;
		}else{
			this.timeLeft -= 1;
			setTimeout(function(){
				Redirect.isTimeToRedirect()
			}, 1000);
		}
	}
}
//if there is no meta html redirect set and we are not in expired page
if($('meta[http-equiv=refresh]').size() === 0 && window.location.href.match(/expired/) === null){
	var baseURL = $('head base').attr('href');
	Redirect.init({timeFrameSeconds: 3600, redirectTo: baseURL + 'expired'}).isTimeToRedirect();
}


/**
 * \fn updateImage( url )
 * \brief Update the big picture (in the singlebid, buynow, closebid pages) when you mouseover on the small pictures
 */
function updateImage()
{
	var images = document.getElementById( "auction_product_image_bid_detail" ).getElementsByTagName( "img" );
	var length = images.length;
	
	for( var i = 1 ; i < length ; i++ )
	{
		if( images[ i ] != null )
		{
			images[ i ].onmouseover = function()
			{
				images[ 0 ].src =  this.name;
			};
		}
	}
}


/**
 * \fn highlight_barTab( url )
 * \brief Update the class of the tab depending of the URL
 * \param url, the current URL
 */
function highlight_barTab( url )
{
	var element;
	
	if( url.indexOf( "/live" ) != -1 )
	{
		element = document.getElementById( "tab_live" );
		
		if( element != null )
			element.getElementsByTagName( "span" )[ 0 ].className += " opacity1";
	}
	else if( url.indexOf( "/closed" ) != -1 )
	{
		element = document.getElementById( "tab_closed" );
		
		if( element != null )
			element.getElementsByTagName( "span" )[ 0 ].className += " opacity1";
	}
	else if( url.indexOf( "/future" ) != -1 )
	{
		element = document.getElementById( "tab_future" );
		
		if( element != null )
			element.getElementsByTagName( "span" )[ 0 ].className += " opacity1";
	}
	else if( url.indexOf( "/easy" ) != -1 )
	{
		element = document.getElementById( "tab_easy" );
		
		if( element != null )
			element.getElementsByTagName( "span" )[ 0 ].className += " opacity1";
	}
	else //the urls is /
	{
		element = document.getElementById( "tab_home" );
		
		if( element != null )
			element.getElementsByTagName( "span" )[ 0 ].className += " opacity1";	
	}	
}


/**
 * \fn highlight_mymadbidLink( url )
 * \brief Update the class of the <li> depending of the URL
 * \param url, the current URL
 */
function highlight_mymadbidLink( url )
{
	var li = document.getElementById( "nav" ).getElementsByTagName( "li" );
	var length = li.length;

	for( var i = 0 ; i < length ; i++ )
	{
		if( li[ i ].getElementsByTagName( "a" )[ 0 ].href.indexOf( url ) != -1 )
			li[ i ].className = "selected";
		else
			li[ i ].className = "unselected";
	}	
}


/**
 * \fn link_iconMinimize()
 * \brief Look for all the <small class='icon_minimize'> in the DOM, and create the onlick event for them -> show/hide
 */
function link_iconMinimize()
{
	var smalls = document.getElementsByTagName( "small" );
	var smallslength = smalls.length;
	
	if( smallslength > 0 )
	{
		for( var i = 0; i< smallslength ; i++ )
		{
			if( smalls[ i ].className.indexOf( "icon_minimize" ) != -1 )
			{
				smalls[ i ].onclick	= function()
				{
					if( this.parentNode != null  )
					{
						var nextTag = this.parentNode;
						
						do nextTag = nextTag.nextSibling;
						while( nextTag && nextTag.nodeType != 1 )		
								
						if( this.className.indexOf( "off" ) != -1 )
						{
							nextTag.style.display = "block";
							this.className = this.className.replace( " off", " on" );	
						}
						else
						{
							nextTag.style.display = "none";
							this.className = this.className.replace( " on", " off" );
						}
					}
				}
			}
		}
	}
}

/**
 * \fn element_foldUnfold( object )
 * \param object the element that fires the call 
 * \brief fold / unfold the sibling element of object's parent element.
 */
function element_foldUnfold( object )
{
   /* The next tag to the parent element, which is calling the function*/
   if( object.parentNode != null )
   {
      var nextTag = object.parentNode;
      do nextTag = nextTag.nextSibling;
      while( nextTag && nextTag.nodeType != 1 )
      if( object.className == "off" )
      {
         nextTag.style.display = "block";
         object.className = "on";
      }
      else
      {
         nextTag.style.display = "none";
         object.className = "off";
      }
   }
} 

/**
 * \fn sortingAuctions()
 * \brief In the easy view page, rearrange the auctions according to the <input type="radio"> 'easy to hard' or 'hard to easy'
 */
function sortingAuctions()
{
	document.getElementById( "sorting_btn" ).onclick = function()
	{
		var checkbox = document.getElementById( "sorting" ).getElementsByTagName( "input" );
		var length = checkbox.length;
		var objArray = [], newIndexes = [], newAuctionIDs = [];
		
		for( var i = 0 ; i < auctionIDs.length ; i++ )				//creating the array of objects
		{
			objArray[ i ] = {
				id: auctionIDs[ i ], 
				level: auctionLevels[ i ] + 1, 
				price: parseFloat( HTMLToNumber( auctionHighestBids[ i ] ) )
			};
		}
	
		for( var i = 0; i < length ; i++ )							//checking which input checkbox is clicked
		{	

			if( ( checkbox[ i ].checked ) && ( checkbox[ i ].value > 0  ) )
			{
				if( checkbox[ i ].name == "price" )					//order by price
				{	
					objArray.sort( function( first, second )
					{

						if( checkbox[ i ].value == 1 )	//high->low
							return second.price - first.price;
							
						if( checkbox[ i ].value == 2 )	//low->high
							return first.price - second.price;
					});
				}
	
				if( checkbox[ i ].name == "level" )				//order by level
				{
				   objArray.sort( function( first, second )
				   {
						for( var k = 0; k < 4; k++ )
							newIndexes[ k ] = 4 - k;
	
						newIndexes = rotate_array( newIndexes, checkbox[ i ].value - 1 );
						return newIndexes[ second.level - 1 ] - newIndexes[ first.level - 1 ];
				   }); 
				}			
			}
		}
	
	 for( var x in objArray )      //creating new array with the new sort
		newAuctionIDs.push( objArray[ x ].id );

		if( newAuctionIDs.length > 0 )
			auction_resortArray( newAuctionIDs );
	};	
}


/**
 * \fn auction_resortArray( newAuctionIDs )
 * \brief Rewrite the content of the <ul id="bidsys_auction_list"> according with the new array 'newAuctionIDs'
 * \param Array of auctions with new position
 */
function auction_resortArray( newAuctionIDs )
{
	if( newAuctionIDs.length == auctionIDs.length )
	{
		var output = "";
		var element;
	
		for( var j in newAuctionIDs )
		{
			element =  document.getElementById( "auction_" + newAuctionIDs[ j ] );
			
			if( element != null )
				output += "<li id='auction_" + newAuctionIDs[ j ] +"' class='" + element.className + "'>"+ element.innerHTML +"</li>";
		}
	
		document.getElementById( "bidsys_auction_list" ).innerHTML = output;
	}
}


/**
 * \fn rotate_array( arr, turns )
 * \brief Shift the elements of an array, several positions
 * \param arr the array to rotate
 * \param turns, the number of times
 */

function rotate_array( arr, turns )
{
    for(var l = arr.length, turns = (Math.abs(turns) >= l && (turns %= l), turns < 0 && (turns += l), turns), i, x; turns; turns = (Math.ceil(l / turns) - 1) * turns - l + (l = turns))
        for(i = l; i > turns; x = arr[--i], arr[i] = arr[i - turns], arr[i - turns] = x);
    return arr;
}


/**
 * \fn shake_element(elementID )
 * \brief Shake an HTML element horizontally
 * \param The id of the element to use getElementById
 * \param duration, the duration of the shake in miliseconds
 * \param speed (optionlal), number of ms for each interval
 * \param pixels (optional), number of pixels to move left/right
 */
function shake_element( elementID, duration, ms, pixels )
{
	var obj = document.getElementById( elementID );
	var speed = ms || 100;
	var distance = pixels || 3;
	var interval;
	
	if( obj != null )
	{
		obj.style.position = "relative";
		var time = 0;
		var x = distance;

		interval = window.setInterval( function () 
		{
			if( time < duration )
			{
				time = time + speed;
				x = distance - x;
				obj.style.left = x + "px";		
			}
			else
			{
				window.clearInterval( interval );
			}
				
		} , speed );
	}
}


/**
 * \fn Referform( credits_friend, credits_purchase )
 * \brief Manage the form in /refer_details. It adds new input, remove input and calculate the total credits
 * \param credits_friend and credits_purchase, the number of credits you win
 */
function Referform( credits_friend, credits_purchase )
{
	var fieldset = document.getElementById( "refer_details" ).getElementsByTagName( "fieldset" )[ 0 ]; 
	var default_text = fieldset.getElementsByTagName( "input" )[ 0 ].value;
	var index = 1;
	
	fieldset.getElementsByTagName( "input" )[ 0 ].onfocus = function() 
	{
		this.value = '';
	};
	fieldset.getElementsByTagName( "small" )[ 0 ].onclick = function()
	{
		refer.add();
	};
	
	document.getElementById( "refer_email_0" ).onblur = function()
   {
      validate_email( this.value );
   };

   var validate_email = function( email )
	{
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		
		if( !pattern.test( email.value ) )
		{
         notification_show( email.id, 2, "The email is wrong" );
         return false;
		}

      return true;
	};
	
	this.add = function()
	{
      var email_validate = validate_email( fieldset.getElementsByTagName( "p" )[ 0 ].getElementsByTagName("input")[ 0 ] );
      if ( ! email_validate )
      {
         return false;
      }
		var newobj = fieldset.getElementsByTagName( "p" )[ 0 ].cloneNode( true );
		newobj.getElementsByTagName( "small" )[ 0 ].style.display = "none";
		newobj.getElementsByTagName( "small" )[ 1 ].style.display = "inline-block";
		newobj.getElementsByTagName( "small" )[ 1 ].onclick = function()
		{
			refer.remove( this );
		};
		
		fieldset.appendChild( newobj );
		fieldset.getElementsByTagName( "input" )[ 0 ].value = default_text;		
		fieldset.getElementsByTagName( "input" )[ 0 ].id = "refer_email_" + index;
		
		index ++;
		
		this.calculator();
	};
	this.remove = function( obj )
	{
		fieldset.removeChild( obj.parentNode );
		this.calculator();
	};
	this.calculator = function()
	{
		var friends = fieldset.getElementsByTagName( "input" ).length - 1;

		fieldset.getElementsByTagName( "strong" )[ 0 ].innerHTML = friends*credits_friend;
		fieldset.getElementsByTagName( "strong" )[ 1 ].innerHTML = friends*credits_purchase;
		fieldset.getElementsByTagName( "strong" )[ 2 ].innerHTML = (friends*credits_friend) + (friends*credits_purchase);
	};
}


/**
 * \fn updating_confirmed_first( )
 * \brief Add classes for the first,second,third auction anc change the bids requieres for pictures
 */
function updating_confirmed_first()
{	
	

	if( document.getElementById( "auction_" + auctionIDs[ 0 ] ) != null ) //first auction
	{
		document.getElementById( "auction_" + auctionIDs[ 0 ] ).className += " n1";
		
		var classAttr = document.getElementById( "tActionBid_" + auctionIDs[ 0 ] ).className;
		classAttr = classAttr.split(" ")[1];
		
		document.getElementById( "tActionBid_" + auctionIDs[ 0 ] ).className = "button_bidnow" + " " + classAttr;
	}
		
	if( document.getElementById( "auction_" + auctionIDs[ 1 ] ) != null ) //second auction
	{
		document.getElementById( "auction_" + auctionIDs[ 1 ] ).className += " n2";
		
		var classAttr = document.getElementById( "tActionBid_" + auctionIDs[ 1 ] ).className;
		classAttr = classAttr.split(" ")[1];
		
		document.getElementById( "tActionBid_" + auctionIDs[ 1 ] ).className = "button_bidnow" + " " + classAttr;
	}
		
	if( document.getElementById( "auction_" + auctionIDs[ 2 ] ) != null ) //third auction
	{
		document.getElementById( "auction_" + auctionIDs[ 2 ] ).className += " n2";
		var classAttr = document.getElementById( "tActionBid_" + auctionIDs[ 2 ] ).className;
		classAttr = classAttr.split(" ")[1];
		document.getElementById( "tActionBid_" + auctionIDs[ 2 ] ).className = "button_bidnow" + " " + classAttr;
	}
	
	var li = document.getElementById( "bidsys_auction_list" ).getElementsByTagName( "li" );
	var length = li.length;
	var text = "";
	var credits = 0;
	var first_link = li[ 0 ].getElementsByTagName( "a" )[ 0 ];
	var second_link = li[ 1 ].getElementsByTagName( "a" )[ 0 ];
	
	if( ( li != null ) && ( length > 1 ) )
	{
		for( var i = 0 ; i < length; i++ )
		{
			if( li[ i ].getElementsByTagName( "a" )[ 1 ] != null ) //second link in the <li> -> <a class="coin c1x"
				credits = HTMLToNumber( li[ i ].getElementsByTagName( "a" )[ 1 ].className );

			if( credits > 0 )
			{
				for( var j = 0 ; j < credits; j++ )
					text += "<small class='icon_bid'></small>";
				
				li[ i ].getElementsByTagName( "p" )[ 2 ].innerHTML = text;
			}
	
			text = "";
		}
	}
	
	if( first_link != null && showEasyGhostItemPopup  === 1 ) //disable the first link
	{
		var content = document.getElementById( "confirmed_first_easy_popup" ).innerHTML;
			
		first_link.onclick = function()
		{
			prompt_show( this.innerHTML, content, 0 , "javascript:prompt_close()", "close" );
			return false;
		};
		
		//also image should show popup
		$( li[ 0 ] ).find('img').click(function(){
			var anchorHtml = $(first_link).html();
			prompt_show( anchorHtml, content, 0 , "javascript:prompt_close()", "close" );
			return false;
		});
		
	}
	
	if( second_link != null && showModerateGhostItemPopup === 1 ) //disable the first link
	{
		
		var content = document.getElementById( "confirmed_first_medium_popup" ).innerHTML;
		
		second_link.onclick = function(){
			prompt_show( this.innerHTML, content, 0 , "javascript:prompt_close()", "close" );
			return false;
		};
		
		//also image should show popup
		$( li[ 1 ] ).find('img').click(function(){
			var anchorHtml = $(second_link).html();
			prompt_show( anchorHtml, content, 0 , "javascript:prompt_close()", "close" );
			return false;
		});
	}
}


/**
 * \fn auction_rookies()
 * \brief Change the css/border color of the rookie auctions to green. calling in initDOM() and after auction_resortList()
 */
function auction_rookies()
{
	var all_tags = document.getElementsByTagName( "li" );
	var length = all_tags.length;
	
	for( var i = 0 ; i < length ; i ++ )
	{
		if( all_tags[ i ].id.indexOf( "auction" ) != -1 )	//if is an auction
		{
			if( all_tags[ i ].innerHTML.indexOf( "flag_rookie" ) > 0 ) 	//if is a rookie auction
				all_tags[ i ].className += " border_rookie";
			else
				all_tags[ i ].className = all_tags[ i ].className.replace( "border_rookie", "" );
		}
	}
}


/**
 * \fn auction_highlight()
 * \brief Change the css/highlight of an auction clicked to orange. Also create and read cookie
 * \id, the id of the auction when you clicked, or 0 in initDOM.
 */
function auction_highlight( id )
{
	var obj;
	var cookieValue = "";
	
	if( document.cookie.indexOf( cookiePrefix + "_auction_highlights=" ) != -1 )
		cookieValue =  document.cookie.split( cookiePrefix + "_auction_highlights=" )[ 1 ].split( ";" )[ 0 ];

	if( id == 0 )	//onload page
	{
		if( cookieValue != "" )
		{
			var listID = cookieValue.split( "," );
			var length = listID.length;
			
			for( var i = 0 ; i < length ; i++ )
			{
				obj = document.getElementById( "auction_" + listID[ i ] );
				
				if( obj != null )
					obj.className += " border_color";
			}
		}
	}
	else			//onclick button
	{
		obj = document.getElementById( "auction_" + id );
		
		if( obj != null )
		{
			obj.className += " border_color";
			
			if( cookieValue.indexOf( id ) == -1 ) //if the cookie doesn't exists
			{		
				var date = new Date();
				date.setTime( date.getTime() + ( 7*24*60*60*1000 ) ); //7 days
				cookieValue += id + ",";
				document.cookie = cookiePrefix + "_auction_highlights=" + cookieValue + ";expires=" + date.toGMTString() + "; path=/";
			}
		}
	}
}


/**
 * \fn cookieUser()
 * \brief Check if the user is login or logout, it means if the cookieUserName exists. Return the string, or false
 */
function cookieUser() //
{
	if( document.cookie.indexOf( cookiePrefix + "bidsys_user=" ) != -1 )
		return document.cookie.split( cookiePrefix + "bidsys_user=" )[ 1 ].split( ";" )[ 0 ];	
	else
		return false;
}


/**
 * \fn createUserLinks()
 * \brief Create the <a> tags inside the <var> for the clickable usernames in frontbid, singlebid and easyview
 */
function createUserLinks()
{
	if( cookieUser() )	//if you are login
	{
		var links = document.getElementsByTagName( "var" );
		
		if( links != null )
		{
			var length = links.length;
			var userName;
			var bids;
			var parentID;
			var childrenID;
			var js_link;
			
			for( var i = 0 ; i < length ; i++ )
			{
				userName = links[i].innerHTML;
				parentID = links[i].id.replace( "i_" , "" );
				
				if( document.getElementById( "auction_bidding_history_"+ auctionIDs[ 0 ] ) != null )	/*singlebid*/
				{
					bids = HTMLToNumber( document.getElementById( "tCurBid_" + auctionIDs[ 0 ] ).innerHTML );
					
					if( i == 0 ) 	//first name, out of the <ul>
						childrenID = "tCurBidder_" + parentID;
					else			//normal list of lastbidders
						childrenID = "lastbidder_"	+ ( bids * 100 - i );
				}
				else	/*frontbid and easybid*/
				{
					bids = HTMLToNumber ( document.getElementById( "tCurBid_" + parentID ).innerHTML );
					childrenID = "tCurBidder_" + parentID;
				}
				
				if( ( isNaN( bids ) ) || ( bids != 0 ) )
				{
					js_link = "javascript:notification_show('"+ childrenID +"',7,'"+ userName +"')";
					links[ i ].innerHTML = '<a id="'+ childrenID +'" href="'+ js_link +'">'+ userName +'</a><small class="sneak_over_user"></small>';
				}
			
			}
		}
	}
}



/*
 * fn/class AutoBid()
 * brief-> Class for create the autosuggest input for the form in the 'list_autobid' page
 */
function AutoBid()
{
	var firstTime = true;
	var tagElements = document.getElementById( "autolist" ).getElementsByTagName( "li" );
	var textElements = new Array();
	var values = new Array( "none", "block" );
	
	//document.getElementById( "autotext" ).focus();
	if( tagElements!= null )
	{
		for( var i = 0 ; i < tagElements.length ; i++ )
		{
			textElements[ i ] = tagElements[ i ].getElementsByTagName( "span" )[ 0 ].innerHTML.toLowerCase();

			tagElements[ i ].onclick = function()
			{
				if( document.getElementById( "autotext" ) != null )
				{
					document.getElementById( "autotext" ).value = this.getElementsByTagName( "span" )[ 0 ].innerHTML; //+ " " + convertEntities( "&#10004;" );
					document.getElementById( "auction_id" ).value = this.getElementsByTagName( "span" )[ 0 ].className;
					auto.displayList( 0 );
				}
			};
		}
	}
	
	/*
 	* \fn function AutoBid.reLoad( )
 	* \brief Refresh the input text like reloading the page, when you click on X   
 	*/
	this.reLoad = function ()
	{
		var autotext = document.getElementById( "autotext" );
		
		if( autotext != null )
		{
			autotext.value = "";
			this.keyPress();
			this.displayList( 0 );
		}
	};
	
	/*
 	* \fn function AutoBid.displayList( value )
 	* \brief Show/hide the list of auctions
 	* \param value, 0/1 for 'none' or 'block' to change the css      
 	*/
	this.displayList = function( number )
	{
		if( document.getElementById( "autotext" ) != null )
		{
			if( firstTime == true )
			{
				document.getElementById( "autotext" ).value = "";
				firstTime = false;
			}
		
			document.getElementById( "autolist" ).style.display = values[ number ];
			document.getElementById( "autotext" ).className = values[ number ];
			document.getElementById( "autof" ).getElementsByTagName( "a" )[ 0 ].style.display = values[ number ];
			document.getElementById( "autof" ).getElementsByTagName( "a" )[ 1 ].style.display = values[ 1-number ];
		
			//if( number == 1 )
			//	document.getElementById( "autotext" ).value = document.getElementById( "autotext" ).value.slice(0,-2);
		}
	};
	
	/*
 	* \fn function AutoBid.keyPress( )
 	* \brief Show/hide the auctions while the user is writting   
 	*/
	this.keyPress = function()
	{
		this.displayList(1);
		var text = document.getElementById( "autotext" ).value.toLowerCase();
		
		for( var i in textElements )
			tagElements[ i ].style.display = values[ Math.ceil( ( textElements[ i ].indexOf( text ) + 1 ) / 100 ) ];
	};
	
	if( window.location.href.indexOf( "edit_autobid" ) != -1 )
	{
		if( document.getElementById( "autotext" ) != null )
		{
			document.getElementById( "autotext" ).value = tagElements[ 0 ].getElementsByTagName( "span" )[ 0 ].innerHTML;
			document.getElementById( "autotext" ).className = "none";
		}
	}
}


/**
 * \fn DivSlide( name, automaticMs )
 * \brief Class for display a slideshow, like the slider_front, the slider_winners or the slider_press
 * \param name, the name of the object
 * \param automaticMs, amount of milisecons between the automatic transitions, or 0 for manual performance
 */
function DivSlide( name, automaticMs )
{
	var tag, oldDiv, numDivs;
	this.automatic = false;
	this.name = name;
	
	if( automaticMs > 0 )
		this.automatic = window.setInterval( this.name + ".goNext(true)", automaticMs );
	
	this.init = function()
	{
		if( document.getElementById( this.name )!= null )
		{
			tag = document.getElementById( this.name ).getElementsByTagName( "div" )[ 0 ];
			oldDiv = 0;
			numDivs = tag.getElementsByTagName( "div" ).length;
			moving = false;
			
			/*patch for winners_new, no arrows if there are no more than 1 group of pictures*/
			if( ( this.name == "slide_photos" ) && ( numDivs > 1 ) )
			{
				if( document.getElementById( "winner_arrows" ) != null )
					document.getElementById( "winner_arrows" ).innerHTML = '<a href="javascript:slide_photos.goPrev(false)" class="arrowl" title="Previous"></a><a href="javascript:slide_photos.goNext(false)" class="arrowr" title="Next"></a>';
			}
			/*patch for winners_new*/
			
			if( document.getElementById( this.name + "_links" ) != null )
			{
				var links = "";
			
				for( var i = 0 ; i < numDivs ; i++ )
					links += "<a href='javascript:"+ this.name +".goTo( " + i + ", false )'></a>";
					
				document.getElementById( this.name + "_links" ).innerHTML = links;
				document.getElementById( this.name + "_links" ).getElementsByTagName( "a" )[ 0 ].className = "active";
			}
	
			tag.getElementsByTagName( "div" )[ 0 ].style.display = "block";
		}
	};
	this.goTo = function( index, auto )
	{
		if( ( auto == false ) && ( this.automatic != false ) )
			window.clearInterval( this.automatic );
	
		if( document.getElementById( this.name + "_links" ) != null )
		{
			document.getElementById( this.name + "_links" ).getElementsByTagName( "a" )[ oldDiv ].className = "inactive";
			document.getElementById( this.name + "_links" ).getElementsByTagName( "a" )[ index ].className = "active";
		}
		
		element_fade( tag.getElementsByTagName( "div" )[ oldDiv ], 500, null, true );
		tag.getElementsByTagName( "div" )[ oldDiv ].style.zIndex = "1";		
			
		element_fade( tag.getElementsByTagName( "div" )[ index ], 500 );
		tag.getElementsByTagName( "div" )[ index ].style.zIndex = "2";
		
		oldDiv = index;
	};
	this.goNext = function( auto )
	{
		this.goTo( ( oldDiv >= ( numDivs-1 ) )? 0 :( oldDiv+1 ), auto );
	};
	this.goPrev = function( auto )
	{
		this.goTo( ( oldDiv == 0)? ( numDivs-1 ):( oldDiv-1 ), auto );
	};
	this.init();
}


/**
 * \fn element_getXY( obj )
 * \brief Return the coordenates X,Y of a HTML element
 */
function element_getXY( obj )
{
	var x = 0, y = 0; 
	
	if( obj === document.body )
	{
		return { x : 0, y : 0 }
	}
	
	if( obj.getBoundingClientRect )
	{
		rect = obj.getBoundingClientRect();
		
		if( !document.body.scrollTop )
		{
			scrollLeft = document.documentElement.scrollLeft;
			scrollTop = document.documentElement.scrollTop;
		}
		else
		{
			scrollLeft = document.body.scrollLeft;
			scrollTop = document.body.scrollTop;
		}
		
		x = rect.left + scrollLeft;
		y = rect.top + scrollTop;
	}
	else
	{
		x = obj.offsetLeft;
		y = obj.offsetTop;
		parent = obj.offsetParent;
		
		if( parent != obj )
		{
			while( parent )
			{
				x += parent.offsetLeft;
				y += parent.offsetTop;
				parent = parent.offsetParent;
			}
		}
		
		parent = obj.offsetParent;
		
		while( ( parent ) && ( parent != document.body ) )
		{
			x -= parent.scrollLeft;
			parent = parent.offsetParent;
		}
	}
	
	return { x : x, y : y }
}


/**
 * \fn element_fade( obj, delay, callbk )
 * \brief Start an effect of transition of visibility         
 */
function element_fade( obj, delay, callbk, out ) 
{
	var _obj = obj;
	for( var i = 1; i <= 100; i++ ) 
	{
		( function( j ) 
		{
            setTimeout( function () 
			{  
            	if( out == true )	
					j = 100 - j;
					
                obj.style.opacity = j/100;
                obj.style.MozOpacity = j/100;
                obj.style.KhtmlOpacity = j/100;
                obj.style.zoom = 1; // for ie, set haslayout
                obj.style.display = "block";
				 
				if( (j == 100) && ( callbk != undefined ) ) 
				{
					callbk.call( this, _obj );
				}
                else if( ( out == true ) && ( callbk != undefined ) && ( j == 0 ) ) 
				{
					callbk.call( this, _obj );
				}
				
                obj.style.filter = "alpha(opacity=" + j + ");";
			}, j * delay/100 );
			
        } )( i );
	}
}


/**
 * \fn sidebar_facebook(id)
 * \brief Show the embed html of the facebook box
 * \param id, the id of the profile, therefore, the language
 * \param width, height, pixels of the box 
 */
function sidebar_facebook( id , width, height )
{
	document.writeln( '<fb:fan profile_id="'+id+'" width="'+ width +'" height="'+ height +'" connections="4" stream="false" header="false" border="0" css="%URL_ROOT%css/facebook.css?1"></fb:fan>' );
	var fan_box_url = ( 'https:' == document.location.protocol ? 'https://' : 'http://' ) + 'connect.facebook.net/en_US/all.js';
	loadScriptAsync( fan_box_url, function()
	{
		window.fbAsyncInit = function() 
		{
			FB.init( {appId: "134085630008940", status: true, cookie: true, xfbml: true} );
		};
	});
}
/**
 * \fn facebook( api_key )
 * \brief Facebook api wrapper
 * \param api_key, facebook api key.
 */
function facebook( api_key ) 
{
	var init,
		login,
		login_status,
		test_permissions;
	
	//initialize FB object
	if ( !FB._apiKey ) {
		FB.init( { apiKey : api_key } );
	}
	
	login = function ( callback, perms )
	{
		FB.login( function( response )
		{	
			if ( response.session )
			{
				// Check also for permissions, otherwise abort.
				// This implements a XNOR gate.
				if ( (response.perms ? 1 : 0) === (perms ? 1 : 0) )
				{
					callback && callback( response );
				}
				else
				{
					// Error, permission not granted
					console.log('Permission not granted');
				}
			}
			else
			{
				//Error, cannot login
				console.log('Error in login');
				return false;   
			}
		}, { "perms" : perms ? perms.join(',') : "" } );
	};
	
	login_status = function ( callback, perms )
	{
		// checking if there is any session open
		FB.getLoginStatus( function( response )
		{
			
			if( response.session )
			{
				// the user is logged in. checking permissions
				test_permissions( response, perms, callback );
			}
			else
			{
				// the user is not logged in
				login( callback, perms );
			}
		});
	};
	
	test_permissions = function ( response, perms, callback )
	{	
		var query_perms, 
			uid;
		
		// if perms is empty, there is no need to check if the user
		// grant specific permission
		if ( !perms )
		{
			login( callback, perms );
			return;
		}
		
		query_perms = perms.join(',');
		uid = response.session.uid;
		
		// query for the specific permissions required by the api.
		// if the permission are granted, just fire the callback
		// if not, require to login.
		FB.api({
			"method"	:	"fql.query",
			"query"		:	"SELECT " + query_perms + " FROM permissions WHERE uid=" + uid
		}, 
		function( permissions_granted ) 
		{ 
			permissions_granted = permissions_granted[0];
			
			// check if all the permissions required are a subset
			// of the permission granted.
			for ( permission in permissions_granted )
			{
				if ( parseInt( permissions_granted[permission] ) === 0 )
				{
					// Permission missing. Asking for them
					login( callback, perms );
					return;
				}
			}
			
			// All the permission granted, launching the callback.
			callback && callback();
		});
	};
	
	return {
		"api"		:	function( ) {
							var _arguments = [],
								api_arguments = []
								permissions = [];
							
							_arguments = arguments;
							
							// if the first element is an array, then we need to login and probably
							// ask for additional permissions
							if ( typeof( _arguments[0] ) == "object" && ( _arguments[0] instanceof Array ) )
							{
								
								permissions = _arguments[0];
								
								for ( var k = 0; k < permissions.length; k++ ) 
								{
									// the 'login' entry is just a dummy entries to force the dev to ask for
									// a login. So it needs to be removed cause it doesn't belong to the
									// FB permissions.
									if ( permissions[k] === "login" )
									{
										permissions.splice( k, 1 );
									}
								}
								
								// clever way to use the arguments array as an array.
								// because it's an array, but it's not.
								// removing the first (extra) argument.
								api_arguments = [].splice.apply(_arguments, [1,_arguments.length]);
								
								login_status( function( ) { FB.api.apply(FB, api_arguments) }, permissions.length ? permissions : null );
							}
							// if not, it just falls back to the classical API
							else
							{	
								FB.api.apply(FB, _arguments);
							}
						}
	};
}



/**
 * \fn showLoginSection()
 * \brief Show the login section when the user click on login
 */
function showLoginSection()
{
	var e = document.getElementById( "madbid_login_bar" );
	
	if( e != null )
	{
		e.style.display = "block";		
		e = document.getElementById( "user_name_top" );
		
		if( e != null )
			e.focus();
	} 
}


/**
 * \fn hideLoginSection()
 * \brief Hide the login section. Currently, this function isn't called anywhere
 */
function hideLoginSection()
{
	var e = document.getElementById( "madbid_login_bar" );
	
	if( e != null )
		e.style.display = "none";
}


/**
 * \fn function HTMLToNumber( html )
 * \brief Locale tolerant string to number conversion
 * \param html String to convert to a number
 * Converts the string to a number with tolerance toward different number formatting styles: i.e.
 *		1234.56 => 1234.56 
 *		1234,56 => 1234.56
 *		1.234,56 => 1234.56
 *		1 234,56 => 1234.56
 *		1,234.56 => 1234.56
 *		1 234 => 1234
 *		1234.56 EUR => 1234.56	
 * (supports only two and zero decimal formats)  
 */
function HTMLToNumber( html )
{
	var v = new String( html );
	var vs = v.replace( /[^0-9.,]/g, "" );
	
	if( vs.length >= 3 )
	{
		var decimalPt = vs.charAt( vs.length - 3 );
	
		if( ( decimalPt == "." ) || ( decimalPt == "," ) )
		{
			vs = vs.replace( decimalPt, "DECIMAL" );
			vs = vs.replace( ".", "" );
			vs = vs.replace( ",", "" );
			vs = vs.replace( "DECIMAL", "." );
		}
		else
		{
			vs = vs.replace( ".", "" );
			vs = vs.replace( ",", "" );
		}
	}
	return new Number( parseFloat( vs ) );
	// var hs = new String( html );
	// return new Number( parseFloat( hs.replace( /[^0-9.,]/g, '' ) ) );
}


/**
 * \fn urlencode( v )
 * \brief Convert a string for the URLs sended by ajax in pull/
 */
function urlencode( v )
{
	v = escape( v );
	v = v.replace( "+", "%2B" );
	v = v.replace( "@", "%40" );
	v = v.replace( "/", "%2F" );
	v = v.replace( "*", "%2A" );
	v = v.replace( "%20", "+" );
	return v;
}

// ------------------- BEGIN BIDSYS INTERNAL  -------------------

var timeOn;
var dynamic_time = new Date();
var dynamic_update = 1;
var dynamic_lastUpdate = dynamic_time.getTime() / 1000;
var dynamic_requested = 0;
var dynamic_lastRequest = 0;
var dynamic_waitingRequest = 1;
var dynamic_lastRequestKey = "MAD";
var dynamic_requestFrequency = 1; // was 2 // was 1
var dynamic_forcedFrequency = 1; // was 3 // was 2
var tCount = 0;
var lTimeBegin = 0;
var lTimeOriginal = 0;
var fCount = 4;
var noticeMeTime = 0;
var noticeMeState = -1;
var wantReload = 0;


function dynamic_tick()
{
	var d = new Date();

	if( ( d.getTime() / 1000 ) - dynamic_lastRequest > dynamic_requestFrequency )
	{
		dynamic_lastUpdate = d.getTime() / 1000;
		dynamic_waitingRequest = 0;
		dynamic_requestUpdate();
	}
	else
	{
		dynamic_waitingRequest = 1;
	}
	delete d;
}


function dynamic_requestUpdate()
{
	if( dynamic_update == 1 )
	{
		if( dynamic_requested == 0 )
		{
			var d = new Date();
			dynamic_lastRequest = d.getTime() / 1000;

			// window.ksUpdate.document.updateProcess.submit();			
			ajax_asyncRequestPage( "ajax_update/" + dynamic_lastRequestKey + "/" + dynamic_lastRequest + "/", ajax_updateResponse, ajax_timeoutEvent, 15000 );
			
			dynamic_requested = 1;
			delete d;
		}
		// else we're already updating, so let's ignore this
	}
}


function getTimeStr( a, t, c )
{
	var p = t * 2;

	if( c != 1 )
		p++;

	if( a == 1 )
		return lLongTime[ p ];
	else
		return lShortTime[ p ];
}


function timer_updateAuction( nm, bID, bUT, bType, useHighlight, lastBidTime, curTime )
{
	if( !curTime )
	{
		var d = new Date();		
		curTime = d.getTime();
		delete d;
	}  
	// var d = new Date();
	// d.setTime( bUT * 1000 );

	var el = document.getElementById( nm );

	if( el == null )
		return;
	// var sl = new Number( bUT - ( d.getTime() / 1000 ) );
	// var sl = new Number( bUT - lTimestamp );
	//		( lTimestamp + ( d.getTime() / 1000 ) - lTimeBegin ) 

	var sl = new Number( bUT - 
			( lTimestamp + ( parseInt( curTime / 1000 ) - lTimeBegin ) )  
			// ( lTimestamp +  - lTimeBegin ) 
		);
			
	var tStr = "";

	switch( bType )
	{
	default:
	case 0: // seconds
		tStr = sl.toFixed( 0 );
		break;

	case 1: // hour counter
		if( bUT == 0 )
		{
			tStr = "<span class='bid_time'>" + lLongTime[ 11 ] + "<\/span>";
		}
		else
		{
			var l = sl.toFixed( 0 );

			if( ( l == 2 ) || ( l == 5 ) || ( l == 10 ) )
				dynamic_tick();

			if( l > 0 )
			{
				var hours = Math.floor( l / ( 60 * 60 ) );
				l -= ( hours * 60 * 60 );
				var minutes = Math.floor( l / 60 );
				l -= ( minutes * 60 );
				var seconds = Math.floor( l );

				if( hours > 0 )
				{
					if( useHighlight == 1 )
						tStr += "<span class='bid_time_highlight'>" + hours + "<\/span><span class='bid_time'> " + getTimeStr( 1, 2, hours ) + " <\/span>";
					else
						tStr += "<span class='hl_bid_time_highlight'>" + hours + "<\/span><span class='hl_bid_time'> " + getTimeStr( 1, 2, hours ) + " <\/span>"; // hours + " " + getTimeStr( 1, 2, hours ) + " ";
				}

				if( ( minutes > 0 ) || ( hours > 0 ) )
				{
					if( useHighlight == 1 )
						tStr += "<span class='bid_time_highlight'>" + minutes + "<\/span><span class='bid_time'> " + getTimeStr( 1, 1, minutes ) + " <\/span>";
					else
						tStr += "<span class='hl_bid_time_highlight'>" + minutes + "<\/span><span class='hl_bid_time'> " + getTimeStr( 1, 1, minutes ) + " <\/span>";// minutes + " " + getTimeStr( 1, 1, minutes ) + " ";
				}

				if( hours < 1 )
				{
					if( useHighlight == 1 )
						tStr += "<span class='bid_time_highlight'>" + seconds + "<\/span><span class='bid_time'> " + getTimeStr( 1, 0, seconds ) + "<\/span>";
					else
						tStr += "<span class='hl_bid_time_highlight'>" + seconds + "<\/span><span class='hl_bid_time'> " + getTimeStr( 1, 0, seconds ) + "<\/span>"; // seconds + " " + getTimeStr( 1, 0, seconds );
				}
				
				if( curTime - lastBidTime < 400 )
				{
					tStr = "<span class='bid_time_wrapper_new_bid'>&nbsp;" + tStr + "&nbsp;<\/span>";
				}
				else
				{
					if( sl.toFixed( 0 ) <= 10 )
					{
						tStr = "<span class='bid_time_wrapper_critical'>&nbsp;" + tStr + "&nbsp;<\/span>";
					}
					else
					{
						tStr = "<span class='bid_time_wrapper'>" + tStr + "<\/span>";
					}
				}
			}
			else
			{
				dynamic_tick();
				tStr = lLongTime[ 8 ];
			}
		}
		break;

	case 2: // day counter
		if( bUT == 0 )
		{
			tStr = lShortTime[ 11 ];
		}
		else
		{
			var l = sl.toFixed( 0 );

			if( l > 0 )
			{
				var days = Math.floor( l / ( 60 * 60 * 24 ) );
				l -= ( days * 60 * 60 * 24 );
				var hours = Math.floor( l / ( 60 * 60 ) );
				l -= ( hours * 60 * 60 );
				var minutes = Math.floor( l / 60 );
				l -= ( minutes * 60 );
				var seconds = Math.floor( l );

				if( useHighlight == 1 )
				{
					tStr = "<span class='bid_time_highlight'>" + days + "<\/span>" + getTimeStr( 2, 3, days ) + ", " + 
							"<span class='bid_time_highlight'>" + hours + "<\/span>" + getTimeStr( 2, 2, hours ) + ", " +
							"<span class='bid_time_highlight'>" + minutes + "<\/span>" + getTimeStr( 2, 1, minutes ) + ", " + 
							"<span class='bid_time_highlight'>" + seconds + "<\/span>" + getTimeStr( 2, 0, seconds );
				}
				else
				{					
					tStr = days + getTimeStr( 2, 3, days ) + ", " + hours + getTimeStr( 2, 2, hours ) + ", " +
							minutes + getTimeStr( 2, 1, minutes ) + ", " + seconds + getTimeStr( 2, 0, seconds );
				}
			}
			else
			{
				tStr = "<span class='bid_time'>" + lShortTime[ 8 ] + "<\/span>";
			}
		}
		break;

	case 10: // sleeping
		var l = bUT;
		tStr = lLongTime[ 10 ] + " ";
		var hours = Math.floor( l / ( 60 * 60 ) );
		l -= ( hours * 60 * 60 );
		var minutes = Math.floor( l / 60 );
		l -= ( minutes * 60 );
		var seconds = Math.floor( l );

		if( hours > 0 )
			tStr += hours + " " + getTimeStr( 1, 2, hours ) + " ";

		if( ( minutes > 0 ) || ( hours > 0 ) )
			tStr += minutes + " " + getTimeStr( 1, 1, minutes ) + " ";

		if( hours < 1 )
			tStr += seconds + " " + getTimeStr( 1, 0, seconds );
			
		tStr = "<span class='bid_time'>" + tStr + "<\/span>";

		break;

	case 20: // opening in...
		// do nothing		
		break;
			
	case 61: // paused
		tStr = "<span class='bid_time'>" + lShortTime[ 12 ] + "<\/span>";		
		break;

	case 99: // closed
		tStr = "<span class='bid_time'>" + lShortTime[ 9 ] + "<\/span>";
		var item = document.getElementById( 'tActionBid_' + bID);
		item.setAttribute("class","closed_message");
		item.setAttribute("href", "show/" + bID);
		break;
	}

	if( tStr != "" )
		el.innerHTML = tStr;

	delete sl;
	// delete d;
}


function timer_tick()
{
	var d = new Date();
	tCount++;
	var i = 0;
	var curTime = d.getTime();

	if( dynamic_update == 1 )
		if( ( ( curTime / 1000 ) - dynamic_lastUpdate > dynamic_forcedFrequency ) || ( dynamic_waitingRequest == 1 ) )
			dynamic_tick();

	if( wantReload == 1 )
	{
		if( ( curTime / 1000 ) - lTimeOriginal >= 120 )
		{
			wantReload = 0;
			//forceReload();
		}
	}	

	for( i = 0; i < auctionCount; i++ )
	{
		if( auctionTypes[ i ] != 10 ) // paused / sleeping
		{
			var nm = "tCounter_" + auctionIDs[ i ];
			timer_updateAuction( nm, auctionIDs[ i ], auctionEndTimes[ i ], auctionTypes[ i ], 1, auctionLastBidTimes[ i ], curTime );
		}
	}
	
	delete d;
}


function timer_initTick()
{
	var d = new Date();
	var sl = new Number( d.getTime() / 1000 );
	lTimeBegin = sl.toFixed( 0 );
	lTimeOriginal = lTimeBegin;
	
	createUserLinks();
	
 	timer_tick();
	setInterval( "timer_tick()", 1000 );
	
	if( auctionSyncAuctions == 1 )
		setInterval( "timer_requestNewAuctionData()", 120000 );
		
	delete d;
	delete sl;
}


function ajax_createHttpRequest()
{
	if( typeof XMLHttpRequest == "undefined" )
	{
		XMLHttpRequest = function()
		{
			try {return new ActiveXObject( "Msxml2.XMLHTTP.6.0" )} catch(e) {}
			try {return new ActiveXObject( "Msxml2.XMLHTTP.3.0" )} catch(e) {}
			try {return new ActiveXObject( "Msxml2.XMLHTTP" )} catch(e) {}
			try {return new ActiveXObject( "Microsoft.XMLHTTP" )} catch(e) {}
			throw new Error( "This browser does not support XMLHttpRequest or XMLHTTP." )
		};
	}
	return new XMLHttpRequest();
}


function ajax_asyncRequestPage( url, callbackFunction, timeoutFunction, timeoutInterval )
{
	var request = ajax_createHttpRequest();
	var ajTmOut = setTimeout(
						function( toutFunction, requestObject )
						{ 
							toutFunction( requestObject );
						}, 
						timeoutInterval, timeoutFunction, request );

	request.onreadystatechange = function()
	{
		if( request.readyState == 4 )
		{
			if( request.status == 200 )
			{
				clearTimeout( ajTmOut );
				callbackFunction( url, request );
			}
			else
			{
				clearTimeout( ajTmOut );
				timeoutFunction( request );
			}		
		}
	};	
	request.open( "GET", url, true );
	request.send( null );	
}


function ajax_asyncRequestField( url, callbackFunction, timeoutFunction, timeoutInterval, fieldName )
{
	var request = ajax_createHttpRequest();
	var ajTmOut = setTimeout(
						function( toutFunction, requestObject )
						{ 
							toutFunction( requestObject );
						}, 
						timeoutInterval, timeoutFunction, request );

	request.onreadystatechange = function()
	{
		if( request.readyState == 4 )
		{
			if( request.status == 200 )
			{
				clearTimeout( ajTmOut );
				callbackFunction( url, request, fieldName );
			}
			else
			{
				clearTimeout( ajTmOut );
				timeoutFunction( request );
			}		
		}
	};
	request.open( "GET", url, true );
	request.send( null );
}


function ajax_asyncRequestXML( url, callbackFunction, timeoutFunction, timeoutInterval, userVar )
{
	var request = ajax_createHttpRequest();
	var ajTmOut = setTimeout(
						function( toutFunction, requestObject )
						{ 
							toutFunction( requestObject );
						}, 
						timeoutInterval, timeoutFunction, request );

	request.onreadystatechange = function()
	{
		if( request.readyState == 4 )
		{
			if( request.status == 200 )
			{
				clearTimeout( ajTmOut );
				callbackFunction( url, request, userVar );
			}
			else
			{
				clearTimeout( ajTmOut );
				timeoutFunction( request );
			}		
		}
	};
	request.open( "GET", url, true );
	request.send( null );
}


/**
 * \fn function ajax_timeoutCancel( request )
 * \brief Basic timeout function for Ajax requests 
 */  
function ajax_timeoutCancel( request )
{
	if( request != null )
	{
		request.abort();
		delete request;
	}
	request = null;
}


function ajax_timeoutEvent( request )
{
	if( request != null )
	{
		request.abort();
		delete request;
	}
	request = null;

	var d = new Date();
	dynamic_lastUpdate = d.getTime() / 1000;
	dynamic_requested = 0;
	dynamic_waitingRequest = 0;
	delete d;	
}


function timer_auctionNewBid( index )
{
	var nm = "tCounter_" + auctionIDs[ index ];
	timer_updateAuction( nm, auctionIDs[ index ], auctionEndTimes[ index ], auctionTypes[ index ], 1, auctionLastBidTimes[ index ] );
}


function event_auctionNewType( index )
{
	var dRemind = document.getElementById( "tActionReminder_" + auctionIDs[ index ] );
	var dBid = document.getElementById( "tActionBid_" + auctionIDs[ index ] );

	if( ( auctionTypes[ index ] == 0 ) || ( auctionTypes[ index ] == 1 ) || ( auctionTypes[ index ] == 2 ) || ( auctionTypes[ index ] == 99 ) )
	{
		if( dRemind != null )
			dRemind.style.display = "none";
			
		if( dBid != null )
			dBid.style.display = "block";
	}
	else
	{
		if( dRemind != null )
			dRemind.style.display = "none";
			
		if( dBid != null )
			dBid.style.display = "block";
	}
	
	/*prompt when a user win an auction*/
	if( ( auctionTypes[ index ] == 99 ) )
	{
		var cookieUserName = cookieUser();
		
		if( cookieUserName )
		{
			var auctionName = document.getElementById( "auction_title_" + auctionIDs[ index ] ).innerHTML;
			
			if( cookieUserName == auctionWinners[ index ] )
			{
				if( auctionShowRookies >= 1 ) //rookie user or first time users
				{
					prompt_showPage( "pg_prompt_winrookie" );
				}
				else						//normal user
				{
					prompt_showPage( "pg_prompt_winauction" );
				
					window.setTimeout( function() 
					{
						ajax_asyncRequestField( "pull/?cmd=getAuctionData&auction_id=" + urlencode( auctionIDs[ index ] ), ajax_updateWinPrompt, ajax_timeoutCancel, 20000, 'win_auctiondata');
					
					}, 500 );
				}
			}
		}
	}
}


function event_auctionNewBid( index, previousBidder, timeStr )
{
	var d = new Date();
	auctionLastBidTimes[ index ] = d.getTime();
	setTimeout( "timer_auctionNewBid( " + index + " )", 450 );
	
	var nm = "tCounter_" + auctionIDs[ index ];
	timer_updateAuction( nm, auctionIDs[ index ], auctionEndTimes[ index ], auctionTypes[ index ], 1, auctionLastBidTimes[ index ] );
	
	var js_link;
	
	//update bidding history -> lastbidders list in singlebid page
	if( auctionRecordBidderHistory > 0 )
	{
		var ul = document.getElementById( "auction_bidding_history_" + auctionIDs[ index ] );
		
		if( ul != null)
		{
			var li = ul.getElementsByTagName( "li" );
			var newprice = HTMLToNumber ( auctionHighestBids[ index ] ) * 100;
			var lastprice = 0;
			var newtext = "";
			
			if( li.length >= auctionRecordBidderHistory )
				ul.removeChild( li[ li.length - 1 ] );
			
			if( li[ 0 ] != null )
				lastprice = HTMLToNumber ( li[ 0 ].innerHTML.toLowerCase().split( "</span>" )[ 1 ] ) * 100;
			
			if( cookieUser() )	//if login -> create the first <a> into the <var>
			{	
				js_link = "javascript:notification_show('lastbidder_"+ lastprice +"',7,'"+ auctionWinners[ index ] +"')";
				newtext = '<a id="lastbidder_'+ newprice +'" href="'+ js_link +'">'+ auctionWinners[ index ] +'</a><small class="sneak_over_user"></small>';
			}
			else
			{
				newtext = auctionWinners[ index ];
			}
									
			if( ( lastprice > 0 ) && ( newprice - lastprice > 1 ) )
			{
				if( document.getElementById( "lastbidders_warning" ) != null )
					newtext = document.getElementById( "lastbidders_warning" ).innerHTML;
					
				ul.innerHTML = "<li><span>"+ newtext +"</span>&nbsp;</li>" + ul.innerHTML;
			}
			else
			{
				ul.innerHTML = "<li><span><var>" + newtext  + "</var></span>" + auctionHighestBids[ index ] + "</li>" + ul.innerHTML;
			}
		}
	}	
	
	// update savings
	/*
	var rrp = document.getElementById( 'auction_rrp_' + auctionIDs[ index ] );
	var savings = document.getElementById( 'auction_savings_' + auctionIDs[ index ] );
	
	if( ( rrp != null ) && ( savings != null ) )
	{
		var rrpVal = HTMLToNumber( rrp.innerHTML );
		var bidVal = HTMLToNumber( auctionHighestBids[ index ] );
	
		if( bidVal == 0 )
		{
			savings.innerHTML = bidVal.toFixed() + ' %';
		}
		else
		{
			var n = new Number( $n = 100.0 - ( bidVal / rrpVal * 100.0 ) );
			savings.innerHTML = n.toFixed() + ' %';			
			delete n;
		}
		delete rrpVal;
		delete bidVal;
	}
	*/

	// update auction summary
	var sumBid = document.getElementById( "auction_price_summary_amount_" + auctionIDs[ index ] );
	var sumShipping = document.getElementById( "auction_price_summary_shipping_" + auctionIDs[ index ] );
	var sumVAT = document.getElementById( "auction_price_summary_vat_" + auctionIDs[ index ] );
	var sumTotal = document.getElementById( "auction_price_summary_total_" + auctionIDs[ index ] );
	
	if( ( sumBid != null ) && ( sumVAT != null ) && ( sumTotal != null ) )
	{
		var bidVal = HTMLToNumber( auctionHighestBids[ index ] );
		var shippingVal = 0;
		var delShip = false;
		
		if( sumShipping != null )
		{
			shippingVal= HTMLToNumber( sumShipping.innerHTML );
			delShip = true;
		}
			
		var total = bidVal + shippingVal;
		var newVAT = total * ( auctionVATValue - 1.0 ); 
		var newTotal = total + newVAT 

		sumBid.innerHTML = auctionHighestBids[ index ];
		sumVAT.innerHTML = 	auctionHighestBids[ index ].replace( "," , "." ).replace( bidVal.toFixed( 2 ), newVAT.toFixed( 2 ) );
		sumTotal.innerHTML = auctionHighestBids[ index ].replace( "," , "." ).replace( bidVal.toFixed( 2 ), newTotal.toFixed( 2 ) );
		
		delete bidVal;	
		
		if( delShip == true )
			delete shippingVal;	
	}
	delete d;
}


function ajax_updateResponse( url, response )
{
	var j, i, s, cmd, nm, el, d;
	var cl = response.responseText.split( "\n" );
	var up_iID, up_iUT, up_iType, up_iName, up_iBid, up_timeOut;
	var up_hlID, up_hlUT, up_hlType, up_hlName, up_hlBid, up_hlAuctionTitle, up_hlAuctionImageURL, up_timeStr;
	var up_closedID;
	var oldType, oldName, oldUT;
	var up_lTimestamp;
	var hlUpdated = 0;
	var found;
	var wantRefresh = 0;
	var processedCount = 0;
	var iLastUT = auctionEndTimes.slice( 0 );
	//var hlLastUT = featuredEndTime;
	var foundArray = new Array();
	var js_link;

	if( response.responseText.replace( /^\s+|\s+$/g, '' ) == "NOUPDATE" )
	{
		d = new Date();
		dynamic_lastUpdate = d.getTime() / 1000;
		dynamic_requested = 0;
		dynamic_waitingRequest = 0;
		delete d;		
	}
	else
	{
		for( j = 0; j < auctionCount; j++ )
			foundArray[ j ] = 0;

		for( i = 0; i < cl.length; i++ )
		{
			// s = cl[ i ].trim();
			s = cl[ i ];
			
			if( s.length > 0 )
			{
				cmd = s.split( "\t" );
				
				switch( cmd[ 0 ] )
				{
					case "TK":
						dynamic_lastRequestKey = cmd[ 1 ];
						break;
				
					case "TS":
						up_lTimestamp = parseInt( cmd[ 1 ] );
						break;
				
					case "B":
						up_iID = parseInt( cmd[ 1 ] );
						up_iUT = parseInt( cmd[ 2 ] );
						up_iType = parseInt( cmd[ 3 ] );
						up_iName = cmd[ 4 ];
						up_iBid = cmd[ 5 ];
						up_timeOut = parseInt( cmd[ 6 ] );
						up_timeStr = cmd[ 7 ];
						up_shake = parseInt ( cmd [ 8 ] );
						
						/* shaking thing --->
						//if ( parseInt( cmd[ 8 ] ) == 1 )
						//	shake_element( "auction_" + up_iID );
						
						if ( parseInt( cmd[ 8 ] ) == 1 )
						{
							if( typeof abGroup !== "undefined" )
                        	{
                        		if( abGroup < 2 )
								{	
							   		//shake_element( "auction_" + up_iID );
								}
								else
								{
						   		if( auctionWinners[ 0 ] != cookieUser() )		//if you are not the highest bidder
							   		shake_element( "auction_" + up_iID, 1500 );	
						 		}
                    		 }
						}
						<---- shaking thing */
										
						found = 0;
						processedCount++;
					
						for( j = 0; j < auctionCount; j++ )
						{
							if( auctionIDs[ j ] == up_iID )
							{
								found = 1;
								oldName = auctionWinners[ j ];
								oldType = auctionTypes[ j ];
								oldUT = auctionEndTimes[ j ];					
								auctionEndTimes[ j ] = up_iUT;
								auctionTypes[ j ] = up_iType;	
								auctionWinners[ j ] = up_iName;
								auctionHighestBids[ j ] = up_iBid;
								auctionTimeoutTimes[ j ] = up_timeOut;
								foundArray[ j ] = 1;

								el = document.getElementById( "i_" + up_iID ); //get the <var>
								
								if( el != null )
								{
									if( cookieUser() ) //if you are logged in 
									{	
										var elInner =  document.getElementById( "tCurBidder_" + up_iID ); //get the <a> tag
										
										if( elInner == null )
										{
											if( HTMLToNumber( up_iBid ) > 0 )
											{
												js_link = "javascript:notification_show('tCurBidder_"+ up_iID +"',7,'"+ up_iName +"')";
												el.innerHTML = '<a id="tCurBidder_'+ up_iID +'" href="'+ js_link +'">'+ up_iName +'</a><small class="sneak_over_user"></small>';												
											}
										}
										else
										{
											el.innerHTML = el.innerHTML.replace( new RegExp( elInner.innerHTML ,'g'), up_iName );
										}
									}
									else	//if you are logount, only exists the <var>
									{
										el.innerHTML = up_iName;
									}
								}
								
								
								el = document.getElementById( "tCurBid_" + up_iID );
								
								if( el != null )
									el.innerHTML = up_iBid;

								
								//updating numbers in auction detail page => current price and total price based on new current price
								var auction_price_summary_amount = document.getElementById( "auction_price_summary_amount_" + up_iID );
								
								if(auction_price_summary_amount !== null){
									auction_price_summary_amount.innerHTML = up_iBid;
									var auction_price_summary_shipping = document.getElementById( "auction_price_summary_shipping_" + up_iID );
									var auction_price_summary_total = document.getElementById( "auction_price_summary_total_" + up_iID );
									
									var currencyFormatDetails = {};
									var currentPrice = stringCurrencyToFloat( up_iBid, currencyFormatDetails );
									
									var shippingPrice = auction_price_summary_shipping.innerHTML.match(/\d+.\d+/)[0].replace(',', '.');
									shippingPrice = parseFloat(shippingPrice);
									
									var sum = currentPrice + shippingPrice;
									sum = Math.round(sum*100)/100;
									
									var sum = floatCurrencyToString( sum, currencyFormatDetails );
									auction_price_summary_total.innerHTML = sum;
									
								}
								
								el = document.getElementById( "tTimeout_" + up_iID )
								
								if( el != null )
									el.innerHTML = auction_formatTimeout( up_timeOut );
								
								
								if( ( oldName != up_iName ) || ( oldUT < up_iUT ) )
									event_auctionNewBid( j, oldName, up_timeStr );
									
								if( up_iType != oldType )
									event_auctionNewType( j ); 
									
								break;
							}
						}		
						if( ( found == 0 ) && ( ( up_iType == 0	 ) || ( up_iType == 1 ) || ( up_iType == 2 ) || ( up_iType == 10 ) ) )
						{
							wantRefresh = 1;
						}
						break;				
				}		
			}	
		}

		d = new Date();
		dynamic_lastUpdate = d.getTime() / 1000;
		dynamic_requested = 0;
		dynamic_waitingRequest = 0;
		lTimestamp = up_lTimestamp;
	
		var sl = new Number( d.getTime() / 1000 );
		lTimeBegin = sl.toFixed( 0 );
	
		for( i = 0; i < auctionCount; i++ )
		{
			if( auctionTypes[ i ] != 10 ) // paused / sleeping
			{
				if( iLastUT[ i ] != auctionEndTimes[ i ] )
				{
					var nm = "tCounter_" + auctionIDs[ i ];
					timer_updateAuction( nm, auctionIDs[ i ], auctionEndTimes[ i ], auctionTypes[ i ], 1, auctionLastBidTimes[ i ] );
				}
			}
		}

		// remove auctions after they've closed (15 min grace period per back-end solution)
		var execRemove = true;
		
		if( typeof auctionAllowRemoval !== "undefined" )
			if( auctionAllowRemoval == 0 )
				execRemove = false;
				
		if( execRemove == true )
		{
			for( j = 0; j < auctionCount; j++ )
			{	
				if( ( foundArray[ j ] == 0 ) && ( auctionTypes[ j ] == 99 ) )
					auction_removeAuction( auctionIDs[ j ] );
			}
		}
			
		if( ( wantRefresh == 1 ) && ( useForcedRefresh == 1 ) && ( processedCount > 0 ) )
			timedReload();
	
		delete d;
		delete sl;
		delete foundArray;
	}
	delete response;
	response = null;	
}

/**
 * \fn function stringCurrencyToFloat( str, strFormatDetails )
 * \brief Takes a monetary string of various formatting styles and converts it to a float (preserving formatting data in strFormatDetails).
 * types of input accepted: 
		currency symbol -
			- included as an html code or aphabetic string of 3 characters or more
			- with or without a space separating it from the numeric value
		separators & decimals -
			- number expected to contain two decimal places denoted by a comma or period
			- optional visual separators can be commas, periods, or spaces
 * example types of input accepted: "&pound;1,000.00", "1 000.00&euro;", "1.000,00 EUR", "100,00&#8364;", "&pound; 1.000.000,00"
 * \return Returns float version of string passed and records original formatting in pass-by-reference strFormatDetails;
 * contents of strFormatDetails object: 'currency' => string, 'currency_location' => "first" or "last", 'decimal' => "," or ".", 'space_separator' => bool
 */
function stringCurrencyToFloat( str, strFormatDetails )
{
	if( strFormatDetails === undefined )
		var strFormatDetails = {};
	
	if( str.match( /\d\d$/ ) === null ) //currency at the end
	{
		var currency = str.match( /((\D\D\D+)|((\s+)?&#\d+;))$/ )[0]; 
		strFormatDetails[ 'currency_location' ] = 'last';
	}
	else //currency at the beginning
	{
		var currency = str.match( /^((\D\D\D+)|(&#\d+;(\s+)?))/ )[0]; 
		strFormatDetails[ 'currency_location' ] = 'first';
	}
	
	strFormatDetails[ 'currency' ] = currency;
	str = str.replace( currency, '' ); //remove the currency symbol
	strFormatDetails[ 'decimal' ] = str.match( /[,.]\d\d$/ )[0].substr( 0, 1 );
	
	var cents = str.match( /\d\d$/ )[0];
	
	var wholeNumber = str.substr( 0, ( str.length - 3 ) );
	
	if( wholeNumber.match( /\s/ ) !== null ) //if visual separators are spaces
		strFormatDetails[ 'space_separator' ] = true;
	else 
		strFormatDetails[ 'space_separator' ] = false;
	
	wholeNumberCleaned = wholeNumber.replace( /[,.\s]+/g, '' ); //remove visual commas, spaces or periods
	
	var num = parseFloat( wholeNumberCleaned + '.' + cents );
	
	return num;
}
/**
 * \fn function floatCurrencyToString( num, formatDetails )
 * \brief formats num in the currency format defined in formatDetails;
 * expected contents of the formatDetails object (as provided by the function stringCurrencyToFloat): 
 *		'currency' => string, 'currency_location' => "first" or "last", 'decimal' => "," or ".", 'space_separator' => bool
 * if the speparator is not specified to be spaces, it will be the opposite of the decimal (if comma, period; if period, comma)
 * \return returns the formatted string.
 */
function floatCurrencyToString( num, formatDetails )
{	
	num = num.toFixed(2);
	numberString = String(num);
	
	var wholeNumber = numberString.substr( 0, numberString.length - 3 );
	var cents = numberString.substr( numberString.length - 2, 2 );
	
	//determine what to use as a separator
	if( formatDetails[ 'space_separator' ] === true )
	{
		var separator = ' ';
	} else
	{
		if( formatDetails[ 'decimal' ] === '.' )
			var separator = ',';
		else
			var separator = '.';
	}
	
	//add separator to whole number
	var formattedWholeNumber = '';
	var decimalIndex = 0;
	for(i = wholeNumber.length - 1; i >= 0; i--)
	{
		if( ( decimalIndex % 3 === 0 ) && ( decimalIndex !== 0 ) ){
			formattedWholeNumber =  separator + formattedWholeNumber;
		}
		formattedWholeNumber = wholeNumber[i] + formattedWholeNumber;
		decimalIndex++;
	}
	
	numberString = formattedWholeNumber + formatDetails[ 'decimal' ] + cents;
	
	if( formatDetails[ 'currency_location' ] === 'first' )
		numberString = formatDetails[ 'currency' ] + numberString;
	else
		numberString = numberString + formatDetails[ 'currency' ];
	
	return numberString;
}


function timedReload()
{
	var d = new Date();
	var sl = new Number( d.getTime() / 1000 );
	
	if( sl - lTimeOriginal < 120 )
		wantReload = 1;		
	//else
	//	forceReload();
	
	delete d;
	delete sl;
}


function ajax_validateFields( cmd, inputName, inputName2, fieldName )
{
	var i = document.getElementById( inputName );
	var i2 = document.getElementById( inputName2 );
	
	if( ( i == null ) || ( i2 == null ) )
		return;

	var iv = i.value;
	var iv2 = i2.value;
	
	if( ( iv == "" ) || ( iv2 == "" ) )
	{
		var el = document.getElementById( fieldName );
	
		if( el == null )
			return;
		
		el.innerHTML = "";		
		return;		
	} 
	ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&" + urlencode( inputName ) + "=" + urlencode( iv ) + "&" + urlencode( inputName2 ) + "=" + urlencode( iv2 ), ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );
}

var timeRegister;
function ajax_validateField( cmd, inputName, fieldName )
{
	var i = document.getElementById( inputName );
	
	if( i == null )
		return;

	var iv = i.value;
	
	if( iv == "" )
	{
		var el = document.getElementById( fieldName );
	
		if( el == null )
			return;
		
		el.innerHTML = "";		
		return;		
	} 
	ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&" + urlencode( cmd ) + "=" + urlencode( iv ), ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );

	//add for prompts in register page, remobile in use, reemail in use
	window.clearInterval( timeRegister );
		
	timeRegister = window.setTimeout( function() 
	{
		if( document.getElementById( fieldName ).innerHTML.indexOf( inputName + "_in_use" ) > 0 )
		{
			document.getElementById( fieldName ).innerHTML = '';
			prompt_showPage( "pg_prompt_re" + inputName );
		}
	}, 400 );
}


function ajax_responseToField( url, response, fieldName )
{
	var el = document.getElementById( fieldName );
	
	if( el == null )
		return;
		
	el.innerHTML = response.responseText;
}

function ajax_updateWinPrompt(url, response, fieldName){
	var JSONobj = $.parseJSON(response.responseText);
	var auctionData = [ JSONobj.auctionImage, '<h3>', 'You are the winner of ', JSONobj.auctionTitle, '</h3>' ].join(' ');
	var el = document.getElementById( fieldName );
	if( el == null )
		return;
		
	el.innerHTML = auctionData;
}

function ajax_responseToPostField(url, response, fieldName){
	//when responseText is starting with "sorry" we know that user enetered bad post code
	//alert(response.responseText + " " + fieldName);
	if(fieldName === 'result_postcode' && response.responseText.match(/^Sorry/i) !== null ){
		postCodeBridge.clearSearchFlag();
		prompt_showPage('pg_postcode_not_found');
	}else{
		ajax_responseToField( url, response, fieldName );
	}
}


function getEventKey( e )
{
	var k;

	if( e == null )
		return "";

	if( window.event )
		k = e.keyCode;
	else if( e.which )
		k = e.which;
	else
		return "";
	
	return String.fromCharCode( k );
}


function ajax_getSubMarketingChannels( cmd, inputName, fieldName )
{
	var i = document.getElementById( inputName );
	var iv = i.value;

	ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&" + urlencode( inputName ) + "=" + urlencode( iv ), ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );
}


function ajax_getWinnerInterviewDetails( cmd, inputName, fieldName )
{
	var i = document.getElementById( inputName );
	var iv = i.value;

	ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&" + urlencode( inputName ) + "=" + urlencode( iv ), ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );
}


function ajax_getWinnerData( cmd, inputVal, fieldName )
{
    ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&auction_id=" + inputVal, ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );
	
	window.setTimeout( "slide_winners.init()", 500 );
}

function ajax_getAddressesForPostCode(cmd, inputName, fieldName)
{
	var i = document.getElementById( inputName );
	
	if( i == null )
	{
		return;
	}
	
	var iv = i.value;
	
	if(iv !== '')
	{
		postCodeBridge.findAddressClicked(); //notify PostInfo that find button was clicked
		ajax_asyncRequestField( "pull/?cmd=postcode&postcode=" + urlencode( iv ) + "", ajax_responseToPostField, ajax_timeoutCancel, 20000, fieldName );
	}
}


/** 
 * \fn function convertEntities( str )
 * \brief Convert HTML entities into characters
 * \return Returns the converted string   
 * Converts HTML entities such as &auml; to their respective
 * characters (such as '�')
 */   
var entityDiv = null;

function convertEntities( str )
{
	if( entityDiv == null )
	{
		entityDiv = document.createElement( "div" );
		
		if( entityDiv != null )
		{
			// entityDiv.style = "display: none;";
			entityDiv.setAttribute( "style", "display:none" );
		}
	}
	if( entityDiv == null )
		return str;
		
	entityDiv.innerHTML = str;	
	return entityDiv.innerHTML; 
}


/**
 * \fn function auction_formatTimeout( timeoutSeconds )
 * \brief Format auction timeout time to human-readable form
 * \return Human-readable version of the timeout, i.e. "2 min"   
 */
function auction_formatTimeout( timeoutSeconds )
{
	if( timeoutSeconds >= 60 )
		return ( Math.round( ( timeoutSeconds / 60.0 ) * 10.0 ) / 10.0 ) + " " + lShortTime[ 3 ];
	else
		return timeoutSeconds + " " + lShortTime[ 1 ];	
}


/**
 * \fn function ajax_requestNewAuctionData()
 * \brief Request auction data update from the server
 * 
 * Creates a new Ajax call to the server which will call ajax_newAuctionDataReponse when complete  
 */  
function timer_requestNewAuctionData()
{
	var d = new Date();
	var reqTime = d.getTime() / 1000;
	ajax_asyncRequestXML( "xml/auction/update/" + parseInt( reqTime ) + "/", ajax_newAuctionDataResponse, ajax_timeoutCancel, 15000 );
}


/**
 * \fn function ajax_getXMLNodeValue( node, tagName )
 * \brief Retrieve contents of a tag from a node
 * \param node XML node
 * \param tagName XML tag name
 * Retrieves the textual content of tag tagName from the given node.
 * \return Returns the textual content of the tag or an empty string, if the 
 * tag was not found. 
 */
function ajax_getXMLNodeValue( node, tagName )
{
	if( node == null )
		return "";

	var nd = node.getElementsByTagName( tagName );
	
	if( nd != null )
	{
		var n = nd.item( 0 );
		
		if( n != null )
		{
			if( typeof n.textContent !== "undefined" )
				return n.textContent;
			else
				return n.text;
		}
	}		
	return "";
}


/**
 * \fn function ajax_newAuctionDataResponse( url, response )
 * \brief Processes XML data retrieved from xml/auction/update/
 * \param url Request URL
 * \param response Ajax request response object
 * Processes the response XML data and inserts or removes auctions from the auction list when necessary.          
 */
function ajax_newAuctionDataResponse( url, response )
{
	if( response.responseText == "" )
		return;

	var x = response.responseXML;
	// current auctions
	var ad = x.getElementsByTagName( "auction_list" );
	
	if( ad != null )
	{
		var auctionData = ad.item( 0 );
		
		if( auctionData != null )
		{
			if( auctionData.childNodes != null )
			{
				for( var i = 0; i < auctionData.childNodes.length; i++ )
				{
					var node = auctionData.childNodes.item( i );		
					var auctionID = parseInt( ajax_getXMLNodeValue( node, "auction_id" ) );
					
					if( auction_exists( auctionID ) == false )
					{
						if( 
							( 
								// rookie++
								auctionShowRookies == 2
							)
							||
							( 
								// regular rookies
								( auctionShowRookies == 1 ) && ( parseInt( ajax_getXMLNodeValue( node, "flag_new_paid_users" ) ) == 0 ) 
							)
							||
							(
								// users who have won things
							 	( auctionShowRookies == 0 ) && 
								( parseInt( ajax_getXMLNodeValue( node, "flag_new_users_only" ) ) == 0 ) &&
								( parseInt( ajax_getXMLNodeValue( node, "flag_new_paid_users" ) ) == 0 ) 
							)
						) 
						{
							auction_insertAuction( auctionID, node );
						}
					}
				}
			}
			
			auction_resortList( auctionData );
			//auction_rookies();
		}
	}	
	
	response = null;
}


/**
 * \fn function auction_exists( auctionID )
 * \brief Checks whether the specified auction already exists in the auction data arrays
 * \param auctionID Unique auction ID
 * \retval true If auction exists
 * \retval false If auction doesn't exist           
 */
function auction_exists( auctionID )
{
	for( var i = 0; i < auctionCount; i++ )
	{
		if( auctionIDs[ i ] == auctionID )
			return true;
	}
	return false;
}


/**
 * \fn function auction_getAuctionIndex( auctionID )
 * \brief Finds the specified auction from the auction data array
 * \param auctionID Unique auction ID
 * \return Returns the index in the auction data array or -1 if the auction was not found          
 */
function auction_getAuctionIndex( auctionID )
{
	for( var i = 0; i < auctionCount; i++ )
	{
		if( auctionIDs[ i ] == auctionID )
			return i;
	}
	return -1;
}


/**
 * \fn function auction_insertAuction( auctionID, node )
 * \brief Insert a new auction into the auction list and JavaScript data arrays
 * \param auctionID Unique auction ID
 * \param node XML node containing auction data in xml/auction/update/ format
 * Inserts the specified auction data into the JavaScript data arrays and creates a new list entry (LI 'auction_' + auctionID) to 'bidsys_auction_list' list.          
 */
function auction_insertAuction( auctionID, node )
{
	auctionEndTimes[ auctionCount ] = parseInt( ajax_getXMLNodeValue( node, "auction_iut" ) );
	auctionTypes[ auctionCount ] = parseInt( ajax_getXMLNodeValue( node, "auction_itype" ) );
	auctionIDs[ auctionCount ] = auctionID;
	auctionWinners[ auctionCount ] = ajax_getXMLNodeValue( node, "winner" );
	auctionHighestBids[ auctionCount ] = ajax_getXMLNodeValue( node, "cur_bid" );
	auctionLastBidTimes[ auctionCount ] = 0;
	
	var nIndex = auctionCount;
	auctionCount++;
	
	// add list entry
	var p = document.getElementById( "bidsys_auction_list" );
	var c = document.getElementById( "auction___BID_ID__" ); // ___BID_ID__ (three underscore prefix)
	
	if( ( p != null ) && ( c != null ) )
	{
		var d = c.cloneNode( true );
		d.setAttribute( "id", "auction_" + auctionID );
		d.removeAttribute( "style" );
			
		var h = d.innerHTML;
		h = h.replace( /__BID_ID__/g, auctionID );
		h = h.replace( /__BID_TITLE__/g, ajax_getXMLNodeValue( node, "title" ) );
		h = h.replace( /__BID_PRODUCT_IMAGE__/g, ajax_getXMLNodeValue( node, "image_" + auctionImageType ) );
		h = h.replace( /__BID_VALUE__/g, ajax_getXMLNodeValue( node, "item_price" ) );
		h = h.replace( /__BID_TIMEOUT__/g, ajax_getXMLNodeValue( node, "timeout" ) );
		h = h.replace( /__BID_TIME_LEFT__/g, ajax_getXMLNodeValue( node, "time_left" ) );
		h = h.replace( /__BID_AMOUNT__/g, ajax_getXMLNodeValue( node, "cur_bid" ) );
		h = h.replace( /__BID_WINNER_NAME__/g, ajax_getXMLNodeValue( node, "winner" ) );
		h = h.replace( /__BID_DIRECTIONS__/g, ajax_getXMLNodeValue( node, "sms_str" ) );
		h = h.replace( /__URI_BID_TEXT__/g, '' );
		h = h.replace( /__BID_FLAG_CHARITY__/g, '' ); 	//new text for replace

		d.innerHTML = h;
		// p.appendChild( d );	
		// if( ( auctionTypes[ nIndex ] == 0 ) || ( auctionTypes[ nIndex ] == 1 ) || ( auctionTypes[ nIndex ] == 2 ) )
		// {		
		//	if( p.firstChild != null )
		//		p.insertBefore( d, p.firstChild );
		//	else
		//		p.appendChild( d );
		// }
		// else		
		// {
			if( p.firstChild != null )
			{
				var f = p.firstChild;
				var wasInserted = false;
				
				do
				{
					if( f.nodeType == 1 )
					{
						if( f.id != null )
						{
							var curAuctionID = parseInt( HTMLToNumber( f.id ) );						
							var curAuctionIndex = auction_getAuctionIndex( curAuctionID );
							
							if( curAuctionIndex != -1 )
							{
								if( ( auctionTypes[ curAuctionIndex ] > 2 ) && ( auctionTypes[ curAuctionIndex ] != 99 ) )
								{
									p.insertBefore( d, f );
									wasInserted = true;
									break;
								}
							}
						}
					}
					if( f.nextSibling != null )
						f = f.nextSibling;	
					else
						f = null;	
				}
				while( ( f != null ) && ( wasInserted == false ) );

				if( wasInserted == false )
					p.appendChild( d );
			}
			else
			{
				p.appendChild( d );
			}
		// }

		// change product image		
		var pi = document.getElementById( 'auction_product_img_' + auctionID );
		
		if( pi != null )
		{
			pi.setAttribute( 'src', ajax_getXMLNodeValue( node, 'image_' + auctionImageType ) );
			pi.setAttribute( 'alt', ajax_getXMLNodeValue( node, 'title' ) );
		}

		// choose which button is visible
		if( ( auctionTypes[ nIndex ] == 0 ) || ( auctionTypes[ nIndex ] == 1 ) || ( auctionTypes[ nIndex ] == 2 ) || ( auctionTypes[ nIndex ] == 99 ) )
		{
			auction_hideAuctionFlag( auctionID, 'tActionBid', 'block' );
			auction_hideAuctionFlag( auctionID, 'tActionReminder', 'none' );
		}
		else
		{
			auction_hideAuctionFlag( auctionID, 'tActionBid', 'none' );
			auction_hideAuctionFlag( auctionID, 'tActionReminder', 'block' );
		}		

		// choose which flags are visible		
		if( parseInt( ajax_getXMLNodeValue( node, 'flag_buynow' ) ) == 0 )
			auction_hideAuctionFlag( auctionID, 'flag_buynow' );
			
		if( parseInt( ajax_getXMLNodeValue( node, 'flag_is_international' ) ) == 0 )
			auction_hideAuctionFlag( auctionID, 'flag_is_international' );
			
		if( parseInt( ajax_getXMLNodeValue( node, 'flag_free_bidding' ) ) == 0 )
			auction_hideAuctionFlag( auctionID, 'flag_bid4free' );
			
		if( parseInt( ajax_getXMLNodeValue( node, 'flag_new_users_only' ) ) == 0 )
			auction_hideAuctionFlag( auctionID, 'flag_rookie' );
			
		if( parseInt( ajax_getXMLNodeValue( node, 'flag_24h' ) ) == 0 )
			auction_hideAuctionFlag( auctionID, 'flag_24h' );			
			
		var ct = new Date();			
		auction_setNotice( auctionID, ct.getTime(), 120 );
		delete ct;
	}	
}


/**
 * \fn auction_setNotice( auctionID, startTime, speed )
 * \brief Show visual notification to user
 * \param auctionID Unique auction ID
 * \param startTime Time the notification begun
 * \param speed Number of seconds the notification will be played 
 * Displays a visual notification to the user regarding a new, appearing auction.   
 */
function auction_setNotice( auctionID, startTime, speed )
{
	var t = document.getElementById( "auction_title_" + auctionID );
	
	if( t != null )
	{
		var d = new Date();
		
		if( d.getTime() < startTime + ( speed * 1000 ) )
		{
			var startColorR = 0xB2;
			var startColorG = 0xDD;
			var startColorB = 0xF2;
			var endColorR = 0xFF;
			var endColorG = 0xFF;
			var endColorB = 0xFF;
			
			var f = ( d.getTime() - startTime ) / ( speed * 1000.0 );
			
			if( f > 0 )
				f = f;
			
			if( f < 0 )
				f = 0;
				
			if( f > 1 )
				f = 1;
				
			var newR = startColorR + f * ( endColorR - startColorR );
			var newG = startColorG + f * ( endColorG - startColorG );
			var newB = startColorB + f * ( endColorB - startColorB );				
		
			t.style.backgroundColor = "rgb( " + parseInt( newR ) + ", " +  parseInt( newG ) + ", " +  parseInt( newB ) + " )";
				
			setTimeout( "auction_setNotice( " + auctionID + ", " + startTime + ", " + speed + " )", 500 );
		}
		else
		{
			t.style.backgroundColor = "#fff";
		}
		delete d;
	}
}


/**
 * \fn function auction_hideAuctionFlag( auctionID, flagName )
 * \brief Hide a specific auction flag
 * \param auctionID Unique auction ID
 * \param flagName The name of the auction flag
 * Hides the specified auction flag       
 */
function auction_hideAuctionFlag( auctionID, flagName, type )
{
	var f = document.getElementById( flagName + "_" + auctionID );

	if( f != null )	
		f.style.display = type || "none";
}


/**
 * \fn function auction_removeAuction( auctionID )
 * \brief Remove auction from the auction list and data arrays
 * \param auctionID Unique auction ID 
 * Removes existing auction auctionID from the UL called bidsys_auction_list (LI 'auction_' + auctionID) and from the auction data arrays that are used to store data regarding auctions in JavaScript. 
 */
function auction_removeAuction( auctionID )
{
	for( var i = 0; i < auctionCount; i++ )
	{
		if( auctionIDs[ auctionCount ] == auctionID )
		{
			for( var j = i + 1; j < auctionCount; j++ )
			{
				auctionEndTimes[ j - 1 ] = auctionEndTimes[ j ];
				auctionTypes[ j - 1 ] = auctionTypes[ j ];
				auctionIDs[ j - 1 ] = auctionIDs[ j ];
				auctionWinners[ j - 1 ] = auctionWinners[ j ];
				auctionHighestBids[ j - 1 ] = auctionHighestBids[ j ];
				auctionLastBidTimes[ j - 1 ] = auctionLastBidTimes[ j ];
			}		
		
			auctionEndTimes[ auctionCount - 1 ] = 0;
			auctionTypes[ auctionCount - 1 ] = 0;
			auctionIDs[ auctionCount - 1 ] = 0;
			auctionWinners[ auctionCount - 1 ] = "";
			auctionHighestBids[ auctionCount - 1 ] = "";
			auctionLastBidTimes[ auctionCount - 1 ] = 0;
			auctionCount--;
		}
	}
	
	var c = document.getElementById( "auction_" + auctionID );

	if( ( c != null ) && ( c.parentNode.id != "bidsys_auction_list_closed" ) )
		c.parentNode.removeChild( c );
}


function ajax_validateRegisterField( cmd, inputName, fieldName )
{
	var i = document.getElementById( inputName );
    var iv = i.value;
    ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&" + urlencode( cmd ) + "=" + urlencode( iv ), ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );

}

function ajax_userPostCodes( cmd, fieldName )
{ 
    ajax_asyncRequestField( "pull/?cmd=" + urlencode( cmd ) + "&" + urlencode( cmd ), ajax_responseToField, ajax_timeoutCancel, 20000, fieldName );
}


/**
 * \fn function auction_resortList( xml )
 * \brief Re-arrange the <li> elements depends of the order in the xml file
 * \param xml The content of the xml file          
 */
function auction_resortList( xml )
{
	var ul = document.getElementById( "bidsys_auction_list" );
	
	if( ul.getElementsByTagName( "li" ).length >= 0 )
		var li = ul.getElementsByTagName( "li" )[ 0 ];
	else
		return ;
	
	for( var i = 0 ; i < ( xml.childNodes.length ) ; i++ )
	{
		var liID = li.getAttribute( "id" );
		var xmlID = ajax_getXMLNodeValue( xml.childNodes[ i ] , "auction_id" );
		var flagNewUsersOnly = ajax_getXMLNodeValue( xml.childNodes[ i ] , "flag_new_users_only" );

		if( ( ( auctionShowRookies == 0 ) && ( flagNewUsersOnly == 0 ) ) || ( auctionShowRookies >= 1 ) )
		{
			if( ul.innerHTML.indexOf( "auction_" + xmlID ) >= 0 ) //if the id exists
			{
				if( HTMLToNumber( liID ) != HTMLToNumber( xmlID ) ) //if both are not in the same position
				{
					var tmpElement = li.cloneNode( true );
					var tmpLI = document.getElementById( "auction_" + xmlID );
					
					li.innerHTML = tmpLI.innerHTML;
					li.id = "auction_" + xmlID;
					tmpLI.innerHTML = tmpElement.innerHTML;
					tmpLI.id = tmpElement.getAttribute( "id" );
				}
				
				do //move to the next <li> tag
				{
					li = li.nextSibling;
		
					if( li == null )
						return;
				}
				while( li.nodeType != 1 )
			}
		}
	}
}


/**
 * \fn auction_bid( auctionID )
 * \brief the function that is used for placing a bid when clicking, for example BID NOW or REMINDER
 * \param auctionID The ID of the auction to load       
 */
function auction_bid( auctionID )
{	
	var d = new Date();
	var thisClickTime = d.getTime();

	//reset redirect to expired page
	Redirect.reset();

	if( ( auctionID == lastBidClickID ) && ( ( thisClickTime - lastBidClickTime ) < 2000 ) )	//if you click two times in the same bid and takes less than 2.5 seconds
	{ 
		notification_show( "tActionBid_" + auctionID, 6, "" );
	}	
	else
	{
		lastBidClickTime = thisClickTime;
		
		var current = setTimeout( "notification_show( 'tActionBid_" + auctionID + "', 6, '')", 750 );
	
		ajax_asyncRequestXML( "xml/bid/" + auctionID + "/", ajax_processAuctionBid, ajax_timeoutCancel, 30000, current );
		
		if( pageTracker != null )
			pageTracker._trackEvent( "bidbutton", window.location.pathname, auctionID );
			
	}	
	
	lastBidClickID = auctionID;
}


/**
 * \fn auction_remind( auctionID )
 * \brief process a reminder when user click 'Set Reminder' in the reminder box
 * \param auctionID The ID of the auction to load
  * \param timout the time in minutes       
 */
function auction_remind( auctionID, timeout )
{
	var current = setTimeout( "notification_show( 'tActionReminder_" + auctionID + "', 6, '')", 750 );	
	ajax_asyncRequestXML( "xml/reminder/" + auctionID + "/" + timeout + "/", ajax_processAuctionBid, ajax_timeoutCancel, 30000, current );
}


/**
 * \fn user_setBidsLeft( bidCount )
 * \brief Update user's bid count to bidCount, in the sidebar_membercontrols
 */

function user_setBidsLeft( bidCount )
{
	var el = document.getElementById( "user_bids_left" );
	
	if( el != null )
		el.innerHTML = bidCount;
}


var lastBidClickTime = dynamic_time.getTime();
var lastBidClickID;


/**
 * \fn ajax_processAuctionBid( auctionID )
 * \brief Process the content of a xml for a auction and show prompts, or notifications box    
 */
function ajax_processAuctionBid( url, response, current )
{
	if( response.responseText == "" )
		return;
	
	clearTimeout( current );
	lastBidClickID = 0;

	var xml = response.responseXML;

	if( xml.getElementsByTagName( "result" ).length >= 0 ) 
		xml = xml.getElementsByTagName( "result" )[ 0 ];
	else
		return;	
		
	var auctionState = parseInt( ajax_getXMLNodeValue( xml , "state" ) );
	var auctionID = ajax_getXMLNodeValue( xml , "auction_id" );
	var nameID = "tActionBid"; //if is a bidnow button

/*fake auction*/
var auctionRealID = parseInt( ajax_getXMLNodeValue( xml , "object_id" ) );

if( auctionRealID > 0 )
{
	var auctionFakeIndex = auction_getAuctionIndex( auctionID );
	var auctionFake = document.getElementById( "auction_" + auctionID );
	
	auctionFake.id = "auction_" + auctionRealID;
	auctionFake.innerHTML = auctionFake.innerHTML.replace( new RegExp( auctionID, "g" ), auctionRealID );
	
	auctionIDs[ auctionFakeIndex ] = auctionRealID;
	auctionID = auctionRealID;
}
/*fake auction*/

	if( document.getElementById( "tActionReminder_" + auctionID ) != null )
	{
		if( document.getElementById( "tActionReminder_" + auctionID ).style.display != "none" )
			nameID = "tActionReminder"; //if is a reminder button
	}

	switch( auctionState )
	{
		case 401: //users need to login
			notification_show( nameID + '_' + auctionID, 4, '' ); //notification_type 4 means this is a login box
			break;

		case 307: //users will be redirect
			if( ajax_getXMLNodeValue( xml , "message" ) != null )
				window.location.href = ajax_getXMLNodeValue( xml , "message" );
			break;	

		case 406: //normal error
			break;
			
		case 200: //success
			if( nameID == "tActionBid" ) //success bidding
			{
				var auctionIndex = auction_getAuctionIndex( auctionID );
				var oldWinner = auctionWinners[ auctionIndex ];
				
				//update the values of the javascript variables
				auctionEndTimes[ auctionIndex ] = parseInt( ajax_getXMLNodeValue( xml, "auction_iut" ) );
				auctionTypes[ auctionIndex ] = parseInt( ajax_getXMLNodeValue( xml, "auction_itype" ) );
				auctionWinners[ auctionIndex ] = ajax_getXMLNodeValue( xml, "winner" );
				auctionHighestBids[ auctionIndex ] = ajax_getXMLNodeValue( xml, "cur_bid" );
				event_auctionNewBid( auctionIndex , oldWinner, ajax_getXMLNodeValue( xml , "auction_formatted_time" ) );
		
				if( ajax_getXMLNodeValue( xml, "funding_new_total" ) != "" )
				{
					if( document.getElementById( "unused_bids" ) != null )
						document.getElementById( "unused_bids" ).innerHTML = ajax_getXMLNodeValue( xml , "funding_new_total" );
				}		
				
				// var elementIDtCounter = document.getElementById( 'tCounter_' + auctionID );
				var elementIDtCurBidder = document.getElementById( "tCurBidder_" + auctionID );
				
				// if( elementIDtCounter != null )
				// 	elementIDtCounter.innerHTML = '<span class="bid_time">'+ ajax_getXMLNodeValue( xml , 'time_left ' ) +'</span>';
					
				if( elementIDtCurBidder != null )
					elementIDtCurBidder.parentNode.innerHTML = elementIDtCurBidder.parentNode.innerHTML.replace( new RegExp( elementIDtCurBidder.innerHTML ,"g" ), ajax_getXMLNodeValue( xml, "winner" ) );

					
				if( ajax_getXMLNodeValue( xml, "user_bids_left" ) != "" )
					//user_setBidsLeft( ajax_getXMLNodeValue( xml, "user_bids_left" ) );
					toolbar.setBidsLeft(ajax_getXMLNodeValue( xml, "user_bids_left" ));
					
				//auction_highlight( auctionID );
				
				/*fake auction*/
				var d = new Date();
				var curTime = d.getTime();
				var nm = "tCounter_" + auctionIDs[ auctionIndex ];
				
				timer_updateAuction( nm, auctionIDs[ auctionIndex ], auctionEndTimes[ auctionIndex ], auctionTypes[ auctionIndex ], 1, auctionLastBidTimes[ auctionIndex ], curTime );
				/*fake auction*/
				
				
			}
			break;
		
		default:
			break;
	}

	if( ajax_getXMLNodeValue( xml , "message_type" ) != "" ) //to show a prompt
	{
		var auctionMessageType = parseInt( ajax_getXMLNodeValue( xml , "message_type" ) );
		var auctionMessageTitle = ajax_getXMLNodeValue( xml , "message_title" );
		var auctionMessageContent = ajax_getXMLNodeValue( xml , "message_content" );
		var linkURL = "javascript:prompt_close()";
		var linkText = "Close";
	
		if( ajax_getXMLNodeValue( xml , "message_url" ) != "" )
			linkURL = ajax_getXMLNodeValue( xml , "message_url" );

		if( ajax_getXMLNodeValue( xml , "message_button_title" ) != "" )
			linkText = ajax_getXMLNodeValue( xml , "message_button_title" );
				
		prompt_show( auctionMessageTitle, auctionMessageContent, auctionMessageType, linkURL, linkText );
	}
	
	if( ajax_getXMLNodeValue( xml , "notification_type" ) != "" ) //to show a notification	
	{
		var auctionNotificationType = ajax_getXMLNodeValue( xml , "notification_type" );
		var auctionNotificationContent = ajax_getXMLNodeValue( xml , "notification_content" );
		
		notification_show( nameID + "_" + auctionID, auctionNotificationType, auctionNotificationContent );
	}
}


/**
 * \fn prompt_show( textTitle, textMessage, imgType )
 * \brief Show a centered modal box with a black background and display a message. HTML code in dynamic-boxes.html
 * \param textTitle, the heading of the prompt
 * \param textMessage, the text of the prompt
 * \param imgType, the type of image to display
 * \param linkURL, the URL for the link
 * \param linkText, the text for the link of the bottom
 */
function prompt_show( textTitle, textMessage, imgType, linkURL, linkText )
{
	if( document.getElementById( "prompt_black" ) == null )
	{		
		var divWindow = document.createElement( "div" );
		var content = "";
		var className;
		var newLinkURL = linkURL || "javascript:prompt_close()";
		var newLinkText = linkText || "close";
			
		divWindow.id = "prompt_black";
		divWindow.innerHTML = "&nbsp;";
		document.body.appendChild( divWindow );

		switch( imgType )
		{
			case 1: //sucess
				className = "i_sucess";
				break;
			
			case 2: //error	
			case 3:	//warning
		 		className = "i_warning";
				break;
			
			case 4:	//top up
				className = "i_dial";
				break;
				
			case 5:	//rookie
				className = "i_rookie";
				break;	
			
			case 6:	//bid for free
				className = "i_free";
				break;	

			case 7:	//offer - money pig
				className = "i_offer";
				break;
			
			default: //default
				className = "";
				break;
		}

		if( document.getElementById( "box_prompt" ) != null )
		{
			content = document.getElementById( "box_prompt" ).innerHTML;
			content = content.replace( "h1text", textTitle );
			content = content.replace( "h3text", textMessage );
			content = content.replace( "classimg", className );
			content = content.replace( "#", newLinkURL );
			content = content.replace( "atext", newLinkText );
		}

		divWindow = document.createElement( "div" );
		divWindow.id = "prompt_page";
		divWindow.innerHTML = content;
		document.body.appendChild( divWindow );
	}
}


/**
 * \fn prompt_showPage( fileURL )
 * \brief Show a centered modal box with a black background and load an external file
 * \param fileURL, the URL of the xml to load
 * \param singlehtml, true if the file is a single html or form, not as xml from listpages
 */
function prompt_showPage( fileName, singlehtml )
{
	prompt_close();
		
	var divWindow = document.createElement( "div" );
	divWindow.id = "prompt_black";
	divWindow.innerHTML = "&nbsp;";
	document.body.appendChild( divWindow );
	
	divWindow = document.createElement( "div" );
	divWindow.id = "prompt_page";
	
	if( document.getElementById( "prompt_loading" ) != null )
		divWindow.innerHTML = document.getElementById( "prompt_loading" ).innerHTML ;
		
	document.body.appendChild( divWindow );

	if( singlehtml == true )
		ajax_asyncRequestPage( fileName, ajax_setPageContent, ajax_timeoutCancel, 5000, "" );
	else
		ajax_asyncRequestXML( "xml/page/" + fileName, ajax_setPromptContent, ajax_timeoutCancel, 5000, "" );
}


/**
 * \fn prompt_close()
 * \brief Destroy/hide the modal box which includes a page
 */
function prompt_close(){
	$('#prompt_black').remove();
	$('#prompt_page iframe').hide();//ie8 workaround for removing iframe so that does not show black page
	$('#prompt_page').remove();
}


/**
 * \fn ajax_setPromptContent( url, response )
 * \brief Process the content of a xml and set into the prompt
 */
function ajax_setPromptContent( url, response )
{
	if( response.responseText == "" )
		return;
	
	var xml = response.responseXML;
	var state;
	var prompt_page = document.getElementById( "prompt_page" );

	if( xml.getElementsByTagName( "result" ).length >= 0 )
	{
		xml = xml.getElementsByTagName( "result" )[ 0 ];
		state = parseInt( ajax_getXMLNodeValue( xml , "state" ) );
	}

	if( ( state == 200 ) && ( prompt_page != null ) )
	{
		prompt_page.getElementsByTagName( "p" )[ 0 ].innerHTML = "";
		prompt_page.innerHTML += ajax_getXMLNodeValue( xml , "message" );
	}	
	
	if(url.match(/how/)!== null ){
		prompt_page.setAttribute("class", "buy_now"); //this is because of the design
	}
}


/**
 * \fn ajax_setPageContent( url, response )
 * \brief Process the content of a html and set into the prompt
 */
function ajax_setPageContent ( url, response )
{
	if( response.responseText == '' )
		return;

	var prompt_page = document.getElementById( "prompt_page" );

	if( prompt_page!= null )
	{
		if(url.match(/buynow/)!== null ){
			prompt_page.setAttribute("class", "buy_now"); //this is because of the design
		}
		
		prompt_page.getElementsByTagName( "p" )[ 0 ].innerHTML = "";
		prompt_page.innerHTML += response.responseText;
	}
}





/**
 * \fn notification_show( id, notificationType, notificationContent )
 * \brief Show a small black box to dislpay a message. HTML code in dynamic-boxes.html
 * \param auctionID Unique auction ID          
 */
var prevNotificationTimeout = null;

function notification_show( auctionID, notificationType, notificationContent )
{
	notification_hide();
	
	var newDiv = document.createElement( "div" );
	var obj = document.getElementById( auctionID );
	var pos = element_getXY( obj );
	var content;
	var delay = 3000;

	if( ( newDiv != null ) &&  ( obj != null ) )
	{
		switch( parseInt( notificationType ) )
		{	
			case 1:	//message box success
				delay = 1000;
				
			case 2: //message box error
			case 3: //message box warning
			
				if( document.getElementById( "box_message" ) != null )
				{
					content = document.getElementById( "box_message" ).innerHTML;
					content = content.replace( "messagebox", notificationContent );
				}
				break;
				
			case 4:	//login box
				if( document.getElementById( "box_login" ) != null )
					content = document.getElementById( "box_login" ).innerHTML;
				break;
				
			case 5:	//reminder box
				if( document.getElementById( "box_reminder" ) != null )
				{			
					content = document.getElementById( "box_reminder" ).innerHTML;
					content = content.replace( "auction_remind()", "auction_remind( '"+ auctionID.split( "_" )[ 1 ] +"', document.getElementById( 'timeout' ).value )" );
					document.getElementById( "timeout" ).value = 5;
				}
				break;
				
			case 6:	//loading box
				if( document.getElementById( "box_loading" ) != null )
					content = document.getElementById( "box_loading" ).innerHTML;
				break;
			
			case 7: //sneak box
				if( document.getElementById( "box_sneak" ) != null )		
				{	
					content = document.getElementById( "box_sneak" ).innerHTML;
					content = content.replace( "replaceUser", notificationContent );
					content = content.replace( "replaceID", auctionID );
					content = content.replace( "temp_sneak_message", "m_message" );
				}
				break;
	
			case 8: //moreinfo box, in singlebid_bid.html
				if( document.getElementById( "box_moreinfo" ) != null )
				{
					content = document.getElementById( "box_moreinfo" ).innerHTML;
					content = content.replace( "replaceID", auctionIDs[ 0 ] );
					content = content.replace( "temp_moreinfo_message", "m_message" );
				}
				break;
				
			default:
				content = "";
				break;	
		}
		
		
		var el = document.getElementById( "tooltip" );
		
		if( el != null )
			document.body.removeChild( el );
		
		if( prevNotificationTimeout != null )
		{
			clearTimeout( prevNotificationTimeout );
			prevNotificationTimeout = null;
		}	
		
		newDiv.id = "tooltip";
		newDiv.innerHTML = content;
		document.body.appendChild( newDiv );
		newDiv.style.top = pos.y - ( newDiv.offsetHeight ) + "px";
		newDiv.style.left = pos.x + ( obj.offsetWidth/2 ) - ( newDiv.offsetWidth/2 ) + "px";
		newDiv.style.display = "none";

		element_fade( newDiv, 150 );
		
		if( notificationType < 4 ) //the notification boxes which closes itself automaticly are 1,2,3
		{
			prevNotificationTimeout = setTimeout( function() 
			{ 	
				element_fade( newDiv, 400, 
					function( nd ) 
					{
						el = document.getElementById( "tooltip" );
						
						if( el != null )
							document.body.removeChild( el );
							
						prevNotificationTimeout = null;
					},
					true );
			}, 
			delay );
		}
	}
}


/**
 * \fn notification_hide()
 * \brief Hide/Destroy the notification box      
 */
function notification_hide()
{
	var tooltip = document.getElementById( "tooltip" );
	
	if( tooltip != null )
		document.body.removeChild( tooltip );
} 


/*
// \fn how_demoAuction( highestBidder ) \brief Simulate the performance for bidding in /how page
// \param highestBidder: 'auto' or 'you' if the bidder is machine or user
var count;

function how_demoAuction( highestBidder )
{
	if( document.getElementById( "bidder" ).innerHTML.indexOf( "YOU" ) == -1 )
	{
		var pennys = parseInt( document.getElementById( "pennys" ).innerHTML, 10 ) + 1;
	
		if( pennys == 100 )
		{
			document.getElementById( "pennys" ).innerHTML = "00";
			document.getElementById( "pounds" ).innerHTML = parseInt( document.getElementById( "pounds" ).innerHTML, 10 ) +1 ;
		}
		else
		{
			document.getElementById( "pennys" ).innerHTML = ( pennys < 10 )? "0" + pennys : pennys;
		}
		
		if( highestBidder == "you" )
			document.getElementById( "bidder" ).innerHTML = "Highest Bidder: <span style='color:#f00;font-weight:bold;'>YOU</span>";
		else
			document.getElementById( "bidder" ).innerHTML = "Highest Bidder: User" + ( Math.floor( Math.random()*100 ) + 100 );
			
		clearTimeout( count );
		how_countDown( 30 );
	}
	
}
// \fn how_countDown( newSeconds ) \brief Simulate the countdown for bidding an auction
// \param newSeconds, the number of seconds to start
function how_countDown( newSeconds )
{
	newSeconds--;
	count = setTimeout( "how_countDown( " + newSeconds + " )", 1000 );	
	
	if( newSeconds == ( 30-1 ) ) //number in orange
	{
		document.getElementById( "seconds" ).innerHTML = "<span style='background:#fb0;color:#fff;padding:0 5px'>" + newSeconds + " s</span>";
	}	
	else if( newSeconds <= 10 ) //number in red
	{
		document.getElementById( "seconds" ).innerHTML = "<span style='background:#f00;color:#fff;padding:0 5px'>" + newSeconds + " s</span>";
		
		if( newSeconds == -1 )
		{
			clearTimeout( count );
			document.getElementById( "seconds" ).innerHTML = "Auction Closed";
			document.getElementById( "price" ).innerHTML = "FREE";
			document.getElementById( "details" ).innerHTML = "You are now ready to try our real auctions";
			document.getElementById( "howbutton" ).innerHTML = "register now";
			document.getElementById( "howbutton" ).setAttribute( "href", "register/" );
		}
	}
	else //normal number
	{
		document.getElementById( "seconds" ).innerHTML = newSeconds + " s";
	}
			
	if( ( newSeconds == 5 ) && ( document.getElementById( "bidder" ).innerHTML.indexOf( "YOU" ) == -1 ) )
		how_demoAuction( "auto" );			
}
*/


/**
 * \fn Section( name )
 * \brief Class for handle pages with 2 columns, like faq/ about/ or winners/
 * \param name, the name of the object
 */
function Section( name )
{
	var lastDiv = 0;
	var div = document.getElementById( "static_right" ).getElementsByTagName( "div" );
	var a = document.getElementById( "static_left" ).getElementsByTagName( "a" );
	
	this.name = name;
	
	this.show = function( index )
	{
		if( document.getElementById( "search_input" ) != null ) // ugly
			this.searchReset();
			
		document.getElementById( "section" + lastDiv ).style.display = "none";
		document.getElementById( "section" + index ).style.display = "block";
		document.getElementById( "link" + lastDiv ).className = "none";
		document.getElementById( "link" + index ).className = "selected";

		lastDiv = index;
		window.scrollTo(0,0);
		//element_fade( div[ index ], 1500 );
	};
	this.subsection = function ( indexTag )
	{
		var section = document.getElementById( "static_left" ).getElementsByTagName( "div" );
		
		if( section[ indexTag ] != null )
		{
			if( section[ indexTag ].style.display == "block" )
				section[ indexTag ].style.display = "none";
			else
				section[ indexTag ].style.display = "block";
		}
	};
	this.searchText = function ()
	{
		var text = document.getElementById( "search_input" ).value;
		text = text.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); //trim
		text = text.toLowerCase();
		
		this.searchReset();
		
		if( text != '' )
		{
			var noMatch = 0;
			var content;
			
			for( var i = 0; i < div.length; i++ )
			{
				content = div[ i ].innerHTML;
				
				if( content.toLowerCase().indexOf( text ) != -1 )
				{
					var regular = new RegExp( text, "gi" );
					var isInsideTag = true;
					var textInsideTag = "";
					var arrayTags = new Array();
					var arrayCount = 0;
				
					for( var j = 0; j <= content.length; j++ )
					{
						if( content.charAt( j ) == "<" ) //start of the tag
						{
							isInsideTag = true;
							textInsideTag += content.charAt( j );
						}
						else if( content.charAt( j ) == ">" ) //eng of the tag
						{
							isInsideTag = false;
							textInsideTag += content.charAt( j );
							
							arrayTags[ arrayCount ] = textInsideTag;
							arrayCount ++;
							textInsideTag = "";
						}
						else //between the < and > tags
						{
							if( isInsideTag == true )
								textInsideTag += content.charAt( j );	
						}
					}
			
					for( var k in arrayTags ) //remove the tags for ###
						content = content.replace( arrayTags[ k ], "###" + k );
						
					//content = content.replace( regular, "<span class='search_text'>" + text + "</span>" );
	
					for( var m = 0; m < content.length - text.length; m++ )
					{
						if( content.substring( m, m + text.length ).toLowerCase() == text.toLowerCase() )
						{
							var replacement = "<span class='search_text'>" + content.substring( m, m + text.length ) + "</span>";
							content = content.substring( 0, m ) + replacement + content.substring( m + text.length );
							m += replacement.length - 1;
						}
					}
				
					for( var k in arrayTags ) //retrieve the tags with ###
						content = content.replace( "###" + k, arrayTags[ k ] );
				
					div[ i ].innerHTML = content;
					div[ i ].style.display = "block";	
				}
				else
				{
					div[ i ].style.display = "none";
					noMatch ++;
				}	
			}
	
			if( noMatch == div.length )
				document.getElementById( "search_result" ).innerHTML = "Your search <b>'" + text + "'</b> did not match";
			else
				document.getElementById( "search_result" ).innerHTML = "Searches related to <b>'"+ text +"'</b>";
		}
		else
			document.getElementById( "search_result" ).innerHTML = "<b>Your search did not match</b>";
	};
	this.searchReset = function ()
	{
		document.getElementById( "static_right" ).innerHTML = document.getElementById( "static_right" ).innerHTML.replace( /search_text/g, '' );
		document.getElementById( "search_input" ).value = "";
		document.getElementById( "search_result" ).innerHTML = "";
		
		for( var i = 0; i < div.length; i++ )
			div[i].style.display = "none";
	};
	this.searchPressKey = function (e)
	{
		var key;

		if( window.event )
			key = e.keyCode;
		if( e.which )
			key = e.which;
		if( key == 13 ) //enter key
			this.searchText();	
	};
	
	if( window.location.hash != '' )
		this.show( window.location.hash.split( "#" )[ 1 ] );
	else
		this.show( 0 );
}


/**
 * \fn loadScriptAsync( url, callback )
 * \brief Calls the callback after an external javascript is asynchronously loaded. The JS could not be on the same domain.
 * \param url, the url of the script
 * \param callback, the callback function
 */
function loadScriptAsync( url, callback )
{
	var script = document.createElement( "script" );
	script.type = "text/javascript";
	script.async = true;
	
	if( script.readyState )
	{
		script.onreadystatechange = function()
		{
			if( ( script.readyState === "loaded" ) || ( script.readyState === "complete" ) )
			{
				script.onreadystatechange = null;
				callback();
			}
		};
	} 
	else 
	{
		script.onload = function()
		{
			callback();
		};
	}
	
	script.src = url;
	document.getElementsByTagName( "head" )[ 0 ].appendChild( script );
}


function displayBankDetails(num) 
{
	var extra_data = document.getElementById( "extra_data_" + num );
	
	if( extra_data != null )
	{
		document.getElementById( "extra_data_" + ( 3 - num ) ).style.display = "none";
		extra_data.style.display = "block";	
	}
}


function displayCardIssueNumber( obj, text, id, autohide ) 
{
	if( autohide == false )
		return;

	if( document.getElementById( id ) != null)
	{
		txt = obj.options[ obj.selectedIndex ].value;
		document.getElementById( id ).style.display = "none";
		textArray = text.split( ":" );
		
		if( txt.match( textArray[ 0 ] ) ||  txt.match( textArray[ 1 ] ) )  
			document.getElementById( id ).style.display = "block";
	}
}


function displayCardIssueDate( obj, text, id, autohide ) 
{
	if( autohide == false )
		return;

	if( document.getElementById( id ) != null )
	{
		txt = obj.options[ obj.selectedIndex ].value;
		document.getElementById( id ).style.display = "none";

		textArray = text.split( ":" );
		if( txt.match( textArray[ 0 ] ) ||  txt.match( textArray[ 1 ] ) )  
			document.getElementById( id ).style.display = "inline-block";
	}
}


function sortSearchByOption( url, val, catCode)
{
	newUrl = url + val + '/0';
	
	if( catCode != '' )
		newUrl = newUrl + '/' + catCode;

	window.location.href= newUrl;
}


function sortProductsByOption( url, val )
{
	newUrl = url + val + '/0';
	
	window.location.href= newUrl;
}


function submitSearchNextNavigationForm( page )
{
	document.getElementById( "next" + page ).submit(); 
}


function submitSearchPrevNavigationForm( page )
{
	document.getElementById( "prev" + page ).submit(); 
}


/**
 * \fn SubBar( )
 * \brief Class for create/handle the sneak a peek sub bar (and notification boxes)
 */
function SubBar()
{
	var tag = document.getElementById( "sub_bar" );
	var old_parent = 1;					//index element show by default
	var old_menu = "my_whosneak";		//id element show by default
	var old_left = -1;					//-1 means the first time
	
	this.show = function( value )		//( "open" ) or ( "close" )
	{
		tag.className = value;	
	};
	
	this.toggle = function ()			//the right button to open/close, in the subbar
	{
		if( tag.className == "close" )
		{
			if( old_left == -1 )		//the first time
				this.submenu( old_menu, old_parent );
			else
				this.show( "open" );	
		}
		else
			this.show( "close" );	
	};
	
	this.submenu = function( name, parent ) //clicking the links of the submenu
	{
		if( tag.className == "close" )
			this.show( "open" );
			
		document.getElementById( "sublink" + old_parent ).className = "unselected";
		document.getElementById( "sublink" + parent ).className = "selected";
		document.getElementById( "sub" + old_parent ).style.display = "none";
		document.getElementById( "sub" + parent ).style.display = "block";
		document.getElementById( old_menu ).className = "unselected";
		document.getElementById( name ).className = "selected";
		document.getElementById( "div_" + old_menu ).style.display = "none";
		document.getElementById( "div_" + name ).style.display = "block";
		
		old_parent = parent;
		old_menu = name;
		old_left = 0;
		
		switch( name )
		{
			case "my_whosneak":
				ajax_asyncRequestField( "pull/?cmd=" + name, ajax_responseToField, ajax_timeoutCancel, 20000, "nav_" + name );
				ajax_asyncRequestField( "pull/?cmd=getdefaultsneakdetails", ajax_responseToField, ajax_timeoutCancel, 20000, "section_" + name );
				this.getSneaks();
			break;
			
			case "my_whoisneak":
				ajax_asyncRequestField( "pull/?cmd=" + name, ajax_responseToField, ajax_timeoutCancel, 20000, "nav_" + name );
				ajax_asyncRequestField( "pull/?cmd=getdefaultmysneakdetails", ajax_responseToField, ajax_timeoutCancel, 20000, "section_" + name );
			break;
			
			case "my_auction":
				ajax_asyncRequestField( "pull/?cmd=" + name, ajax_responseToField, ajax_timeoutCancel, 20000, "nav_" + name );
				ajax_asyncRequestField( "pull/?cmd=getdefaultauctiondetails", ajax_responseToField, ajax_timeoutCancel, 20000, "section_" + name );
			break;
			
			case "my_stealth":
				ajax_asyncRequestField( "pull/?cmd=" + name, ajax_responseToField, ajax_timeoutCancel, 20000, "section_" + name );
			break;
			
			default:
			break;
		}
	};
	
	this.leftmenu = function( name, index, param1, param2 )	//clicking in the links of the <ul>, leftmenu
	{
		var links = document.getElementById( "nav_" + old_menu ).getElementsByTagName( "a" );
		links[ old_left ].className = "unselected";
		links[ index ].className = "selected";
		old_left = index;
		
		switch( name )
		{
			case "get_auction":
				ajax_asyncRequestField( "pull/?cmd=" + name + "&auction_id=" + urlencode( param1 ) + "&user_action_id=" + urlencode( param2 ), ajax_responseToField, ajax_timeoutCancel, 20000, "section_my_auction" );
			break;
			
			case "get_user":
				ajax_asyncRequestField( "pull/?cmd=" + name + "&targetUserName=" + urlencode( param1 ) + "&user_notification_id=" + urlencode( param2 ), ajax_responseToField, ajax_timeoutCancel, 20000, "section_my_whosneak" );
			break;
			
			case "get_mysneak":
				ajax_asyncRequestField( "pull/?cmd=" + name + "&targetUserName=" + urlencode( param1 ) + "&user_action_id=" + urlencode( param2 ), ajax_responseToField, ajax_timeoutCancel, 20000, "section_my_whoisneak" );
			break;
			
			default:
			break;
		}
	};
	
	this.getSneaks = function() 				//refresh the sneaks number 'in red', in the header of the subbar
	{	
		ajax_asyncRequestField( "pull/?cmd=sneaks", ajax_responseToField, ajax_timeoutCancel, 20000, "usersneaks" );
	};
	
	this.buyStealth = function( confirmed )		//clicking the link in the 'stealth' section
	{		
		if( confirmed == "true" )
		{
			ajax_asyncRequestField( "pull/?cmd=buystealthmode", ajax_responseToField, ajax_timeoutCancel, 20000, "b_message" );
			user_setBidsLeft( document.cookie.split( cookiePrefix + "bidsys_credits=" )[ 1 ].split( ";" )[ 0 ] );
		}
		else if( confirmed == "cancel" )
		{
			document.getElementById( "b_message2" ).style.display = "none";
		}
		else
		{
			document.getElementById( "b_message2" ).style.display = "block";
		}
	};
	
	this.getSneaks();
	
	if( ( document.cookie.indexOf( cookiePrefix + "sneak_last_update=" ) != -1 ) && ( document.cookie.indexOf( cookiePrefix + "sneak_num_new_notifications=" ) != -1 ) )
	{
		var sneakLastUpdate =  document.cookie.split( cookiePrefix + "sneak_last_update=" )[ 1 ].split( ";" )[ 0 ];
		var sneakNumNewNotifications =  document.cookie.split( cookiePrefix + "sneak_num_new_notifications=" )[ 1 ].split( ";" )[ 0 ];
		var now = new Date().getTime()/1000;
		var diff = 900000;
		
		if ( ( ( now - diff ) > sneakLastUpdate ) && ( sneakNumNewNotifications == "0" ) )
			window.setInterval( this.getSneaks, diff );
	}
}

/**
 * \fn removeSub_bar()
 * \brief This function remove the sub_bar that contains sneak-a-peek information from 'confirm_mobile' and 'confirm' pages
 */
function removeSub_bar()
{
	var item = document.getElementById( 'sub_bar' );
	item.parentNode.removeChild( item );
}


/**
 * \fn sneakUser( targetUserName, auctionID, confirmed )
 * \brief Clicking on the 'username'-> tooltip -> button (in frontbid,closebid,easyview,etc)
 * \param targetUserName, the name of the target
 * \param auctionID, the id of the auction involved
 * \param confirmed, when the user click the second time
 */
function sneakUser( targetUserName, auctionID, confirmed ) 	//
{		
	if( confirmed == "true" )
	{
		notification_hide();
		//bar.submenu( "my_whoisneak", 1 );
		//user_setBidsLeft( document.cookie.split( cookiePrefix + "bidsys_credits=" )[ 1 ].split( ";" )[ 0 ] );
		prompt_showPage("pull/?cmd=getdefaultmysneakdetails",true);
		$('#prompt_page').addClass('sneak_prompt pscroll');
		toolbar.setBidsLeft();
		
	}
	else
	{
		if( document.getElementById( "m_message" ) != null )
		{
			if( document.getElementById( "box_loading" ) != null )
				document.getElementById( "m_message" ).innerHTML = document.getElementById( "box_loading" ).innerHTML;	
				
			ajax_asyncRequestField( "pull/?cmd=sneakauser&target_username=" + urlencode( targetUserName ), ajax_getSneakValue, ajax_timeoutCancel, 20000, auctionID );
		}
	}
}


/**
 * \fn sneakAuction( auctionID, confirmed )
 * \brief Clicking 'complete bidding'->tooltip-> button (in singlebid)
 * \param auctionID, the id of the auction involved
 * \param confirmed, when the user click the second time
 */
function sneakAuction( auctionID, confirmed )
{
	if( confirmed == "true" )
	{
		notification_hide();
		//bar.submenu( "my_auction", 2 );
		//user_setBidsLeft( document.cookie.split( cookiePrefix + "bidsys_credits=" )[ 1 ].split( ";" )[ 0 ] );
		//prompt_showPage("pull/?cmd=get_auction&auction_id=" + auctionID,true);
		prompt_showPage("pull/?cmd=get_auction&auction_id=" + auctionID,true);
		$('#prompt_page').addClass('sneak_prompt pscroll');
		toolbar.setBidsLeft();
	}
	else
	{
		if( document.getElementById( "m_message" ) != null )
		{
			if( document.getElementById( "prompt_loading" ) != null )
				document.getElementById( "m_message" ).innerHTML = document.getElementById( "prompt_loading" ).innerHTML;	
	
			ajax_asyncRequestField( "pull/?cmd=viewauctiondetails&auction_id=" + urlencode( auctionID ), ajax_getSneakValue, ajax_timeoutCancel, 20000, "moreinfo_auction_" + auctionID );
		}
	}
}


/**
 * \fn sneakAuction( auctionID, confirmed )
 * \brief When the user click on 'confirm', in the sneak 'notification box' it shows a message
 * \param url, the ajax URL
 * \param response, the ajax response
 * \param fieldName, to display the notification_show  
 */
function ajax_getSneakValue( url, response, fieldName )
{
	var value = response.responseText;
	
	if( document.getElementById( "m_message" ) != null )
	{
		if( value == "401" )	//user is logout
			notification_show( fieldName, 4 );
		else					
			document.getElementById( "m_message" ).innerHTML = value;
	}
}



function capitaliseFirstLetter( string )
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * \fn check_all( status )
 * \brief Assign the value from the variable status to all the checkboxes 
 * \param status, true for checked, false for unchecked
 */
function check_all ( status )
{
	for( i = 0; i < document.bidsys_form.length ; i++ )
	{ 
		if( document.bidsys_form.elements[i].type == "checkbox" )
		{ document.bidsys_form.elements[i].checked = status; } 
	}
}

function support_popup_logged_in( category, emailSubject, email )
{
    prompt_showPage( "pg_prompt_support" );

    window.setTimeout( function() 
    {
        document.getElementById( 'category' ).value = email;
        
		document.getElementById( 'subject' ).value = capitaliseFirstLetter( emailSubject ) + " (" + cookieUser() + ")";

        document.getElementById( 'username' ).value = cookieUser();

        document.getElementById( 'commonPanel' ).style.visibility = 'visible';
        document.getElementById( 'commonPanel' ).style.display = 'block';
        
        document.getElementById( 'userNamePanel' ).style.visibility = 'visible';
        document.getElementById( 'userNamePanel' ).style.display = 'block';
        
        if( category == 'payments' )
        {
            document.getElementById( 'paymentOptionPanel' ).style.visibility = 'visible';
            document.getElementById( 'paymentOptionPanel' ).style.display = 'block';
        }

        if( category == 'tracking' )
        {
            document.getElementById( 'trackingPanel' ).style.visibility = 'visible';
            document.getElementById( 'trackingPanel' ).style.display = 'block';
        }

        if( category == 'bidding' )
        {
            document.getElementById( 'biddingPanel' ).style.visibility = 'visible';
            document.getElementById( 'biddingPanel' ).style.display = 'block';
        }
    
    }, 500 );
}



function support_popup_guest( category, emailSubject, email )
{
    prompt_showPage( "pg_prompt_support" );

    window.setTimeout( function() 
    {
		document.getElementById( 'category' ).value = email;
        
        document.getElementById( 'subject' ).value = capitaliseFirstLetter( emailSubject );

        document.getElementById( 'commonPanel' ).style.visibility = 'visible';
        document.getElementById( 'commonPanel' ).style.display = 'block';

        document.getElementById( 'emailPanel' ).style.visibility = 'visible';
        document.getElementById( 'emailPanel' ).style.display = 'block';

        if( category == 'payments' )
        {
            document.getElementById( 'paymentOptionPanel' ).style.visibility = 'visible';
            document.getElementById( 'paymentOptionPanel' ).style.display = 'block';
        }

        if( category == 'tracking' )
        {
            document.getElementById( 'trackingPanel' ).style.visibility = 'visible';
            document.getElementById( 'trackingPanel' ).style.display = 'block';
        }

        if( category == 'bidding' )
        {
            document.getElementById( 'biddingPanel' ).style.visibility = 'visible';
            document.getElementById( 'biddingPanel' ).style.display = 'block';
        }
    
    }, 500 );
}

function capitaliseFirstLetter( string )
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}


// ------------------- BEGIN IE6 DETECT SCRIPT -------------------
// Browser detect script (c) Peter-Paul Koch Retrieved From: http://www.end6.org/eng/
// MadBid Developer(s) Involved: Aleksi Asikainen Date: 2010-04-16 Author: Miquel Hudin
// License: http://creativecommons.org/licenses/by-sa/3.0/us/ You are free: to Share � to copy, distribute and transmit the work to Remix � to adapt the work

function ie6_test()
{
	var ie6_version = -1;

	if( navigator.appName == "Microsoft Internet Explorer" )
	{
		var re  = new RegExp( "MSIE ([0-9]{1,}[\.0-9]{0,})" );
		
		if( re.exec( navigator.userAgent ) != null )
			ie6_version = parseFloat( RegExp.$1 );
	}

	if( ( navigator.appName=="Microsoft Internet Explorer" ) && ( ie6_version < 7 ) && ( ie6_readCookie( 'end6' ) != '1' ) ) 
	{
		prompt_showPage( "pg_prompt_ie6" );
		ie6_createCookie( "end6", 1, 14400 );
	}
}
function ie6_readCookie( the_name )
{
	var nameEQ = the_name + "=";
	var ca = document.cookie.split( ";" );
	
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		
		while( c.charAt(0) == ' ' )
			c = c.substring( 1, c.length );
			
		if( c.indexOf( nameEQ ) == 0 )
			return c.substring( nameEQ.length , c.length );
	}
	return null;
}
function ie6_createCookie( name, value, minutes )
{
	var minutes = minutes;
	
	if( minutes ) 
	{
		var date = new Date();
		date.setTime(date.getTime()+(minutes*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else
	{
		var expires = "";
	}
}


//postCodeBridge is defined in mad-enhance, it is link to PostInfo object between contexts
function ajax_getAddressIDSelector(element, command){
	var addressID = $(element).find('option:selected').val();
	
	//if we are triggering postcode for user addresses then we should also update cached user data when switching between different forms (find address, enter manualy, my addresses)
	var userDataUpdateEnabled = $(element).attr('id').match(/^old/) !== null ? true : false;
	postCodeBridge.userDataUpdateEnabled = userDataUpdateEnabled;
	postCodeBridge.addSpinner(element);
	
	if(command === 'postcode_details')
	{
		postCodeBridge.clearPostInputsAndValueSpans();
	}
	ajax_asyncRequestField( "pull/?cmd=" + command + "&postcode_details=" + addressID , ajax_responseToPostcode, ajax_timeoutCancel, 20000, 'fieldName' );
}

function ajax_responseToPostcode( url, response, fieldName ){
	if(response.responseText !== ''){
		var responseObj = eval("(" + response.responseText + ")");
		postCodeBridge.updatePostInputFields( responseObj );
	}
}



// ------------------- END IE6 DETECT SCRIPT -------------------

/* -------------------------------------------- INITIALIZATION ------------------------------------------ */

window.onload = initDOM;


/* -------------------------------------------- Jquery Plugin Starts ------------------------------------------ */
/*!
 * jQuery UI 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI
 */
(function(c,j){function k(a,b){var d=a.nodeName.toLowerCase();if("area"===d){b=a.parentNode;d=b.name;if(!a.href||!d||b.nodeName.toLowerCase()!=="map")return false;a=c("img[usemap=#"+d+"]")[0];return!!a&&l(a)}return(/input|select|textarea|button|object/.test(d)?!a.disabled:"a"==d?a.href||b:b)&&l(a)}function l(a){return!c(a).parents().andSelf().filter(function(){return c.curCSS(this,"visibility")==="hidden"||c.expr.filters.hidden(this)}).length}c.ui=c.ui||{};if(!c.ui.version){c.extend(c.ui,{version:"1.8.16",
keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}});c.fn.extend({propAttr:c.fn.prop||c.fn.attr,_focus:c.fn.focus,focus:function(a,b){return typeof a==="number"?this.each(function(){var d=
this;setTimeout(function(){c(d).focus();b&&b.call(d)},a)}):this._focus.apply(this,arguments)},scrollParent:function(){var a;a=c.browser.msie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(c.curCSS(this,"position",1))&&/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(c.curCSS(this,
"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0);return/fixed/.test(this.css("position"))||!a.length?c(document):a},zIndex:function(a){if(a!==j)return this.css("zIndex",a);if(this.length){a=c(this[0]);for(var b;a.length&&a[0]!==document;){b=a.css("position");if(b==="absolute"||b==="relative"||b==="fixed"){b=parseInt(a.css("zIndex"),10);if(!isNaN(b)&&b!==0)return b}a=a.parent()}}return 0},disableSelection:function(){return this.bind((c.support.selectstart?"selectstart":
"mousedown")+".ui-disableSelection",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}});c.each(["Width","Height"],function(a,b){function d(f,g,m,n){c.each(e,function(){g-=parseFloat(c.curCSS(f,"padding"+this,true))||0;if(m)g-=parseFloat(c.curCSS(f,"border"+this+"Width",true))||0;if(n)g-=parseFloat(c.curCSS(f,"margin"+this,true))||0});return g}var e=b==="Width"?["Left","Right"]:["Top","Bottom"],h=b.toLowerCase(),i={innerWidth:c.fn.innerWidth,innerHeight:c.fn.innerHeight,
outerWidth:c.fn.outerWidth,outerHeight:c.fn.outerHeight};c.fn["inner"+b]=function(f){if(f===j)return i["inner"+b].call(this);return this.each(function(){c(this).css(h,d(this,f)+"px")})};c.fn["outer"+b]=function(f,g){if(typeof f!=="number")return i["outer"+b].call(this,f);return this.each(function(){c(this).css(h,d(this,f,true,g)+"px")})}});c.extend(c.expr[":"],{data:function(a,b,d){return!!c.data(a,d[3])},focusable:function(a){return k(a,!isNaN(c.attr(a,"tabindex")))},tabbable:function(a){var b=c.attr(a,
"tabindex"),d=isNaN(b);return(d||b>=0)&&k(a,!d)}});c(function(){var a=document.body,b=a.appendChild(b=document.createElement("div"));c.extend(b.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0});c.support.minHeight=b.offsetHeight===100;c.support.selectstart="onselectstart"in b;a.removeChild(b).style.display="none"});c.extend(c.ui,{plugin:{add:function(a,b,d){a=c.ui[a].prototype;for(var e in d){a.plugins[e]=a.plugins[e]||[];a.plugins[e].push([b,d[e]])}},call:function(a,b,d){if((b=a.plugins[b])&&
a.element[0].parentNode)for(var e=0;e<b.length;e++)a.options[b[e][0]]&&b[e][1].apply(a.element,d)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(a,b){if(c(a).css("overflow")==="hidden")return false;b=b&&b==="left"?"scrollLeft":"scrollTop";var d=false;if(a[b]>0)return true;a[b]=1;d=a[b]>0;a[b]=0;return d},isOverAxis:function(a,b,d){return a>b&&a<b+d},isOver:function(a,b,d,e,h,i){return c.ui.isOverAxis(a,d,h)&&
c.ui.isOverAxis(b,e,i)}})}})(jQuery);

/*!
 * jQuery UI Widget 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Widget
 */
(function(b,j){if(b.cleanData){var k=b.cleanData;b.cleanData=function(a){for(var c=0,d;(d=a[c])!=null;c++)try{b(d).triggerHandler("remove")}catch(e){}k(a)}}else{var l=b.fn.remove;b.fn.remove=function(a,c){return this.each(function(){if(!c)if(!a||b.filter(a,[this]).length)b("*",this).add([this]).each(function(){try{b(this).triggerHandler("remove")}catch(d){}});return l.call(b(this),a,c)})}}b.widget=function(a,c,d){var e=a.split(".")[0],f;a=a.split(".")[1];f=e+"-"+a;if(!d){d=c;c=b.Widget}b.expr[":"][f]=
function(h){return!!b.data(h,a)};b[e]=b[e]||{};b[e][a]=function(h,g){arguments.length&&this._createWidget(h,g)};c=new c;c.options=b.extend(true,{},c.options);b[e][a].prototype=b.extend(true,c,{namespace:e,widgetName:a,widgetEventPrefix:b[e][a].prototype.widgetEventPrefix||a,widgetBaseClass:f},d);b.widget.bridge(a,b[e][a])};b.widget.bridge=function(a,c){b.fn[a]=function(d){var e=typeof d==="string",f=Array.prototype.slice.call(arguments,1),h=this;d=!e&&f.length?b.extend.apply(null,[true,d].concat(f)):
d;if(e&&d.charAt(0)==="_")return h;e?this.each(function(){var g=b.data(this,a),i=g&&b.isFunction(g[d])?g[d].apply(g,f):g;if(i!==g&&i!==j){h=i;return false}}):this.each(function(){var g=b.data(this,a);g?g.option(d||{})._init():b.data(this,a,new c(d,this))});return h}};b.Widget=function(a,c){arguments.length&&this._createWidget(a,c)};b.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(a,c){b.data(c,this.widgetName,this);this.element=b(c);this.options=
b.extend(true,{},this.options,this._getCreateOptions(),a);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();this._trigger("create");this._init()},_getCreateOptions:function(){return b.metadata&&b.metadata.get(this.element[0])[this.widgetName]},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+
"-disabled ui-state-disabled")},widget:function(){return this.element},option:function(a,c){var d=a;if(arguments.length===0)return b.extend({},this.options);if(typeof a==="string"){if(c===j)return this.options[a];d={};d[a]=c}this._setOptions(d);return this},_setOptions:function(a){var c=this;b.each(a,function(d,e){c._setOption(d,e)});return this},_setOption:function(a,c){this.options[a]=c;if(a==="disabled")this.widget()[c?"addClass":"removeClass"](this.widgetBaseClass+"-disabled ui-state-disabled").attr("aria-disabled",
c);return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(a,c,d){var e=this.options[a];c=b.Event(c);c.type=(a===this.widgetEventPrefix?a:this.widgetEventPrefix+a).toLowerCase();d=d||{};if(c.originalEvent){a=b.event.props.length;for(var f;a;){f=b.event.props[--a];c[f]=c.originalEvent[f]}}this.element.trigger(c,d);return!(b.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery);

/*!
 * jQuery UI Mouse 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Mouse
 *
 * Depends:
 *	jquery.ui.widget.js
 */
(function(b){var d=false;b(document).mouseup(function(){d=false});b.widget("ui.mouse",{options:{cancel:":input,option",distance:1,delay:0},_mouseInit:function(){var a=this;this.element.bind("mousedown."+this.widgetName,function(c){return a._mouseDown(c)}).bind("click."+this.widgetName,function(c){if(true===b.data(c.target,a.widgetName+".preventClickEvent")){b.removeData(c.target,a.widgetName+".preventClickEvent");c.stopImmediatePropagation();return false}});this.started=false},_mouseDestroy:function(){this.element.unbind("."+
this.widgetName)},_mouseDown:function(a){if(!d){this._mouseStarted&&this._mouseUp(a);this._mouseDownEvent=a;var c=this,f=a.which==1,g=typeof this.options.cancel=="string"&&a.target.nodeName?b(a.target).closest(this.options.cancel).length:false;if(!f||g||!this._mouseCapture(a))return true;this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet)this._mouseDelayTimer=setTimeout(function(){c.mouseDelayMet=true},this.options.delay);if(this._mouseDistanceMet(a)&&this._mouseDelayMet(a)){this._mouseStarted=
this._mouseStart(a)!==false;if(!this._mouseStarted){a.preventDefault();return true}}true===b.data(a.target,this.widgetName+".preventClickEvent")&&b.removeData(a.target,this.widgetName+".preventClickEvent");this._mouseMoveDelegate=function(e){return c._mouseMove(e)};this._mouseUpDelegate=function(e){return c._mouseUp(e)};b(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);a.preventDefault();return d=true}},_mouseMove:function(a){if(b.browser.msie&&
!(document.documentMode>=9)&&!a.button)return this._mouseUp(a);if(this._mouseStarted){this._mouseDrag(a);return a.preventDefault()}if(this._mouseDistanceMet(a)&&this._mouseDelayMet(a))(this._mouseStarted=this._mouseStart(this._mouseDownEvent,a)!==false)?this._mouseDrag(a):this._mouseUp(a);return!this._mouseStarted},_mouseUp:function(a){b(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=
false;a.target==this._mouseDownEvent.target&&b.data(a.target,this.widgetName+".preventClickEvent",true);this._mouseStop(a)}return false},_mouseDistanceMet:function(a){return Math.max(Math.abs(this._mouseDownEvent.pageX-a.pageX),Math.abs(this._mouseDownEvent.pageY-a.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return true}})})(jQuery);

/*
 * jQuery UI Slider 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Slider
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
(function(d){d.widget("ui.slider",d.ui.mouse,{widgetEventPrefix:"slide",options:{animate:false,distance:0,max:100,min:0,orientation:"horizontal",range:false,step:1,value:0,values:null},_create:function(){var a=this,b=this.options,c=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),f=b.values&&b.values.length||1,e=[];this._mouseSliding=this._keySliding=false;this._animateOff=true;this._handleIndex=null;this._detectOrientation();this._mouseInit();this.element.addClass("ui-slider ui-slider-"+
this.orientation+" ui-widget ui-widget-content ui-corner-all"+(b.disabled?" ui-slider-disabled ui-disabled":""));this.range=d([]);if(b.range){if(b.range===true){if(!b.values)b.values=[this._valueMin(),this._valueMin()];if(b.values.length&&b.values.length!==2)b.values=[b.values[0],b.values[0]]}this.range=d("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header"+(b.range==="min"||b.range==="max"?" ui-slider-range-"+b.range:""))}for(var j=c.length;j<f;j+=1)e.push("<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>");
this.handles=c.add(d(e.join("")).appendTo(a.element));this.handle=this.handles.eq(0);this.handles.add(this.range).filter("a").click(function(g){g.preventDefault()}).hover(function(){b.disabled||d(this).addClass("ui-state-hover")},function(){d(this).removeClass("ui-state-hover")}).focus(function(){if(b.disabled)d(this).blur();else{d(".ui-slider .ui-state-focus").removeClass("ui-state-focus");d(this).addClass("ui-state-focus")}}).blur(function(){d(this).removeClass("ui-state-focus")});this.handles.each(function(g){d(this).data("index.ui-slider-handle",
g)});this.handles.keydown(function(g){var k=true,l=d(this).data("index.ui-slider-handle"),i,h,m;if(!a.options.disabled){switch(g.keyCode){case d.ui.keyCode.HOME:case d.ui.keyCode.END:case d.ui.keyCode.PAGE_UP:case d.ui.keyCode.PAGE_DOWN:case d.ui.keyCode.UP:case d.ui.keyCode.RIGHT:case d.ui.keyCode.DOWN:case d.ui.keyCode.LEFT:k=false;if(!a._keySliding){a._keySliding=true;d(this).addClass("ui-state-active");i=a._start(g,l);if(i===false)return}break}m=a.options.step;i=a.options.values&&a.options.values.length?
(h=a.values(l)):(h=a.value());switch(g.keyCode){case d.ui.keyCode.HOME:h=a._valueMin();break;case d.ui.keyCode.END:h=a._valueMax();break;case d.ui.keyCode.PAGE_UP:h=a._trimAlignValue(i+(a._valueMax()-a._valueMin())/5);break;case d.ui.keyCode.PAGE_DOWN:h=a._trimAlignValue(i-(a._valueMax()-a._valueMin())/5);break;case d.ui.keyCode.UP:case d.ui.keyCode.RIGHT:if(i===a._valueMax())return;h=a._trimAlignValue(i+m);break;case d.ui.keyCode.DOWN:case d.ui.keyCode.LEFT:if(i===a._valueMin())return;h=a._trimAlignValue(i-
m);break}a._slide(g,l,h);return k}}).keyup(function(g){var k=d(this).data("index.ui-slider-handle");if(a._keySliding){a._keySliding=false;a._stop(g,k);a._change(g,k);d(this).removeClass("ui-state-active")}});this._refreshValue();this._animateOff=false},destroy:function(){this.handles.remove();this.range.remove();this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all").removeData("slider").unbind(".slider");this._mouseDestroy();
return this},_mouseCapture:function(a){var b=this.options,c,f,e,j,g;if(b.disabled)return false;this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()};this.elementOffset=this.element.offset();c=this._normValueFromMouse({x:a.pageX,y:a.pageY});f=this._valueMax()-this._valueMin()+1;j=this;this.handles.each(function(k){var l=Math.abs(c-j.values(k));if(f>l){f=l;e=d(this);g=k}});if(b.range===true&&this.values(1)===b.min){g+=1;e=d(this.handles[g])}if(this._start(a,g)===false)return false;
this._mouseSliding=true;j._handleIndex=g;e.addClass("ui-state-active").focus();b=e.offset();this._clickOffset=!d(a.target).parents().andSelf().is(".ui-slider-handle")?{left:0,top:0}:{left:a.pageX-b.left-e.width()/2,top:a.pageY-b.top-e.height()/2-(parseInt(e.css("borderTopWidth"),10)||0)-(parseInt(e.css("borderBottomWidth"),10)||0)+(parseInt(e.css("marginTop"),10)||0)};this.handles.hasClass("ui-state-hover")||this._slide(a,g,c);return this._animateOff=true},_mouseStart:function(){return true},_mouseDrag:function(a){var b=
this._normValueFromMouse({x:a.pageX,y:a.pageY});this._slide(a,this._handleIndex,b);return false},_mouseStop:function(a){this.handles.removeClass("ui-state-active");this._mouseSliding=false;this._stop(a,this._handleIndex);this._change(a,this._handleIndex);this._clickOffset=this._handleIndex=null;return this._animateOff=false},_detectOrientation:function(){this.orientation=this.options.orientation==="vertical"?"vertical":"horizontal"},_normValueFromMouse:function(a){var b;if(this.orientation==="horizontal"){b=
this.elementSize.width;a=a.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)}else{b=this.elementSize.height;a=a.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)}b=a/b;if(b>1)b=1;if(b<0)b=0;if(this.orientation==="vertical")b=1-b;a=this._valueMax()-this._valueMin();return this._trimAlignValue(this._valueMin()+b*a)},_start:function(a,b){var c={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){c.value=this.values(b);
c.values=this.values()}return this._trigger("start",a,c)},_slide:function(a,b,c){var f;if(this.options.values&&this.options.values.length){f=this.values(b?0:1);if(this.options.values.length===2&&this.options.range===true&&(b===0&&c>f||b===1&&c<f))c=f;if(c!==this.values(b)){f=this.values();f[b]=c;a=this._trigger("slide",a,{handle:this.handles[b],value:c,values:f});this.values(b?0:1);a!==false&&this.values(b,c,true)}}else if(c!==this.value()){a=this._trigger("slide",a,{handle:this.handles[b],value:c});
a!==false&&this.value(c)}},_stop:function(a,b){var c={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){c.value=this.values(b);c.values=this.values()}this._trigger("stop",a,c)},_change:function(a,b){if(!this._keySliding&&!this._mouseSliding){var c={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){c.value=this.values(b);c.values=this.values()}this._trigger("change",a,c)}},value:function(a){if(arguments.length){this.options.value=
this._trimAlignValue(a);this._refreshValue();this._change(null,0)}else return this._value()},values:function(a,b){var c,f,e;if(arguments.length>1){this.options.values[a]=this._trimAlignValue(b);this._refreshValue();this._change(null,a)}else if(arguments.length)if(d.isArray(arguments[0])){c=this.options.values;f=arguments[0];for(e=0;e<c.length;e+=1){c[e]=this._trimAlignValue(f[e]);this._change(null,e)}this._refreshValue()}else return this.options.values&&this.options.values.length?this._values(a):
this.value();else return this._values()},_setOption:function(a,b){var c,f=0;if(d.isArray(this.options.values))f=this.options.values.length;d.Widget.prototype._setOption.apply(this,arguments);switch(a){case "disabled":if(b){this.handles.filter(".ui-state-focus").blur();this.handles.removeClass("ui-state-hover");this.handles.propAttr("disabled",true);this.element.addClass("ui-disabled")}else{this.handles.propAttr("disabled",false);this.element.removeClass("ui-disabled")}break;case "orientation":this._detectOrientation();
this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation);this._refreshValue();break;case "value":this._animateOff=true;this._refreshValue();this._change(null,0);this._animateOff=false;break;case "values":this._animateOff=true;this._refreshValue();for(c=0;c<f;c+=1)this._change(null,c);this._animateOff=false;break}},_value:function(){var a=this.options.value;return a=this._trimAlignValue(a)},_values:function(a){var b,c;if(arguments.length){b=this.options.values[a];
return b=this._trimAlignValue(b)}else{b=this.options.values.slice();for(c=0;c<b.length;c+=1)b[c]=this._trimAlignValue(b[c]);return b}},_trimAlignValue:function(a){if(a<=this._valueMin())return this._valueMin();if(a>=this._valueMax())return this._valueMax();var b=this.options.step>0?this.options.step:1,c=(a-this._valueMin())%b;a=a-c;if(Math.abs(c)*2>=b)a+=c>0?b:-b;return parseFloat(a.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var a=
this.options.range,b=this.options,c=this,f=!this._animateOff?b.animate:false,e,j={},g,k,l,i;if(this.options.values&&this.options.values.length)this.handles.each(function(h){e=(c.values(h)-c._valueMin())/(c._valueMax()-c._valueMin())*100;j[c.orientation==="horizontal"?"left":"bottom"]=e+"%";d(this).stop(1,1)[f?"animate":"css"](j,b.animate);if(c.options.range===true)if(c.orientation==="horizontal"){if(h===0)c.range.stop(1,1)[f?"animate":"css"]({left:e+"%"},b.animate);if(h===1)c.range[f?"animate":"css"]({width:e-
g+"%"},{queue:false,duration:b.animate})}else{if(h===0)c.range.stop(1,1)[f?"animate":"css"]({bottom:e+"%"},b.animate);if(h===1)c.range[f?"animate":"css"]({height:e-g+"%"},{queue:false,duration:b.animate})}g=e});else{k=this.value();l=this._valueMin();i=this._valueMax();e=i!==l?(k-l)/(i-l)*100:0;j[c.orientation==="horizontal"?"left":"bottom"]=e+"%";this.handle.stop(1,1)[f?"animate":"css"](j,b.animate);if(a==="min"&&this.orientation==="horizontal")this.range.stop(1,1)[f?"animate":"css"]({width:e+"%"},
b.animate);if(a==="max"&&this.orientation==="horizontal")this.range[f?"animate":"css"]({width:100-e+"%"},{queue:false,duration:b.animate});if(a==="min"&&this.orientation==="vertical")this.range.stop(1,1)[f?"animate":"css"]({height:e+"%"},b.animate);if(a==="max"&&this.orientation==="vertical")this.range[f?"animate":"css"]({height:100-e+"%"},{queue:false,duration:b.animate})}}});d.extend(d.ui.slider,{version:"1.8.16"})})(jQuery);
/* -------------------------------------------- Jquery Plugin Ends ------------------------------------------ */

/**
 * @author Bharathy Seralathan
 * Jan 2012
 * madbid 3.0
**/

/*************************Front Banner Carousel - US 6065 Starts***************************/
function banner_Carousel() {
    var a = 5000;
    var g = 5000;
    var f = true;
    var e = "";
    var d = false;

    function c(i) {
        $("#slider-nav li.active").removeClass("active");
        $(i).addClass("active");
        $("#slider .content").hide();
        var h = $(i).find("a").attr("lang");
        $(h).fadeIn();
        return false
    }
    function b() {
        if (f) {
            var h = setTimeout(function() {
                if (!f) {
                    clearTimeout(h);
                    return
                } else {
                    var i = $("#slider-nav li.active");
                    if (i.length == 0 || $("#slider-nav li.active").attr("id") == $("#slider-nav li:last").attr("id")) {
                        if (!d) {
                            g = $("#slider-nav li:first").attr("time");
                            c($("#slider-nav li:first"))
                        }
                    } else {
                        if (!d) {
                            g = $("#slider-nav li.active").next().attr("time");
                            c($("#slider-nav li.active").next())
                        }
                    }
                    if (g < 500) {
                        g = a
                    }
                    b()
                }
            }, g)
        }
    }
    $(document).ready(function() {
        $("#slider .content").hide();
        f = true;
        if (location.hash != "") {
            var h = '#' + location.hash.split("#")[1];
            $(location.hash).show();
            $("#slider-nav li:has(a[lang=" + h + "])").addClass("active").show()
        } else {
            $("#slider-nav li:first").addClass("active").show();
            $("#slider .content:first").show()
        }
        g = $("#slider-nav li.active").attr("time");
        b();
        $("#slider").hover(function() {
            d = true
        }, function() {
            d = false
        });
        $("#slider-nav li").hover(function() {
            d = true;
            c(this)
        }, function() {
            d = false
        })
    })
};
/*************************Front Banner Carousel - US 6065 Ends***************************/




/***************************ToolBar - US 5437 Starts*************************************/

var toolbar = {	
	sneakClickCount : 0,
	sneakStartTime : null,
	autobidClickCount : 0,
	autobidStartTime : null,
	//creditClickCount :  0,
	//creditStartTime : null,
	setBidsLeft : function (bidcount){
		var cookie_bids = null;
		if(bidcount==undefined)
			cookie_bids = document.cookie.split( cookiePrefix + "bidsys_credits=" )[ 1 ].split( ";" )[ 0 ];
		else 
			cookie_bids = bidcount;
		if(cookie_bids != null || cookie_bids!='')
			$('#tb_bidsLeft').html( '('+cookie_bids+')' );
	},
	
	setCashLeft : function (){
		if( document.cookie.indexOf( cookiePrefix + "bidsys_cash=" ) != -1 ) 
		{ 
			var cookie_cash = document.cookie.split( cookiePrefix + "bidsys_cash=" )[ 1 ].split( ";" )[ 0 ]; 
			if(cookie_cash  != null || cookie_cash !='')
				$('.wallet_balance').html(cookie_cash );
		}
	},
	
	setUserName : function (elemId){
		var cookie_user = document.cookie.split( cookiePrefix + "bidsys_user=" )[ 1 ].split( ";" )[ 0 ];
		if(cookie_user  != null || cookie_user !='')
			$('#'+elemId).html(cookie_user);
	},
	
	getCredit : function(creditElem){
		this.commandShowCredits = function(obj){
			if(obj.response!=null){
				creditElem.find('.total').html(obj.response.credits.total);
				creditElem.find('.free').html(obj.response.credits.free);
				creditElem.find('.paid').html(obj.response.credits.paid);
				$('#tb_buy_failure,#tb_buy_success,#tb_errorMessage,#boxCreditConfirm').hide();

				$('#tb_topup').hide();
				$('#tb_no_topup').show();
			}
			else{
				//silently died
			}
	};
	if(creditElem.find('.tb_pop').is(':hidden')){
			ajaxManager.getCreditAJAX.call(this,20);
	}	
	},
	
	//getSneak display list of user who sneaked me and my sneaked user list and my auction peek list
	getSneak : function(SneakElem){
		//callback for MySneak
		this.commandShowMySneak = function(obj){
			if(obj.response!=null){
				var sneakuserObj = obj.response.users;
				if(sneakuserObj == null || typeof sneakuserObj == undefined){
					$('#mySneakList').empty().siblings('#all_mysneak').hide();
					$('#noSneak').removeClass('hide').addClass('show');
				}
				else {
					var sneakUserList = [];
					for (var sneakuser in sneakuserObj) {
				  		sneakUserList.push('<li class="tb_link" id="'+sneakuserObj[sneakuser].user_action_id+'" lang="'+sneakuser+'">'+ sneakuser +'</li>');
					}
					$('#noSneak').removeClass('show').addClass('hide');
					$('#mySneakList').empty().append(sneakUserList.join('')).siblings('#all_mysneak').show();
				}
			}
			else{
				$('#mySneakList').empty().siblings('.seeall').hide();
				$('#noSneak').removeClass('hide').addClass('show');
			}
		};
		//callback for WhoSneakedMe
		this.commandShowWhoSneakedMe = function(obj){
			if(obj.response!=null){
				var userObj = obj.response.users;
				if(userObj == null || typeof userObj == undefined){
					$('#whoSneakedMeList').empty().siblings('#all_whosneaked').hide();
					$('#noWhoSneak').removeClass('hide').addClass('show');
				}
				else {
					var userList = [];
					for (var user in userObj) {
				  		userList.push('<li class="tb_link" id="'+userObj[user].notification_id+'" lang="'+user+'">'+ user +'</li>');
					}
					if($('#noWhoSneak')) $('#noWhoSneak').removeClass('show').addClass('hide');
					$('#whoSneakedMeList').empty().append(userList.join('')).siblings('#all_whosneaked').show();
				}
			}
			else{
				$('#whoSneakedMeList').empty().siblings('.seeall').hide();
				$('#noWhoSneak').removeClass('hide').addClass('show');
			}
		};
		//callback for auction peek list
		this.commandShowAuctions = function(obj){
			if(obj.response!=null){
				var auctionsObj = obj.response.auctions;
				if(auctionsObj == null || typeof auctionsObj == undefined){
					$('#auctionPeekList').empty().siblings('#all_auctionpeek').hide();
					$('#noAuctionPeek').removeClass('hide').addClass('show');
				}
				else {
					var auctionsList = [];
					for (var key in auctionsObj) {
				  		auctionsList.push('<li class="tb_link" id="'+key+'" lang="'+auctionsObj[key].user_action_id+'">'+ auctionsObj[key].item_title +'</li>');
					}
					$('#noAuctionPeek').removeClass('show').addClass('hide');
					$('#auctionPeekList').empty().append(auctionsList.join('')).siblings('#all_auctionpeek').show();
				}
			}else{
				$('#auctionPeekList').empty().siblings('.seeall').hide();
				$('#noAuctionPeek').removeClass('hide').addClass('show');
			}
			function setSneakPopupPos(){
				var popup = SneakElem.find('.tb_pop');
				//popup.css(setToolbarPopupPosition(SneakElem.find('.highlighter'),popup));
			}
			setTimeout(setSneakPopupPos,400);
			
		};
		
			if(SneakElem.find('.tb_pop').is(':hidden')){
				ajaxManager.getSneakAJAX.call(this,20);
		
		};
	},
	
	getMySneakUser : function(obj){
		var userName = obj.attr('lang'), userActionId = obj.attr('id');
		prompt_showPage("pull/?cmd=get_mysneak&targetUserName="+userName+"&user_action_id="+userActionId,true);
		$('#prompt_page').addClass('sneak_prompt pscroll');
	},
		
	getWhoSneakedMe : function(obj){
		var userName = obj.attr('lang'), userNotificationId = obj.attr('id');
		prompt_showPage("pull/?cmd=get_user&targetUserName="+userName+"&user_notification_id="+userNotificationId,true);
		$('#prompt_page').addClass('sneak_prompt pscroll');
	},
	
	//dispaly the pop up(prompt) of auction peeks in detail
	getAuctionData : function(obj){
		var auctionId = obj.attr('id'), userActionId = obj.attr('lang');
		prompt_showPage("pull/?cmd=get_auction&auction_id="+auctionId+"&user_action_id="+userActionId,true);
		$('#prompt_page').addClass('sneak_prompt pscroll');
	},
	
	//display stealth mode data based on ON and OFF of stealth mode
	getStealth : function(stleathElem){
		this.commandShowMyStealth = function(obj){
			var isStealthMode = obj.response.stealth;
			if(isStealthMode){
				$('.successMsg').hide();
				$('#tbPopNotStealth,#stealth_confirm,#extend_msg').removeClass('show').addClass('hide');
				$('#tbPopStealth').find('.expire_date').html(obj.response.expiryDate);
				$('#tbPopStealth').removeClass('hide').addClass('show');
				$('#stealth_message').show();
				$('#buyStealthLink').bind('click',function(){
					$('#extend_msg').removeClass('hide').addClass('show');
					$('#tbPopStealth').append($('#stealth_confirm').removeClass('hide').addClass('show'));
					$('#stealth_message').hide();
				});
			}
			else {
				$('.successMsg').hide();
				$('#tbPopNotStealth,#makeme_container').removeClass('hide').addClass('show');
				$('#tbPopStealth,#stealth_confirm').removeClass('show').addClass('hide');
				$('#tb_makeMeStealth').bind('click',function(){
					$('#makeme_container').removeClass('show').addClass('hide');
					$('#tbPopNotStealth').append($('#stealth_confirm').removeClass('hide').addClass('show'));
				});
			}
			$('#tb_stealthCancel').bind('click',function(){
				$('#stealth_message').show();
				if(!isStealthMode){ $('#makeme_container').removeClass('hide').addClass('show'); }
				$('#stealth_confirm,#extend_msg').removeClass('show').addClass('hide');
			});
			var popup = stleathElem.find('.tb_pop');
			//popup.css(setToolbarPopupPosition(stleathElem.find('.highlighter'),popup));
		};
		
		if(stleathElem.find('.tb_pop').is(':hidden')){
			ajaxManager.sendRequest('json/toolbar/my_stealth/','json', true, 'GET','', this.commandShowMyStealth, null);
		}
	},
	
	buyStealth : function(){
		this.callback = function(response){
			var responseHTML = $("<div class='successMsg'></div>");
			responseHTML.html(response);
			$('#tbPopNotStealth').before(responseHTML);
			$('#tbPopStealth,#tbPopNotStealth').addClass('hide').removeClass('show');
			var childElem = $('#tbPopNotStealth').parent('.tb_pop');
			var parentElem = childElem.parents('.highlighter');
			//childElem.css(setToolbarPopupPosition(parentElem,childElem));
			toolbar.setBidsLeft();
		};
		ajaxManager.sendRequest('pull/?cmd=buystealthmode','html', true, 'GET','', this.callback, null);
	},
	
	getAutobid : function(autobidElem){
		this.autobidCallback = function(obj){
			if(obj.response!=null){
				$('.autobid_count').html(obj.response);
			}
		};
		if(autobidElem.find('.tb_pop').is(':hidden')){
				ajaxManager.getautobidAJAX.call(this,20);
		};
	},
	
	processOneClick : function(oneclick_btn){
		this.oneClickCallback = function(obj){
			if(obj.response!=null){
				if(obj.response.paid){
					$('#tb_topup,#boxCreditConfirm,#tb_buy_failure,#tb_processing,#tb_errorMessage').hide();
					$('#tb_buy_success').show();
					toolbar.setBidsLeft();
					toolbar.setCashLeft();
					madBid.hideMask();
				}
				else{
					$('#tb_buy_failure').show();
					$('#tb_topup,#boxCreditConfirm,#tb_buy_success,#tb_processing,#tb_errorMessage').hide();
				}
			}else{
				if(obj.status=400){
					madBid.hideMask();
					$('#tb_processing,#boxCreditConfirm').hide();
					$('#tb_errorMessage').show();
				}
			}
			var popup = oneclick_btn.parents('.tb_pop');
		 	//popup.css(setToolbarPopupPosition(popup.parents('.highlighter'),popup));
		};
		var purchaseData = {
			credit_count : ($('#creditid').val()!='') ? $('#creditid').val() : null, 
			paymentType : ($('#card_type').val()=='wallet') ? 'wallet' : 'card',
			cardval : ($('#card_type').val()!='wallet') ? $('#card_type').val() : null,
			pay_by : 'toolbar',
			callback_URL : window.location.href
		}
		ajaxManager.sendRequest('json/toolbar/purchase_credit','json', true, 'PUT','', this.oneClickCallback, purchaseData);
	},
	setToolbarPos : function(){
		//var toolbarPosLeft = ($(window).width()-$('#toolList').width())/2;
		//$('#toolbar').css({'left':toolbarPosLeft+482});
	}
};

$(window).resize(function() {
  toolbar.setToolbarPos();
});

$(document).ready(function() {
if( cookieUser() ){
	$('tb_container').show();
	$('.bar_bottom').css({'margin-bottom': '60px' });
	$('#tb_topup,#boxCreditConfirm').hide();
	$('#tb_buy_success').hide();
	$('#tb_buy_failure').hide();
	$('#toolbar').find('.seeall').hide();
	toolbar.setBidsLeft();
	toolbar.setCashLeft();
	toolbar.setUserName('tb_username');
	$('#auctionPeekList li').live('click',function(){
		toolbar.getAuctionData($(this));
	});
	
	$('#tb_stealthConfirm').live('click',function(){
		toolbar.buyStealth();
	});
	
	$('#mySneakList li').live('click',function(){
		toolbar.getMySneakUser($(this));
	});
	
	$('#whoSneakedMeList li').live('click',function(){
		toolbar.getWhoSneakedMe($(this));
	});
	
	$('#oneclick_btn').live('click',function(){
		$('#tb_topup,#tb_no_topup').hide();
		var confirmBox = $('#boxCreditConfirm');
		var popup = confirmBox.parents('.tb_pop');
		confirmBox.show();
		//popup.css(setToolbarPopupPosition(confirmBox.parents('.highlighter'),popup));
	});
	
	$('#creditbuy_confirm').live('click',function(){
		madBid.showMask();
		$('#tb_processing').show();
		toolbar.processOneClick($('#oneclick_btn'));
	});
	
	$('#creditbuy_cancel').live('click',function(){
		$('#boxCreditConfirm,#tb_processing').hide();
		var topupBox = $('#tb_topup');
		var popup = topupBox.parents('.tb_pop');
		topupBox.show();
		//popup.css(setToolbarPopupPosition(topupBox.parents('.highlighter'),popup));
	});
	
	//fn to show popup onclick of each tool in toolbar
	$('#toolList > li').bind('click',function(e){
		if(cookieUser()){
			if($(this).find('.tb_pop').length!=0){
				var popup = $(this).find('.tb_pop');
				var parentElem = popup.parents('.highlighter');
				$(this).siblings().find('.highlighter').removeClass('active').find('.selected').remove().end().find('.tb_pop').hide();
				switch($(this).attr('id')){
				    case "tb_sneak":
						toolbar.getSneak($(this));
						break;
					case "tb_stealth":
						toolbar.getStealth($(this));
						break;
					case "tb_credit":
						toolbar.getCredit($(this));
						break;	
					case "tb_autobid":
						toolbar.getAutobid($(this));
						break;
					default:
						break;
				};
				popup.show().end().find('.txt_yellow').end().find('.highlighter').addClass('active');	//popup.show().css(setToolbarPopupPosition(parentElem,popup)).end().find('.txt_yellow').end().find('.highlighter').addClass('active');		
			}
		}else{
			 e.preventDefault();
			notification_show($(this).attr('id'), 4, '' );
		}
	});
	
	$('.toolLi').hover(
	  function () {
	    $(this).find('.tb_text').addClass("item_hover");
	  },
	  function () {
	    $(this).find('.tb_text').removeClass("item_hover");
	});
	
	//fn to hide popup onclick of minimize button
	$('.close').bind('click',function(e){
		$(this).closest('.tb_pop').hide();
		$(this).closest('.highlighter').removeClass('active').find('.txt_yellow').end().find('.selected').remove();
		e.stopPropagation();
		return false;
	});
	
	//fn to hide popup onclick of out of toolbar
	$(document).mouseup(function (e){
	    var container = $("#toolbar");
	    var promptcontainer = $('#prompt_page');
	    var promptElem = $(e.target).attr('id');
	   if(container.has(e.target).length === 0 && promptcontainer.has(e.target).length===0 &&(promptElem!='prompt_black'))
	    	$('#toolList > li').find('.highlighter').removeClass('active').find('.txt_yellow').end().find('.selected').remove().end().find('.tb_pop').hide();
	});
	
	$('.help').live('click',function(){
		notification_show( "oneclick_help", "2", $(this).html());
	});
	toolbar.setToolbarPos();
}
else{
	$('.tb_container').hide();
}
});

/* This method is used to make AJAX calls*/
var madBid ={}; //MadBid Global nameSpace. Any Global variables should go inside this Object
madBid.DebugMode = true;
madBid.showMask = function(){
	var mask = $('<div id="prompt_black"> </div>');
	$('body').append(mask);
};
madBid.hideMask = function(){
	$('#prompt_black').remove();
};
madBid.showLoaderIcon = function(parentElem){
	var loader = $('<p id="loader"></p>');
	$('body').append(parentElem);
	$('#loader').css({top:'50%',left:'50%'});
}
madBid.hideLoaderIcon = function(){
	$('#loader').remove();
}

var ajaxManager = (function(){
	return{}
})();

ajaxManager.sendRequest = function(ajaxUrl, dataType, asyncFlag, methodFlag, serializeFunc, callback, sendData){
   try{
   			//var fullAjaxUrl = 'http'+ (document.location.protocol=='https:'?'s://':'://')+window.location.hostname+'/'+ajaxUrl;
        	$.ajax({
            url: ajaxUrl,
            dataType : dataType,
            type : methodFlag,
            async: asyncFlag,
            data : (sendData) ? SerializeManager.serializeJson(sendData) : null,
            success: function(data){        
				    if(typeof callback!=undefined){
				    	if((data.status) ? (data.status==200) : true)
				    	{
				    		callback(data);
				    	}
				    	else if(data.status==400){
				    		callback(data); //handle 400 error at call back
				    	}else
				    	{
				    		if(madBid.DebugMode) 
				    			alert("DebugMode: Ajax Error in " + data.cmd +". Status Code =" + data.status + " message = " + data.message);
				    		else{
				    			alert('Your transaction is failed. Please Try Again later');
				    		}
				    	}
				    }
            },
            error : function(XMLHttpObj, textStatus, errorThrown){
            	//internal server error
            	//show debug error
    			if(madBid.DebugMode) alert("Server encountered an error " + textStatus + ", Error Thrown : " + errorThrown);
    			else{
    				alert("Internal Server Error");
    			}
            },
            beforeSend : function(xhr){
            	/* if any callback required before send*/
            }
        });
    }
    catch(error){
    	//show debug error
    	//if(madBid.DebugMode) alert('Error type:'+ error.name + 'Error message:' + error.message + 'Javascript error in ajaxManager.sendRequest for " + ajaxUrl);
    }
}

ajaxManager.getSneakAJAX = function(timerInterval){
	this.sneakClickCount++;
	if(this.sneakClickCount===1){ 
		this.sneakStartTime = new Date().getTime();
		ajaxManager.sendRequest('json/toolbar/my_whosneak/','json', true, 'GET','', this.commandShowWhoSneakedMe , null);
		ajaxManager.sendRequest('json/toolbar/my_whoisneak/','json', true, 'GET','', this.commandShowMySneak , null); 
		ajaxManager.sendRequest('json/toolbar/my_auction/','json', true, 'GET','', this.commandShowAuctions , null);
	}
	else{ 
		 var diffTime = ((new Date().getTime()) - this.sneakStartTime)/1000 ;
		 if(diffTime>=timerInterval){ 
		 	ajaxManager.sendRequest('json/toolbar/my_whosneak/','json', true, 'GET','', this.commandShowWhoSneakedMe , null);
		 	ajaxManager.sendRequest('json/toolbar/my_whoisneak/','json', true, 'GET','', this.commandShowMySneak , null); 
		 	ajaxManager.sendRequest('json/toolbar/my_auction/','json', true, 'GET','', this.commandShowAuctions , null); 
		 	this.sneakStartTime = new Date().getTime(); 
		 }
	}
};

ajaxManager.getCreditAJAX = function(timerInterval){
	//this.creditClickCount++;
	//if(this.creditClickCount===1){ 
		//this.creditStartTime = new Date().getTime();
		ajaxManager.sendRequest('json/toolbar/my_credit/','json', false, 'GET','', this.commandShowCredits , null); 
	/*}
	else{ 
		 var diffTime = ((new Date().getTime()) - this.creditStartTime)/1000 ;
		 if(diffTime>=timerInterval){ 
		 	ajaxManager.sendRequest('json/toolbar/my_credit/','json', false, 'GET','', this.commandShowCredits , null); 
		 	this.creditStartTime = new Date().getTime(); 
		 }
	}*/
};

ajaxManager.getautobidAJAX = function(timerInterval){
	this.autobidClickCount++;
	if(this.autobidClickCount===1){ 
		this.autobidStartTime = new Date().getTime();
		ajaxManager.sendRequest('json/autobid/count','json', false, 'GET','', this.autobidCallback , null); 
	}
	else{ 
		 var diffTime = ((new Date().getTime()) - this.autobidStartTime)/1000 ;
		 if(diffTime>=timerInterval){ 
		 	ajaxManager.sendRequest('json/autobid/count','json', false, 'GET','', this.autobidCallback , null); 
		 	this.autobidStartTime = new Date().getTime(); 
		}
	}
};

var SerializeManager = (function(){
	return {};
})();

SerializeManager.serializeJson = function(Obj){
	return (JSON.stringify(Obj));
};

SerializeManager.DeserializeJson = function(Obj){
	return (JSON.parse(Obj));
};

var setToolbarPopupPosition =  function(parent,child){
	return{
			top : -(parent.height()+$(child).height()-9),
			left : -(($(child).width()/2) - (parent.width()/2))-6
	}
}

/*array.some not supported in IE. So protype version for IE*/
if (!Array.prototype.some)
{
  Array.prototype.some = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this &&
          fun.call(thisp, this[i], i, this))
        return true;
    }

    return false;
  };
}
/***************************ToolBar - US 5437 Ends*************************************/



// #6426 -- Facebook Login -- Aleksi -- 2012-02-09 -- BEGIN

function facebook_userLoginHandler()
{
	$.get( "json/facebook/login/" , function( data ){
	
			if( data[ "status" ] == 200 )
			{
				// User was logged in, now reload to get the "logged in" view
				facebook_reloadAfterLogin();	
			}
			else if( data[ "status" ] == 307 )
			{
				// User is logged in Facebook, but no connection to MadBid users found => registration form
				setTimeout( "facebook_testRegisterSlide()", 100 );

				if( registerBridge.isClosed == true )
					$("a.register").click();
			}
			else
			{
				// Login failed; do nothing
				return;
			}
			
	});
}


function facebook_testRegisterSlide()
{
	if( registerBridge.isClosed == true )
	{
		setTimeout( "facebook_testRegisterSlide()", 30 );
	}
	else
	{
		facebook_userCreateHandler();
	}
}


function facebook_userCreateHandler()
{
	$.get( "json/facebook/get/" , function( data ){
		
		if( data[ "status" ] == 200 )
		{
			$('#register-facebook-button').addClass('hidden');
		
			// Successfully grabbed user data from Facebook -- now 
			// populate form fields
			$("#user_name").val( data[ "response" ][ "user_name" ] );
			$("#email").val( data[ "response" ][ "email" ] );
			
			ajax_validateField( 'email', 'email', 'result_email' );
			ajax_validateField( 'user', 'user_name', 'result_user_name' );
			
			$("#mobile").focus();			
		}
		else if( data[ "status" ] == 307 )
		{
			// User already has a MadBid account, so just reload the page to
			// get the "logged in" view
			facebook_reloadAfterLogin();					
		}
						
	});
}


function facebook_userConnectHandler()
{
	$.get( "json/facebook/connect/" , function( data ){
		
		if( data[ "status" ] == 200 )
		{
			facebook_reloadAfterLogin();	
		}
		
		// otherwise do nothing
	});
}


function facebook_reloadAfterLogin()
{
	// kludge kluge ! what if POST result?
	var url = window.location.pathname; 
	if(url.match( "/login" )!=-1){
		location.href = './';
	}else{
		location.reload();
	}

}

// #6426 -- Facebook Login -- Aleksi -- 2012-02-09 -- END

/** AUTO TOPUP **/
$( "input[type=checkbox]#save_card_on_file").change(function(){
if ( $("input[type=checkbox]#save_card_on_file:checked").length === 1 )
	$("input[type=checkbox]#enable_auto_topup").attr("checked", "checked");
else 
	$("input[type=checkbox]#enable_auto_topup").removeAttr("checked")
});
/** END AUTO TOPUP **/


/** RAPID RECHARGE **/

function rapid_recharge_on()
{
	prompt_close();
	$( "input[type=checkbox]#enable_rapid_recharge" ).attr( "checked", "checked" );
	$( "input[type=checkbox]#saved_enable_auto_topup" ).attr( "checked", "checked" );
}

function rapid_recharge_off()
{
	prompt_close();
	$( "input[type=checkbox]#enable_rapid_recharge" ).removeAttr( "checked" );
	$( "input[type=checkbox]#saved_enable_auto_topup" ).removeAttr( "checked" );
}

/** END RAPID RECHARGE **/

function countdown_timer()
{
	var timestamp_now = Math.round(+new Date()/1000);
	if( timestamp_now <= timestamp_count_to )
	{
		var js_timeout = new Date((timestamp_count_to - timestamp_now)*1000);
		
		var hours = js_timeout.getHours();
		var minutes = js_timeout.getMinutes();
		var seconds = js_timeout.getSeconds();
		
		if( hours < 10 )
		{
			hours = "0" + hours;
		}
		
		if( minutes < 10 )
		{
			minutes = "0" + minutes;
		}
		
		if( seconds < 10 )
		{
			seconds = "0" + seconds;
		}
		
		$("#hrs").text(hours);
		$("#min").text(minutes);
		$("#sec").text(seconds);
		
		setTimeout('countdown_timer()', 1000);
	}
}