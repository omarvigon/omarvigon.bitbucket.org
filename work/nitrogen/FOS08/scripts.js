// prevent iPad browser 'elastic' bounce movement
document.ontouchmove=function(e){e.preventDefault()}

$(document).ready(function() {	
	$('#pi-slide-link').click(function() {

		var piStatus = $('#pi-slide').css('top');

		if (piStatus == '768px') // if slide panel is hidden
        {
			var panelHeight = $('#pi-slide').height();
			//var displayPos = 1486 - panelHeight; // 1486 = 768(screen height) + 768(PI div height) - 50px panel top padding
			var displayPos = 2254 - panelHeight;   //NEW doble PI div height
			var display = displayPos + 'px';
			
			$('#pi-slide').animate({ top : display }, 500);
			$('#main-mask').fadeIn(300);
		}
		else
		{
			$('#pi-slide').animate({ top : '768px' }, 500);
			$('#main-mask').fadeOut(300);
		}
	});

	$('#main-mask').click(function() {

		var piSlideTop = $('#pi-slide').css('top');
		var topMenuStatus = $('#menu').css('top');

		if (piSlideTop !== '768px')
			$('#pi-slide').animate({ top : '768px' }, 500);
		else if (topMenuStatus == '0px')
			$('#menu').animate({ top : '-131px' }, 500);
		$(this).fadeOut(300);
		
		var isPopUp = $('#pop-up').css('opacity');
					
		if(isPopUp) {
			$('#pop-up').animate({'opacity': 0}, 200, function() {
    			$('#pop-up').css('display', 'none');
  			});
		}	
	});
	
    $('#showPI,#showPI2').click(function ()
	{
        var panelHeight = $('#pi-slide').height();
        var amount = ( $(this).attr('id') == 'showPI' )? 1490:695; //NEW, showPI or showPI2	
        var displayPos = panelHeight - amount;
        var display = (0 - displayPos) + 'px';

        $('#pi-slide').animate({ top: display }, 500);
    });
	
	var myScroll1 = new iScroll('pi-scroller-1', {
		hScrollbar : false,
		hScroll : false,
		bounce : false,
		checkDOMChanges : true,
        hideScrollbar : false
	});
	var myScroll2 = new iScroll('pi-scroller-2', {
		hScrollbar : false,
		hScroll : false,
		bounce : false,
		checkDOMChanges : true,
        hideScrollbar : false
	});
	
    $('button.closePI').click(function () {
        $('#pi-slide').animate( { top: '768px' }, 300);
        $('#main-mask').fadeOut(300);
    });

    $('.refLink,.refbutton').click(function () {
        $('#pi-slide').css('top', '768px');
        $('#main-mask').fadeOut(300);
    })				
					
	//Fade in the page 
	$("body").removeClass().delay(1000).queue(function(next){
		$(this).animate({opacity: 1},1000)
		$("ul.purple-bul li").each(function(index) {
			$(this).delay(150*index).animate({opacity: 1},400);
		});
		
		$("ul.brown-bul li").each(function(index) {
			$(this).delay(150*index).animate({opacity: 1},400);
		});
		next();
	});
	
	$('#goto2').click(function()
	{
		$('ol.refs').hide();
		$('ol.r2').show();
		$('#sec-1').fadeOut(200);
		$('#sec-2').fadeIn(400);
	});
	$('#goto3').click(function()
	{
		$('ol.refs').hide();
		$('ol.r3').show();
		$('#sec-1').fadeOut(200);
		$('#sec-3').fadeIn(400);
	});
	$('#goto4').click(function()
	{
		$('ol.refs').hide();
		$('ol.r1').show();
		$('#sec-1').fadeOut(200);
		$('#sec-4').fadeIn(400);
	});
	
	$('section .close-button').click(function()
	{
		$('ol.refs').hide();
		$('ol.r1').show();
		$('#sec-2,#sec-3,#sec-4').fadeOut(200);
		$('#sec-1').fadeIn(400);
		return false;
	});
	
	$('#info').click(function () //showPop
	{ 
		$('#main-mask, #pop-up').css('display', 'block');
		$('#pop-up').animate({'opacity': 1}, 200);
    });
	
	$('#pop-close').click(function () //hidePop
	{ 
		$('#pop-up').animate({'opacity': 0}, 200, function() {
    		$('#main-mask, #pop-up').css('display', 'none');
  		});
		return false;
    });
	
	var objValue = [6, 6, 6];
    var maxValue = 12;
    var maxWidth = 285;
    var prevCode;

    $('#key div:nth-child(1),#key div:nth-child(2)').toggle();

    $('#showResult').bind('click', function () {
        playBarChart();
        $('#key div:nth-child(1),#key div:nth-child(2)').toggle();
    });

    $('.slid').change(function () {
        var slider_value = $(this).val();
        setNewValue($(this).attr('name'), slider_value);
    })

    function setNewValue(sliderNum, val) {
        objValue[parseInt(sliderNum[7])] = val;
    }

    function playBarChart() {
        $('#multiSlider').fadeOut()//hide sliders
        $('#barchart').fadeIn();
        $('#showResult').css('display', 'none');
        $('#goto4').css('display', 'block');

        setAnimation('#slider-0');
        setAnimation('#slider-1');
        setAnimation('#slider-2');
    }

    function setAnimation(slider) {
        var num = $(slider).attr('name')[7];
        var widthValue = getWidth(num);
        var directionValue = getDirection(objValue[num])

        initUserResults(slider, num, widthValue, directionValue);
        initResults();
    }

    function getWidth(num) {

        var unitWidth = maxWidth / (maxValue / 2);
        var newWidth;

        if (getDirection(objValue[num]) == "right")
            newWidth = (objValue[num] - 6) * unitWidth;

        if (getDirection(objValue[num]) == "left")
            newWidth = maxWidth - (objValue[num] * unitWidth);

        return newWidth;
    }

    function getDirection(val) {
        var direction; (val < (maxValue / 2)) ? direction = "left" : direction = "right";
        return direction;
    }

    function initUserResults(slider, num, newWidth, directionValue) {
        $(slider).slider('disable');

        if (directionValue == "left")
            $('.greybar:eq(' + num + ') ').addClass('barleft');
        else
            $('.greybar:eq(' + num + ') ').addClass('barright');

        $('.greybar:eq(' + num + ') ').stop().animate({ width: newWidth }, 1200);
    }
    function initResults() {

        $(".brownbar:eq(0)").addClass('barleft').animate({ width: 89 + "px" }, 1200);
        $(".brownbar:eq(1)").addClass('barright').animate({ width: 83 + "px" }, 1200);
        $(".brownbar:eq(2)").addClass('barright').animate({ width: 49 + "px" }, 1200);
    }
});