(function()
{
	document.ontouchmove = function(e){ e.preventDefault(); }	// prevent iPad browser 'elastic' bounce movement
	
	var mask = document.querySelector('#mask');
	var pislide = document.querySelector('#pi-slide');
	
	document.querySelector('#pi-link').onclick=function()
	{
		var height = window.getComputedStyle( pislide ).getPropertyValue('height');	
		pislide.style.top=( 1486 - parseInt(height) )+'px';
		mask.removeAttribute('hidden');
	}
	mask.onclick=function()
	{
		pislide.style.top='';
		mask.setAttribute('hidden','hidden');
	};

	var box=document.querySelector('#box1');
	var boxlink=box.querySelectorAll('a');

	for(var i in boxlink)
	{
		boxlink[i].onclick=function()
		{
			var parent=this.parentNode;
			(parent.className=='box1small')? box.className='on':box.removeAttribute('class');
			box.querySelector('[hidden]').removeAttribute('hidden');
			parent.setAttribute('hidden','hidden');
		}
	}
	
	var main=document.querySelector('.main');
	var popup=document.querySelectorAll('.popup');
	
	for(var i in popup)
	{
		if(popup[i].querySelector)
		{
			popup[i].querySelector('.close').onclick=function()
			{
				this.parentNode.classList.remove('on');
				main.className='main';
			}
		}
	}

	main.querySelector('.study').onclick=function()
	{
		popup[0].classList.add('on');
		main.className='main off';
	}
	main.querySelector('.graph').onclick=function()
	{
		popup[1].classList.add('on');
		main.className='main off';
		popup[1].querySelector('.graph').classList.add('start');
	}
	
	//pagescripts
	var cube=document.querySelector("#cube div");
	var link1=document.querySelector('.cube-1');
	var link2=document.querySelector('.cube-2');
	
	link1.onclick=function()
	{
		cube.className=this.className;
		this.setAttribute('hidden','hidden');
		link2.removeAttribute('hidden');
	}
	link2.onclick=function()
	{
		cube.className=this.className;
		this.setAttribute('hidden','hidden');
		link1.removeAttribute('hidden');
	}	
})()