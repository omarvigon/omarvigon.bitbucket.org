// prevent iPad browser 'elastic' bounce movement
document.ontouchmove=function(e){e.preventDefault()}

$(document).ready(function() {	
	$('#pi-slide-link').click(function() {

		var piStatus = $('#pi-slide').css('top');

		if (piStatus == '768px') // if slide panel is hidden
        {
			var panelHeight = $('#pi-slide').height();
			//var displayPos = 1486 - panelHeight; // 1486 = 768(screen height) + 768(PI div height) - 50px panel top padding
			var displayPos = 2254 - panelHeight;   //NEW doble PI div height
			var display = displayPos + 'px';
			
			$('#pi-slide').animate({ top : display }, 500);
			$('#main-mask').fadeIn(300);
		}
		else
		{
			$('#pi-slide').animate({ top : '768px' }, 500);
			$('#main-mask').fadeOut(300);
		}
	});

    $('#main-mask').click(function() {

        var piSlideTop = $('#pi-slide').css('top');
        var topMenuStatus = $('#menu').css('top');

		if (piSlideTop !== '768px')
			$('#pi-slide').animate({ top : '768px' }, 500);
		else if (topMenuStatus == '0px')
			$('#menu').animate({ top : '-131px' }, 500);
        $(this).fadeOut(300);
    });

    $('#showPI,#showPI2').click(function ()
	{
        var panelHeight = $('#pi-slide').height();
        var amount = ( $(this).attr('id') == 'showPI' )? 1490:695; //NEW, showPI or showPI2	
        var displayPos = panelHeight - amount;
        var display = (0 - displayPos) + 'px';

        $('#pi-slide').animate({ top: display }, 500);
    });
	
	var myScroll1 = new iScroll('pi-scroller-1', {
		hScrollbar : false,
		hScroll : false,
		bounce : false,
		checkDOMChanges : true,
        hideScrollbar : false
	});
	var myScroll2 = new iScroll('pi-scroller-2', {
		hScrollbar : false,
		hScroll : false,
		bounce : false,
		checkDOMChanges : true,
        hideScrollbar : false
	});
	
    $('button.closePI').click(function () {
        $('#pi-slide').animate( { top: '768px' }, 300);
        $('#main-mask').fadeOut(300);
    });

    $('.refLink,.refbutton').click(function () {
        $('#pi-slide').css('top', '768px');
        $('#main-mask').fadeOut(300);
    })

	//Fade in the page 
	$("body").removeClass().delay(1000).queue(function(next){
		$(this).animate({opacity: 1},1000)
		$("ul.purple-bul li").each(function(index) {
			$(this).delay(150*index).animate({opacity: 1},400);
		});
		
		$("ul.brown-bul li").each(function(index) {
			$(this).delay(150*index).animate({opacity: 1},400);
		});
		next();
	});

    $('#chart_2').click(function () {
        $('#chart_bg').fadeIn(200);
        $('#main_content, article > button').fadeOut(200);
        $('.footer-summary').fadeIn(300);

        animategroups2();
    });

    $('#main_content').click(function () {
        $('div#references ol li:lt(2)').hide(); // Hide the first two ref LI
        $('div#references ol').attr({start: '3'}); // Change ref ordered list start number
        $('#chart_bg').fadeIn(200);
        $('#main_content, article > button').fadeOut(200);
        $('.footer-summary').fadeIn(300);
        animategroups2();
    });
	
    $('#page_close').click(function (e) {
        $('div#references ol li:lt(2)').show(); // Show the first two ref LI
        $('div#references ol li:nth-child(2)').css({ height: 'inherit !important' }); // To fix strange height value that jQuery was adding to the 2nd LI on show.
        $('div#references ol').attr({ start: '1' }); // Change ref ordered list start number
        e.preventDefault();
        $('#chart_bg').fadeOut(200);
        $('#main_content, article > button').fadeIn(200);
    });
	
	// create the bar chart		
	$('#barchart-small').chartAxis({ 
		'width': 400,
		'height': 400,
		'backgroundColor' : 'none',
		'headerText': '',

		// additionalText: can contain additional values, must specify the text, size, alignment and x,y position
		'additionalText': [ [ [''], [9], ['center'], [254], [252] ] ],			
		'plotAreaLeft':0,
		'plotAreaRight': 0,
		'plotAreaTop': 0,
		'plotAreaBottom': 0,		
		'yAxisValues': [ '', 
			'', 
			'', 
			'', 
			'', 
			'', 
			'', 
			'', 
			'' ],
		'yLabel': '',
		'yMaxValue': 10,
		'xAxisValues': [ '', '' ],
	});
	
	animategroups();
});
function animategroups()
{
	$('.greybar').hide();
	animategroup1();
	setTimeout("$('.greybar').show();animategroup2()",2000);
}
function animategroups2()
{
	$('.greybar-popup').hide();
	animategrouppopup1();
	setTimeout("$('.greybar-popup').show();animategrouppopup2();",2000);
}
function animategroup1()
{
	animateBar('#barchart-small .brownbar:nth-of-type(1)',40.1,31,92,2000,0,0,'');
	animateBar('#barchart-small .brownbar:nth-of-type(3)',40.1,31,104,2000,0,0,'');
	animateBar('#barchart-small .brownbar:nth-of-type(5)',40.1,31,105,2000,0,0,'');
}
function animategroup2()
{
	animateBar('#barchart-small .greybar:nth-of-type(2)',40.1,31,163,2000,0,0,'*');
	animateBar('#barchart-small .greybar:nth-of-type(4)',40.1,31,185,2000,0,0,'*');
	animateBar('#barchart-small .greybar:nth-of-type(6)',40.1,31,144,2000,0,0,'*');
}
function animategrouppopup1()
{
	animateBar('#barchart-small-popup .brownbar-popup:nth-of-type(1)',45.1,22,92,2000,0,0,'');
	animateBar('#barchart-small-popup .brownbar-popup:nth-of-type(3)',45.1,22,104,2000,0,0,'');
	animateBar('#barchart-small-popup .brownbar-popup:nth-of-type(5)',45.1,22,105,2000,0,0,'');
}
function animategrouppopup2()
{
	animateBar('#barchart-small-popup .greybar-popup:nth-of-type(2)',45.1,22,163,2000,0,0,'*');
	animateBar('#barchart-small-popup .greybar-popup:nth-of-type(4)',45.1,22,185,2000,0,0,'*');
	animateBar('#barchart-small-popup .greybar-popup:nth-of-type(6)',45.1,22,144,2000,0,0,'*');
}

$('#page_close').click(function(){
	$('.refs li:nth-of-type(2)').css('visibility', 'visible').animate({'height' : 28});
	animategroups();

});

// Function that animates the bar in the chart
function animateBar(targetElement,maxHeight,maxHeightValue,targetValue,animationDuration,decimalPlacesRounding, barDelay, symbolAtTheEnd)
{
	// get targetElement height in pixels that we need to animate to
	var targetElementHeight = maxHeight*targetValue/maxHeightValue;
	
	// prepare the element for the animation
	$(targetElement).css({
		height:targetElementHeight,
		bottom: -targetElementHeight
	});

	// animate the div
	$(targetElement + ' p').fadeIn(550); // optional
	
	$(targetElement).delay(barDelay).animate( {bottom: 0}, 
	{
		duration:animationDuration,
		step: function(now) {
		var currentValue = (now + targetElementHeight)*maxHeightValue/maxHeight;
		var valueToStr = currentValue.toFixed(decimalPlacesRounding).toString();
		var textValue = valueToStr.replace('.',',');
		$(targetElement + ' p').html(textValue + symbolAtTheEnd);
	}
});
};

/*
Name: jquery.chartaxis.js Description: A jQuery plugin that works with HTML and CSS to draw the x and y axes of a chart.
*/

(function( $ ){

	$.fn.chartAxis = function( options ) {

		var settings = $.extend( {

			'color': '#00000',
			'backgroundColor': '#fff',
			'width': 431,
			'height': 371,
			'headerText': 'Header text',

			// additionalText: can contain additional values, must specify the text, size, alignment and x,y position
			'additionalText': [ [ ['Additional text'], [7], ['right'], [240], [45] ], 
				  [ ['More additional text'], [6], ['right'], [240], [64] ], 
				  [ ['Footnote¹'], [9], ['left'], [75], [350] ] ],
			
			'plotAreaLeft': 73,
			'plotAreaRight': 411,
			'plotAreaTop': 60,
			'plotAreaBottom': 291,
			'axisColor': '#000000',
			'yAxisValues': [ '0','1','2','3','4','5','6','7','8','9','10' ],
			'yLabel': 'y-Axis Label',
			'yMaxValue': 10,
			'xAxisValues': [ 'Bar one', 'Bar two', 'Bar three', 'Bar four' ],
			'markWidth': 3,
			'barSpacing': 20, 
			'barBorder': false, 
			'doubleBarSpacingBetweenBarsButNotBetweenBarAndPlotAreaBorder': true, 
			'font': 'FrutigerLTStd45LightRegular', 
			'fontFamily': 'FrutigerLTStd45LightRegular', 
			'fontBold': 'FrutigerLTStd67Bold', 
			'fontFamilyBold': 'FrutigerLTStd67Bold',
			'planeBarColors': [ ], // for specifying colours for legend boxes
			'planeLegend': [ ], // for specifying the plane legend labels
			'legendBottomOffset' : 0,
			'legendXOffset' : 0
			
		}, options);
	    
		return this.each(function() {

			// begin: main
			var id = $(this).attr('id');
			var adjustment = 1;
			// if this is the larger bar chart, adjust the size
			if (id == 'barchart-large')
			{
				var regularChartWidth = $('#barchart-small').width();
				var largeChartWidth = $('#barchart-large').width();
				adjustment = largeChartWidth / regularChartWidth;
				settings.width = Math.round(settings.width * adjustment);
				settings.height = Math.round(settings.height * adjustment);
				settings.plotAreaLeft = Math.round(settings.plotAreaLeft * adjustment);
				settings.plotAreaRight = Math.round(settings.plotAreaRight * adjustment);
				settings.plotAreaTop = Math.round(settings.plotAreaTop * adjustment);
				settings.plotAreaBottom = Math.round(settings.plotAreaBottom * adjustment);
				settings.markWidth = Math.round(settings.markWidth * adjustment);
				settings.barSpacing = Math.round(settings.barSpacing * adjustment);
			}

			var plotAreaWidth = settings.plotAreaRight - settings.plotAreaLeft;
			var plotAreaHeight = settings.plotAreaBottom - settings.plotAreaTop;
			var nrOfPlanes = settings.planeLegend.length; // derive the number of planes from the first data object

			// add the background layer, 5px padding for rounded corners and drop shadow
			var backgroundMarkup = '<div class="background" '
				+ 'style="display: block; border: 1px solid ' 
				+ settings.backgroundColor 
				+ '; background-color: ' + settings.backgroundColor + '; ' 
				+ 'width: ' + (settings.width - 10) + 'px; ' 
				+ 'height: ' + (settings.height - 10) + 'px; '
				+ 'position: absolute; top: 5px; left: 5px;'
				+ '"></div>';
			$('#' + id).append(backgroundMarkup);

			// add the chart, bars and bar-values layers
			var xAxisLineMarkup = '<div class="xaxis" ' 
				+ 'style="display: block; border: none; background-color: ' 
				+ settings.axisColor + '; ' 
				+ 'width: ' + plotAreaWidth + 'px; ' 
				+ 'height: 1px; '
				+ 'position: absolute; '
				+ 'left: ' + settings.plotAreaLeft + 'px; '
				+ 'top: ' + settings.plotAreaBottom + 'px; ' 
				+ '"></div>';
			$('#' + id).append(xAxisLineMarkup);
			
	        // add the x-axis labels and data 
	        for(var n = 0; n < settings.xAxisValues.length; n++) {
				var i = n;
				var plotAreaWidth = settings.plotAreaRight - settings.plotAreaLeft;
				var nrOfxAxisValues = settings.xAxisValues.length;
				var nrOfBarGutters = nrOfxAxisValues + 1;
				var xAxisLabelWidth = plotAreaWidth / nrOfxAxisValues;
				var barsWidth;
				
				if (settings.doubleBarSpacingBetweenBarsButNotBetweenBarAndPlotAreaBorder)
					barsWidth = Math.round((plotAreaWidth - (settings.barSpacing 
						* (nrOfBarGutters * 2 - 2))) / nrOfxAxisValues);
				else 
					barsWidth = Math.round((plotAreaWidth - (settings.barSpacing * nrOfBarGutters)) 
						/ nrOfxAxisValues);

				// add the axis mark
				var markSpacing = Math.round(i * (plotAreaWidth / nrOfxAxisValues));
				
				// add the label
				var labelX;
				if (settings.doubleBarSpacingBetweenBarsButNotBetweenBarAndPlotAreaBorder)
					labelX = Math.round(i * xAxisLabelWidth);
				else 
					labelX = Math.round((i * barsWidth) + ((i + 1) * settings.barSpacing));

				var labelMarkup = '<div class="xaxislabel xaxislabel' + (i + 1) + ' "'
					+ 'style="display: block; border: none; ' 
					+ 'width: ' + xAxisLabelWidth + 'px; ' 
					+ 'position: absolute; '
					+ 'left: ' + (settings.plotAreaLeft + labelX) + 'px; '
					+ 'top: ' + (settings.plotAreaBottom + (5 * adjustment)) + 'px; '
					+ 'font-size: ' + Math.round(8 * adjustment) + 'pt; '
					+ 'line-height: 1.4; text-align: center; '
					+ 'color: ' + settings.color + '; '
					+ '">' + settings.xAxisValues[i] + '</div>';
				$('#' + id).append(labelMarkup);
	        }

			var yAxisLineMarkup = '<div style="display: block; border: none; background-color: ' 
				+ settings.axisColor + '; ' 
				+ 'width: 1px; ' 
				+ 'height: ' + plotAreaHeight + 'px; '
				+ 'position: absolute; '
				+ 'left: ' + settings.plotAreaLeft + 'px; '
				+ 'top: ' + settings.plotAreaTop + 'px; ' 
				+ '"></div>';
			$('#' + id).append(yAxisLineMarkup);

	        // add the y-axis marks and labels 
    		// add the label
			var yLabelMarkup = '<div class="ylabel" '
				+ 'style="display: block; border: none; ' 
				+ '-webkit-transform: rotate(-90deg); ' 
				+ 'position: absolute; '
				+ 'left: ' + (settings.plotAreaLeft - 250 - (40 * adjustment)) + 'px; '
				+ 'top: ' + (settings.plotAreaTop
					+ (plotAreaHeight / 2) - (10 * adjustment)
					) + 'px; ' 
				+ 'font-size: ' + Math.round(8 * adjustment) + 'pt; '
				+ 'color: ' + settings.color + '; '
				+ 'width: 500px; '
				+ 'font-family: ' + settings.fontFamily + '; '
				+ 'line-height: 1.4; text-align: center; '
				+ '">' + settings.yLabel + '</div>';
			$('#' + id).append(yLabelMarkup);

	        for(var n = 0; n < (settings.yAxisValues.length); n++) {
				var i = n;
				var markSpacing = Math.round(i * (plotAreaHeight / (settings.yAxisValues.length - 1)));

				// add the axis mark
				var markMarkup = '<div style="display: block; border: none; background-color: ' 
					+ settings.axisColor + '; ' 
					+ 'width: ' + settings.markWidth + 'px; ' 
					+ 'height: 1px; '
					+ 'position: absolute; '
					+ 'left: ' + (settings.plotAreaLeft - settings.markWidth) + 'px; '
					+ 'top: ' + (settings.plotAreaTop + markSpacing) + 'px; ' 
					+ '"></div>';
				$('#' + id).append(markMarkup);
				
				// add the label
				var labelMarkup = '<div class="yaxislabel yaxislabel' + (i + 1) + '" '
					+ 'style="display: block; border: none; ' 
					+ 'width: ' + Math.round(20 * adjustment) + 'px; ' 
					+ 'position: absolute; '
					+ 'left: ' + (settings.plotAreaLeft - settings.markWidth - (30 * adjustment)) + 'px; '
					+ 'top: ' + (settings.plotAreaTop + markSpacing - (8 * adjustment)) + 'px; ' 
					+ 'font-size: ' + Math.round(8 * adjustment) + 'pt; '
					+ 'line-height: 1.4; text-align: right; '
					+ 'color: ' + settings.color + '; '
					+ '">' + settings.yAxisValues[(settings.yAxisValues.length - 1) - i] + '</div>';
				$('#' + id).append(labelMarkup);
	        }

    		// add additional text, add the header
			var headerTextMarkup = '<div class="header" '
				+ 'style="display: block; border: none; ' 
				+ 'width: ' + settings.width + 'px; ' 
				+ 'position: absolute; '
				+ 'top: ' + (15 * adjustment) + 'px; ' 
				+ 'font-size: ' + Math.round(10 * adjustment) + 'pt; '
				+ 'font-family: ' + settings.fontFamilyBold + '; '
				+ 'line-height: 1.2em; text-align: center; '
				+ 'color: ' + settings.color + '; '
				+ '">' + settings.headerText + '</div>';
			$('#' + id).append(headerTextMarkup);

	        for(var n = 0; n < settings.additionalText.length; n++) {
				var i = n;
				var textMarkup = '<div class="additional-text additional-text' + (i + 1) + '" '
					+ 'style="display: block; border: none; ' 
					+ 'position: absolute; '
					+ 'left: ' + (settings.additionalText[i][3] * adjustment) + 'px; ' 
					+ 'top: ' + (settings.additionalText[i][4] * adjustment) + 'px; ' 
					+ 'font-size: ' + Math.round(settings.additionalText[i][1] * adjustment) + 'pt; '
					+ 'line-height: 1.2em; text-align: ' + settings.additionalText[i][2][0] + '; '
					+ 'color: ' + settings.color + '; '
					+ '">' + settings.additionalText[i][0][0] + '</div>';
				$('#' + id).append(textMarkup);
	        }
			
			// add the planes legend make sure the number of planes and labels. are the same as we need the colours for the labels
			if (nrOfPlanes == settings.planeLegend.length) 
			{
				var chartLegendMarkup = '<div class="chart-legend" style="position:absolute; width:100%; '
				+ 'bottom: ' + settings.legendBottomOffset + 'px; '
				+ 'left: ' + Math.round(settings.legendXOffset * adjustment) + 'px; '
				+ '">';
				
				for(var legendCount = nrOfPlanes-1; legendCount >= 0 ;  legendCount--) {
					var legendBoxSideLength = Math.round(10 * adjustment);
					var legendBoxSpacing = Math.round(7 * adjustment);
					var legendItemWidth = Math.round(200 * adjustment);
					var legendBoxMarkup = '<div class="legendbox legendbox' + (i + 1) + '" ' 
						+ 'style="display: block; border: 1px solid #000;' + '; '
						+ 'background-color: ' 
						+ settings.planeBarColors[nrOfPlanes - legendCount - 1] + '; ' 
						+ 'width: ' + legendBoxSideLength + 'px; ' 
						+ 'height: ' + legendBoxSideLength + 'px; '
						+ 'float: left; margin-right:10px;'
						+ '"></div>';
						
					chartLegendMarkup += legendBoxMarkup;

					var legendLabelMarkup = '<div class="legendlabel legendlabel' + (i + 1) + '" '
						+ 'style="float:left; border: none; padding-right:' + Math.round(50 * adjustment) + 'px;' 
						+ 'font-size: ' + Math.round(6 * adjustment) + 'pt; '
						+ 'line-height: 1.65em; text-align: left; '
						+ 'color: ' + settings.color + '; '
						+ '">' + settings.planeLegend[nrOfPlanes - legendCount - 1] + '</div>';
					
					chartLegendMarkup += legendLabelMarkup;	
				}
			}
			$('#' + id).append(chartLegendMarkup + '</div>');
		});
	};

})( jQuery );