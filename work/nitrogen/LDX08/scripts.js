(function()
{
	document.ontouchmove = function(e){ e.preventDefault(); }	// prevent iPad browser 'elastic' bounce movement
	
	var mask = document.querySelector('#mask');
	var pislide = document.querySelector('#pi-slide');
	
	document.querySelector('#pi-link').onclick=function()
	{
		var height = window.getComputedStyle( pislide ).getPropertyValue('height');	
		pislide.style.top=( 1486 - parseInt(height) )+'px';
		mask.removeAttribute('hidden');
	}
	mask.onclick=function()
	{
		pislide.style.top='';
		mask.setAttribute('hidden','hidden');
	};
	
	var main=document.querySelector('.main');
	var popup=document.querySelector('.popup');
	var ol=pislide.querySelector('ol');
	
	popup.querySelector('.close').onclick=function()
	{
		popup.classList.remove('on');
		main.className='main start';
		ol.removeAttribute('class');
	}
	main.querySelector('.graph').onclick=function()
	{
		popup.classList.add('on');
		main.className='main off';
		ol.className='off';
	}
	
	//pagescripts
	main.className='main start';
	var links=popup.querySelectorAll('.mask');
	
	for(var i in links)
		links[i].onclick=function()	{ this.style.opacity='0' }

})();