// prevent iPad browser 'elastic' bounce movement
document.ontouchmove=function(e){e.preventDefault()}

$(document).ready(function() {	
	$('#pi-slide-link').click(function() {

		var piStatus = $('#pi-slide').css('top');

		if (piStatus == '768px') // if slide panel is hidden
        {
			var panelHeight = $('#pi-slide').height();
			//var displayPos = 1486 - panelHeight; // 1486 = 768(screen height) + 768(PI div height) - 50px panel top padding
			var displayPos = 2254 - panelHeight;   //NEW doble PI div height
			var display = displayPos + 'px';
			
			$('#pi-slide').animate({ top : display }, 500);
			$('#main-mask').fadeIn(300);
		}
		else
		{
			$('#pi-slide').animate({ top : '768px' }, 500);
			$('#main-mask').fadeOut(300);
		}
	});

	$('#main-mask').click(function() {

		var piSlideTop = $('#pi-slide').css('top');
		var topMenuStatus = $('#menu').css('top');

		if (piSlideTop !== '768px')
			$('#pi-slide').animate({ top : '768px' }, 500);
		else if (topMenuStatus == '0px')
			$('#menu').animate({ top : '-131px' }, 500);
		$(this).fadeOut(300);
	});
	
    $('#showPI,#showPI2').click(function ()
	{
        var panelHeight = $('#pi-slide').height();
        var amount = ( $(this).attr('id') == 'showPI' )? 1490:695; //NEW, showPI or showPI2	
        var displayPos = panelHeight - amount;
        var display = (0 - displayPos) + 'px';

        $('#pi-slide').animate({ top: display }, 500);
    });
	
	var myScroll1 = new iScroll('pi-scroller-1', {
		hScrollbar : false,
		hScroll : false,
		bounce : false,
		checkDOMChanges : true,
        hideScrollbar : false
	});
	var myScroll2 = new iScroll('pi-scroller-2', {
		hScrollbar : false,
		hScroll : false,
		bounce : false,
		checkDOMChanges : true,
        hideScrollbar : false
	});
	
    $('button.closePI').click(function () {
        $('#pi-slide').animate( { top: '768px' }, 300);
        $('#main-mask').fadeOut(300);
    });

    $('.refLink,.refbutton').click(function () {
        $('#pi-slide').css('top', '768px');
        $('#main-mask').fadeOut(300);
    })			
	
	//Fade in the page 
	$("body").removeClass().delay(1000).queue(function(next){
		$(this).animate({opacity: 1},1000)
		$("ul.purple-bul li").each(function(index) {
			$(this).delay(150*index).animate({opacity: 1},400);
		});
		
		$("ul.star-bul li").delay(1750).queue(function(next){
			$(this).animate({opacity: 1},400);
			next();
		});
		next();
	});

	$('#te-prev').click(function()
	{
		$('img.p2').hide();
		$('img.p1').show();
		
	});
	$('#te-next').click(function()
	{
		$('img.p1').hide();
		$('img.p2').show();
	});
});

/**
 * jquery.transitions.js CSS3 Animations for Image Transitions
 * http://www.codrops.com Copyright 2011, Pedro Botelho / Codrops Free to use under the MIT license. Date: Mon Dec 19 2011
 */
$(function() {
	
	var TransitionEffects	= (function() {

		var $teWrapper		= $('#te-wrapper'),
			$teCover		= $teWrapper.find('div.te-cover'),
			$teDivs			= $teWrapper.find('div.te-divs > .face'),
			divCount		= $teDivs.length,
			currentDiv		= 0,
			$navNext		= $('#te-next'),
			$navPrev		= $('#te-prev'),
			$teTransition	= $teWrapper.find('.te-transition'),
			// requires perspective
			wPerspective	= [ 'te-cube'],
			animated		= false,
			init			= function() {
				
				$navNext.on( 'click', function( event ) {
					
					if( animated )
						return false;
						
					animated = true;	
					showNext();
					return false;
					
				});
				
				$navPrev.on( 'click', function( event ) {
					
					if( animated )
						return false;
						
					animated = true;	
					showPrev();
					return false;
					
				});
				
				$teWrapper.on({
					'webkitAnimationStart' : function( event ) {
						
					},
					'webkitAnimationEnd'   : function( event ) {

						$teCover.removeClass('te-hide');			
						$teWrapper.removeClass('te-perspective');
						$teTransition.removeClass('te-show');
						$teTransition.removeClass('te-show-rev');
						animated = false;
					}
				});
			},
			showNext		= function() {
				
				$('#te-next').hide();
				$('#te-prev').show();

				$teTransition.addClass('te-show');
				$teCover.addClass('te-hide');						
				$teWrapper.addClass('te-perspective');
				updateDivs();
			},
			showPrev		= function() {
				
				$('#te-prev').hide();
				$('#te-next').show();
				
				$teTransition.addClass('te-show-rev');
				$teCover.addClass('te-hide');
				$teWrapper.addClass('te-perspective');
				updateDivsRev();
			},
			updateDivs	= function() {
				var $back 	= $teTransition.find('div.te-back'),
					$front	= $teTransition.find('div.te-front');
				
				( currentDiv === divCount - 1 ) ? ( last_div = divCount - 1, currentDiv = 0 ) : ( last_div = currentDiv, ++currentDiv );
				
				var $last_div 	= $teDivs.eq( last_div ),
					$currentDiv	= $teDivs.eq( currentDiv );
				
				$front.empty().append($last_div.html());
				$back.empty().append($currentDiv.html());
				$teCover.html($currentDiv.html());
				
				$('#pi-slide #p1').hide();
				$('#pi-slide #p2').show();
			},
			
			updateDivsRev	= function() {
				var $back 	= $teTransition.find('div.te-back'),
					$front	= $teTransition.find('div.te-front');
				
				( currentDiv === divCount + 1 ) ? ( last_div = divCount + 1, currentDiv = 0 ) : ( last_div = currentDiv, --currentDiv );
				
				var $last_div 	= $teDivs.eq( last_div ),
					$currentDiv	= $teDivs.eq( currentDiv );
				
				$front.empty().append($last_div.html());
				$back.empty().append($currentDiv.html());
				$teCover.html($currentDiv.html());
				
				$('#pi-slide #p2').hide();
				$('#pi-slide #p1').show();
			};
			
		return { init : init };

	})();
	
	TransitionEffects.init();	
});