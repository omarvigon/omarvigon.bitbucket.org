function masinfo(num)
{
	var texto="<img src='img/tienda"+num+".jpg' alt='Ribes & Casals establecimientos' align='right' hspace='16' vspace='60'>";
	switch(num)
	{
		case 1:
		texto=texto+"<h2>Pau Claris, 79 (Barcelona)<br>Tel. 93.301.23.45</h2>Venta de tejidos al detall y mayor.<p>Este establecimiento es uno de los almacenes m�s antiguos de la ciudad dedicados a la venta de tejidos. En este gran espacio de 700m2 se encuentra una extensa colecci�n en la que se puede encontrar todo tipo de tejidos en un ambiente armon�a entre su antig�edad y su adaptaci�n a los nuevos tiempos.";
		break;
		
		case 2:
		texto=texto+"<h2>Roger de Ll�ria, 7 (Barcelona)<br>Tel. 93.343.79.00</h2>Venta de tejidos al detall y mayor.<p>Este establecimiento fue inaugurado en el a&ntilde;o 2000, cuenta con una amplia superficie en la que se pueden encontrar todo tipo de tejidos.";
		break;
		
		case 3:
		texto=texto+"<h2>Atocha, 26 (Madrid)<br>Tel. 91.369.45.00</h2>Este establecimiento cuenta con tres plantas en las que se pueden encontrar todo tipo de tejidos.";
		break;
		
		case 4:
		texto=texto+"<h2>Linotipia, manzana 4 nave 23 (Sevilla)<br>Tel. 954.997.420</h2>En este establecimiento se pueden encontrar una gran variedad de tejidos de fantas�a, flamenca, espect�culos y decoraciones.<br><br>Pol�gono La Negrilla<br>C/ Linotipia, manzana 4 nave 23 (pasaje interior)<br>41016 SEVILLA <br>Tel. 954.997.420<br>Fax 954.527.689";
		break;
		
		default:
		break;
	}
	document.getElementById('linea5').innerHTML="<a href='javascript:masinfo(1)'>Pau Claris,79 (Barcelona)</a> | <a href='javascript:masinfo(2)'>Roger de Ll�ria, 7 (Barcelona)</a> | <a href='javascript:masinfo(3)'>Atocha, 26 (Madrid)</a> | <a href='javascript:masinfo(4)'>Sevilla(Pol�gono la Negrilla)</a>";
	document.getElementById('linea4').innerHTML=texto;
}
function tejidos2(num)
{
	var texto="<img src='img/tejidos"+num+".jpg' alt='Nuestros tejidos' align='right'>";
	switch(num)
	{
		case 1:
		texto=texto+"En <b>RIBES & CASALS</b> se puede encontrar todo tipo de tejidos de diferentes calidades y en una gran gama de colores para as� poder confeccionar todo tipo de prendas, desde trajes a vestidos de novia.";
		break;
		
		case 2:
		texto=texto+"Tenemos un amplio surtido de tejidos para todo tipo de decorados, entre ellos se pueden encontrar: <ul><li>s�banas</li><li>manteler�as</li><li>tapicer�as</li><li>rizos de toallas</li><li>lonetas</li><li>etc.</li></ul>";
		break;
		
		case 3:
		texto=texto+"<b>RIBES & CASALS</b> proporciona un amplio surtido de art�culos de fantas�a relacionados con el mundo del espect�culo, el carnaval y el flamenco. Este mercado es una de nuestras especialidades y proveemos a una gran variedad de comparsas y espect�culos. En nuestros establecimientos se pueden encontrar:<ul><li>topos</li><li>lycras</li><li>rasos</li><li>terciopelos</li><li>lam�s</li><li>pelos sint�ticos</li><li>lentejuelas</li><li>etc.</li></ul>";
		break;
	}
	document.getElementById('linea4').innerHTML=texto;
}
function tejidos3(num)
{
	var texto="<img src='img/tejidos"+num+".jpg' alt='Nuestros tejidos' align='right'>";
	switch(num)
	{
		case 1:
		texto=texto+"In <b>RIBES & CASALS</b> it is possible to find a lots of diferent fabrics of diferent qualities and colours, ideal for suits, wedding dresses, night dresses etc.";
		break;
		
		case 2:
		texto=texto+"We have a wide range of textiles for home and decoration, like:<ul><li>sheets</li><li>table linen</li><li>upholstery</li><li>towels, etc.</li></ul>";
		break;
		
		case 3:
		texto=texto+"<b>RIBES & CASALS</b> offers a wide range of articles related to the show business and to carnival. This market is one of our biggest markets, and we provide to many different groups. In our establishments you can find:<ul><li>lycras</li><li>satin</li><li>velvets</li><li>metallics</li><li>spangels</li><li>foil printed, etc</li></ul>";
		break;
	}
	document.getElementById('linea4').innerHTML=texto;
}
function tejidos4(num)
{
	var texto="<img src='img/tejidos"+num+".jpg' alt='Nuestros tejidos' align='right'>";
	switch(num)
	{
		case 1:
		texto=texto+"Chez <b>RIBES & CASALS</b> vous pourrez trouver une grande vari�t� de tissus de diff�rentes qualit�s et un large �ventail de couleurs afin de pouvoir confectionner des costumes, des robes de mari� et des tenues de f�tes, de c�r�monie, etc. Parmi les soies que nous proposons vous trouverez :<ul><li>soies naturelles</li><li>soies sauvages</li><li>taffetas</li><li>satins</li><li>taffetas glac�s</li><li>ottomans</li><li>grand choix de cr�pes, etc.</li></ul>";
		break;
		
		case 2:
		texto=texto+"Nous disposons d'un grand choix de tissus pour la maison pour confectionner :<ul><li>linge</li><li>draps de lit</li><li>tapisserie</li><li>tissu-�ponge pour serviettes de toilette, etc.</li></ul>";
		break;
		
		case 3:
		texto=texto+"<b>RIBES & CASALS</b> fournit une large gamme d'articles fantaisie destin�s au monde du spectacle et aux carnavals. Ce march� est l'une de nos sp�cialit�s et nous fournissons bon nombre de figurants et de spectacles. Vous trouverez dans nos �tablissements :<ul><li>lycras</li><li>satins</li><li>velours</li><li>lam�s</li><li>fourrures synth�tiques</li><li>dentelles, etc.</li></ul>";
		break;
	}
	document.getElementById('linea4').innerHTML=texto;
}
function mapa(cual)
{
	switch(cual)
	{
		case 1:
		document.getElementById('capamapa').innerHTML="Barcelona<a href='javascript:cerrar()'>[X]</a><br><img src='img/mapa_bcn.jpg' border='0' hspace='10' vspace='10' alt='Mapa del establecimiento'>";
		break;
		
		case 2:
		document.getElementById('capamapa').innerHTML="Madrid<a href='javascript:cerrar()'>[X]</a><br><img src='img/mapa_madrid.jpg' border='0' hspace='10' vspace='10' alt='Mapa del establecimiento'>";
		break;
		
		case 3:
		document.getElementById('capamapa').innerHTML="Sevilla<a href='javascript:cerrar()'>[X]</a><br><img src='img/mapa_sevilla.jpg' border='0' hspace='10' vspace='10' alt='Mapa del establecimiento'>";
		break;
	}
	document.getElementById('capamapa').style.visibility="visible";
}
function cerrar()
{
	document.getElementById('capamapa').style.visibility="hidden";
}
var x=0;
function cambiarimg()
{
	if(x==5)	x=1;
	else		x++;
	document.all['cabecera'].src="img/cabecera"+x+".jpg";
	setTimeout("cambiarimg()",5000);
}
function iniciar()
{
	var ruta1=document.location.href;
	var ruta2=ruta1.substr(ruta1.length-4);
	var i=ruta1.length;
	while(ruta1.charAt(i)!="/")
	{
		temp=i;
		i--;
	}
	ruta2=ruta1.substr(temp);
	ruta3=ruta2.substr(ruta2.length-3);
	
	switch(ruta3)
	{
		case "ing":
		document.getElementById('linea2a').innerHTML="<a href='ribescasals.htm?ing'>About Ribes & Casals</a> | <a href='establecimientos.htm?ing'>Our establishments</a> | <a href='tejidos.htm?ing'>Our Fabrics</a>";
		break;
		
		case "fra":
		document.getElementById('linea2a').innerHTML="<a href='ribescasals.htm?fra'>Connaissez Ribes & Casals</a> | <a href='establecimientos.htm?fra'>Nos �tablissements</a> | <a href='tejidos.htm?fra'>Nos tissus</a>";
		break;
	}
	
	switch(ruta2)
	{
		case "index.htm?ing":
		document.getElementById('linea3').innerHTML="<h1>Ribes & Casals</h1>";
		document.getElementById('linea4').innerHTML="<img src='img/logoetiqueta.jpg' alt='Ribes & Casals Tienda 1933' vspace='6' align='right'><h2>RIBES & CASALS</h2>SELLING OF FABRICS WHOLESALE AND RETAIL<p><b>BARCELONE +34 93 301 23 45<br>MADRID +34 91 369 45 00<br>SEVILLE +34 95 499 74 20</b></p>";
		break;
		
		case "index.htm?fra":
		document.getElementById('linea3').innerHTML="<h1>Ribes & Casals</h1>";
		document.getElementById('linea4').innerHTML="<img src='img/logoetiqueta.jpg' alt='Ribes & Casals Tienda 1933' vspace='6' align='right'><h2>RIBES & CASALS</h2>VENTE DE TISSUS EN GROS ET AU D�TAIL<p><b>BARCELONE +34 93 301 23 45<br>MADRID +34 91 369 45 00<br>SEVILLA +34 95 499 74 20</b></p>";
		break;
		
		case "ribescasals.htm?ing":
		document.getElementById('linea3').innerHTML="<h1>About<br>&nbsp;&nbsp;&nbsp;&nbsp;Ribes & Casals</h1>";
		document.getElementById('linea4').innerHTML="<h2>Selling of fabrics wholesale and retail</h2><img src='img/acerca1.jpg' alt='Acerca de Ribes & Casals' align='right'>RIBES & CASALS is a company with a great experience that was founded in Barcelona in the year <b>1933</b> in a small establishment of the street Alta de Sant Pere. <p>In the year 1945, <b>RIBES & CASALS</b> moved to Pau Claris street, this new establishment became one of the biggest fabric stores in town. <p>Throughout the years, <b>RIBES & CASALS</b> has been adapting to all the new fashions and trends, offering a wide range of fabrics at very reasonable relation between price and quality, and an excellent service. <p><b>RIBES & CASALS</b>, is also trader and a distributor of fabrics. <p>Nowadays, <b>RIBES & CASALS</b> is composed by tree establishments: two of them in Barcelona and one in Madrid, in all of them there are a wide variety of fabrics dedicated to many different needs.";
		break;
		
		case "ribescasals.htm?fra":
		document.getElementById('linea3').innerHTML="<h1>Connaissez<br>&nbsp;&nbsp;&nbsp;&nbsp;Ribes & Casals</h1>";
		document.getElementById('linea4').innerHTML="<h2>Vente de tissus en gros et au d�tail</h2><img src='img/acerca1.jpg' alt='Acerca de Ribes & Casals' align='right'>Entreprise d'exp�rience fond�e � Barcelone en <b>1933</b> dans un petit �tablissement de la rue Alta de Sant Pere. <p>En 1945, <b>RIBES & CASALS</b> a transf�r� ses locaux dans la rue Pau Claris et est devenu l'un des principaux �tablissements de la ville sp�cialis�s dans la vente de tissus. <p>Au fil des ans, <b>RIBES & CASALS</b> s'est adapt� aux diff�rentes modes et tendances, offrant une gamme de tissus �tendue et vari�e, un excellent rapport qualit�-prix et un service client�le efficace. <p><b>RIBES & CASALS</b> est �galement importateur et distributeur de tissus de saison. <p>Aujourd'hui, <b>RIBES & CASALS</b> est compos� de quatre �tablissements : trois d'entre eux � Barcelone et un � Madrid, ils proposent tous une grande vari�t� de tissus destin�s � une grande vari�t� d'utilisations.";
		break;
		
		case "tejidos.htm?ing":
		document.getElementById('linea3').innerHTML="<h1>Our<br>&nbsp;&nbsp;&nbsp;&nbsp;Fabrics</h1>";
		document.getElementById('linea5').innerHTML="<a href='javascript:tejidos3(1)'>Wools, silks and fantasies</a> | <a href='javascript:tejidos3(2)'>Decoration</a> | <a href='javascript:tejidos3(3)'>Carnival and spectacles</a>";
		document.getElementById('linea4').innerHTML="<img src='img/tejidos0.jpg' alt='Nuestros tejidos' align='right'><h2>Select a kind of fabrics</h2>";
		break;
		
		case "tejidos.htm?fra":
		document.getElementById('linea3').innerHTML="<h1>Nos<br>&nbsp;&nbsp;&nbsp;&nbsp;tissus</h1>";
		document.getElementById('linea5').innerHTML="<a href='javascript:tejidos4(1)'>Laines, soies et fantaisies</a> | <a href='javascript:tejidos4(2)'>Maison</a> | <a href='javascript:tejidos4(3)'>Carnaval et spectacles</a>";
		document.getElementById('linea4').innerHTML="<img src='img/tejidos0.jpg' alt='Nuestros tejidos' align='right'><h2>S�lectionnez une cat�gorie de tissus</h2>";
		break;
		
		case "establecimientos.htm?ing":
		document.getElementById('linea3').innerHTML="<h1>Our<br>&nbsp;&nbsp;&nbsp;&nbsp;establishments</h1>";
		document.getElementById('linea4').innerHTML="<table cellspacing='4' cellpadding='4'><tr><td class='bloque1'><h3>PAU CLAR&Iacute;S, 79<br>(BARCELONA)<br>Tel. +34 93.301.23.45</h3><a href='javascript:mapa(1)'>See the map</a><p>Monday to Friday from 9:15 to 13:30 and 16:00 to 20:00<p>Saturdays from 9:15 to 14:00 and 16:30 to 20:00</td><td class='bloque1'><h3>ROGER DE LL&Uacute;RIA, 7<br>(BARCELONA)<br>Tel. +34 93.343.79.00</h3><a href='javascript:mapa(1)'>See the map</a><p>Monday to Saturday from 9:30 to 20:00 (OPENED ON MIDDAYS)</td><td class='bloque1'><h3>ATOCHA, 26<br>(MADRID)<br>Tel. +34 91.369.45.00</h3><a href='javascript:mapa(2)'>See the map</a><p>From Monday to Saturday from 9:30h to 14:00h and 16:30h to 20:00h</td><td class='bloque1'><h3>POL&Iacute;GONO LA NEGRILLA<br>(SEVILLA)<br>Tel. +34 95.499.74.20</h3><a href='javascript:mapa(3)'>See the map</a><p>Monday to Friday from 9:15 to 14:00 and 16:30 to 19:45</td></tr></table>";
		break;
		
		case "establecimientos.htm?fra":
		document.getElementById('linea3').innerHTML="<h1>Nos<br>&nbsp;&nbsp;&nbsp;&nbsp;�tablissements</h1>";
		document.getElementById('linea4').innerHTML="<table cellspacing='4' cellpadding='4'><tr><td class='bloque1'><h3>PAU CLAR&Iacute;S, 79<br>(BARCELONA)<br>Tel. +34 93.301.23.45</h3><a href='javascript:mapa(1)'>Voir des cartes</a><p>Du lundi au vendredi de 9.15 � 13.30 et 16.00 � 20.00 <p>Samedi de 9.15 � 14.00 et 16.30 � 20.00</td><td class='bloque1'><h3>ROGER DE LL&Uacute;RIA, 7<br>(BARCELONA)<br>Tel. +34 93.343.79.00</h3><a href='javascript:mapa(1)'>Voir des cartes</a><p>Du lundi au samedi de 9.30 � 20.00 (OUVERT SUR MIDDAYS)</td><td class='bloque1'><h3>ATOCHA, 26<br>(MADRID)<br>Tel. +34 91.369.45.00</h3><a href='javascript:mapa(2)'>Voir des cartes</a><p>De Lundi � Samedis de 9:30 � 14:00 et 16:30 � 20 h</td><td class='bloque1'><h3>POL&Iacute;GONO LA NEGRILLA<br>(SEVILLA)<br>Tel. +34 95.499.74.20</h3><a href='javascript:mapa(3)'>Voir des cartes</a><p>Du lundi au vendredi de 9.15 � 14.00 et 16.30 � 19.45</td></tr></table>";
		break;
	}
	cambiarimg();
}