<?php include("../cabecera.php"); ?>

<? switch($_SESSION["lang"]) {
	
	case "cat": ?>
	<h1>Concurs Internacional de Piano per Joves Promeses</h1>
	<div class="cuerpo">   
		<img src="http://www.eugeniaverdet.com/img/pianos.gif" class="piano" alt="Eugenia Verdet">
		<h2>Premi Eug&egrave;nia Verdet</h2>
		<p><b><em>1r. Premi</em> 1.500&euro;</b><br> i la promoci&oacute; de dos concerts p&uacute;blics <i>(patrocinat per Eug&egrave;nia Verdet)</i> i un concert amb piano i orquestra <i>(patrocinat per FIDELCOLOR)</i>.<br>Catalunya Musica oferi tamb&eacute; l'grabaci&oacute; d'un concert.</p>
		<p><b><em>2n. Premi</em> 1.000&euro;</b><br> <i>(patrocinat per ACADEMIA TASTAVINS SANT HUMBERT)</i> i la promoci&oacute; de dos concerts p&uacute;blics.</p>	
		<p><b><em>3r. Premi</em> 600&euro;</b><br> <i>(patrocinat per ROTARY CLUB VILAFRANCA DEL PENED&Eacute;S)</i> i la promoci&oacute; de dos concerts p&uacute;blics.</p>
		<p>&middot; Els premis podr&aacute;n declarse-se ex aequo o desert si el Jurat ho considera oport&uacute;.</p>
		<p>&middot; El Jurat estara format per pianistes de diferentes conservatoris i escoles de Catalunya.</p>
		<p>&middot; Les decicions del jurat seran inapel.lables.</p>
		<p>&middot; La inscripci&oacute; presuuposa l'aceptaci&oacute; de les presents bases.</p>	
	<? break;
				
	case "ing": ?>
	<h1>International Piano Contest for Young Talents</h1>
    <div class="cuerpo">
		<img src="http://www.eugeniaverdet.com/img/pianos.gif" class="piano" alt="Eugenia Verdet">
        <h2>Eug&egrave;nia Verdet Award</h2>
        <p><b><em>1st prize:</em> 1.500&euro;</b><br>and  the promotion of two public concerts sponsored by Eug&egrave;nia Verdet and a concert  for piano and orchestra sponsored by <i>FIDELCOLOR</i>. The classical radio station Catalunya M&uacute;sica  will record one concert.</p>
		<p><b><em>2nd prize:</em> 1.000&euro;</b><br>sponsored by <i>ACADEMIA TASTAVINS SANT HUMBERT</i> and the promotion of two public concerts. </p>
		<p><b><em>3rd prize:</em> 600&euro;</b><br>sponsored  by <i>ROTARY CLUB VILAFRANCA DEL PENED&Egrave;S</i> and the promotion of two public concerts.</p>
		<p>&middot; Any prizes may be declared ex aequo or not awarded if  the Jury considers so.</p>
		<p>&middot; The Jury will be composed by some piano players  from different conservatories and music schools from Catalunya. </p>
		<p>&middot; Jury decisions will not be open to appeal.</p>
		<p>&middot; Registration implies the acceptance of the present  rules.</p>
	<? break;
	
	case "ger": ?>
	<h1>Internationaler  Klavier-Wettbewerb f&uuml;r junge Talente</h1>
    <div class="cuerpo">
		<img src="http://www.eugeniaverdet.com/img/pianos.gif" class="piano" alt="Eugenia Verdet">
        <h2>Preis Eug&egrave;nia Verdet</h2>
        <p><b><em>Der 1. Preis:</em> betr&auml;gt 1.500&euro;</b> die von Eug&egrave;nia  Verdet beigetragen werden. Dazu f&ouml;rdern Eugenia Verdet und die Firma <i>Fidelcolor</i> einerseits respektive zwei  &ouml;ffentliche Konzerte und ein Konzert mit Klavier und Orchester. Andererseits  unterst&uuml;tzt <i>Catalunya M&uacute;sica </i>die  Aufnahme eines Konzertes.</p>
		<p><b><em>Der 2. Preis:</em> betr&auml;gt 1.000&euro;</b> die von der <i>Academia Tastavins Sant Humbert</i> beigetragen  werden. Dazu werden noch zwei &ouml;ffentlichen Konzerte unterst&uuml;tzt.</p>
		<p><b><em>Der 3. Preis:</em> betr&auml;gt 600&euro;</b> die vom <i>Rotary Club Vilafranca del Pened&egrave;s</i> beigetragen  werden. Dazu werden noch zwei &ouml;ffentlichen Konzerte unterst&uuml;tzt.</p>
		<p>&middot; Die Jury ist nicht  verpflichtet, Preise zu verleihen, die sie als nicht verdient betrachtet.  Preise m&uuml;ssen deshalb nicht zwingend vergeben werden.
		<br>&middot; Die Jury setzt sich  aus Pianisten verschiedener Konservatorien und Musikschulen Kataloniens Zusammen.
		<br>&middot; Gegen die  Entscheidungen der Jury kann keine Berufung eingelegt werden.
		<br>&middot; Durch die Anmeldung  werden die Wettbewerbsbedingungen akzeptiert.</p>
	<? break;
	
	case "fra": ?>
	<h1>Concours International de Piano pour des Jeunes Espoirs</h1>
    <div class="cuerpo">   
		<img src="http://www.eugeniaverdet.com/img/pianos.gif" class="piano" alt="Eugenia Verdet">
        <h2>Prix Eug&egrave;nia Verdet</h2>
        <p><b><em>1er Prix&nbsp;:</em> 1.500&euro;</b><br>et le financement de deux concerts publics <i>(sponsoris&eacute;  par Eug&egrave;nia Verdet)</i>, ainsi qu&rsquo;un concert accompagn&eacute; du piano et de l&rsquo;orchestre <i>(sponsoris&eacute; par FIDELCOLOR) Catalunya M&uacute;sica</i> offre aussi la  possibilit&eacute; d&rsquo;enregistrer un concert.</p>
 		<p><b><em>2&egrave;me Prix&nbsp;: </em> 1.000&euro;</b><br> <i>(sponsoris&eacute; par ACADEMIA TASTAVINS SANT HUMBERT)</i> et le financement de deux concerts publics.</p>
  		<p><b><em>3&egrave;me Prix&nbsp;: </em> 600&euro;</b><br> <i>(sponsoris&eacute; par ROTARY CLUB VILAFRANCA DEL  PENED&Eacute;S)</i> et le financement de deux concerts publics.</p>
  		<p>&middot; Les prix  pourront &ecirc;tre d&eacute;clar&eacute;s <i>ex aequo </i>ou vacants  si le Jur&eacute; le consid&egrave;re opportun.</p>
 		<p>&middot; Le Jur&eacute; se  composera de pianistes en provenance de diff&eacute;rents conservatoires et &eacute;coles de  Catalogne.</p>
  		<p>&middot; Les d&eacute;cisions  du jur&eacute; n&rsquo;auront pas d&rsquo;appel.</p>
		<p>&middot; L&rsquo;inscription  implique l&rsquo;acceptation de ces crit&egrave;res.</p>
	<? break;
		
	default: ?>
	<h1>Concurso Internacional de Piano para J&oacute;venes Promesas</h1>
	<div class="cuerpo">   
		<img src="http://www.eugeniaverdet.com/img/pianos.gif" class="piano" alt="Eugenia Verdet">	
		<h2>Premio Eugenia Verdet</h2>
		<p><b><em>1r. Premio</em> 1.500&euro;</b><br> y la promoci&oacute;n de dos conciertos p&uacute;blicos <i>(patrocinados por Eugenia Verdet)</i> y un concierto con piano y orquesta <i>(patrocinado por FIDELCOLOR)</i>.<br>Catalunya Musica ofrece tambi&eacute;n la grabaci&oacute;n de un cocierto.</p>
		<p><b><em>2n. Premio</em> 1.000&euro;</b><br> <i>(patrocinado por ACADEMIA TASTAVINS SANT HUMBERT)</i> y la promoci&oacute;n de dos conciertos p&uacute;blicos.</p>
		<p><b><em>3r. Premio</em> 600&euro;</b><br> <i>(patrocinado por ROTARY CLUB VILAFRANCA DEL PENED&Eacute;S)</i> y la promoci&oacute;n de dos conciertos p&uacute;blicos.</p>
		<p>&middot; Los premios podran declararse desiertos si el Jurado lo considera oportuno.</p>
		<p>&middot; El Jurado estara formado por pianistas de diferentes conservatorios y escuelas de Catalu&ntilde;a.</p>
		<p>&middot; Las decisiones del jurado ser&aacute;n inapelables.</p>
		<p>&middot; La inscripci&oacute;n presupone la aceptaci&oacute;n de las presentes bases.</p>                
	<? break; } ?>

    </div>
   
<?php include("../piecera.php"); ?>