<?php include("../cabecera.php"); ?>

    <h1>Contacto / Contacte / Contact / Contact us / Kontakt</h1>
 	<div class="cuerpo">   
    	<img src="../img/pianos.gif" class="piano" alt="Eugenia Verdet Piano" title="Eugenia Verdet Piano">
        <?php if($_POST["email"])
		{
			foreach($_POST as $nombre_campo => $valor)
 				$cuerpo=$cuerpo."<b>".$nombre_campo ."</b> : ".$valor."<br>";

			if(mail("info@eugeniaverdet.com","Comentario de la pagina web",$cuerpo,"Content-type:text/html\nBcc:omar@indianwebs.com"))
				echo '<p>Su comentario se ha enviado. Gracias por su colaboraci&oacute;n.</p><p>His comment has been sent. Thank you for your cooperation.</p>';
				
		} else { ?>	
        
        	<? switch($_SESSION["lang"]) {
	
			case "cat": ?>
            <p>Per favor empleni el seg&uuml;ent formulari i li donarem una resposta com m&eacute;s aviat millor.</p>
            <p>Tamb&eacute; pot contactar amb nosaltres per tel&egrave;fon en el <b>(+34) 93 200 05 17</b></p>
			<? break;
						
			case "ing": ?>
            <p>Please,  fill the following registration form. We&rsquo;ll contact you as soon as possible.<br>
  			<p>You can also contact us by telephone <b>(+34) 93 200 05 17.</b></p>
			<? break;
			
			case "ger": ?>
            <p>F&uuml;llen Sie bitte folgendes  Formular aus und sie werden so schnell wie m&ouml;glich unsere Antwort bekommen.</p>
			<p>Sie k&ouml;nnen uns auch  telefonisch erreichen unter der Nummer <b>(+34)  93 200 05 17.</b></p>
			<? break;
			
			case "fra": ?>
            <p>Afin de nous contacter,  remplissez le formulaire suivant. Nous vous r&eacute;pondrons le plus rapidement  possible.</p>
			<p>Vous pouvez  aussi nous contacter par t&eacute;l&eacute;phone&nbsp;: <b>(+34) 93 200 05 17</b></p>
			<? break;
				
			default: ?>
            <p>Por favor rellene el siguiente formulario y le daremos una respuesta lo antes posible.</p>
            <p>Tambi&eacute;n puede contactar con nosotros por tel&eacute;fono en el <b>(+34) 93 200 05 17</b></p>
			<? break; } ?>
            
 	        <form name="form1" action="contacto.php" method="post">
    	    Nombre/Name/Pr&eacute;nom:			
            <input type="text" name="nombre" size="42" maxlength="40">
        	Nationality/Nationalit&eacute;/Staatsangehörigkeit:	
            <input type="text" name="pais" size="42" maxlength="40">
	        E-Mail:			
            <input type="text" name="email" size="42" maxlength="40">
    	    Tel./Tel. number/T&eacute;l&eacute;phone/Telefonnummer:
            <input type="text" name="telefono" size="42" maxlength="40">
        	Comentario/Comment/Commentair/Anmerkungen:	
            <textarea name="comentario" cols="43" rows="6"></textarea>
 	        <p align="right"><input type="submit" value=" ENVIAR / SUBMIT / ENVOYER / SENDEN "></p>
    	    </form>
        <? } ?>
    </div>
    
<?php include("../piecera.php"); ?>