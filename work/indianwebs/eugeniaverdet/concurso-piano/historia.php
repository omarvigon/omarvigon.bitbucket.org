<?php include("../cabecera.php"); ?>

    <h1>Eug&egrave;nia Verdet</h1>
 	<div class="cuerpo_hi"> 	
    	
        <? switch($_SESSION["lang"]) {
	
			case "cat": ?>
            <h2>Biograf&iacute;a</h2>
            <p>Neix  a Barcelona.</p>
			<p>Comen&ccedil;a  els estudis de piano a l&rsquo;edat de sis anys a la prestigiosa Acad&egrave;mia Marshall,  sota la direcci&oacute; de la seva tia, la gran pianista Josefina Verdet, destacada  deixeble del mestre Frank Marshall, successor del reconegut compositor Enric  Granados.</p>
			<p>Promptament  demostra unes grans aptituds i sensibilitats musicals que la porten a la  graduaci&oacute; als 14 anys.</p>
			<p>Durant  la seva joventut va donar diversos concerts de piano, dels quals cal destacar  l&rsquo;organitzat per l&rsquo;Associaci&oacute; Beethoven de Joventuts Musicals en el col&middot;legi  Sant Jordi l&rsquo;any 1957.</p>
			<p>El compositor  i cr&iacute;tic Xavier Monsalvatge va definir la seva personalitat art&iacute;stica, en el  setmanari Destino, afirmant que &ldquo;<i>Eug&egrave;nia  Verdet estudia amb entusiasme i la seva dicci&oacute; reflexa un temperament que  escapa del corrent. La seva manera de tocar es vehement i expressiva</i>&rdquo; </p>
			<p>El 1961 deixa la seva carrera musical, contrau matrimoni i es dedica a la seva  fam&iacute;lia, per&ograve; continua vinculada a la m&uacute;sica impartint classes de piano, principalment  a gent jove.</p>
			<p>El 2002 crea el premi Eug&egrave;nia Verdet per a joves int&egrave;rprets de piano, amb la  finalitat de promoure i donar a con&egrave;ixer a joves promeses de la m&uacute;sica. La seva  il&middot;lusi&oacute; es aquesta iniciativa es consolidi en el temps i arribi a assolir un  prestigi i ress&ograve; internacionals.</p>
			<p>El premi est&agrave; atorgat per un prestigi&oacute;s jurat i gaudeix d&rsquo;una alta participaci&oacute;  d&rsquo;un gran nivell musical, on han destacat entre altres: Sonia Rom&aacute;n, Dina  Ned&eacute;ltcheva, Maria del Hoyo, Christophe Berruex, Sara Vignolo, ...</p>
			<p>El concurs s&rsquo;organitza a Vilob&iacute; del Pened&egrave;s, municipi molt vinculat a la fam&iacute;lia, al qual cal agrair la seva participaci&oacute; en un acte cultural d&rsquo;aquesta  naturalesa.</p>
			<? break;
						
			case "ing": ?>
            <h2>Biography</h2>
            <p>Eug&egrave;nia Verdet was  born in Barcelona.  She began her piano studies when she was six years old at the prestigious Marshall Academy. The Academy was ruled by the  great pianist Josefina Verdet, Eug&egrave;nia&rsquo;s aunt. Josefina was an outstanding  disciple of the renowned Frank Marshall, successor of the well-known composer  Enric Granad&oacute;s.</p>
            <p>Shortly after, she  showed great musical skills and got her graduation at the early age of  fourteen. All along her youth, she performed several piano contests. One of the  most important competitions was organized by the Associaci&oacute; Beethoven de  Joventuts Musicals at the Sant   Jordi School  in 1957.</p>
            <p>The composer and  music critic Xavier Monsalvatge talked about her artistic personality in the  Destino weekly magazine: &ldquo;Eug&egrave;nia Verdet is a very enthusiastic student and her  diction shows an unusual artistic temperament. Her performances are vehement  and very expressive.&rdquo;</p>
            <p>In 1961 Eug&egrave;nia  Verdet gave up her musical career, she got married and devoted herself to the  care of her family. However, Eug&egrave;nia never abandoned the world of the music, as  she taught young people how to play piano.</p>
            <p>In 2002 she created  the Eug&egrave;nia Verdet Award for young piano players, with the aim to promote and  make known young promising talents. Her dream was to have this initiative  consolidated and to achieve good reputation and international success.</p>
            <p>The Award is given by a very reputed jury and a good number of high-qualified musicians take part in it. Sonia Rom&aacute;n, Dina Ned&eacute;ltcheva, Maria del Hoyo, Christophe Berreux and Sara Vignolo have been outstanding contestants in previous editions.</p>
            <p>The contest is held  in Vilob&iacute; del Pened&egrave;s, a special town for the family. We express our gratitude  to Vilob&iacute; for participating in such a cultural event.</p>
			<? break;
			
			case "ger": ?>
            <h2>Biografie</h2>
			<p>Die in Barcelona  geborene Pianistin Eug&egrave;nia Verdet begann im Alter von sechs Jahren Klavier zu  spielen. Sie besuchte die angesehenen <i>Academia  Marshall</i> in Barcelona, wo sie bei ihrer Tante, die bedeutende  Klavierspielerin Josefina Verdet, lernte. Josefina Verdet war eine  herausragende Sch&uuml;lerin des Maestros Frank Marshall, Nachfolger des bekannten  Komponisten Enric Granados.</p>
			<p>Von Anfang an  zeigte sich, dass sie grosses Talent f&uuml;r Klavier und musikalische Begabung hatte,  die ihr es erm&ouml;glichten, das Studium mit 14 Jahren zum Abschluss zu bringen.</p>
			<p>In ihrer Jugendzeit  gab sie einige Konzerte. Hervorzuheben ist das Konzert, das von der Vereinigung <i>Asociaci&oacute;n Beethoven</i> von <i>Juventudes Musicales </i>1957 an der Schule <i>Sant Jordi</i> organisiert wurde. </p>
			<p>Xavier  Montsalvatge, Kritiker und Komponist, schrieb &uuml;ber ihre k&uuml;nstlerische  Pers&ouml;nlichkeit in der Wochenzeitung <i>Destino</i> und behauptete, dass &bdquo;Eug&egrave;nia Verdet mit Begeisterung Klavier spielen lerne,  ihr Stil eine ganz besondere Pr&auml;gung habe und ihre ungew&ouml;hnliche Spielweise  ausdrucksvoll sei&ldquo;.</p>
			<p>1961 heiratete sie und  gab ihre musikalische Karriere auf, um sich ihrer Familie zu widmen. Trotzdem  verlor sie nicht den Kontakt zu der Musikwelt, indem sie Klavierunterricht  insbesondere Jugendlichen erteilte.</p>
			<p>2002 schuf sie den  Eug&egrave;nia Verdet Preis f&uuml;r junge Pianisten mit dem Zweck, junge Talente zu  f&ouml;rdern und bekannt zu geben. Sie w&uuml;rde sich freuen, wenn diese Initiative sich  mit der Zeit festigen k&ouml;nnte und der Preis Weltruf bek&auml;me.</p>
			<p>An diesem Preis, der  von einer angesehenen Jury verliehen wird, nehmen zahlreiche hochbegabte junge  Musiker teil, von denen einige wie zum Beispiel Sonia Permanece, Dina  Ned&eacute;ltcheva, Maria del Hoyo, Cripstophe Berruex und Sara Vignolo zu erw&auml;hnen  sind.</p>
			<p>Der eng mit der  Familie verbundenen Ortschaft von Vilob&iacute; del Pened&egrave;s, wo der Wettbewerb organisiert  wird, zeigen wir uns sehr dankbar f&uuml;r ihre Teilnahme an einem solchen  kulturellen Ereignis dieser Art. </p>
			<? break;
			
			case "fra": ?>
            <h2>Biographie</h2>
            <p>Eugenia Verdet  est n&eacute;e &agrave; Barcelone.</p>
            <p>Elle a commenc&eacute;  ses &eacute;tudes de piano quand elle avait six ans. Elle s&rsquo;est inscrite dans la  prestigieuse Acad&eacute;mie Marshall, sous la direction de sa tante, la grande  pianiste Josefina Verdet, qui &eacute;tait une des plus importantes disciples du  ma&icirc;tre Frank Marshall &ndash;successeur du c&eacute;l&egrave;bre compositeur Enric Granados.</p>
            <p>Eugenia a tout-de-suite  d&eacute;montr&eacute; de grandes aptitudes et une grande sensibilit&eacute; musicale, lui  permettant ainsi d&rsquo;obtenir son dipl&ocirc;me &agrave; l&rsquo;&acirc;ge de 14 ans.</p>
            <p>Dans le courant  de sa jeunesse elle a donn&eacute; plusieurs concerts de piano, donc ressort le  concert organis&eacute; par l&rsquo;Association des Jeunesses Musicales &agrave; l&rsquo;&eacute;cole Sant Jordi  en 1957.</p>
            <p>Le compositeur  et critique Xavier Monsalvatge a d&eacute;fini, dans l&rsquo;hebdomadaire Destino, la  personnalit&eacute; artistique de la pianiste avec les mots suivants&nbsp;: <i>&laquo;&nbsp;Eug&egrave;nia Verdet &eacute;tudie avec enthousiasme  et sa diction est le reflet d&rsquo;un temp&eacute;rament inhabituel. Sa mani&egrave;re de jouer  est v&eacute;h&eacute;mente et expressive&nbsp;&raquo;</i>.</p>
            <p>En 1961, Verget  a quitt&eacute; ses &eacute;tudes musicales, s&rsquo;est mari&eacute;e et s&rsquo;est consacr&eacute;e &agrave; sa famille.  Pourtant, elle a continu&eacute; a &ecirc;tre li&eacute;e au monde de la musique en donnant des  cours de piano, surtout aux jeunes.&nbsp; </p>
            <p>En 2002, elle a  cr&eacute;&eacute; le prix Eug&egrave;nia Verdet pour des jeunes interpr&egrave;tes de piano, avec le  double objectif de favoriser et de faire conna&icirc;tre des jeunes espoirs de la  musique. Son illusion est que cette initiative se consolide dans le temps et  obtienne un prestige et une reconnaissance internationale.</p>
            <p>Le prix est  attribu&eacute; par un prestigieuse jur&eacute; et dispose d&rsquo;une participation &eacute;lev&eacute;e avec un  niveau musical tr&egrave;s haut. On peut remarquer, entre autres, les noms  suivants&nbsp;: Sonia Rom&aacute;n, Dina Ned&eacute;ltcheva, Maria del Hoyo, Christophe Berreux, Sara Vignolo...</p>
            <p>Le concours a  lieu &agrave; Vilob&iacute; del Pened&egrave;s, une localit&eacute; qui est tr&egrave;s li&eacute;e &agrave; la famille Verget.  Par cons&eacute;quence, nous tenons &agrave; remercier ce village pour sa participation &agrave; un  acte culturel de ce type.</p>
			<? break;
				
			default: ?>
            <h2>Biograf&iacute;a</h2>
			<p>Eug&egrave;nia Verdet nace en Barcelona.</p>
			<p>Empieza los estudios de piano a la edad de seis a&ntilde;os en  la prestigiosa Academia Marshall, bajo la direcci&oacute;n de su t&iacute;a, la gran pianista  Josefina Verdet, destacada disc&iacute;pula del maestro Frank Marshall, sucesor del  reconocido compositor Enric Granados. </p>
			<p>Desde sus comienzos, demuestra unas grandes aptitudes y  sensibilidades musicales que permiten su graduaci&oacute;n a los 14 a&ntilde;os. </p>
			<p>Durante su juventud dio varios conciertos de piano, de  los cuales hace falta destacar el organizado por la Asociaci&oacute;n Beethoven  de Juventudes Musicales en el Colegio Sant Jordi en el a&ntilde;o 1957.</p>
			<p>El compositor y cr&iacute;tico Xavier Monsalvatge defini&oacute; su  personalidad art&iacute;stica, en el semanario Destino, afirmando que &ldquo;Eug&egrave;nia Verdet  estudia con entusiasmo y su dicci&oacute;n refleja un temperamento que escapa de la  corriente. Su manera de tocar es vehemente y expresiva&rdquo; </p>
			<p>En 1961 deja su carrera musical, contrae matrimonio y  se dedica a su familia, pero contin&uacute;a vinculada a la m&uacute;sica impartiendo clases  de piano, principalmente a gente joven. </p>
			<p>El 2002 crea el premio Eug&egrave;nia Verdet para j&oacute;venes  int&eacute;rpretes de piano, con el fin de promover y dar a conocer a j&oacute;venes promesas  de la m&uacute;sica. Su ilusi&oacute;n es que esta iniciativa se consolide en el tiempo y  llegue a lograr un prestigio y eco internacional. </p>
			<p>El premio est&aacute; otorgado por un prestigioso jurado y  disfruta de una alta participaci&oacute;n de un gran nivel musical, donde han  destacado entre otras: Sonia Permanece, Dina Ned&eacute;ltcheva, Maria del Hoyo,  Christophe Berruex, Sara Vignolo... </p>
			<p>El concurso se organiza en Vilob&iacute; del Pened&egrave;s,  municipio muy vinculado a la familia,&nbsp; y  al que hay que agradecerle su participaci&oacute;n en un acto cultural de esta  naturaleza. </p>
			<? break; } ?>
    </div>
    
<?php include("../piecera.php"); ?>