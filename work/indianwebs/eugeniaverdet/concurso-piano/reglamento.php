<?php include("../cabecera.php"); ?>

<? switch($_SESSION["lang"]) {
	
	case "cat": ?>
	<h1>Concurs Internacional de Piano per Joves Promeses</h1>
	<div class="cuerpo">
		<img src="../img/pianos.gif" class="piano">	
        <h2>Bases del Concurs</h2>
		<p><b>Edat :</b> fins a 28 anys<br>El concurs constar&egrave; de duess fases, les dues sera;n p&uacute;bliques.</p>
        <p><em><b>Primera Fase :</b></em><br>&middot; Es celebrar&aacute; a <i>l'Institut d'Estudis Nortamericans</i> de Barcelona, el dia 8 de Maig del 2009 de 10:00h a 14:00h y de 16:00h a 19:00h.<br>&middot; S'interpretar&aacute;n obres de lliure elecci&oacute;, d'estils diferents (cl&agrave;sico,rom&agrave;ntico, i segle XX) i d'un temp total de 15 minuts.<br>&middot; El jurat seleccionar&agrave; els pianistes que passaran a la fase final.</p>
        <p><em><b>Fase Final :</b></em><br>&middot; Es celebrar&agrave;; a <i>l'Auditori Jaume Via</i> de Vilob&iacute; del Pened&eacute;s, el dia 10 de Maig del 2009, de 10:00h a 14:00h.<br>&middot; Els participants de la primera fase caldr&agrave; que interpretin altres obres, d'una durada de 30 minuts.<br>&middot; Els concursants participaran per ordre alfab&egrave;tic.<br>&middot; La inscripci&oacute; pressuposa l'aceptaci&oacute; de les presents bases.</p>
	<? break;
				
	case "ing": ?>
	<h1>International Piano Contest for Young Talents</h1>
	<div class="cuerpo">
		<img src="../img/pianos.gif" class="piano">	
        <h2>Rules of the  Competition</h2>
        <p><b>Age :</b> until 28. <br>The  contest consists of two public stages.</p>
		<p><em><b>First stage : </b></em></p><ul><li>This stage will take place in the <i>Instituto de Estudios Norteamericanos</i> of Barcelona the 8th of May 2009 from 10:00 until 14:00 and from 16:00 until 19:00. </li><li>Each  candidate shall perform a 15 minute free choice piece of music.</li><li>The jury  will select the piano players for the final stage.</li></ul></p>
		<p><em><b>Final stage : </b></em></p><ul><li>This stage will take place in the <i>Jaume Via</i> de Vilob&iacute; del Pened&egrave;s Auditorium the 10th of May 2009 from 10:00 until 14:00.</li><li>Contestants shall perform piano pieces different  from the first stage. 30 minutes will be the maximum time for each contestant  and performance.</li><li>Contestants will perform in alphabetical  order.</li><li>Registration implies the acceptance of the present  rules.</li></ul></p>
	<? break;
	
	case "ger": ?>
	<h1>Internationaler  Klavier-Wettbewerb f&uuml;r junge Talente</h1>
	<div class="cuerpo">
		<img src="../img/pianos.gif" class="piano">	
        <h2>Wettbewerbsbedingungen</h2>
        <p>Alter bis 28 Jahren <br>Der Wettbewerb  besteht aus zwei &ouml;ffentlichen Durchg&auml;ngen.</p>
		<p><em><b>Erster Durchgang : </b></em></p><p>&middot; Der erste  Durchgang wird im <i>Instituto de Estudios Norteamericanos</i> in Barcelona am 8.Mai 2009 von 10:00 Uhr bis 14:00 Uhr und  von 16:00 Uhr bis 19:00 Uhr&nbsp; stattfinden.<br>&middot; Dabei werden die  Teilnehmer frei ausgew&auml;hlte Werke in verschiedenen Stilen (klassisch, Barock  und zeitgen&ouml;ssisch) in insgesamt 15 Minuten vortragen m&uuml;ssen.<br>&middot; Die Jury wird die  Kandidaten ausw&auml;hlen, die in die engere Wahl kommen.</p>
		<p><em><b>Letzter Durchgang :</b></em></p><p>&middot; Der letzte  Durchgang wird im Konzertsaal <i>Auditorio  Jaime Via</i> von Vilob&iacute; del Pened&egrave;s am 10. Mai 2009, von 10:00 Uhr bis 14:00  Uhr, stattfinden.<br>&middot; Die ausgew&auml;hlten  Konkurrenten werden neue St&uuml;cke f&uuml;r eine Dauer von 30 Minuten interpretieren.<br>&middot; Man wird  alphabetisch geordnet spielen.<br>&middot; Bei der Anmeldung  werden die Wettbewerbsbedingungen akzeptiert.</p>
	<? break;
	
	case "fra": ?>
	<h1>Concours International de Piano pour des Jeunes Espoirs</h1>
	<div class="cuerpo">   
		<img src="../img/pianos.gif" class="piano">	
        <h2>Crit&egrave;res du Concours</h2>
        <p><b>&Acirc;ge :</b> jusqu&rsquo;&agrave; 28 ans<br>Le concours se composera de deux phases et les deux seront publiques.</p>
		<p><em><b>Premi&egrave;re Phase : </b></em><br><br>&middot; Aura lieu au <i>Instituto de Estudios Norteamericanos</i> de Barcelona, le 8 Mai 2009 de 10:00h  &agrave; 14:00h et de 16:00h &agrave; 19:00h.<br>&middot; Les candidats pourront choisir leurs morceux et le style de celles-ci  (classique, romantique et du si&egrave;cle XX), &agrave; condition qu&rsquo;elles aient une dur&eacute;e  totale de 15 minutes. <br>&middot; Le jur&eacute; s&eacute;lectionnera les pianistes qui passeront &agrave; la phase finale.</p>
		<p><em><b>Phase Finale : </b></em><br><br>&middot; Aura lieu &agrave; <i>l&rsquo;Auditorium Jaume Via</i> de Vilob&iacute; del Pened&egrave;s, le 10 Mai 2009, de  10:00h &agrave; 14:00h.<br>&middot; Les participants s&eacute;lectoinn&eacute;s lors de la premi&egrave;re phase devront interpr&eacute;ter  un nouveau morceau d&rsquo;une dur&eacute;e de 30 minutes.<br>&middot; Les participants interpr&eacute;teront leurs morceaux par ordre alphab&eacute;tique.<br>&middot; L&rsquo;inscription implique l&rsquo;acceptation de ces crit&egrave;res.</p>
	<? break;
		
	default: ?>
	<h1>Concurso Internacional de Piano para J&oacute;venes Promesas</h1>
	<div class="cuerpo">   
		<img src="../img/pianos.gif" class="piano">
        <h2>Bases del Concurso</h2>
		<p><b>Edad :</b> hasta 28 a&ntilde;os<br>El concurso constar&aacute; de dos fases, ambas ser&aacute;n p&uacute;blicas.</p>
        <p><em><b>Primera Fase :</b></em><br>&middot; Se celebrar&aacute; en el <i>Instituto de Estudios Norteamericanos</i> de Barcelona, el dia 8 de Mayo del 2009 de 10:00h a 14:00h y de 16:00h a 19:00h.<br>&middot; Se interpretar&aacute;n obras de libre elecci&oacute;n, de estilos diferentes (cl&aacute;sico,rom&aacute;ntico, y siglo XX) y de un tiempo total de 15 minutos.<br>&middot; El jurado seleccionar&aacute; los pianistas que pasar&aacute;n a la fase final.</p>
        <p><em><b>Fase Final :</b></em><br>&middot; Se celebrar&aacute; en el <i>Auditorio Jaume Via</i> de Vilob&iacute; del Pened&eacute;s, el dia 10 de Mayo del 2009, de 10:00h a 14:00h.<br>&middot; Los participantes de la primera fase  tendr&aacute;n que interpretar&aacute;n otras obras, de una duraci&oacute;n de 30 minutos.<br>&middot; Los concursantes participar&aacute;n por orden alfab&eacute;tico.<br>&middot; La inscripci&oacute;n presupondr&aacute; la aceptaci&oacute;n de las presentes bases.</p>	
	<? break; } ?>

    </div>
   
<?php include("../piecera.php"); ?>