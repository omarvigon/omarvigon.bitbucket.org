<?php include("../cabecera.php"); ?>

<? switch($_SESSION["lang"]) {
	
	case "cat": ?>
	<h1>Concurs Internacional de Piano per Joves Promeses</h1>
	<div class="cuerpo_hi">
       	<h2>Jurat</h2>
        <p><b><em>President d&rsquo;Honor : </em>Sr.  Jordi Roch i Bosch</b><br>President  de les Joventuts Musicals d&rsquo;Espanya<br>&middot;President d&rsquo;Honor de Jeunesses Musicals  International<br>&middot;Membre individual del International Music Council de l&rsquo;UNESCO.</p>
        <p><b><em>President : </em>Sr.  Angel Soler</b><br>Concertista  i director de l&rsquo;Estudi de M&uacute;sica que porta el seu nom, especialitzat en el  ensenyament del piano, i &eacute;s professor de m&uacute;sica de cambra al Conservatori del  Liceu</p>
        <h2>Vocals</h2>
        <p><b>Sra. Ana M&ordf; Cardona</b><br>Concertista  i Catedr&agrave;tica de piano del Conservatori Superior de Musica del Liceu</p>
        <p><b>Sra. Montserrat Gasset</b><br>Pianista  diplomada per l&rsquo;Acad&egrave;mia Marshall</p>
        <p><b>Sr. Cecilio Tieles</b><br>Concertista  i Catedr&agrave;tic del Conservatori Superior del Liceu i del Professional de Vilaseca</p>
        <p><b>Sr. Gennady Dzubenko</b><br>Concertista,  ha format part entre d&rsquo;altres del The Romantic Trio (Moscou) i del Trio  Mediterrani (Barcelona). Professor de l&rsquo;escola de musica Luthier de Barcelona,  del curs internacional Issac Albeniz de Camprodon i deg&agrave; de la facultat de  piano del Conservatori Shnittke de Moscou</p>
        <p><b>Sra. Eug&egrave;nia Verdet</b><br>Pianista  i fundadora del Premi</p> 
	<? break;
				
	case "ing": ?>
	<h1>International Piano Contest for Young Talents</h1>
	<div class="cuerpo_hi">
      	<h2>Jury</h2>
        <p><b><em>President  by Honours</em>:  Mr. Jordi Roch Bosch</b><br>&middot;President  of the Joventuts Musicals d&rsquo;Espanya.<br>&middot;President  by honours of the Jeunesses Musicals International.<br>&middot;Member  of the UNESCO Music Council.</p>
		<p><b><em>President: </em>Mr. Angel Soler</b><br>&middot;Mr. Soler is soloist and director of the  Angel Soler Estudi de M&uacute;sica. Mr. Soler is major in piano pedagogy and teaches at  the Conservatori del Liceu Chamber.</p>
        <h2>Members</h2>
        <p><b>Mrs. Ana M&ordf; Cardona</b><br>&middot;Soloist and piano profesor of the Conservatori Superior de M&uacute;sica del Liceu.</p>
		<p><b>Mrs. Montserrat Gasset</b><br>&middot;Piano player qualified by the Marshall   Academy.</p>
		<p><b>Mr. Cecilio Tieles</b><br>&middot;Soloist and profesor of the Conservatori Superior de M&uacute;sica del Liceu and the Conservatori Professional  de Vilaseca.</p>
		<p><b>Mrs. Gennady Dzubenko</b><br>&middot;Soloist. Mrs. Dzubenko has been member of <i>The  Romantic Trio</i> (Moscow) and member of the <i>Trio Mediterrani </i>(Barcelona). Teacher of the Luthier School  from Barcelona.  Teacher of the Issac Alb&eacute;niz de Camprod&oacute;n International Course. Chancellor of  the Piano Shnittke Conservatoire (Moscow).</p>
		<p><b>Mrs. Eug&egrave;nia Verdet</b><br>&middot;Piano player and Eug&egrave;nia Verdet  Contest founder.</p>
	<? break;
	
	case "ger": ?>
	<h1>Internationaler  Klavier-Wettbewerb f&uuml;r junge Talente</h1>
	<div class="cuerpo_hi">
      	<h2>Die Jury</h2>
        <p><b><em>Ehrenpr&auml;sident</em>:  Mr. Jordi Roch Bosch</b><br>&middot;Pr&auml;sident von <i>Juventudes Musicales de Espa&ntilde;a.</i><br>&middot;Ehrenpr&auml;sident von <i>Jeneusses Musicals International</i>.<br>&middot;Mitglied des <i>International Musica Council</i> der UNESCO.</p>
		<p><b><em>Pr&auml;sident: </em>Mr. Angel Soler</b><br>&middot;Konzertspieler und  Leiter der Schule f&uuml;r Musik, die nach ihm benannt ist. Dozent im 4. Jahr am <i>Conservatorio del Liceo</i> in Barcelona.</p>
        <h2>Mitglieder</h2>
        <p><b>Mrs. Ana M&ordf; Cardona</b><br>&middot;Konzertpianistin und Professorin am <i>Conservatorio  Superior de M&uacute;sica del Liceo</i> in Barcelona.</p>
		<p><b>Mrs. Montserrat Gasset</b><br>&middot;Diplompr&uuml;fung f&uuml;r  Klavier an der <i>Academia Marshall</i> in Barcelona.</p>
		<p><b>Mr. Cecilio Tieles</b><br>&middot;Konzertpianist und Professor am <i>Conservatorio  Superior de M&uacute;sica del Liceo</i> in Barcelona und am <i>Conservatorio Profesional de Vila-seca</i>.</p>
		<p><b>Mrs. Gennady Dzubenko</b><br>&middot;Konzertspieler,  Angeh&ouml;riger verschiedener Gruppen, wie zum Beispiel des Trios <i>The Romantic</i> aus Moskau und des <i>Tr&iacute;o Mediterr&aacute;neo</i>.<br>Musiklehrer an der  Schule Luthier in Barcelona und an dem Internationalen Kurs Isaac Albeniz in  Camprodon. <br> Dekan an der  Fakult&auml;t f&uuml;r Klavierlehre des Konservatoriums Schnittke aus Moskau..</p>
		<p><b>Mrs. Eug&egrave;nia Verdet</b><br>&middot;Pianistin und  Gr&uuml;nderin des Preises.</p>
	<? break;
	
	case "fra": ?>
	<h1>Concours International de Piano pour des Jeunes Espoirs</h1>
	<div class="cuerpo_hi"> 
    	<h2>Jur&eacute;</h2>
        <p><b><em>Pr&eacute;sident d&rsquo;Honneur : </em>M. Jordi Roch i Bosch</b><br>&middot;Pr&eacute;sident des <i>Joventuts Musicals  d&rsquo;Espanya</i><br>&middot;Pr&eacute;sident d&rsquo;Honneur de Jeunesses Musicales International<br>&middot;Membre  individuel du International Music Council de l&rsquo;UNESCO</p>
		<p><b><em>Pr&eacute;sident : </em>M. Angel Soler</b><br>&middot;Concertiste et directeur du Studio de Musique du m&ecirc;me nom, sp&eacute;cialis&eacute; dans l&rsquo;enseignement  du piano, et professeur de musique de chambre au Conservatoire du Liceu de  Barcelone</p>
        <h2>Membres</h2>
        <p><b>Ana M&ordf; Cardona</b><br>&middot;Concertiste et professeur de piano au Conservatoire Sup&eacute;rieur de Musique du  Liceu de Barcelone.</p>
		<p><b>Mm. Montserrat  Gasset</b><br>&middot;Pianiste ma&icirc;tris&eacute;e par l&rsquo;Acad&eacute;mie Marshall.</p>
		<p><b>M. Cecilio  Tieles</b><br>&middot;Concertiste et professeur au Conservatoire Sup&eacute;rieur de Musique du Liceu de  Barcelone et au Conservatoire Professionnel de Vilaseca (Tarragona).</p>
		<p><b>M. Gennady  Dzubenko</b><br>&middot;Concertiste et membre, entre autres, du The Romantic Trio (Moscou) et du Trio  Mediterrani (Barcelone). Professeur &agrave; l&rsquo;&eacute;cole de musique Luthier de Barcelone,  du cours international Issac Albeniz de Camprodon (Catalogne) et doyen de la  facult&eacute; de piano du Conservatoire Shnittke de Moscou.</p>
		<p><b>Mm. Eug&egrave;nia  Verdet</b><br>&middot;Pianiste et fondatrice du prix</p>
	<? break;
		
	default: ?>
	<h1>Concurso Internacional de Piano para J&oacute;venes Promesas</h1>
	<div class="cuerpo_hi"> 
    	<h2>Jurado</h2>
    	<p><b><em>Presidente de honor : </em>Dr.Jordi Roch i Bosh</b><br>&middot; Presidente de Juventudes Musicales de Espa&ntilde;a.<br>&middot; Presidente de honor de Jeunesses Musicals Internacional.<br>&middot; Miembro individual del International Musica Council de la UNESCO.</p>
    	<p><b><em>Presidente :</em> Sr. Angel Soler</b><br>&middot; Concertista y director de Estudio de M&uacute;sica que trae su nombre, especializado en la ense&ntilde;anza del piano, y es profesor de m&uacute;sica de cuarto al Conservatorio del Liceo </p>
    	<h2>Vocales</h2>
    	<p><b>Sra. Ana M&ordf; Cardona</b><br>&middot; Concertista y Catedr&aacute;tica de piano del Conservatorio Superior de Musica del Liceo </p>
    	<p><b>Sra. Montserrat Gasset</b><br>&middot; Pianista diplomada por&rsquo;l Academia Marshall </p>
    	<p><b>Sr. Cecilio Tieles</b><br>Concertista y Catedr&aacute;tico del Conservatorio Superior del Liceo y del Profesional de Vilaseca </p>
    	<p><b>Sr. Gennady Dzubenko</b><br>&middot; Concertista, ha formado parte de entre otras del The Romantic Tr&iacute;o (Mosc&uacute;) y del Tr&iacute;o Mediterr&aacute;neo (Barcelona).<br>&middot; Profesor de la escuela de musica Luthier de Barcelona, del curso internacional Issac Albeniz de Camprodon y decano de la facultad de piano del Conservatorio Shnittke de Mosc&uacute;</p>
    	<p><b>Sra. Eug&egrave;nia Verdet</b><br>&middot; Pianista y fundadora del Premio </p>
	<? break; } ?>
    
    </div>

<?php include("../piecera.php"); ?>