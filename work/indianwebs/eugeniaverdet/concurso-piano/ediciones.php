<?php include("../cabecera.php"); ?>
   	
<? switch($_SESSION["lang"]) {
	
	case "cat": ?>
	<h1>Edicions anteriors del Concurs Eug&egrave;nia Verdet</h1>
	<div class="cuerpo_hi">
		<h2>Nostres/as guanyadors</h2>
		<p>&nbsp;&nbsp;<b>I Edici&oacute;</b> del concurs de piano 2002<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/I-edicion-2002.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Veure retallada de premsa</em></a></p>
		<p>&nbsp;<b>II Edici&oacute;</b> del concurs de piano 2004<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/II-edicion-2004.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Veure retallada de premsa</em></a></p>
		<p><b>III Edici&oacute;</b> del concurs de piano 2005<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/III-edicion-2005.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Veure retallada de premsa</em></a></p>
		<p>&nbsp;<b>IV Edici&oacute;</b> del concurs de piano 2006<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/IV-edicion-2006.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Veure retallada de premsa</em></a></p>
		<p>&nbsp;&nbsp;<b>V Edici&oacute;</b> del concurs de piano 2007<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/V-edicion-2007.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Veure retallada de premsa</em></a></p>
		<p>&nbsp;&nbsp;<b>VI Edici&oacute;</b> del concurs de piano 2008<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/VI-edicion-2008.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Veure retallada de premsa</em></a></p>
		<p>&nbsp;</p><p>&nbsp;</p>
	<? break;
				
	case "ing": ?>
	<h1>Eugenia Verdet  Previous Editions</h1>
	<div class="cuerpo_hi">
		<h2>Our winners</h2>
		<p>&nbsp;&nbsp;<b>I Edition </b>Contest 2002<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/I-edicion-2002.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>See  press review</em></a></p>
		<p>&nbsp;<b>II Edition </b>Contest 2004<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/II-edicion-2004.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>See  press review</em></a></p>
		<p><b>III Edition </b>Contest 2005<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/III-edicion-2005.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>See  press review</em></a></p>
		<p>&nbsp;<b>IV Edition </b>Contest 2006<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/IV-edicion-2006.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>See  press review</em></a></p>
		<p>&nbsp;&nbsp;<b>V Edition </b>Contest 2007<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/V-edicion-2007.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>See  press review</em></a></p>
		<p>&nbsp;&nbsp;<b>VI Edition </b>Contest 2008<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/VI-edicion-2008.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>See  press review</em></a></p>
		<p>&nbsp;</p><p>&nbsp;</p>
	<? break;
	
	case "ger": ?>
	<h1>Vorherige Ausgaben des Preises Eug&egrave;nia Verdet</h1>
	<div class="cuerpo_hi">
		<h2>Unsere Preistr&auml;ger/-inen</h2>
		<p>&nbsp;&nbsp;<b>I Ausgabe </b>des Klavierwettbewerbs 2002<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/I-edicion-2002.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>siehe Presseschau</em></a></p>
		<p>&nbsp;<b>II Ausgabe </b>des Klavierwettbewerbs 2004<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/II-edicion-2004.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>siehe Presseschau</em></a></p>
		<p><b>III Ausgabe </b>des Klavierwettbewerbs 2005<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/III-edicion-2005.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>siehe Presseschau</em></a></p>
		<p>&nbsp;<b>IV Ausgabe </b>des Klavierwettbewerbs 2006<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/IV-edicion-2006.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>siehe Presseschau</em></a></p>
		<p>&nbsp;&nbsp;<b>V Ausgabe </b>des Klavierwettbewerbs 2007<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/V-edicion-2007.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>siehe Presseschau</em></a></p>
		<p>&nbsp;&nbsp;<b>VI Ausgabe </b>des Klavierwettbewerbs 2008<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/VI-edicion-2008.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>siehe Presseschau</em></a></p>
		<p>&nbsp;</p><p>&nbsp;</p>
	<? break;
	
	case "fra": ?>
	<h1>Editions ant&eacute;rieurs au concours Eug&egrave;nia Verdet</h1>
	<div class="cuerpo_hi">
		<h2>Nos gagnants</h2>
		<p>&nbsp;&nbsp;<b>I Edition</b> du concours de piano 2002<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/I-edicion-2002.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Voir la coupure de  presse</em></a></p>
		<p>&nbsp;<b>II Edition</b> du concours de piano 2004<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/II-edicion-2004.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Voir la coupure de  presse</em></a></p>
		<p><b>III Edition</b> du concours de pianoo 2005<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/III-edicion-2005.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Voir la coupure de  presse</em></a></p>
		<p>&nbsp;<b>IV Edition</b> du concours de piano 2006<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/IV-edicion-2006.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Voir la coupure de  presse</em></a></p>
		<p>&nbsp;&nbsp;<b>V Edition</b> du concours de piano 2007<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/V-edicion-2007.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Voir la coupure de  presse</em></a></p>
		<p>&nbsp;&nbsp;<b>VI Edition</b> du concours de piano 2008<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/VI-edicion-2008.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Voir la coupure de  presse</em></a></p>
		<p>&nbsp;</p><p>&nbsp;</p>
	<? break;
		
	default: ?>
	<h1>Ediciones anteriores del Concurso Eug&egrave;nia Verdet</h1>
	<div class="cuerpo_hi"> 
		<h2>Nuestros/as ganadores</h2>
		<p>&nbsp;&nbsp;<b>I Edici&oacute;n</b> del concurso de piano 2002<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/I-edicion-2002.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Ver recorte de prensa</em></a></p>
		<p>&nbsp;<b>II Edici&oacute;n</b> del concurso de piano 2004<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/II-edicion-2004.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Ver recorte de prensa</em></a></p>
		<p><b>III Edici&oacute;n</b> del concurso de piano 2005<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/III-edicion-2005.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Ver recorte de prensa</em></a></p>
		<p>&nbsp;<b>IV Edici&oacute;n</b> del concurso de piano 2006<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/IV-edicion-2006.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Ver recorte de prensa</em></a></p>
		<p>&nbsp;&nbsp;<b>V Edici&oacute;n</b> del concurso de piano 2007<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/V-edicion-2007.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Ver recorte de prensa</em></a></p>
		<p>&nbsp;&nbsp;<b>VI Edici&oacute;n</b> del concurso de piano 2008<br>&nbsp;&nbsp;<a href="http://www.eugeniaverdet.com/files/VI-edicion-2008.pdf" target="_blank"><img src="http://www.eugeniaverdet.com/img/icon_pdf.gif" border="0" align="absmiddle"> <em>Ver recorte de prensa</em></a></p>
		<p>&nbsp;</p><p>&nbsp;</p>
	<? break; } ?>    
    
    </div>
    
<?php include("../piecera.php"); ?>