<?php include("../cabecera.php"); ?>

<? switch($_SESSION["lang"]) {
	
	case "cat": ?>
	<h1>Patrocinadors del Concurs</h1>
	<? break;
				
	case "ing": ?>
	<h1>Sponsors</h1>
	<? break;
	
	case "ger": ?>
	<h1>Sponsoren</h1>
	<? break;
	
	case "fra": ?>
	<h1>Sponsors</h1>
	<? break;
		
	default: ?>
	<h1>Patrocinadores del Concurso</h1>
	<? break; } ?>
    
 	<div class="cuerpo">   
        <table cellpadding="0" cellspacing="0" class="tb_patrocina">
        <tr>
        	<td><img src="http://www.eugeniaverdet.com/img/patrocinador_vilobi.gif" alt="Vilobi del Penedes" title="Vilobi del Penedes"></td>  
            <td><img src="http://www.eugeniaverdet.com/img/patrocinador_airolo.gif" alt="Airolo Catering" title="Airolo Catering"></td>
            <td><img src="http://www.eugeniaverdet.com/img/patrocinador_rotaryclub.gif" alt="Rotary Club de Vilafranca del Penedes" title="Rotary Club de Vilafranca del Penedes"></td>	
        	<td><img src="http://www.eugeniaverdet.com/img/patrocinador_caixapenedes.gif" alt="Caixa Penedes" title="Caixa Penedes"></td>
        </tr>
        <tr>
        	<td><img src="http://www.eugeniaverdet.com/img/patrocinador_freixenet.gif" alt="Freixenet" title="Freixenet"></td>
            <td><img src="http://www.eugeniaverdet.com/img/patrocinador_penedes.gif" alt="Academia Tastavins Penedes" title="Academia Tastavins Penedes"></td>
        	<td><img src="http://www.eugeniaverdet.com/img/patrocinador_pioneer.gif" alt="Pioneer" title="Pioneer"></td>
             <td><img src="http://www.eugeniaverdet.com/img/patrocinador_fidelcolor.gif" alt="Fidelcolor" title="Fidelcolor"></td>
        </tr>
        <tr>
        	<td><img src="http://www.eugeniaverdet.com/img/patrocinador_sony.gif" alt="Sony" title="Sony"></td>
            <td><img src="http://www.eugeniaverdet.com/img/patrocinador_vallformosa.gif" alt="Vallformosa" title="Vallformosa"></td>
        	<td><img src="http://www.eugeniaverdet.com/img/patrocinador_auditori.gif" alt="Auditori Pau Casals" title="Auditori Pau Casals"></td>
            <td><img src="http://www.eugeniaverdet.com/img/patrocinador_catmusica.gif" alt="Catalunya Musica" title="Catalunya Musica"></td>
        </tr>
        </table>
    </div>
   
<?php include("../piecera.php"); ?>