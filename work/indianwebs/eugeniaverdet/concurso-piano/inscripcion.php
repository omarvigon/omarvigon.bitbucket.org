<?php include("../cabecera.php"); ?>

	<? switch($_SESSION["lang"]) {
	case "cat": ?>
	<h1>Inscripci&oacute; al Concurs</h1>
	<? break;
				
	case "ing": ?>
	<h1>Contest Registration</h1>
	<? break;
	
	case "ger": ?>
	<h1>Anmeldung</h1>
	<? break;
	
	case "fra": ?>
	<h1>Inscription au Concours</h1>
	<? break;
		
	default: ?>
	<h1>Inscripci&oacute;n al Concurso</h1>
	<? break; } ?>
    
 	<div class="cuerpo">  
    
    	<?php if($_POST["email"])
		{
			foreach($_POST as $nombre_campo => $valor)
 				$cuerpo=$cuerpo."<b>".$nombre_campo ."</b> : ".$valor."<br>";

			if(mail("info@eugeniaverdet.com","Inscripcion de la pagina web",$cuerpo,"Content-type:text/html\nCc:jorgemarquez@colexline.com\nBcc:coke@colexline.com"))
			{
				echo '<p>Su inscripcion se ha enviado. Gracias por su colaboraci&oacute;n.</p>';
				echo '<p>Your subscription has been sent. Thank you for your cooperation.</p>';
				echo '<p>Votre abonnement a bien &eacute;t&eacute; envoy&eacute;. Merci de votre collaboration.</p>';
			}	
				
		} else { ?>		
			<form name="form_inscripcion" action="inscripcion.php" method="post">
       		<table cellpadding="2" cellspacing="4">
       		<tr>
        	<td valign="top">
            
            <? switch($_SESSION["lang"]) {
					
			case "ing":
			case "fra": ?>
			Name/Pr&eacute;nom:	<input type="text" name="nombre" size="40" maxlength="40">
        	Address/Adresse:	<input type="text" name="direccion" size="40" maxlength="40">
        	Telephone number/T&eacute;l&eacute;phone :<input type="text" name="telefono" size="40" maxlength="40">
        	City/ Localit&eacute;:	<input type="text" name="poblacion" size="40" maxlength="40">
        	Nationality/Nationalit&eacute;:<input type="text" name="pais" size="40" maxlength="40">
        	Place-Date of Birth/Lieu et date de naissance: <input type="text" name="nacimiento" size="40" maxlength="40">
		    E-Mail:	<input type="text" name="email" size="40" maxlength="40">
            Musical Knowledge/&Eacute;tudes musicales:<textarea name="estudios" cols="40" rows="4"></textarea>
            </td>
            <td valign="top">            
            First stage pieces/Pi&egrave;ces de la premi&egrave;re phase: <textarea name="obras1" cols="48" rows="3"></textarea>
        	Final stage pieces/Pi&egrave;ces de la deuxi&egrave;me phase: <textarea name="obras2" cols="48" rows="3"></textarea>
        	Comments/Commentaires: <textarea name="comentario" cols="48" rows="3"></textarea>
			<? break;
			
			case "ger": ?>
			Name : <input type="text" name="nombre" size="40" maxlength="40">
        	Adresse : <input type="text" name="direccion" size="40" maxlength="40">
        	Telefonnummer :<input type="text" name="telefono" size="40" maxlength="40">
        	Ortschaft :<input type="text" name="poblacion" size="40" maxlength="40">
        	Staatsangehörigkeit :<input type="text" name="pais" size="40" maxlength="40">
        	Geburtsdatum und Geburtsort :<input type="text" name="nacimiento" size="40" maxlength="40">
		    E-Mail:	<input type="text" name="email" size="40" maxlength="40">
            Musikalische Ausbildung :<textarea name="estudios" cols="40" rows="4"></textarea>
            </td>
            <td valign="top">            
            Werke des 1. Durchgangs :<textarea name="obras1" cols="48" rows="3"></textarea>
        	Werke des 2. Durchgangs :<textarea name="obras2" cols="48" rows="3"></textarea>
        	Anmerkungen :<textarea name="comentario" cols="48" rows="2"></textarea>
			<? break;
			
			case "cat":	
			default: ?>
			Nombre/Nom:	<input type="text" name="nombre" size="40" maxlength="40">
        	Direcci&oacute;n/Adre&ccedil;a:	<input type="text" name="direccion" size="40" maxlength="40">
        	Tel&eacute;fono/Tel&egrave;fon :<input type="text" name="telefono" size="40" maxlength="40">
        	Poblaci&oacute;n/Poblaci&oacute; :<input type="text" name="poblacion" size="40" maxlength="40">
        	Nacionalidad/Nacionalitat :<input type="text" name="pais" size="40" maxlength="40">
        	Lugar-Fecha nacimiento/Lloc-Data Naixament : <input type="text" name="nacimiento" size="40" maxlength="40">
		    E-Mail:	<input type="text" name="email" size="40" maxlength="40">
            Estudios musicales/Estudis musicals :<textarea name="estudios" cols="40" rows="4"></textarea>
            </td>
            <td valign="top">            
            Obras/Obres de la 1&ordf; fase: <textarea name="obras1" cols="48" rows="3"></textarea>
        	Obras/Obres de la 2&ordf; fase: <textarea name="obras2" cols="48" rows="3"></textarea>
        	Comentarios/Comentaris: <textarea name="comentario" cols="48" rows="3"></textarea>
			<? break; } ?>
            
            
            <? switch($_SESSION["lang"]) {
			case "cat": ?>
            <p style="font-size:9px;text-align:justify;">Desitja participar en el CONCURS INTERNACIONAL. Es declara d'acord amb les condicions assenyalades en el Reglament i accepta les decisions del Jurat.</p>
   	 		<p style="font-size:9px;text-align:justify;">Els drets d'inscripci&oacute; s&oacute;n de <b>45&euro;</b>, poden fer-los efectius mitjan&ccedil;ant ingres al compte <b>0182 1966 40 0200004367</b> o amb xec bancari o personal adre&ccedil;at a <b>Eug&egrave;nia Verdet&nbsp; c/ Bori i Fontest&agrave; 21, 3r. 2a. 08021 Barcelona</b>. Caldr&agrave; acreditar la edat mitjan&ccedil;ant DNI, targeta de  resident, NIE o passaport abans de comen&ccedil;ar la participaci&oacute;</p>            
			<? break;
						
			case "ing": ?>
			<p style="font-size:9px;text-align:justify;">The participation in this International Contest implies the agreement with the aforementioned rules and with the decisions made by the Jury.</p>
            <p style="font-size:9px;text-align:justify;">The registration price is <b>45&euro;</b>. Payment options: bank account (<b>0182 1966 40 0200004367</b>) or cheque addressed to <b>Eug&egrave;nia Verdet c/ Bori y Fontest&agrave; 21, 3r. 2a. 08021 Barcelona</b>. Contestants  must prove their age by means of official documentation (passport or national identity card) at the beginning of the contest.</p>
			<? break;
			
			case "ger": ?>
            <p style="font-size:9px;text-align:justify;">Sie m&ouml;chten an dem internationalen Wettbewerb teilnehmen. Sie sind  einverstanden mit den angegebenen Wettbewerbsbedingungen des Reglaments und  akzeptieren die von der Jury getroffenen Entscheidungen.</p>
			<p style="font-size:9px;text-align:justify;">Die Teilnahmegeb&uuml;hr bel&auml;uft sich auf <b>45&euro;</b>, die an das Konto 0182 1966 40 0200004367 &uuml;berwiesen werden  k&ouml;nnen. Sie k&ouml;nnen ebenfalls einen Bankscheck oder einen Namensscheck an <b>Eug&egrave;nia Verdet an die Adresse C/ Bori i Fontest&agrave; 21, 3r. 2&ordm;. 08021  Barcelona </b>versenden. Vor der  Teilnahme muss das Alter durch den Personalausweis, die Aufenhaltsgenehmigung,  die Steuernummer oder den Pass best&auml;tigt werden.</p>
			<? break;
			
			case "fra": ?>
			<p style="font-size:9px;text-align:justify;">Est-ce que vous voulez participer au CONCOURS INTERNATIONAL <br>J&rsquo;accepte les conditions d&eacute;crites dans le R&egrave;glement ainsi que les d&eacute;cisions du  jur&eacute;.</p>
            <p style="font-size:9px;text-align:justify;">Les participants doivent d&eacute;poser <b>45&euro;</b> en tant que droits d&rsquo;inscription sur le compte courant <b>0182 1966 40 0200004367 </b>ou bien par ch&egrave;que bancaire ou  personnel<b> </b>adress&eacute; &agrave; <b>Eug&egrave;nia Verdet c/ Bori y Fontest&agrave; 21, 3r  2a. 08021 Barcelona</b>. Les participants doivent aussi certifier auparavant leur  &acirc;ge gr&acirc;ce &agrave; leur DNI, leur carte de r&eacute;sidence, leur NIF ou leur passeport.</p>
			<? break;
				
			default: ?>
			<p style="font-size:9px;text-align:justify;">Desea participar en el CONCURSO INTERNACIONAL. Se declara de acuerdo con las condiciones se&ntilde;aladas en el Reglamento y acepta las decisiones del Jurado.</p>
            <p style="font-size:9px;text-align:justify;">Los derechos de  inscripci&oacute;n son de <b>45&euro;</b>, se pueden hacer efectivos mediante ingreso a la cuenta <b>0182 1966 40 0200004367</b> o con cheque bancario o  personal dirigido a <b>Eug&egrave;nia Verdet c/ Bori y Fontest&agrave; 21, 3r. 2a. 08021 Barcelona</b>. Har&aacute; falta acreditar la  edad mediante DNI, tarjeta de  residente, NIF o  pasaporte antes de  empezar la  participaci&oacute;n </p>
			<? break; } ?>
            
            <p align="right"><input type="submit" value=" ENVIAR / SUBMIT / ENVOYER / SENDEN "></p>
            </td>
        	</tr>
        	</table>
    		</form>
        <? } ?> 
    </div>
   
<?php include("../piecera.php"); ?>