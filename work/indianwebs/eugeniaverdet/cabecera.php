<?php
session_start();
if($_GET["lang"])			
	$_SESSION["lang"]=$_GET["lang"];
else	
{
	if(!$_SESSION["lang"])	
		$_SESSION["lang"]="esp";
}	
$nombre=strtoupper($_SERVER['PHP_SELF']);
$nombre=substr($nombre,1,strlen($nombre)-5);

$boton1["esp"]="Inicio";				
$boton2["esp"]="Concurso";				
$boton3["esp"]="Reglamento";			
$boton4["esp"]="Jurado";				
$boton5["esp"]="Premios";				
$boton6["esp"]="Patrocinadores";		
$boton7["esp"]="Inscripci&oacute;n";	
$boton8["esp"]="Historia";				
$boton9["esp"]="Contacto";				
$boton10["esp"]="Nuestros/as ganadores";
$boton11["esp"]="Ganador 2008";

$boton1["cat"]="Inici";
$boton2["cat"]="Concurs";
$boton3["cat"]="Reglament";
$boton4["cat"]="Jurat";
$boton5["cat"]="Premis";
$boton6["cat"]="Patrocinadors";
$boton7["cat"]="Inscripci&oacute;";
$boton8["cat"]="Historia";
$boton9["cat"]="Contacte";
$boton10["cat"]="Nostres/as guanyadors";
$boton11["cat"]="Guanyador 2008";

$boton1["ing"]="Home";				
$boton2["ing"]="Contest";				
$boton3["ing"]="Rules and regulations";			
$boton4["ing"]="Jury";				
$boton5["ing"]="Awards";				
$boton6["ing"]="Sponsors";		
$boton7["ing"]="Registration";	
$boton8["ing"]="History";				
$boton9["ing"]="Contact us";				
$boton10["ing"]="Our winners";
$boton11["ing"]="Winner 2008";

$boton1["fra"]="D&eacute;but";				
$boton2["fra"]="Concours";				
$boton3["fra"]="R&egrave;glement";			
$boton4["fra"]="Jur&eacute;";				
$boton5["fra"]="Prix";				
$boton6["fra"]="Sponsors";		
$boton7["fra"]="Inscription";	
$boton8["fra"]="Histoire";				
$boton9["fra"]="Contact";				
$boton10["fra"]="Nos gagnants";
$boton11["fra"]="Gagnant 2008";

$boton1["ger"]="Home";				
$boton2["ger"]="Wettbewerb";				
$boton3["ger"]="Richtlinien";			
$boton4["ger"]="Die Jury";				
$boton5["ger"]="Die Preise";				
$boton6["ger"]="Sponsoren";		
$boton7["ger"]="Anmeldung";	
$boton8["ger"]="Biografie";				
$boton9["ger"]="Kontakt";				
$boton10["ger"]="Unsere Preistr&auml;ger/-inen";
$boton11["ger"]="Preistr&auml;ger/-inen 2008";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)">
    <meta name="revisit" content="7 days">
    <meta name="robots" content="index,follow,all">
	<meta name="description" content="Eugenia Verdet VII Concurso Internacional de Piano apra Jovenes Promesas. Eugenia Verdet Historia del certamen, patrocinadores, bases de inscripcion.">
	<meta name="keywords" content="Eugenia Verdet, concurso piano, eugenia concurso piano, biografia eugenia verdet,concurso piano verdet,piano eugenia verdet">
    <meta name="lang" content="es,sp,spanish,espa�ol,espanol,castellano">
    <meta name="author" content="www.indianwebs.com">
	<title>Eugenia Verdet VII Concurso Internacional de Piano para Jovenes Promesas <?php echo $nombre;?></title>
	<link rel="stylesheet" type="text/css" href="http://www.eugeniaverdet.com/estilos.css">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>

<body>
<div id="superior">&nbsp;</div>

<div id="central">
	<img src="http://www.eugeniaverdet.com/img/fondo_3.gif" class="idiomas" border="0" usemap="#mapa1">
    
	<div id="boton">
    	<img src="http://www.eugeniaverdet.com/img/eugenia-verdet.gif" alt="Eugenia Verdet" title="Eugenia Verdet">
    	<a href="http://www.eugeniaverdet.com/index.php" title="Eugenia Verdet Inicio"><? echo $boton1[$_SESSION["lang"]]?></a>
        <a href="http://www.eugeniaverdet.com/concurso-piano/reglamento.php" title="Concurso de Piano Eugenia Verdet"><? echo $boton2[$_SESSION["lang"]]?></a>
        	<? if($_SERVER['PHP_SELF']=="/concurso-piano/reglamento.php" || $_SERVER['PHP_SELF']=="/concurso-piano/jurado.php" || $_SERVER['PHP_SELF']=="/concurso-piano/premios.php" || $_SERVER['PHP_SELF']=="/concurso-piano/patrocinadores.php" || $_SERVER['PHP_SELF']=="/concurso-piano/inscripcion.php") { ?>
           	<a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/reglamento.php" title="Reglamento del Concurso de Piano"><? echo $boton3[$_SESSION["lang"]]?></a>
       	 	<a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/jurado.php" title="Jurado del Concurso de Piano"><? echo $boton4[$_SESSION["lang"]]?></a>
        	<a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/premios.php" title="Premios del Concurso de Piano"><? echo $boton5[$_SESSION["lang"]]?></a>
        	<a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/patrocinadores.php" title="Patrocinadores del Concurso de Piano"><? echo $boton6[$_SESSION["lang"]]?></a>
        	<a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/inscripcion.php" title="Inscripcion del Concurso de Piano"><? echo $boton7[$_SESSION["lang"]]?></a>
        	<? } ?>
        <a href="http://www.eugeniaverdet.com/concurso-piano/historia.php" title="Historia del Concurso de Piano"><? echo $boton8[$_SESSION["lang"]]?></a>
        <? if($_SERVER['PHP_SELF']=="/concurso-piano/historia.php" || $_SERVER['PHP_SELF']=="/concurso-piano/ediciones.php" || $_SERVER['PHP_SELF']=="/concurso-piano/ganador.php") { ?>
           	<a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/ganador.php" title="Ganador del Concurso de Piano"><? echo $boton11[$_SESSION["lang"]]?></a>
            <a style="font-size:11px;color:#999999;" href="http://www.eugeniaverdet.com/concurso-piano/ediciones.php" title="Ediciones del Concurso de Piano"><? echo $boton10[$_SESSION["lang"]]?></a>
        	<? } ?>
		<a href="http://www.eugeniaverdet.com/concurso-piano/contacto.php" title="Contacto con Eugenia Verdet"><? echo $boton9[$_SESSION["lang"]]?></a>
    </div>