<?php include("cabecera.php"); ?>

    <? switch($_SESSION["lang"]) {
	
	case "cat": ?>
    <h1>VII Concurs Internacional de Piano per Joves Promeses</h1>
	<? break;
			
	case "ing": ?>
    <h1>VII International Piano Contest for Young Talents</h1>
	<? break;
	
	case "ger": ?>
<h1>VII Internationaler  Klavier-Wettbewerb f&uuml;r junge Talente</h1>
	<? break;
	
	case "fra": ?>
<h1>VII Concours International de Piano pour des Jeunes Espoirs</h1>
	<? break;
	
	default: ?>
<h1>VII Concurso Internacional de Piano para J&oacute;venes Promesas</h1>
	<? break; } ?>
	
  <div class="cuerpo">   
    	<img src="http://www.eugeniaverdet.com/img/pianos.gif" class="piano">
        <? switch($_SESSION["lang"])
		{ 
			case "cat": ?>
            <br><h2>Premi Eug&egrave;nia Verdet</h2><br>
            <h2>Ajuntament de Vilob&iacute; del Pened&egrave;s</h2><br>
            <h2><em>Dies 8-10 de Maig de 2009</em></h2><br>     
            <img title="Concurso de Piano Eugenia Verdet" src="http://www.eugeniaverdet.com/img/concurso_piano.gif" alt="Concurso de Piano Eugenia Verdet">
        	<p>Asociación Musical Eugenia Verdet </p>			
			<? break;
			
			case "ing": ?>
			<br><h2>Eugenia Verdet Award</h2><br>
			<h2>Vilobi del Penedes Council</h2><br>
			<h2><em>Days: 8th-10th May 2009</em></h2><br>
            <img src="http://www.eugeniaverdet.com/img/concurso_piano.gif" alt="Eugenia Verdet Piano Contest" title="Eugenia Verdet Piano Contest">
        	<p>Asociación Musical Eugenia Verdet </p>
						<!--
            <p><b>First stage:</b><br>VILAFRANCA MUSEUM <i>(MUSEU DEL VI)</i><br>Vilafranca del Penedes <i>(Barcelona)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=+%09Pla%C3%A7a+Jaume+I,+n%C3%BAmero+1+%C2%B7+08720+Vilafranca+del+Pened%C3%A8s&sll=41.34943,1.69875&sspn=0.003697,0.006909&ie=UTF8&ll=41.349237,1.69874&spn=0.007394,0.013819&z=16&iwloc=addr" target="_blank">Map...</a></p>
            <p><b>Final stage:</b><br>Jaume Via de Vilobi Auditorium <i>(Barcelona)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=Vilob%C3%AD&ie=UTF8&ll=41.410806,1.761932&spn=0.236372,0.4422&z=11&iwloc=addr" target="_blank">Map...</a></p>
            <h2><a href="http://www.eugeniaverdet.com/concurso-piano/inscripcion.php" title="Eugenia Verdet Registration Piano Contest">GET YOUR REGISTRATION HERE!</a></h2> 
            -->
			<? break;
			
			case "ger": ?>
            <br><h2>Eugenia Verdet Preis</h2><br>
            <h2>Rathaus von Vilob&iacute; del Pened&egrave;s</h2><br>
            <h2><em>Vom 8 bis 10 Mai 2009</em></h2><br>
            <img src="http://www.eugeniaverdet.com/img/concurso_piano.gif" alt="Eugenia Verdet Preis" title="Eugenia Verdet Preis">
        	<p>Asociación Musical Eugenia Verdet </p>            <!--
            <p><b>Erster Durchgang:</b><br>VILAFRANCA MUSEUM <i>(MUSEU DEL VI)</i><br>Vilafranca del Penedes <i>(Barcelona)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=+%09Pla%C3%A7a+Jaume+I,+n%C3%BAmero+1+%C2%B7+08720+Vilafranca+del+Pened%C3%A8s&sll=41.34943,1.69875&sspn=0.003697,0.006909&ie=UTF8&ll=41.349237,1.69874&spn=0.007394,0.013819&z=16&iwloc=addr" target="_blank">Landkarte...</a></p>
            <p><b>Letzter Durchgang:</b><br>Konzertsaal Auditorio Jaume Via de Vilobi <i>(Barcelona)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=Vilob%C3%AD&ie=UTF8&ll=41.410806,1.761932&spn=0.236372,0.4422&z=11&iwloc=addr" target="_blank">Landkarte...</a></p>
            <h2><a href="http://www.eugeniaverdet.com/concurso-piano/inscripcion.php" title="Eugenia Verdet Registration Piano Contest">SIE K&Ouml;NNEN SICH HIER ANMELDEN!</a></h2>
            -->
			<? break;
			
			case "fra": ?>
            <br><h2>Prix Eug&egrave;nia Verdet</h2><br>
            <h2>Mairie de Vilob&iacute; del Pened&egrave;s (Catalogne)</h2><br>
            <h2><em>Date: 8-10 Mai 2009</em></h2><br>
            <img src="http://www.eugeniaverdet.com/img/concurso_piano.gif" alt="Eugenia Verdet Piano Contest" title="Eugenia Verdet Piano Contest">
        	<p>Asociación Musical Eugenia Verdet </p>            <!--
            <p><b>Premi&egrave;re Phase:</b><br>MUS&Eacute;E DE VILAFRANCA <i>(MUS&Eacute;E DU VIN) </i><br>Vilafranca del Pened&egrave;s <i>(Barcelone)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=+%09Pla%C3%A7a+Jaume+I,+n%C3%BAmero+1+%C2%B7+08720+Vilafranca+del+Pened%C3%A8s&sll=41.34943,1.69875&sspn=0.003697,0.006909&ie=UTF8&ll=41.349237,1.69874&spn=0.007394,0.013819&z=16&iwloc=addr" target="_blank">Carte...</a></p>
  			<p><b>Phase Finale : </b><br>Auditorium Jaume Via de Vilob&iacute; <i>(Barcelone)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=Vilob%C3%AD&ie=UTF8&ll=41.410806,1.761932&spn=0.236372,0.4422&z=11&iwloc=addr" target="_blank">Carte...</a></p>
		  	<h2><a href="http://www.eugeniaverdet.com/concurso-piano/inscripcion.php" title="Eugenia Verdet Prix">INSCRIVEZ VOUS ICI!</a></h2>
            -->
            <? break;
			
			default: ?>
            <br><h2>Premio Eugenia Verdet</h2><br>
        	<h2>Ayuntamiento de Vilob&iacute; del Pened&egrave;s</h2><br>
        	<h2><em>D&iacute;as 8-10 de Mayo de 2009</em></h2><br>
        	<img src="http://www.eugeniaverdet.com/img/concurso_piano.gif" alt="Concurso de Piano Eugenia Verdet" title="Concurso de Piano Eugenia Verdet">
        	<p>Asociación Musical Eugenia Verdet </p>
            <!--
            <p><b>Primera Fase:</b><br>MUSEU DE VILAFRANCA <i>(MUSEU DEL VI)</i><br>Vilafranca del Pened&eacute;s <i>(Barcelona)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=+%09Pla%C3%A7a+Jaume+I,+n%C3%BAmero+1+%C2%B7+08720+Vilafranca+del+Pened%C3%A8s&sll=41.34943,1.69875&sspn=0.003697,0.006909&ie=UTF8&ll=41.349237,1.69874&spn=0.007394,0.013819&z=16&iwloc=addr" target="_blank">Mapa...</a></p>  
        	<p><b>Fase Final :</b><br>Auditorio Jaume Via de Vilob&iacute; <i>(Barcelona)</i> <a href="http://maps.google.es/maps?f=q&hl=es&geocode=&q=Vilob%C3%AD&ie=UTF8&ll=41.410806,1.761932&spn=0.236372,0.4422&z=11&iwloc=addr" target="_blank">Mapa...</a></p>
        	<h2><a href="http://www.eugeniaverdet.com/concurso-piano/inscripcion.php" title="Inscripcion del Concuso de Eugenia Verdet">&iexcl;INSCR&Iacute;BETE AQUI!</a></h2>
            -->
			<? break;
		}	
		?>
  </div>

<?php include("piecera.php"); ?>