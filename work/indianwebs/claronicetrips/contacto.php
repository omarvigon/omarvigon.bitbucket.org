<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/new/templates/plantilla.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="headedit" -->
<script type="text/javascript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' debe ser una direcci�n de correo v�lida.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += 'Su ''- '+nm+' es necesario.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<script type="text/javascript">
function comprobar()
{
	if(document.bov.condiciones.checked && document.bov.nombre.value!="" && document.bov.telefono.value!="" && document.bov.email.value!="")
		document.bov.submit();
	else
		alert("Los campos marcados (*) son obligatorios");
}
</script>
<!-- InstanceEndEditable -->
<style type="text/css">
-->
</style>
<link href="css/estilos.css" rel="stylesheet" type="text/css">
<title>Claro nice trips | Agencia de viajes</title>
<link rel="shortcut icon" href="/new/favicon.ico">
</head>
<body>
<div id="center"align="center">
	<div id="headercontent">
		<div id="header">
			<div id="logo"><a href="index.htm"><img src="images/logo-claro.gif" alt="Claro nice tripsa ,gencia de viajes" width="173" height="88" border="0"></a></div>
	        <!-- InstanceBeginEditable name="botonera" -->
	        <div id="boton5">
              <div id="texto-boton">
                <h4><a href="claro-agencia-de-viajes.htm">Claro nicetrips</a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="europa.htm">Nuestros destinos </a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="servicios-a-empresas.htm">Servicios a empresas </a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="incentivos-convenciones.htm">Incentivos Convenciones</a> </h4>
              </div>
              <div id="texto-boton">
                <h4><a href="contacto.php">Contacto</a></h4>
              </div>
            </div>
	        <!-- InstanceEndEditable --></div>
	</div>
<div id="wrapper">

<!-- InstanceBeginEditable name="content" -->  
<div id="contenido-b">
<div id="col2-1">
  <table width="520"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="470">
		<img src="images/formulario.jpg" width="470" height="51">
		<? 
		if (!$HTTP_POST_VARS){ 
		?> 
		<form action="contacto.php" method="post" name="bov" id="form" > 
    <p>Si desea ponerse en contacto con<strong> Claro nice trips,</strong> env&iacute;enos el siguiente formulario: </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74" class="formstexto">Empresa</td>
        <td><input name="empresa" type="text" class="formsrespuesta" id="empresa" size="50" maxlength="50"></td>
      </tr>
      <tr>
        <td class="formstexto">Nombre*</td>
        <td><input name="nombre" type="text" class="formsrespuesta" id="nombre" size="50" maxlength="50"></td>
      </tr>
      <tr>
        <td class="formstexto">Direcci&oacute;n</td>
        <td><input name="direccion" type="text" class="formsrespuesta" id="direccion" size="50" maxlength="50"></td>
      </tr>
      <tr valign="middle">
        <td class="formstexto">Poblaci&oacute;n</td>
        <td><input name="poblacion" type="text" class="formsrespuesta" id="poblacion" size="33" maxlength="50"> 
          &nbsp;&nbsp;&nbsp;CP &nbsp;&nbsp;&nbsp;
            <input name="cp" type="text" class="formsrespuesta" id="cp" size="5" maxlength="5"></td>
        </tr>
      <tr>
        <td class="formstexto">Provincia</td>
        <td><select name="provincia" class="formsrespuesta">
                          <option selected>Indique su Provincia</option>
       					  <option value="Alava">Alava</option>
                          <option value="Albacete">Albacete</option>
                          <option value="Alicante">Alicante</option>
                          <option value="Almer�a">Almer�a</option>
                          <option value="Asturias">Asturias</option>
                          <option value="Avila">Avila</option>
                          <option value="Badajoz">Badajoz</option>
                          <option value="Baleares">Baleares</option>
                          <option value="Barcelona">Barcelona</option>
                          <option value="Burgos">Burgos</option>
                          <option value="Cantabria">Cantabria</option>
                          <option value="Castell�n">Castell�n</option>
                          <option value="Ceuta">Ceuta</option>
                          <option value="Ciudad Real">Ciudad Real</option>
                          <option value="Cuenca">Cuenca</option>
                          <option value="C�ceres">C�ceres</option>
                          <option value="C�diz">C�diz</option>
                          <option value="C�rdoba">C�rdoba</option>
                          <option value="Gerona">Gerona</option>
                          <option value="Granada">Granada</option>
                          <option value="Guadalajara">Guadalajara</option>
                          <option value="Guipuzcoa">Guipuzcoa</option>
                          <option value="Huelva">Huelva</option>
                          <option value="Huesca">Huesca</option>
                          <option value="Ja�n">Ja�n</option>
                          <option value="La Coru�a">La Coru�a</option>
                          <option value="La Rioja">La Rioja</option>
                          <option value="Las Palmas">Las Palmas</option>
                          <option value="Le�n">Le�n</option>
                          <option value="Lleida">Lleida</option>
                          <option value="Lugo">Lugo</option>
                          <option value="Madrid">Madrid</option>
                          <option value="Melilla">Melilla</option>
                          <option value="Murcia">Murcia</option>
                          <option value="M�laga">M�laga</option>
                          <option value="Navarra">Navarra</option>
                          <option value="Orense">Orense</option>
                          <option value="Palencia">Palencia</option>
                          <option value="Pontevedra">Pontevedra</option>
                          <option value="Salamanca">Salamanca</option>
                          <option value="Segovia">Segovia</option>
                          <option value="Sevilla">Sevilla</option>
                          <option value="Soria">Soria</option>
                          <option value="Sta. C. Tenerife">Sta. C. Tenerife</option>
                          <option value="Tarragona">Tarragona</option>
                          <option value="Teruel">Teruel</option>
                          <option value="Toledo">Toledo</option>
                          <option value="Valencia">Valencia</option>
                          <option value="Valladolid">Valladolid</option>
                          <option value="Vizcaya">Vizcaya</option>
                          <option value="Zamora">Zamora</option>
                          <option value="Zaragoza">Zaragoza</option>
                      </select></td>
      </tr>
      <tr>
        <td class="formstexto">Tel&eacute;fono*</td>
        <td><input name="telefono" type="text" class="formsrespuesta" id="telefono" size="12" maxlength="12"></td>
      </tr>
      <tr>
        <td class="formstexto">E-mail*</td>
        <td><input type="text" name="email" class="formsrespuesta" id="email" size="50" maxlength="50"></td>
		
      </tr>
      <tr>
        <td colspan="2" valign="bottom" class="formstexto">&iquest;En que podemos ayudarle?&nbsp;&nbsp;&nbsp;&nbsp;          <select name="informacion" size="1" class="formsrespuesta">
          <option value="Claro Club">Claro Club</option>
          <option value="Informaci�n para viaje" selected>Informaci&oacute;n para viaje</option>
          <option value="Solicitud de proovedor">Solicitud de proovedor</option>
          <option value="Otras cosas">Otras cosas</option>
             </select></td>
      </tr>
	  <tr>
        <td class="formstexto">&nbsp;</td>
        <td><textarea name="comentarios" cols="48" rows="4" wrap="VIRTUAL" class="formsrespuesta" id="comentarios">Comentarios</textarea></td>
      </tr>
    </table>
				<div id=capafinal>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td width="74" valign="top">&nbsp;</td>
				<td valign="top"><p>
					<input type="checkbox" name="condiciones">(*) Acepto las <a href="condiciones.htm" target="_blank">condiciones de env&iacute;o de informaci&oacute;n</a>
					<br>          
					<input name="clubclaro" type="checkbox" checked>Deseo recibir informaci&oacute;n peri&oacute;dica de <strong>Claro Club </strong></p>
					<p><input type="button" class="botonenviar" onclick="comprobar();" value="Enviar formulario >>" ></p>
				</td>
				</tr>
				</table>
				</div>
    </form>
	
	  <? 
}else{ 
    //Estoy recibiendo el formulario, compongo el cuerpo 
   
	$cuerpo .= "Empresa: " . $HTTP_POST_VARS["empresa"] . "\n"; 
	$cuerpo .= "CIF: " . $HTTP_POST_VARS["cif"] . "\n"; 
    $cuerpo .= "Nombre: " . $HTTP_POST_VARS["nombre"] . "\n"; 
	$cuerpo .= "Direccion: " . $HTTP_POST_VARS["direccion"] . "\n"; 
	$cuerpo .= "Poblacion: " . $HTTP_POST_VARS["poblacion"] . "\n"; 
	$cuerpo .= "CP: " . $HTTP_POST_VARS["cp"] . "\n"; 
	$cuerpo .= "Provincia: " . $HTTP_POST_VARS["provincia"] . "\n"; 
	$cuerpo .= "Telefono: " . $HTTP_POST_VARS["telefono"] . "\n"; 
    $cuerpo .= "Email: " . $HTTP_POST_VARS["email"] . "\n"; 
	$cuerpo .= "Informaci�n: " . $HTTP_POST_VARS["informacion"] . "\n"; 
	$cuerpo .= "Comentarios: " . $HTTP_POST_VARS["comentarios"] . "\n"; 
	
	$remitente = $_REQUEST["email"];
	
	if($_POST["clubclaro"]=="on")
	{
		if(mysql_connect("lldb542.servidoresdns.net","qdx482","9lPsGrFSK91E"))
		{
			mysql_select_db("qdx482");
			if (mysql_query("insert into usuarios set empresa='".$_POST["empresa"]."',nombre='".$_POST["nombre"]."',direccion='".$_POST["direccion"]." - ".$_POST["poblacion"]." - ".$_POST["cp"]." - ".$_POST["provincia"]."',telefono='".$_POST["telefono"]."',email='".$_POST["email"]."',otros='".$_POST["informacion"]."' "))
				echo "OK ";
		}
		mysql_close();
	}
    //mando el correo... 
    mail("info@estudigrafic.net", "Formulario recibido de claronicetrips.com",$cuerpo, "From: $remitente"); 

    //doy las gracias por el env�o 
    echo "Gracias. Hemos recibido su formulario correctamente.
	En breve nos pondremos en contacto con usted"; 
} 
?> 
  </td>
      </tr>
  </table>
  </div>
  <div id="col-2-2"><img src="images/informacion.jpg">
    </div>

</div>
<!-- InstanceEndEditable -->
<!--contenido2-->

</div>
</div>
<!--pie de pagina-->
<div id="pie1">
	<div align="center">
	<div id="contenido2">
      <div id="col-405">
	  	 <div id="cabeceras-noticias"><img src="images/seleccion-claro.gif" alt="seleccion claro nicetrips, agencias de viajes" width="182" height="17"></div>
			<a href="../destinos%20claro%20nice%20trips,%20agencia%20de%20viajes"><img src="images/tahiti-playa.jpg" border="0" class="right-margin-20"></a>
			<strong>Tahit�</strong>
			<br>
			Sin duda el paraiso para las mejores vacaciones: playas de arena blanca, aguas cristalinas, ...<br>
			<a href="#" >Conocer detalles</a>
	  </div>
	  <div id="col-202-left">
		  	<div id="cabeceras-noticias"><img src="images/claro-club.gif" alt="claro nice trips club, agencia de viajes" width="132" height="18"></div>
			<a href="../salimos%20de%20viaje%20con%20claro%20nice%20trips,%20agencia%20de%20viajes"><img src="images/maleta.gif" width="87" height="103" border="0" class="right-margin-20"></a>
			<strong>Novedad</strong>
			<br>
			Para estar al d�a de las �ltimas propuestas.<br>
		  <a href="contacto.php" >Suscribirse</a>
	  </div>
		<div id="col-202-right">
		  	<div id="cabeceras-noticias"><img src="images/canada.gif" alt="destino claro nice trips, agencia de viajes" width="95" height="18"></div>
			Montreal, Toronto, Vancouver...<br>
			Reserva de vacaciones 2009.<br>
			<a href="#" >M�s informaci�n</a>
			<br><br>
			<img src="images/avion-airtransat.jpg" alt="flota air transat" width="190" height="50">
		</div>
	<div>&nbsp;</div>
	</div>
</div>
</div>
	
	<div id="pie2">
	<div align="center">
		<div id="contenido-pie">
			<div id="contenido-pie1">
			  <h5><a href="index.htm">Inicio</a> | <a href="claro-agencia-de-viajes.htm">Claro nice Trips</a> | <a href="europa.htm">Destinos</a> | <a href="servicios-a-empresas.htm">Empresas</a> | <a href="incentivos-convenciones.htm">Incentivos</a> | <a href="contacto.php">Contacto </a></h5>
			</div>
			<div id="contenido-pie2">
			  <h5>&copy; 2008 Claro nice trips SL
                <br>
			Todos los derechos reservados</h5>
			</div>
	  </div>	
		<div>&nbsp;</div>			
	</div>	
</div>
</body>
<!-- InstanceEnd --></html>
