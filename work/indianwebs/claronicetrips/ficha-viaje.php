<?php require_once('Connections/claronicetrips.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "-1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_claronicetrips, $claronicetrips);
$query_Recordset1 = sprintf("SELECT * FROM viaje WHERE id = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $claronicetrips) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/plantilla.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="headedit" --><!-- InstanceEndEditable -->
<style type="text/css">
-->
</style>
<link href="css/estilos.css" rel="stylesheet" type="text/css">
<meta name="description" content="Agencia de viajes ubicada en el centro de Barcelona con destinos seleccionados y viajes exclusivos.">
<meta name="keywords" content="agencia de viajes, agencia de viatges, cruceros, hoteles, vuelos, destinos exclusivos, viajes de lujo, viajes de empresa, barcelona, espa�a, europa, asia, america, oceania, africa, vuelta al mundo">
<title>Claro nice trips | Agencia de viajes</title>
<link rel="shortcut icon" href="/favicon.ico">
</head>
<body>
<div id="center"align="center">
	<div id="headercontent">
	  <div id="header">
			<div id="logo"><a href="index.htm"><img src="images/logo-claro.gif" alt="Claro nice tripsa ,gencia de viajes" width="173" height="88" border="0"></a></div>
	        <!-- InstanceBeginEditable name="botonera" -->
	        <div id="boton2">
              <div id="texto-boton">
                <h4><a href="claro-agencia-de-viajes.htm">Claro nicetrips</a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="destinos.htm">Nuestros destinos </a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="servicios-a-empresas.htm">Servicios a empresas </a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="reservas-online.htm">Reservas
                <br>online </a></h4>
              </div>
              <div id="texto-boton">
                <h4><a href="contacto.php">Contacto</a></h4>
              </div>
            </div>
	        <!-- InstanceEndEditable --></div>
	</div>
<div id="wrapper">

<!-- InstanceBeginEditable name="content" -->  
<div id="contenido-a">
	<div id="col2-1a-sinmargen">
		
        <div id="foto1"><p>
		<?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/panel/img/".$_GET["id"]."-foto1.jpg")) {?>        
        <a href="#" target="_blank"><img src="http://www.claronicetrips.com/panel/img/<?=$_GET["id"];?>-foto1.jpg" alt="<?php echo $row_Recordset1['titulo']; ?> <?php echo $row_Recordset1['provincia']; ?>" width="200" border="0"></a>
        <?php } ?>  
        </p></div>          
        
        <div id="foto2"><p>
		<?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/panel/img/".$_GET["id"]."-foto2.jpg")) {?>
        <a href="#" target="_blank"><img src="http://www.claronicetrips.com/panel/img/<?=$_GET["id"];?>-foto2.jpg" alt="<?php echo $row_Recordset1['titulo']; ?> <?php echo $row_Recordset1['provincia']; ?>" width="200" border="0"></a>
        <?php } ?> 
        </p></div>
        
        <div id="foto3"><p>
		<?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/panel/img/".$_GET["id"]."-foto3.jpg")) {?>
        <a href="#" target="_blank"><img src="http://www.claronicetrips.com/panel/img/<?=$_GET["id"];?>-foto3.jpg" alt="<?php echo $row_Recordset1['titulo']; ?> <?php echo $row_Recordset1['provincia']; ?>" width="200" border="0"></a>
        <?php } ?> 
        </p></div>
        
        <div id="foto-logo"><p>
		<?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/panel/img/".$_GET["id"]."-logo.jpg")) {?>
        <a href="#" target="_blank"><img src="http://www.claronicetrips.com/panel/img/<?=$_GET["id"];?>-logo.jpg" alt="<?php echo $row_Recordset1['titulo']; ?>" width="200" border="0"></a>
        <?php } ?> 
        </p></div>
        
	</div>
	<div id="col2-1b">
		<div id="provincia"><?php echo $row_Recordset1['provincia']; ?></div>
		<div id="titulo"><?php echo $row_Recordset1['titulo']; ?></div>
		<div id="resumen"><?php echo $row_Recordset1['subtitulo']; ?></div>
		<div id="info"><a href="javascript:history.back()">< Volver &nbsp;&nbsp;</a> |&nbsp;&nbsp; <a href="javascript:window.print();"><img src="images/printer-ico.gif" width="14" height="14" border="0">&nbsp;&nbsp; Imprimir</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="ficha-pdf.php?id=<?=$_GET["id"]?>" target="_blank">Ver PDF</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="contacto.php">Solicitar presupuesto </a></div>
		<div id="descripcion">
		  <p><?php echo $row_Recordset1['descripcion']; ?></p>
		</div>
		<div id="fechas"><?php echo $row_Recordset1['otros']; ?></div>
		<div id="otros"></div>
	  </div>
	<div id="col3-3-sinmargen">
	  <img src="images/todos-los-destinos.jpg" width="216" height="377" border="0" usemap="#Map">
      <map name="Map">
        <area shape="rect" coords="26,241,106,257" href="http://www.claronicetrips.com/viajes.php?continente=cruceros">
        <area shape="rect" coords="26,140,100,159" href="http://www.claronicetrips.com/viajes.php?continente=europa">
        <area shape="rect" coords="25,158,99,176" href="http://www.claronicetrips.com/viajes.php?continente=africa">
        <area shape="rect" coords="26,175,100,193" href="http://www.claronicetrips.com/viajes.php?continente=america">
        <area shape="rect" coords="26,192,100,209" href="http://www.claronicetrips.com/viajes.php?continente=asia">
        <area shape="rect" coords="26,208,100,225" href="http://www.claronicetrips.com/viajes.php?continente=oceania">
        <area shape="rect" coords="26,224,165,242" href="http://www.claronicetrips.com/viajes.php?continente=mundo">
      </map>
	  </div>
</div>
<!-- InstanceEndEditable -->
<!--contenido2-->

</div>
</div>
<!--pie de pagina-->
<div id="pie1">
	<div align="center">
	<div id="contenido2">
	  <div id="col-202-right">
		  	<div id="cabeceras-noticias"><a href="tahiti.htm"><img src="images/tahiti.gif" alt="tahiti" width="190" height="18" border="0"></a></div>
			Sin duda alguna <strong>Tahit&iacute; </strong>es el paraiso para las mejores vacaciones.
			<br>
			<a href="tahiti.htm" >M�s informaci�n</a>
			<br><br>
			<a href="tahiti.htm"><img src="images/tahiti.jpg" alt="tahiti" width="190" height="50" border="0"></a>
	  </div>
	  <div id="col-202-right">
		  	<div id="cabeceras-noticias"><a href="canada.htm"><img src="images/canada.gif" alt="canada" width="190" height="18" border="0"></a></div>
			<strong>Montreal, Toronto, Vancouver...</strong>
			<br>
			Reserva de vacaciones 2009.<br>
			<a href="canada.htm" >M�s informaci�n</a>
			<br><br>
		    <a href="canada.htm"><img src="images/avion-airtransat.jpg" alt="canada" width="190" height="50" border="0"></a>
		</div>
	  <div id="col-202-right">
		  	<div id="cabeceras-noticias"><a href="http://www.claronicetrips.com/viajes.php?continente=disneyworld"><img src="images/walt-disney.gif" alt="walt disney world" width="190" height="18" border="0"></a></div>
			Informaci&oacute;n y reservas para <strong>Orlando-Walt Disney World
			</strong>
			<br>
			<a href="http://www.claronicetrips.com/viajes.php?continente=disneyworld">Ver viajes</a>
			<br><br>
		    <a href="http://www.claronicetrips.com/viajes.php?continente=disneyworld"><img src="images/orlando.jpg" alt="walt disney world" width="190" height="50" border="0"></a>
		</div>
	  <div id="col-202-right">
		  	<div id="cabeceras-noticias"><a href="http://www.claronicetrips.com/viajes.php?continente=puentes"><img src="images/escapadas.gif" alt="escapadas" width="190" height="18" border="0"></a></div>
			Puentes, escapadas, etc.
			<br>
			Propuestas para <strong>Semana Santa
			</strong>
			<br>
			<a href="http://www.claronicetrips.com/viajes.php?continente=puentes">Ver propuestas</a>
			<br><br>
		    <a href="http://www.claronicetrips.com/viajes.php?continente=puentes"><img src="images/escapadas.jpg" alt="escapadas" width="190" height="50" border="0"></a>
		</div>
	<div>&nbsp;</div>
	</div>
</div>
</div>
	
	<div id="pie2">
	<div align="center">
		<div id="contenido-pie">
			<div id="contenido-pie1">
			  <h5><a href="index.htm">Inicio</a> | <a href="claro-agencia-de-viajes.htm">Claro nice Trips</a> | <a href="destinos.htm">Destinos</a> | <a href="servicios-a-empresas.htm">Empresas</a> | <a href="reservas-online.htm">Reservas on line </a> | <a href="contacto.php">Contacto </a></h5>
			</div>
			<div id="contenido-pie2">
			  <h5>&copy; 2008 Claro nice trips SL
                <br>
			Todos los derechos reservados | <a href="condiciones.htm">Legal</a> </h5>
			</div>
	  </div>	
		<div>&nbsp;</div>			
	</div>	
</div>
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($Recordset1);
?>
