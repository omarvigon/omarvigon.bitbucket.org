<?php
if (!isset($_SESSION)) {
  session_start();}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>

<?php 
if($_GET["borrar"])
	unlink($_SERVER["DOCUMENT_ROOT"]."/panel/img/".$_GET["borrar"]);
	
function redimensionar($nombre)
{
	$ruta=$_SERVER["DOCUMENT_ROOT"]."/panel/img/".$_POST["queid"]."-".$nombre.".jpg";
	
	if(copy($_FILES[$nombre]["tmp_name"],$ruta))
	{
		list($width, $height) = getimagesize($ruta);
		if($nombre=="logo")
		{
			$newwidth = 80;
			$newheight = floor((80 * $height) / $width);
		}
		else
		{
			$newwidth = 200;
			$newheight = floor((200 * $height) / $width);
		}
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($ruta);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagejpeg($thumb,$ruta);
		imagedestroy($thumb);
		return true;
	}
	else
		return false;
}

if($_FILES["foto1"]["type"]=="image/jpeg" || $_FILES["foto1"]["type"]=="image/pjpeg")
	redimensionar("foto1");
if($_FILES["foto2"]["type"]=="image/jpeg" || $_FILES["foto2"]["type"]=="image/pjpeg")
	redimensionar("foto2");
if($_FILES["foto3"]["type"]=="image/jpeg" || $_FILES["foto3"]["type"]=="image/pjpeg")
	redimensionar("foto3");
if($_FILES["logo"]["type"]=="image/jpeg" || $_FILES["logo"]["type"]=="image/pjpeg")
	redimensionar("logo");	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Panel de Control</title>
<link rel="stylesheet" href="_estilos.css" type="text/css" />
</head>

<body>
<img src="http://www.claronicetrips.com/images/logo-claro.gif" alt="">
<h2>Fotos "<?=$_GET["titulo"]?>"</h2>
<p><a href="inicio.php">Volver al inicio</a></p>

<form name="archivos" method="post" action="fotos.php?id=<?=$_GET["id"]?>" enctype="multipart/form-data">
<input type="hidden" name="queid" value="<?=$_GET["id"]?>" />
<table cellpadding="4" cellspacing="1" align="center">
    <tr>
      <td nowrap="nowrap" align="right">Foto1: (.jpg)</td>
      <td>
      	<?php if(file_exists("img/".$_GET["id"]."-foto1.jpg"))
			echo '<img title="" src="img/'.$_GET["id"].'-foto1.jpg" align="absmiddle" alt="" /> <a href="fotos.php?id='.$_GET["id"].'&borrar='.$_GET["id"].'-foto1.jpg">BORRAR</a>';
		 else
      		echo '<input type="file" name="foto1" size="96" />';
         ?></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Foto2: (.jpg)</td>
      <td>
      	<?php if(file_exists("img/".$_GET["id"]."-foto2.jpg"))
			echo '<img title="" src="img/'.$_GET["id"].'-foto2.jpg" align="absmiddle" alt="" /> <a href="fotos.php?id='.$_GET["id"].'&borrar='.$_GET["id"].'-foto2.jpg">BORRAR</a>';
		 else
      		echo '<input type="file" name="foto2" size="96" />';
         ?></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Foto3: (.jpg)</td>
      <td><?php if(file_exists("img/".$_GET["id"]."-foto3.jpg"))
			echo '<img title="" src="img/'.$_GET["id"].'-foto3.jpg" align="absmiddle" alt="" /> <a href="fotos.php?id='.$_GET["id"].'&borrar='.$_GET["id"].'-foto3.jpg">BORRAR</a>';
		 else
      		echo '<input type="file" name="foto3" size="96" />';
         ?></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Logo: (.jpg)</td>
      <td><?php if(file_exists("img/".$_GET["id"]."-logo.jpg"))
			echo '<img title="" src="img/'.$_GET["id"].'-logo.jpg" align="absmiddle" alt="" /> <a href="fotos.php?id='.$_GET["id"].'&borrar='.$_GET["id"].'-logo.jpg">BORRAR</a>';
		 else
      		echo '<input type="file" name="logo" size="96" />';
         ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center"><input type="submit" value="Subir imagenes" /></td>
    </tr>
</table>
</form>
</body>
</html>
