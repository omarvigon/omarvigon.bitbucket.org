<?php require_once('../Connections/claronicetrips.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE viaje SET titulo=%s, subtitulo=%s, clase=%s, descripcion=%s, continente=%s, pais=%s, provincia=%s, otros=%s WHERE id=%s",
                       GetSQLValueString($_POST['titulo'], "text"),
                       GetSQLValueString($_POST['subtitulo'], "text"),
					   GetSQLValueString($_POST['clase'], "text"),
                       GetSQLValueString($_POST['descripcion'], "text"),
                       GetSQLValueString($_POST['continente'], "text"),
                       GetSQLValueString($_POST['pais'], "text"),
                       GetSQLValueString($_POST['provincia'], "text"),
                       GetSQLValueString($_POST['otros'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_claronicetrips, $claronicetrips);
  $Result1 = mysql_query($updateSQL, $claronicetrips) or die(mysql_error());

  $updateGoTo = "inicio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_claronicetrips, $claronicetrips);
$query_Recordset1 = sprintf("SELECT * FROM viaje WHERE id = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $claronicetrips) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" type="text/javascript" src="editor_tiny/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({mode : "textareas", theme : "simple" });
</script>
<title>Panel de control</title>
<link rel="stylesheet" href="_estilos.css" type="text/css" />
</head>

<body>
<img src="http://www.claronicetrips.com/images/logo-claro.gif" alt="">
<h2>Modificar viaje</h2>
<p><a href="inicio.php">Volver al inicio</a></p>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
<table cellpadding="4" cellspacing="1" align="center">
<tr>
      <td nowrap="nowrap" align="right">Titulo:</td>
      <td><input type="text" name="titulo" value="<?php echo htmlentities($row_Recordset1['titulo'], ENT_COMPAT, 'utf-8'); ?>" class="caja1"  /></td>
</tr>
<tr>      
      <td nowrap="nowrap" align="right">Subtitulo:</td>
      <td><input type="text" name="subtitulo" value="<?php echo htmlentities($row_Recordset1['subtitulo'], ENT_COMPAT, 'utf-8'); ?>" class="caja1"  /></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Clase:</td>
      <td><select name="clase" class="caja1">    	
      	<option value="Itinerarios" <?php if (!(strcmp("Itinerarios", $row_Recordset1['clase']))) {echo "selected=\"selected\"";} ?>>Itinerarios</option>
        <option value="Estancias" <?php if (!(strcmp("Estancias", $row_Recordset1['clase']))) {echo "selected=\"selected\"";} ?>>Estancias</option>
        </select></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Tipo:</td>
      <td><select name="continente" class="caja1">
        <option value="europa" <?php if (!(strcmp("europa", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Europa</option>
        <option value="africa" <?php if (!(strcmp("africa", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Africa</option>
        <option value="asia" <?php if (!(strcmp("asia", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Asia</option>
        <option value="america" <?php if (!(strcmp("america", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>America</option>
        <option value="oceania" <?php if (!(strcmp("oceania", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Oceania</option>
        <option value="mundo" <?php if (!(strcmp("mundo", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Vuelta al mundo</option>
		<option value="puentes" <?php if (!(strcmp("puentes", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Puentes</option>
		<option value="invierno" <?php if (!(strcmp("invierno", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Invierno</option>
        <option value="cruceros" <?php if (!(strcmp("cruceros", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Cruceros</option>
        <option value="disneyworld" <?php if (!(strcmp("disneyworld", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Disneyworld</option>
        <option value="balnearios" <?php if (!(strcmp("balnearios", $row_Recordset1['continente']))) {echo "selected=\"selected\"";} ?>>Balnearios</option>
        </select></td>
</tr>
<tr>        
      <td nowrap="nowrap" align="right">Pais:</td>
      <td><select name="pais" class="caja1">
        <option value="AF" <?php if (!(strcmp("AF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Afganistan</option>
        <option value="AL" <?php if (!(strcmp("AL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Albania</option>
        <option value="DE" <?php if (!(strcmp("DE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Alemania </option>
        <option value="AD" <?php if (!(strcmp("AD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Andorra</option>
		<option value="AO" <?php if (!(strcmp("AO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Angola</option>
        <option value="AI" <?php if (!(strcmp("AI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Anguila</option>
        <option value="AQ" <?php if (!(strcmp("AQ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Antartida</option>
        <option value="AG" <?php if (!(strcmp("AG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Antigua y Barbuda</option>
        <option value="AN" <?php if (!(strcmp("AN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Antillas Neerlandesas</option>
        <option value="SA" <?php if (!(strcmp("SA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Arabia Saudi</option>
        <option value="DZ" <?php if (!(strcmp("DZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Argelia</option>
        <option value="AR" <?php if (!(strcmp("AR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Argentina</option>
        <option value="AM" <?php if (!(strcmp("AM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Armenia</option>
        <option value="AW" <?php if (!(strcmp("AW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Aruba</option>
        <option value="AU" <?php if (!(strcmp("AU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Australia</option>
<option value="AT" <?php if (!(strcmp("AT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Austria</option>
        <option value="AZ" <?php if (!(strcmp("AZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Azerbaiyan</option>
        <option value="BS" <?php if (!(strcmp("BS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bahamas</option>
        <option value="BH" <?php if (!(strcmp("BH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bahrain</option>
        <option value="BD" <?php if (!(strcmp("BD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bangladesh</option>
        <option value="BB" <?php if (!(strcmp("BB", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Barbados</option>
        <option value="BE" <?php if (!(strcmp("BE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Belgica</option>
        <option value="BZ" <?php if (!(strcmp("BZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Belice</option>
        <option value="BJ" <?php if (!(strcmp("BJ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Benin</option>
        <option value="BM" <?php if (!(strcmp("BM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bermudas</option>
        <option value="BY" <?php if (!(strcmp("BY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bielorrusia</option>
        <option value="BO" <?php if (!(strcmp("BO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bolivia</option>
        <option value="BA" <?php if (!(strcmp("BA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bosnia y Hercegovina</option>
        <option value="BW" <?php if (!(strcmp("BW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Botsuana</option>
        <option value="BR" <?php if (!(strcmp("BR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Brasil</option>
        <option value="BN" <?php if (!(strcmp("BN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Brunei</option>
        <option value="BG" <?php if (!(strcmp("BG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Bulgaria</option>
        <option value="BF" <?php if (!(strcmp("BF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Burkina Faso</option>
        <option value="BI" <?php if (!(strcmp("BI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Burundi</option>
        <option value="BT" <?php if (!(strcmp("BT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Butan</option>
        <option value="CV" <?php if (!(strcmp("CV", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Cabo Verde</option>
        <option value="KH" <?php if (!(strcmp("KH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Camboya</option>
        <option value="CM" <?php if (!(strcmp("CM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Camerun</option>
        <option value="CA" <?php if (!(strcmp("CA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Canada</option>
        <option value="TD" <?php if (!(strcmp("TD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Chad</option>
        <option value="CL" <?php if (!(strcmp("CL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Chile</option>
        <option value="CN" <?php if (!(strcmp("CN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>China</option>
        <option value="CY" <?php if (!(strcmp("CY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Chipre</option>
        <option value="VA" <?php if (!(strcmp("VA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Ciudad del Vaticano</option>
        <option value="CO" <?php if (!(strcmp("CO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Colombia</option>
        <option value="KM" <?php if (!(strcmp("KM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Comores</option>
        <option value="CG" <?php if (!(strcmp("CG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Congo</option>
        <option value="CD" <?php if (!(strcmp("CD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Congo, Republica Democratica del</option>
        <option value="KP" <?php if (!(strcmp("KP", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Corea del Norte</option>
        <option value="KR" <?php if (!(strcmp("KR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Corea del Sur</option>
        <option value="CI" <?php if (!(strcmp("CI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Costa de Marfil</option>
        <option value="CR" <?php if (!(strcmp("CR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Costa Rica</option>
<option value="HR" <?php if (!(strcmp("HR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Croacia</option>
        <option value="CU" <?php if (!(strcmp("CU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Cuba</option>
        <option value="DK" <?php if (!(strcmp("DK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Dinamarca</option>
        <option value="DM" <?php if (!(strcmp("DM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Dominica</option>
        <option value="EC" <?php if (!(strcmp("EC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Ecuador</option>
        <option value="EG" <?php if (!(strcmp("EG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Egipto</option>
        <option value="SV" <?php if (!(strcmp("SV", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>El Salvador</option>
        <option value="AE" <?php if (!(strcmp("AE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Emiratos arabes Unidos</option>
        <option value="ER" <?php if (!(strcmp("ER", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Eritrea</option>
        <option value="SK" <?php if (!(strcmp("SK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Eslovaquia</option>
        <option value="SI" <?php if (!(strcmp("SI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Eslovenia</option>
        <option value="ES" <?php if (!(strcmp("ES", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>> Espa&ntilde;a</option>
        <option value="US" <?php if (!(strcmp("US", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Estados Unidos</option>
        <option value="EE" <?php if (!(strcmp("EE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Estonia</option>
        <option value="ET" <?php if (!(strcmp("ET", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Etiopia</option>
        <option value="PH" <?php if (!(strcmp("PH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Filipinas</option>
        <option value="FI" <?php if (!(strcmp("FI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Finlandia</option>
        <option value="FJ" <?php if (!(strcmp("FJ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Fiji</option>
        <option value="FR" <?php if (!(strcmp("FR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Francia</option>
        <option value="GA" <?php if (!(strcmp("GA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Gabon</option>
        <option value="GM" <?php if (!(strcmp("GM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Gambia</option>
        <option value="GE" <?php if (!(strcmp("GE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Georgia</option>
<option value="GH" <?php if (!(strcmp("GH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Ghana</option>
        <option value="GI" <?php if (!(strcmp("GI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Gibraltar</option>
        <option value="GD" <?php if (!(strcmp("GD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Granada</option>
        <option value="GR" <?php if (!(strcmp("GR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Grecia</option>
        <option value="GL" <?php if (!(strcmp("GL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Groenlandia</option>
        <option value="GP" <?php if (!(strcmp("GP", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guadalupe</option>
        <option value="GU" <?php if (!(strcmp("GU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guam</option>
<option value="GT" <?php if (!(strcmp("GT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guatemala</option>
        <option value="GF" <?php if (!(strcmp("GF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guayana Francesa</option>
        <option value="GG" <?php if (!(strcmp("GG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guernsey</option>
        <option value="GN" <?php if (!(strcmp("GN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guinea</option>
        <option value="GW" <?php if (!(strcmp("GW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guinea-Bissau</option>
        <option value="GQ" <?php if (!(strcmp("GQ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guinea Ecuatorial</option>
        <option value="GY" <?php if (!(strcmp("GY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Guyana</option>
        <option value="HT" <?php if (!(strcmp("HT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Haiti</option>
        <option value="HN" <?php if (!(strcmp("HN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Honduras</option>
        <option value="HK" <?php if (!(strcmp("HK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Hong Kong</option>
        <option value="HU" <?php if (!(strcmp("HU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Hungria</option>
        <option value="IN" <?php if (!(strcmp("IN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>India</option>
        <option value="ID" <?php if (!(strcmp("ID", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Indonesia</option>
        <option value="IR" <?php if (!(strcmp("IR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Iran</option>
        <option value="IQ" <?php if (!(strcmp("IQ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Iraq</option>
<option value="IE" <?php if (!(strcmp("IE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Irlanda</option>
        <option value="BV" <?php if (!(strcmp("BV", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Isla Bouvet</option>
        <option value="CX" <?php if (!(strcmp("CX", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Isla Christmas</option>
        <option value="IM" <?php if (!(strcmp("IM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Isla de Man</option>
        <option value="IS" <?php if (!(strcmp("IS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islandia</option>
        <option value="NF" <?php if (!(strcmp("NF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Isla Norfolk</option>
        <option value="AX" <?php if (!(strcmp("AX", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Aland</option>
        <option value="KY" <?php if (!(strcmp("KY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Caiman</option>
        <option value="CC" <?php if (!(strcmp("CC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Cocos</option>
        <option value="CK" <?php if (!(strcmp("CK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Cook</option>
        <option value="FO" <?php if (!(strcmp("FO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Feroe</option>
<option value="GS" <?php if (!(strcmp("GS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Georgia del Sur y Sandwich del Sur</option>
        <option value="HM" <?php if (!(strcmp("HM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Heard y McDonald</option>
        <option value="FK" <?php if (!(strcmp("FK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Malvinas</option>
        <option value="MP" <?php if (!(strcmp("MP", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Mariana del Norte</option>
        <option value="MH" <?php if (!(strcmp("MH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Marshall</option>
        <option value="UM" <?php if (!(strcmp("UM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas menores alejadas de los Estados Unidos</option>
        <option value="PN" <?php if (!(strcmp("PN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Pitcairn</option>
        <option value="SB" <?php if (!(strcmp("SB", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Salomon</option>
        <option value="SJ" <?php if (!(strcmp("SJ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Svalbard y Jan Mayen</option>
        <option value="TC" <?php if (!(strcmp("TC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Turcas y Caicos</option>
        <option value="VI" <?php if (!(strcmp("VI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Virgenes, EE.UU.</option>
        <option value="VG" <?php if (!(strcmp("VG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Islas Virgenes Britanicas</option>
        <option value="IL" <?php if (!(strcmp("IL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Israel</option>
        <option value="IT" <?php if (!(strcmp("IT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Italia</option>
        <option value="JM" <?php if (!(strcmp("JM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Jamaica</option>
        <option value="JP" <?php if (!(strcmp("JP", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Japon</option>
        <option value="JE" <?php if (!(strcmp("JE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Jersey</option>
        <option value="JO" <?php if (!(strcmp("JO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Jordania</option>
        <option value="KZ" <?php if (!(strcmp("KZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Kazajistan</option>
        <option value="KE" <?php if (!(strcmp("KE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Kenia</option>
<option value="KG" <?php if (!(strcmp("KG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Kirguizistan</option>
        <option value="KI" <?php if (!(strcmp("KI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Kiribati</option>
        <option value="KW" <?php if (!(strcmp("KW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Kuwait</option>
        <option value="LA" <?php if (!(strcmp("LA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Laos</option>
        <option value="LS" <?php if (!(strcmp("LS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Lesoto</option>
        <option value="LV" <?php if (!(strcmp("LV", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Letonia</option>
        <option value="LB" <?php if (!(strcmp("LB", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Libano</option>
        <option value="LR" <?php if (!(strcmp("LR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Liberia</option>
        <option value="LY" <?php if (!(strcmp("LY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Libia</option>
        <option value="LI" <?php if (!(strcmp("LI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Liechtenstein</option>
<option value="LT" <?php if (!(strcmp("LT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Lituania</option>
        <option value="LU" <?php if (!(strcmp("LU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Luxemburgo</option>
        <option value="MO" <?php if (!(strcmp("MO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Macao</option>
        <option value="MK" <?php if (!(strcmp("MK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Macedonia</option>
        <option value="MG" <?php if (!(strcmp("MG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Madagascar</option>
        <option value="MY" <?php if (!(strcmp("MY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Malasia</option>
        <option value="MW" <?php if (!(strcmp("MW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Malaui</option>
        <option value="MV" <?php if (!(strcmp("MV", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Maldivas</option>
        <option value="ML" <?php if (!(strcmp("ML", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mali</option>
        <option value="MT" <?php if (!(strcmp("MT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Malta</option>
        <option value="MA" <?php if (!(strcmp("MA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Marruecos</option>
        <option value="MQ" <?php if (!(strcmp("MQ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Martinica</option>
        <option value="MU" <?php if (!(strcmp("MU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mauricio</option>
        <option value="MR" <?php if (!(strcmp("MR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mauritania</option>
        <option value="YT" <?php if (!(strcmp("YT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mayotte</option>
        <option value="MX" <?php if (!(strcmp("MX", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mexico</option>
        <option value="FM" <?php if (!(strcmp("FM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Micronesia</option>
        <option value="MD" <?php if (!(strcmp("MD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Moldavia</option>
        <option value="MC" <?php if (!(strcmp("MC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Monaco</option>
        <option value="MN" <?php if (!(strcmp("MN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mongolia</option>
        <option value="ME" <?php if (!(strcmp("ME", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Montenegro</option>
        <option value="MS" <?php if (!(strcmp("MS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Montserrat</option>
        <option value="MZ" <?php if (!(strcmp("MZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Mozambique</option>
        <option value="MM" <?php if (!(strcmp("MM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Myanmar (Birmania)</option>
        <option value="NA" <?php if (!(strcmp("NA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Namibia</option>
        <option value="NR" <?php if (!(strcmp("NR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Nauru</option>
        <option value="NP" <?php if (!(strcmp("NP", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Nepal</option>
        <option value="NI" <?php if (!(strcmp("NI", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Nicaragua</option>
        <option value="NE" <?php if (!(strcmp("NE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Niger</option>
        <option value="NG" <?php if (!(strcmp("NG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Nigeria</option>
        <option value="NU" <?php if (!(strcmp("NU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Niue</option>
        <option value="NO" <?php if (!(strcmp("NO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Noruega</option>
        <option value="NC" <?php if (!(strcmp("NC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Nueva Caledonia</option>
        <option value="NZ" <?php if (!(strcmp("NZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Nueva Zelanda</option>
        <option value="OM" <?php if (!(strcmp("OM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Oman</option>
<option value="NL" <?php if (!(strcmp("NL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Paises Bajos</option>
<option value="PK" <?php if (!(strcmp("PK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Pakistan</option>
<option value="PW" <?php if (!(strcmp("PW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Palaos (Belau)</option>
        <option value="PA" <?php if (!(strcmp("PA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Panama</option>
        <option value="PG" <?php if (!(strcmp("PG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Papua-Nueva Guinea</option>
        <option value="PY" <?php if (!(strcmp("PY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Paraguay</option>
        <option value="PE" <?php if (!(strcmp("PE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Peru</option>
<option value="PF" <?php if (!(strcmp("PF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Polinesia Francesa</option>
        <option value="PL" <?php if (!(strcmp("PL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Polonia</option>
        <option value="PT" <?php if (!(strcmp("PT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Portugal</option>
        <option value="PR" <?php if (!(strcmp("PR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Puerto Rico</option>
        <option value="QA" <?php if (!(strcmp("QA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Qatar</option>
<option value="GB" <?php if (!(strcmp("GB", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Reino Unido</option>
        <option value="CF" <?php if (!(strcmp("CF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Republica Centroafricana</option>
        <option value="CZ" <?php if (!(strcmp("CZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Republica Checa</option>
        <option value="DO" <?php if (!(strcmp("DO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Republica Dominicana</option>
        <option value="RE" <?php if (!(strcmp("RE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Reunion</option>
        <option value="RW" <?php if (!(strcmp("RW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Ruanda</option>
<option value="RO" <?php if (!(strcmp("RO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Rumania</option>
        <option value="RU" <?php if (!(strcmp("RU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Rusia</option>
        <option value="EH" <?php if (!(strcmp("EH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Sahara Occidental</option>
        <option value="WS" <?php if (!(strcmp("WS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Samoa</option>
        <option value="AS" <?php if (!(strcmp("AS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Samoa americana</option>
        <option value="KN" <?php if (!(strcmp("KN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>San Cristobal y Nieves</option>
        <option value="SM" <?php if (!(strcmp("SM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>San Marino</option>
        <option value="PM" <?php if (!(strcmp("PM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>San Pedro y Miquelon</option>
        <option value="SH" <?php if (!(strcmp("SH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Santa Elena</option>
        <option value="LC" <?php if (!(strcmp("LC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Santa Lucia</option>
        <option value="ST" <?php if (!(strcmp("ST", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Santo Tome y Principe</option>
        <option value="VC" <?php if (!(strcmp("VC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>San Vicente y las Granadinas</option>
        <option value="SN" <?php if (!(strcmp("SN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Senegal</option>
        <option value="RS" <?php if (!(strcmp("RS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Serbia</option>
        <option value="CS" <?php if (!(strcmp("CS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Serbia y Montenegro</option>
        <option value="SC" <?php if (!(strcmp("SC", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Seychelles</option>
        <option value="SL" <?php if (!(strcmp("SL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Sierra Leona</option>
        <option value="SG" <?php if (!(strcmp("SG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Singapur</option>
        <option value="SY" <?php if (!(strcmp("SY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Siria</option>
        <option value="SO" <?php if (!(strcmp("SO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Somalia</option>
        <option value="LK" <?php if (!(strcmp("LK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Sri Lanka</option>
        <option value="SZ" <?php if (!(strcmp("SZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Suazilandia</option>
<option value="ZA" <?php if (!(strcmp("ZA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Sudafrica</option>
<option value="SD" <?php if (!(strcmp("SD", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Sudan</option>
<option value="SE" <?php if (!(strcmp("SE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Suecia</option>
        <option value="CH" <?php if (!(strcmp("CH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Suiza</option>
        <option value="SR" <?php if (!(strcmp("SR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Surinam</option>
        <option value="TH" <?php if (!(strcmp("TH", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tailandia</option>
        <option value="TW" <?php if (!(strcmp("TW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Taiwan</option>
        <option value="TZ" <?php if (!(strcmp("TZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tanzania</option>
        <option value="TJ" <?php if (!(strcmp("TJ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tayikistan</option>
        <option value="IO" <?php if (!(strcmp("IO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Territorio Britanico del Oceano indico</option>
<option value="PS" <?php if (!(strcmp("PS", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Territorio Palestino</option>
        <option value="TF" <?php if (!(strcmp("TF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Territorios Australes Franceses</option>
        <option value="TL" <?php if (!(strcmp("TL", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Timor Oriental</option>
        <option value="TG" <?php if (!(strcmp("TG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Togo</option>
        <option value="TK" <?php if (!(strcmp("TK", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tokelau</option>
        <option value="TO" <?php if (!(strcmp("TO", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tonga</option>
        <option value="TT" <?php if (!(strcmp("TT", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Trinidad y Tobago</option>
        <option value="TN" <?php if (!(strcmp("TN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tunez</option>
<option value="TM" <?php if (!(strcmp("TM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Turkmenistan</option>
        <option value="TR" <?php if (!(strcmp("TR", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Turquia</option>
        <option value="TV" <?php if (!(strcmp("TV", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Tuvalu</option>
        <option value="UA" <?php if (!(strcmp("UA", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Ucrania</option>
        <option value="UG" <?php if (!(strcmp("UG", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Uganda</option>
        <option value="UY" <?php if (!(strcmp("UY", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Uruguay</option>
        <option value="UZ" <?php if (!(strcmp("UZ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Uzbekistan</option>
        <option value="VU" <?php if (!(strcmp("VU", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Vanuatu</option>
        <option value="VE" <?php if (!(strcmp("VE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Venezuela</option>
        <option value="VN" <?php if (!(strcmp("VN", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Vietnam</option>
        <option value="VM" <?php if (!(strcmp("VM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Vuelta al mundo</option>
        <option value="WF" <?php if (!(strcmp("WF", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Wallis y Futuna</option>
        <option value="YE" <?php if (!(strcmp("YE", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Yemen</option>
        <option value="DJ" <?php if (!(strcmp("DJ", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Yibuti</option>
        <option value="ZM" <?php if (!(strcmp("ZM", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Zambia</option>
<option value="ZW" <?php if (!(strcmp("ZW", $row_Recordset1['pais']))) {echo "selected=\"selected\"";} ?>>Zimbabue</option>
	  </select></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Provincia:</td>
      <td><input type="text" name="provincia" value="<?php echo htmlentities($row_Recordset1['provincia'], ENT_COMPAT, 'utf-8'); ?>" class="caja1"  /></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Descripcion:</td>
      <td><textarea name="descripcion" rows="12"><?php echo $row_Recordset1['descripcion']; ?></textarea></td>
</tr>
 <tr>
      <td nowrap="nowrap" align="right">Otros:</td>
      <td><textarea name="otros"><?php echo $row_Recordset1['otros']; ?></textarea></td>
</tr>
<tr>
     <td colspan="2" align="center"><input type="submit" value="Modificar" /></td>
</tr>
</table>
<input type="hidden" name="MM_update" value="form1" />
<input type="hidden" name="id" value="<?php echo $row_Recordset1['id']; ?>" />
</form>

</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
