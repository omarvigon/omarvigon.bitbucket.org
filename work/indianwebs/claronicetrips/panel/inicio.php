<?php require_once('../Connections/claronicetrips.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!isset($_SESSION)) {
  session_start();}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_claronicetrips, $claronicetrips);
$query_Recordset1 = "SELECT id, titulo,subtitulo,clase, continente, pais, provincia FROM viaje ORDER BY continente ASC";
$Recordset1 = mysql_query($query_Recordset1, $claronicetrips) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Panel de Control</title>
<link rel="stylesheet" href="_estilos.css" type="text/css" />
</head>

<body>
<img src="http://www.claronicetrips.com/images/logo-claro.gif" alt="">
<h2><?php echo $totalRows_Recordset1 ?> Viajes</h2>
<p><a href="insertar.php">[+] Insertar nuevo viaje</a> | <a href="usuarios.php">[+] Ver usuarios</a> | <a href="usuarios-email.php">[+] Ver emails</a> | <a href="mailing.php">[+] Enviar mailing</a> | <a href="<?php echo $logoutAction ?>">[+] Cerrar Sesion</a></p>

<table cellpadding="4" cellspacing="1" class="tabla1">
  <tr>
    <td class="cab">Titulo</td>
    <td class="cab">Subtitulo</td>
    <td class="cab">Clase</td>
    <td class="cab">Tipo</td>
    <td class="cab">Pais</td>
    <td class="cab">Provincia</td>
    <td class="cab" colspan="3">ACCIONES</td>
  </tr>
  <?php do { ?>
    <tr>
      <td class="cab2"><?php echo $row_Recordset1['titulo']; ?></td>
      <td class="cab2" width="300"><?php echo $row_Recordset1['subtitulo']; ?></td>
      <td class="cab2"><?php echo $row_Recordset1['clase']; ?></td>
      <td class="cab2"><?php echo $row_Recordset1['continente']; ?></td>
      <td class="cab2"><?php echo $row_Recordset1['pais']; ?></td>
      <td class="cab2"><?php echo $row_Recordset1['provincia']; ?></td>    
      <td class="cab2"><a href="fotos.php?id=<?php echo $row_Recordset1['id']; ?>&titulo=<?php echo $row_Recordset1['titulo']; ?>">FOTOS</a></td>
      <td class="cab2"><a href="modificar.php?id=<?php echo $row_Recordset1['id']; ?>">MODIFICAR</a></td>
      <td class="cab2"><a href="eliminar.php?id=<?php echo $row_Recordset1['id']; ?>">BORRAR</a></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
