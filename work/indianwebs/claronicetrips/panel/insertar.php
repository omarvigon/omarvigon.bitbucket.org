<?php require_once('../Connections/claronicetrips.php'); ?>
<?php
if (!isset($_SESSION)) {session_start();}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO viaje (titulo, subtitulo, clase, descripcion, continente, pais, provincia, otros) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['titulo'], "text"),
                       GetSQLValueString($_POST['subtitulo'], "text"),
					   GetSQLValueString($_POST['clase'], "text"),
                       GetSQLValueString($_POST['descripcion'], "text"),
                       GetSQLValueString($_POST['continente'], "text"),
                       GetSQLValueString($_POST['pais'], "text"),
                       GetSQLValueString($_POST['provincia'], "text"),
                       GetSQLValueString($_POST['otros'], "text"));

  mysql_select_db($database_claronicetrips, $claronicetrips);
  $Result1 = mysql_query($insertSQL, $claronicetrips) or die(mysql_error());

  $insertGoTo = "inicio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="_estilos.css" type="text/css" />
<script language="javascript" type="text/javascript" src="editor_tiny/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({mode : "textareas", theme : "simple" });
</script>
<title>Panel de control</title>
</head>

<body>
<img src="http://www.claronicetrips.com/images/logo-claro.gif" alt="">
<h2>Nuevo viaje</h2>
<p><a href="inicio.php">Volver al inicio</a></p>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
<table cellpadding="4" cellspacing="1" align="center">
<tr>
      <td nowrap="nowrap" align="right">Titulo:</td>
      <td><input type="text" name="titulo" value="" class="caja1" /></td>
</tr>
<tr>      
      <td nowrap="nowrap" align="right">Subtitulo:</td>
      <td><input type="text" name="subtitulo" value="" class="caja1" /></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Clase:</td>
      <td><select name="clase" class="caja1">    	
      	<option value="Itinerarios">Itinerarios</option>
        <option value="Estancias">Estancias</option>
        </select></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Tipo:</td>
      <td><select name="continente" class="caja1">    	
      	<option value="europa">Europa</option>
        <option value="africa">Africa</option>
        <option value="asia">Asia</option>
        <option value="america">America</option>
        <option value="oceania">Oceania</option>
		<option value="mundo">Vuelta al mundo</option>
		<option value="puentes">Puentes</option>
		<option value="invierno">Invierno</option>
        <option value="cruceros">Cruceros</option>
        <option value="disneyworld">Disneyworld</option>
        <option value="balnearios">Balnearios</option>
        </select></td>
</tr>
<tr>        
      <td nowrap="nowrap" align="right">Pais:</td>
      <td><select name="pais" class="caja1">
  <option value="AF">Afganistan</option>
  <option value="AL">Albania</option>
  <option value="DE">Alemania </option>
  <option value="AD">Andorra</option>
  <option value="AO">Angola</option>
  <option value="AI">Anguila</option>
  <option value="AQ">Antartida</option>
  <option value="AG">Antigua y Barbuda</option>
  <option value="AN">Antillas Neerlandesas</option>
  <option value="SA">Arabia Saudi</option>
  <option value="DZ">Argelia</option>
  <option value="AR">Argentina</option>
  <option value="AM">Armenia</option>
  <option value="AW">Aruba</option>
  <option value="AU">Australia</option>
  <option value="AT">Austria</option>
  <option value="AZ">Azerbaiyan</option>
  <option value="BS">Bahamas</option>
  <option value="BH">Bahrain</option>
  <option value="BD">Bangladesh</option>
  <option value="BB">Barbados</option>
  <option value="BE">Belgica</option>
  <option value="BZ">Belice</option>
  <option value="BJ">Benin</option>
  <option value="BM">Bermudas</option>
  <option value="BY">Bielorrusia</option>
  <option value="BO">Bolivia</option>
  <option value="BA">Bosnia y Hercegovina</option>
  <option value="BW">Botsuana</option>
  <option value="BR">Brasil</option>
  <option value="BN">Brunei</option>
  <option value="BG">Bulgaria</option>
  <option value="BF">Burkina Faso</option>
  <option value="BI">Burundi</option>
  <option value="BT">Butan</option>
  <option value="CV">Cabo Verde</option>
  <option value="KH">Camboya</option>
  <option value="CM">Camerun</option>
  <option value="CA">Canada</option>
  <option value="TD">Chad</option>
  <option value="CL">Chile</option>
  <option value="CN">China</option>
  <option value="CY">Chipre</option>
  <option value="VA">Ciudad del Vaticano</option>
  <option value="CO">Colombia</option>
  <option value="KM">Comores</option>
  <option value="CG">Congo</option>
  <option value="CD">Congo, Republica Democratica del</option>
  <option value="KP">Corea del Norte</option>
  <option value="KR">Corea del Sur</option>
  <option value="CI">Costa de Marfil</option>
  <option value="CR">Costa Rica</option>
  <option value="HR">Croacia</option>
  <option value="CU">Cuba</option>
  <option value="DK">Dinamarca</option>
  <option value="DM">Dominica</option>
  <option value="EC">Ecuador</option>
  <option value="EG">Egipto</option>
  <option value="SV">El Salvador</option>
  <option value="AE">Emiratos arabes Unidos</option>
  <option value="ER">Eritrea</option>
  <option value="SK">Eslovaquia</option>
  <option value="SI">Eslovenia</option>
  <option value="ES" selected="selected">Espa&ntilde;a</option>
  <option value="US">Estados Unidos</option>
  <option value="EE">Estonia</option>
  <option value="ET">Etiopia</option>
  <option value="PH">Filipinas</option>
  <option value="FI">Finlandia</option>
  <option value="FJ">Fiji</option>
  <option value="FR">Francia</option>
  <option value="GA">Gabon</option>
  <option value="GM">Gambia</option>
  <option value="GE">Georgia</option>
  <option value="GH">Ghana</option>
  <option value="GI">Gibraltar</option>
  <option value="GD">Granada</option>
  <option value="GR">Grecia</option>
  <option value="GL">Groenlandia</option>
  <option value="GP">Guadalupe</option>
  <option value="GU">Guam</option>
  <option value="GT">Guatemala</option>
  <option value="GF">Guayana Francesa</option>
  <option value="GG">Guernsey</option>
  <option value="GN">Guinea</option>
  <option value="GW">Guinea-Bissau</option>
  <option value="GQ">Guinea Ecuatorial</option>
  <option value="GY">Guyana</option>
  <option value="HT">Haiti</option>
  <option value="HN">Honduras</option>
  <option value="HK">Hong Kong</option>
  <option value="HU">Hungria</option>
  <option value="IN">India</option>
  <option value="ID">Indonesia</option>
  <option value="IR">Iran</option>
  <option value="IQ">Iraq</option>
  <option value="IE">Irlanda</option>
  <option value="BV">Isla Bouvet</option>
  <option value="CX">Isla Christmas</option>
  <option value="IM">Isla de Man</option>
  <option value="IS">Islandia</option>
  <option value="NF">Isla Norfolk</option>
  <option value="AX">Islas Aland</option>
  <option value="KY">Islas Caiman</option>
  <option value="CC">Islas Cocos</option>
  <option value="CK">Islas Cook</option>
  <option value="FO">Islas Feroe</option>
  <option value="GS">Islas Georgia del Sur y Sandwich del Sur</option>
  <option value="HM">Islas Heard y McDonald</option>
  <option value="FK">Islas Malvinas</option>
  <option value="MP">Islas Mariana del Norte</option>
  <option value="MH">Islas Marshall</option>
  <option value="UM">Islas menores alejadas de los Estados Unidos</option>
  <option value="PN">Islas Pitcairn</option>
  <option value="SB">Islas Salomon</option>
  <option value="SJ">Islas Svalbard y Jan Mayen</option>
  <option value="TC">Islas Turcas y Caicos</option>
  <option value="VI">Islas Virgenes, EE.UU.</option>
  <option value="VG">Islas Virgenes Britanicas</option>
  <option value="IL">Israel</option>
  <option value="IT">Italia</option>
  <option value="JM">Jamaica</option>
  <option value="JP">Japon</option>
  <option value="JE">Jersey</option>
  <option value="JO">Jordania</option>
  <option value="KZ">Kazajistan</option>
  <option value="KE">Kenia</option>
  <option value="KG">Kirguizistan</option>
  <option value="KI">Kiribati</option>
  <option value="KW">Kuwait</option>
  <option value="LA">Laos</option>
  <option value="LS">Lesoto</option>
  <option value="LV">Letonia</option>
  <option value="LB">Libano</option>
  <option value="LR">Liberia</option>
  <option value="LY">Libia</option>
  <option value="LI">Liechtenstein</option>
  <option value="LT">Lituania</option>
  <option value="LU">Luxemburgo</option>
  <option value="MO">Macao</option>
  <option value="MK">Macedonia</option>
  <option value="MG">Madagascar</option>
  <option value="MY">Malasia</option>
  <option value="MW">Malaui</option>
  <option value="MV">Maldivas</option>
  <option value="ML">Mali</option>
  <option value="MT">Malta</option>
  <option value="MA">Marruecos</option>
  <option value="MQ">Martinica</option>
  <option value="MU">Mauricio</option>
  <option value="MR">Mauritania</option>
  <option value="YT">Mayotte</option>
  <option value="MX">Mexico</option>
  <option value="FM">Micronesia</option>
  <option value="MD">Moldavia</option>
  <option value="MC">Monaco</option>
  <option value="MN">Mongolia</option>
  <option value="ME">Montenegro</option>
  <option value="MS">Montserrat</option>
  <option value="MZ">Mozambique</option>
  <option value="MM">Myanmar (Birmania)</option>
  <option value="NA">Namibia</option>
  <option value="NR">Nauru</option>
  <option value="NP">Nepal</option>
  <option value="NI">Nicaragua</option>
  <option value="NE">Niger</option>
  <option value="NG">Nigeria</option>
  <option value="NU">Niue</option>
  <option value="NO">Noruega</option>
  <option value="NC">Nueva Caledonia</option>
  <option value="NZ">Nueva Zelanda</option>
  <option value="OM">Oman</option>
  <option value="NL">Paises Bajos</option>
  <option value="PK">Pakistan</option>
  <option value="PW">Palaos (Belau)</option>
  <option value="PA">Panama</option>
  <option value="PG">Papua-Nueva Guinea</option>
  <option value="PY">Paraguay</option>
  <option value="PE">Peru</option>
  <option value="PF">Polinesia Francesa</option>
  <option value="PL">Polonia</option>
  <option value="PT">Portugal</option>
  <option value="PR">Puerto Rico</option>
  <option value="QA">Qatar</option>
  <option value="GB">Reino Unido</option>
  <option value="CF">Republica Centroafricana</option>
  <option value="CZ">Republica Checa</option>
  <option value="DO">Republica Dominicana</option>
  <option value="RE">Reunion</option>
  <option value="RW">Ruanda</option>
  <option value="RO">Rumania</option>
  <option value="RU">Rusia</option>
  <option value="EH">Sahara Occidental</option>
  <option value="WS">Samoa</option>
  <option value="AS">Samoa americana</option>
  <option value="KN">San Cristobal y Nieves</option>
  <option value="SM">San Marino</option>
  <option value="PM">San Pedro y Miquelon</option>
  <option value="SH">Santa Elena</option>
  <option value="LC">Santa Lucia</option>
  <option value="ST">Santo Tome y Principe</option>
  <option value="VC">San Vicente y las Granadinas</option>
  <option value="SN">Senegal</option>
  <option value="RS">Serbia</option>
  <option value="CS">Serbia y Montenegro</option>
  <option value="SC">Seychelles</option>
  <option value="SL">Sierra Leona</option>
  <option value="SG">Singapur</option>
  <option value="SY">Siria</option>
  <option value="SO">Somalia</option>
  <option value="LK">Sri Lanka</option>
  <option value="SZ">Suazilandia</option>
  <option value="ZA">Sudafrica</option>
  <option value="SD">Sudan</option>
  <option value="SE">Suecia</option>
  <option value="CH">Suiza</option>
  <option value="SR">Surinam</option>
  <option value="TH">Tailandia</option>
  <option value="TW">Taiwan</option>
  <option value="TZ">Tanzania</option>
  <option value="TJ">Tayikistan</option>
  <option value="IO">Territorio Britanico del Oceano indico</option>
  <option value="PS">Territorio Palestino</option>
  <option value="TF">Territorios Australes Franceses</option>
  <option value="TL">Timor Oriental</option>
  <option value="TG">Togo</option>
  <option value="TK">Tokelau</option>
  <option value="TO">Tonga</option>
  <option value="TT">Trinidad y Tobago</option>
  <option value="TN">Tunez</option>
  <option value="TM">Turkmenistan</option>
  <option value="TR">Turquia</option>
  <option value="TV">Tuvalu</option>
  <option value="UA">Ucrania</option>
  <option value="UG">Uganda</option>
  <option value="UY">Uruguay</option>
  <option value="UZ">Uzbekistan</option>
  <option value="VU">Vanuatu</option>
  <option value="VE">Venezuela</option>
  <option value="VN">Vietnam</option>
  <option value="VM">Vuelta al mundo</option>
  <option value="WF">Wallis y Futuna</option>
  <option value="YE">Yemen</option>
  <option value="DJ">Yibuti</option>
  <option value="ZM">Zambia</option>
  <option value="ZW">Zimbabue</option>
		</select>
      </td></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Provincia:</td>
      <td><input type="text" name="provincia" value="" class="caja1" /></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Descripcion:</td>
      <td><textarea name="descripcion" rows="12"></textarea></td>
</tr>
<tr>
      <td nowrap="nowrap" align="right">Otros:</td>
      <td><textarea name="otros"></textarea></td>
</tr>
<tr>
     <td colspan="2" align="center"><input type="submit" value="Insertar" /></td>
</tr>
</table>
<input type="hidden" name="MM_insert" value="form1" />
</form>

</body>
</html>