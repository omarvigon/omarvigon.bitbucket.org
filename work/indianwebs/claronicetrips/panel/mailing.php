<?php require_once('../Connections/claronicetrips.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!isset($_SESSION)) {
  session_start();}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Panel de Control</title>
<!--
<script language="javascript" type="text/javascript" src="editor_tiny/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({mode : "textareas", theme : "simple" });
</script>
-->
<link rel="stylesheet" href="_estilos.css" type="text/css" />
</head>

<body>
<img src="http://www.claronicetrips.com/images/logo-claro.gif" alt="">
<p><a href="inicio.php">Volver al inicio</a></p>

<?php
if($_POST["asunto"])
{
		$texto_antes='<div align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 11px;background-color: #53C3EB;">
		<div style="background-color: #FFFFFF;width: 600px;">
	 	<div style="background-color: #FFFFFF;width: 600px;height: 100px;"><img src="http://www.claronicetrips.com/images/claro-cabecera.jpg" width="600" height="100" border="0" usemap="#Map">
       	<map name="Map">
        <area shape="rect" coords="411,24,586,76" href="http://www.claronicetrips.com">
       	</map>
	 	</div>
		<div style="width: 560px;padding-right: 20px;padding-left: 20px;padding-top: 10px;padding-bottom: 10px; text-align:left;">';
		
		$texto_despues='</div>
		<div style="background-color: #FFFFFF;width: 600px;margin: 0px;">
		<p align="center"><strong>
		<br>Claro nice trips, SL</strong><br>Gran V&iacute;a de les Corts Catalanes, 617 entlo 5&ordf; - 08007 Barcelona
		<br>Tel. 933 176 525 - Fax 933 178 260<br><a href="mailto:info@claronicetrips.com">info@claronicetrips.com</a></p>
		<p>&nbsp;</p>
		</div>
		</div>
	
		<div style="color: #C3EBE9;width: 600px;font-size: 9px;padding-top: 10px;padding-bottom: 10px;">
	  	<div align="justify">En virtud de la Ley 34/2002 de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico (LSSI-CE), en vigor desde el 12 de octubre de 2002 y de la Ley Org&aacute;nica 15/199 del 13/12/1999 de Protecci&oacute;n de Datos Espa&ntilde;ola, le comunicamos que su direcci&oacute;n de correo electr&oacute;nico forma parte de un fichero automatizado perteneciente a Claro nice trips SL, con la finalidad de enviar publicidad relativa a los productos representados y servicios ofrecidos, as&iacute; como comunicarle su opci&oacute;n de cancelaci&oacute;n o modificaci&oacute;n de estos datos. En virtud de la leyes antes mencionadas, usted tiene derecho de oposici&oacute;n, acceso, rectificaci&oacute;n y cancelaci&oacute;n de sus datos, dirigiendo un correo electr&oacute;nico a <a href="mailto:info@claronicetrips.com">info@claronicetrips.com</a>. Le recordamos que sus datos nunca son suministrados a terceros bajo ning&uacute;n concepto.</div>
		</div>
		</div>';
		
		//$mensaje = htmlentities($_POST["cuerpo"], ENT_NOQUOTES, 'UTF-8');
		//$mensaje=utf8_encode($_POST["cuerpo"]);
		$asunto=str_replace("ñ","&ntilde;",$_POST["asunto"]);
		$mensaje=str_replace("\n","<br>",$_POST["cuerpo"]);
		$mensaje=str_replace('\"','&quot;',$mensaje);
		$mensaje=$texto_antes.$mensaje.$texto_despues;
		
		if($_POST["prueba"])
		{
			if(mail("info@claronicetrips.com",$asunto,$mensaje,"From:info@claronicetrips.com\r\nMIME-Version:1.0\r\nCc:g5@estudigrafic.net\r\nContent-type:text/html;charset=iso-8859-1\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
				echo "<p>&bull; Enviado a <b>info@claronicetrips.com</b></p>"; 
			if(mail("info@estudigrafic.net",$asunto,$mensaje,"From:info@claronicetrips.com\r\nMIME-Version:1.0\r\nCc:omar@indianwebs.com\r\nBcc:alex.ciobanu@indianwebs.com\r\nContent-type:text/html;charset=iso-8859-1\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
				echo "<p>&bull; Enviado a <b>info@estudigrafic.net</b></p>";
		}
		else
		{	
			if (!function_exists("GetSQLValueString")) {
			function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
			{
			  if (PHP_VERSION < 6) {
				$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;}
			
			  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
			  switch ($theType) {
				case "text":
				  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				  break;    
				case "long":
				case "int":
				  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
				  break;
				case "double":
				  $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				  break;
				case "date":
				  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				  break;
				case "defined":
				  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				  break;
			  }
			  return $theValue;
			}
			}
			mysql_select_db($database_claronicetrips, $claronicetrips);
			$query_Recordset1 = "SELECT email FROM usuarios";
			$Recordset1 = mysql_query($query_Recordset1, $claronicetrips) or die(mysql_error());
			$row_Recordset1 = mysql_fetch_assoc($Recordset1);
			$totalRows_Recordset1 = mysql_num_rows($Recordset1);
			do { 
			
				if(mail($row_Recordset1['email'],$asunto,$mensaje,"From:info@claronicetrips.com\r\nMIME-Version:1.0\r\nContent-type:text/html;charset=iso-8859-1\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
					echo "<p>&bull; Enviado a <b>".$row_Recordset1['email']."</b></p>"; 
			} while ($row_Recordset1 = mysql_fetch_assoc($Recordset1));
			
			mysql_free_result($Recordset1);
		}	

}
else
{
?>
<form method="post" name="form_envio" action="mailing.php" >
<input type="hidden" name="prueba" value="" />
<table cellpadding="4" cellspacing="1">
  <tr>
    <td>Asunto:</td>
    <td><input type="text" name="asunto" class="caja1" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Mensaje:</td>
    <td><textarea name="cuerpo" cols="100" rows="18">Introducir aqu&iacute; texto emailing... <?php /*include("mailing.htm");*/?></textarea></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td><input type="button" onclick="document.form_envio.prueba.value=true;document.form_envio.submit()" value="ENVIAR PRUEBA" /> </td>
    <td><input type="button" onclick="document.form_envio.submit()" value="ENVIAR" /></td>
  </tr>
</table>
</form>
<? } ?>
</body>
</html>
