<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">

		<div class="interior3">

		<h1>A la antigua usanza</h1>
		<img src='http://www.hmemagazine.com/img/selection2.jpg' alt="HME Hombre Selection" title="HME Hombre Selection" align="left" hspace="10">
		<p style="margin-bottom: 0;">Cruxt Watch, distribuidora en Espa&ntilde;a de la fi rma relojera Citizen, ofrece un nuevo despertador digital que recuerda los antiguos relojes de arena pero, en esta ocasi&oacute;n, dispone de una caja de pl&aacute;stico enmarcando el despertador que representa el fluir constante del tiempo a partir de una tecnolog&iacute;a sencilla. Muestra un display de arena digital que al darle la vuelta, las part&iacute;culas van cayendo en fragmentos de 60 segundos. En ambas bases se dispone de funciones como hora, cron&oacute;metro y cuenta atr&aacute;s.</p>

		<p>M&aacute;s informaci&oacute;n: 914 354 181</p>

	  </div>

	</td>
</tr>
<?php include("00piecera.php");?>