<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">
		<div class="interior3">
		
		<h1>Caracter&iacute;sticas T&eacute;cnicas :</h1>
		Impresi&oacute;n Offset - Formato Revista Din A-4 (210 x 297) Tiraje digital CTP entre 7.500 y 10.000 ejemplares.
		<br><b>Calendario Editorial: </b>Enero - Marzo - Mayo - Julio - Septiembre - Noviembre
		<p><b>Importante: </b>Editamos con tecnolog&iacute;a CTP, esto significa el requerimiento  de los formatos CD o DVD para soporte digital, adjuntando una prueba de color para un  seguimiento &oacute;ptimo en el resultado final.<br />
		Archivos: Freehand , InDesign o en formato de imagen,  preferible con los textos trazados y adem&aacute;s incluir una carpeta con todo el  material utilizado en el proceso de desarrollo&nbsp; (im&aacute;genes, tipograf&iacute;as,  etc...).</p>
		<b>Medidas :</b><br>
		Doble p&aacute;g. (sangre): 426x303 mm <img src='img/publi1.gif' align="absmiddle">
		P&aacute;g. (sangre): 216x303 mm <img src='img/publi2.gif' align="absmiddle"><br>
		1/2 p&aacute;g. horiz. (sangre): 216x151 mm  <img src='img/publi3.gif' align="absmiddle">
		1/2 p&aacute;g. verti. (sangre): 106x303 mm <img src='img/publi4.gif' align="absmiddle">
		
		<p><b>Ediciones  3ideas SCP</b> C/ San Antoni, 97 Bajos 2&ordf;&nbsp; &middot; 08880 Cubelles (Barcelona) Spain Tel. +34  93 895 65 72 <a href='mailto:publicidad@hmemagazine.com'>publicidad@hmemagazine.com</a>		
		</div>
	</td>
</tr>
<?php include("00piecera.php");?>