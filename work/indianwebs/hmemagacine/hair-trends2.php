<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		<h1>Control total de la actividad de un peluquer&iacute;a con Ofim&aacute;tica </h1>

        <p style="margin-bottom: 0;">Desarrollado &iacute;ntegramente en entorno Windows, OFIPELUQ es el programa de la empresa Ofim&aacute;tica ideal para la mecanizaci&oacute;n total de la actividad administrativa y comercial de una peluquer&iacute;a. Partiendo de la filosof&iacute;a de &uacute;nico dato, todos los m&oacute;dulos est&aacute;n conectados entre s&iacute;, enlazando y generando la contabilidad de forma autom&aacute;tica. Es modular y flexible, por lo que es f&aacute;cilmente adaptable a las necesidades de cualquier tama&ntilde;o de peluquer&iacute;a o sal&oacute;n de belleza. Se puede trabajar con/sin reservas previas o con clientes directos, con los que se pueden pactar tarifas o comisiones especiales. La gesti&oacute;n de reservas tiene un manejo muy simplificado en base al control gr&aacute;fico del planning de la Agenda de Reservas, para desde una &uacute;nica visi&oacute;n tener acceso a la situaci&oacute;n de las mismas durante el d&iacute;a o toda la semana. Se puede mecanizar la actividad de los establecimientos con un facil&iacute;simo manejo, ya que las ventas se pueden efectuar mediante pantallas t&aacute;ctiles. Todos los datos generados (facturas, tickets...) son controlados desde administraci&oacute;n para su posible posterior tratamiento. OFIPELUQ facilita tambi&eacute;n el control de los distintos almacenes en que se puede seccionar la gesti&oacute;n: productos de belleza, ropa, material de limpieza, material de trabajo, etc, pudiendo hacer movimientos entre almacenes. Todas las facturas recibidas de los proveedores son recepcionadas y chequeadas por el programa y al final de cada d&iacute;a, se podr&aacute;n efectuar los arqueos de cajas y TPV&acute;s y efectuar los cierres correspondientes. </p>

        <p style="margin-top: 0;"><br>

          M&aacute;s informaci&oacute;n: 902 250 144 </p>

        <p><a href="http://www.ofi.es">www.ofi.es</a></p>

        <p style="margin-bottom: 0;">&nbsp;</p>

<h1 style="margin-top: 0; margin-bottom: 0;"><br>

        </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>