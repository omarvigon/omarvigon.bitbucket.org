<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		<h1>Body Factory : Sumando socios</h1>
		
        <img src='img/wellness1.jpg' alt="HME Magazine Wellness" title="HME Magazine Wellness" align="left" hspace="10">
		<p style="margin-bottom: 0;">La cadena en gesti&oacute;n de gimnasios e instalaciones deportivas, Body Factory, ha alcanzado la cifra de 50.000 socios en Espa&ntilde;a repartidos en sus 34 centros, personas que se preocupan por la salud y el bienestar y que disfrutan con la pr&aacute;ctica de deporte. La empresa ha cerrado el a&ntilde;o con una facturaci&oacute;n de 29 millones de euros, un 3% m&aacute;s que el a&ntilde;o anterior. </p>
		<p>En 2007, cabe recordar adem&aacute;s, acometi&oacute; su internacionalizaci&oacute;n con la apertura de dos centros en Italia.</p>
		<p>M&aacute;s informaci&oacute;n: 902 363 550</p>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>