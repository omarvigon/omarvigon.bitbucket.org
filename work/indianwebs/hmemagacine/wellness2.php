<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">
		
		<h1>Selvert Thermal : Una joya de crema</h1>
		<img src='img/wellness2.jpg' alt="HME Magazine Wellness" title="HME Magazine Wellness" align="left" hspace="10">
		<p style="margin-bottom: 0;">Selvert Thermal ha creado una innovadora l&iacute;nea de productos destinados a mimar la piel con su nueva formulaci&oacute;n suiza y el agua termal como componente b&aacute;sico. Thermal&iacute;sima es el resultado de un avance tecnol&oacute;gico: la biotecnolog&iacute;a termal, una t&eacute;cnica basada en el cultivo de c&eacute;lulas biotecnol&oacute;gicas en un medio de agua termal. La crema, de textura delicada, est&aacute; indicada en el tratamiento de la piel madura mejorando su textura en tan s&oacute;lo ocho d&iacute;as.</p>

		<p style="margin-top: 0;">M&aacute;s informaci&oacute;n: www.selvert.com</p>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>