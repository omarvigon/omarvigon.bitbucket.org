<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">

		<div class="interior3">

		<h1>La Toja : Directo del manantial</h1>
		<img src='img/wellness3.jpg' alt="HME Magazine Wellness" title="HME Magazine Wellness" align="left" hspace="10">
		<p style="margin-bottom: 0;">Los productos de Manantiales de La Toja, durante m&aacute;s de 100 a&ntilde;os, han ofrecido sus benefi cios a los visitantes del balneario, pero ahora est&aacute;n al alcance de todos a trav&eacute;s de la web www.latoja.com/manantiales, donde se puede adquirir cualquiera de ellos. La gama propone aquellos especiales para regalar, en estuches de madera, que incluyen pastillas de jab&oacute;n de sales, sales de ba&ntilde;o, gel y colonia de hombre y mujer.</p>

		<p style="margin-top: 0;">M&aacute;s informaci&oacute;n: 932 904 492</p>

	  </div>

	</td>
</tr>
<?php include("00piecera.php");?>
