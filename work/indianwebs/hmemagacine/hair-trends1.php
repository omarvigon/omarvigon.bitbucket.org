<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		<h1>El nuevo activador de matices fr&iacute;os de Univerlook</h1>

        <p style="margin-bottom: 0;">Univerlook ha lanzado al mercado profesional Blue Booster, el nuevo tratamiento para enriquecer y activar los matices fr&iacute;os en el cabello, como mechas, platino, canas y blancos, ampliando la completa familia Nutritive Method. El nuevo champ&uacute; se presenta en dos formatos: para el sal&oacute;n y para la reventa. Blue Booster protege el cabello de las agresiones externas y mejora la propiedad del mismo, con m&aacute;s fuerza y resistencia, a la par que le previene del envejecimiento amarillento (neutraliza sus reflejos). Gracias a la incorporaci&oacute;n en sus principios activos del Alba Plus, el nuevo producto incrementa la penetraci&oacute;n y fijaci&oacute;n de los colores. </p>

        <p style="margin-top: 0;"><br>

          M&aacute;s informaci&oacute;n: 916 555 070 </p>

        <p style="margin-bottom: 0;">&nbsp;</p>

<h1 style="margin-top: 0; margin-bottom: 0;"><br>

        </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>