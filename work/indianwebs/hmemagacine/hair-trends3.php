<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		

		<h1>Pahi aporta comodidad y ergonom&iacute;a al sal&oacute;n de peluquer&iacute;a</h1>

        <p style="margin-bottom: 0;">F&iacute;garo y Desiree son los nombres de sendos sillones de peluquer&iacute;a pertenecientes a la atractiva l&iacute;nea Premium de la firma Pahi y son exponente de la vanguardia en mobiliario de peluquer&iacute;a. Rojo para F&iacute;garo y naranja metalizado para Desiree. Dos concepciones del arte y de la ergonom&iacute;a para que la clientela se sienta a gusto en el sal&oacute;n. La comodidad y la ergonom&iacute;a que propone la empresa de Jordi Solerdelcoll, con m&aacute;s de 30 a&ntilde;os de experiencia, entroncan con las l&iacute;neas en movimiento, la vistosidad y la profunda atracci&oacute;n que logran los salones donde se ubican. La compa&ntilde;&iacute;a busca y logra crear espacios de trabajo con una intensa vida propia, individual, &uacute;nicos y al gusto de cada profesional.</p>

        <p style="margin-top: 0; margin-bottom: 0;"><br>

          M&aacute;s informaci&oacute;n: 938 890 417</p>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>