<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">
		<div class="interior3">
		<img src="img/revista.jpg" border="0" alt="HME Hombre" align="left" vspace="8">
		<h1>HME Hombre: La primera revista profesional del cuidado personal del hombre</h1>
		<p>El nuevo  inter&eacute;s del hombre por la cosm&eacute;tica, el renovado concepto de peluquer&iacute;a de  caballeros y la atenci&oacute;n prestada a la moda y sus tendencias, imponen una nueva  realidad en el mundo de la belleza: el cuidado personal del hombre. </p>
		<p>HME Hombre es  la revista profesional del mercado de la belleza especializada en el cuidado  masculino. En ella las firmas de cosm&eacute;tica, peluquer&iacute;a, aparatolog&iacute;a o los  centros de est&eacute;tica dan a conocer sus nuevas l&iacute;neas masculinas, tratamientos,  inauguraciones, colecciones, promociones, etc. </p>
		<p>Hoy por hoy,  nuestro medio es el soporte id&oacute;neo para hacer llegar al p&uacute;blico final y  profesional las &uacute;ltimas novedades de su empresa y una de las mejores formas  para estar informado de las que ofrece su competencia. </p>
		<p>En definitiva,  los profesionales de estos sectores encontrar&aacute;n en las p&aacute;ginas de HME Hombre  informaciones &uacute;tiles y actuales: tratamientos de est&eacute;tica, novedades de  producto, reportajes paso a paso, nuevas colecciones, tendencias, etc. Adem&aacute;s,  expertos de cada &aacute;rea ofrecen sus conocimientos y puntos de vista sobre  cuestiones que interesan tanto al circuito profesional como al consumidor final. Nuestro medio  piensa en un hombre que se cuida, que se preocupa por su imagen y que est&aacute;  atento a las corrientes est&eacute;ticas que recorren los foros internacionales. Un  hombre seguro de s&iacute; mismo, moderno, urbano, divertido y sensible.</p>
		</div>
	</td>
</tr>
<?php include("00piecera.php");?>