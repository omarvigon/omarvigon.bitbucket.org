<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">

		<div class="interior3">

		<h1>Ventiklar : Pura elegancia</h1>
		<img src='img/wellness4.jpg' alt="HME Magazine Wellness" title="HME Magazine Wellness" align="left" hspace="10">
		<p style="margin-bottom: 0;">Ventiklar presenta su nueva mampara Selenia3, un ejemplo de c&oacute;mo la calidad unida al dise&ntilde;o minimalista puede lograr resultados ampliamente decorativos y de extrema modernidad. Es f&aacute;cil y r&aacute;pida de montar, adem&aacute;s de c&oacute;moda, elegante y con muy poca perfi ler&iacute;a, regulable y desmontable. La altura es de 190 cm, la anchura oscila entre los 90 y los 120 cm y las puertas se eligen transparentes, cristal carglass, mate o carr&eacute;, as&iacute; como en grabado est&aacute;ndar.</p>

		<p style="margin-top: 0;">M&aacute;s informaci&oacute;n: 932 649 350</p>
	  </div>

	</td>
</tr>
<?php include("00piecera.php");?>