<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		

		<h1>Relax mediterr&aacute;neo en Las Dunas Beach Hotel &amp; Spa</h1>

		<p style="margin-bottom: 0;">Para quienes buscan tratar de alargar las vacaciones, lejos del tumulto de las grandes ciudades y de los compromisos de la agenda, nada como escaparse al sur de Espa&ntilde;a, entre Marbella y Estepona, a Las Dunas Beach Hotel &amp; Spa, un hotel de cinco estrellas Gran Lujo. All&iacute; se encontrar&aacute;, entre otras instalaciones, con el Serenity Spa, instalado por la firma Sytem-Pool, un espacio donde todos los clientes pueden beneficiarse de los &uacute;ltimos productos para relajar cuerpo y mente Blue-Spa. El centro est&aacute; dotado de dos saunas finlandesas, ba&ntilde;o turco, fuente de hielo y duchas de esencias, de contraste, escocesa, vichy, tropic y ducha cubo. Todo ello conforma un circuito de salud y bienestar a la altura de un establecimiento de alto nivel que se complementa con exclusivos tratamientos de belleza. El Hotel Las Dunas Beach, por otro lado, cuenta con 88 espaciosas suites con magn&iacute;ficas vistas al mar, la costa de &Aacute;frica y los exuberantes jardines, adem&aacute;s de una excelente oferta gastron&oacute;mica con cuatro restaurantes de diferentes propuestas culinarias: La Brasserie Felix, de cocina italiana y asi&aacute;tica; El Lido, excelente restaurante gourmet mediterr&aacute;neo; el club de playa Las Palmeras, junto a la piscina del hotel; y el Mediterr&aacute;neo, con especialidades andaluzas e internacionales al mediod&iacute;a.</p>

		<p style="margin-top: 0;"><br>

		  M&aacute;s informaci&oacute;n: 952 794 345</p>

		<p style="margin-bottom: 0;">&nbsp;</p>

<h1 style="margin-top: 0; margin-bottom: 0;"><br>

		  </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>