<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">
		<div class="interior3">

		<img src="img/distribucion.jpg" border="0" align="left" vspace="8" hspace="4">
		<h1>Distribuci&oacute;n</h1>
		HME Hombre es una revista cuya  distribuci&oacute;n se basa en un circuito profesional, en el que aproximadamente el  35% del total est&aacute; destinado a salones de peluquer&iacute;a, otro 35% representa los  centros de est&eacute;tica y cl&iacute;nicas, un 10% los spas urbanos y balnearios, un 5% se  distribuye en gimnasios, y el 15% restante se dirige a hoteles.
		<p>Estos porcentajes se aplican a un  tiraje de 10.000 ejemplares con una vigencia de dos meses, lo que se traduce en  una audiencia que supera los 3 millones de lectores. Hoy por hoy, el c&aacute;lculo de  la audiencia se ha realizado a partir de consultas telef&oacute;nicas realizadas a los  propios puntos de distribuci&oacute;n. En este sentido, el resultado es de  aproximadamente dos personas/d&iacute;a, en los gimnasios y de 8 personas/d&iacute;a en los  centros de est&eacute;tica y salones de peluquer&iacute;a. 
		HME Hombre desde su lanzamiento  ha apostado por una distribuci&oacute;n de calidad en la que el criterio aplicado es  la selecci&oacute;n exhaustiva de los destinos de la revista. Asimismo,  progresivamente se ir&aacute; ampliando la tirada aplicando las mismas directrices. 
		El objetivo que persigue esta  distribuci&oacute;n de calidad es garantizar la m&aacute;xima efectividad y rentabilidad de  la inversi&oacute;n publicitaria realizada por la firma anunciante. Geogr&aacute;ficamente la distribuci&oacute;n  se concentra en dos ciudades: Madrid y Barcelona. Entre las dos capitales  absorben el 80% del total de la distribuci&oacute;n, mientras que el 20% restante est&aacute;  repartido entre las dem&aacute;s provincias.</p>
		<p>Por &uacute;ltimo, HME participa como  expositor en Ferias especializadas, tales como: Sal&oacute;n Look, IMAXE y BESA, en  Espa&ntilde;a, y en Expocosm&eacute;tica en Oporto (Portugal), as&iacute; como en eventos y jornadas  t&eacute;cnicas. En estos cert&aacute;menes realizamos una distribuci&oacute;n directa al  profesional, tanto al expositor como al profesional que visita la feria.</p>

		</div>
	</td>
</tr>
<?php include("00piecera.php");?>