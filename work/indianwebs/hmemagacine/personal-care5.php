<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		<h1>Aromas de Provenza propone un novedoso az&uacute;car corporal</h1>

		<p style="margin-bottom: 0;">Aromas de Provenza, la firma especializada en el cuidado del cuerpo y perfumes del hogar, da a conocer su az&uacute;car corporal, un nuevo producto especialmente pensado para eliminar las c&eacute;lulas muertas e impurezas de la piel. Se trata de un az&uacute;car coloreado y perfumado, mezclado con aceite, una combinaci&oacute;n que sirve para hidratar y exfoliar la piel del cuerpo en un solo gesto, dos propiedades fundamentales para tratar la epidermis en los meses estivales y en su posterior recuperaci&oacute;n tras la exposici&oacute;n solar. Su uso frecuente suaviza e ilumina la piel, dej&aacute;ndola tersa y libre de impurezas. El az&uacute;car exfoliante pertenece a la l&iacute;nea Une Bouteille &agrave; la Mer de Aromas de Provenza que incluye, adem&aacute;s, aceite decorativo, aceite de ba&ntilde;o, sales de ba&ntilde;o, espuma de ba&ntilde;o, probetas de ba&ntilde;o y jabones. Una vez m&aacute;s, recurre a la creaci&oacute;n de nuevas gamas de productos para satisfacer todas las necesidades de sus clientes, una filosof&iacute;a que le ha llevado a contar con una de las gamas de productos naturales m&aacute;s importantes de nuestro pa&iacute;s. En los m&aacute;s de diez establecimientos que la firma tiene en Espa&ntilde;a se distribuyen una gran variedad de productos, todos ellos elaborados a base de ingredientes de primera calidad procedentes de la Provenza francesa. </p>

		<p style="margin-top: 0;"><br>

		  M&aacute;s informaci&oacute;n: 902 121 200</p>

		<h1 style="margin-bottom: 0;"><br>

          </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>