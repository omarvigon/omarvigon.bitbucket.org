<html>
<head>
<style type="text/css">
	body,select,input,textarea{font:12px 'Trebuchet MS', Arial, sans-serif;}
	input			 	 {border:1px solid #000000;}
	select,input,textarea{display:block;}
	h2					 {text-align:center;}
</style>	
</head>

<body class="interior">

<h2>HME NEWSLETTER</h2>
<center>
<?php
$texto="<table cellspacing='3' cellpadding='3' style='font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;background:#EEEEEE;'>
<tr>
<td colspan='5'><div align='left'><span style='font-size:32px;color:#0066CC;font-weight:bold;'>NEWSLETTER</span> <span style='padding-left:90px;'><b>&nbsp;&nbsp;&nbsp; Septiembre/Octubre - Revista n�10</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <img src='http://www.hmemagazine.com/img/cabecera_news.jpg' hspace='40'></span><br>
    <b> HME Hombre</b></div></td>
</tr>
<tr>
<td colspan='5' style='font-size:18px'><div align='left'>Personal Care</div></td>
</tr>
<tr>
<td valign='top' bgcolor='#FFFFFF' width='170'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'>Mesoestetic

</p>
  <img src='http://www.hmemagazine.com/img/pc_1.jpg'>
  <p> Con la llegada del <br>
oto&ntilde;o, son perceptibles los <br>
efectos de las horas <br>
pasadas al aire libre <br>
durante el verano y, por <br>
ello, es el momento para <br>
aplicar los cuidados <br>
necesarios con <br>
tratamientos cosm&eacute;ticos <br>
que promuevan...</p>
  <p style='margin-bottom: 0;'><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p></td>
<td valign='top' bgcolor='#FFFFFF' width='170'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Skeyndor </p>
<img src='http://www.hmemagazine.com/img/pc_2.jpg'><p> Las altas temperaturas, el <br>
  contacto prologado con el <br>
  agua e incluso la <br>
  irregularidad en la dieta <br>
  son algunas...</p>
<p style='margin-bottom: 0;'><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p></td>
<td valign='top' bgcolor='#FFFFFF' width='170'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Tom Robinn </p>
<img src='http://www.hmemagazine.com/img/pc_3.jpg'><p> Tras el verano, la piel del <br>
  hombre requiere <br>
  mantener, de forma <br>
  natural, el bronceado de la <br>
  etapa estival...<br>
  <br>
  <a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
</td>
<td valign='top' bgcolor='#FFFFFF' width='170'><p style='font-weight:bold; font-size:10px; background:#EEEEEE;'>Monodosis de placer

</p>
  <p><img src='http://www.hmemagazine.com/img/pc_4.jpg'></p>
  <p> Las 'Ceramidas E+F Plus' <br>
    de Reuser son aut&eacute;nticas <br>
    monodosis de placer con <br>
    efectos concluyentes a los <br>
    21 d&iacute;as de comenzar el<br>tratamiento...</p>
<p style='margin-bottom: 0;'><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p></td>
<td valign='top' bgcolor='#FFFFFF' width='170'><p style='font-weight:bold; font-size:10px; background:#EEEEEE;'> Aromas de Provenza </p>
  <p style='margin-bottom: 0;'><img src='http://www.hmemagazine.com/img/pc_5.jpg'><br>
  </p>
  <p style='margin-top: 0;'> Aromas de Provenza, la <br>
    firma especializada en el <br>
    cuidado del cuerpo y <br>
    perfumes del hogar, <br>
da a conocer su <br>
az&uacute;car corporal, un<br>nuevo producto...</p>
  <p style='margin-bottom: 0;'><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p></td>
</tr>
<tr>
<td colspan='5' style='font-size:18px;color:#F9B200;'>Hair Trends</td>
</tr>
<tr>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Univerlook </p>
<img src='http://www.hmemagazine.com/img/ht_1.jpg'><p> Univerlook ha lanzado al <br>
  mercado profesional Blue <br>
  Booster, el nuevo <br>
  tratamiento para <br>
  enriquecer... </p>
<p><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
<p style='margin-bottom: 0;'>&nbsp;</p></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Control total </p>
<img src='http://www.hmemagazine.com/img/ht_2.jpg'><p> Desarrollado &iacute;ntegramente <br>
  en entorno Windows, <br>
  OFIPELUQ es el programa <br>
  de la empresa Ofim&aacute;tica <br>
  ideal para la mecanizaci&oacute;n <br>
  total de la actividad <br>
  administrativa... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Pahi aporta comodidad </p>
<img src='http://www.hmemagazine.com/img/ht_3.jpg'>
<p> F&iacute;garo y Desiree son los <br>
  nombres de sendos <br>
  sillones de peluquer&iacute;a<br> pertenecientes...</p>
<p style='margin-bottom: 0;'><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Lendan frena la ca&iacute;da </p>
<img src='http://www.hmemagazine.com/img/ht_4.jpg'><p> Lendan propone su <br>
l&iacute;nea Pilosome Stimul <br>
como sistema de <br>
tratamiento avanzado <br>
profesional contra la ca&iacute;da <br>
del cabello con resultados <br>
visibles de mejora desde <br>
los primeros d&iacute;as de <br>
seguimiento... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Cabellos protegidos </p>
<img src='http://www.hmemagazine.com/img/ht_5.jpg'><p> La firma de origen italiano<br>especializado <br>
en cosm&eacute;tica y belleza <br>
natural, Bottega Verde, <br>
presenta... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
</tr>
<tr>
<td colspan='5' style='font-size:18px;color:#972A76;'>Fashion Life</td>
</tr>
<tr>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Alphadventure </p>
<img src='http://www.hmemagazine.com/img/fl_1.jpg'><p> En la colecci&oacute;n que <br>
  Alphadventure presenta <br>
  para la temporada de <br>
  invierno destacan las <br>
  prendas... </p>
<p><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
<p style='margin-bottom: 0;'>&nbsp;</p></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Montblanc </p>
<img src='http://www.hmemagazine.com/img/fl_2.jpg'><p> La firma Montblanc, desde <br>
  1992, ha rendido <br>
  homenaje a la vida y obra <br>
  de grandes escritores con <br>
  la 'Edici&oacute;n Escritores de <br>
  Montblanc' de fama <br>
  mundial. Este a&ntilde;o 2007 la
  <br>
  atenci&oacute;n ha reca&iacute;do... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'>Massana</p>
  <img src='http://www.hmemagazine.com/img/fl_3.jpg'><p>La nueva colecci&oacute;n de <br>
    Massana viste a un <br>
    hombre sencillo y natural, <br>
    urbano y aventurero al <br>
    mismo tiempo. Las <br>
    prendas est&aacute;n <br>
    confeccionadas... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'>Nuevos relojes Zodiac

</p>
<img src='http://www.hmemagazine.com/img/fl_4.jpg'><p>Zodiac presenta sus <br>
  nuevos modelos de relojes <br>
  para la temporada oto&ntilde;o-<br>
  invierno con... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'>Nueva tienda Lacoste

</p>
<img src='http://www.hmemagazine.com/img/fl_5.jpg'><p> Blanca y con neones de <br>
  colores, este nuevo <br>
  espacio sigue la l&iacute;nea de <br>
  las tiendas Lacoste de todo <br>
  el mundo. Un concepto de <br>
  tienda... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
</tr>
<tr>
<td colspan='5' style='font-size:18px;color:#DFDE8D;'>New Style</td>
</tr>
<tr>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Spa para cada necesidad </p>
<img src='http://www.hmemagazine.com/img/ns_1.jpg'><p> Saunas Dur&aacute;n acaba de <br>
  incorporar a su gama de <br>
  modelos de spas de l&iacute;nea <br>
  privada los modelo Pacific, <br>
  Antartic, Atl&aacute;ntida y <br>
  Ocean. El primero es el... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'>Ceras de 3&ordf; generaci&oacute;n </p>
<img src='http://www.hmemagazine.com/img/ns_2.jpg'><p>Ellos tambi&eacute;n se cuidan y <br>
  cada vez es mayor el <br>
  n&uacute;mero de hombres que <br>
  desea tener una piel <br>
  suave, libre de pelos... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Instituts Saurina </p>
<img src='http://www.hmemagazine.com/img/ns_3.jpg'><p> Para este a&ntilde;o 2007, <br>
  Matilde Saurina propone <br>
  un plan con nombre <br>
  propio, es decir, un <br>
  tratamiento... <br>
<br>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
</td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold; font-size:10px; background:#EEEEEE;'>Pies mimados con Beter

</p>
  <p><img src='http://www.hmemagazine.com/img/ns_4.jpg'><br>
      <br>
Beter ofrece los mejores <br>
productos para cuidar los <br>
pies y poder lucirlos <br>
perfectos. La l&iacute;nea de <br>
herramientas de pedicura <br>
Beter integra tijeras, <br>
corta&uacute;&ntilde;as y cortacallos de <br>
alta calidad para asegurar <br>
un corte regular y preciso, <br>
as&iacute; como una gran <br>
variedad de limas que se <br>
adaptan a...<br>
<br>
    <br>
    <a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
  </td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Freixenet Saunasport </p>
<img src='http://www.hmemagazine.com/img/ns_5.jpg'><p> Freixanet Saunasport, la <br>
  empresa espa&ntilde;ola <br>
  especializada en dise&ntilde;o y <br>
  construcci&oacute;n de &Aacute;reas <br>
  Wellness, estar&aacute; presente <br>
  en el Sal&oacute;n Internacional <br>
  de la Piscina 2007... </p>
<p><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
<p style='margin-bottom: 0;'>&nbsp;</p></td>
</tr>
<tr>
<td colspan='5' style='font-size:18px;color:#B53719;'>Agenda</td>
</tr>
<tr>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> M&aacute;sQUEagua Moncloa </p>
<img src='http://www.hmemagazine.com/img/ag_1.jpg'><p> La capital espa&ntilde;ola ha <br>
  acogido recientemente la <br>
  apertura de un nuevo <br>
  centro dedicado al cuidado <br>
  del cuerpo y la salud, con <br>
  una completa oferta de <br>
  servicios en est&eacute;tica... </p>
<p><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
</td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Termatalia 2007 </p>
<img src='http://www.hmemagazine.com/img/ag_2.jpg'><p> La Feria Internacional de <br>
  Turismo Termal, <br>
  Termatalia, se celebrar&aacute; <br>
  en Expourense entre <br>
  los d&iacute;as 19 al 21 de <br>
  octubre de 2007, <br>
  siendo Uruguay... </p>
<p><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
</td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Expolook </p>
<img src='http://www.hmemagazine.com/img/ag_3.jpg'><p> Los d&iacute;as 29 y 30 de <br>
  septiembre, el Recinto <br>
  Feria de Santiago de <br>
  Compostela acogi&oacute; la IV <br>
  edici&oacute;n de Expolook, el <br>
  Sal&oacute;n Internacional de <br>
  Peluquer&iacute;a y Est&eacute;tica, la <br>
  cita m&aacute;s relevante... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'>SAL&Oacute;N LOOK </p>
<img src='http://www.hmemagazine.com/img/ag_4.jpg'><p> Sal&oacute;n Look Internacional <br>
  es el gran escaparate <br>
  profesional de la belleza y <br>
  la medicina est&eacute;tica y se <br>
  celebrar&aacute; muy pronto, del <br>
  26 al 28 de octubre en <br>
  Feria de Madrid. Son <br>
  muchas las novedades 
  que <br>
  se van a presentar... </p>
<p><a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></p>
<p style='margin-bottom: 0;'>&nbsp;</p></td>
<td valign='top' bgcolor='#FFFFFF'><p style='font-weight:bold;font-size:10px;background:#EEEEEE;'> Relax mediterr&aacute;neo </p>
<img src='http://www.hmemagazine.com/img/ag_5.jpg'><p> Para quienes buscan tratar <br>
  de alargar las vacaciones, <br>
  lejos del tumulto de las <br>
  grandes ciudades y de los <br>
  compromisos de la <br>
  agenda, nada como <br>
  escaparse al sur de <br>
  Espa&ntilde;a, entre... </p>
<a style='text-decoration:none;color:#4478B2;' href='http://www.hmemagazine.com'>+ Leer m&aacute;s...</a></td>
</tr>
<tr>
<td colspan='5' align='center'><b>Revista HME Hombre</b> | Ediciones 3ideas | C/ San Antoni, 97, Bajos 2� 08880 Cubelles (Barcelona) Espa&ntilde;a<br>
  Telf: +34 93 895 65 72| Fax: 93 895 67 64| <a style='text-decoration:none;color:#4478B2;' href='mailto:administracion@hmemagazine.com'>administracion@hmemagazine.com</a></td>
</tr>
<tr>
  <td colspan='5' align='center'><b><em>* </em></b>Pr&oacute;xima edici&oacute;n:<em> Moda - Complementos</em></td>
</tr>
<tr>
	<td colspan='5' align='center' style='color:#BBBBBB;'>Si deseas desinscribierte, pulsa ";

$conexion=mysql_connect("mysql5.gestionar.info","ab5010","hme000026");
mysql_select_db("ac0940");

$resultado=mysql_query("select email from usuarios order by email");
echo "<h2>".mysql_num_rows($resultado)."</h2>";
/*for($i=0;$i<mysql_num_rows($resultado);$i++)
{
	$fila=mysql_fetch_row($resultado);	
	echo "--- ".$fila[0]."<br>";
	
	if( (mail($fila[0], "HME Newsletter", $texto."<a style='text-decoration:none;color:#BBBBBB;' href='http://hmemagazine.com/newsletter/unsuscribe.php?user=".$fila[0]."'>aqu&iacute;</a></td></tr></table>", "From: newsletter@hmemagazine.com\nContent-type: text/html\n")) )
			echo "ENVIADO A: ".$fila[0]."<br>";
	else
			echo "ERROR : ".$fila[0]."<br>";
}*/

//if( (mail("omar@indianwebs.com", "Revista HME HOMBRE", $texto."<a style='text-decoration:none;color:#BBBBBB;' href='http://hmemagazine.com/newsletter/unsuscribe.php?user=".$fila[0]."'>aqu&iacute;</a></td></tr></table>", "From: newsletter@hmemagazine.com\nContent-type: text/html\n")) )
//	echo "El mail se ha enviado a omar";

mysql_close($conexion);
?>
</center>

</body>
</html>

