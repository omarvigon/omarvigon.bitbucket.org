<tr>
	<td class="cajamid">
		<table cellpadding="0" cellspacing="0">
		<tr>
		<td>
			<a class="bt1" href="http://www.hmemagazine.com/hme-personal-care.php" title="HME Personal Care"><p><em>PERSONAL CARE</em><br>La piel del hombre se merece unos cuidados espec&iacute;ficos, de  ah&iacute; que cada vez m&aacute;s firmas de cosm&eacute;tica saquen al mercado productos destinados a realzar su est&eacute;tica...</p></a>
			<a class="bt2" href="http://www.hmemagazine.com/hme-hair-trends.php" title="HME Hairtrends"><p><em>HAIR TRENDS</em><br>Las colecciones m&aacute;s actuales de los n&uacute;mero uno en peluquer&iacute;a,  as&iacute; como las &uacute;ltimas novedades del sector, consejos y cursos se dan cita en esta secci&oacute;n...</p></a>
			<a class="bt3" href="http://www.hmemagazine.com/hme-wellness.php" title="HME Wellness"><p><em>WELLNESS</em><br>El cliente masculino est&aacute; creciendo en los centros de  belleza, spas, etc. donde busca el m&aacute;ximo bienestar, mientras cuida su cuerpo  con tratamientos exclusivos...</p></a>
			<a class="bt4" href="http://www.hmemagazine.com/hme-selection.php" title="HME Selection"><p><em>SELECTION</em><br>Para estar a la &uacute;ltima en moda, fotograf&iacute;a, tecnolog&iacute;a,  hoteles, deportes, relojes, telefon&iacute;a, libros, restaurantes... Ser&aacute;s el primero  en conocerlo todo...</p></a>
			<a class="bt5" href="http://www.hmemagazine.com/hme-agenda.php" title="HME Agenda"><p><em>AGENDA</em><br>El calendario anual est&aacute; repleto de citas imprescindibles  para los profesionales de la est&eacute;tica, cosm&eacute;tica, peluquer&iacute;a y relax y aqu&iacute;  podr&aacute;s tomar nota de cada detalle...</p></a>
		</td>
		<td>
			<p><a href="javascript:suscrip()"><img src="http://www.hmemagazine.com/img/boton7a.gif" border="0" title="HME Hombre - La revista profesional del ciudado personal del hombre" alt="HME Hombre - La revista profesional del ciudado personal del hombre"></a></p>
			<p><a href="http://www.hmemagazine.com/hme-newsletter.php"><img src="http://www.hmemagazine.com/img/boton7.gif" border="0" title="HME Magazine Revista de cosmetica masculina" alt="HME Magazine Revista de cosmetica masculina"></a></p>
			<div class="marquesina">
				<marquee scrolldelay="20" scrollamount="1">
				<a href="http://www.dunasoftpc.com" target="_blank"><img src="http://www.hmemagazine.com/img/logo_dunasoft.gif" hspace="4" border="0" title="HME Magazine Belleza del hombre"></a>
				<a href="http://www.ofi.es/software/ofipeluq.php" target="_blank"><img src="http://www.hmemagazine.com/img/logo_ofi.gif" hspace="4" border="0" title="HME Hombre Depilacion masculina"></a>
				<a href="http://www.ref-haircare.es" target="_blank"><img src="http://www.hmemagazine.com/img/logo_ref.jpg" hspace="4" border="0" title="HME Magazine Belleza del hombre"></a>
				<a href="http://www.depileve.com" target="_blank"><img src="http://www.hmemagazine.com/img/logo_Depileve.jpg" hspace="4" border="0" title="HME Hombre Depilación masculina"></a>
				<a href="http://www.guapo-guapo.com" target="_blank"><img src="http://www.hmemagazine.com/img/logo-guapo.jpg" hspace="4" border="0" title="HME Hombre Ropa interior masculina"></a>			
				<a href="http://www.corporacioncapilar.es" target="_blank"><img src="http://www.hmemagazine.com/img/logo_Corporacion_Capilar.gif" hspace="4" border="0" title="HME Hombre cosmetica masculina"></a>
				<a href="http://www.lacoste.com/es/intro.html" target="_blank"><img src="http://www.hmemagazine.com/img/logo_lacoste.gif" hspace="4" border="0" title="HME Hombre Ciudado personal"></a>
				<a href="http://www.cebado.es" target="_blank"><img src="http://www.hmemagazine.com/img/logo_cebado.gif" hspace="4" border="0" title="HME Magazine Belleza del hombre"></a>
				<a href="http://www.planethair.biz/idioma.asp" target="_blank"><img src="http://www.hmemagazine.com/img/logo_planethair.gif" hspace="4" border="0" title="HME Magazine Ciudado del cabello"></a>
				<a href="http://www.carfer.com" target="_blank"><img src="http://www.hmemagazine.com/img/logo_carfer.gif" hspace="4" border="0" title="HME Hombre Moda masculina"></a>
				<a href="http://www.hombreactual.es" target="_blank"><img src="http://www.hmemagazine.com/img/logo_hombreactual.gif" hspace="4" border="0" title="HME Hombre Ciudado personal"></a>
				<a href="http://www.corpora.es" target="_blank"><img src="http://www.hmemagazine.com/img/logo_corpora.gif" hspace="4" border="0" title="HME Hombre La revista profesional"></a>
				<a href="http://www.farmavent.com" target="_blank"><img src="http://www.hmemagazine.com/img/logo_farmavent.gif" hspace="4" border="0" title="HME Hombre Ciudado personal"></a>
				</marquee>
            </div>
			<p><a href="img/fedcat_hme.pdf" target="_blank"><img src="http://www.hmemagazine.com/img/boton9.gif" border="0" alt="Fedcat y HME Hombre se unen para ofrecer mas servicios al peluquero y el esteticista" title="Fedcat y HME Hombre se unen para ofrecer mas servicios al peluquero y el esteticista"></a></p>
		</td>
		</tr>
		</table>
	</td>
</tr>

<?php if($_SERVER['PHP_SELF']=="/index.php") { ?>
<tr>
	<td class="noticias">
    	<p><b>TITULARES SEPTIEMBRE 2008</b></p>
        <table cellpadding="0" cellspacing="0"><tr>
        <td><a href="http://www.hmemagazine.com/noticias/cuidado-cabello.php"><b>El cuidado del cabello</b><br />El objetivo es mantenerlo sano y fuerte. Se lleve largo, corto, rizado, liso... El cabello es una se&ntilde;a de identidad...</a></td>
        <td><a href="http://www.hmemagazine.com/noticias/caida-cabello-varon.php"><b>La ca&iacute;da del cabello en el var&oacute;n</b><br />El 25% de los varones espa&ntilde;oles habr&aacute; perdido parte de su cabellera a los 25 a&ntilde;os, y el 40% presentar&aacute; una p&eacute;rdida importante...</a></td>
        <td><a href="http://www.hmemagazine.com/noticias/estres-en-el-hombre.php"><b>Estr&eacute;s en el Hombre</b><br />El estr&eacute;s es uno de los trastornos m&aacute;s comunes de esta &eacute;poca. Las causas habr&iacute;a que buscarlas...</a></td>
        <td><a href="http://www.hmemagazine.com/noticias/mejor-momento-para-hacer-deporte.php"><b>&iquest;Cu&aacute;l es el mejor momento para hacer Deporte?</b><br />Nuestro reloj biol&oacute;gico controla tambi&eacute;n el rendimiento muscular, cardiovascular y la motivaci&oacute;n a la hora de hacer ejercicio...</a></td>
        <td><a href="http://www.hmemagazine.com/noticias/lesiones-en-el-deporte.php"><b>Lesiones en el Deporte</b><br />Todas las personas tienen los tejidos susceptibles de lesionarse por debilidad intr&iacute;nseca, sobrecarga o factores biomec&aacute;nicos...</a></td>
        </tr>
        </table>

	</td>
</tr>
<? } ?>

<tr>
	<td class="piecera">
		<img src="http://www.hmemagazine.com/img/banner1.gif" alt="HME Cuidado personal del hombre" title="HME Cuidado personal del hombre">
		<embed src="http://www.hmemagazine.com/flash/banner_2.swf" quality="high" width="162" height="140"></embed>
		<embed src="http://www.hmemagazine.com/flash/guapo.swf" quality="high" width="162" height="140"></embed>
		<img src="http://www.hmemagazine.com/img/hme-fedcat.jpg" width="162" height="140" border="0" alt="HME Hombre y FEDCAT se unen para ofrecer mas servicios al peluquero y el esteticista" title="HME Hombre y FEDCAT se unen para ofrecer mas servicios al peluquero y el esteticista"></a> 
		<a href="http://www.depileve.com/" target="_blank"><img src="http://www.hmemagazine.com/flash/bronzewax.jpg" width="162" height="140" border="0" alt="HME Tratamientos depilatorios para hombre" title="HME Tratamientos depilatorios para hombre"></a>

    	<p align="center"><a href="http://www.indianwebs.com" target="_blank" title="Dise&ntilde;o Web Barcelona" style="font:10px Arial, Helvetica, sans-serif;color:#999999;text-decoration:none;">Dise&ntilde;o Web Barcelona</a></p>
    </td>
</tr>
</table>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">_uacct = "UA-1394015-1";urchinTracker();</script>
</body>
</html>



