<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		

		<h1>Sal&oacute;n Look 2007 calienta motores</h1>

		<p style="margin-bottom: 0;">Sal&oacute;n Look Internacional es el gran escaparate profesional de la belleza y la medicina est&eacute;tica y se celebrar&aacute; muy pronto, del 26 al 28 de octubre en Feria de Madrid. Son muchas las novedades que se van a presentar en el certamen, destacando un nuevo pabell&oacute;n de Salud, Belleza y Bienestar, abierto a todo el p&uacute;blico. El objetivo con ello es aunar de forma equilibrada estos tres conceptos como respuesta al creciente inter&eacute;s que existe en la sociedad actual acerca del cuidado personal, tanto de hombres como de mujeres. Centros de nutrici&oacute;n, cl&iacute;nicas anti-aging, masajes, balneoterapia, fisioterapia, etc. son algunas de las propuestas que el visitante podr&aacute; conocer de cerca en el recinto ferial, donde acudir&aacute;n m&aacute;s de 500 firmas de profesionales. El Forum de la Calidad de Vida que tendr&aacute; lugar durante la celebraci&oacute;n del sal&oacute;n acoger&aacute; distintas conferencias impartidas por reputados especialistas, adem&aacute;s de ofrecer la posibilidad de participar en un taller de automaquillaje, hacerse un chequeo express de tensi&oacute;n y glucosa, presenciar talleres de moda y shows y, c&oacute;mo no, numerosas demostraciones est&eacute;ticas. El Sal&oacute;n Look Internacional acogi&oacute; en su anterior edici&oacute;n a m&aacute;s de 73.600 visitantes, tanto profesionales como p&uacute;blico en general. El certamen es considerado la primera feria profesional de la imagen y la est&eacute;tica integral en Espa&ntilde;a y tiene tras de s&iacute; una imparable trayectoria que alcanza ya la d&eacute;cada de existencia.</p>

		<p style="margin-top: 0;"><br>

		  M&aacute;s informaci&oacute;n: 902 221 515</p>

		<h1 style="margin-bottom: 0;"><br>

		  </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>