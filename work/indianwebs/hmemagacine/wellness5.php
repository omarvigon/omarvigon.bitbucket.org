<?php include("00cabecera.php");?>
<tr>
	<td class="cajacentro">

		<div class="interior3">

		<h1>Technogym : Un metro cuadrado de dise&ntilde;o</h1>
		<img src='img/wellness5.jpg' alt="HME Magazine Wellness" title="HME Magazine Wellness" align="left" hspace="10">
		<p style="margin-bottom: 0;">Kinesis Personal by Technogym&reg; introduce el nuevo concepto de entrenamiento: Soft Workout. Se trata de un objeto de arte que sincroniza con el mundo moderno del dise&ntilde;o integr&aacute;ndose en cualquier espacio (spas, hoteles, etc.), a la vez que proporciona la posibilidad de m&aacute;s de 300 ejercicios en menos de un metro cuadrado. Kinesis Personal mueve el cuerpo y la mente y est&aacute; preparada para trabajar la fuerza, flexibilidad y equilibrio.</p>

		<p> M&aacute;s informaci&oacute;n: www.technogym.com</p>

	  </div>

	</td>
</tr>
<?php include("00piecera.php");?>