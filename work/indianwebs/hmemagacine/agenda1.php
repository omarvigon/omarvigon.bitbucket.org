<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		<h1>M&aacute;sQUEagua Moncloa abre sus puertas en Madrid</h1>

		<p style="margin-bottom: 0;">La capital espa&ntilde;ola ha acogido recientemente la apertura de un nuevo centro dedicado al cuidado del cuerpo y la salud, con una completa oferta de servicios en est&eacute;tica, peluquer&iacute;a, relajaci&oacute;n y masaje terap&eacute;utico. M&aacute;sQUEagua Moncloa presume de una ubicaci&oacute;n c&eacute;ntrica y recrea un nuevo concepto de spa urbano que permite a los clientes cuidarse durante todo el a&ntilde;o, combinando las propiedades del agua con la diversificaci&oacute;n en el resto de sus servicios. El objetivo es potenciar los h&aacute;bitos de vida m&aacute;s saludables y agradables, de ah&iacute; que M&aacute;sQUEagua proponga seis sencillos consejos para cuidarse por dentro y por fuera: No dejarse vencer por el estr&eacute;s, Cuidarse por dentro y por fuera, Experimentar nuevas sensaciones, Acabar con esa molestia, Que el bronceado dure todo el a&ntilde;o y Un cambio de imagen. La guinda a todas estas propuestas la pone la zona de descanso y relax donde se puede tomar un zumo. Todo ello sin olvidar que M&aacute;sQUEagua Moncloa trabaja con la l&iacute;nea de productos Selvert Termal, un laboratorio suizo cuyas formulaciones est&aacute;n destinadas a cuidar y proteger la piel de la agresividad de su entorno.</p>

		<p style="margin-top: 0;"><br>

		  M&aacute;s informaci&oacute;n: 915 445 385</p>

		<p style="margin-bottom: 0;">&nbsp;</p>

<h1 style="margin-top: 0; margin-bottom: 0;"><br>

		  </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>