<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		

		<h1>Uruguay es el pa&iacute;s invitado en Termatalia 2007</h1>

		<p style="margin-bottom: 0;">La Feria Internacional de Turismo Termal, Termatalia, se celebrar&aacute; en Expourense entre los d&iacute;as 19 al 21 de octubre de 2007, siendo Uruguay el pa&iacute;s invitado en esta s&eacute;ptima edici&oacute;n del certamen, reforzando con ello su papel de puente de conexi&oacute;n entre el termalismo europeo y el latinoamericano. Exposiciones, premios, conferencias, masajes y descuentos ser&aacute;n parte del atractivo del sal&oacute;n gallego. Termatalia es la &uacute;nica Feria Internacional del Turismo Termal de la Pen&iacute;nsula Ib&eacute;rica, de ah&iacute; que se haya convertido en referente mundial para los profesionales del sector, con la participaci&oacute;n de expositores de cuatro continentes. Debido tambi&eacute;n a este papel relevante, el sal&oacute;n se convierte en la plataforma ideal para difundir todos los productos y servicios relacionados con el turismo termal, la talasoterapia y los spas (tratamientos, aparatolog&iacute;a, cosm&eacute;tica, etc.), sin olvidar los tres ejes de actuaci&oacute;n del agua en los que se basa esta industria: mineral, medicinal y termal. Por otra parte, Termatalia asume un firme compromiso con el agua y se propone potenciar el aprovechamiento de los recursos hidr&aacute;ulicos fomentando un uso racional y sumergir as&iacute; a sus habitantes en la Cultura del agua. Acciones consolidadas como la Bolsa de Contrataci&oacute;n, Misiones Comerciales de importadores y touroperadores, el Encuentro Internacional sobre Agua y Termalismo con presencia de m&aacute;s de medio centenar de expertos, el Bar de Aguas y la recreaci&oacute;n de un Espacio Balneario ampl&iacute;an el potencial del certamen para convertirlo en una cita ineludible del sector mundial.</p>

		<p style="margin-top: 0;"><br>

		  M&aacute;s informaci&oacute;n: 988 366 030</p>

		<h1 style="margin-bottom: 0;"><br>

	      </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>