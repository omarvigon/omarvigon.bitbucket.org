<?php include("00cabecera.php");?>

<tr>

	<td class="cajacentro">

		<div class="interior3">

		

		<h1>Recuperaci&oacute;n post-verano con el Pack Seasons Fall 2007 de Mesoestetic</h1>

		<p style="margin-bottom: 0;">Con la llegada del oto&ntilde;o, son perceptibles los efectos de las horas pasadas al aire libre durante el verano y, por ello, es el momento para aplicar los cuidados necesarios con tratamientos cosm&eacute;ticos que promuevan la regeneraci&oacute;n de la piel y la recuperaci&oacute;n de la tersura y flexibilidad natural. Mesoestetic, la divisi&oacute;n de Cosm&eacute;tica M&eacute;dica del grupo Body_Esthetic Laboratories, como parte de su programa de tratamientos Seasons, lanza el nuevo Pack Seasons Fall 2007, un programa de protecci&oacute;n integral desarrollado en tres frentes: exfoliaci&oacute;n, regeneraci&oacute;n e hidrataci&oacute;n. El pack se compone de: Body Milk con a-hidroxi&aacute;cidos, una emulsi&oacute;n hidratante que protege la piel manteniendo y restaurando el manto hidrolip&iacute;dico natural; Gel Regenerador, una crema facial con propiedades regeneradoras y alto poder revitalizante; Mask Face 3, una mascarilla de acci&oacute;n exfoliante-purificante que elimina las c&eacute;lulas muertas de la epidermis; y, por &uacute;ltimo, Radiance Bx finishing lines, un cosm&eacute;tico revolucionario que consigue una reducci&oacute;n de las l&iacute;neas de expresi&oacute;n. </p>

		<p style="margin-top: 0;"><br>

		  M&aacute;s informaci&oacute;n: 900 101 099</p>

		<h1><br>

	      </h1>

		<h1 style="margin-bottom: 0;"><br>

		  </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>