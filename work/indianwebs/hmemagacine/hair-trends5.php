<?php include("00cabecera.php");?>
<tr>

	<td class="cajacentro">

		<div class="interior3">

		<h1>Cabellos protegidos bajo el sol con Bottega Verde</h1>

        <p style="margin-bottom: 0;">La firma de origen italiano especializado en cosm&eacute;tica y belleza natural, Bottega Verde, presenta nuevos productos para el cuidado del cabello, el rostro, el cuerpo y maquillaje. Todos ellos se basan en principios activos vegetales, testados dermatol&oacute;gicamente y garantizados con la certificaci&oacute;n ISO 9002/2000. En concreto, el champ&uacute; solar nutritivo y protector con prote&iacute;nas de trigo es un producto que elimina eficazmente los restos de salitre, cloro y arena y contiene un filtro UV para lucir un pelo brillante y fuerte incluso bajo el sol. Adem&aacute;s, cuenta con una selecci&oacute;n de prote&iacute;nas procedentes del trigo que forman una eficaz barrera contra las agresiones externas y garantiza la suavidad y luminosidad del cabello. Se presenta en envases de 200 ml. y puede adquirirse en cualquiera de las 21 tiendas que la firma tiene en Espa&ntilde;a.</p>

        <p style="margin-top: 0;"><br>

          M&aacute;s informaci&oacute;n: 952 610 404</p>

        <h1 style="margin-bottom: 0;"><br>

        </h1>

	  </div>

	</td>

</tr>
<?php include("00piecera.php");?>