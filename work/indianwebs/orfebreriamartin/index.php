<?php include ("cabecera.php"); ?>

    <tr>
        <td class="co_left" valign="middle">
            <center><script type="text/javascript">
            new fadeshow(fadeimages, 400, 250, 0, 4000, 1)
            </script></center>
        </td>
        <td class="co_right">
        	<p>Desde 1965, fecha de fundaci&oacute;n de la empresa, nos hemos dedicado a la fabricaci&oacute;n de art&iacute;culos en plata de 1&ordf;ley , 925 o/oo, a dicho material noble hemos aportado nuestra experiencia artesanal para conseguir una optima relaci&oacute;n calidad - precio. </p>
            <p>Con el paso de los a&ntilde;os y correspondiendo a las variaciones propias, hemos aumentado la oferta, tanto de  modelos como de dise&ntilde;os y combinaciones con materiales afines, consiguiendo actualmente una amplia gama de articulos , como la ultima  incorporaci&oacute;n de trofeos y regalos de empresa personalizados.</p>
            <p>Os invitamos a un recorrido por parte de nuestros productos, sed bienvenidos.</p>
          <p><a href="mailto:plata@orfebreriamartin.com"><em>plata@orfebreriamartin.com</em></a><em>Tel: 93 436.49.85<br>Fax: 93 456.47.74</em></p> 
        </td>
    </tr>

<?php include ("piecera.php"); ?>