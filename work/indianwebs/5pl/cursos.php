<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
    <h1 align="center">Calendario de Cursos del Primer Trimestre</h1>
    <h4 align="center">Cons&uacute;ltenos los horarios</h4>
    <table cellpadding="6" cellspacing="0" align="center">
    <tr>
    	<td><em><b>Curso:</b></em></td>
        <td class="gris2"><b>MARZO</b></td>
        <td>&nbsp;</td>
        <td><em><b>Curso:</b></em></td>
        <td class="gris2"><b>ABRIL</b></td>
        <td>&nbsp;</td>
        <td><em><b>Curso:</b></em></td>
        <td class="gris2"><b>MAYO</b></td>
    </tr>
    <tr>
    	<td class="gris3"><br />
    	  <br />Dirección y Gestión de Proyectos <br />(5 sesiones)</td>
      <td class="gris"><br />
        <br />
      13 -16-17- 23-24 Bilbao</td>
      <td>&nbsp;</td>
        <td class="gris3">Modelo de Compensación Integral: I Política de Compensación y Beneficios   II Sistema de Compensación Variable  III Sist. Compensación no económicos y en &quot;especies&quot;            (4 sesiones)</td>
      <td class="gris"><br />
        <br />
        <br />
      15-22-29 Bilbao y  06 Mayo</td>
      <td>&nbsp;</td>
        <td class="gris3"><br />
          <br />
          Lean Manufacturing Avanzado          <br />
        (4 sesiones)</td>
      <td class="gris"><br />
        <br />
      13-20-27 Bilbao  y 03 de Junio</td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td class="gris3"><br />
          <br />
        Gestión de conflicto de intereses: Negociación         (2 sesiones)</td>
        <td class="gris"><br />
          <br />
        17-24 Bilbao</td>
      <td>&nbsp;</td>
        <td class="gris3">Politica Comercial: Estrategia, Herramientas y Remuneración y Motivación de equipos de ventas               <br />
        (3 sesiones)</td>
      <td class="gris"><br />
        <br />
      08-15-22 Bilbao</td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="gris3">Dirección Estratégica, Relaciones Laborales y Seguridad Social <br />( 4 sesiones)</td>
      <td class="gris"><br />PROXIMAMENTE</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<tr>
    	<td colspan="8">&nbsp;</td>
    </tr>
    <tr>
    	<td><em><b>Curso:</b></em></td>
        <td class="gris2"><b>JUNIO</b></td>
        <td>&nbsp;</td>
        <td><em><b>Curso:</b></em></td>
        <td class="gris2"><b>JULIO</b></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
    	<td class="gris3">Reducción de Costes; almacenes y transportes       <br />
   	    (2 sesiones)</td>
      <td class="gris"><br />
      8-10 Bilbao</td>
      <td>&nbsp;</td>
        <td class="gris3">          Análisis y Valoración de Cias.                <br />
        (3 sesiones)</td>
      <td class="gris"><br />
      15-22-29 Bilbao</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
    	<td class="gris3">Métodos y aplicaciones para reducción de Stocks                    <br />
   	    ( 3 sesiones)</td>
      <td class="gris"><br />
      05-12-19 Bilbao</td>
	  <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
    	<td class="gris3">Diseño de Servicios <br />
   	    (4 sesiones)</td>
      <td class="gris">26 Bilbao y 03-10-17 Julio</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
        <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="gris3">Foreign Trade; Riesgos y Oportunidades <br />
      (4 sesiones)</td>
      <td class="gris">17-24 Bilbao 1-8 Julio</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
</div>
<?php include("includes/3piecera.php");?>