<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
    <div id="izq">
   		<a title="" <?php if($_GET["sec"]=='inscripcion') echo 'style="color:#ff6600;background:#000000;"';?> href="matricula.php?sec=inscripcion">&raquo; Solicitud de Inscripci&oacute;n</a>
    	<a title="" <?php if($_GET["sec"]=='matriculacion') echo 'style="color:#ff6600;background:#000000;"';?> href="matricula.php?sec=matriculacion">&raquo; Impreso de Matriculaci&oacute;n</a>
    </div>
    <div id="der">
    <?php switch($_GET["sec"])
	{
		default:?>		
        <img title="" src="img/sec_clientes.jpg" class="img_der" alt="" />
        <h1>Procedimiento  de matriculaci&oacute;n</h1>
		<p>Las personas interesadas en realizar  alguna unidad formativa, deben cumplimentar la <a title="" href="matricula.php?sec=inscripcion"><em>Solicitud de Inscripci&oacute;n</em></a> on line.</p>
		<p>Como comprobaci&oacute;n de su recepci&oacute;n se  remitir&aacute; comunicaci&oacute;n con los datos de su solicitud.</p>
		<p>Estudiada y aprobada la solicitud,  se podr&aacute; iniciar Matriculaci&oacute;n, cumplimentando el <a title="" href="matricula.php?sec=matriculacion"><em>Impreso de Matriculaci&oacute;n</em></a> on line.</p>
		<p>En breve recibir&aacute; un correo con la  confirmaci&oacute;n de su matricula y convoc&aacute;ndole en lugar, d&iacute;a y hora para la  realizaci&oacute;n de la jornada.</p>
		<? break;
		
		case "inscripcion": 
		if($_POST["email"]) 
      	{ 	
          	foreach($_POST as $nombre_campo => $valor)
              	$cuerpo=$cuerpo."<b>".$nombre_campo ."</b> : ".$valor."<br>";
          	if(mail("info@5pl.org","Solicitud de Inscripcion",$cuerpo,"From:info@5pl.org\r\nMIME-Version:1.0\r\nContent-type:text/html;charset=utf-8\r\nBcc:omar@indianwebs.com\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
              	echo "<p>Muchas  gracias por su solicitud de inscripci&oacute;n.</p><p>Nos  pondremos en contacto con usted a la mayor brevedad.</p><p>Atentamente</p><p><em>5PL BUSINESS  TRAINING</em></p>";
      	} 
		else { ?>
        <h1>Solicitud de Inscripci&oacute;n</h1>
        <form method="post" name="form_inscripcion" action="matricula.php?sec=inscripcion" style="margin:12px 0 0 48px;">
        <table cellpadding="4" cellspacing="0">
        <tr>
          <td><label>Curso</label></td>
          <td><select name="curso" multiple="multiple" style="width:500px;height:200px;background:#000000;color:#ffffff;">
			  <option value="ninguno" selected="selected">(Seleccionar...)</option>
			  <?php include("includes/inc.cursos.php");?>
      		  </select></td>
        </tr>
        <tr>
          <td><label>Nombre y apellidos (*)</label></td>
          <td><input type="text" name="nombre" class="caja2" />  </td>
        </tr>
        <tr>
          <td><label>Direcci&oacute;n (*)</label></td>
          <td><input type="text" name="direccion" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Poblaci&oacute;n</label></td>
          <td><input type="text" name="poblacion" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Provincia</label></td>
          <td><input type="text" name="provincia" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Pais</label></td>
          <td><input type="text" name="pais" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>C&oacute;digo Postal</label></td>
          <td><input type="text" name="cp" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>E-Mail (*)</label></td>
          <td><input type="text" name="email" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Tel&eacute;fono (*)</label></td>
          <td><input type="text" name="telefono" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Movil</label></td>
          <td><input type="text" name="movil" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Empresa</label></td>
          <td><input type="text" name="empresa" class="caja2" /></td>
        </tr>
        <tr>
          <td><label>Puesto</label></td>
          <td><input type="text" name="puesto" class="caja2" /></td>
        </tr>
        <tr>
        	<td colspan="2"><h4>BREVE CURRICULUM VITAE</h4></td>
        </tr>
        <tr>
          <td><label>Expediente acad&eacute;mico</label></td>
          <td><textarea name="curriculum_1" rows="16" class="caja2txtarea"></textarea></td>
        </tr>
        <tr>
          <td><label>Expediente Profesional</label></td>
          <td><textarea name="curriculum_2" rows="16" class="caja2txtarea"></textarea></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="checkbox" name="aceptar" />Aceptar las condiciones legales (*)</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>(*) Campos obligatorios<br /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="button" onclick="validar_inscripcion()" value="ACEPTAR"/></td>
        </tr>
        </table>
        </form>
        <? } break;
		
		case "matriculacion":
		if($_POST["email"]) 
      	{ 	
          	foreach($_POST as $nombre_campo => $valor)
              	$cuerpo=$cuerpo."<b>".$nombre_campo ."</b> : ".$valor."<br>";
          	if(mail("info@5pl.org","Impreso de matriculacion",$cuerpo,"From:info@5pl.org\r\nMIME-Version:1.0\r\nContent-type:text/html;charset=utf-8\r\nBcc:omar@indianwebs.com\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
              	echo "<p>Muchas  gracias por su impreso de matriculacion.</p><p>Nos  pondremos en contacto con usted a la mayor brevedad.</p><p>Atentamente</p><p><em>5PL BUSINESS  TRAINING</em></p>";
      	} 
		else { ?>
        <h1>Impreso de matriculaci&oacute;n</h1>        
        <form method="post" name="form_matriculacion" action="matricula.php?sec=matriculacion" class="f1">
        
        <h4>Datos personales</h4>
        <table cellpadding="0" cellspacing="4">
        <tr> 
        	<td><label>Nombre y apellidos (*)</label>
        	<input type="text" name="nombre" class="caja2"/>        
            </td>
            <td><label>Curso</label>
            	<select name="curso" style="width:400px;background:#000000;color:#ffffff;">
                <option value="ninguno" selected="selected">(Seleccionar...)</option>
			  	<?php include("includes/inc.cursos.php");?>
      		  	</select></td>
        </tr>
        <tr>
        	<td><label>Direcci&oacute;n (*)</label>
        	<input type="text" name="direccion" class="caja2" /></td>
        	<td><label>Poblaci&oacute;n</label>
        	<input type="text" name="poblacion" class="caja2" /></td>
        </tr>
        <tr>
        	<td><label>Provincia</label>
	        <input type="text" name="provincia" class="caja2" /></td>
            <td><label>C&oacute;digo Postal</label>
        	<input type="text" name="cp" class="caja2" /></td>
        </tr>
        <tr>
    	    <td><label>Pais</label>
        	<input type="text" name="pais" class="caja2" /></td>
	        <td><label>E-Mail (*)</label>
    	    <input type="text" name="email" class="caja2" /></td>
        </tr>
        <tr>    
        	<td><label>Tel&eacute;fono (*)</label>
        	<input type="text" name="telefono" class="caja2" /></td>
        	<td><label>Movil</label>
        	<input type="text" name="movil" class="caja2" /></td>
        </tr>    
        <tr>
        	<td><label>Empresa</label>
        	<input type="text" name="empresa" class="caja2" /></td>
       		<td><label>Direcci&oacute;n de la empresa</label>
        	<input type="text" name="empresadireccion" class="caja2" /></td>
        </tr>
        <tr>
        	<td><label>Cargo</label>
        	<input type="text" name="cargo" class="caja2" /></td>
        	<td><label>Sector</label>
        	<input type="text" name="sector" class="caja2" /></td>
        </tr>
        </table>
        
        <h4>Datos para la factura</h4>
        <table cellpadding="0" cellspacing="4">
        <tr>
        	<td><label>Nombre y apellidos</label>
        	<input type="text" name="nombrefactura" class="caja2" /></td>
            <td><label>NIF/CIF</label>
        	<input type="text" name="niffactura" class="caja2" /></td>
        </tr>    
        <tr>
        	<td><label>Direcci&oacute;n</label>
        	<input type="text" name="direccionfactura" class="caja2" /></td>
        	<td><label>Poblaci&oacute;n</label>
        	<input type="text" name="poblacionfactura" class="caja2" /></td>
        </tr>    
        <tr>
        	<td><label>Provincia</label>
        	<input type="text" name="provinciafactura" class="caja2" /></td>
        	<td><label>C&oacute;digo Postal</label>
        	<input type="text" name="cpfactura" class="caja2" /></td>
        </tr>    
        </table>
        
        <h4>Forma de pago (*)</h4>
        <input type="radio" name="formapago" value="efectivo" />En efectivo<br />
        <input type="radio" name="formapago" value="visa" />Pago por visa<br />
        <input type="radio" name="formapago" value="cheque" />Cheque nominativo a favor de 5PL<br />
        <input type="radio" name="formapago" value="domiciliacion" />Domiciliaci&oacute;n bancaria <input type="text" name="cuenta1" maxlength="4" size="4" onkeyup="comprobar(1)" />-<input type="text" name="cuenta2" maxlength="4" size="4" onkeyup="comprobar(2)" />-<input type="text" name="cuenta3" maxlength="2" size="2" onkeyup="comprobar(3)" />-<input type="text" name="cuenta4" maxlength="10" size="10" /><br />
        
        <table cellpadding="0" cellspacing="4">
        <tr>
        	<td><label>Titular de la cuenta</label></td>
            <td><input type="text" name="titularcuenta" class="caja2" /></td>
        </tr>
        <tr>
        	<td><label>Poblaci&oacute;n</label></td>
            <td><input type="text" name="poblacioncuenta" class="caja2" /></td>
        </tr>
        <tr>
        	<td><br /><label>Comentarios</label></td>
            <td><br /><textarea name="comentario" rows="6" class="caja2txtarea"></textarea></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td><input type="checkbox" name="aceptar" />Aceptar las condiciones legales (*)</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>(*) Campos obligatorios</td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td><input type="button" onclick="validar_matriculacion()" value="ACEPTAR"/></td>
        </tr>
        </table>
        </form>
		<? } break;
	}?>

    </div>
</div>
<?php include("includes/3piecera.php");?>