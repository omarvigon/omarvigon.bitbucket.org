<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
	<table cellpadding="0" cellspacing="6">
	<tr>
    
    <?php switch($_GET["sec"])
	{
		case "jornadas": ?>
        	<td>
            	<a title="" class="bt1" <?php if($_GET["sec"]=='jornadas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=jornadas">&raquo; Jornadas Profesionales</a><br />
                <a title="" class="bt1" <?php if($_GET["sec"]=='cursos') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=cursos">&raquo; Cursos ejecutivos</a><br />
                <a title="" class="bt1" <?php if($_GET["sec"]=='programas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=programas">&raquo; Programas</a></td>
        	<td rowspan="3" width="300">
            <h1>Areas de formaci&oacute;n</h1>
            <cite style="margin-left:10px;">Secci&oacute;n en desarrollo. Para informaci&oacute;n detallada <a href="contacto.php">contacte con nosotros.</a></cite>
            <p>&bull; <a href="#">Comercial – Ventas</a></p>
            <p>&bull; <a href="#">Marketing</a></p>
            <p>&bull; <a href="#">Administraci&oacute;n – Finanzas – Fiscal</a></p>
            <p>&bull; <a href="#">RR HH y pol&iacute;tica de Personal</a></p>
            <p>&bull; <a href="#">Habilidades Directivas</a></p>
            <p>&bull; <a href="#">Operaciones Industriales</a></p>
            <p>&bull; <a href="#">Supply Chain Management</a></p>
            <p>&bull; <a href="#">Comercio Internacional</a></p>
            <p>&bull; <a href="#">Dise&ntilde;o y desarrollo de Servicios</a></p>
            <p>&bull; <a href="#">Jornadas especificas de inter&eacute;s general</a></p>            
       		</td>
        	<td><img title="" src="img/sec_formacion.jpg" alt="" /></td>
		<? break;
		
		case "cursos": ?>
        	<td>
            	<a title="" class="bt1" <?php if($_GET["sec"]=='jornadas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=jornadas">&raquo; Jornadas Profesionales</a><br />
                <a title="" class="bt1" <?php if($_GET["sec"]=='cursos') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=cursos">&raquo; Cursos ejecutivos</a><br />
                <a title="" class="bt1" <?php if($_GET["sec"]=='programas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=programas">&raquo; Programas</a></td>
        	<td rowspan="3" width="300">
            <h1>Cursos ejecutivos</h1>
        	<cite style="margin-left:10px;">Secci&oacute;n en desarrollo. Para informaci&oacute;n detallada <a href="contacto.php">contacte con nosotros.</a></cite>
            <p>&bull; Comercial – Ventas</p>
    		<p>&bull; Marketing</p>
    		<p>&bull; Administraci&oacute;n – Finanzas – Fiscal</p>
    		<p>&bull; Operaciones Industriales</p>
        	<p>&bull; RR HH y pol&iacute;tica de Personal</p>
    		<p>&bull; Supply Chain Management</p>
       		</td>
        	<td><img title="" src="img/sec_formacion.jpg" alt="" /></td>
		<? break;
		
		case "programas": ?>
        	<td>
            	<a title="" class="bt1" <?php if($_GET["sec"]=='jornadas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=jornadas">&raquo; Jornadas Profesionales</a><br />
                <a title="" class="bt1" <?php if($_GET["sec"]=='cursos') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=cursos">&raquo; Cursos ejecutivos</a><br />
                <a title="" class="bt1" <?php if($_GET["sec"]=='programas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=programas">&raquo; Programas</a></td>
        	<td rowspan="3" width="300">
            <h1>Programas</h1>
        	<cite style="margin-left:10px;">Secci&oacute;n en desarrollo. Para informaci&oacute;n detallada <a href="contacto.php">contacte con nosotros.</a></cite>
            <p>&bull; Comercial – Ventas</li>
    		<p>&bull; Marketing</p>
    		<p>&bull; Administraci&oacute;n – Finanzas – Fiscal</p>
    		<p>&bull; Operaciones Industriales</p>
        	<p>&bull; RR HH y pol&iacute;tica de Personal</p>
    		<p>&bull; Supply Chain Management</p>
       		</td>
        	<td><img title="" src="img/sec_formacion.jpg" alt="" /></td>
		<? break;
		
		default: ?>
    		<td><a title="" class="bt1" <?php if($_GET["sec"]=='jornadas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=jornadas">&raquo; Jornadas Profesionales</a></td>
	        <td width="300">Es una unidad formativa de un solo d&iacute;a (excepcionalmente de 2 o m&aacute;s) de 7  horas de duraci&oacute;n.<br /><br /></td>
    	    <td rowspan="3"><img title="" src="img/sec_formacion.jpg" alt="" /></td>
	    </tr>
    	<tr>
	    	<td><a title="" class="bt1" <?php if($_GET["sec"]=='cursos') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=cursos">&raquo; Cursos ejecutivos</a></td>
        	<td>Es una combinaci&oacute;n de Jornadas que definen un itinerario formativo con un  objetivo programado y homog&eacute;neo,&nbsp;con un plazo m&aacute;ximo de realizaci&oacute;n de 2  a&ntilde;os.<br /><br /></td>
		</tr>
	    <tr>
    		<td><a title="" class="bt1" <?php if($_GET["sec"]=='programas') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php?sec=programas">&raquo; Programas</a></td>
	        <td>Tienen la consideraci&oacute;n de Programas aquellos itinerarios transversales a  Cursos Ejecutivos que tienen como misi&oacute;n una visi&oacute;n global de &Aacute;rea, con un  plazo m&aacute;ximo de realizaci&oacute;n de 2 a&ntilde;os.<br />
			<br />Consta de Asignaturas &ldquo;Troncales&rdquo; (obligatorias) y &ldquo;Selectivas&rdquo; (a escoger de  entre el Catalogo del &Aacute;rea).<br /></td>

		<? break;
	}?>
    </tr>
	</table>  
</div>
<?php include("includes/3piecera.php");?>