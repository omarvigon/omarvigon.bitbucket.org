<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
    <div id="izq">
   		<a title="" <?php if($_GET["sec"]=='porque' || !$_GET['sec']) echo 'style="color:#ff6600;background:#000000;"';?> href="presentacion.php?sec=porque">&raquo; &iquest;Por qu&eacute;?</a>
    	<a title="" <?php if($_GET["sec"]=='vision') echo 'style="color:#ff6600;background:#000000;"';?> href="presentacion.php?sec=vision">&raquo; Nuestra visi&oacute;n</a>
        <a title="" <?php if($_GET["sec"]=='mision') echo 'style="color:#ff6600;background:#000000;"';?> href="presentacion.php?sec=mision">&raquo; Nuestra misi&oacute;n</a>    	
    	<a title="" <?php if($_GET["sec"]=='objetivos') echo 'style="color:#ff6600;background:#000000;"';?> href="presentacion.php?sec=objetivos">&raquo; Objetivos</a>
        
        <!-- 
        <marquee scrollamount="2" scrolldelay="60" direction="up">
        	 <p>Viernes 13 de Marzo Direcci&oacute;n de proyectos</p>
        </marquee>-->
    </div>
    <div id="der">
    <?php switch($_GET["sec"])
	{
		default:
		case "porque": ?>
        <img title="" src="img/sec_present.jpg" class="img_der" alt="" />
        <h1>&iquest;Por qu&eacute;?</h1>
        <p>La vida empresarial necesita una formaci&oacute;n en la gesti&oacute;n,  complementaria a la universitaria y a la formativa que proporcionan las  empresas.</p>
        <p>La continua evoluci&oacute;n de los mercados hace necesario tener en cuenta  los puntos de vista de profesionales desarrollados en otros sectores.</p>
		<p>La preparaci&oacute;n intelectual y profesional es fundamental para  conseguir los objetivos empresariales.</p>
        <p>Comenzamos nuestra andadura con un &uacute;nico  pensamiento: la formaci&oacute;n plena para directivos que facilite su evoluci&oacute;n tanto  profesional como personal. Impartida siempre, por profesionales y para  profesionales.</p>
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        <h3>&ldquo; Lo que sabemos es una gota de agua; lo que ignoramos es  el oc&eacute;ano&rdquo;
        <br /><div align="right">Newton&#13;</div></h3>
		<? break;
		
		case "vision": ?>
        <img title="" src="img/sec_presentacion.jpg" class="img_der" alt="" />
        <h1>Nuestra visi&oacute;n</h1>
        <p>Queremos ser la  primera y la mejor elecci&oacute;n de las empresas para la formaci&oacute;n de sus  profesionales y en sus necesidades de asesoramiento. </p>
		<p>Nuestra actividad  principal ser&aacute; adelantarnos, en cada momento, a las necesidades de las empresas  clientes, ofreciendo una amplia gama de servicios y soluciones&nbsp; (Formaci&oacute;n profesional y Servicios de  Asesoramiento ) aportando una&nbsp; continua  evoluci&oacute;n en las jornadas de formaci&oacute;n que aumenten el crecimiento  profesionales de las personas en las empresas y les ayude en su desarrollo y evoluci&oacute;n  personal; haciendo accesibles los&nbsp;  modelos de gesti&oacute;n y servicios mas avanzados, con los profesionales  mejor cualificados en cada una de las &aacute;reas.</p>
        <h3>&ldquo;Nada se puede ense&ntilde;ar a un hombre &hellip;..solo ayudarle a encontrarlo por si mismo&rdquo;
        <br /><div align="right">Galileo Galilei</div></h3>
		<? break;
		
		case "mision": ?>
        <img title="" src="img/sec_presentacion2.jpg" class="img_der" alt="" />
        <h1>Nuestra misi&oacute;n</h1>
	    <p>Dar  servicio a nuestros clientes mediante la formacion directiva de alta calidad a  la comunidad empresarial creando espacios de desarrollo profesional y personal.</p>
		<p>Contribuir  a que los profesionales que se acerquen a la formacion, les ayude a evolucionar y asignen a esta experiencia un valor: su inversion en tiempo,  dinero y esfuerzo.</p>
		<br /><br /><br /><br /><br /><br /><br /><br /><br />
        <h3>&ldquo; Los hombres cuando ense&ntilde;an ... aprenden&rdquo;.
        <br /><div align="right">S&eacute;neca&#13;</div></h3>
		<? break;
		
		case "objetivos": ?>
        <img title="" src="img/sec_presentacion3.jpg" class="img_der" alt="" />
        <h1>Objetivos</h1>
		<p>&bull; Ofrecer  una formaci&oacute;n que refuerce los conocimientos individuales, desarrolle las  competencias directivas, proporcione una visi&oacute;n estrat&eacute;gica y potencie las  actitudes profesionales.<br /><br /></p>
		<p>&bull; Desarrollar personal y profesionalmente a las personas, de  modo que contribuyan a unas organizaciones mas efectivas y eficientes.<br /><br /></p>
		<p>&bull; Contribuir al conocimiento a trav&eacute;s de actividades de  formaci&oacute;n en la gesti&oacute;n profesional y practica.</p>
		<p>&nbsp;</p><p>&nbsp;</p>
        <br /><br /><br /><br /><br />
        <h3>&ldquo; Transformamos conocimientos y experiencias de gesti&oacute;n en competencias y habilidades directivas y ejecutivas."</h3>
		<? break;
	}?>
    </div>
</div>
<?php include("includes/3piecera.php");?>