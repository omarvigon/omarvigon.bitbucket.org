<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
    <div id="izq">
   		&nbsp;
    </div>
    <div id="der">
        <h1>Mapa del Sitio</h1>
        <ul>
            <li><a title="Presentacion" href="presentacion.php">Presentaci&oacute;n</a></li>
            <ul>
               	<li><a href="presentacion.php?sec=porque">&raquo; &iquest;Por qu&eacute;?</a>
    			<li><a href="presentacion.php?sec=vision">&raquo; Nuestra visi&oacute;n</a>
        		<li><a href="presentacion.php?sec=mision">&raquo; Nuestra misi&oacute;n</a>    	
    			<li><a href="presentacion.php?sec=objetivos">&raquo; Objetivos</a>
            </ul>
			<li><a title="Cobertura" href="cobertura.php">Cobertura</a></li>
			<li><a title="Formacion" href="formacion.php">Formaci&oacute;n</a></li>
            <ul>
                <li><a href="formacion.php?sec=jornadas">&raquo; Jornadas Profesionales</a></li>
                <li><a href="formacion.php?sec=cursos">&raquo; Cursos ejecutivos</a></li>
                <li><a href="formacion.php?sec=programas">&raquo; Programas</a></li>
            </ul>
			<li><a title="Servicios" href="servicios.php">Servicios</a></li>
            <ul>
           		<li><a href="servicios.php?sec=company">&raquo; In company</a></li>
		    	<li><a href="servicios.php?sec=tutoria">&raquo; Tutor&iacute;as</a></li>
    		    <li><a href="servicios.php?sec=consultoria">&raquo; Consultor&iacute;a</a></li>
	    		<li><a href="servicios.php?sec=manager">&raquo; Manager Share</a></li>
            </ul>
			<li><a title="Cursos" href="cursos.php">Cursos</a></li>
			<li><a title="Acuerdos" href="acuerdos.php">Acuerdos</a></li>
            <ul>
            	<li><a href="acuerdos.php?sec=empresas">&raquo; Empresas</a></li>
    			<li><a href="acuerdos.php?sec=asociaciones">&raquo; Asociaciones</a></li>
        		<li><a href="acuerdos.php?sec=instituciones">&raquo; Instituciones</a></li> 	
		    	<li><a href="acuerdos.php?sec=convenios">&raquo; Convenios</a></li>
            </ul>
			<li><a title="Clientes" href="clientes.php">Clientes</a></li>
			<li><a title="Informacion" href="informacion.php">Informaci&oacute;n</a></li>
			<li><a title="Matricula" href="matricula.php">Matr&iacute;cula</a></li>
            <ul>
           		<li><a href="matricula.php?sec=inscripcion">&raquo; Solicitud de Inscripci&oacute;n</a></li>
		    	<li><a href="matricula.php?sec=matriculacion">&raquo; Impreso de Matriculaci&oacute;n</a></li>
            </ul>
			<li><a title="Contacto" href="contacto.php">Contacto</a></li>
        </ul>
    </div>
</div>
<?php include("includes/3piecera.php");?>