<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
	<div id="izq">
   		<a title="" <?php if($_GET["sec"]=='empresas' || !$_GET['sec']) echo 'style="color:#ff6600;background:#000000;"';?> href="acuerdos.php?sec=empresas">&raquo; Empresas</a>
    	<a title="" <?php if($_GET["sec"]=='asociaciones') echo 'style="color:#ff6600;background:#000000;"';?> href="acuerdos.php?sec=asociaciones">&raquo; Asociaciones</a>
        <a title="" <?php if($_GET["sec"]=='instituciones') echo 'style="color:#ff6600;background:#000000;"';?> href="acuerdos.php?sec=instituciones">&raquo; Instituciones</a>    	
    	<a title="" <?php if($_GET["sec"]=='convenios') echo 'style="color:#ff6600;background:#000000;"';?> href="acuerdos.php?sec=convenios">&raquo; Convenios</a>
    </div>
    <div id="der">
    <?php switch($_GET["sec"])
	{
		default:
		case "empresas": ?>
        <img title="" src="img/sec_acuerdos.jpg" class="img_der" alt="" />
        <h1>Empresas</h1>
        <p>Los acuerdos con empresas abarcan  tanto los programas realizados &ldquo;In Company&rdquo; como la formaci&oacute;n de empleados en  Jornadas abiertas; representando una ventaja econ&oacute;mica importante en el caso de  desarrollo de planes de formaci&oacute;n integrales a toda la compa&ntilde;&iacute;a al poder  discriminar la formaci&oacute;n especifica ( exclusiva y dedicada ) con la generalista  a personas concretas.</p>
		<? break;
		
		case "asociaciones": ?>
        <img title="" src="img/sec_acuerdos2.jpg" class="img_der" alt="" />
        <h1>Asociaciones</h1>
        <p>Acuerdos de colaboraci&oacute;n mutua  que se basan en el mantenimiento de condiciones especiales al colectivo  profesional que engloba la Asociaci&oacute;n frente a la difusi&oacute;n e informaci&oacute;n de las  Jornadas de 5Pl en los medio de comunicaci&oacute;n propios. Esta colaboraci&oacute;n puede  extenderse a aquella formaci&oacute;n especifica del sector que la Asociaci&oacute;n  considere conveniente desarrollar o en la generaci&oacute;n de conferencias,  propuestas y avaladas como propias, en la que intervendr&aacute;n los profesionales  mas id&oacute;neos al perfil solicitado.</p>
		<? break;
		
		case "instituciones": ?>
        <img title="" src="img/sec_legal.jpg" class="img_der" alt="" />
        <h1>Instituciones</h1>
	    <p>Desarrollo de planes de formaci&oacute;n  espec&iacute;ficos, dise&ntilde;ados para un sector y en un &aacute;rea concreta,&nbsp; que convocados por la Instituci&oacute;n se destinen  a empresas de sectores concretos, como beneficio a sus empleados, dando a  conocer procedimientos de gesti&oacute;n punteros en su t&eacute;cnica o en la forma de  afrontarlos.</p>
		<? break;
		
		case "convenios": ?>
        <img title="" src="img/sec_acuerdos3.jpg" class="img_der" alt="" />
        <h1>Convenios</h1>
		<p>Colaboraciones con otras  instituciones, con o sin &aacute;nimo de lucro,&nbsp;  en un proyecto formativo conjunto en los que cada una de las partes  aporta&nbsp; su know how con un objetivo  concreto en el tiempo, en el fin y en la responsabilidad.</p>
		<? break;
	}?>
    </div>
</div>
<?php include("includes/3piecera.php");?>