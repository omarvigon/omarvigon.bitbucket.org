function comprobar(cual)
{
	switch(cual)
	{
		case 1:
		if(document.form_matriculacion.cuenta1.value.length==4)
		{
			document.form_matriculacion.cuenta2.value="";
			document.form_matriculacion.cuenta2.focus();
		}
		break;
		
		case 2:
		if(document.form_matriculacion.cuenta2.value.length==4)
		{
			document.form_matriculacion.cuenta3.value="";
			document.form_matriculacion.cuenta3.focus();
		}
		break;
		
		case 3:
		if(document.form_matriculacion.cuenta3.value.length==2)
		{
			document.form_matriculacion.cuenta4.value="";
			document.form_matriculacion.cuenta4.focus();
		}
		break;
	}
}
function validar_info()
{
	var errores=false;
	
	if(document.form_info.nombre.value=="" || document.form_info.direccion.value=="" || document.form_info.telefono.value=="")
		errores=true;

	if(document.form_info.email.value.search (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig) )
		errores=true;

	if(!document.form_info.aceptar.checked)
		errores=true;

	if(!errores)
		document.form_info.submit();
	else
		window.alert("Los campos marcados con (*) son obligatorios");
}
function validar_matriculacion()
{
	var errores=false;
	
	if(document.form_matriculacion.nombre.value=="" || document.form_matriculacion.direccion.value=="" || document.form_matriculacion.telefono.value=="")
		errores=true;

	if(document.form_matriculacion.email.value.search (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig) )
		errores=true;

	if(!document.form_matriculacion.aceptar.checked)
		errores=true;

	if(!errores)
		document.form_matriculacion.submit();
	else
		window.alert("Los campos marcados con (*) son obligatorios");
}
function validar_inscripcion()
{
	var errores=false;
	
	if(document.form_inscripcion.nombre.value=="" || document.form_inscripcion.direccion.value=="" || document.form_inscripcion.telefono.value=="")
		errores=true;

	if(document.form_inscripcion.email.value.search (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig) )
		errores=true;

	if(!document.form_inscripcion.aceptar.checked)
		errores=true;

	if(!errores)
		document.form_inscripcion.submit();
	else
		window.alert("Los campos marcados con (*) son obligatorios");
}