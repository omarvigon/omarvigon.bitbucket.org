
    <meta name="language" content="es" />
    <meta name="abstract" content="5PL BUSINESS TRAINING" />
    <meta name="subject" content="5PL BUSINESS TRAINING" />
    <meta name="author" content="5PL BUSINESS TRAINING" />
    <meta name="copyright" content="5PL BUSINESS TRAINING" />
    <link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
    <link rel="alternate" type="application/rss+xml" title="RSS" href="http://www.eada.edu/es/rd-rss/3.html"/>
    <script language="javascript" type="text/javascript" src="includes/_funciones.js"></script>
</head>

<body>
<div id="arriba">
	<a title="5pl" href="index.php"><img title="5PL" src="img/cabecera2.jpg" alt="5pl" /></a>
	<div>
	<a title="Presentacion" <?php if($_SERVER['PHP_SELF']=='/presentacion.php') echo 'style="color:#ff6600;background:#000000;"';?> href="presentacion.php">Presentaci&oacute;n</a>
	<a title="Cobertura" <?php if($_SERVER['PHP_SELF']=='/cobertura.php') echo 'style="color:#ff6600;background:#000000;"';?> href="cobertura.php">Cobertura</a>
	<a title="Formacion" <?php if($_SERVER['PHP_SELF']=='/formacion.php') echo 'style="color:#ff6600;background:#000000;"';?> href="formacion.php">Formaci&oacute;n</a>
	<a title="Servicios" <?php if($_SERVER['PHP_SELF']=='/servicios.php') echo 'style="color:#ff6600;background:#000000;"';?> href="servicios.php">Servicios</a>
	<a title="Cursos" <?php if($_SERVER['PHP_SELF']=='/cursos.php') echo 'style="color:#ff6600;background:#000000;"';?> href="cursos.php">Cursos</a>
	<a title="Acuerdos" <?php if($_SERVER['PHP_SELF']=='/acuerdos.php') echo 'style="color:#ff6600;background:#000000;"';?> href="acuerdos.php">Acuerdos</a>
	<a title="Clientes" <?php if($_SERVER['PHP_SELF']=='/clientes.php') echo 'style="color:#ff6600;background:#000000;"';?> href="clientes.php">Clientes</a>
	<a title="Informacion" <?php if($_SERVER['PHP_SELF']=='/informacion.php') echo 'style="color:#ff6600;background:#000000;"';?> href="informacion.php">Informaci&oacute;n</a>
	<a title="Matricula" <?php if($_SERVER['PHP_SELF']=='/matricula.php') echo 'style="color:#ff6600;background:#000000;"';?> href="matricula.php">Matr&iacute;cula</a>
	<a title="Contacto" <?php if($_SERVER['PHP_SELF']=='/contacto.php') echo 'style="color:#ff6600;background:#000000;"';?> href="contacto.php">Contacto</a>
	</div>
</div>