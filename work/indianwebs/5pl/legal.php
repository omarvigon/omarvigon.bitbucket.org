<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">
    <div id="izq">
   		&nbsp;
    </div>
    <div id="der">
        <img title="" src="img/sec_acuerdos4.jpg" class="img_der" alt="" />
        <h1>Aviso Legal</h1>
		<p>1.- La informaci&oacute;n de esta p&aacute;gina Web y podr&aacute; cambiarse o actualizarse sin  previo aviso.</p>
		<p>2.- Toda la informaci&oacute;n que reciba 5 PL Business Training a trav&eacute;s de este  sitio Web se considerar&aacute; confidencial.</p>
		<p>3.- No se responsabiliza ni garantiza la disponibilidad y continuidad en el  acceso a esta p&aacute;gina Web o que el mismo est&eacute; libre de errores.</p>
		<p>4.- 5PL no se responsabiliza de los contenidos que personas ajenas a 5PL  hayan incluido en los hiperv&iacute;nculos a los que se hace referencia en esta  p&aacute;gina.</p>
		<p>5.- El establecimiento del hiperv&iacute;nculo no implica en ning&uacute;n caso la  existencia de relaciones entre 5PL&nbsp; y el  propietario de la p&aacute;gina Web en la que se establezca, ni la aceptaci&oacute;n y  aprobaci&oacute;n por parte de 5 PL de sus contenidos o servicios all&iacute; ofrecidos y  puestos a disposici&oacute;n del p&uacute;blico.</p>
		<p>6.- 5PL se compromete a respetar la confidencialidad de los datos aportados  de car&aacute;cter personal de los visitantes a la p&aacute;gina Web en aplicaci&oacute;n de lo  dispuesto en la Ley Org&aacute;nica de protecci&oacute;n de datos de car&aacute;cter personal.</p>
		<p>7.- 5PL no se hace responsable de ning&uacute;n da&ntilde;o o perjuicio en el software o  hardware del usuario que se derive del acceso a su p&aacute;gina Web o del uso de  informaci&oacute;n o aplicaciones en ella contenidas.</p>
		<p>8.- El Usuario es consciente y acepta voluntariamente que el uso de  cualquier contenido de esta p&aacute;gina Web tiene lugar, en todo caso, bajo su &uacute;nica  y exclusiva responsabilidad.</p>
		<p><b>CONTACTO LEGAL </b></p><p>Para cualquier sugerencia o pregunta contacte, con nosotros.</p>
		<p><b>5PL Engineering  Business Training SL<br />info@5pl.org</b></p>
    </div>
</div>
<?php include("includes/3piecera.php");?>