<?php include("includes/1cabecera.php");?>
    <title>5PL BUSINESS TRAINING</title>
    <meta name="title" content="5PL BUSINESS TRAINING" />
    <meta name="description" content="5PL BUSINESS TRAINING" />
    <meta name="keywords" content="5PL BUSINESS TRAINING" />
<?php include("includes/2cabecera.php");?>
<div id="medio">

	<div id="izq">
   		<a title="" <?php if($_GET["sec"]=='company' || !$_GET['sec']) echo 'style="color:#ff6600;background:#000000;"';?> href="servicios.php?sec=company">&raquo; In company</a>
    	<a title="" <?php if($_GET["sec"]=='tutoria') echo 'style="color:#ff6600;background:#000000;"';?> href="servicios.php?sec=tutoria">&raquo; Tutor&iacute;as</a>
        <a title="" <?php if($_GET["sec"]=='consultoria') echo 'style="color:#ff6600;background:#000000;"';?> href="servicios.php?sec=consultoria">&raquo; Consultor&iacute;a</a>    	
    	<a title="" <?php if($_GET["sec"]=='manager') echo 'style="color:#ff6600;background:#000000;"';?> href="servicios.php?sec=manager">&raquo; Manager Share</a>
    </div>
    <div id="der">
    <?php switch($_GET["sec"])
	{
		default:
		case "company": ?>
        <img title="" src="img/sec_servicios1.jpg" class="img_der" alt="" />
        <h1>In company</h1>
        <p>Dise&ntilde;o de proyectos formativos, dise&ntilde;ados  para una empresa, tanto con los m&oacute;dulos Standard que se relacionan en el  catalogo como aquellos espec&iacute;ficos que se nos solicite que desarrollemos,  impartidos en los locales de la empresa o en un lugar externo designado para su  realizaci&oacute;n.</p>
		<? break;
		
		case "tutoria": ?>
        <img title="" src="img/sec_servicios3.jpg" class="img_der" alt="" />
        <h1>Tutor&iacute;as</h1>
        <p>Desarrollo del dise&ntilde;o de proyectos  formativos id&eacute;ntico a un &ldquo;In Company&rdquo;pero con la selecci&oacute;n de un caso real de  la empresa, de aplicaci&oacute;n directa al programa desarrollado, que debe resolverse  en el plazo de 4 tutor&iacute;as, posteriores a la realizaci&oacute;n del curso, con una  frecuencia (a decidir) aconsejada de 2-3 semanas entre cada una de ellas y  realizada por integrantes del curso.</p>
		<? break;
		
		case "consultoria": ?>
        <img title="" src="img/sec_servicios2.jpg" class="img_der" alt="" />
        <h1>Consultor&iacute;a</h1>
	    <p>Servicio de apoyo a empresas en aquellas  &aacute;reas de mejora, o de an&aacute;lisis, que sea necesario estudiar una situaci&oacute;n  concreta y formal, efectuada externamente y por profesionales seniors,  especialistas en su &aacute;rea.</p>
		<? break;
		
		case "manager": ?>
        <img title="" src="img/sec_servicios.jpg" class="img_der" alt="" />
        <h1>Manager Share</h1>
		<p>Disponibilidad de un profesional, a  tiempo parcial, de alto nivel que durante un tiempo, acotado y concreto,  colabore con la empresa en apoyo de &aacute;reas o proyectos en los que se requiera un  profesional especialista.</p>
		<? break;
	}?>
    </div>
</div>
<?php include("includes/3piecera.php");?>