<?php include "c_cabecera.php";?>
	<br><a href="3clases_eng.php"><b>�Why chose ASTON?</b></a>
		<div class="subtext">Solutions & Experience</div>
	<span class="azul1"><b>Out teachers & courses</b></span>
		<div class="subtext">In time with you</div>
		<div class="subtext2"><a href="3clases_eng.php?aux3=profesores">Our teachers</a></div>
		<div class="subtext2"><a href="3clases_eng.php?aux3=cursos">Our courses</a></div>
	<br><a href="3clases_eng.php?aux3=clases"><b>Our classes</b></a>
		<div class="subtext">We come to you</div>
		<div class="subtext2"><a href="3clases_eng.php?aux3=chicos">Private classes for children</a></div>
		<div class="subtext2"><a href="3clases_eng.php?aux3=adultos">Private classes for adults</a></div>
		<div class="subtext2"><a href="3clases_eng.php?aux3=empresas">In-company classes</a></div>
		<div class="subtext2"><a href="3clases_eng.php?aux3=especial">Specialized courses</a></div>
	<br><a href="3clases_eng.php?aux3=examenes"><b>Official Exams</b></a>
		<div class="subtext">Prepare for the future!</div>
	<a href="3clases_eng.php?aux3=extranjero"><b>Courses abroad</b></a>
		<div class="subtext">Choose your experience!</div>
	<a href="3clases_eng.php?aux3=common"><b>The Common European Framework</b></a>
		<div class="subtext">Get the recognition you deserve!</div>
</div>

<div id="c_clases">
	<div class="correo">
	<a href="mailto:astoninfo@astonidiomas.com" class="blanco" style="text-decoration:none;">astoninfo@astonidiomas.com</a>
	</div>

	
	<?php
	switch($aux3)
	{
	case "profesores":
	echo "<div id='tclases'>Our Teachers: our human capital</div>";
	echo "<div class='textblanco'>";
	echo "<br>All our classes are given by University graduate calibre and TESOL or CELTA certified Teachers who are contracted after a rigorous interview procedure.
	<p>Our team consists of motivated, energetic people who take pride in and pleasure from their work. Often having travelled widely, they are interesting people who are experienced in teaching and bring a wealth of character and life to the class. We like to think that in addition to learning the language, the class is also a time to disconnect and revitalise from the contact within the classroom.
	<p>Aston recognises that the Teachers are our �materia prima� and provide them with support, advice, resources and on-going training to ensure quality and vitality in the classroom.";
	echo "</div>";
	echo "<img src='imgs/f_clases2.jpg' name='imagen3'>";	
	break;
	
	case "cursos":
	echo "<div id='tclases'>Our courses: In time with you</div>";
	echo "<div class='textblanco4'>";
	echo "<br>The academic year for youngsters or the financial year for companies. Maybe you need a personal development course for specific needs, a refresher or maintenance. Choose from our set course dates or tell us what you need. We will design the rest.
	<p>In any case, clearly defined objectives combined with regular frequency and consistent attendance give the results we all want.
	<p>All classes, whether in-company or private, are concluded by termly reports and an end of course exam, sometimes official and sometimes personalized or internal. These are complemented by a quality control survey and student attendance reports.";	
	echo "</div>";
	echo "<img src='imgs/f_clases3.jpg' name='imagen3'>";
	break;
	
	case "clases":
	echo "<div id='tclases'>Our classes: We come to you</div>";	
	echo "<div class='textblanco0'>";
	echo "<br>Classes in the comfort of your own home or in the convenience of your place of work, whenever you want.
	<p>Today�s fast pace of living means that every  moment counts and by coming to you, we make sure you get the very most: our Teachers adapt to your schedule and residence or work, from first thing in the morning to late evenings, five days a week.";
	echo "</div>";
	echo "<img src='imgs/f_clases4.jpg' name='imagen3'>";
	break;
	
	case "chicos":
	echo "<div id='tclases'>Private classes for children: activate and motivate</div>";
	echo "<div class='textblanco'>";
	echo "<br>We use the latest books and resource material such as DVD�s and games to motivate children through entertaining classes that stimulate their interest and learning, participation, visual stimulation and memory.
	<ul><li>Native Teachers experienced in Teaching children.
	<li>Fun, entertaining classes.
	<li>Classes done at home in small groups or 1 to 1.
	<li>Schoolwork support and general English courses.
	<li>Cambridge young learners, KET & PET exams.
	<li>Trinity graded interview exams.</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases5.jpg' name='imagen3'>";
	break;
	
	case "adultos":
	echo "<div id='tclases'>Private classes for adults: combining lifestyle and learning.</div>";
	echo "<div class='textblanco'>";
	echo "<br>In Aston we understand the difficulties of language learning for adults. For this reason we offer maximum flexibility regarding the time, frequency and place of  the class so as to concentrate on the level, needs and progress of the student.
	<ul><li>Interesting, courteous and dynamic native Teachers.
	<li>Conversation and grammar correction for maintenance.
	<li>Official exam courses.
	<li>One to one and small groups.
	<li>English for travel & leisure.
	<li>Vanguard material and methodolgy.</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases6.jpg' name='imagen3'>";
	break;
	
	case "empresas":
	echo "<div id='tclases'>In-company classes: Help to work, help to progress. </div>";	
	echo "<div class='textblanco0'>";
	echo "<br>From micro companies to multinationals, Aston understands the importance of communication.
	<ul><li>Our Teachers come to you, whether a group or private class.
	<li>General or specific English with a frequency and over a period of time that suits you.
	<li>Fixed objective and content courses adapted to every level.
	<li>Personal development one to one classes for specifics.
	<li>Official exam courses as  measurement tools and motivators.
	<li>English for business courses: commercial English, writing faxes, letters, reports and emails, telephone structures. Skills in negotiating, convincing and defending arguments, presentations and conferencing.
	<li>Periodic attendance and progress reports. 
	<li>Complementary highly personalized business skills courses in the UK.</ul></div>";
	echo "<img src='imgs/f_clases7.jpg' name='imagen3'>";
	break;
	
	case "especial":
	echo "<div id='tclases'>Specialized courses: Make it perfect!</div>";
	echo "<div class='textblanco'>";
	echo "<br>Whatever your level, we will help you to determine the way ahead to get results. Identify and achieve the level and kind of English you need - break stagnant learning cycles or climb the ladder of internationally recognised levels.
	<p>With an obligation-free assessment, we can help you to define your English.
	<ul><li>Personal development and perfectioning of certain aspects of English.
	<li>Preparation and presentation to official exams at all levels, from children to adults, for studies or for work.
	<li>Unforgettable English language courses in some of the most priviledged places of study and leisure around the world.
	<li>The most personalized courses of the EFL sector at the UK�s leading centre headquarters for professionals. Progress in language learning here is a proven fact</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases8.jpg' name='imagen3'>";
	break;
	
	case "examenes":
	echo "<div id='tclases'>Official exams: Prepare for the future!</div>";	
	echo "<div class='textblanco3'>";
	echo "<br>How do you know if you have really progressed or reached a required level in English? Only by taking official exams with an external examining body will you know objectively! 
	<p><b>Prepare yourself</b> for the future and professional requirements with Cambridge or Trinity. ASTON is an official Trinity College London examination centre and is officially recognised by the Cambridge University ESOL examination service:
	<p><b>LEVEL TEST: Find out your ranking on the common European framework.</b></p> 
	<p>With our free level test, we will be able to situate you on the CEF ranking, clarify your doubts and advise you on the next steps ahead.
	<br><br><a href='files/cambridge_exams.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Cambridge Exams</a><br>
	<a href='files/trinity_exams.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Trinity Exams</a><br>
	<a href='files/level_test_part1.doc' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_doc.gif' hspace='6' border='0'>Level test part1</a><br>
	<a href='files/level_test_part2.doc' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_doc.gif' hspace='6' border='0'>Level test part2</a>";
	echo "</div>";
	break;
	
	case "extranjero":
	echo "<div id='tclases'>Language Courses around the world: Choose your experience!</div>";	
	echo "<div class='textblanco3'>";
	echo "<br><b>Live and learn!</b> Learning a language is so much more than grammar, vocabulary and exams. With our exclusive courses around the English-speaking world, you will get a real feeling for the language and a chance to put what you know into practice, to lose your fears and to take a definitive step forward.
	<p>Many of these study-holidays work in a complementary manner with our courses here in Barcelona, helping to complete the language learning cycle. Given the long and close working relationship we enjoy with our centres at priviledged study and tourism locations around the world, progress in many ways is guaranteed.
	<p><b>Executives and business people: Break stagnant learning cycles and save time!</b>
	<p>Can you answer the following questions clearly & positively?
	<ul><li>Can you commit to yet another year of classes? 
	<li>Are standard classes a good use of time and resources?
	<li>Are you progressing the way you want/need?
	<li>Have you reached the required level of competence?
	<li>When will you be able to stop doing regular classes?</ul>
	<b>With our formula of:</b><li>ASTON personal development + Pilgrims Business English + Official assessment
	<p>We guarantee you that you will break stagnant learning cycles and give a definitive leap forward. Our tailor-made classes & our exceptional business English course in Canterbury, England will save you time and money in the long term because we:
	<ul><li>Shorten your English study calendar dramatically.
	<li>Make effective use of every moment you dedicate to learning English.
	<li>Identify your real needs and concentrate only on them.
	<li>Help you reach your target level of competence and prove it.
	<li>Set clear start and finish points for your English learning.</ul>
	<p>We consider your time precious and your fees an investment. We treat them as such. 
	<p>See the <a href='2cursos.php' style='color:gray'>'Cursos de Idiomas'</a> section on the home page for detailed information.";
	echo "</div>";
	break;
	
	case "common":
	echo "<div id='tclases'>The Common European Framework</div>";	
	echo "<div class='textblanco'>";
	echo "<br>Where do you stand on the English learning path? Why not take our free level test and send us the result? We�ll be happy to correct it and to give you a good indication of your ranking on the table below.
	<br><br><a href='files/marco_europeo_comun.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Common European Framework</a>";
	echo "</div>";
	break;
		
	default:
	echo "<div id='tclases'>�Why choose ASTON? - Solutions & Experience</div>";	
	echo "<div class='textblanco4'>";
	echo "<br><p><b>Solutions for learning:</b>
	<p>We offer real solutions because we see our students through - we understand the difficulties of language learning and our highly personalized courses are designed to suit real needs and abilities.
	<p>We combine the convenience of service in your home or place of work with stimulating classes that maximise the learning process.
	<p>Together with you, we assess and plan the trajectory, content and duration of your course.
	<p><b>Credibility and experience:</b><p>We are qualified professionals in EFL with a successful 20 year proven track record as an English teaching organization. We strive for progress and results through quality teaching to a spectrum of clients from young learners, teenagers and adults to businesses,  executives and professionals. 
	<p><b>Something for everyone:</b>
	<p><b>All our courses:</b>The level, objectives and frequency as well as the content of our courses are decided upon with the client prior to starting a course. We establish where you want to go and how to get there.
	<p><b>Recognition:</b>As well as being a registered Trinity examination centre, we are also recognised by the Cambridge University ESOL examination service; but perhaps the best recognition we have comes from the loyalty of our clients.
	<p><b>Completing the learning circle:</b>We teach English during the academic year, and during the months of summer, we organize English courses in Spain and around the English-speaking world for children, teenagers, young adults and professionals at exclusive sites with exclusive agreements for definitve results.";
	echo "</div><br>";
	echo "<img src='imgs/f_clases1.jpg' name='imagen3'>";
	break;
	
	}
	?>
	</div>
</div>

<?php include "c_piecera.php";?>