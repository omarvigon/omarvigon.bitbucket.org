<?php include "c_cabecera.php";?>
	<br><br><br>
	<a class="bblue" href="index_cat.php?aux=equipo">el nostre equip</a>
	<a class="bblue" href="index_cat.php?aux=delegacion">delegacions</a>
	<a class="bblue" href="index_cat.php?aux=contacta">contacta amb nosaltres</a>
	<a class="bblue" href="index_cat.php?aux=trabaja">treballa amb ASTON</a>
	<a class="bblue" href="index_cat.php?aux=catalogo">sol.licitut de cat�leg</a>
	<a class="bblue" href="index_cat.php?aux=inscripcion">full d'inscripci&oacute;</a>
	<a class="bblue" href="index_cat.php?aux=enlaces">enlla�os d'inter&eacute;s</a>

	<a href="index_cat.php?aux=fotos"><img src="imgs/photogalery2.jpg" alt="Galeria de Fotos" border="0" vspace="10"></a>
</div>

<div id="contenedor2">
	<div id="fecha"></div>
	<div class="abs1"><img src="imgs/banner.gif" border="0"></div>
<?php 
	switch($aux)
	{
		case "equipo":
		echo '<div id="cverde3">EQUIP</div>';
		echo "<div class='equipo'>
		<span class='verde1'>Directora General i Fundadora:</span><br><b>&Aacute;ngels Faus</b><br>Llicenciada en Hist&oacute;ria - UAB 1978<br>Proficiency en llengua Anglesa � Cambridge University
		<br><br><span class='verde1'>Gerent:</span><br><b>Eduardo Soler</b><br>Llicenciat en Enginyeria Industrial (Gesti&oacute;n) - UPC 2002
		<br><br><span class='verde1'>Contabilitat y Administraci&oacute;:</span><br><b>Mireia Falqu&eacute;s</b><br>Secretariat de Direcci&oacute;
		<br><br><span class='verde1'>Director Comercial:</span><br><b>Saverio Costanzo</b><br>M&aacute;ster en Relacions Internacionals, UB 2005<br>Proficiency en llengua Anglesa � Cambridge University<br>Diplomat en Comunicaci&oacute; Corporativa, UB 2002
		<br><br><span class='verde1'>Departamento de Cursos d' Angl&eacute;s (In-company i particulars):</span><br><b>Jon Heavers</b><br>Bachelor of Science � London Metropolitan University 1990<br>Cambridge University CELTA (TEFL)<br>Cap d'Estudis (responsable d'un equip de 25 professors universitaris qualificats, docents de llengua anglesa)<br>
		<br><span class='verde1'>Departament de Traducci&oacute;:</span><br><b>Jos&eacute; Manuel Alonso</b><br>Llicenciat en Traducci&oacute; e Interpretaci&oacute;, UPF 2001<br>Traductor jurat d'angl&eacute;s<br>Gestor de projectes i traductor en plantilla
		<br><b>Aurelia Manils</b><br>Llicenciada en Filolog&iacute;a Anglesa, UB 1989<br>Traductora en plantilla
		<br><b>Neus Ribas</b><br>Llicenciada en Traducci&oacute; e Interpretaci&oacute; � UPF 1999<br>Traductora jurada d'angl&eacute;s<br>Traductora en plantilla
		<br><b>Alejandra de Castro</b><br>Formaci&oacute; profesional administratiu II<br>Administrativa
		<br><br><span class='verde1'>Departament de Cursos a l'extranger:</span><br><b>Carla Parellada</b><br>Postgraduate in Art Administration � New South West University (Sydney 2005)<br>Llicenciada en Hist&oacute;ria de l'Art, Universitat de Girona 2003<br>Responsable Area d'Atenci&oacute; al Client
		<br><b>Mamen Planas</b><br>Llicenciada en Historia del Arte, UB 2003<br>Consultora
		<br><b>Victoria Pasqual del Pobil</b><br>Master in Business i Recursos Humans, European University, <br>Llicenciada en ADE, European University 1975.<br>Consultora
		<br><b>Carmen Alcina</b><br>Responasable Any Acad�mic<br>
		<br><span class='verde1'>Recepci&oacute;:</span><br><b>Eva Bas</b>
		<br>Una sort per a tots!</div>";
		echo "<img src='imgs/equipo.jpg'>";
		break;
	
		case "delegacion":
		echo '<div id="cverde3">DELEGACIONS</div>';
		echo '<div class="delegacion"><p><b>ILLES BALEARS</b><br><span class="verde1">LYC-Lenguaje y Comunicacion</span><br>Foners 1,3� B<br>07006 Palma de Mallorca<br>+34 902 36 79 18<br><a href="mailto:baleares@astonidiomas.com">baleares@astonidiomas.com</a> 
		<p><b>ARAG&Oacute;</b><br><span class="verde1">Global World</span><br>Gran V&iacute;a, 30 bis 50005 Zaragoza<br>+34 902 88 40 69<br><a href="mailto:aragon@astonidiomas.com">aragon@astonidiomas.com</a>
		<p><b>GIRONA</b><br><span class="verde1">Isabel Alsius</span><br> +34 639 97 93 78<br><a href="mailto:girona@astonidiomas.com">girona@astonidiomas.com</a></div>';
		echo '<div class="delegacion">
		<p><b>MADRID</b><br><span class="verde1">Paloma Leira</span><br>+34 605 02 76 39 (tardas)<br><span class="verde1">Paloma Fl&oacute;rez</span><br>+34 639 18 34 07 (tardas)<br><a href="mailto:madrid@astonidiomas.com">madrid@astonidiomas.com</a>
		<p><b>PA&Iuml;S BASC-NAVARRA</b><br><span class="verde1">Natacha San Juan</span><br>+34 655 12 55 57<br><a href="mailto:pv-navarra@astonidiomas.com">pv-navarra@astonidiomas.com</a>
		<p><b>COMUNITAT VALENCIANA</b><br><span class="verde1">Rosario Beltr�n</span><br>+34 635 41 22 15<br><a href="mailto:valencia@astonidiomas.com">valencia@astonidiomas.com</a></div>';
		break;
		
		case "contacta":
		echo "<div id='cverde3'>CONTACTA AMB NOSALTRES</div>";
		echo "<div class='cverde3_txt'><b>OFICINA PRINCIPAL</b><br><span class='verde1'>ASTON Idiomas en el Mundo</span><br>Pg. Manuel Girona, 82 08034 Barcelona<br>Tel. +34 93 280 28 90 - Fax +34 93 280 28 84<br><a href='mailto:astoninfo@astonidiomas.com'>astoninfo@astonidiomas.com</a>
		<p><b>Horari d'atenci&oacute; al client</b><br>de 9 a 14 i de 16 a 19.30 de dilluns a divendres</div>";
		echo "<img src='imgs/contacta_con_nosotros.gif' name='imagen1'>";
		break;
		
		case "trabaja":
		echo '<div id="cverde3">TRABAJA CON NOSOTROS</div>';
		echo '<div class="cverde3_txt"><b> <a href="1traduccion_cat.php?aux2=traductores">- Traductores</a>';
		echo '<p> - <a href="files/english_teacher.doc" target="_blank">English teacher</a>';
		echo '<p> - <a href="files/monitor_camps_catalunya_baleares.doc" target="_blank">Monitor en nuestros <i>camps</i> en Catalu&ntilde;a y Baleares</a>';
		echo '<p> - <a href="files/monitor_grupos_extranjero.doc" target="_blank">Monitor acompa&ntilde;ante para los grupos en el extranjero</a>';
		echo '<p> - <a href="files/english_teacher_camps_catalunya_baleares.doc" target="_blank">English teacher for  our camps in Catalu&ntilde;a & Baleares</a></b></div>
		<img src="imgs/trabaja_nosotros.gif" border="0" alt="Trabaja con nosotros">';
		break;
		
		case "catalogo":
		echo '<div id="cverde3">SOLICITUD DE CAT�LOGO</div>';
		echo '<div class="cverde3_txt">Rellena este formulario y te enviaremos el cat�logo gratis';
		echo '<form name="form_catalogo" method="post" action="4envios.php?tema=catalogo" target="_blank"><b>TIPO DE CAT�LOGO</b><br>
		<input type="checkbox" name="tipo1">Adultos y ejecutivos<br>
		<input type="checkbox" name="tipo2">Juniors Extranjero<br>
		<input type="checkbox" name="tipo3">Juniors Catalu&ntilde;a y Baleares<br>
		<br><b>DATOS DEL SOLICITANTE</b>
		<br>Nombre y apellidos <input type="text" name="nombre" size="45" class="form_caja">
		<br>Direcci&oacute;n <input type="text" name="direccion" size="56" class="form_caja">
		<br>Codigo Postal <input type="text" name="postal" size="15" class="form_caja"> Poblaci&oacute;n <input type="text" name="poblacion" size="15" class="form_caja">
		<br>Tel&eacute;fono <input type="text" name="telefono" size="15" class="form_caja"> M&oacute;vil <input type="text" name="movil" size="16" class="form_caja">
		<br>E-Mail <input type="text" name="email" size="45" class="form_caja">
		<br>Fecha de Nacimiento <input type="text" name="nacimiento" size="15" class="form_caja">
		<br>�C&oacute;mo nos ha conocido?<br><textarea cols="50" rows="2" name="conocido"></textarea>
		<br>Comentarios<br><textarea cols="50" rows="2" name="comentarios"></textarea>
		<br><br><span style="padding-left:375px;"><input type="submit" class="boton_envio" value="Enviar"></span></form>';
		echo '</div>';
		break;
		
		case "inscripcion":
		echo '<div id="cverde3">HOJA DE INSCRIPCI&Oacute;N</div>';
		echo "<div class='cverde3_txt'>Descarregui en PDF la fulla d'inscripci&oacute; correspondent</div>";
		echo '<div style="padding-left:150px;"><a href="files/inscripcion_adultos_ejecutivos_cat.pdf" target="_blank"><b>ADULTS I EXECUTIUS</b> <img src="imgs/inscripcion1.jpg" border="0" alt="Adultos y Ejecutivos" align="middle"></a></div>';
		echo '<div style="padding-left:10px;"><a href="files/inscripcion_juniors_extranjero_cat.pdf" target="_blank"><img src="imgs/inscripcion2.jpg" border="0" alt="Juniors Extranjero" align="middle"> <b>JUNIORS AL EXTRANJER</b></a></div>';
		echo '<div style="padding-left:80px;"><a href="files/inscripcion_espana_cat.pdf" target="_blank"><b>JUNIORS A CATALUNYA I BALEARS</b> <img src="imgs/inscripcion3.jpg" border="0" alt="Juniors Catalu&ntilde;a y Baleares" align="middle"></a></div>';
		break;
		
		case "enlaces":
		echo "<div id='cverde3'>Enlla�os d'inter&eacute;s</div>";
		echo '<div class="cverde3_txt"><b>WEBS EN INGLES :</b>
		<br><a href="http://www.nytimes.com/">New York Times</a>
		<br><a href="http://news.sky.com/skynews/home">Sky News</a>
		<br><a href="http://www.cnn.com/">CNN</a>
		<br><a href="http://www.washingtonpost.com/">The Washigton Post</a>
		<br><a href="http://www.timesonline.co.uk/global/">The Times</a>
		<br><a href="http://www.guardian.co.uk/">The Guardian</a>
		<br><a href="http://www.ft.com/">Financial Times</a>
		<br><br><b>WEBS EN FRANCES :</b>
		<br><a href="http://www.lemonde.fr/">Le Monde</a>
		<br><a href="http://www.louvre.fr/llv/commun/home_flash.jsp">Louvre</a>
		<br><a href="http://www.centrepompidou.fr/Pompidou/Accueil.nsf/tunnel?OpenForm">Centre Pompidou</a>
		<br><a href="http://www.volterre.fr/">Volterre</a></div>';
		break;
		
		case "fotos":
		echo "<div id='cverde3'>GALERIA D'FOTOS</div>";
		echo '<div class="galeria1">';
		$conexion=mysql_connect("lldb542.servidoresdns.net","qaz095","45t0n");
		mysql_select_db("qaz095");
		
		$tipos=mysql_query("select distinct tipo from archivos");
		for($k=0;$k<mysql_num_rows($tipos);$k++)
		{
			$ctipo=mysql_fetch_row($tipos);
			$temp=substr($ctipo[0],7);
			echo "<p><b>".$temp."</b></p>";
			$consulta=mysql_query("select * from archivos where tipo='galeria".$temp."'");
			for($j=0;$j<mysql_num_rows($consulta);$j++)
			{
				$fila=mysql_fetch_row($consulta);
				echo "<a href='admin/galeria/".$fila[1]."' target='_blank'><img src='admin/galeria/".$fila[1]."' border='0' align='center' vspace='2' hspace='2' width='80'></a>";
			}
		}
		
		echo '</div>';
		mysql_close($conexion);
		break;
	
		default:
		echo "<div><div id='crojo'><a href='1traduccion_cat.php' style='color:#FFFFFF'>TRADUCCI&Oacute;</a></div>
			 <div id='crojo2'> i correci&oacute; de textos</div></div>
			<div class='texto_der'>En l'�mbit jur&iacute;dic, t&eacute;cnic, financer, empresarial, cultural, art&iacute;stic, comunicacions, comercial, publicitari, moda, confecci&oacute;, alimentaci&oacute;, m&eacute;dic, qu&iacute;mic i farmac&eacute;utic...</div>
		<div><div id='cverde'><a href='2cursos.php' style='color:#FFFFFF'>CURSOS D'IDIOMES</a></div>
			 <div id='cverde2'>para joves, adults i executius</div></div>
			 <div class='texto_der'>Alemania, Austria, Canad�, Estados Unidos, Francia, Irlanda, China, Malta, Reino Unido, Suiza, Campamentos en Espa&ntilde;a</div>
		<div><div id='cazul'><a href='3clases_cat.php' style='color:#FFFFFF'>CLASSES D'IDIOMES</a></div>
			 <div id='cazul2'>per a particulars i empreses</div></div>
			 <div class='texto_der'>Cursos generals o espec&iacute;fics individuals o per grups, en empreses o a domicili, solucions ling�&iacute;stiques per a empreses, preparaci&oacute; per a ex�mens oficials</div>";
		break;
	}
	echo "</div>";
	include "c_piecera.php";
?>
