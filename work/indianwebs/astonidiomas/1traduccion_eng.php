<?php include "c_cabecera.php";?>

	<p><a href="1traduccion_eng.php"><b>Why choose Faus & Planas?</b></a></p>
	<span class="azul1"><b>Translations and proof reading</b></span>
	   <div class="subtext2"><a href="1traduccion_eng.php?aux2=metodo">Work methodology </a></div>
	   <div class="subtext2"><a href="1traduccion_eng.php?aux2=correccion">Spelling, grammatical, sintax and style correction</a></div>
	   <div class="subtext2"><a href="1traduccion_eng.php?aux2=areas">Areas covered and clients</a></div>
	<p><a href="1traduccion_eng.php?aux2=enviarnos"><b>How to send us a text</b></a></p>
	<span class="azul1"><b>Interpretation</b></span>
		<div class="subtext2"><a href="1traduccion_eng.php?aux2=contextos">Contexts</a></div>
	    <div class="subtext2"><a href="1traduccion_eng.php?aux2=modalidad">Types</a></div>
	    <div class="subtext2"><a href="1traduccion_eng.php?aux2=especial">Areas of specialization</a></div>
	<p><a href="1traduccion_eng.php?aux2=presupuesto"><b>Tariff Estimate Request</b></a></p>
	<p><a href="1traduccion_eng.php?aux2=pedidos"><b>Order Form</b></a></p>
	<p><a href="1traduccion_eng.php?aux2=traductores"><b>Translators' Area</b></a></p>
	<div class="cont_abajo1">
		<a href="http://www.act.es" target="_blank"><img src="imgs/logo_act.jpg" alt="ACT" border="0" hspace="4"></a>
		<a href="http://www.euatc.org" target="_blank"><img src="imgs/logo_euatc.jpg" alt="EUATC" border="0" hspace="4"></a>
	</div>
</div>

<div id="c_traduccion">
	 <div class="correo">
	 <a href="mailto:fausyplanas@astonidiomas.com" style="text-decoration:none;color:#FFFFFF;">fausyplanas@astonidiomas.com</a>
	 </div>
	
	<?php 
	switch($aux2)
	{
		case "metodo":
		echo "<div id='ttraduccion'>Work methodology </div>";
		echo "<div class='textblanco3'><br>";
		echo "<p>Our objective is the FINAL QUALITY of the product and this can only be guaranteed by a team made up of professionals and the application of strict work methodology.
		<img src='imgs/metodo1.gif' align='left' hspace='6' vspace='42'><p><b>ASSIGNMENT </b><li>The translations manager assigns the text to the best suited translator in every case, depending on the target language (always a NATIVE translator) and the speciality. The assessment of the original here is vital, given that the field and type of text will determine the terminology and style, and tus guarantee the best possible sitting of the original with the end result.
		<img src='imgs/metodo2.gif' vspace='14'><p><b>TRANSLATION</b><li>It is in the phase of translation that the translator brings to bear his/her own knowledge as well as the resources and tools that MODERN TECHNOLOGY makes available. In Faus & Planas we consult on-line dictionaries, databases and centres of terminology along with the constant update of applied linguistic guidelines, our assisted translation programmes and related memory banks.
		<img src='imgs/metodo3.gif' align='left' vspace='48'><p><b>QUALITY CONTROL</b><li>�	At this stage, the translated text undergoes a strict verification procedure in order to guarantee its completion and that the numbers, names and other information included are correct.
		<li>A second translator-corrector verifies the terminological fitting and spelling of the text and submits it to an additional assessment to improve any areas that may require modification. At this stage it is vital to maintain coherence throughout all the documents of any one client with a frequent publication given that the COMPANY IMAGE may well depend on it. Our translators put together glossaries which they apply and update constantly, maintaining direct contact with the client wherever necessary.
		<img src='imgs/metodo4.gif' align='right'><p><b>FORMAT</b><li>The text format adapts entirely to the original or even improves it so as to achieve an impeccable PRESENTATION. In some cases the image of the document transmits as much as the content.
		<img src='imgs/metodo_otro.gif' align='left' vspace='4' hspace='6'><p><b>DELIVERY PUNCTUALITY</b><li>Finally, the translated text is delivered to the client with the maximum possible punctuality. The work methodology described does not allow unnecessary delays as we clearly know to what extent we can commit ourselves.
		<p><b>CONFIDENTIALITY </b>We demand a commitment to the upmost secrecy of the content of our <br><br>Clients� texts from all our personnel (be they in-house or from our external network).";
		echo "</div>";
		break;
		
		case "correccion":
		echo "<div id='ttraduccion'>Spelling, grammatical, sintax and style correction</div>";
		echo "<div class='textblanco3'><br>
		The success of a business or the prestige of a lecturer can be undermined by a defect in form. For this reason our team of professionals is on hand to correct your original documents written in any of the predominant languages (of our environment).
		<p>In the case of widely-distributed texts (catalogues, advertisement flyers, etc.), it is highly recommendable to undertake a galley proof reading: Once the layout is complete, the text undergoes an additional verification to eliminate any kind of  printing error and even to improve any last details. This way correction costs incurred once vast print runs have been done are avoided.
		<p>For this kind of service, contact with the client is fundamental as we can occasionally assess and help to find the best possible way to express your ideas on paper.";		
		echo "</div>";
		break;
		
		case "areas":
		echo "<div id='ttraduccion'>AREAS COVERED AND CLIENTS</div>";
		echo "<div class='textblanco3'><br>";
		echo "<img src='imgs/area_juridico.gif' align='left' vspace='0' hspace='4'><p><b>Legal sector </b><li>All type of legal documentation - contracts, certificates, etc. -  used on a daily basis by companies in legal dealings. Specific documents from lawyer�s firms, consultants and consultancy firms, banks and savings banks, etc. <br><br>Clients: Lawyer�s firms: (Prat y Roca, Salgado Riba and Pardo, Aequo Animo Advocats, VMV 1899 Letrados Asesores, Vendrell and Associats, Audihispana Abogados y Asesores, Folch Advocats, Larrumbe Serrano-Ferran, Buigas, Roig-Ar�n, Labastida y del R�o, Jaus�s, Conde-Escalza, Castilla, etc.); insurance companies (BCH Vida, Groupama, Zurich); legal departments of commercial companies such as Chupa-chups, Torraspapel, TAC Consultants, Cirsa, Media Planning, Miret Laboratories and IBM, amongst others.
<img src='imgs/area_financiero.gif' align='left' vspace='24' hspace='8'><p><b>Business & Finance sector </b><li>Reports, annual accounts, financial statements, stock issuing leaflets, company management studies and all manner of banking documentation. <br><br>Clients: We work actively with different banks and saving banks (contracted since 1997 by  la Caixa d�Estalvis i Pensions de Barcelona 'la Caixa' to translate the documents of its legal and fiscal consultancy departments, Credit, Auditory and E-Banking departments as well as the Banco de Europa (Factoring, Leasing and Renting). In addition we undertake work for EADA, Seraudit Auditors and Licencing Consultants and for banking entities such as Barclays and Privat Bank.
<img src='imgs/area_cultura.gif' align='left' vspace='20' hspace='6'><p><b>Culture and communication </b><li>Art exhibition catalogues and any kind of related documentation: <br><br>Clients: 'la Caixa' Foundation, the National Art Museum of Catalonia, the Museum of Contemporary Art of Barcelona, the Museum of Contemporary Art of Castilla & Le�n, Fundaci�n Biacs (Contemporary Art Biennial of Seville), Ediciones El Viso, amongst others. 
<li>Sociologist writings. <br><br>Clients: Lidia Falc�n, lawyer & writer.
<img src='imgs/area_publicitario.gif' align='left' vspace='36' hspace='6'><p><b>Commercial and publicity sector</b><li>Marketing and sales improvement programmes. Publicity leaflets for various products. Daily translation of faxes and habitual correspondence between companies. Web page translations. Power Point format presentations. <br><br>Clients: Snack Ventures, S.A., Institut Futur, Optimus Medina, Grupo Tetrans, Odisea 2000.
<img src='imgs/area_moda.gif' align='left' vspace='24' hspace='6'><p><b>Fashion sector </b><li>Publications to create awareness of new products, technical documentation (materials, production�). <br><br>Clients: Ermenegildo Zegna, Burberry, Evita Peroni.
<img src='imgs/area_alimentacion.gif' align='left' vspace='26' hspace='6'><p><b>Food sector </b><li>Information on the composition of foodstuffs, restaurant menus, general information on food, cooking recipes. <br><br>Clients:  SEB Ib�rica, Meli� Barcelona, Girafa Digital, Prats Fatj�, Semadia Marketing.
<img src='imgs/area_tecnicos.gif' align='left' vspace='60' hspace='6'><p><b>Technical documents </b><li>Car assessments & valuations. Patents. Technical information on different products from the telecommunication sector. Architecture & urbanism. Engineering. The environment. Safety specifications for chemical products. Instruction manuals for small electro-domestic devices. Health & safety at work regulations, health and environment. Internal communication documentation on plant maintenance. Computing software (user manuals). Sound and general safety proofing. Logistics. <br><br>Clients:  Carburos Met�licos, Rom�n y Asociados, GTH Iberia, NTE, Germ�n Vilalta, Rockwool, Departament de Pol�tica Territorial i Obres P�bliques (Generalitat de Catalunya), Sal�n Internacional de la Log�stica, Catalana de Tractament d�Olis Residuals, J. Isern Patentes y Marcas. 
<img src='imgs/area_medicina.gif' align='left' vspace='24' hspace='6'><p><b>Medical sector</b><li>Reports on population health and illness incidents, health awareness publications, medical slides back up documentation. <br><br>Clients: The Catalan Health Service, Asepeyo, the Academic Foundation of medical sciences, the Santa Madrona school of nursing.
<img src='imgs/area_farmaceutico.gif' align='left' vspace='12' hspace='4'><p><b>Chemical & pharmaceutical sector</b><li>Brochures and publicity for chemical and pharmaceutical companies. <br><br>Clients: Biokit, S.A., Esteve Qu�mica, Instrumentation Laboratory, Laboratorios Fardi, Amgen. 
<img src='imgs/area_cosmetica.gif' align='left' vspace='6' hspace='4'><p><b>Cosmetic sector</b><li>Documentation on cosmetic products (be it commercial or publicity, technical or regarding composition and ingredients). <br><br>Clients: Cosmobelleza, Bruno Vassari, Valmont.
<p><b>Sworn translations </b><li>The translation of all kind of documents to be presented to official Spanish organizations, in court, etc. These translations are signed by a sworn translator, specially authorized by the secretary of the interior/Home office, and come attached to the original copy, which retains its legal validity. Examples of official documents which routinely require sworn translation are: birth certificates, registration on the civil registry, marriage certificates, judicial sentences and resolutions.";
		echo "</div>";
		break;
		
		case "enviarnos":
		echo "<div id='ttraduccion'>HOW TO SEND US A TEXT</div><br>";
		echo "<div class='textblanco3'>";
		echo "<img src='imgs/icon_tfn.gif' align='left' hspace='6' vspace='8'>By telephone  <b>[93 280 28 90 </b>or <b>902 882 103]</b>, contact the translation department  and specify all possible details relating to the translation or correction required.
<p><img src='imgs/icon_raton.gif' align='left' hspace='6' vspace='8'>By E-mail  <b><a href='mailto:fausyplanas@astonidiomas.com' style='text-decoration:none;color:#FFFFFF'>[fausyplanas@astonidiomas.com]</a></b>, indicating the combination of languages required along with any other information deemed necessary and the date by which the translated or corrected text is needed.
<p><img src='imgs/icon_fax.gif' align='left' hspace='6' vspace='4'>By fax  <b>[93 280 28 84]</b>, giving your personal details and details of the translation or correction required, deadline date, etc.
<p><img src='imgs/icon_sobre.gif' align='left' hspace='6' vspace='4'>In person, at our office <b>[Pg. Manuel Girona, 82]</b>, to hand us the document you wish to translate or correct and to specify any point therein you consider of special importance.    
<p>You may request a prior fee estimate on the final approximate amount for the service via our <b><a href='1traduccion_eng.php?aux2=presupuesto' style='text-decoration:none;color:#FFFFFF;'>TARIFF ESTIMATE REQUEST</a></b> 
<p>For our <br><br>Clients� convenience, we put our standardised  <b><a href='1traduccion_eng.php?aux2=pedidos' style='text-decoration:none;color:#FFFFFF;'>ORDER FORM </a></b> at your disposal to allow you to undertake the service in the easiest and quickest possible way.";
		echo "</div>";
		break;
		
		case "contextos":
		echo "<div id='ttraduccion'>INTERPRETATION: Contexts</div><br>";
		echo "<div class='textblanco'>";
		echo "Our team is made up of duly qualified professionals, all members of the International Association of Conference Interpreters (AIIC).
		<p>We offer interpretation services in the following languages: English, French, German, Italian, Catalan, Spanish, Russian, Arabic, Chinese, Japanese, Dutch and Greek.
		<p>In case your premises are not already equipped, we can also bring all the technical elements necessary (booths, receivers, technicians) or simply a portable system (without cabins or technicians).
		<p><b>Contexts that may require interpretation: </b> 
		<br>Business meetings, presentations, debates, interviews, conferences, courses, television and radio presentations, press releases, work committees, international conferences, and exhibitions, amongst others.";
		echo "</div><br><img src='imgs/int_contextos.gif' border='0'>";
		break;
		
		case "modalidad":
		echo "<div id='ttraduccion'>Types of interpretation</div><br>";
		echo "<div class='textblanco3'>";
		echo "<img src='imgs/int_enlace.gif' align='left' hspace='6' vspace='4'><p><b>Linked interpretation.</b><br>The interpreter accompanies a client or a group who need to communicate in a different language. The context is informal and interruptions in speech are constant. This kind of interpretation is common during business meetings in trade fairs or exhibitions, for example.
		<img src='imgs/int_consecutiva.gif' align='left' hspace='6' vspace='32'><p><b>Consecutive interpretation.</b><br>For formal events at which the speaker talks for 5 to 10 minutes, during which the interpreter takes notes (according to a particular technique). The interpreter then translates what has been said. This type of interpretation is used in negotiations, presentations of programmes or projects, press releases, seminars, etc., for example.
		<img src='imgs/int_simultanea.gif' align='left' hspace='6' vspace='26'><p><b>Simultaneous interpretation.</b><br>The interpreter works from a specially equipped booth to translate the speaker�s speech simultaneously. This kind of interpretation is more agile and faster than the consecutive type and is suited to congresses, large conferences, board meetings, etc.";
		echo "</div>";
		break;
		
		case "especial":
		echo "<div id='ttraduccion'>Areas of specialization</div>";
		echo "<img src='imgs/f_traduccion2.jpg' name='imagen1'>";
		echo "<div class='textblanco2'>";
		echo "<br><b>Some of our areas of specialization:</b> 
		<p>The legal, financial, commercial, pharmaceutical, medical (cardiology, gynecology, dermatology, neurology, orthodontic, nursing, logopedic, etc.), economic, the media, the environment, technical presentations, computing, work committees, art, information technology, linguistic, architecture, literature and museology sectors.";
		echo "</div>";
		break;
		
		case "presupuesto":
		echo "<div id='ttraduccion'>Tariff Estimate Request</div>";
		echo "<div class='textblanco3'><br>";
		echo "<form name='form_presupuesto' method='post' action='4envios.php?tema=presupuesto' target='_blank'><b>CLIENT DETAILS</b>
		<br>Name & surname/s <input type='text' name='nombre' size='40' class='caja_form'>
		<br>Company <input type='text' name='empresa' size='40' class='caja_form'>
		<br>Address <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Tel. <input type='text' name='telefono' size='20' class='caja_form'>
		<br>Fax &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fax' size='20' class='caja_form'>
		<br>E-Mail <input type='text' name='email' size='40' class='caja_form'>
		<br>How did you hear about us? <input type='text' name='conocido' size='70' class='caja_form'>
		<br><br><b>SERVICE REQUIRED<hr size='1'>
		TRANSLATION</b><br><select name='jurada'><option value='jurada'>Sworn</option><option value='nojurada'>Simple/option></select>
		<br>No. of words source text(s) <input type='text' name='palabras' size='20' class='caja_form'>
		<br>Source language  <input type='text' name='iorigen' size='40' class='caja_form'>
		<br>Target language <input type='text' name='idestino' size='40' class='caja_form'>
		<br>Description of the text(s) <input type='text' name='descripcion' size='40' class='caja_form'>
		<br>Does the target text require any special format? <input type='text' name='formato' size='40' class='caja_form'>
		<br>Requested deadline <input type='text' name='entrega' size='40' class='caja_form'>
		<br>Other <input type='text' name='otros' size='40' class='caja_form'>
		<br><br><b>PROOF READING</b><br><select name='correccion1'><option value='ortografica'>Spelling, grammar, syntax & style</option><option value='revision'>Galley proof reading</option></select>
		<br>No. of words source text(s) <input type='text' name='palabras2' size='20' class='caja_form'>
		<br>Source language <input type='text' name='iorigen2' size='40' class='caja_form'>
		<br>Description of the text(s) <input type='text' name='descripcion2' size='40' class='caja_form'>
		<br>Requested deadline <input type='text' name='entrega2' size='40' class='caja_form'>
		<br>Other <input type='text' name='otros2' size='40' class='caja_form'>
		<br><br><b>INTERPRETATION</b><br>	<select name='jurada2'><option value='jurada'>Sworn</option><option value='nojurada'>Simple</option></select>
		<br>Type of interpretation:<select name='tipoint'><option valuie='simultanea'>simultaneous</option><option value='consecutiva'>consecutive</option><option value='enlace'>linked </option></select>
		<br>Do you require technical equipment?<select name='equipo'><option value='requiere'>Yes</option><option value='norequiere'>No</option></select>
		<br>(please specify: booths, receivers, microphone, headphones, etc.)
		<br>Working languages <input type='text' name='itrabajo' size='40' class='caja_form'>
		<br>Event day(s) <input type='text' name='jornada' size='45' class='caja_form'>
		<br>Other <input type='text' name='otros3' size='40' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Send'></form>";
		echo "</div>";
		break;
		
		case "pedidos":
		echo "<div id='ttraduccion'>Order Form</div>";
		echo "<div class='textblanco3'><br>";
		echo "<form name='form_pedido' method='post' action='4envios.php?tema=pedidos' target='_blank'>
		<br>DATE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fecha' size='20' class='caja_form'>
		<br>Our Ref: <input type='text' name='nref' size='20' class='caja_form'>
		<br><br><b>CLIENT DETAILS:</b>
		<br>Company: <input type='text' name='empresa' size='40' class='caja_form'>
		<br>Address: <input type='text' name='direccion' size='40' class='caja_form'>
		<br>City & post code: <input type='text' name='ciudad' size='40' class='caja_form'>
		<br>Tax Identification Number / Code:<input type='text' name='nif' size='20' class='caja_form'>
		<br>Contact name: <input type='text' name='persona' size='40' class='caja_form'>
		<br>Telephone: <input type='text' name='telefono' size='20' class='caja_form'>
		<br>Your Ref: <input type='text' name='sref' size='20' class='caja_form'>
		<br><br><b>SERVICE REQUIRED:</b>
		<br><input type='checkbox' name='cpresupuesto'>Price estimate (obligation free and without cost)
		<br><input type='checkbox' name='csencilla'>SIMPLE translation from <input type='text' name='tsencilla' size='20' class='caja_form'> 
		<br><input type='checkbox' name='cjurada'>SWORN translation from <input type='text' name='tjurada' size='20' class='caja_form'>
		<br><input type='checkbox' name='cinterpretacion'>Interpretation type <input type='radio' name='rinterpretacion'>LINKED <input type='radio' name='rinterpretacion'>SIMULTANEOUS <input type='radio' name='rinterpretacion'> CONSECUTIVE from <input type='text' name='tinterpretacion' size='20' class='caja_form'>
		<br><input type='checkbox' name='cestilo'>STYLE correction into <input type='text' name='testilo' size='20' class='caja_form'>
		<br><input type='checkbox' name='cortografica'>SPELLING correction into <input type='text' name='tortografica' size='20' class='caja_form'>
		<br><input type='checkbox' name='cgalerada'>GALLEY PROOF correction into <input type='text' name='tgalerada' size='20' class='caja_form'>
		<br><br><b>COMPLETION & HANDOVER CONDITIONS:</b>
		<br><input type='checkbox' name='cnormal'>NORMAL timing    
		<br><input type='checkbox' name='curgente'>URGENT timing (deadline by <input type='text' name='tmaximo1' size='20' class='caja_form'> at <input type='text' name='tmaximo2' size='20' class='caja_form'> o'clock) 
		<br>(subject to a 25-50% surcharge depending on urgency) 
		<br><input type='checkbox' name='cfax'>To be sent by  FAX 
		<br><input type='checkbox' name='cmail'>To be sent by E-MAIL 
		<br><input type='checkbox' name='cmensajero'>To be sent by MESSENGER
		<p>If you wish us to invoice an entity other than yourself, please give us its details below:</p>
		Company: <input type='text' name='empresa2' size='50' class='caja_form'>
		<br>Address: <input type='text' name='direccion2' size='50' class='caja_form'>
		<br>City & post code: <input type='text' name='ciudad2' size='50' class='caja_form'>
		<br>Tax Identification Number / Code: <input type='text' name='nif2' size='20' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Send'></form>";
		echo "</div>";
		break;
		
		case "traductores":
		echo "<div id='ttraduccion'>Translators' Area</div>";
		echo "<div class='textblanco3'><br>";
		echo "If you are a professional and experienced translator, then you may be interested in working for us on a <i>free-lance</i> basis. To do so, please begin by filling in the form below.
		<p>Thank you for taking the time and your interest in Faus y Planas. We look forward to the possibility of working together in the future.
		<form name='form_traductor' method='post' action='4envios.php?tema=traductores' target='_blank'><b>PERSONAL INFORMATION</b>
		<br>Name <input type='text' name='nombre' size='40' class='caja_form'>
		<br>Surname/s  <input type='text' name='apellido1' size='40' class='caja_form'>
		<br>Date of birth <input type='text' name='nacimiento' size='40' class='caja_form'>
		<br>National identity document No. <input type='text' name='nif' size='40' class='caja_form'>
		<br>Nationality <input type='text' name='nacion' size='40' class='caja_form'>
		<br>Address <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Post code <input type='text' name='cpostal' size='40' class='caja_form'>
		<br>City <input type='text' name='ciudad' size='40' class='caja_form'>
		<br>Province <input type='text' name='provincia' size='40' class='caja_form'>
		<br>Country <input type='text' name='pais size='40' class='caja_form'>
		<br>E-mail <input type='text' name='email' size='40' class='caja_form'>
		<br>Telephone <input type='text' name='telefono' size='20' class='caja_form'>
		<br>Mobile phone <input type='text' name='telefono2' size='20' class='caja_form'>
		<br>Fax &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fax' size='20' class='caja_form'>
		<br>QUALIFICATIONS <input type='text' name='titulacion' size='65' class='caja_form'>
		<br>LANGUAGE COMBINATIONS (source language ,target language)<input type='text' name='combinacion' size='65' class='caja_form'>
		<br>YEARS OF EXPERIENCE AS A TRANSLATOR <input type='text' name='experiencia' size='20' class='caja_form'>
		<br>SPECIALIZED AREAS IN WHICH YOU ROUTINELY WORK <input type='text' name='areas' size='65' class='caja_form'>
		<br>AVAILABILITY (indicate no. hours / day, week days)<input type='text' name='disponibilidad' size='65' class='caja_form'>
		<br>COMPUTING HARDWARE & SOFTWARE AT YOUR DISPOSAL <input type='text' name='herramientas' size='65' class='caja_form'>
		<br>Would you be willing to undertake a short unpaid trial translation?<select name='dispuesto'><option value='si'>Yes</option><option value='no'>No</option></select>
		<br>ADDITIONAL COMMENTS <input type='text' name='observaciones' size='65' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Send'></form>";
		echo "</div>";
		break;
		
		default:
		echo "<div id='ttraduccion'>WHY CHOOSE FAUS & PLANAS?</div>";
		echo "<img src='imgs/f_traduccion.jpg' name='imagen1'><div class='especial2'>one world, 6,000 languages</div>";
		echo "<div class='textblanco'>";
		echo "<br><p>- <b>Experience:</b> We are a consolidated company with a proven track record in translation, our field of work since 1988. Our trusted client base is your best guarantee. 
		<p>- <b>Our team:</b> We have a sound network of professional translators who only translate into their mother tongue as well as our in-house team to guarantee you the very best final result in a changing and increasingly demanding context, such as is present day communication. 
		<p>- <b>Service:</b> Daily contact with the client is fundamental in maintaining a good personal and professional relationship. We are open to any suggestion you may have and to adapt ourselves to your needs as we wish to improve every day and in order to do so, your opinion is vital.
		<p>- <b>Punctuality:</b> We meet project deadlines. We pride ourselves on our honesty and we are true to our commitments.  
		<p>- <b>Specialization:</b> We translate, proof read and correct texts of any area of specialization.
		<p>- <b>Reliability:</b> Our services are notable for their outstanding professionalism. 
		<p>- <b>Excellent price / quality relationship:</b> All the translated texts undergo an exhaustive quality control and correction process. 
		<p>- <b>Technology:</b> We have on-line dictionaries, databases, access to centres of terminology, language guidelines, assisted translation programmes and so forth at our disposal.
		<p>- <b>Absolute confidentiality and privacy </b>assured by both our in-house team and our <i>free-lance</i> personnel.  
		<p>- <b>Personalization:</b> We know how to give the most appropriate tone to each text depending on its required use. Each project is unique. We prepare terminological glossaries for each client.
		<p>We are members of the ACT (the association of centres specializing in translation), a Spanish nationwide association founded in 1990 that regulates the sector and supports its members. The ACT guarantees that all of its members (some 55 companies at present) meet certain vital requirements in order to provide the best quality in translation and interpretation services, with the guarantee of the upmost rigorousness and professionalism. The ACT is in turn a founding member of the EUATC (the union of European translation companies), an international organization whose objectives are professionalism, the promotion of quality practices and the co-operation amongst sector companies.";
		echo "</div>";
		break;
	}
	?>
	</div>
	
</div>
<?php include "c_piecera.php";?>