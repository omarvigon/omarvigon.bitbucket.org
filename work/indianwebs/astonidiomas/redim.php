<?php 

$origen="admin/galeria/Cimg6549.jpg";
$destino="admin/galeria/Cimg6549_mini.jpg";
$destino_temporal=tempnam("tmp/","tmp");
redimensionar_jpeg($origen, $destino_temporal, 80, 80, 25); 

// guardamos la imagen
$fp=fopen($destino,"w");
fputs($fp,fread(fopen($destino_temporal,"r"),filesize($destino_temporal)));
fclose($fp); 

// mostramos la imagen
echo "<a href='admin/galeria/Cimg6549.jpg' target='_blank'><img src='admin/galeria/Cimg6549_mini.jpg' border='0'></a>";

function redimensionar_jpeg($img_original, $img_nueva, $img_nueva_anchura, $img_nueva_altura, $img_nueva_calidad)
{    
	// crear una imagen desde el original    
	$img = ImageCreateFromJPEG($img_original); 
	   
	// crear una imagen nueva    
	$thumb = imagecreatetruecolor($img_nueva_anchura,$img_nueva_altura);  
	  
	// redimensiona la imagen original copiandola en la imagen    
	ImageCopyResized($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura,ImageSX($img),ImageSY($img));     
	
	// guardar la nueva imagen redimensionada donde indicia $img_nueva    
	ImageJPEG($thumb,$img_nueva,$img_nueva_calidad);    
	ImageDestroy($img);
}
?>

