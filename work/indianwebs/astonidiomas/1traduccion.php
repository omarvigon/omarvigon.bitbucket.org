<?php include "c_cabecera.php";?>

	<p><a href="1traduccion.php"><b>Por qu&eacute; elegir Faus y Planas</b></a></p>
	<span class="azul1"><b>Traducci&oacute;n y correcci&oacute;n de textos</b></span>
	   <div class="subtext2"><a href="1traduccion.php?aux2=metodo">Metodo de trabajo</a></div>
	   <div class="subtext2"><a href="1traduccion.php?aux2=correccion">Correcci&oacute;n ortogr&aacute;fica, gramatical, sint&aacute;ctica y estilo</a></div>
	   <div class="subtext2"><a href="1traduccion.php?aux2=areas">&Aacute;reas de trabajo y clientes</a></div>
	<p><a href="1traduccion.php?aux2=enviarnos"><b>C&oacute;mo enviarnos un texto</b></a></p>
	<span class="azul1"><b>Interpretaci&oacute;n</b></span>
		<div class="subtext2"><a href="1traduccion.php?aux2=contextos">Contextos</a></div>
	    <div class="subtext2"><a href="1traduccion.php?aux2=modalidad">Modalidades</a></div>
	    <div class="subtext2"><a href="1traduccion.php?aux2=especial">&Aacute;reas de especializaci&oacute;n</a></div>
	<p><a href="1traduccion.php?aux2=presupuesto"><b>Solicitud de presupuesto</b></a></p>
	<p><a href="1traduccion.php?aux2=pedidos"><b>Formulario de pedido</b></a></p>
	<p><a href="1traduccion.php?aux2=traductores"><b>&Aacute;rea para traductores</b></a></p>
	<div class="cont_abajo1">
		<a href="http://www.act.es" target="_blank"><img src="imgs/logo_act.jpg" alt="ACT" border="0" hspace="4"></a>
		<a href="http://www.euatc.org" target="_blank"><img src="imgs/logo_euatc.jpg" alt="EUATC" border="0" hspace="4"></a>
	</div>
</div>

<div id="c_traduccion">
	 <div class="correo">
	 <a href="mailto:fausyplanas@astonidiomas.com" style="text-decoration:none;color:#FFFFFF;">fausyplanas@astonidiomas.com</a>
	 </div>
	
	<?php 
	switch($aux2)
	{
		case "metodo":
		echo "<div id='ttraduccion'>Traducci&oacute;n: M&eacute;todo de trabajo</div>";
		echo "<div class='textblanco4'><br>";
		echo "<p>Nuestro objetivo es la CALIDAD FINAL del producto y &eacute;sta s&oacute;lo puede garantizarse con un equipo formado por profesionales y aplicando un estricto m&eacute;todo de trabajo.
		<p><b>ASIGNACI&Oacute;N</b><li>La persona responsable de gestionar las traducciones asigna el texto al traductor m&aacute;s indicado en cada caso, dependiendo del idioma de destino (siempre ser&aacute; un traductor NATIVO) y de la especialidad. En este punto es importante la VALORACI&Oacute;N DEL ORIGINAL, ya que el &aacute;mbito y el registro de un texto permitir&aacute;n establecer la terminolog&iacute;a y el estilo y garantizar la m&aacute;xima adecuaci&oacute;n del resultado final respecto al original.<br> 
		<p><b>TRADUCCI&Oacute;N</b><li>Es la fase de traducci&oacute;n, propiamente dicha, en la que el traductor utiliza sus propios conocimientos y todos los recursos y herramientas que la TECNOLOG&Iacute;A ACTUAL pone a su disposici&oacute;n. En Faus y Planas consultamos diccionarios electr&oacute;nicos, bases de datos y centros de terminolog&iacute;a, adem&aacute;s de actualizar constantemente la normativa ling�&iacute;stica aplicada, as&iacute; como nuestros programas de traducci&oacute;n asistida y memorias relacionadas. 
		<p><p><b>CONTROL</b><li>En esta fase, el texto ya traducido se somete a una estricta revisi&oacute;n para garantizar que est&eacute; completo y que las cifras, nombres y otros datos que aparecen sean correctos. 
		<li>Un segundo traductor-corrector supervisa la adecuaci&oacute;n terminol&oacute;gica y ortogr&aacute;fica del texto y lo somete a una valoraci&oacute;n adicional para mejorar los aspectos que pudieran requerirlo. En esta fase, es muy importante mantener la COHERENCIA en todos los documentos de un mismo cliente que presenten cierta periodicidad, ya que la IMAGEN DE UNA EMPRESA puede depender en gran parte de ello. Nuestros traductores elaboran glosarios y los aplican y actualizan constantemente, manteniendo un contacto directo con los clientes en los casos necesarios. 
		<p><b>FORMATO</b><li>El formato del texto se adapta al original o incluso se mejora, para conseguir una PRESENTACI&Oacute;N impecable. En ocasiones, la imagen de un documento comunica tanto como su contenido. 
		<p><b>PUNTUALIDAD EN LAS ENTREGAS</b><li>Finalmente, el texto traducido se entregar&aacute; al cliente con la m&aacute;xima puntualidad posible. El m&eacute;todo de trabajo descrito no supone retrasos innecesarios, ya que sabemos perfectamente hasta donde podemos comprometernos. 
		<p><b>CONFIDENCIALIDAD.</b><br><br><li>Exigimos a nuestro personal (tanto interno como externo) el compromiso de no divulgar a terceros el contenido de los textos trabajados.";
		echo "</div>";
		echo "<img src='imgs/f_metodo_trabajo.jpg'>";
		break;
		
		case "correccion":
		echo "<div id='ttraduccion'>Correcci&oacute;n ortogr&aacute;fica, gramatical, sint&aacute;ctica y estilo</div>";
		echo "<div class='textblanco22'><br>
		El &eacute;xito de un negocio o el prestigio de un conferenciante pueden verse comprometidos por un defecto de forma. Por ello, nuestros profesionales tambi&eacute;n est&aacute;n a su disposici&oacute;n para corregir documentos originales escritos en los idiomas de mayor uso en nuestro entorno. 
		<p>Si se trata de textos de amplia divulgaci&oacute;n (cat&aacute;logos, folletos publicitarios, etc.) es aconsejable adem&aacute;s realizar una revisi&oacute;n de GALERADAS: despu&eacute;s de la maquetaci&oacute;n, el texto se somete a una revisi&oacute;n adicional para descartar cualquier tipo de error tipogr&aacute;fico e incluso para mejorar alg&uacute;n detalle final. As&iacute; se evitan los costes derivados de tener que rectificar errores cuando un texto ya se ha impreso en grandes tirajes. 
		<p>Para este tipo de servicio, el contacto con el cliente es muy importante, ya que en ocasiones podemos asesorarle y ayudarle a encontrar la mejor manera de expresar sus ideas sobre el papel.";		
		echo "</div><br><img src='imgs/f_traduccion_mio2.gif' border='0'>";
		break;
		
		case "areas":
		echo "<div id='ttraduccion'>Traducci&oacute;n: &Aacute;reas de trabajo y clientes</div>";
		echo "<div class='textblanco44'><br>";
		echo "<p><b>Sector jur&iacute;dico</b><br><br><li>Documentaci&oacute;n jur&iacute;dica de todo tipo �contratos, actas, etc.� utilizada a diario por las empresas en sus tr&aacute;mites legales. Documentos espec&iacute;ficos de bufetes de abogados, consultores y asesor&iacute;as, bancos y cajas de ahorros, etc. <br><br>Clientes:<br>Bufetes de abogados (Prat y Roca, Salgado Riba and Pardo, Aequo Animo Advocats, VMV 1899 Letrados Asesores, Vendrell and Associats, Audihispana Abogados y Asesores, Folch Advocats, Larrumbe Serrano-Ferran, Buigas, Landwell Abogados y Asesoes Fiscales, Labastida y del R&iacute;o, Jaus�s, Conde-Escalza, Castilla, etc.); <br>Compa&ntilde;&iacute;as aseguradoras (BCH Vida, Groupama, Zurich); departamentos jur&iacute;dicos de empresas comerciales como Chupa-Chups, Torraspapel, Tac Consultants, Cirsa, Media Planning, Laboratorios Miret, IBM, entre otros.
		<p><b>Sector financiero y empresarial</b><li>Memorias, cuentas anuales, informes, estados financieros, folletos de emisi&oacute;n de acciones, estudios sobre gesti&oacute;n empresarial, documentaci&oacute;n bancaria de todo tipo. <br><br>Clientes: colaboramos activamente con distintas entidades bancarias y cajas de ahorros (designados desde septiembre de 1997 por la Caixa d�Estalvis i Pensions de Barcelona �la Caixa� para traducir los documentos de sus departamentos de Asesor&iacute;a jur&iacute;dica, Asesor&iacute;a fiscal, Cr&eacute;ditos, Auditor&iacute;a, Banca electr&oacute;nica y Banco de Europa (Factoring, Leasing y Renting). Asimismo, trabajamos para EADA, Seraudit Auditores y Licencing Consultants, y para entidades bancarias como Barclays Bank y Privat Bank. 
		<p><b>Cultura y comunicaciones</b><li>Cat&aacute;logos de exposiciones de arte y todo tipo de documentaci&oacute;n relacionada.  <br><br>Clientes: Fundaci&oacute;n �la Caixa�, Museu Nacional d�Art de Catalunya, Museu d�Art Contemporani de Barcelona, Museo de Arte Contempor&aacute;neo de Castilla y Le&oacute;n, Fundaci&oacute;n Biacs (Bienal de Arte Contempor&aacute;neo de Sevilla), Ediciones El Viso, entre otros.
		<li>Ensayos de tema sociol&oacute;gico. <br><br>Clientes: Lidia Falc&oacute;n.
		<p><b>Sector comercial y publicitario</b><li>Programas de marketing y mejora de las ventas. Folletos publicitarios sobre distintos productos. Traducci&oacute;n diaria de documentos de fax y correspondencia habitual entre empresas. Traducci&oacute;n de p&aacute;ginas web. Presentaciones en formato Power Point. <br><br>Clientes: Snack Ventures, S.A., Institut Futur, Optimus Medina, Grupo Tetrans, Odisea 2000.
		<p><b>Sector de la moda y la confecci&oacute;n</b><li>Publicaciones de divulgaci&oacute;n de nuevos productos, documentaci&oacute;n de tipo t&eacute;cnico (tejidos, fabricaci&oacute;n...). <br><br>Clientes: Ermenegildo Zegna, Burberry, Evita Peroni.
		<p><b>Sector de la alimentaci&oacute;n</b><li>Informaci&oacute;n sobre composici&oacute;n de los alimentos, cartas de restaurantes, divulgaci&oacute;n general sobre temas alimentarios, recetas de cocina. <br><br>Clientes: SEB Ib&eacute;rica, Meli� Barcelona, Girafa Digital, Prats Fatj&oacute;, Semadia Marketing. 
		<p><b>Documentos t&eacute;cnicos</b><li>Peritajes de autom&oacute;vil. Patentes. Informaci&oacute;n t&eacute;cnica sobre distintos productos del sector de las telecomunicaciones. Arquitectura y urbanismo. Ingenier&iacute;a. Medio ambiente. Fichas de seguridad de productos qu&iacute;micos. Instrucciones de uso de peque�os electrodom&eacute;sticos. Normas de seguridad laboral, salud y medio ambiente. Comunicados internos de mantenimiento de maquinaria. Inform&aacute;tica-software (manuales de usuario). Aislamiento t&eacute;cnico y ac&uacute;stico. Log&iacute;stica. <br><br>Clientes: Carburos Met&aacute;licos, Rom&aacute;n y Asociados, GTH Iberia, NTE, Vilalta Vila, Rockwool, Departament de Pol&iacute;tica Territorial i Obres P&uacute;bliques (Generalitat de Catalunya), Sal&oacute;n Internacional de la Log&iacute;stica, Catalana de Tractament d�Olis Residuals, J. Isern Patentes y Marcas. 
		<p><b>Medicina</b><li>Informes sobre salud de la poblaci&oacute;n e incidencia de enfermedades, publicaciones sanitarias divulgativas, documentaci&oacute;n de apoyo sobre dispositivos m&eacute;dicos. <br><br>Clientes: Banc de Sang i Teixits, Servei Catal� de la Salud, Asepeyo, Fundaci&oacute; Acad�mica de Ci�ncies M�diques, Escuela de Enfermer&iacute;a Santa Madrona. 
		<p><b>Sector qu&iacute;mico y farmac&eacute;utico</b><li>Prospectos y publicidad para empresas del sector qu&iacute;mico-farmac&eacute;utico. <br><br>Clientes: Biokit, Esteve Qu&iacute;mica, Instrumentation Laboratory, Laboratorios Fardi, Amgen. 
		<p><b>Sector de la cosm&eacute;tica</b><li>Documentaci&oacute;n sobre productos cosm&eacute;ticos (ya sea publicitaria o comercial como t&eacute;cnica, relativa a composici&oacute;n e ingredientes).<br><br>Clientes: Cosmobelleza, Bruno Vassari, Valmont.  
		<p><b>Traducciones juradas</b><li>Traducci&oacute;n de todo tipo de documentos que deban presentarse ante organismos oficiales espa�oles, en juicios, etc. Estas traducciones est&aacute;n certificadas por un traductor jurado, habilitado especialmente por el Ministerio del Interior, y se adjuntan al documento original, que conserva su validez legal. Algunos ejemplos de los documentos oficiales que suelen requerir traducci&oacute;n jurada son: partidas de nacimiento, inscripciones en el registro, certificados de matrimonio, sentencias y resoluciones judiciales, etc.";
		echo "</div>";
		echo "<img src='imgs/f_area_trabajo.jpg'>";
		break;
		
		case "enviarnos":
		echo "<div id='ttraduccion'>C&oacute;mo enviarnos un texto</div><br>";
		echo "<div class='textblanco3'>";
		echo "<img src='imgs/icon_tfn.gif' align='left' hspace='6' vspace='8'>Por tel&eacute;fono <b>[93 280 28 90 </b>o al <b>902 882 103]</b>, p&oacute;ngase en contacto con el departamento de traducci&oacute;n, especificando cualquier detalle en relaci&oacute;n con el trabajo de traducci&oacute;n o correcci&oacute;n. 
<p><img src='imgs/icon_raton.gif' align='left' hspace='6' vspace='8'>Por correo electr&oacute;nico <b><a href='mailto:fausyplanas@astonidiomas.com' style='text-decoration:none;color:#FFFFFF'>[fausyplanas@astonidiomas.com]</a></b>, indicando la combinaci&oacute;n de idiomas o cualquier otra informaci&oacute;n que consideren necesario, as&iacute; como la fecha en la que necesitan disponer del texto traducido o corregido. 
<p><img src='imgs/icon_fax.gif' align='left' hspace='6' vspace='4'>Por fax <b>[93 280 28 84]</b>, indicando en la car&aacute;tula sus datos, detalles sobre la traducci&oacute;n o correcci&oacute;n, plazo de entrega, etc.
<p><img src='imgs/icon_sobre.gif' align='left' hspace='6' vspace='4'>Personalmente, en nuestras oficinas <b>[Pg. Manuel Girona, 82]</b>, para entregarnos el documento que desean traducir y puntualizar cualquier detalle que consideren importante.
<p>Pueden solicitar presupuesto previo para conocer el importe final aproximado del servicio deseado mediante esta <b><a href='1traduccion.php?aux2=presupuesto' style='text-decoration:none;color:#FFFFFF;'>SOLICITUD DE PRESUPUESTO.</a></b> 
<p>Para facilitar los tr&aacute;mites a nuestros Clientes, ponemos a su disposici&oacute;n un <b><a href='1traduccion.php?aux2=pedidos' style='text-decoration:none;color:#FFFFFF;'>FORMULARIO DE PEDIDO</a></b> est&aacute;ndar, que le permitir&aacute; gestionar el servicio deseado de la forma m&aacute;s sencilla y r&aacute;pida.";
		echo "</div>";
		break;
		
		case "contextos":
		echo "<div id='ttraduccion'>Interpretaci&oacute;n: Contextos</div><br>";
		echo "<div class='textblanco5'>";
		echo "Nuestro equipo est&aacute; integrado por profesionales debidamente cualificados, todos ellos miembros de la Asociaci&oacute;n Internacional de Int&eacute;rpretes de Conferencia (AIIC). 
		<p>Ofrecemos servicios de interpretaci&oacute;n en los idiomas siguientes: ingl&eacute;s, franc&eacute;s, alem&aacute;n, italiano, catal&aacute;n, castellano, ruso, &aacute;rabe, chino, japon&eacute;s, holand&eacute;s, griego. 
		<p>En caso de que los locales del cliente no est&eacute;n equipados, podemos aportar los elementos t&eacute;cnicos necesarios (cabinas fijas, receptores, personal t&eacute;cnico), o bien un sistema port&aacute;til (sin cabina ni personal t&eacute;cnico).
		<p><b>Contextos que pueden requerir un servicio de interpretaci&oacute;n:</b> 
		<p>Reuniones de negocios, presentaciones, debates, entrevistas, conferencias, cursos, presentaciones en radio y televisi&oacute;n, ruedas de prensa, comit&eacute;s de empresa, conferencias internacionales, exposiciones, entre otros.";
		echo "</div><br><img src='imgs/int_contextos.gif' border='0'>";
		break;
		
		case "modalidad":
		echo "<div id='ttraduccion'>Interpretaci&oacute;n: Modalidades de interpretaci&oacute;n</div><br>";
		echo "<div class='textblanco3'>";
		echo "<img src='imgs/int_enlace.gif' align='left' hspace='6' vspace='4'><p><b>Interpretaci&oacute;n de enlace</b><br>El int&eacute;rprete acompa�a a una persona o grupo de personas que necesitan comunicarse en un idioma distinto del suyo. El contexto es informal y las interrupciones del discurso son constantes. Es la modalidad habitual, por ejemplo, para reuniones comerciales en ferias o exposiciones. 
		<img src='imgs/int_consecutiva.gif' align='left' hspace='6' vspace='28'><p><b>Interpretaci&oacute;n consecutiva</b><br>Situaciones formales en las que el orador habla entre 5 y 10 minutos, durante los cuales el int&eacute;rprete toma notas siguiendo una t&eacute;cnica determinada. A continuaci&oacute;n, el int&eacute;rprete traduce lo que se ha dicho. Se utiliza, por ejemplo, en negociaciones, presentaciones de programas o proyectos, ruedas de prensa, seminarios, etc. 
		<img src='imgs/int_simultanea.gif' align='left' hspace='6' vspace='26'><p><b>Interpretaci&oacute;n simult&aacute;nea</b><br>El int&eacute;rprete trabaja en cabinas dotadas con el material t&eacute;cnico necesario para traducir simult&aacute;neamente el discurso del orador. La interpretaci&oacute;n es m&aacute;s &aacute;gil y r&aacute;pida que la modalidad consecutiva y resulta adecuada para congresos, grandes conferencias, consejos de administraci&oacute;n, etc.";
		echo "</div>";
		break;
		
		case "especial":
		echo "<div id='ttraduccion'>Interpretaci&oacute;n: &Aacute;reas de especializaci&oacute;n</div>";
		echo "<img src='imgs/f_traduccion2.jpg' name='imagen1'>";
		echo "<div class='textblanco2'>";
		echo "<br><b>Algunas de nuestras &aacute;reas de especializaci&oacute;n:</b> 
		<p>Sector jur&iacute;dico, financiero, comercial, farmac&eacute;utico, m&eacute;dico (cardiolog&iacute;a, ginecolog&iacute;a, dermatolog&iacute;a, neurolog&iacute;a, odontolog&iacute;a, enfermer&iacute;a, logopedia, etc.), econ&oacute;mico, medios de comunicaci&oacute;n, medio ambiente, presentaciones t&eacute;cnicas, inform&aacute;tica, comit&eacute;s de empresa, arte, tecnolog&iacute;as de la informaci&oacute;n, ling�&iacute;stica, arquitectura, literatura, museolog&iacute;a.";
		echo "</div>";
		break;
		
		case "presupuesto":
		echo "<div id='ttraduccion'>Solicitud de presupuesto</div>";
		echo "<div class='textblanco3'><br>";
		echo "<form name='form_presupuesto' method='post' action='4envios.php?tema=presupuesto' target='_blank'><b>DATOS DEL SOLICITANTE</b>
		<br>Nombre y apellidos <input type='text' name='nombre' size='40' class='caja_form'>
		<br>Empresa <input type='text' name='empresa' size='40' class='caja_form'>
		<br>Direcci&oacute;n <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Tel&eacute;fono <input type='text' name='telefono' size='20' class='caja_form'>
		<br>Fax &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fax' size='20' class='caja_form'>
		<br>E-Mail <input type='text' name='email' size='40' class='caja_form'>
		<br>�C&oacute;mo nos ha conocido? <input type='text' name='conocido' size='70' class='caja_form'>
		<br><br><b>SERVICIO SOLICITADO<hr size='1'>
		TRADUCCI&Oacute;N</b><br><select name='jurada'><option value='jurada'>Jurada</option><option value='nojurada'>No Jurada</option></select>
		<br>N� de palabras texto(s) originale(s) <input type='text' name='palabras' size='20' class='caja_form'>
		<br>Idioma(s) de origen <input type='text' name='iorigen' size='40' class='caja_form'>
		<br>Idioma(s) de destino <input type='text' name='idestino' size='40' class='caja_form'>
		<br>Descripcion texto(s) <input type='text' name='descripcion' size='40' class='caja_form'>
		<br>Formato texto(s) de destino deseado <input type='text' name='formato' size='40' class='caja_form'>
		<br>Fecha de entrega deseada <input type='text' name='entrega' size='40' class='caja_form'>
		<br>Otros <input type='text' name='otros' size='40' class='caja_form'>
		<br><br><b>CORRECCI&Oacute;N</b><br><select name='correccion1'><option value='ortografica'>Ortogr&aacute;fica, gramatical, sint&aacute;ctica y de estilo</option><option value='revision'>Revisi&oacute;n de galeradas</option></select>
		<br>N� de palabras texto(s) originale(s) <input type='text' name='palabras2' size='20' class='caja_form'>
		<br>Idioma(s) de origen <input type='text' name='iorigen2' size='40' class='caja_form'>
		<br>Descripcion texto(s) <input type='text' name='descripcion2' size='40' class='caja_form'>
		<br>Fecha de entrega deseada <input type='text' name='entrega2' size='40' class='caja_form'>
		<br>Otros <input type='text' name='otros2' size='40' class='caja_form'>
		<br><br><b>INTERPRETACI&Oacute;N</b><br>
		<select name='jurada2'><option value='jurada'>Jurada</option><option value='nojurada'>No Jurada</option></select>
		<br>Tipo de interpretaci&oacute;n:<select name='tipoint'><option valuie='simultanea'>simult&aacute;nea</option><option value='consecutiva'>consecutiva</option><option value='enlace'>enlace</option></select>
		<br>�Requiere equipo t&eacute;cnico? <select name='equipo'><option value='requiere'>S&iacute;</option><option value='norequiere'>No</option></select>
		<br>(especifique: cabina, receptores, micr&oacute;fono, auriculares, etc.)
		<br>Idioma(s) de trabajo <input type='text' name='itrabajo' size='40' class='caja_form'>
		<br>Jornada(s) de trabajo <input type='text' name='jornada' size='45' class='caja_form'>
		<br>(especifique fecha, horario, lugar del evento, etc.)
		<br>Otros <input type='text' name='otros3' size='40' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Enviar'></form>";
		echo "</div>";
		break;
		
		case "pedidos":
		echo "<div id='ttraduccion'>Formulario de pedido</div>";
		echo "<div class='textblanco3'><br>";
		echo "<form name='form_pedido' method='post' action='4envios.php?tema=pedidos' target='_blank'>
		<br>FECHA <input type='text' name='fecha' size='20' class='caja_form'>
		<br>N/Ref: <input type='text' name='nref' size='20' class='caja_form'>
		<br><br><b>DATOS CLIENTE:</b>
		<br>Empresa <input type='text' name='empresa' size='40' class='caja_form'>
		<br>Direcci&oacute;n <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Ciudad y codigo postal <input type='text' name='ciudad' size='40' class='caja_form'>
		<br>NIF <input type='text' name='nif' size='20' class='caja_form'>
		<br>Persona de contacto <input type='text' name='persona' size='40' class='caja_form'>
		<br>Tel&eacute;fono <input type='text' name='telefono' size='20' class='caja_form'>
		<br>S/Ref: <input type='text' name='sref' size='20' class='caja_form'>
		<br><br><b>SERVICIO A REALIZAR:</b>
		<br><input type='checkbox' name='cpresupuesto'>Presupuesto (sin compromiso y sin coste adicional)
		<br><input type='checkbox' name='csencilla'>Traducci&oacute;n SENCILLA al idioma <input type='text' name='tsencilla' size='20' class='caja_form'> 
		<br><input type='checkbox' name='cjurada'>Traducci&oacute;n JURADA al idioma <input type='text' name='tjurada' size='20' class='caja_form'>
		<br><input type='checkbox' name='cinterpretacion'>Interpretaci&oacute;n de <input type='radio' name='rinterpretacion'>ENLACE <input type='radio' name='rinterpretacion'>SIMULT&Aacute;NEA <input type='radio' name='rinterpretacion'> CONSECUTIVA al idioma <input type='text' name='tinterpretacion' size='20' class='caja_form'>
		<br><input type='checkbox' name='cestilo'>Correcci&oacute;n de ESTILO al idioma <input type='text' name='testilo' size='20' class='caja_form'>
		<br><input type='checkbox' name='cortografica'>Correcci&oacute;n ORTOGR&Aacute;FICA al idioma <input type='text' name='tortografica' size='20' class='caja_form'>
		<br><input type='checkbox' name='cgalerada'>Correcci&oacute;n de GALERADAS al idioma <input type='text' name='tgalerada' size='20' class='caja_form'>
		<br><br><b>CONDICIONES DE ENTREGA:</b>
		<br><input type='checkbox' name='cnormal'>Plazo NORMAL  
		<br><input type='checkbox' name='curgente'>Plazo URGENTE (plazo m&aacute;ximo <input type='text' name='tmaximo1' size='20' class='caja_form'> a las <input type='text' name='tmaximo2' size='20' class='caja_form'> horas) 
		<br>(sujeto a un recargo del 25% al 50% en funci&oacute;n de la urgencia) 
		<br><input type='checkbox' name='cfax'>Env&iacute;o por FAX 
		<br><input type='checkbox' name='cmail'>Env&iacute;o por E-MAIL 
		<br><input type='checkbox' name='cmensajero'>Env&iacute;o por MENSAJERO
		<p>En caso de que debamos efectuar la factura a nombre de otra sociedad, rogamos nos indiquen a continuaci&oacute;n los datos correspondientes:</p>
		Empresa <input type='text' name='empresa2' size='50' class='caja_form'>
		<br>Direcci&oacute;n <input type='text' name='direccion2' size='50' class='caja_form'>
		<br>Ciudad y codigo postal <input type='text' name='ciudad2' size='50' class='caja_form'>
		<br>NIF <input type='text' name='nif2' size='20' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Enviar'></form>";
		echo "</div>";
		break;
		
		case "traductores":
		echo "<div id='ttraduccion'>&Aacute;rea para los traductores</div>";
		echo "<div class='textblanco3'><br>";
		echo "Si es usted un traductor profesional y experimentado quiz&aacute; est&eacute; interesado en trabajar con nosotros como <i>free-lance</i>. Para ello, puede rellenar el formulario que encontrar&aacute; a continuaci&oacute;n.
		<p>Le agradecemos el tiempo que nos ha dedicado y esperamos poder trabajar con usted en el futuro.
		<form name='form_traductor' method='post' action='4envios.php?tema=traductores' target='_blank'><b>DATOS PERSONALES</b>
		<br>Nombre <input type='text' name='nombre' size='40' class='caja_form'>
		<br>Primer apellido <input type='text' name='apellido1' size='40' class='caja_form'>
		<br>Segundo apellido <input type='text' name='apellido2' size='40' class='caja_form'>
		<br>Fecha de nacimiento <input type='text' name='nacimiento' size='40' class='caja_form'>
		<br>NIF <input type='text' name='nif' size='40' class='caja_form'>
		<br>Nacionalidad <input type='text' name='nacion' size='40' class='caja_form'>
		<br>Direcci&oacute;n <input type='text' name='direccion' size='40' class='caja_form'>
		<br>C&oacute;digo postal <input type='text' name='cpostal' size='40' class='caja_form'>
		<br>Ciudad <input type='text' name='ciudad' size='40' class='caja_form'>
		<br>Provincia <input type='text' name='provincia' size='40' class='caja_form'>
		<br>Pa&iacute;s <input type='text' name='pais' size='40' class='caja_form'>
		<br>Correo electr&oacute;nico <input type='text' name='email' size='40' class='caja_form'>
		<br>Tel&eacute;fono <input type='text' name='telefono' size='20' class='caja_form'>
		<br>M&oacute;vil <input type='text' name='telefono2' size='20' class='caja_form'>
		<br>Fax &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fax' size='20' class='caja_form'>
		<br>Titulaci&oacute;n/formaci&oacute;n <input type='text' name='titulacion' size='65' class='caja_form'>
		<br>Combinaci&oacute;n de idiomas (idioma de origen, idioma de destino) <input type='text' name='combinacion' size='65' class='caja_form'>
		<br>A&ntilde;os de experiencia como traductor <input type='text' name='experiencia' size='25' class='caja_form'>
		<br>&Aacute;reas de especializaci&oacute;n en las que trabaja habitualmente<input type='text' name='areas' size='65' class='caja_form'>
		<br>Disponibilidad (indique n� de horas /d&iacute;as, d&iacute;s a la semana)<input type='text' name='disponibilidad' size='65' class='caja_form'>
		<br>Herramientas inform&aacute;ticas de las que dispone<input type='text' name='herramientas' size='65' class='caja_form'>
		<br>�Estar&iacute;as dispuesto a realizar una breve prueba de traducci&oacute;n no remunerada?<select name='dispuesto'><option value='si'>S&iacute;</option><option value='no'>No</option></select>
		<br>Observaciones<br><input type='text' name='observaciones' size='65' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Enviar'></form>";
		echo "</div>";
		break;
		
		default:
		echo "<div id='ttraduccion'>�Por qu&eacute; elegir Faus y Planas Traducciones?</div>";
		echo "<img src='imgs/f_traduccion_mio.gif' name='imagen1'><div class='especial'>un mundo, 6.000 lenguas <br></div>";
		echo "<div class='textblanco'>";
		echo "<br><p>- <b>Experiencia:</b> somos una empresa seria con un s&oacute;lido bagaje en el campo de la traducci&oacute;n, &aacute;mbito donde nos movemos desde 1988. Nos avala una amplia cartera de Clientes. 
		<p>- <b>Equipo humano:</b> contamos con un s&oacute;lida red de profesionales de la traducci&oacute;n que s&oacute;lo trabajan hacia su idioma materno, y con varios 'in-house', capacitados para garantizar el mejor resultado final en un contexto cambiante y cada vez m&aacute;s exigente como el de la comunicaci&oacute;n actual.   
		<p>- <b>Trato:</b> el contacto diario con el cliente es fundamental para poder mantener una buena relaci&oacute;n personal y profesional. Estamos abiertos a cualquier sugerencia y a adaptarnos a sus necesidades porque deseamos mejorar d&iacute;a a d&iacute;a y para ello es fundamental contar con su opini&oacute;n.
		<p>- <b>Puntualidad:</b> finalizamos los encargos en el tiempo previsto. Somos honestos y sabemos en todo momento hasta donde podemos comprometernos.
		<p>- <b>Especializaci&oacute;n:</b> traducimos y corregimos textos de cualquier especialidad.
		<p>- <b>Fiabilidad:</b> nuestros servicios se caracterizan por su alta profesionalidad.
		<p>- <b>Excelente relaci&oacute;n calidad precio:</b> todos nuestros trabajos se someten a un exhaustivo control de calidad y correcci&oacute;n.
		<p>- <b>Tecnolog&iacute;a:</b> disponemos de diccionarios electr&oacute;nicos, bases de datos, acceso a centros de terminolog&iacute;a, normativas ling�&iacute;sticas, programas de traducci&oacute;n asistida, etc.
		<p>- <b>Confidencialidad y privacidad absoluta</b> tanto por parte del personal de la empresa como de los <i>free-lance</i>.
		<p>- <b>Personalizaci&oacute;n:</b> sabemos darle a cada texto el tono m&aacute;s adecuado dependiendo del uso del mismo. Cada encargo es &uacute;nico. Confeccionamos glosarios terminol&oacute;gicos individualizados para cada cliente.
		<p>Somos miembros de ACT (Agrupaci&oacute;n de Centros Especializados en Traducci&oacute;n), una asociaci&oacute;n espa�ola de &aacute;mbito nacional fundada en 1990 cuya actividad consiste en regular el sector y respaldar a las empresas asociadas. La ACT garantiza que todos sus miembros (un total de cincuenta y cinco empresas en la actualidad) cumplen ciertos requisitos indispensables para prestar unos servicios de traducci&oacute;n e interpretaci&oacute;n de la mejor calidad, con la garant&iacute;a del m&aacute;ximo rigor y profesionalidad. Dicha asociaci&oacute;n, a su vez, es miembro fundador de la EUATC (Uni&oacute;n de Asociaciones de Empresas de Traducci&oacute;n Europeas), una organizaci&oacute;n internacional cuyos objetivos son la profesionalidad, la promoci&oacute;n de m&eacute;todos de calidad y la cooperaci&oacute;n entre empresas del sector.";
		echo "</div>";
		break;
	}
	?>
	</div>
	
</div>
<?php include "c_piecera.php";?>