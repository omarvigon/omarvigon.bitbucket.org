<?php include "c_cabecera.php";?>
	<br><br><a href="3clases.php"><b>�Por qu&eacute; con ASTON?</b></a>
		<div class="subtext">Soluciones y experiencia</div>
	<span class="azul1"><b>Nuestros profesores y cursos</b></span>
		<div class="subtext">En sinton&iacute;a con el alumno</div>
		<div class="subtext2"><a href="3clases.php?aux3=profesores">Nuestros profesores</a></div>
		<div class="subtext2"><a href="3clases.php?aux3=cursos">Nuestros cursos</a></div>
	<br><a href="3clases.php?aux3=clases"><b>Nuestras clases</b></a>
		<div class="subtext">Traemos la clase al alumno</div>
		<div class="subtext2"><a href="3clases.php?aux3=chicos">Clases particulares para ni&ntilde;os</a></div>
		<div class="subtext2"><a href="3clases.php?aux3=adultos">Clases particulares para adultos</a></div>
		<div class="subtext2"><a href="3clases.php?aux3=empresas">Clases en empresas</a></div>
		<div class="subtext2"><a href="3clases.php?aux3=especial">Cursos especializados</a></div>
	<br><a href="3clases.php?aux3=examenes"><b>Ex&aacute;menes oficiales</b></a>
		<div class="subtext">�Prep&aacute;rese para el futuro!</div>
	<a href="3clases.php?aux3=extranjero"><b>Cursos de idiomas en todo el mundo</b></a>
		<div class="subtext">�Dise&ntilde;e su experiencia!</div>
	<a href="3clases.php?aux3=common"><b>El Marco Europeo Com&uacute;n</b></a>
		<div class="subtext">�Obtener el reconocimiento merecido!</div>
</div>

<div id="c_clases">
	<div class="correo">
	<a href="mailto:astoninfo@astonidiomas.com" class="blanco" style="text-decoration:none;">astoninfo@astonidiomas.com</a>
	</div>

	
	<?php
	switch($aux3)
	{
	case "profesores":
	echo "<div id='tclases'>Nuestros profesores: nuestro capital humano</div>";
	echo "<div class='textblanco55'>";
	echo "<br>Todas nuestras clases est&aacute;n impartidas por licenciados universitarios y profesores homologados en TESOL (ense&ntilde;anza del ingl&eacute;s para hablantes de otros idiomas) o CELTA (certificado para la ense&ntilde;anza del ingl&eacute;s a adultos), contratados tras un riguroso proceso de selecci&oacute;n. 
	<p>Nuestro equipo est&aacute; formado por profesionales motivados y din&aacute;micos que disfrutan con su trabajo. En su mayor&iacute;a, se trata de personas con gran experiencia en viajes y en la ense&ntilde;anza del idioma en diferentes partes del mundo, por lo que su personalidad enriquece y dinamiza sumamente las clases. Nuestra idea es que las clases, adem&aacute;s de servir para aprender el idioma, sean una oportunidad para romper la rutina diaria y renovar las energ&iacute;as gracias al contacto humano. 
	<p>En Aston somos conscientes de que los profesores constituyen nuestro activo m&aacute;s valioso y les proporcionamos apoyo, asesoramiento, recursos y formaci&oacute;n continua para garantizar la calidad de unas clases sumamente din&aacute;micas.";
	echo "</div>";
	echo "<img src='imgs/f_clases2.jpg' name='imagen3'>";	
	break;
	
	case "cursos":
	echo "<div id='tclases'>Nuestros cursos: en sinton&iacute;a con el alumno</div>";
	echo "<div class='textblanco4'>";
	echo "<br>Curso acad&eacute;mico para j&oacute;venes o curso financiero para empresas. Es posible que el alumno necesite un curso personalizado, adaptado a ciertas necesidades, o bien un curso de actualizaci&oacute;n o de mantenimiento. Elija entre nuestras propuestas de clases o ind&iacute;quenos qu&eacute; horario necesita; del resto nos ocuparemos nosotros. 
	<p>En cualquiera de los casos, la definici&oacute;n clara de los objetivos, la regularidad y la asistencia sistem&aacute;tica a las clases permiten alcanzar los resultados deseados. 
	<p>Cuando terminan las clases, tanto las de empresa como las particulares, se entregan informes trimestrales, y a finales de curso se realiza un examen que puede ser oficial o, en ocasiones, personalizado o interno. &Eacute;stos se complementan con una evaluaci&oacute;n de control de calidad y con informes de asistencia del alumno.";	
	echo "</div>";
	echo "<img src='imgs/f_clases3.jpg' name='imagen3'>";
	break;
	
	case "clases":
	echo "<div id='tclases'>Nuestras Clases: Traemos la clase al alumno</div>";	
	echo "<div class='textblanco0'>";
	echo "<br><p>Puede elegir entre la comodidad de las clases particulares a domicilio o la utilidad de asistir a ellas en el lugar de trabajo.
	<p>Sabemos que el ajetreado ritmo de vida actual mantiene a todo el mundo ocupado todos los minutos del d&iacute;a. Es por ello que, al desplazarnos nosotros, nos aseguramos de que el cliente aproveche al m&aacute;ximo su tiempo: nuestros profesores se adaptan a la agenda y al lugar de trabajo o al domicilio del alumno; desde primera hora de la ma&ntilde;ana hasta la noche, cinco d&iacute;as a la semana.";
	echo "</div>";
	echo "<img src='imgs/f_clases4.jpg' name='imagen3'>";
	break;
	
	case "chicos":
	echo "<div id='tclases'>Clases particulares para ni&ntilde;os: �din&aacute;micas y motivadoras!</div>";
	echo "<div class='textblanco55'>";
	echo "<br>Utilizamos los libros y los recursos m&aacute;s recientes, como DVD y juegos, para motivar a los j&oacute;venes en unas clases entretenidas que estimulan su inter&eacute;s y su aprendizaje y fomentan su participaci&oacute;n y su memoria mediante el refuerzo de est&iacute;mulos visuales.
	<ul><li>Profesores nativos con experiencia en la ense&ntilde;anza a ni&ntilde;os. 
	<li>Clases divertidas y entretenidas.
	<li>Clases a domicilio en grupos reducidos o individuales.  
	<li>Apoyo para los deberes y clases generales de ingl&eacute;s. 
	<li>Ex&aacute;menes de Cambridge para ni&ntilde;os, KET & PET.
	<li>Ex&aacute;menes orales de Trinity College (Graded Interview).</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases5.jpg' name='imagen3'>";
	break;
	
	case "adultos":
	echo "<div id='tclases'>Clases particulares para adultos: <br>combinaci&oacute;n de estilo de vida y aprendizaje</div>";
	echo "<div class='textblanco5'>";
	echo "<br>En Aston conocemos las dificultades que conlleva para un adulto el hecho de aprender un idioma. Por ello, ofrecemos la m&aacute;xima flexibilidad en cuanto a horarios, frecuencia y lugar de las clases, lo que nos permite concentrarnos en el nivel, las necesidades y el avance del alumno.
	<ul><li>Profesores nativos formales, din&aacute;micos y con una interesante personalidad. 
	<li>Conversaciones que incluyen correcci&oacute;n gramatical para mantener el nivel adquirido. 
	<li>Cursos para ex&aacute;menes oficiales.
	<li>Clases individuales o en grupos reducidos. 
	<li>Ingl&eacute;s para viajes y ocio.
	<li>Material y metodolog&iacute;a de vanguardia.</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases6.jpg' name='imagen3'>";
	break;
	
	case "empresas":
	echo "<div id='tclases'>Clases en empresas: ayudamos a trabajar y a prosperar</div>";	
	echo "<div class='textblanco5'>";
	echo "<br>Para peque&ntilde;as empresas o para multinacionales, en Aston conocemos <b>la importancia de la comunicaci&oacute;n.</b>  
	<ul><li>Nuestros profesores acuden al lugar concertado para la clase, ya sea individual o para un grupo reducido. 
	<li>Ingl&eacute;s general o espec&iacute;fico, con la frecuencia o durante el tiempo que el alumno requiera. 
	<li>Cursos con determinaci&oacute;n previa de objetivos y contenidos, adaptados a todos los niveles. 
	<li>Clases individuales de perfeccionamiento, para objetivos espec&iacute;ficos. 
	<li>Cursos de preparaci&oacute;n para ex&aacute;menes oficiales como instrumentos motivadores y de autoevaluaci&oacute;n. 
	<li>Cursos de ingl&eacute;s para la empresa: ingl&eacute;s comercial, redacci&oacute;n de faxes, cartas, informes y mensajes de correo electr&oacute;nico, estructuras para conversaci&oacute;n telef&oacute;nica. Competencia para la negociaci&oacute;n, argumentos para defender y convencer, presentaciones y conferencias. 
	<li>Asistencia peri&oacute;dica e informes sobre el avance del alumno. 
	<li>Cursos complementarios de capacitaci&oacute;n para los negocios, altamente personalizados, en el Reino Unido. </ul>";
	echo "</div><br>";
	echo "<img src='imgs/f_clases7.jpg' name='imagen3'>";
	break;
	
	case "especial":
	echo "<div id='tclases'>Cursos especializados: �busquemos la perfecci&oacute;n!</div>";
	echo "<div class='textblanco'>";
	echo "<br>Sea cual sea el nivel del alumno, le ayudaremos a decidir qu&eacute; camino seguir para lograr los mejores resultados. Descubra el nivel de ingl&eacute;s que necesita y cons&iacute;galo: acabe con los ciclos de aprendizaje estancado y escale las cotas m&aacute;s reconocidas internacionalmente. 
	<p>Le proponemos una evaluaci&oacute;n opcional para ayudarle a establecer su nivel de ingl&eacute;s. 
	<ul><li>Perfeccionamiento y mejora personal en ciertas &aacute;reas del ingl&eacute;s. 
	<li>Preparaci&oacute;n y presentaci&oacute;n a ex&aacute;menes oficiales de todos los niveles, para ni&ntilde;os o adultos, para fines acad&eacute;micos o laborales. 
	<li>Cursos de ingl&eacute;s inolvidables en algunos de los centros de estudio y ocio m&aacute;s privilegiados del mundo. 
	<li>Los cursos m&aacute;s personalizados del sector de la ense&ntilde;anza del ingl&eacute;s en las principales instituciones del Reino Unido para profesionales. Progresos demostrados en el aprendizaje del idioma en estos centros.</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases8.jpg' name='imagen3'>";
	break;
	
	case "examenes":
	echo "<div id='tclases'>Ex&aacute;menes oficiales: �prep&aacute;rese para el futuro!</div>";	
	echo "<div class='textblanco3'>";
	echo "<br>�C&oacute;mo podemos saber si hemos avanzado realmente o si hemos alcanzado el nivel de ingl&eacute;s deseado? La respuesta pasa necesariamente por la presentaci&oacute;n a ex&aacute;menes oficiales de un organismo examinador externo que nos eval�e objetivamente.
	<p><b>Prep&aacute;rese para el futuro</b> y para las exigencias profesionales con los ex&aacute;menes de Cambridge o Trinity. ASTON es un centro examinador oficial del Trinity College, de Londres, y cuenta con el reconocimiento del departamento de ex&aacute;menes ESOL de Cambridge University.
	<p><b>PRUEBA DE NIVEL: Descubra qu&eacute; lugar ocupa en el marco europeo com�n</b>  
	<p>Con nuestra prueba de nivel gratuita podremos situarle en la clasificaci&oacute;n CEF, adem&aacute;s de aclarar sus dudas y aconsejarle sobre los pasos que debe dar en el futuro.
	<br><br><a href='files/cambridge_exams.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Cambridge Exams</a><br>
	<a href='files/trinity_exams.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Trinity Exams</a><br>
	<a href='files/level_test_part1.doc' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_doc.gif' hspace='6' border='0'>Level test part1</a><br>
	<a href='files/level_test_part2.doc' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_doc.gif' hspace='6' border='0'>Level test part2</a>";
	echo "</div>";
	break;
	
	case "extranjero":
	echo "<div id='tclases'>Cursos de idiomas en todo el mundo: �dise�e tu experiencia!</div>";	
	echo "<div class='textblanco3'>";
	echo "<br>�Vivir y aprender! Aprender un idioma es mucho m&aacute;s que conocer su gram&aacute;tica y su vocabulario y aprobar ex&aacute;menes. Nuestros exclusivos cursos en todos los pa&iacute;ses de habla inglesa proporcionan un contexto ling�&iacute;stico real y la oportunidad de practicar todo lo aprendido, olvidar los temores y dar un paso definitivo hacia adelante.  
	<p>Muchas de estas propuestas de vacaciones combinadas con estudio son el complemento ideal de nuestros cursos en Barcelona, para cerrar el ciclo de aprendizaje del idioma. La larga y estrecha colaboraci&oacute;n que mantenemos con estos centros en zonas privilegiadas de todo el mundo garantiza el avance del alumno, en todos los sentidos. 
	<p><b>Ejecutivos y cargos de empresa: �Rompa con los ciclos de aprendizaje estancado y ahorre tiempo!</b> 
	<p>�Podr&iacute;a responder sincera y positivamente a estas preguntas? 
	<ul><li>�Es capaz de comprometerse un a&ntilde;o m&aacute;s a asistir a clases?  
	<li>�Cree que en las clases est&aacute;ndar se aprovechan al m&aacute;ximo el tiempo y los recursos? 
	<li>�Est&aacute; avanzando de la forma que quiere / necesita? 
	<li>�Ha alcanzado el nivel de competencia deseado? 
	<li>�Cu&aacute;ndo podr&aacute; dejar de asistir a clases regularmente?</ul> 
	<p><b>Con nuestra f&oacute;rmula:</b>
	<li>Desarrollo personal ASTON + Ingl&eacute;s comercial Pilgrims + Evaluaci&oacute;n oficial
	<p>Le garantizamos que podr&aacute; romper con los ciclos de aprendizaje estancado y dar un salto definitivo hacia adelante. Nuestras clases a medida y nuestro excepcional curso de ingl&eacute;s comercial en Canterbury, Inglaterra, le ayudar&aacute;n a ahorrar dinero a largo plazo porque: 
	<ul><li>Reducir&aacute; espectacularmente su calendario de estudio del ingl&eacute;s. 
	<li>Aprovechar&aacute; eficazmente todos los momentos que dedique a aprender ingl&eacute;s. 
	<li>Descubrir&aacute; cu&aacute;les son sus necesidades reales y se concentrar&aacute; �nicamente en ellas.
	<li>Recibir&aacute; ayuda para alcanzar su nivel de competencia deseado y poder demostrar que lo ha alcanzado. 
	<li>Marcar&aacute; unos puntos de inicio y final claros para su aprendizaje del ingl&eacute;s. </ul>
	<p>Creemos que su tiempo es oro y que el precio que paga es una inversi&oacute;n, y es por ello que merecen todo nuestro respeto. 
	<p>Vea el apartado <a href='2cursos.php' style='color:gray'>'Cursos de Idiomas'</a> en la p&aacute;gina de inicio para obtener informaci&oacute;n m&aacute;s detallada ";
	echo "</div>";
	break;
	
	case "common":
	echo "<div id='tclases'>El Marco Europeo Com&uacute;n</div>";	
	echo "<div class='textblanco'>";
	echo "<br>�En qu&eacute; punto se encuentra, dentro del recorrido del aprendizaje del ingl&eacute;s? Le proponemos que responda a la prueba de nivel gratuita y nos la remita. Estaremos encantados de corregirla y le enviaremos una orientaci&oacute;n fiable sobre su lugar de clasificaci&oacute;n, seg�n la tabla siguiente.
	<br><br><a href='files/marco_europeo_comun.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Common European Framework</a>";
	echo "</div>";
	break;
		
	default:
	echo "<div id='tclases'>�Por qu&eacute; con ASTON? - Soluciones y experiencia</div>";	
	echo "<div class='textblanco4'>";
	echo "<br><p><b>Soluciones para el aprendizaje:</b></p>Ofrecemos <b>soluciones reales</b> porque entendemos a nuestros alumnos: conocemos las dificultades que conlleva aprender un idioma y dise&ntilde;amos nuestros <b>cursos altamente personalizados</b> para adaptarlos a las necesidades y las competencias reales. 
	<p>Combinamos la <b>comodidad del servicio en el domicilio particular o en el lugar de trabajo</b> con unas clases estimulantes para obtener el m&aacute;ximo provecho del proceso de aprendizaje. 
	<p>Conjuntamente con el alumno, evaluamos y planificamos el desarrollo, el contenido y la duraci&oacute;n del curso. 
	<p><b>Credibilidad y experiencia:</b></p>Somos un equipo de <b>profesionales cualificados en EFL (ingl&eacute;s como lengua extranjera)</b> con una probada trayectoria de veinte a&ntilde;os como organizaci&oacute;n dedicada a la ense&ntilde;anza del ingl&eacute;s. Nuestro objetivo es que el alumno avance en su aprendizaje del idioma y consiga buenos resultados, e impartimos ense&ntilde;anza de la mejor calidad a un amplio espectro de alumnos: ni&ntilde;os, adolescentes y adultos, empresas, ejecutivos y otros profesionales.
	<p><b>Cursos adaptados a cualquier necesidad:</b></p> 
	<b>En todos nuestros cursos:</b> Antes de empezar, y conjuntamente con el cliente, se establecen el nivel, los objetivos y la frecuencia de las clases, as&iacute; como el contenido del curso. Se decide d&oacute;nde se quiere llegar y qu&eacute; camino hay que seguir.
	<p><b>Reconocimiento:</b> Adem&aacute;s de ser un centro examinador oficial de Trinity College, contamos con el reconocimiento del departamento de ex&aacute;menes de ESOL (ingl&eacute;s para hablantes de otros idiomas) de Cambridge University. Sin embargo, quiz&aacute;s el mejor reconocimiento de todos es la fidelidad de nuestros clientes. 
	<p><b>El ciclo completo de aprendizaje:</b> Impartimos clases de ingl&eacute;s durante el curso escolar y organizamos estancias para aprender el idioma en Espa&ntilde;a y en los pa&iacute;ses de habla inglesa durante el verano. &Eacute;stas se dirigen a todas las edades y niveles, y se ofrecen en los centros m&aacute;s exclusivos, con el compromiso de unos acuerdos �nicos para garantizar los resultados.";
	echo "</div><br>";
	echo "<img src='imgs/f_clases1.jpg' name='imagen3'>";
	break;
	
	}
	?>
	</div>
</div>

<?php include "c_piecera.php";?>