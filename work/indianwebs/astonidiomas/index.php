<?php include "c_cabecera.php";?>
	<br><br><br>
	<a class="bblue" href="index.php?aux=equipo">nuestro equipo</a>
	<a class="bblue" href="index.php?aux=delegacion">delegaciones</a>
	<a class="bblue" href="index.php?aux=contacta">contacta con nosotros</a>
	<a class="bblue" href="index.php?aux=trabaja">trabaja con ASTON</a>
	<a class="bblue" href="index.php?aux=catalogo">solicitud de cat&aacute;logo</a>
	<a class="bblue" href="index.php?aux=inscripcion">hoja de inscripci&oacute;n</a>
	<a class="bblue" href="index.php?aux=enlaces">enlaces de inter&eacute;s</a>

	<a href="index.php?aux=fotos"><img src="imgs/photogalery2.jpg" alt="Galeria de Fotos" border="0" vspace="10"></a>
</div>

<div id="contenedor2">
	<div id="fecha"></div>
	<div class="abs1"><img src="imgs/banner.gif" border="0"></div>
<?php 
	switch($aux)
	{
		case "equipo":
		echo '<div id="cverde3">NUESTRO EQUIPO</div>';
		echo '<div class="equipo">
		<span class="verde1">Directora General y Fundadora:</span><br><b>&Aacute;ngeles Faus</b><br>Licenciada en Historia - UAB 1978<br>Proficiency en Ingl&eacute;s � Cambridge University
		<br><br><span class="verde1">Gerente:</span><br><b>Eduardo Soler</b><br>Licenciado en Ingenier&iacute;a Industrial (Gesti&oacute;n) - UPC 2002
		<br><br><span class="verde1">Contabilidad y Administraci&oacute;n:</span><br><b>Mireia Falqu&eacute;s</b><br>Secretariado de Direcci&oacute;n
		<br><br><span class="verde1">Director Comercial:</span><br><b>Saverio Costanzo</b><br>Master en Relaciones Internacionales, UB 2005<br>Proficiency en Ingl&eacute;s � Cambridge University<br>Diplomado en Comunicaci&oacute;n Corporativa, UB 2002
		<br><br><span class="verde1">Departamento de Cursos de Ingl&eacute;s (In-company y particulares):</span><br><b>Jon Heavers</b><br>Bachelor of Science � London Metropolitan University 1990<br>Cambridge University CELTA (TEFL)<br>Jefe de Estudios (responsable de un equipo de 25 profesores universitarios cualificados como docentes de la lengua inglesa)<br>
		<br><span class="verde1">Departamento de Traducci&oacute;n:</span><br>
		<b>Aur&egrave;lia Manils</b><br>Licenciada en Filolog&iacute;a Inglesa, UB 1989<br>Traductora en plantilla
		<br><b>Neus Ribas</b><br>Licenciada en Traducci&oacute;n e Interpretaci&oacute;n � UPF 1999<br>Traductora jurada de ingl&eacute;s<br>Traductora en plantilla
		<br><b>Alejandra de Castro</b><br>Formaci&oacute;n profesional administrativo II<br>Administrativa
		<br><b>Jos&eacute; Manuel Alonso</b><br>Licenciado en Traducci&oacute;n e Interpretaci&oacute;n, UPF 2001<br>Traductor jurado de ingl&eacute;s y traductor en plantilla
		<br><br><span class="verde1">Departamento de Cursos en el extranjero:</span><br><b>Carla Parellada</b><br>Postgraduate in Art Administration � New South West University (Sydney 2005)<br>Licenciada en Historia del Arte, Universitat de Girona 2003<br>Responsable Atenci&oacute;n al Cliente
		<br><b>Mamen Planas</b><br>Licenciada en Historia del Arte, UB 2003<br>Consultora
		<br><b>Victoria Pasqual del Pobil</b><br>Master in Business y Recursos Humanos, European University, <br>Licenciada en ADE, European University 1995.<br>Consultora
		<br><b>Carmen Alcina</b><br>Responsable A�o Acad�mico<br>
		<br><span class="verde1">Recepci&oacute;n:</span><br><b>Eva Bas</b>
		<br>Una suerte para tod@s!</div><img src="imgs/equipo.jpg">';
		break;
	
		case "delegacion":
		echo '<div id="cverde3">DELEGACIONES</div>';
		echo '<div class="delegacion"><p><b>ISLAS BALEARES</b><br><span class="verde1">LYC-Lenguaje y Comunicacion</span><br>Foners 1, 3� B<br>07006 Palma de Mallorca<br>+34 902 36 79 18<br><a href="mailto:baleares@astonidiomas.com">baleares@astonidiomas.com</a> 
		<p><b>ARAG&Oacute;N</b><br><span class="verde1">Global World</span><br>Gran V&iacute;a, 30 bis 50005 Zaragoza<br>+34 902 88 40 69<br><a href="mailto:aragon@astonidiomas.com">aragon@astonidiomas.com</a>
		<p><b>GIRONA</b><br><span class="verde1">Isabel Alsius</span><br> +34 639 97 93 78<br><a href="mailto:girona@astonidiomas.com">girona@astonidiomas.com</a>
		<p><b>MADRID</b><br><span class="verde1">Paloma Leira</span><br>+34 605 02 76 39 (tardes)<br><span class="verde1">Paloma Fl&oacute;rez</span><br>+34 639 18 34 07 (tardes)<br><a href="mailto:madrid@astonidiomas.com">madrid@astonidiomas.com</a>
		<p><b>PA&Iacute;S VASCO-NAVARRA</b><br><span class="verde1">Natacha San Juan</span><br>+34 655 12 55 57<br><a href="mailto:pv-navarra@astonidiomas.com">pv-navarra@astonidiomas.com</a>
		<p><b>COMUNIDAD VALENCIANA</b><br><span class="verde1">Rosario Beltr&aacute;n</span><br>+34 635 41 22 15<br><a href="mailto:valencia@astonidiomas.com">valencia@astonidiomas.com</a></div>';
		echo "<img src='imgs/delegaciones.jpg'>";
		break;
		
		case "contacta":
		echo "<div id='cverde3'>CONTACTA CON NOSOTROS</div>";
		echo "<div class='cverde3_txt'><b>OFICINA PRINCIPAL</b><br><span class='verde1'>ASTON Idiomas en el Mundo</span><br>Pg. Manuel Girona, 82 08034 Barcelona<br>Tel. +34 93 280 28 90 - Fax +34 93 280 28 84<br><a href='mailto:astoninfo@astonidiomas.com'>astoninfo@astonidiomas.com</a>
		<p><b>Horario de atenci&oacute;n al cliente</b><br>de 9 a 14 y de 16 a 19.30 de lunes a viernes</div><br><br>";
		echo "<img src='imgs/contacta_con_nosotros.gif' name='imagen1'>";
		break;
		
		case "trabaja":
		echo '<div id="cverde3">TRABAJA CON NOSOTROS</div>';
		echo '<div class="cverde3_txt"><b><a href="1traduccion.php?aux2=traductores">- Traductores</a>';
		echo '<p> - <a href="files/english_teacher.doc" target="_blank">English teacher</a>';
		echo '<p> - <a href="files/monitor_camps_catalunya_baleares.doc" target="_blank">Monitor en nuestros <i>camps</i> en Catalu&ntilde;a y Baleares</a>';
		echo '<p> - <a href="files/monitor_grupos_extranjero.doc" target="_blank">Monitor acompa&ntilde;ante para los grupos en el extranjero</a>';
		echo '<p> - <a href="files/english_teacher_camps_catalunya_baleares.doc" target="_blank">English teacher for  our camps in Catalu&ntilde;a & Baleares</a></b></div><br><br>
		<img src="imgs/trabaja_nosotros.gif" border="0" alt="Trabaja con nosotros">';
		break;
		
		case "catalogo":
		echo '<div id="cverde3">SOLICITUD DE CAT&Aacute;LOGO</div>';
		echo '<div class="cverde3_txt">Cumplimente este formulario y recibir&aacute; el cat&aacute;logo por correo';
		echo '<form name="form_catalogo" method="post" action="4envios.php?tema=catalogo" target="_blank"><b>TIPO DE CAT&Aacute;LOGO</b><br>
		<input type="checkbox" name="tipo1">Adultos y ejecutivos
		<input type="checkbox" name="tipo2">J&uacute;niors extranjero
		<input type="checkbox" name="tipo3">J&uacute;niors Catalu&ntilde;a y Baleares
		<br><b>DATOS DEL SOLICITANTE</b>
		<br>Nombre y apellidos <input type="text" name="nombre" size="45" class="form_caja">
		<br>Direcci&oacute;n <input type="text" name="direccion" size="56" class="form_caja">
		<br>C&oacute;digo postal <input type="text" name="postal" size="15" class="form_caja"> Poblaci&oacute;n <input type="text" name="poblacion" size="15" class="form_caja">
		<br>Tel&eacute;fono <input type="text" name="telefono" size="15" class="form_caja"> M&oacute;vil <input type="text" name="movil" size="16" class="form_caja">
		<br>E-Mail <input type="text" name="email" size="25" class="form_caja">
		Fecha nacimiento <input type="text" name="nacimiento" size="15" class="form_caja">
		<br>�C&oacute;mo nos ha conocido?<br><textarea cols="50" rows="2" name="conocido"></textarea>
		<br>Comentarios<br><textarea cols="50" rows="2" name="comentarios"></textarea>
		<br><br><span style="padding-left:375px;"><input type="submit" class="boton_envio" value="Enviar"></span></form>';
		echo '</div>';
		break;
		
		case "inscripcion":
		echo '<div id="cverde3">HOJA DE INSCRIPCI&Oacute;N</div>';
		echo '<div class="cverde3_txt">Descargue en PDF la hoja de inscripci&oacute;n correspondiente</div>';
		echo '<div style="padding-left:150px;"><a href="files/inscripcion_adultos_ejecutivos.pdf" target="_blank"><b>ADULTOS Y EJECUTIVOS</b> <img src="imgs/inscripcion1.jpg" border="0" alt="Adultos y Ejecutivos" align="middle"></a></div>';
		echo '<div style="padding-left:10px;"><a href="files/inscripcion_juniors_extranjero.pdf" target="_blank"><img src="imgs/inscripcion2.jpg" border="0" alt="Juniors Extranjero" align="middle"> <b>J&Uacute;NIORS EN EL EXTRANJERO</b></a></div>';
		echo '<div style="padding-left:80px;"><a href="files/inscripcion_espana.pdf" target="_blank"><b>J&Uacute;NIORS EN CATALU&Ntilde;A Y BALEARES</b> <img src="imgs/inscripcion3.jpg" border="0" alt="Juniors Catalu&ntilde;a y Baleares" align="middle"></a></div>';
		break;
		
		case "enlaces":
		echo '<div id="cverde3">ENLACES DE INTER&Eacute;S</div>';
		echo '<div class="cverde3_txt"><b>WEBS EN INGLES :</b>
		<br><a href="http://www.nytimes.com/">New York Times</a>
		<br><a href="http://news.sky.com/skynews/home">Sky News</a>
		<br><a href="http://www.cnn.com/">CNN</a>
		<br><a href="http://www.washingtonpost.com/">The Washigton Post</a>
		<br><a href="http://www.timesonline.co.uk/global/">The Times</a>
		<br><a href="http://www.guardian.co.uk/">The Guardian</a>
		<br><a href="http://www.ft.com/">Financial Times</a>
		<br><br><b>WEBS EN FRANCES :</b>
		<br><a href="http://www.lemonde.fr/">Le Monde</a>
		<br><a href="http://www.louvre.fr/llv/commun/home_flash.jsp">Louvre</a>
		<br><a href="http://www.centrepompidou.fr/Pompidou/Accueil.nsf/tunnel?OpenForm">Centre Pompidou</a>
		<br><a href="http://www.volterre.fr/">Volterre</a></div>';
		break;
		
		case "fotos":
		echo '<div id="cverde3">GALERIA DE FOTOS</div>';
		echo '<div class="galeria1">';
		$conexion=mysql_connect("lldb542.servidoresdns.net","qaz095","45t0n");
		mysql_select_db("qaz095");
		$tipos=mysql_query("select distinct tipo from archivos");
		
		for($k=0;$k<mysql_num_rows($tipos);$k++)
		{
			$ctipo=mysql_fetch_row($tipos);
			$temp=substr($ctipo[0],7);
			echo "<p><b>".$temp."</b></p>";
			$consulta=mysql_query("select * from archivos where tipo='galeria".$temp."'");
			for($j=0;$j<mysql_num_rows($consulta);$j++)
			{
				$fila=mysql_fetch_row($consulta);
				echo "<a href='admin/galeria/".$fila[1]."' target='_blank'><img src='admin/minis/".$fila[1]."' border='0' align='center' vspace='2' hspace='2'></a>";
			}
		}
		
		echo '</div>';
		mysql_close($conexion);
		break;
	
		default:
		echo '
		<div><div id="crojo"><a href="1traduccion.php" style="color:#FFFFFF">TRADUCCI&Oacute;N</a></div>
			 <div id="crojo2"> y correci&oacute;n de textos</div></div>
			<div class="texto_der">En el &aacute;mbito jur&iacute;dico, t&eacute;cnico, financiero, empresarial, cultural, art&iacute;stico, comercial, comunicaciones, publicitario, moda, confecci&oacute;n, alimentaci&oacute;n, m&eacute;dico, qu&iacute;mico y farmac&eacute;utico...</div>
		<div><div id="cverde"><a href="2cursos.php" style="color:#FFFFFF">CURSOS DE IDIOMAS</a></div>
			 <div id="cverde2">para j&oacute;venes, adultos y ejecutivos</div></div>
			 <div class="texto_der">Alemania, Austria, Canad&aacute;, Estados Unidos, Francia, Irlanda, China, Malta, Reino Unido, Suiza, Campamentos en Espa&ntilde;a </div>
		<div><div id="cazul"><a href="3clases.php" style="color:#FFFFFF">CLASES DE IDIOMAS</a></div>
			 <div id="cazul2">a particulares y empresas</div></div>
			 <div class="texto_der">Cursos generales o espec&iacute;ficos, individuales o en grupo, In-company o a domicilio, soluciones ling&uuml;&iacute;sticas para empresas, preparaci&oacute;n para ex&aacute;menes oficiales</div>';
		break;
	}
	echo "</div>";
	include "c_piecera.php";
?>
