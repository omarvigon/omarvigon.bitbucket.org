<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
<?php if(isset($_GET["id"]))
{	
	editar_encabezado(); ?>  
    <table id="editar" cellpadding="0" cellspacing="4">
    <tr><th>nombre</th>
        <td><input type="text" name="nombre" value="<?php echo $fila["nombre"]?>" maxlength="64" /></td></tr>
 	<tr><th>domicilio</th>
        <td><input type="text" name="domicilio" value="<?php echo $fila["domicilio"]?>" maxlength="128" /></td></tr>
    <tr><th>provincia</th>
        <td><input type="text" name="provincia" value="<?php echo $fila["provincia"]?>" maxlength="128" /></td></tr>                
    <tr><th>email</th>
        <td><input type="text" name="email" value="<?php echo $fila["email"]?>" maxlength="64" /></td></tr>
	<tr><th>telefonos</th>
        <td><input type="text" name="telefono" value="<?php echo $fila["telefono"]?>" maxlength="32" /></td></tr>
    <tr><th>dni</th>
        <td><input type="text" name="dni" value="<?php echo $fila["dni"]?>" maxlength="16" /></td></tr>
	<tr><th>fecha nacimiento</th>
		<td><input type="text" name="fecha_nacimiento" style="width:100px;" value="<?php echo $fila["fecha_nacimiento"]?>" readonly="readonly" maxlength="10" /> <iframe height="20" src="includes/calendar/select.php?que=nacimiento" frameborder="0" marginheight="0" marginwidth="0"></iframe></td></tr>
	<tr><th>fecha alta</th>
		<td><input type="text" name="fecha_alta" style="width:100px;" value="<?php echo $fila["fecha_alta"]?>" readonly="readonly" maxlength="10" /> <iframe height="20" src="includes/calendar/select.php?que=alta" frameborder="0" marginheight="0" marginwidth="0"></iframe></td></tr>
	<tr><th>fecha baja</th>
		<td><input type="text" name="fecha_baja" style="width:100px;" value="<?php echo $fila["fecha_baja"]?>" readonly="readonly" maxlength="10" /> <iframe height="20" src="includes/calendar/select.php?que=baja" frameborder="0" marginheight="0" marginwidth="0"></iframe></td></tr>        
    <tr><th>condicion</th>
        <td><select name="estado" class="ancho150"><option value="1">Activo</option><option value="0">Inactivo</option><option value="2">Candidato</option></select></td></tr>
    <tr><th>estado civil</th>
        <td><select name="estadocivil" class="ancho150"><option value="0">Soltero</option><option value="1">Casado</option><option value="2">Separado</option></select></td></tr>        
	<tr><th>convenio</th>
        <td><select name="id_convenio" class="ancho150"><?php listar_select("convenio");?></select></td></tr>
	<tr><th>tipo contrato</th>
        <td><select name="contrato_tipo" class="ancho150"><option value="0">(Seleccionar)</option><option value="1">Indefinido</option><option value="2">Obra y servicio</option><option value="3">Interinidad</option><option value="4">Fijo discontinuo</option></select></td></tr>
    <tr><th>horas al dia por contrato</th>
        <td><input type="text" name="contrato_hora" value="<?php echo $fila["contrato_hora"]?>" maxlength="4" /></td></tr>             
	<tr><th>comentario</th>
        <td><textarea name="comentario"><?php echo $fila["comentario"]?></textarea></td></tr>        
	<tr><th>extra (atraso)</th>
		<td><input type="text" name="extra_atraso" value="<?php echo $fila["extra_atraso"]?>" maxlength="8" /></td></tr>
	<tr><th>extra (anticipo)</th>
		<td><input type="text" name="extra_anticipo" value="<?php echo $fila["extra_anticipo"]?>" maxlength="8" /></td></tr>
	<tr><th>extra (dieta)</th>
		<td><input type="text" name="extra_dieta" value="<?php echo $fila["extra_dieta"]?>" maxlength="8" /></td></tr>
    <tr><td>&nbsp;</td>
        <td><input type="button" onclick="window.open('<?php echo $_SERVER['PHP_SELF'];?>','_self');" value="  CANCELAR  " /> <input type="submit" value="  ACEPTAR  " /></td></tr>
    </table>
    </form>

<?php } else 
{ 
	$clausula=($_POST["txtbusqueda"])? "where nombre like '%".$_POST["txtbusqueda"]."%' ":"";
	tabla::listar("id,nombre,email,fecha_alta,fecha_nacimiento",$clausula); 
} ?>	

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2piecera.php"); ?>