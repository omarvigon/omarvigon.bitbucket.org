CREATE TABLE IF NOT EXISTS `trabajador` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_convenio` mediumint(8) unsigned NOT NULL,
  `estado` tinyint(3) unsigned NOT NULL,
  `estadocivil` tinyint(3) unsigned NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `domicilio` varchar(128) NOT NULL,
  `provincia` varchar(128) NOT NULL,
  `email` varchar(64) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `dni` varchar(16) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `fecha_alta` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `convenio` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) NOT NULL,
  `hora_semana` varchar(8) NOT NULL,
  `hora_diurno` varchar(8) NOT NULL,
  `hora_nocturno` varchar(8) NOT NULL,
  `hora_festivo` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `festivos` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_convenio` mediumint(8) unsigned NOT NULL,
  `dia` date NOT NULL,
  PRIMARY KEY (`id`)
 ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4)


CREATE TABLE IF NOT EXISTS `cliente` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) NOT NULL,
  `direccion` varchar(128) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `cp` varchar(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `servicio` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_cliente` mediumint(8) unsigned NOT NULL,
  `id_trabajador` mediumint(8) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `estado` tinyint(3) unsigned NOT NULL,
  `hora1` time NOT NULL,
  `hora2` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `usuario` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipo` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `estadistica` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` mediumint(9) NOT NULL,
  `fechahora` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;