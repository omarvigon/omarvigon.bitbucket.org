<?php
session_start();

function redireccionar($pagina)
{
	echo "<script type='text/javascript'>window.open('".$pagina."','_self')</script>";	
}
function var_server()
{
	foreach($_SERVER as $nombre_campo => $valor)
		echo "<br />\$".$nombre_campo."='".$valor."'";
}
function editar_encabezado()
{
	$modo=($_GET["id"]>0)? "modificar=".$_GET["id"]:"insertar=true";
	$texto=explode("=",$modo);
	echo '<h6>'.$texto[0].' '.TABLA.'</h6>
	<form action="'.$_SERVER['PHP_SELF'].'?'.$modo.'" method="post" name="f_editar">';
}
function listar_select($tabla)
{
	//escribe un listado con de <option> con todos los regtistros de una tabla
	$texto='<option value="0">(Seleccionar)</option>';
	$resultado=tabla::ejecutar("select id,nombre from ".$tabla." order by nombre");

	while($fila=mysql_fetch_assoc($resultado))
	{
		$texto.='<option value="'.$fila["id"].'">'.$fila["nombre"].'</option>';
	}
	echo $texto;
}
function extras_borrar()
{
	//pone los campos de extras a 0 en todos los trabajadores
	echo "<h6>Borrar extras</h6>";
	$consulta="update trabajador set extra_atraso='0',extra_anticipo='0',extra_dieta='0' ";
	if(tabla::ejecutar($consulta))
		echo "<p>La operacion se ha realizado con exito</p>";
}
function servicio_insertar()
{
	//genera un bucle de insert into masivo para los servicios de un trabajador
	
	$consulta="insert into servicio (id_cliente,id_trabajador,fecha,modo,hora1,hora2) values ";
	$incremento=($_POST["dias_medio"]<7)? 1:$_POST["dias_medio"];
	$fechas1=explode("-",$_POST["dcArrival"]);
	$fechas2=explode("-",$_POST["dcDeparture"]);
	$fecha1=mktime(0,0,0,$fechas1[1],$fechas1[2],$fechas1[0]);
	$fecha2=mktime(0,0,0,$fechas2[1],$fechas2[2],$fechas2[0]);
	$numerodias=round(($fecha2- $fecha1) / (60 * 60 * 24));

	for($i=0;$i<=$numerodias;$i=$i+$incremento)
	{
		$dia=date("Y-m-d",mktime(0,0,0,$fechas1[1],$fechas1[2]+$i,$fechas1[0]));
		if($_POST["dias_medio"]==1) //sin sabados y domingos
		{
			$quedia=date("w",mktime(0,0,0,$fechas1[1],$fechas1[2]+$i,$fechas1[0]));	
			if($quedia>=1 && $quedia<=5)
				$consulta.="('".$_POST["id_cliente"]."','".$_POST["id_trabajador"]."','".$dia."','".$_POST["modo"]."','".$_POST["hora1"]."','".$_POST["hora2"]."'),";
		}
		else
			$consulta.="('".$_POST["id_cliente"]."','".$_POST["id_trabajador"]."','".$dia."','".$_POST["modo"]."','".$_POST["hora1"]."','".$_POST["hora2"]."'),";
	}
	$consulta=rtrim($consulta,",");
	if(tabla::ejecutar($consulta))
		echo "<h4>Los servicios se han insertado</h4>";
}
function servicio_eliminar()
{
	$consulta="delete from servicio where id_trabajador='".$_POST["id_trabajador"]."' and id_cliente='".$_POST["id_cliente"]."' and fecha between '".$_POST["dcArrival"]."' and '".$_POST["dcDeparture"]."'";
	if(tabla::ejecutar($consulta))
		echo "<h4>Los servicios se han borrado</h4>";
}
function festivos()
{
	if($_POST["insertar"])
		tabla::ejecutar("insert into festivos set id_convenio='".$_GET["id"]."',dia='".$_POST["dia"]."' ");
	if($_POST["eliminar"])
		tabla::ejecutar("delete from festivos where id='".$_POST["eliminar"]."' ");
}
function informe_generar()
{
	$datos=array();
	
	//and modo para que no coja dias en vacaciones ??
	$fila=mysql_fetch_assoc(tabla::ejecutar("select trabajador.nombre,extra_atraso,extra_anticipo,extra_dieta,contrato_hora,hora_diurno,hora_nocturno,hora_festivo from trabajador,convenio where trabajador.id_convenio=convenio.id and trabajador.id='".$_POST["id_trabajador"]."' "));
	$datos["nombre"]=$fila["nombre"];
	$datos["contrato_hora"]=$fila["contrato_hora"];
	$datos["hora_diurno"]=$fila["hora_diurno"];
	$datos["hora_nocturno"]=$fila["hora_nocturno"];
	$datos["hora_festivo"]=$fila["hora_festivo"];
	$datos["extra_atraso"]=$fila["extra_atraso"];
	$datos["extra_anticipo"]=$fila["extra_anticipo"];
	$datos["extra_dieta"]=$fila["extra_dieta"];
	
	$datos["dias_laborables"]=calcular_dias_laborables();
	$datos["horas_mes"]=$datos["contrato_hora"]*$datos["dias_laborables"];
	
	$datos["horas_trabajadas"]=tabla::cargar0("select subtime(sec_to_time(sum(time_to_sec(hora2))),sec_to_time(sum(time_to_sec(hora1)))) from servicio where fecha like '".$_POST["ano"]."-".$_POST["mes"]."-%' and id_trabajador='".$_POST["id_trabajador"]."' ");
	$datos["horas_trabajadas"]=substr($datos["horas_trabajadas"],0,strlen($datos["horas_trabajadas"])-3);
	
	$datos["horas_extras"]=tabla::cargar0("select subtime('".$datos["horas_trabajadas"]."','".$datos["horas_mes"].":00:00')");
	$datos["horas_extras"]=substr($datos["horas_extras"],0,strlen($datos["horas_extras"])-3);
	$datos["horas_extras_importe"]=calcular_importe($datos["horas_extras"],$datos["hora_diurno"]);
	
	$datos["horas_nocturnas"]=calcular_horas_nocturnas();
	$datos["horas_nocturnas_importe"]=calcular_importe($datos["horas_nocturnas"],$datos["hora_nocturno"]);

	$datos["horas_festivas"]=tabla::cargar0("select subtime(sec_to_time(sum(time_to_sec(hora2))) , sec_to_time(sum(time_to_sec(hora1)))) from servicio where id_trabajador= '".$_POST["id_trabajador"]."' AND fecha LIKE '".$_POST["ano"]."-".$_POST["mes"]."-%' and (dayofweek(fecha)=1 or servicio.fecha in (select dia from trabajador,convenio,festivos where trabajador.id='".$_POST["id_trabajador"]."' and trabajador.id_convenio=convenio.id and convenio.id=festivos.id_convenio))");
	$datos["horas_festivas"]=substr($datos["horas_festivas"],0,strlen($datos["horas_festivas"])-3);
	$datos["horas_festivas_importe"]=calcular_importe($datos["horas_festivas"],$datos["hora_festivo"]);
	return $datos;
}
function informe_exportar($fecha_informe)
{
	$archivo="dato;valor\n";
	foreach($_SESSION["datos"] as $nombre_campo => $valor)
		$archivo.="dato ".$nombre_campo.";".$valor."\n";
		
	header("Content-Description:File Transfer");
	header("Content-type:application/csv");
	header("Content-Disposition:attachment;filename=informe".$fecha_informe.".csv");
	echo $archivo;
	exit();
}
function calendario_dia($fecha)
{
	//escribe todos los servicios para un dia concreto para un trabajador o para un cliente
	$texto="";
	if($_GET["tabla"]=="cliente")
		$consulta="select servicio.id as idservicio,nombre,modo,hora1,hora2 from servicio,trabajador where servicio.id_trabajador=trabajador.id and id_cliente='".$_GET["id_cliente"]."' and fecha='".$fecha."' order by hora1 ";
	else if($_GET["tabla"]=="trabajador")		
		$consulta="select servicio.id as idservicio,nombre,modo,hora1,hora2 from servicio,cliente where servicio.id_cliente=cliente.id and id_trabajador='".$_GET["id_trabajador"]."' and fecha='".$fecha."' order by hora1 ";
	
	$sumahora=tabla::cargar0("select subtime(sec_to_time(sum(time_to_sec(hora2))),sec_to_time(sum(time_to_sec(hora1)))) from servicio where fecha='".$fecha."' and id_".$_GET["tabla"]."='".$_GET["id_".$_GET["tabla"]]."' ");
	$resultado=tabla::ejecutar($consulta);
	
	while($fila=mysql_fetch_assoc($resultado))
	{
		$texto.="<span class='ley_".$fila["modo"]."'><a title='".$fila["nombre"]."' href='javascript:popup(".$fila["idservicio"].",0)'>".substr($fila["hora1"],0,5)."-".substr($fila["hora2"],0,5)."</a></span>";
	}
	$texto.="&nbsp;</td>
	<td>Total <b> ".substr($sumahora,0,5)."</b></td>
	<td><a href=javascript:popup(0,'".$fecha."')>Crear</a>";
	
	return $texto;
}
function popup()
{
	//hace un insert,update o delete del popup abierto para un servicio concreto
	if($_POST["accion"]=="modificar")
	{
		if(tabla::ejecutar("update servicio set id_cliente='".$_POST["id_cliente"]."',id_trabajador='".$_POST["id_trabajador"]."',modo='".$_POST["modo"]."',hora1='".$_POST["hora1"]."',hora2='".$_POST["hora2"]."' where id='".$_POST["id"]."' "))
			echo '<h4>El servicio se ha modificado</h4><script type="text/javascript">popup_cerrar();</script>';
	}
	else if($_POST["accion"]=="insertar")
	{
		if(tabla::ejecutar("insert into servicio set id_cliente='".$_POST["id_cliente"]."',id_trabajador='".$_POST["id_trabajador"]."',modo='".$_POST["modo"]."',hora1='".$_POST["hora1"]."',hora2='".$_POST["hora2"]."',fecha='".$_POST["fecha"]."' "))
			echo '<h4>El servicio se ha creado</h4><script type="text/javascript">popup_cerrar();</script>';
	}
	else if($_POST["accion"]=="eliminar")
	{
		if(tabla::ejecutar("delete from servicio where id='".$_POST["id"]."' "))
			echo '<h4>El servicio se ha borrado</h4><script type="text/javascript">popup_cerrar();</script>';
	}
}
function calcular_importe($hora,$precio)
{
	$dividir=explode(":",$hora);
	$minutos=($dividir[0]*60)+$dividir[1];
	return round($precio*($minutos/60),2);
}
function calcular_dias_laborables()
{
	$dias_laborables=0;
	for ($i=1;$i<=UltimoDia($_POST["ano"],$_POST["mes"]);$i++)
	{
		$dow = numero_dia_semana($i,$mes,$ano);
		if($dow<6)
			$dias_laborables++;
	}
	return $dias_laborables;
}
function calcular_horas_nocturnas()
{
	$suma_nocturnas="00:00";
	$resultado_nocturnas=tabla::ejecutar("SELECT id, SEC_TO_TIME(SUM(HT)) as HorasTrabajadas FROM (SELECT id, TIME_TO_SEC(SUBTIME('06:00',hora1)) as HT FROM servicio WHERE id_trabajador='".$_POST["id_trabajador"]."' and fecha like '".$_POST["ano"]."-".$_POST["mes"]."-%' and (hora1<'06:00' AND hora2>'06:00') UNION SELECT id, TIME_TO_SEC(SUBTIME(hora2,'22:00')) as HT FROM servicio WHERE id_trabajador='".$_POST["id_trabajador"]."' and fecha like '".$_POST["ano"]."-".$_POST["mes"]."-%' and (hora1<'22:00' AND hora2>'22:00')) as T1 GROUP BY id");
	while($fila_nocturna=mysql_fetch_assoc($resultado_nocturnas))
	{
		$hora1=explode(":",$suma_nocturnas);
		$hora2=explode(":",$fila_nocturna["HorasTrabajadas"]);
		$horas=(int)$hora1[0]+(int)$hora2[0];
		$minutos=(int)$hora1[1]+(int)$hora2[1];
		$horas+=(int)($minutos/60);
		$minutos=$minutos%60;
		if($horas<10)
			$horas="0".$horas;
		if($minutos<10)
			$minutos="0".$minutos;
		$suma_nocturnas=$horas.":".$minutos;	
	}
	return $suma_nocturnas;
}
//select * from servicio where id_trabajador=1 and fecha like '2009-07-%' and ((hora1 between '22:00:00' and '23:59:59') or (hora2 between '22:00:00' and '23:59:59')) or ((hora1 between '00:00:00' and '06:00:00') or (hora2 between '00:00:00' and '06:00:00'))
/*
SELECT id,SEC_TO_TIME(SUM(HT)) as HorasTrabajadas
FROM
(
 SELECT id,TIME_TO_SEC(SUBTIME('06:00',hora1)) as HT FROM servicio WHERE id_trabajador=1 and fecha like '2009-07-%' and (hora1<'06:00' AND hora2>'06:00')
 UNION
 SELECT id,TIME_TO_SEC(SUBTIME(hora2,'22:00')) as HT FROM servicio WHERE id_trabajador=1 and fecha like '2009-07-%' and (hora1<'22:00' AND hora2>'22:00')
) 
as T1  
GROUP BY id;
*/
/*************************funciones de calendario.php**********************************/
function UltimoDia($a,$m)
{
  	if (((fmod($a,4)==0) and (fmod($a,100)!=0)) or (fmod($a,400)==0)) 
  		{$dias_febrero = 29;}
  	else 
  		{$dias_febrero = 28; }
	
  	switch($m) 
  	{
    case  1: $valor = 31; break;
    case  2: $valor = $dias_febrero; break;
    case  3: $valor = 31; break;
    case  4: $valor = 30; break;
    case  5: $valor = 31; break;
    case  6: $valor = 30; break;
    case  7: $valor = 31; break;
    case  8: $valor = 31; break;
    case  9: $valor = 30; break;
    case 10: $valor = 31; break;
    case 11: $valor = 30; break;
    case 12: $valor = 31; break;
	}
  	return $valor;
}
function numero_dia_semana($d,$m,$a)
{ 
  $f = getdate(mktime(0,0,0,$m,$d,$a)); 
  $d = $f["wday"];
  if ($d==0) 
  	{$d=7;}
  return $d;
} 
?>