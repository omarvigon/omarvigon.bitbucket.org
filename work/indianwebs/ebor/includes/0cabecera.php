<?php
//ini_set("session.gc_maxlifetime","5");
//echo "<p align='center'>".ini_get("session.gc_maxlifetime")."</p>";
//ini_set("session.cache_expire","1");
//echo "<p align='center'>".session_cache_expire()."</p>";

include("funciones.php");
include("constantes.php");
include("clases.php");

if(!isset($_SESSION["autentico"]) && !strstr($_SERVER['PHP_SELF'],"index.php"))
	redireccionar("index.php");	
if(isset($_POST["login"]) && isset($_POST["password"]))
	usuario::acceder2();	
if(isset($_GET["logout"]))
	usuario::salir();
if(isset($_GET["insertar"]))
	tabla::insertar();
if(isset($_GET["modificar"]))
	tabla::modificar($_GET["modificar"]);
if(isset($_GET["eliminar"]))
	tabla::eliminar($_GET["eliminar"]);
if(isset($_GET["id"]))
	$fila=tabla::cargar($_GET["id"]);
if($_POST["exportar"])
	informe_exportar($_POST["fecha_informe"]);	
?>