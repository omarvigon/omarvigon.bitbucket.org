<?php

function UltimoDia($a,$m)
{
  	if (((fmod($a,4)==0) and (fmod($a,100)!=0)) or (fmod($a,400)==0)) 
  	{$dias_febrero = 29;}
  	else 
  	{$dias_febrero = 28; }
	
  	switch($m) 
  	{
    case  1: $valor = 31; break;
    case  2: $valor = $dias_febrero; break;
    case  3: $valor = 31; break;
    case  4: $valor = 30; break;
    case  5: $valor = 31; break;
    case  6: $valor = 30; break;
    case  7: $valor = 31; break;
    case  8: $valor = 31; break;
    case  9: $valor = 30; break;
    case 10: $valor = 31; break;
    case 11: $valor = 30; break;
    case 12: $valor = 31; break;
	}
  	return $valor;
}
function numero_dia_semana($d,$m,$a)
{ 
  $f = getdate(mktime(0,0,0,$m,$d,$a)); 
  $d = $f["wday"];
  if ($d==0) {$d=7;}
  return $d;
} 

$hoy = getdate();
$anohoy = $hoy["year"];
$meshoy  = $hoy["mon"];
$diahoy  = $hoy["mday"];
$ano = $_GET["ano"];
$mes  = $_GET["mes"];
$dia  = 1;

if (($ano==0)||($mes==0))
{
  $ano=$anohoy;
  $mes =$meshoy;
}
$dias_mes = UltimoDia($ano,$mes);
$NombreMes = constant("MES".$mes);
$NumeroSemanas = 6;
if ($mes==1) {
  $anoant = $ano-1;
  $mesant = 12;
  $anosig = $ano;
  $messig = $mes+1;
} else if ($mes==12) {
  $anoant = $ano;
  $mesant = $mes-1;
  $anosig = $ano+1;
  $messig = 1;
} else {
  $anoant = $ano;
  $mesant = $mes-1;
  $anosig = $ano;
  $messig = $mes+1;
}
$anoanterior  = $ano-1;
$anosiguiente = $ano+1;

?>
<html>
<head>
<style>
body		{margin:0;}
.tabla2		{border:0;margin:0 5px 0 0;float:left;}
th			{font:18px Arial,Helevetica,san-serif;text-align:center;background:#BD0000;color:#fff;}
td			{font:18px Arial,Helevetica,san-serif;padding:8px 15px;}
a			{color:#fff;text-decoration:none;}	
.celda1		{text-align:center;background:#ddd;color:#666;font-weight:bold;}
.celda2		{text-align:center;background:#666;color:#fff;font-weight:bold;}
</style>
</head>
<body>

<?php
$aux_mes=$mes;
$aux_ano=$ano;

echo "<table class='tabla2'><tr><th colspan='7'><a href=calendario.php?id=".$_GET["id"]."&ano=".$anoant."&mes=".$mesant."><<</a> ".$NombreMes." ".$aux_ano." <a href=calendario.php?id=".$_GET["id"]."&ano=".$anosig."&mes=".$messig.">>></a></th></tr><tr>";
echo "<th>L</th><th>M</th><th>M</th><th>J</th><th>V</th><th>S</th><th>D</th></tr>";

for ($semana=1;$semana<=$NumeroSemanas;$semana++)
{
	echo "<tr>";
	for ($diasem=1;$diasem<=7;$diasem++)
	{ 
		$dow = numero_dia_semana($dia,$aux_mes,$aux_ano);
		if (($dow==$diasem) && ($dia<=$dias_mes)) 
		{
			$valor = $dia;
			$dia++;
		}
		else 
			$valor = "&nbsp;";
			
		echo ($diasem<6)? "<td class='celda1'>".$valor."</td>":"<td class='celda2'>".$valor."</td>";
	} 
	echo "</tr>";
}
echo "</table>";
?>
</body>
</html>