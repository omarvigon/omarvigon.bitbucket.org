/////////////////// Plug-in file for CalendarXP 8.0 /////////////////
// This file is totally configurable. You may remove all the comments in this file to shrink the download size.
/////////////////////////////////////////////////////////////////////

///////////// Calendar Onchange Handler ////////////////////////////
// It's triggered whenever the calendar gets changed to y(ear),m(onth),d(ay)
// d = 0 means the calendar is about to switch to the month of (y,m); 
// d > 0 means a specific date [y,m,d] is about to be selected.
// e is a reference to the triggering event object
// Return a true value will cancel the change action.
// NOTE: DO NOT define this handler unless you really need to use it.
////////////////////////////////////////////////////////////////////
// function fOnChange(y,m,d,e) {}

///////////// Calendar AfterSelected Handler ///////////////////////
// It's triggered whenever a date gets fully selected.
// The selected date is passed in as y(ear),m(onth),d(ay)
// e is a reference to the triggering event object
// NOTE: DO NOT define this handler unless you really need to use it.
////////////////////////////////////////////////////////////////////
function fAfterSelected(y,m,d,e) {
	// gTheme is the parameter array parsed from calendar tag id.
	// gContainer is the parent window object.
	if (gTheme[3]=="gfFlat_1") {	// set arrival date
		gContainer.document.f_servicio.dcArrival.value=fDateString(y,m,d);
	} else { 						// set departure date
		gContainer.document.f_servicio.dcDeparture.value=fDateString(y,m,d);
	}
}
///////////// Calendar Cell OnDrag Handler ///////////////////////
// It triggered when you try to drag a calendar cell. (y,m,d) is the cell date. 
// aStat = 0 means a mousedown is detected (dragstart)
// aStat = 1 means a mouseover between dragstart and dragend is detected (dragover)
// aStat = 2 means a mouseup is detected (dragend)
// e is a reference to the triggering event object
// Return true to skip the set date action, if any.
// NOTE: DO NOT define this handler unless you really need to use it.
////////////////////////////////////////////////////////////////////
// function fOnDrag(y,m,d,aStat,e) {}

////////////////// Calendar OnResize Handler ///////////////////////
// It's triggered after the calendar panel has finished drawing.
// NOTE: DO NOT define this handler unless you really need to use it.
////////////////////////////////////////////////////////////////////
// function fOnResize() {}

// ====== Following are self-defined and/or custom-built functions! =======
// alter the theme option so that the calendar won't shrink automatically.
gbShrink2fit=false;

// enable agenda sharing
gbShareAgenda=true;

var fIsSelectedDate=null; // not used.

// reset date range to start from today
//omar marcar por defecto el dia actual
//gBegin=gToday;
//gsOutOfRange="Sorry, you may not go beyond the designated range!\ne.g. You may not select past days.";

function fPad0(n) {
	return n<10?"0"+n:n;
}
function fDateString(y,m,d) {	// ---  DD/MM/YYYY format
	//original
	//return fPad0(d)+"/"+fPad0(m)+"/"+y;
	//omar
	return y+"-"+fPad0(m)+"-"+fPad0(d);
}