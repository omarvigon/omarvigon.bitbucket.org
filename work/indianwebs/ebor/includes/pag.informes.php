<h6>Informes</h6>
<h5 style="margin:-10px 0 20px 0;">
<form action="index.php?modo=informes" method="post" name="f_informes">
trabajador <select name="id_trabajador" class="ancho150"><?php listar_select("trabajador");?></select>
mes <select name="mes" class="ancho150"><option value="01"><?php echo MES01?></option><option value="02"><?php echo MES02?></option><option value="03"><?php echo MES03?></option><option value="04"><?php echo MES04?></option><option value="05"><?php echo MES05?></option><option value="06"><?php echo MES06?></option><option value="07"><?php echo MES07?></option><option value="08"><?php echo MES08?></option><option value="09"><?php echo MES09?></option><option value="10"><?php echo MES10?></option><option value="11"><?php echo MES11?></option><option value="12"><?php echo MES12?></option></select>
a&ntilde;o <select name="ano" class="ancho150"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option></select>
<input type="submit" value=" GENERAR " />
</form>
</h5>

<?php 
if($_POST["id_trabajador"]) 
{	
	$_SESSION["datos"]=informe_generar();
	//almaceno en variable de session para poder escribir todos los datos en el csv; ?>
    
    <table id="editar" cellpadding="2" cellspacing="4">
    <tr><th>trabajador</th><td><?php echo $_SESSION["datos"]["nombre"]?></td>
    	<td>&nbsp;&nbsp;</td>
        <th>fecha</th><td><?php echo constant("MES".$_POST["mes"])." ".$_POST["ano"]." <i>(".$_SESSION["datos"]["dias_laborables"]." dias laborables)</i>";?></td></tr>
    
    <tr><th>horas dia por contrato</th><td><?php echo $_SESSION["datos"]["contrato_hora"]?>:00 h.</td>
    	<td>&nbsp;&nbsp;</td>
        <th>importe hora diurno</th><td><?php echo $_SESSION["datos"]["hora_diurno"]?> &euro;</td></tr>
        
    <tr><th>horas semana por contrato</th><td><?php echo ($_SESSION["datos"]["contrato_hora"]*5)?>:00 h.</td>
    	<td>&nbsp;&nbsp;</td>
        <th>importe hora nocturno</th><td><?php echo $_SESSION["datos"]["hora_nocturno"]?> &euro;</td></tr>
        
    <tr><th>horas mes por contrato</th><td><?php echo $_SESSION["datos"]["horas_mes"];?>:00 h.</td>
    	<td>&nbsp;&nbsp;</td>
        <th>importe hora festivo</th><td><?php echo $_SESSION["datos"]["hora_festivo"]?> &euro;</td></tr>
    
    <tr><th colspan="5"><hr /></th></tr>    
	<tr><th>horas trabajadas</th><td><?php echo $_SESSION["datos"]["horas_trabajadas"];?> h.</td>
    	<td>&nbsp;&nbsp;</td>
        <th>&nbsp;</th><td>&nbsp;</td></tr>  
        
    <tr><th>horas extras trabajadas</th><td><?php echo $_SESSION["datos"]["horas_extras"];?> h.</td>
    	<td>&nbsp;&nbsp;</td>
        <th>importe de horas extras</th><td><?php echo $_SESSION["datos"]["horas_extras_importe"];?> &euro;</td></tr>            
        
    <tr><th>horas nocturnas trabajadas *</th><td><?php echo $_SESSION["datos"]["horas_nocturnas"];?> h.</td>
    	<td>&nbsp;&nbsp;</td>
		<th>importe de horas nocturnas</th><td><?php echo $_SESSION["datos"]["horas_nocturnas_importe"];?> &euro;</td></tr>
        
    <tr><th>horas festivas trabajadas **</th><td><?php echo $_SESSION["datos"]["horas_festivas"];?> h.</td>
    	<td>&nbsp;&nbsp;</td>
    	<th>importe de horas festivas</th><td><?php echo $_SESSION["datos"]["horas_festivas_importe"];?> &euro;</td></tr>
    
    <tr><th colspan="5"><hr /></th></tr>
    <tr><th>extras atraso</th><td><?php echo $_SESSION["datos"]["extra_atraso"]?> &euro;</td>
    	<td>&nbsp;&nbsp;</td>
    	<th>-</th><td>-</td></tr>
        
    <tr><th>extras anticipo</th><td><?php echo $_SESSION["datos"]["extra_anticipo"]?> &euro;</td>
    	<td>&nbsp;&nbsp;</td>
    	<th>-</th><td>-</td></tr> 
        
    <tr><th>extras dieta</th><td><?php echo $_SESSION["datos"]["extra_dieta"]?> &euro;</td>
    	<td>&nbsp;&nbsp;</td>
    	<th>-</th><td>-</td></tr>
    </table>
    <fieldset>
    	<legend>Leyenda</legend>
        <cite>(*) Desde las 22:00h hasta las 06:00h</cite>
        <cite>(**) Los domingos</cite>
    </fieldset>
    <p>
    <form action="index.php?modo=informes" method="post">
    <input type="hidden" name="fecha_informe" value="<?php echo constant("MES".$_POST["mes"])."".$_POST["ano"];?>"/>
    <input type="hidden" name="exportar" value="true" />
    <input type="submit" value="EXPORTAR" />
    </form>
    </p>
    
<?php } ?>