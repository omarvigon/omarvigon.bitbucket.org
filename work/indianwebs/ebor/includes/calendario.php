<?php
include("funciones.php");
include("constantes.php");
include("clases.php");

$hoy = getdate();
$anohoy = $hoy["year"];
$meshoy  = $hoy["mon"];
$diahoy  = $hoy["mday"];
$ano = $_GET["ano"];
$mes  = $_GET["mes"];
$dia  = 1;

if (($ano==0)||($mes==0))
{
  $ano=$anohoy;
  $mes =$meshoy;
}
$dias_mes = UltimoDia($ano,$mes);
if ($mes==1) {
  $anoant = $ano-1;
  $mesant = 12;
  $anosig = $ano;
  $messig = $mes+1;
} else if ($mes==12) {
  $anoant = $ano;
  $mesant = $mes-1;
  $anosig = $ano+1;
  $messig = 1;
} else {
  $anoant = $ano;
  $mesant = $mes-1;
  $anosig = $ano;
  $messig = $mes+1;
}
$mes=($mes<10)? "0".$mes:$mes;
?>
<html>
<head>
<style>
body		{margin:0;}
h4			{width:700px;font:14px Arial,Helevetica,san-serif;margin:0;padding:5px;font-weight:bold;background:#BD0000;color:fff;text-align:center;}
h4 a		{color:fff;}
table		{margin:0;} /*border-collapse:collapse;*/
cite 		{border-top:1px solid #fff;}
a			{color:#BD0000;}	
a img		{border:0;}

th			{font:11px Arial,Helevetica,san-serif;text-align:center;width:20px;}
td			{font:11px Arial,Helevetica,san-serif;}
.celda1	th	{background:#ccc;color:#666;font-weight:bold;}
.celda2	th	{background:#333;color:#fff;font-weight:bold;}
.celda3 th	{background:#BD0000;color:#fff;}

.celda1	td	{background:#ddd;}
.celda2	td	{background:#ddd;}
.celda3 td	{background:#ddd;}

.ley_1 a	{color:#093;font-weight:bold;padding:0 10px 0 0;/*background:#093;*/}
.ley_2 a	{color:#C60;font-weight:bold;padding:0 10px 0 0;/*background:#C60;*/}
.ley_3 a 	{color:#06F;font-weight:bold;padding:0 10px 0 0;/*background:#06F;*/}
.ley_4 a	{color:#FC0;font-weight:bold;padding:0 10px 0 0;/*background:#FC0;*/}
</style>
<script type="text/javascript" src="_funciones.js"></script>
</head>
<body>

<?php
$sumahoras=tabla::cargar0("select subtime(sec_to_time(sum(time_to_sec(hora2))),sec_to_time(sum(time_to_sec(hora1)))) from servicio where fecha like '".$ano."-".$mes."-%' and id_".$_GET["tabla"]."='".$_GET["id_".$_GET["tabla"]]."' ");

$texto="<h4><a href=calendario.php?tabla=".$_GET["tabla"]."&id_".$_GET["tabla"]."=".$_GET["id_".$_GET["tabla"]]."&ano=".$anoant."&mes=".$mesant."><<</a> <a href='javascript:window.location.reload();'>".constant("MES".$mes)." ".$ano."</a> <a href=calendario.php?tabla=".$_GET["tabla"]."&id_".$_GET["tabla"]."=".$_GET["id_".$_GET["tabla"]]."&ano=".$anosig."&mes=".$messig.">>></a></h4>
<table width='700' cellspacing='1' cellpadding='3'>";

for ($i=1;$i<=$dias_mes;$i++)
{
	$dow = numero_dia_semana($i,$mes,$ano);
	$i=($i<10)? "0".$i:$i;
	$diafestivo=tabla::cargar0("select dia from festivos,convenio,trabajador where festivos.id_convenio=convenio.id and convenio.id=trabajador.id_convenio and trabajador.id='".$_GET["id_trabajador"]."' and dia='".$ano."-".$mes."-".$i."'");
	if($diafestivo)
		$texto.="<tr class='celda3'>";
	else
		$texto.=($dow<6)? "<tr class='celda1'>":"<tr class='celda2'>";	
	$texto.="<th>".$i."</th><th>".constant("DIA_".$dow)."</th>";
	$texto.="<td width='540'>".calendario_dia($ano."-".$mes."-".$i)."</td>";
	$texto.="</tr>";
}
echo $texto."<tr class='celda3'><th colspan='2'>TOTAL</th><th>-</th><th colspan='2'>".$sumahoras." horas</th></tr></table>";
?>
</body>
</html>