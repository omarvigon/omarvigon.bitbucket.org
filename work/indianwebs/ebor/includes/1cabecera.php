<?php include($_SERVER['DOCUMENT_ROOT']."/includes/0cabecera.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Panel de control</title>
    <link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
    <script type="text/javascript" src="includes/_funciones.js"></script>
</head>

<body>
<div id="left">
<a href="index.php"><img src="img/logo.gif" alt="Logo" /></a>

<?php if(isset($_SESSION["autentico"])) { ?>
    <div>
    <a accesskey="1" href="editar.trabajador.php?id=true"><b>&middot;</b> Nuevo trabajador</a>
    <a accesskey="2" href="editar.trabajador.php"><b>&middot;</b> Listar trabajadores</a>
    <a accesskey="z" href="index.php?modo=buscar"><b>&middot;</b> Buscar trabajador</a>
    <a accesskey="3" href="javascript:confirma_extras()"><b>&middot;</b> Borrar extras</a>
    <hr size="1" />
    <a accesskey="4" href="editar.convenio.php?id=true"><b>&middot;</b> Nuevo convenio</a>
    <a accesskey="5" href="editar.convenio.php"><b>&middot;</b> Listar convenios</a>
    <hr size="1" />
    <a accesskey="6" href="editar.servicio.php?modo=nuevo"><b>&middot;</b> Nuevos servicios</a>
    <a accesskey="7" href="editar.servicio.php?modo=eliminar"><b>&middot;</b> Eliminar servicios</a>
    <a accesskey="8" href="editar.servicio.php"><b>&middot;</b> Cuadrante de servicios</a>
    <hr size="1" />
    <a accesskey="6" href="index.php?modo=informes"><b>&middot;</b> Informes</a>
    <hr size="1" />
    <a accesskey="9" href="editar.cliente.php?id=true"><b>&middot;</b> Nuevo cliente</a>
    <a accesskey="q" href="editar.cliente.php"><b>&middot;</b> Listar cliente</a>
    <hr size="1" />
    <a accesskey="w" href="editar.usuario.php?id=true"><b>&middot;</b> Nuevo usuario</a>
    <a accesskey="e" href="editar.usuario.php"><b>&middot;</b> Listar usuarios</a>
    <a accesskey="e" href="#"><b>&middot; Estadisticas de usuarios</b></a>
    <hr size="1" />
    <a accesskey="0" href="<?php echo $_SERVER['PHP_SELF']."?logout=true";?>"><b>&middot;</b> Salir</a>
    </div>
<?php } else { ?>
    <form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
	<label>Login</label>
	<input type="text" name="login" size="22" maxlength="22" />
	<label>Password</label>
	<input type="password" name="password" size="22" maxlength="22" />
	<input type="submit" value=" ENVIAR " />
	</form>
<?php } ?>
</div>
<div id="centro">