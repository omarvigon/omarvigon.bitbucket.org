<fieldset>
	<h4>1 de cada 4 personas sufre un exceso de sudoraci&oacute;n en los pies</h4>
	<h5>En los pies tenemos 600 gl&aacute;ndulas sudor&iacute;paras por cm&sup2;</h5>
	<p style="font-size:11px;margin:0 16px 4px 0;">El sudor est&aacute; compuesto por casi un 99% de agua, donde hay disueltas sustancias como sales, urea o &aacute;cido l&aacute;ctico. Su descomposici&oacute;n es la causante del mal olor.</p>
    <p style="font-size:11px;margin:0 16px 4px 0;">El exceso de sudor procedente de las gl&aacute;ndulas se puede regular, eliminando tambi&eacute;n el mal olor, con productos especializados: PEUSEK-ba&ntilde;o, PEUSEK-hydro y SEK-sport.</p>
</fieldset>