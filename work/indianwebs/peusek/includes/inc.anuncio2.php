<fieldset>
	<h4>&iquest; Por qu&eacute; nos huelen los pies?</h4>
	<h5>El 80% de la poblaci&oacute;n sufre o ha sufrido problemas en los pies. El m&aacute;s frecuente: el mal olor.</h5>
	<p style="font-size:11px;margin:0 16px 4px 0;">El ritmo de vida actual, el uso de calzado desportivo o los zapatos y botas pueden producir exceso de sudor, que, al descomponerse, causa el mal olor en los pies.</p>
    <p style="font-size:11px;margin:0 16px 4px 0;">Para neutralizar y eliminar el mal olor es recomendable usar a diario desodorantes espec&iacute;ficos:  PEUSEK-sport y/o plantillas: PEUSEK-plantillas.</p>
</fieldset>