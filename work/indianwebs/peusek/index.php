<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Evita el exceso de sudor en los pies y el mal olor en los pies</title>
	<meta name="description" content="Productos para los pies, antitranspirantes, desodorantes para los pies, refrescantes y cremas para los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="imghome">
    	<img title="Experiencia y amor por los pies" src="http://www.peusek.es/img/peusek-home.jpg" alt="Experiencia y amor por los pies" />
    </div>
    <div class="cuerpohome">
        <div class="columna1">
        <h2>antitranspirantes</h2> 
        <p>&middot; <a title="Ba&ntilde;o para los pies" href="http://www.peusek.es/productos/bano.php">Ba&ntilde;o</a></p>
		<p>&middot; <a title="Hydro para los pies" href="http://www.peusek.es/productos/hydro.php">Hidro</a></p>
		<p>&middot; <a title="Sek para los pies" href="http://www.peusek.es/productos/sek.php">Sek</a></p>
        </div>
        
        <div class="columna2">
        <h2>desodorantes</h2>
        <p>&middot; <a title="Express para los pies" href="http://www.peusek.es/productos/express.php">Express</a></p>
		<p>&middot; <a title="Express 150 para los pies" href="http://www.peusek.es/productos/express150.php">Express 150</a></p>
		<p>&middot; <a title="Plantillas para los pies" href="http://www.peusek.es/productos/plantillas.php">Plantillas</a></p>
		<p>&middot; <a title="Sek para los pies" href="http://www.peusek.es/productos/sek.php">Sek</a></p>
        </div>
        
        <div class="columna3">
        <h2>refrescantes</h2>
        <p>&middot; <a title="Arcandol para los pies" href="http://www.peusek.es/productos/arcandol.php">Arcandol</a></p>
		<p>&middot; <a title="Sek para los pies" href="http://www.peusek.es/productos/sek.php">Sek</a></p>
        </ul>
        </div>
        
       	<div class="columna4">
        <h2>cremas</h2>
        <p>&middot; <a title="Crem para los pies" href="http://www.peusek.es/productos/crema.php">Crema</a></p>
        </div>
     </div>	

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>