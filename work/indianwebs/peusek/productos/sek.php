<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Sek</title>
	<meta name="description" content="Productos Peusek Sek. Desodorante Antitranspirante Refrescante para los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
    
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Sek Peusek" src="http://www.peusek.es/img/producto_sek.jpg" alt="Sek Peusek" />
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Sek</h1>
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/inc.anuncio1.php");?>
            
        	<h2>Desodorante Antitranspirante Refrescante</h2>
			<h3>&bull; CARACTER&Iacute;STICAS:</h3>
			<p>&middot; Un solo producto para distintintos problemas.<br />&middot; Personalizado con marca y dise&ntilde;o propio.<br />&middot; En &ldquo;Spray&rdquo; con gas propelente (respetando la capa de ozono).</p>
			<h3>&bull; EL PRODUCTO DE LAS SOLUCIONES:</h3>
			<p><b>&middot;  DESODORANTE:</b> Combate la descomposici&oacute;n del sudor, impidiendo su mal olor.<br />
    		<b>&middot; ANTITRANSPIRANTE:</b> Regula el exceso de sudor de los pies, sin dificultar la transpiraci&oacute;n normal.<br />
    		<b>&middot;  REFRESCANTE:</b> Con efecto tonificante perceptible de inmediato, en los pies fatigados y ardorosos.</p>
    	</div>	
    </div>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>