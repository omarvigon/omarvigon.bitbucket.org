<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Ba&ntilde;o</title>
	<meta name="description" content="Productos Peusek Ba&ntilde;o. Tratamiento antitranspirante para los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<br /><br />
            <img title="Bano Peusek" src="http://www.peusek.es/img/producto_bano.jpg" alt="Bano Peusek" />
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Ba&ntilde;o</h1>
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/inc.anuncio1.php");?>
            
        	<h2>Tratamiento antitranspirante</h2>
			<h3>&bull; INDICACIONES:</h3>
			<p>Controla el <b>exceso de sudor y mal olor</b> causado por su descomposici&oacute;n. Peusek-ba&ntilde;o, corrige la sudoraci&oacute;n excesiva de los pies, sin impedir su normal transpiraci&oacute;n.</p>
			<h3>&bull; MODO DE EMPLEO:</h3>
			<p>Pediluvio con el contenido del sobre N&ordm; 1, seguido de espolvoreado con  el sobre N&ordm; 2.&nbsp; Act&uacute;a con una sola aplicaci&oacute;n con efectos de larga  duraci&oacute;n.</p>
			<h3>&bull; PRESENTACI&Oacute;N:</h3>
			<p>Estuche con dos sobres de polvo</p>
    	</div>	
    </div>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>