<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Hydro</title>
	<meta name="description" content="Productos Peusek Hydro. Combate el exceso de sudor y el mal olor de los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Hydro Peusek" src="http://www.peusek.es/img/producto_hydro.jpg" alt="Hydro Peusek" />	
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Hydro</h1>
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/inc.anuncio1.php");?>
            
        	<h2>Loci&oacute;n Antitranspirante </h2>
			<h3>&bull; INDICACIONES:</h3>
			<p>Combate el exceso de sudor y el mal olor de los pies. Complemento de su  aseo matinal, que confiere plena seguridad en su higiene personal.  Aplicaci&oacute;n c&oacute;moda y agradable, por el toque de ligereza y frescor que  proporciona.</p>
			<h3>&bull; MODO DE EMPLEO:</h3>
			<p>Agradable y c&oacute;moda aplicaci&oacute;n, mediante pulverizado del pie, varias  veces por semana o diariamente a cualquier hora.</p>
			<h3>&bull; PRESENTACI&Oacute;N:</h3>
			<p>Vaporizador l&iacute;quido de 100 ml. Sin gas.</p>
    	</div>	
    </div>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>