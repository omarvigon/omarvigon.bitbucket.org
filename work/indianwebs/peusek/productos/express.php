<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Express</title>
	<meta name="description" content="Productos Peusek Express. Desodorante para calzado y pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Express Peusek" src="http://www.peusek.es/img/producto_express.jpg" alt="Express Peusek" />	
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Express</h1>
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/inc.anuncio2.php");?>
            
        	<h2>Desodorante para calzado y pies</h2>
			<h3>&bull; INDICACIONES:</h3>
			<p>Elimina  radicalmente el mal olor del calzado, incluido el de las zapatillas  deportivas, as&iacute; como el de las habitaciones y armarios donde suelen  estar presentes.</p>
			<h3>&bull; MODO DE EMPLEO:</h3>
			<p>Espolvorear  el interior del calzado con dos o tres ligeras presiones en el bote  antes de usar el calzado, o para mayor comodidad el dia anterior a su  utilizaci&oacute;n. Esta operaci&oacute;n la pueden realizar las madres con hijos  peque&ntilde;os o reticentes en su cuidado personal.</p>
            <h3>&bull; PRESENTACI&Oacute;N:</h3>
            <p>Bote aplicador de polvo.</p>
    	</div>	
    </div>    

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>