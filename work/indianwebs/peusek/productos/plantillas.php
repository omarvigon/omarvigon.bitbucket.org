<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Plantillas</title>
	<meta name="description" content="Productos Peusek Plantillas. Plantillas Desodorantes y Confortables" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Plantillas Peusek" src="http://www.peusek.es/img/producto_plantillas.jpg" alt="Plantillas Peusek" />
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Plantillas</h1>
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/inc.anuncio2.php");?>
            
       		<h2>Plantillas Desodorantes y Confortables</h2>
			<h3>&bull; DESODORANTE DE ACCI&Oacute;N TRIPLE:</h3>
			<p>Debido a su capa de fibras especiales que permite una concentraci&oacute;n del 18% de  Carb&oacute;n activado, con un poder de absorci&oacute;n del sudor y su mal olor muy  superior frente a la concentraci&oacute;n habitual del 6%.</p>
			<h3>&bull; CONFORTABLES:</h3>
			<p>Fabricadas  con un tejido de alta resistencia y agradable tacto por su parte  superior, y una base de l&aacute;tex esponjoso que act&uacute;a a modo de colch&oacute;n  amortiguador del impacto del pie contra el suelo, proporcionando  descanso y comodidad.</p>
    	</div>	
    </div>	

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>