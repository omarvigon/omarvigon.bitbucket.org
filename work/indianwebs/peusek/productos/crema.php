<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Crem</title>
	<meta name="description" content="Productos Peusek Crema pies. Crema suavizante e hidratante para los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Crema Peusek" src="http://www.peusek.es/img/producto_crema.jpg" alt="Crema Peusek" />	
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Crem</h1>
            
            <fieldset>
				<h4>S&oacute;lo el 20% de la poblaci&oacute;n hidrata sus pies</h4>
				<h5>A diferencia de las manos y otras partes del cuerpo, nos hidratamos poco los pies</h5>
				<p style="font-size:11px;margin:0 16px 4px 0;">La falta de hidrataci&oacute;n en los pies provoca asperezas, descamaci&oacute;n de la piel y la aparici&oacute;n de durezas. Tambi&eacute;n la falta de hidrataci&oacute;n agrava la dolencia de las grietas en los talones, muy dolorosas y antiest&eacute;ticas.</p>
    			<p style="font-size:11px;margin:0 16px 4px 0;">La aplicaci&oacute;n habitual de crema especial para los pies, hidrata y suaviza, eliminando durezas y talones agrietados: PEUSEK-crema.</p>
			</fieldset>

        	<h2>Crema suavizante e hidratante para los pies</h2>
			<h3>&bull; INDICACIONES:</h3>
			<p>Restablece la elasticidad y tersura de la piel reseca, descamada o  agrietada. Su poder humectante, ablanda las durezas y callosidades de  los pies. Igualmente eficaz para las manos y en especial para la piel  rugosa y &aacute;spera de codos y rodillas. Es de f&aacute;cil absorci&oacute;n y no mancha.</p>
			<h3>&bull; MODO DE EMPLEO:</h3>
			<p>Aplicar la crema preferentemente en peque&ntilde;as cantidades, extendi&eacute;ndola  a modo de masaje hasta su absorci&oacute;n, repitiendo discrecionalmente esta  operaci&oacute;n.</p>
            <h3>&bull; PRESENTACI&Oacute;N:</h3>
            <p>Crema en tubo flexible de 75 ml.</p>
    	</div>	
    </div>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>