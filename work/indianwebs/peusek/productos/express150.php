<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Express 150</title>
	<meta name="description" content="Productos Peusek Express 150 Tama&ntilde;o Familiar para los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Express 150 Peusek" src="http://www.peusek.es/img/producto_express150.jpg" alt="Express 150 Peusek" />	
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Express 150</h1>
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/inc.anuncio2.php");?>
            
       	 	<h2>PEUSEK Express 150 Tama&ntilde;o Familiar</h2>
			<h3>&bull; CARACTER&Iacute;STICAS:</h3>
			<p>&middot; Opcionalmente puede usarse por varios miembros de la misma familia<br />
			&middot; Supone un ahorro sustancial de precio, respecto al tama&ntilde;o mormal.<br />
			&middot; Por su contenido casi cuatro veces superior, se reduce la frecuencia de compra.</p>
    	</div>	
    </div> 

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>