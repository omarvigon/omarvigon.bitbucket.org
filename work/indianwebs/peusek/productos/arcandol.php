<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php");?>
	<title>Productos Peusek Arcandol</title>
	<meta name="description" content="Productos Peusek Arcandol. Relajante y Tonificante para los pies" />
	<meta name="keywords" content="peusek, productos pies, mal olor pies, sudor pies, crema pies, desodorante pies, refrescante pies, arcandol, pies secos" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>
	
    <div class="cuerpo">
    	<div class="cuerpo_izq">
    		<img title="Arcandol Peusek" src="http://www.peusek.es/img/producto_arcandol.jpg" alt="Arcandol Peusek" />	
		</div>    
    	<div class="cuerpo_der">		
        	<h1>Productos Peusek &raquo; Arcandol</h1>
            
            <fieldset>
				<h4>&iquest;Sab&iacute;as que la felicidad empieza por los pies?</h4>
				<h5>Porque unos pies fatigados son motivo de malhumor.</h5>
				<p style="font-size:11px;margin:0 16px 4px 0;">A lo largo de nuestra vida recorremos <b>m&aacute;s de 160.000 km</b>: equivalente a dar la vuelta al mundo 4 veces. Los pies nos permiten trabajar, hacer deporte, bailar o simplemente estar de pie.</p>
    			<p style="font-size:11px;margin:0 16px 4px 0;">Existen productos que relajan y tonifican nuestro pies fatigados: PEUSEL-arcandol y SEK-sport.</p>
			</fieldset>
            
        	<h2>Loci&oacute;n Relajante y Tonificante</h2>
			<h3>&bull; INDICACIONES:</h3>
			<p>Aplicado  antes y despu&eacute;s de cualquier actividad, proporciona al momento  sensaci&oacute;n de bienestar, frescor y descanso, manteni&eacute;ndolos en plena  forma.</p>
			<h3>&bull; MODO DE EMPLEO:</h3>
			<p>Pulverizar sobre los pies, en especial plantas y tobillos. Optativamente dar un masaje.</p>
			<h3>&bull; PRESENTACI&Oacute;N:</h3>
			<p>Bote vaporizador l&iacute;quido de 100 ml. Sin gas.</p>
    	</div>	
    </div>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php");?>