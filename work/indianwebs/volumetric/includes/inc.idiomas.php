<?php
define("BOT_1","Qui&eacute;nes somos");
define("BOT_2","Promociones");
define("BOT_2B","Promociones en curso");
define("BOT_2C","Promociones terminadas");
define("BOT_3","Alquiler");

define("BOT_9","NOTICIAS");
define("BOT_10","Aviso Legal");
define("BOT_11","Pol&iacute;tica de privacidad");
define("BOT_12","Contacto");

define("BOT_15A","Presentaci&oacute;n");
define("BOT_15B","Calidades");
define("BOT_15C","Avance Obra");
define("BOT_15D","Punto de venta");
define("BOT_16","Fotograf&iacute;as");
define("BOT_16B","Video");
define("BOT_16C","Situaci&oacute;n");
define("BOT_17A","Interiores");
define("BOT_17B","Exteriores");
define("BOT_17C","Piso Piloto");
?>