<?php include("0cabecera.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Panel de control</title>
    <link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
    <script type="text/javascript" src="includes/_funciones.js"></script>
    <script type="text/javascript" src="editor_tiny/tiny_mce.js"></script>
	<script type="text/javascript">
	tinyMCE.init({
		mode : "textareas",theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,forecolor,backcolor,code,pasteword",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,blockquote,link,unlink,image,|,hr,removeformat,visualaid,sub,sup,charmap,print,tablecontrols",	
		/*,
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		*/
		theme_advanced_toolbar_location : "top",theme_advanced_toolbar_align : "left",theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,content_css : "css/word.css",
		template_external_list_url : "lists/template_list.js",external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",media_external_list_url : "lists/media_list.js",
		template_replace_values : {username : "Some User",staffid : "991234" }});
	</script>
</head>

<body>
<div id="left">
<a href="index.php"><img src="http://www.volumetric.eu/logo.gif" alt="Volumetric" /></a>

<?php if(isset($_SESSION["autentico"])) { ?>
	
    <div>
	<a href="editar.promociones.php?id=true">&raquo; Nueva promocion</a>
	<a href="editar.promociones.php">&raquo; Listar promociones</a>
    <hr color="#fff" size="1" />
    <a href="#">&raquo; Nueva noticia</a>
	<a href="#">&raquo; Listar noticias</a>
    <hr color="#fff" size="1" />
	<a href="<?php echo $_SERVER['PHP_SELF']."?logout=true";?>">&raquo; Salir</a>
 	</div>
    
<?php } else { ?>
    <form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
	<label>Login</label>
	<input type="text" name="login" size="22" maxlength="22" />
	<label>Password</label>
	<input type="password" name="password" size="22" maxlength="22" />
	<input type="submit" value=" ENVIAR " />
	</form>
<?php } ?>
</div>
<div id="centro">