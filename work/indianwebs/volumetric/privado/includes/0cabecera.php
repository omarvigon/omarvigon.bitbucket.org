<?php
session_start();
include("clases.php");
include("0config.php");

$temp=explode(".",$_SERVER['PHP_SELF']);
define("TABLA",$temp[1]);

function redireccionar($pagina)
{
	echo "<script type='text/javascript'>window.open('".$pagina."','_self')</script>";
	/*header("Location:".$pagina);*/
}
function editar_encabezado()
{
	$modo=($_GET["id"]>0)? "modificar=".$_GET["id"]:"insertar=true";
	$texto=explode("=",$modo);
	echo '<h6>'.$texto[0].' registro</h6>
	<form action="'.$_SERVER['PHP_SELF'].'?'.$modo.'" method="post" name="f_editar">';	
}
function redimensionar($nombre,$carpeta)
{
	$ruta=$_SERVER["DOCUMENT_ROOT"]."/img/promociones/".$_GET["id"]."-".$nombre.".jpg";
	
	if(copy($_FILES[$nombre]["tmp_name"],$ruta))
	{
		list($width, $height) = getimagesize($ruta);
		if($width >= $height)
		{
			$newwidth = 350;
			$newheight = floor((350 * $height) / $width);
		}
		else
		{
			$newheight = 350;
			$newwidth = floor((350 * $width) / $height);
		}
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($ruta);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagejpeg($thumb,$ruta);
		imagedestroy($thumb);
		return true;
	}
	else
		return false;
}

if(!isset($_SESSION["autentico"]) && !strstr($_SERVER['PHP_SELF'],"index.php"))
	redireccionar("index.php");	
if(isset($_POST["login"]) && isset($_POST["password"]))
	usuario::acceder1();	
if(isset($_GET["logout"]))
	usuario::salir();
if(isset($_GET["insertar"]))
	tabla::insertar();
if(isset($_GET["modificar"]))
	tabla::modificar($_GET["modificar"]);
if(isset($_GET["eliminar"]))
	tabla::eliminar($_GET["eliminar"]);
if(isset($_GET["id"]))
	$fila=tabla::cargar($_GET["id"]);
	
/*al eliminar una promocion tambien todas las imagenes*/	
?>