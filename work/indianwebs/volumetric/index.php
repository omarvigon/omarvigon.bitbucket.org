<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php"); ?>
    <title>Volumetric</title>
	<meta name="description" content="Volumetric" />
	<meta name="keywords" content="Volumetric" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php"); ?>

    <h4>&raquo; <a href="http://www.volumetric.eu/">Volumetric</a></h4>
    <div class="noticias">
    	<h5><?php echo BOT_9;?></h5>
        <p><b>VOLUMETRIC OBTÉ EL CERTIFICAT DE QUALITAT ISO9001</b></p>
        <p>En data 11 d´Abril de 2005, Volumetric ha obtingut el Certificat de Qualitat ISO 9001, lliurat per l´empresa certificadora APPLUS+, conforme als requisits de la norma segons ENAC CERFICICACIÓN ISO 9001:2000.</p>
    </div>
    
    <p class="phome"><img title="Volumetric Europe" src="http://www.volumetric.eu/img/mapa.gif" usemap="#Map" alt="Volumetric Europe" /></p>
    <map name="Map" id="Map">
    <area shape="poly" coords="59,172,59,266,104,288,205,218,120,160" href="http://www.volumetric.eu/promociones.php" title="Spain" alt="Spain" />
    <area shape="poly" coords="294,81,393,75,395,153,299,151" href="http://www.volumetric.pl/" title="Polonia" alt="Polonia" />
    <area shape="poly" coords="316,160,385,158,357,203,311,197" href="http://www.volumetric.hu/" title="Hungria" alt="Hungria" />
    <area shape="poly" coords="389,157,361,203,408,230,454,215,456,165" href="http://www.volumetric.ru/" title="Rumania" alt="Rumania" />
    </map>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php"); ?>