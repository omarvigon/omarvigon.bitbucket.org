<?php
define("ELIGE_JUGUETE","toys");
define("ELIGE_ARTICULO","items");
define("ELIGE_CAT","Choose a category");

define("ENLACES","Links");
define("SITEMAP","Sitemap");
define("SALUDO","Hello");
define("SALUDO2","Exchange toys");
define("SALUDO3","Buy and Sell");
define("PGANTERIOR","Previous");
define("PGSIGUIENTE","Next");
define("LO_ULTIMO","THE LATEST ADDITIONS!");
define("LO_ULTIMO2","The latest additions");
define("QUIERO_0","Give away");
define("QUIERO","I WANT TO EXCHANGE!!");
define("QUIERO2","I want to exchange...");
define("SMS_QUIEN","From whom / for whom");
define("SMS_MENSAJE","subject");
define("SMS_FECHA","date");
define("SMS_ACCION","what do you want to do with the message?");
define("SMS_RESPUESTA","Your answer");
define("OLVIDADO_1","I've forgotten my Troc Name");
define("OLVIDADO_2","I've forgotten my password");
define("OLVIDADO_3","Please provide your email address, so we can send you your access codes...");
define("OLVIDADO_3_PADRES","Please provide your email address, so we can send you your access codes...");

define("ALERT_FAVDEL","This toy has been deleted from your selections");
define("ALERT_FAVDEL2","This item has been deleted from your selections");
define("ALERT_FAVADD","This toy has been added to your selections");
define("ALERT_FAVADD2","This item has been added to your selections");
define("ALERT_REGISTRO","Your password has been sent to your email inbox.<br>You will need your password* and your Troc Name to enter your KIDDYTROC account: in order to start exchanging.<br><br>CONGRATULATIONS!!<br>You have successfully registered. In order to start exchanging, you must upload pictures of the toys onto the website in the &ldquo;add a toy&rdquo; section and follow each step. You&acute;re nearly there!<br><br>*You can change the password whenever you want. You should write a word that is easy to remember and that has a maximum of 6 letters.<br><br>See you soon in KIDDYTROC<br>KING BUDDI");
define("ALERT_JUGMOD","The toy has been correctly modified");
define("ALERT_JUGMOD2","The item has been correctly modified");
define("ALERT_JUGDEL","Your toy has been removed");
define("ALERT_JUGDEL2","Your item has been removed");
define("ALERT_JUGOK","CONGRATULATIONS!<br />UPLOADING has been successful and your  toy is ready for exhange!<br />Remember that  you can add as many&nbsp; toys as you want.");
define("ALERT_JUGOK2","CONGRATULATIONS!<br />Your item has been uploaded successfully.<br /><br />Remember that you can add as many items as you want.");
define("ALERT_SMSOK","The message has been sent");
define("ALERT_SMSDEL","The message has been deleted ");
define("ALERT_DENEGAR","Operacion realizada");
define("ALERT_ERROR","OOPS!!<br />You haven't completed all the sections of the form.<br /><br />TRY AGAIN!<br />The missing sections are marked in red.");
define("ALERT_EMAIL_1_N","<p>CONGRATULATIONS!!</p><p>Welcome!</p><p>You are successfully registered in KIDDYTROC &ndash; KIDS' AREA</p><p>This is your pass word:");
define("ALERT_EMAIL_2_N"," (  you can change your password whenever you want account/my details )</p><p>See you soon in Kiddytroc.</p><p>King Buddi.</p>");
define("ALERT_EMAIL_1_P","<p>CONGRATULATIONS!!</p><p>Welcome!</p><p>You are successfully registered in KIDDYTROC &ndash; PARENTS' AREA</p><p>This is your pass word:");
define("ALERT_EMAIL_2_P"," (  you can change your password whenever you want account/my details )</p><p>See you soon in Kiddytroc.</p><p>King Buddi.</p>");
define("ALERT_ERROR_2","OOPS!! The verification code is wrong");
define("ALERT_ERROR_3","OOPS!! That Troc Name or E-mail does alredy belongs to somebody else");
define("ALERT_ERROR_4","WARNING: you entered a wrong e-mail.");
define("ALERT_DATOS","Your details have been correctly modified.");

define("ZONA_PADRES","PARENTS' AREA");
define("ZONA_NINOS","KIDS' AREA");
define("ZONA_LEGAL","User agreement");
define("ZONA_LEGAL2","Accept agreement and privacy policy");
define("ZONA_CONTACTO","Contact");
define("ZONA_CONTACTO_OK","The comment has been sent. Thank you.");
define("ZONA_REGISTRO","REGISTER");
define("ZONA_REGISTRO2","REGISTER");
define("ZONA_MENSAJES","My Messages");
define("ZONA_JUGUETES","My Toys");
define("ZONA_ANADIR","Add a Toy");
define("ZONA_FAVORITOS","My Selections");
define("ZONA_DATOS","My Details");
define("ZONA_DESCONECTAR","Log Out");
define("ZONA_ERROR","Volver al inicio");
define("ZONA_BUSCAR","Search Results");
define("ZONA_ARTICULOS","My Items");
define("ZONA_ANADIR2","Add an Item");

define("DATO_NOMBRE","Name");
define("DATO_NOMBRE2","Name");
define("DATO_SEXO_1","Boy");
define("DATO_SEXO_0","Girl");
define("DATO_NOM_NINO","Kid's Name");
define("DATO_NOM_PADRE","Mother's or Father's Name");
define("DATO_SEXO","Girl/boy?");
define("DATO_SEXO_PADRES","Man or woman?");
define("DATO_SEXO_PADRES1","Man");
define("DATO_SEXO_PADRES0","Woman");
define("DATO_APELLIDOS","Surname");
define("DATO_NACIMIENTO","Date of birth");
define("DATO_DIRECCION","Address");
define("DATO_CP","Postal code:");
define("DATO_POBLACION","Town");
define("DATO_PROVINCIA","Province");
define("DATO_PAIS","Country");
define("DATO_TFN","Telephone number");
define("DATO_EMAIL","E-mail");
define("DATO_COMENTARIO","Comments");
define("DATO_CONTRA","Password");
define("DATO_TROCDESC","What made you choose your Troc Name?");
define("DATO_JUGUETECOMO","Description");
define("DATO_JUGUETEMOTIVO","You would exchange it for...");
define("DATO_IMAGEN","Choose the image you prefer");
define("DATO_CATEGORIA","Category");
define("DATO_EDADES","For what age");
define("DATO_ANTIGUO","How long have you had this toy?");
define("DATO_MARCA","Brand");
define("DATO_CONSERVA","State of conservation:");
define("DATO_MOTIVO1","Look at the toys");
define("DATO_MOTIVO2"," is looking for. As you now know what your friend likes, you will have more chance to make a  succesfull exchange");
define("DATO_VISITAS","Visits");
define("DATO_VISIBLE","Visible on the website");
define("DATO_ANO","year");
define("DATO_ANOS","years");
define("DATO_MAXIMO","maximum 255 characters");
define("DATO_INCORRECTO","The data are not correct. Please try again.");
define("DATO_OBLIGATORIO","Compulsory fields");
define("DATO_VERIFICA","Enter verification code");
define("DATO_VERIFICA_PADRES","Enter verification code");
define("DATO_NINO","Personal details of the kid");
define("DATO_ANADIR","Adding a toy is very easy. <br>All you have to do is to  complete this page correctly.");
define("DATO_JUGUETE","Details of the toy:");
define("DATO_JPG","The photo must be uploaded in JPEG format");
define("DATO_RECORDARME","Remember my details on this computer");
define("DATO_PRECIO","Price");
define("DATO_ELIGE_X","EXCHANGE");
define("DATO_ELIGE_0","You must select 1 of the 3 options");
define("DATO_ELIGE_1","I want to sell it");
define("DATO_ELIGE_2","I want to exchange it");
define("DATO_ELIGE_3","I want to give away");

define("FECHA_DIA","day");
define("FECHA_MES","month");
define("FECHA_ANO","year");
define("MES_1","January");
define("MES_2","February");
define("MES_3","Marzch");
define("MES_4","April");
define("MES_5","May");
define("MES_6","June");
define("MES_7","July");
define("MES_8","Agugust");
define("MES_9","September");
define("MES_10","Octuober");
define("MES_11","November");
define("MES_12","December");

define("EDAD2","0-2 years old");
define("EDAD5","2-5 years old");
define("EDAD8","5-8 years old");
define("EDAD12","8-12 years old");
define("EDAD14","more than 12 years");
define("DESDE_0","less than ");
define("DESDE_1","for ");
define("DESDE_2","more than ");
define("CONSERVA1","Deteriorated");
define("CONSERVA2","Normal");
define("CONSERVA3","Optimal");
define("CONSERVA4","New. Unused");

define("TXT_VERMIS_J","See my toys");
define("TXT_VERSUS_J","See his/her toys");
define("TXT_VERMIS_A","See my items");
define("TXT_VERSUS_A","See his/her items");
define("TXT_RESPONDER","KING BUDDI has put us in touch. He&acute;s really great!<br>I like one of your toys and I would like to exchange it  for one of mine. These are the toys I can offer you:<br>I hope to hear from you soon! ");
define("TXT_BUSQUEDA_0","No match found");
define("TXT_FAVORITOS","You like all these toys but... Which one are you going to keep?");
define("TXT_SELECCIONAR","Select...");
define("TXT_YAESTAS","Already registered user. If you want to create a new account you must first disconnect");
define("TXT_REL_1","If a friend has recommended KIDDYTROC, write their TROC NAME here");
define("TXT_REL_2","IMPORTANT:<br /><br />If someone in your family is already registered, state their TROC NAME so that KING BUDDI knows they belong to your family");
define("TXT_JUGUETES_1","How are you doing?");
define("TXT_JUGUETES_2","These are the toys you have for  exchange.<br><em>REMEMBER</em> that at any time you  want, you can modify the description of your toys, remove them from the list or  make them invisible so that no-one but you can see them.");
define("TXT_ACCESO1_N","<p>Hello!</p><p>Your access codes for KIDDYTROC -  KIDS' AREA are:</p>");
define("TXT_ACCESO2_N","<p> (  you can change your password whenever you want account/my details )</p><p>See you soon in Kiddytroc.</p><p>King Buddi.</p>");
define("TXT_ACCESO1_P","<p>Hello!</p><p>Your access codes for KIDDYTROC -  PARENTS' AREA are:</p>");
define("TXT_ACCESO2_P","<p>(  you can change your password whenever you want account/my details )</p><p>See you soon in Kiddytroc.</p><p>King Buddi.</p>");
define("TXT_OLVIDO1","Your  access codes have been sent to your email address: ");
define("TXT_OLVIDO2","You can also request them by sending an email to");
define("TXT_OTROS1","Do you want to see your toys? Click here");
define("TXT_OTROS2","Do you want to see the other toys from this kid? Click here");
define("TXT_VENDER","I want to sell...");
define("TXT_COMPRAR","I want to buy...");

define("TXT_LEGALMAIL","<p style='font-size:9px;'>In compliance with the Organic  Law 15/1999 of 13 December on Protection of Personal Data, we wish to  inform you that your personal contact details have been incorporated  in computerized files belonging to Karine Boveroux, who will be the  only recipient of said data, the purpose of which is to deal with  clients and handle actions related to commercial communications. At  the same time, we would like to remind you that you have the right to  access, rectify, amend and/or delete such data, as provided for by  law, via a written request addressed to Karine Boveroux at: Kiddytroc  Ref. Protecci&oacute;n de datos. Apartado de correos n&deg;33- 08192 Sant  Quirze del Vall&egrave;s &ndash; Spain.</p>");
define("TXT_AVISOLEGAL","<p><b>USER AGREEMENT AND PRIVACY POLICY</b></p>
<p>The <a href='http://www.kiddytroc.com/'>www.kiddytroc.com</a> domain  belongs to Karine BOVEROUX Nie X-9298978-D.<br>
  Address: Karine Boveroux (Kiddytroc)<br>P.O.Box n&deg;33- 08192 Sant Quirze del Vall&egrave;s &ndash; Spain.<br>
  Karine Boveroux complies with all legal requirements.<br>In  compliance with the Organic Law 15/1999 of 13 December on Protection of  Personal Data, we wish to inform you that your personal contact details have  been incorporated in computerized files belonging to Karine Boveroux, who will  be the only recipient of said data, the purpose of which is to deal with  clients and handle actions related to commercial communications. At the same  time, we would like to remind you that you have the right to access, rectify,  amend and/or delete such data, as provided for by law, via a written request  addressed to Karine Boveroux at: Kiddytroc Ref. Protecci&oacute;n de datos. Apartado de correos n&deg;33- 08192 Sant Quirze del Vall&egrave;s &ndash; Spain</p>
<p><b>Intellectual  and Industrial Property</b></p>
<p>In  compliance with the international agreements on Intellectual and Industrial  Property, Karine Boveroux is the sole and exclusive owner of the Intellectual  and Industrial Property rights of the <a href='http://kiddytroc.com/'>KIDDYTROC.COM</a> website  and its contents. Total or partial reproduction of the website is entirely prohibited, under  penalty of instigation of legal proceedings.</p>
<p><b>Contractual  clauses</b></p>
<p>Any person  registered in the <a href='http://kiddytroc.com/'>KIDDYTROC.COM</a> database  declares that they have read and accepted the following conditions:<br>
  In  agreement with the legal resolutions covering the protection of children and  those under age: any under-age or unemancipated user must ask permission from  their parent(s) or legal guardian(s) to register, navigate and effect  exchanges.<br>The  commercial part of the website dedicated to buying and selling is reserved  exclusively for adults (Parents' area).<br>
  KIDDYTROC  reserves the right to cancel the account of any person who does not meet the  ethical criteria of KIDDYTROC: Advertisements and/or messages cannot under any  circumstances contain vulgar, sexual or pornographic, racist or discriminatory  words. Should this not be complied with, KIDDYTROC has the right to alert the  relevant legal institutions.<br>
  We  wish to remind you that the advertisements (exchanging, buying and selling) are  reserved for private individuals.<br>
  We  ask you to promote, use and exchange emails politely. At any moment, you can  notify and inform us (<a href='mailto:info@kiddytroc.com'>info@kiddytroc.com</a>) of any  bad conduct on the part of a user or of any failure to comply with the criteria  regarding KIDDYTROC.</p>
<p><b>Disclosure  of your personal details</b></p>
<p>KIDDYTROC  agrees never to sell, rent or reveal your personal details given on  registering, except in the following two indicated cases: in an agreement  signed by yourself personally, or a request from legal authorities.<br>
  The  parties agree that the conditions are subject to Spanish legislation. In the  event of any dispute regarding the interpretation or execution of these  conditions, and in the event of failure in any attempt to reach an amicable  solution, authority will be exclusively attributed to the Spanish Courts.</p>
  <p><b>Google  Advertising Cookie and Privacy Policies</b></p>
<p>Google,  as a third party vendor, uses <a href='http://www.doubleclick.com/privacy/faq.aspx' target='_blank'>Dart cookie </a> to serve ads on on  Kiddytroc. Google's use of the DART cookie enables it to serve  ads to our users based on their visit to our site and other sites on  the Internet. Users may opt out of the use of the DART cookie by  visiting the <a href='http://www.google.com/privacy_ads.html' target='_blank'>Google ad  and content networrk privacy policy</a>.</p>");

define("AYUDA_NAME","<p>Invent a TROC NAME for yourself: from now on, your  KIDDYTROC friends will refer to you by this name.<br>Warning: don&rsquo;t forget your TROC  NAME: write it in your note book.<br>You will need your TROC&nbsp; NAME to enter your personal pages and to  exchange toys<br></p>");
define("AYUDA_VERIFICA","<p>You must enter the verification code you were given.<br>Enter this code in the appropriate box.</p>");
define("AYUDA_FAVORITO","<p>How&acute;s it going? Any problems?<br>If you click on the &ldquo;Add to my  selections&rdquo;, this toy will immediately be saved in the &ldquo;My selections&rdquo; section  of your KIDDYTROC<br><br>It is very handy because, in  this way, when you want to show the toys you like to your parents you won&acute;t  have to look for them one by one. Just click on the &ldquo;My selections&rdquo; section and  you will find them all there.<br>Great, isn&acute;t it?</p>");
define("AYUDA_MIS","<p>Make sure you have thought it through carefully before choosing.<br>If the other child lives near you, it may be easier for you to meet up  and exchange toys. If a toy is very very old, do you still want to make the  exchange? Are you certain it is for your age group? Is it a fair exchange?<br>It is very important  that you ask your parents&acute; advice before exchanging with anyone.<br>They will help you to make the best choice!</p>");
define("AYUDA_TRUEQUE","<p>Warning!!<br>Try to make sure that the toy  you are offering to your new friend has&nbsp;  approximately the same&nbsp; value as  theirs, otherwise they will not exchange it for yours. <br>If yours is worth less, you can  offer them several of your toys in the COMMENTS section.<br>Good luck and  happy swapping!</p>");
define("AYUDA_EDAD","<p>If you are not sure what age a child should be to play with you toy... Well, don&acute;t waste time - go and ask your parents</p>");
define("AYUDA_ANTIGUO","<p>How long have you had this toy?<br>Come on, try to remember, I&rsquo;m sure you can! </p>");
define("AYUDA_CONSERVA","<p>Whatever  happens, don&acute;t lie !!<br>If you don&acute;t tell the truth, the  other children won&acute;t want to exchange toys with you.<br>So be honest, it will be better for you in the end!</p>");
define("AYUDA_SUBIR","<p>Look for the photos you have taken of your toy on your computer and choose the best one.</p><p>If you are not sure how to do it, ask for help at home. You will see how easy it is!</p>");
define("AYUDA_INTERCAMBIO","<p>Do you like dolls?<br>Are you keen on remote control  cars?<br>Are  you mad about footballs?<br>Then make a note of it.<br>Write down all the toys you  would like to have, that will help all your KIDDYTROC friends to know what toys  to exchange with you.<br>Don&rsquo;t wait,  try it out right now! </p>");
define("AYUDA_VISIBLE","<p>Do you want other children to  see this toy? <br>Or would you prefer to keep it  invisible for a few days so you can decide whether you really want to exchange  it?<br>If so, it will appear in your MY  TOYS section but no-one else but you will see it until you change this option.<br>If you decide to make it  visible, tick the box, and if you want it to be invisible, leave the box blank.  It&acute;s up to you!</p>");
define("AYUDA_CONTRA","<p>Do you want to change your  password?<br>Think carefully and write a  secret word with a maximum of 6 letters that is  easy to remember, so you won&acute;t forget it.<br>Want a tip? Write a question in  a notebook and use the answer as your password. For example: what is the name of your favourite toy? In this way, nobody but you will know your secret and  you won&acute;t forget it.</p>");

define("PADRE_NAME","<p>What name would you like to appear in KIDDYTROC? Write a pseudonym.</p>");
define("PADRE_REGISTRO","Your password has been sent to your email inbox.<br>You will need your password* and your Troc Name to enter your KIDDYTROC account: so that you can buy and sell.<br /><br />CONGRATULATIONS!!<br><br>You have successfully registered, you can now buy and sell.<br>If you want to sell items, you must first upload a picture in the section &ldquo;add an item&rdquo; and follow the steps. It&rsquo;s very easy!<br><br>*You can change the password whenever you want. You should write a word that is easy to remember and that has a maximum of 6 letters.<br><br>See you soon in KIDDYTROC<br>KING BUDDI");
define("PADRES_CONTRASENA","<p>You can change the password whenever you want.<br>You should  write a word that is easy to remember and that has a maximum of 6 letters.</p>");
define("PADRES_CONTACTAR","<p>CONTACT THE SELLER</p>");
define("PADRES_ANTIGUO","How long have you had this item ?");
define("PADRES_ARTICULOS","<b>How are you doing?</b><br /><br />These are the items  you want to sell.<br />REMEMBER that at any  time you want, you can modify the description of your items<br />remove them from  the list or make them invisible so that no-one but you can see them.");
define("PADRES_FAVORITO","This is the list of articles you like.  Before you decide whether to buy or to exchange them, you can click  on each one and see their characteristics once again.");
define("PADRES_ADARTICULO","Adding an item is very easy.<br /><br />You only have to fill in this page correctly.");
define("PADRES_SUBIR","<p>You must upload a picture so that KIDDYTROC&acute;s users can have an idea of what it looks like.</p>");
define("PADRES_VISIBLE","<p>Do you want this article to be seen by KIDDYTROC&acute;s users? <br>Or would you  rather keep it invisible for a few days?<br>If you wish it to be visible, tick the box.<br>If you want  it to be invisble, leave the box blank:<br> it will  appear in your &ldquo;my articles&rdquo; section but KIDDYTROC&acute;s users will not see it  until you tick the box.</p>");
define("PADRES_ERROR","SORRY !!<br />You haven't completed all the sections of the form.<br /><br />TRY AGAIN!<br />The missing sections are marked in red.");
define("PADRES_REL_1","If a friend has recommended KIDDYTROC ,write their TROC NAME here");
define("PADRES_REL_2","IMPORTANT:<br /><br />If someone in your house is already registered, state their TROC NAME so that KING BUDDI knows they belong to your family");
define("PADRES_AYUDA_VER","<p>You must enter the verification code you were given.<br>Enter this code in the appropriate box.</p>");

/*metas de la zona de ni�os, index y categorias principales*/
define("META_T_INDEX","Exchange Your Toys And Games For Free");
define("META_D_INDEX","Exchange Toys and Games.Free register and communication");
define("META_K_INDEX","exchange toys, exchange games");

define("META_T_N_CONTACTA","KiddyTroc.es - Contacto");
define("META_T_N_REGISTRO","KiddyTroc.es - Inscripcion para Ni&ntilde;os");
define("META_T_N_LEGAL","KiddyTroc.es - Aviso Legal ");
define("META_T_N_USERS","KiddyTroc.es - Usuarios");

define("META_T_N_CAT_1","Intercambio de Juguetes Aire Libre");
define("META_T_N_CAT_2","Intercambio de Juguetes Belleza / Joyas");
define("META_T_N_CAT_3","Intercambio de Juguetes Cocinitas");
define("META_T_N_CAT_4","Intercambio de Juguetes Construcciones");
define("META_T_N_CAT_5","Intercambio de Disfraces para ni&ntilde;os");
define("META_T_N_CAT_6","Intercambio de Juegos Educativos y juegos de Rol");
define("META_T_N_CAT_7","Intercambio de Figuras de acci&oacute;n");
define("META_T_N_CAT_8","Intercambio de Juguetes musicales");
define("META_T_N_CAT_9","Intercambio de Juegos de consola");
define("META_T_N_CAT_10","Intercambio de Juegos de mesa");
define("META_T_N_CAT_11","Intercambio de Juegos de ordenador");
define("META_T_N_CAT_12","Intercambio de Libros y comics");
define("META_T_N_CAT_13","Intercambio de Juguetes de Manualidades");
define("META_T_N_CAT_14","Intercambio de Mu&ntilde;ecas y Peluches");
define("META_T_N_CAT_15","Intercambio de M&uacute;sica y Pel&iacute;culas");
define("META_T_N_CAT_16","Intercambio de Juguetes de Radio control");
define("META_T_N_CAT_17","Intercambio de Veh&iacute;culos de Juguete");
define("META_T_N_CAT_18","Intercambio de Otros Juguetes y Accesorios para bebes y ni&ntilde;os");

define("META_D_N_CAT_1","Intercambio de juegos y juguetes al aire libre: bicicletas, patinetas, patines, cometas, pelotas...");
define("META_D_N_CAT_2","Intercambio de juegos y juguetes de belleza, juegos de maquillaje, joyas de juguete...");
define("META_D_N_CAT_3","Intercambio de cocinitas de juguete: fregaderos, hornos, neveras, alimentos y utensilios de juguet...");
define("META_D_N_CAT_4","Intercambio de juegos y juguetes de construccion y armado.");
define("META_D_N_CAT_5","Intercambio de disfraces juveniles y disfraces para ni&ntilde;os.");
define("META_D_N_CAT_6","Intercambio de juegos y juguetes educativos, juegos cient�ficos, juegos de rol y estrategia.�");
define("META_D_N_CAT_7","Intercambio de figuras de accion, mu&ntilde;ecos articulados, mu&ntilde;ecos transformables, figuras de peliculas.");
define("META_D_N_CAT_8","Intercambio de juguetes musicales: tambores, flautas, pianos, guitarras, xilofonos...");
define("META_D_N_CAT_9","Intercambio de juegos de consola: Wii, PlayStation 3, PS3, XBox, Nintendo DS, PSP, Gamecube y Dreamcast.");
define("META_D_N_CAT_10","Intercambio de juegos de mesa, juegos educativos, de habilidad o cientificos.");
define("META_D_N_CAT_11","Intercambio de videojuegos, juegos para PC y Mac.");
define("META_D_N_CAT_12","Intercambio de libros juveniles y comics.");
define("META_D_N_CAT_13","Intercambio de juegos de arte, manualidades y puzzles.");
define("META_D_N_CAT_14","Intercambio de mu&ntilde;ecas y peluches, juegos de vestir mu&ntilde;ecas, dibujos de mu&ntilde;ecas, mu&ntilde;ecas antiguas.");
define("META_D_N_CAT_15","Intercambio de musica y peliculas juveniles, infantiles, CD &amp; DVD.");
define("META_D_N_CAT_16","Intercambio de juguetes de radio control: autos, aviones, barcos, helicopteros, coches, trenes...");
define("META_D_N_CAT_17","Intercambio de vehiculos juguete: bicicletas, triciclos, vehiculos electricos juveniles, vehiculos sin motor");
define("META_D_N_CAT_18","Intercambio de otros juegos y juguetes y Accesorios de bebes y ni&ntilde;os");

define("META_K_N_CAT_1","intercambio bicicletas, intercambio patinetas, intercambio patines, intercambio cometas, intercambio pelotas");
define("META_K_N_CAT_2","intercambio juegos belleza, intercambio juegos maquillaje, intercambio joyas juguete");
define("META_K_N_CAT_3","intercambio cocinitas juguete, intercambio hornos juguete, intercambio neveras juguete, intercambio alimentos y utensilios juguete");
define("META_K_N_CAT_4","intercambio juegos construccion, intercambio juegos armado");
define("META_K_N_CAT_5","intercambio disfraces juveniles, intercambio disfraces ni&ntilde;os");
define("META_K_N_CAT_6","intercambio juegos educativos, intercambio juegos cientificos, intercambio juegos rol");
define("META_K_N_CAT_7","intercambio figuras accion, intercambio mu&ntilde;ecos articulados, intercambio mu&ntilde;ecos transformables, intercambio figuras peliculas");
define("META_K_N_CAT_8","intercambio tambores juguete, intercambio flautas juguete, intercambio pianos juguete, intercambio guitarras juguete, intercambio xilofonos juguete");
define("META_K_N_CAT_9","intercambio juegos consola, intercambio playstation 3, intercambio xbox, intercambio nintendo ds,intercambio gamecube, intercambio dreamcast, intercambio wii");
define("META_K_N_CAT_10","intercambio juegos mesa, intercambio juegos educativos, intercambio juegos habilidad, intercambio juegos cientificos");
define("META_K_N_CAT_11","intercambio viedojuegos, intercambio juegos pc, intercambio juegos mac");
define("META_K_N_CAT_12","intercambio libros juveniles, intercambio comics");
define("META_K_N_CAT_13","intercambio juegos arte, intercambio manualidades, intercambio puzzles");
define("META_K_N_CAT_14","intercambio mu&ntilde;ecas, intercambio peluches, intercambio juegos vestir mu&ntilde;ecas, intercambio dibujos mu&ntilde;ecas, intercambio mu&ntilde;ecas antiguas");
define("META_K_N_CAT_15","intercambio musica juvenil, intercambio peliculas juveniles, intercambio CD, intercambio DVD");
define("META_K_N_CAT_16","intercambio juguetes radio control, intercambio autos, intercambio aviones, intercambio barcos, intercambio helicopteros, intercambio coches, intercambio trenes");
define("META_K_N_CAT_17","intercambio vehiculos juguete, intercambio bicicletas, intercambio triciclos, intercambio vehiculos electricos juveniles , intercambio vehiculos sin motor");
define("META_K_N_CAT_18","intercambio otros juegos, intercambio otros juguetes y accesorios");

/*metas de la zona de padres, index y categorias principales*/
define("META_T_PADRES","Buy and Sell, Exchange Toys and Kids' Items For Free");
define("META_D_PADRES","Buy and Sell, Exchange Toys and Kids' Items For Free");
define("META_K_PADRES","Buy and Sell, Exchange Toys and Kids' Items For Free");

define("META_T_REGISTRO","KiddyTroc.es - Inscripcion para Padres");

define("META_T_CAT_1","Intercambio de Juguetes Aire Libre");
define("META_T_CAT_2","Intercambio de Juguetes Belleza / Joyas");
define("META_T_CAT_3","Intercambio de Juguetes Cocinitas");
define("META_T_CAT_4","Intercambio de Juguetes Construcciones");
define("META_T_CAT_5","Intercambio de Disfraces para ni&ntilde;os");
define("META_T_CAT_6","Intercambio de Juegos Educativos y juegos de Rol");
define("META_T_CAT_7","Intercambio de Figuras de acci&oacute;n");
define("META_T_CAT_8","Intercambio de Juguetes musicales");
define("META_T_CAT_9","Intercambio de Juegos de consola");
define("META_T_CAT_10","Intercambio de Juegos de mesa");
define("META_T_CAT_11","Intercambio de Juegos de ordenador");
define("META_T_CAT_12","Intercambio de Libros y comics");
define("META_T_CAT_13","Intercambio de Juguetes de Manualidades");
define("META_T_CAT_14","Intercambio de Mu&ntilde;ecas y Peluches");
define("META_T_CAT_15","Intercambio de M&uacute;sica y Pel&iacute;culas");
define("META_T_CAT_16","Intercambio de Juguetes de Radio control");
define("META_T_CAT_17","Intercambio de Veh&iacute;culos de Juguete");
define("META_T_CAT_18","Intercambio de Otros articulos y Accesorios para bebes y ni&ntilde;os");
define("META_T_CAT_19","Accesorios de hogar para bebes y ni&ntilde;os, infantiles y juveniles");
define("META_T_CAT_20","Ropa para bebes y ni&ntilde;os/as, infantil y juvenil");
define("META_T_CAT_21","Ropa premam&aacute; y ropa para embazarazas");
define("META_T_CAT_22","Accesorios de Transporte para bebes y ni&ntilde;os Accesorios de Viaje para bebes y ni&ntilde;os");

define("META_D_CAT_1","Intercambio de juegos y juguetes al aire libre: bicicletas, patinetas, patines, cometas, pelotas...");
define("META_D_CAT_2","Intercambio de juegos y juguetes de belleza, juegos de maquillaje, joyas de juguete...");
define("META_D_CAT_3","Intercambio de cocinitas de juguete: fregaderos, hornos, neveras, alimentos y utensilios de juguet...");
define("META_D_CAT_4","Intercambio de juegos y juguetes de construccion y armado.");
define("META_D_CAT_5","Intercambio de disfraces juveniles y disfraces para ni&ntilde;os.");
define("META_D_CAT_6","Intercambio de juegos y juguetes educativos, juegos cient�ficos, juegos de rol y estrategia.�");
define("META_D_CAT_7","Intercambio de figuras de accion, mu&ntilde;ecos articulados, mu&ntilde;ecos transformables, figuras de peliculas.");
define("META_D_CAT_8","Intercambio de juguetes musicales: tambores, flautas, pianos, guitarras, xilofonos...");
define("META_D_CAT_9","Intercambio de juegos de consola: Wii, PlayStation 3, PS3, XBox, Nintendo DS, PSP, Gamecube y Dreamcast.");
define("META_D_CAT_10","Intercambio de juegos de mesa, juegos educativos, de habilidad o cientificos.");
define("META_D_CAT_11","Intercambio de videojuegos, juegos para PC y Mac.");
define("META_D_CAT_12","Intercambio de libros juveniles y comics.");
define("META_D_CAT_13","Intercambio de juegos de arte, manualidades y puzzles.");
define("META_D_CAT_14","Intercambio de mu&ntilde;ecas y peluches, juegos de vestir mu&ntilde;ecas, dibujos de mu&ntilde;ecas, mu&ntilde;ecas antiguas.");
define("META_D_CAT_15","Intercambio de musica y peliculas juveniles, infantiles, CD &amp; DVD.");
define("META_D_CAT_16","Intercambio de juguetes de radio control: autos, aviones, barcos, helicopteros, coches, trenes...");
define("META_D_CAT_17","Intercambio de vehiculos juguete: bicicletas, triciclos, vehiculos electricos juveniles, vehiculos sin motor");
define("META_D_CAT_18","Intercambio de otros juegos y juguetes y Accesorios de bebes y ni&ntilde;os");
define("META_D_CAT_19","Venta de accesorio de hogar para bebes y ni&ntilde;os, accesorio de hogar infantiles y juveniles: cunas, cambiadores, parques, tronas, hamacas.");
define("META_D_CAT_20","Venta de ropa para bebes y ni&ntilde;os, ropa infantil y juvenil.");
define("META_D_CAT_21","Venta de ropa premama, ropa embarazadas");
define("META_D_CAT_22","Venta de Accesorios para transporte de bebes y ni&ntilde;os: cochecitos, sillas de paseo, mochilas porta bebes Venta de Accesorios para viajes de bebes y ni&ntilde;os: sillas auto, cunas de viaje");

define("META_K_CAT_1","intercambio bicicletas, intercambio patinetas, intercambio patines, intercambio cometas, intercambio pelotas");
define("META_K_CAT_2","intercambio juegos belleza, intercambio juegos maquillaje, intercambio joyas juguete");
define("META_K_CAT_3","intercambio cocinitas juguete, intercambio hornos juguete, intercambio neveras juguete, intercambio alimentos y utensilios juguete");
define("META_K_CAT_4","intercambio juegos construccion, intercambio juegos armado");
define("META_K_CAT_5","intercambio disfraces juveniles, intercambio disfraces ni&ntilde;os");
define("META_K_CAT_6","intercambio juegos educativos, intercambio juegos cientificos, intercambio juegos rol");
define("META_K_CAT_7","intercambio figuras accion, intercambio mu&ntilde;ecos articulados, intercambio mu&ntilde;ecos transformables, intercambio figuras peliculas");
define("META_K_CAT_8","intercambio tambores juguete, intercambio flautas juguete, intercambio pianos juguete, intercambio guitarras juguete, intercambio xilofonos juguete");
define("META_K_CAT_9","intercambio juegos consola, intercambio playstation 3, intercambio xbox, intercambio nintendo ds,intercambio gamecube, intercambio dreamcast, intercambio wii");
define("META_K_CAT_10","intercambio juegos mesa, intercambio juegos educativos, intercambio juegos habilidad, intercambio juegos cientificos");
define("META_K_CAT_11","intercambio viedojuegos, intercambio juegos pc, intercambio juegos mac");
define("META_K_CAT_12","intercambio libros juveniles, intercambio comics");
define("META_K_CAT_13","intercambio juegos arte, intercambio manualidades, intercambio puzzles");
define("META_K_CAT_14","intercambio mu&ntilde;ecas, intercambio peluches, intercambio juegos vestir mu&ntilde;ecas, intercambio dibujos mu&ntilde;ecas, intercambio mu&ntilde;ecas antiguas");
define("META_K_CAT_15","intercambio musica juvenil, intercambio peliculas juveniles, intercambio CD, intercambio DVD");
define("META_K_CAT_16","intercambio juguetes radio control, intercambio autos, intercambio aviones, intercambio barcos, intercambio helicopteros, intercambio coches, intercambio trenes");
define("META_K_CAT_17","intercambio vehiculos juguete, intercambio bicicletas, intercambio triciclos, intercambio vehiculos electricos juveniles , intercambio vehiculos sin motor");
define("META_K_CAT_18","intercambio otros juegos, intercambio otros juguetes y accesorios");
define("META_K_CAT_19","venta intercambio muebles para bebes y ni&ntilde;os, venta intercambio muebles infantiles y juveniles, venta intercambio cunas, venta intercambio cambiadores, venta intercambio parques, venta intercambio tronas, venta intercambio hamacas");
define("META_K_CAT_20","venta intercambio ropa para bebes y ni&ntilde;os, venta intercambio ropa infantil y juvenil");
define("META_K_CAT_21","venta intercambio ropa premama, venta intercambio ropa embarazadas");
define("META_K_CAT_22","venta intercambio cochecitos, venta intercambio sillas paseo, venta intercambio mochilas porta bebes,venta intercambio sillas auto, venta intercambio cunas viaje");
?>