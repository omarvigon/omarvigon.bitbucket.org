<?php
define("ELIGE_JUGUETE","juguetes");
define("ELIGE_ARTICULO","articulos");
define("ELIGE_CAT","Elige una categoria");

define("ENLACES","Enlaces");
define("SITEMAP","Mapa Web");
define("SALUDO","&iexcl; Hola");
define("SALUDO2","Trueque de juguetes");
define("SALUDO3","Compra-Venta");
define("PGANTERIOR","Anterior");
define("PGSIGUIENTE","Siguiente");
define("LO_ULTIMO","&iexcl;LO &Uacute;LTIMO!");
define("LO_ULTIMO2","Lo &Uacute;ltimo");
define("QUIERO_0","Regalo");
define("QUIERO","&iexcl;QUIERO HACER UN TRUEQUE!");
define("QUIERO2","Quiero intercambiar...");
define("SMS_QUIEN","de quien / para quien");
define("SMS_MENSAJE","mensaje");
define("SMS_FECHA","fecha");
define("SMS_ACCION","que hacer con este mensaje");
define("SMS_RESPUESTA","Tu respuesta");
define("OLVIDADO_1","He olvidado mi Troc Name");
define("OLVIDADO_2","He olvidado mi Password");
define("OLVIDADO_3","Anota tu correo electr&oacute;nico y te enviaremos la informaci&oacute;n a esta cuenta...");
define("OLVIDADO_3_PADRES","Anota tu correo electr&oacute;nico y te enviaremos la informaci&oacute;n a esta cuenta...");

define("ALERT_FAVDEL","Se ha eliminado este juguete de tus favoritos");
define("ALERT_FAVDEL2","Se ha eliminado este art&iacute;culo de tus favoritos");
define("ALERT_FAVADD","Este juguete se ha a&ntilde;adido a tus favoritos");
define("ALERT_FAVADD2","Este art&iacute;culo se ha a&ntilde;adido a tus favoritos");
define("ALERT_REGISTRO","Tu contrase&ntilde;a se ha enviado a tu correo electronico.<br>Necesitas Tu contrase&ntilde;a * y tu Troc Name para entrar en tu cuenta KIDDYTROC, para realizar trueques.<br><br>&iexcl;&iexcl;ENHORAHBUENA!!<br>Te has registrado con &eacute;xito. Ahora para realizar un trueque, tienes que subir las fotos de tus juguetes a la web desde el apartado ''a&ntilde;adir juguete'' y seguir todos los pasos. &iexcl;Es muy f&aacute;cil! &iexcl;Venga que ya te queda menos!<br><br>*Puedes cambiar la contrase&ntilde;a cuando quieras en el partado ''mis datos'': s&oacute;lo tienes que escribir una palabra que recuerdes con facilidad y que tenga un m&aacute;ximo de 6 letras.<br><br>Saludos de KING BUDDI");
define("ALERT_JUGMOD","El juguete se ha modificado correctamente");
define("ALERT_JUGMOD2","El art&iacute;culo se ha modificado correctamente");
define("ALERT_JUGDEL","Se ha eliminado este juguete");
define("ALERT_JUGDEL2","Se ha eliminado este art&iacute;culo");
define("ALERT_JUGOK","La OPERACI&Oacute;N SUBIDA ha sido un &eacute;xito y tu juguete est&aacute; listo para que hagas un trueque.<br />Recuerda que puedes a&ntilde;adir todos los juguetes que quieras");
define("ALERT_JUGOK2","&iexcl;FELICIDADES !<br />Tu art&iacute;culo se ha cargado con &eacute;xito.<br /><br />Recuerda que puedes a&ntilde;adir todos los art&iacute;culos que quieras.");
define("ALERT_SMSOK","El mensaje se ha enviado");
define("ALERT_SMSDEL","El mensaje se ha eliminado");
define("ALERT_DENEGAR","Operacion realizada");
define("ALERT_ERROR","&iexcl;&iexcl;UPSSSS!!<br />No has rellenado todos los campos del formulario.<br /><br />&iexcl;VUELVE A INTENTARLO!<br />Los campos aparecen marcados en rojo");
define("ALERT_EMAIL_1_N","<p>ENHORABUENA!!</p><p>Bienvenido!</p><p>Te has registrado con exito en  KIDDYTROC ZONA NI&Ntilde;OS ( puedes cambiar la contrase&ntilde;a en tu cuenta /  mis datos )</p><p>Este es tu contrase&ntilde;a:");
define("ALERT_EMAIL_2_N"," </p><p>Nos vemos en Kiddytroc.</p><p>Saludos de King Buddi.</p>");
define("ALERT_EMAIL_1_P","<p>ENHORABUENA!!</p><p>Bienvenido!</p><p>Te has registrado con exito en  KIDDYTROC ZONA PADRES ( puedes cambiar la contrase&ntilde;a en tu cuenta /  mis datos )</p><p>Este es tu contrase&ntilde;a:");
define("ALERT_EMAIL_2_P"," </p><p>Nos vemos en Kiddytroc.</p><p>Saludos de King Buddi.</p>");
define("ALERT_ERROR_2","UPSSSS!! El codigo de verificaci&oacute;n no es correcto");
define("ALERT_ERROR_3","UPSSSS!! Ese Troc Name o E-Mail ya esta reservado");
define("ALERT_ERROR_4","ERROR : Ese correo electronico no esta registrado en kiddytroc");
define("ALERT_DATOS","Tus datos se han guardado correctamente");

define("ZONA_PADRES","ZONA PADRES");
define("ZONA_NINOS","ZONA NI&Ntilde;OS");
define("ZONA_LEGAL","Aviso Legal");
define("ZONA_LEGAL2","Acepto las condiciones legales");
define("ZONA_CONTACTO","Contactar");
define("ZONA_CONTACTO_OK","El comentario se ha enviado. Gracias.");
define("ZONA_REGISTRO","REG&Iacute;STRATE");
define("ZONA_REGISTRO2","REG&Iacute;STRATE");
define("ZONA_MENSAJES","Mis Mensajes");
define("ZONA_JUGUETES","Mis Juguetes");
define("ZONA_ANADIR","A&ntilde;adir Juguete");
define("ZONA_FAVORITOS","Mis Favoritos");
define("ZONA_DATOS","Mis Datos");
define("ZONA_DESCONECTAR","Desconectar");
define("ZONA_ERROR","Volver al inicio");
define("ZONA_BUSCAR","Resultados de la B&uacute;squeda");
define("ZONA_ARTICULOS","Mis Art&iacute;culos");
define("ZONA_ANADIR2","A&ntilde;adir Art&iacute;culo");

define("DATO_NOMBRE","Nombre");
define("DATO_NOMBRE2","Nombre");
define("DATO_SEXO_1","Ni&ntilde;o");
define("DATO_SEXO_0","Ni&ntilde;a");
define("DATO_NOM_NINO","Nombre del ni&ntilde;o/a");
define("DATO_NOM_PADRE","Nombre del padre/madre");
define("DATO_SEXO","&iquest;Eres ni&ntilde;o o ni&ntilde;a?");
define("DATO_SEXO_PADRES","&iquest;Hombre o mujer?");
define("DATO_SEXO_PADRES1","Hombre");
define("DATO_SEXO_PADRES0","Mujer");
define("DATO_APELLIDOS","Apellidos");
define("DATO_NACIMIENTO","Fecha de nacimiento");
define("DATO_DIRECCION","Direcci&oacute;n");
define("DATO_CP","C&oacute;digo postal");
define("DATO_POBLACION","Poblaci&oacute;n");
define("DATO_PROVINCIA","Provincia");
define("DATO_PAIS","Pa&iacute;s");
define("DATO_TFN","Tel&eacute;fono");
define("DATO_EMAIL","Correo electr&oacute;nico");
define("DATO_COMENTARIO","Comentario");
define("DATO_CONTRA","Contrase&ntilde;a");
define("DATO_TROCDESC","&iquest;C&oacute;mo es tu Troc Name?");
define("DATO_JUGUETECOMO","Descripci&oacute;n");
define("DATO_JUGUETEMOTIVO","Lo intercambiar&iacute;as por...");
define("DATO_IMAGEN","Elige una imagen para identificarte");
define("DATO_CATEGORIA","Categoria");
define("DATO_EDADES","Para que edad");
define("DATO_ANTIGUO","&iquest;Desde hace cuanto tiempo lo tienes?");
define("DATO_MARCA","Marca");
define("DATO_CONSERVA","Estado de conservaci&oacute;n");
define("DATO_MOTIVO1","Mira qu&eacute; juguetes busca");
define("DATO_MOTIVO2","Conociendo sus gustos tendr&aacute;s m&aacute;s oportunidades de que te acepte el trueque");
define("DATO_VISITAS","Visitas");
define("DATO_VISIBLE","Visible en la Web");
define("DATO_ANO","a&ntilde;o");
define("DATO_ANOS","a&ntilde;os");
define("DATO_MAXIMO","M&aacute;ximo 255 caract&eacute;res");
define("DATO_INCORRECTO","Los datos no son correctos. Int&eacute;ntalo de nuevo");
define("DATO_OBLIGATORIO","Campos obligatorios");
define("DATO_VERIFICA","introduce c&oacute;digo de verificaci&oacute;n");
define("DATO_VERIFICA_PADRES","introduce c&oacute;digo de verificaci&oacute;n");
define("DATO_NINO","Los datos del ni&ntilde;o o ni&ntilde;a son");
define("DATO_ANADIR","A&ntilde;adir un juguete es muy f&aacute;cil.<br />S&oacute;lo tienes que completar correctamente esta p&aacute;gina.");
define("DATO_JUGUETE","Datos del juguete");
define("DATO_JPG","La foto en formato JPG");
define("DATO_RECORDARME","Recordarme en este equipo");
define("DATO_PRECIO","Precio");
define("DATO_ELIGE_X","HACER UN TRUEQUE");
define("DATO_ELIGE_0","Puedes elegir una de las 3 opciones (vender, trocar, regalar)");
define("DATO_ELIGE_1","Quiero vender");
define("DATO_ELIGE_2","Quiero hacer un trueque");
define("DATO_ELIGE_3","Quiero regalar");

define("FECHA_DIA","dia");
define("FECHA_MES","mes");
define("FECHA_ANO","a&ntilde;o");
define("MES_1","Enero");
define("MES_2","Febrero");
define("MES_3","Marzo");
define("MES_4","Abril");
define("MES_5","Mayo");
define("MES_6","Junio");
define("MES_7","Julio");
define("MES_8","Agosto");
define("MES_9","Septiembre");
define("MES_10","Octubre");
define("MES_11","Noviembre");
define("MES_12","Diciembre");

define("EDAD2","0-2 a&ntilde;os");
define("EDAD5","2-5 a&ntilde;os");
define("EDAD8","5-8 a&ntilde;os");
define("EDAD12","8-12 a&ntilde;os");
define("EDAD14","mayor de 12 a&ntilde;os");
define("DESDE_0","desde hace menos de");
define("DESDE_1","desde hace");
define("DESDE_2","desde hace m&aacute;s de");
define("CONSERVA1","Deteriorado");
define("CONSERVA2","Normal");
define("CONSERVA3","&Oacute;ptimo");
define("CONSERVA4","Nuevo. Sin usar");

define("TXT_VERMIS_J","Ver mis juguetes");
define("TXT_VERSUS_J","Ver sus juguetes");
define("TXT_VERMIS_A","Ver mis art&iacute;culos");
define("TXT_VERSUS_A","Ver sus art&iacute;culos");
define("TXT_RESPONDER","KING BUDDI nos ha puesto en contacto. &iexcl;Est&aacute; hecho todo un crack! Me gusta uno de tus juguetes y me gustar&iacute;a cambi&aacute;rtelo por uno de los m&iacute;os :");
define("TXT_BUSQUEDA_0","No se ha encontrado nada con esos terminos");
define("TXT_FAVORITOS","Todos estos juguetes te gustan pero... &iquest;Con cu&aacute;l te quedas?");
define("TXT_SELECCIONAR","Seleccionar...");
define("TXT_YAESTAS","Ya estas registrado como usuario. Si quieres crear una nueva cuenta debes desconectarte primero.");
define("TXT_REL_1","Si un amigo te ha recomendado KIDDYTROC, escribe su TROC NAME aqu&iacute;");
define("TXT_REL_2","IMPORTANTE:<br /><br />Si alguien de tu casa ya est&aacute; registrado, pon su TROC NAME para que KING BUDDI sepa que es de tu familia");
define("TXT_JUGUETES_1","&iquest;Qu&eacute; tal est&aacute;s?");
define("TXT_JUGUETES_2","Estos son los juguetes que tienes para cambiar.<br /><br /><em>RECUERDA</em> que siempre puedes modificar la descripci&oacute;n, eliminarlos de la lista o hacerlos invisibles para que nadie los pueda ver, s&oacute;lo t&uacute;.");
define("TXT_ACCESO1_N","<p>Hola!</p><p>Tus datos de acceso KIDDYTROC ZONA  NI&Ntilde;OS son:</p>");
define("TXT_ACCESO2_N","<p>(  puedes cambiar la contrase&ntilde;a en tu cuenta / mis datos ) </p><p>Nos vemos en Kiddytroc.</p><p>Saludos de King Buddi.</p>");
define("TXT_ACCESO1_P","<p>Hola!</p><p>Tus datos de acceso KIDDYTROC ZONA  PADRES son:</p>");
define("TXT_ACCESO2_P","<p>(  puedes cambiar la contrase&ntilde;a en tu cuenta / mis datos )</p><p>Nos vemos en Kiddytroc.</p><p>Saludos de King Buddi.</p>");
define("TXT_OLVIDO1","Tus datos de acceso se han enviado a la cuenta: ");
define("TXT_OLVIDO2","Tambi&eacute;n puedes solicitar los datos a");
define("TXT_OTROS1","&iquest;Quieres ver tus juguetes? Pulsa aqu&iacute;.");
define("TXT_OTROS2","&iquest;Quieres ver los juguetes de esta persona? Pulsa aqu&iacute;.");
define("TXT_VENDER","Quiero vender...");
define("TXT_COMPRAR","Quiero comprar...");

define("TXT_LEGALMAIL","<p style='font-size:9px;'>En cumplimiento de la ley  Org&aacute;nica 15/1999 de 13 de Diciembre, de Protecci&oacute;n de Datos De  Car&aacute;cter Personal, le informamos que sus datos personales de  contacto han sido incorporados en ficheros informatizados titularidad  de Karine Boveroux, que sera la &uacute;nica destinar&iacute;a de los datos, cuya  finalidad es la gesti&oacute;n de clientes y acciones de comunicaci&oacute;n  comercial. As&iacute; mismo le informamos de que tiene la posibilidad de  ejercer los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y  oposici&oacute;n, previstos en la ley mediante carta dirigida a Karine  Boveroux &ndash; Kiddytroc Ref. Protecci&oacute;n de datos. Direcci&oacute;n:  Apartado de correo N&deg;33- 08192 Sant Quirze Del Valles &ndash; Espa&ntilde;a.</p>");
define("TXT_AVISOLEGAL","<p><b>AVISO LEGAL</b></p>
<p>El dominio www.kiddytroc.com pertenece a Karine BOVEROUX Nie X-9298978-D direcci&oacute;n Karine Boveroux ( Kiddytroc ) Apartado de correo N&deg;33- 08192 Sant Quirze Del Valles &ndash; Espa&ntilde;a.<br>
  Karine Boveroux cumple con todos los requisitos legales.</p>
<p>En cumplimiento de la ley Org&aacute;nica 15/1999 de 13 de Diciembre, de Protecci&oacute;n de Datos De Car&aacute;cter Personal, le informamos que sus datos personales de contacto han sido incorporados en ficheros informatizados titularidad de Karine Boveroux, que sera la &uacute;nica destinar&iacute;a de los datos, cuya finalidad es la gesti&oacute;n de clientes y acciones de comunicaci&oacute;n comercial. As&iacute; mismo le informamos de que tiene la posibilidad de ejercer los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n, previstos en la ley mediante carta dirigida a Karine Boveroux &ndash; Kiddytroc Ref. Protecci&oacute;n de datos. Direcci&oacute;n: Apartado de correo N&deg;33- 08192 Sant Quirze Del Valles &ndash; Espa&ntilde;a</p>
<p><b>Propiedad Intelectual y industrial</b></p>
<p>En conformidad con los convenios internacionales sobre propiedad intelectual e industrial Karine Boveroux es el &uacute;nico y exclusivo propietario de los derechos de propiedad intelectual e industrial de la web KIDDYTROC.COM como de sus contenidos. La reproducci&oacute;n total o parcial de la web esta totalmente prohibida bajo pena de enjuiciamiento.</p>
<p><b>Cl&aacute;usulas contractuales</b></p>
<p>Cualquier persona inscrita en el sitio KIDDYTROC.COM reconoce haber le&iacute;do y aceptado las siguientes condiciones:</p>
<p>En acuerdo con las disposiciones legales que regulan la protecci&oacute;n de los ni&ntilde;os y los menores de edad: Cualquier usuario no mayor de edad o no emancipado debe solicitar permiso de un padre o tutor legal para registrar, navegar y hacer trueque.</p>
<p>La parte mercantil de la web dedicada a la compra y venta est&aacute; reservada exclusivamente para adultos ( Area padres )</p>
<p>KIDDYTROC se ver&aacute;n obligados a cancelar la cuenta de cualquier persona que no re&uacute;ne los criterios &eacute;ticos KIDDYTROC :<br>
  Los anuncios, mensajes en ning&uacute;n caso podr&aacute;n contener palabras vulgares, sexuales o pornogr&aacute;ficos, racistas o discriminatorios: en caso de desbordamiento KIDDYTROC tiene raz&oacute;n al advertir a la instituciones judiciales competentes.</p>
<p>Le recordamos que los anuncios (trueque, compra, venta )est&aacute;n reservados para las personas privadas.</p>
<p>Le rogamos que fomenten y usen de cortes&iacute;a al intercambiar mails. Usted podr&aacute; en cualquier momento notificarnos : informarnos (info@kiddytroc.com) por mala conducta por parte de un usuario que no cumple con los criterios de respeto de KIDDYTROC.</p>
<p>Las partes convienen en que estas condiciones est&aacute;n sujetos a la legislaci&oacute;n espa&ntilde;ola. En caso de cualquier controversia sobre la interpretaci&oacute;n o la ejecuci&oacute;n de estas condiciones y en caso de fracaso de cualquier intento de encontrar una soluci&oacute;n amigable, la competencia se atribuye expresamente a los tribunales espa&ntilde;oles.</p>
<p>KIDDYTROC se compromete a nunca vender, alquilar o revelar nunca sus datos personales recogidos al registrarse, excepto en 2 casos que se indican a continuaci&oacute;n: acuerdo por escrito de usted o solicitud por escrito de las autoridades judicial.</p>
<p><b>Cookie de publicidad de Google y pol&iacute;ticas de privacidad</b></p>
<p>Google, como proveedor de terceros, utiliza cookies de <a href='http://www.doubleclick.com/privacy/faq.aspx' target='_blank'>Dart</a> para publicar anuncios en Kiddytroc. El uso de la cookie DART permite a Google publicar  anuncios a usuarios que visitan nuestro sitio y otros sitios de  Internet. Los usuarios pueden inhabilitar el uso de la cookie de DART  accediendo <a href='http://www.google.com/privacy_ads.html' target='_blank'>al  anuncio de Google y a la politica de privacidad de la red</a> (solo disponible en ingl&eacute;s).</p>");

define("AYUDA_NAME","<p>Inv&eacute;ntate un nombre. Y a partir de ahora, tus amigos de KIDDYTROC te llamar&aacute;n as&iacute;.</p><p>&iexcl;Ah! &iexcl;Y sobretodo que no se te olvide!</p><p>Ap&uacute;ntalo en una libreta, come espinacas o haz lo que quieras, porque tu TROC NAME lo necesitas para entrar en tus p&aacute;ginas personales y poder hacer trueques.</p>");
define("AYUDA_VERIFICA","<p>Debes introducir el c&oacute;digo de verificaci&oacute;n que nosotros te damos.</p><p>Tan solo f&iacute;jate en el c&oacute;digo y escribe lo mismo en el apartado correspondiente.</p>");
define("AYUDA_FAVORITO","<p>&iquest;Qu&eacute; tal? &iquest;Alg&uacute;n problemilla?</p><p>Si clicas en el recuadro &ldquo;A&ntilde;ardir a mis favoritos&rdquo;, este juguete inmediatamente se guardar&aacute; en tu apartado &ldquo;Mis favoritos&rdquo; de KIDDYTROC.</p><p>Es muy pr&aacute;ctico porque as&iacute;, cuando quieras ense&ntilde;ar los juguetes que te gustan a tus padres, no tendr&aacute;s que volver a buscarlos uno por uno. S&oacute;lo tendr&aacute;s que clicar en el apartado &ldquo;Mis favoritos&rdquo; y all&iacute; estar&aacute;n todos.</p><p>Genial, &iquest;no?</p>");
define("AYUDA_MIS","<p>&iexcl;Antes de elegir debes pensar en todo!</p><p>Si el otro ni&ntilde;o vive cerca de tu casa ser&aacute; m&aacute;s f&aacute;cil que os conozc&aacute;is y cambi&eacute;is el juguete. Si un juguete est&aacute; muy, muy viejo, &iquest;quieres cambiarlo igualmente? &iquest;Est&aacute;s seguro de que es para tu edad? &iquest;Te quieren dar gato por liebre?...</p><p>Es importante que pidas ayuda a tus padres antes de proponer un trueque a alguien.</p><p>&iexcl;&iexcl;Ellos te ayudar&aacute;n a elegir el m&aacute;s mol&oacute;n!!</p>");
define("AYUDA_TRUEQUE","<p>&iexcl;&iexcl;Cuidad&iacute;n!!</p><p>Intenta que el juguete que le propones a tu nuevo amigo tenga el valor aproximado al suyo, sino no te lo cambiar&aacute;.</p><p>Si el tuyo es de un valor inferior, puedes proponerle varios de tus juguetes en el apartado COMENTARIOS.</p><p>&iexcl;Suerte y a triunfar! </p>");
define("AYUDA_EDAD","<p>&iquest;No sabes qu&eacute; edad debe tener un ni&ntilde;o o ni&ntilde;a para poder jugar con tu juguete?</p><p>Pues &iexcl;ya tardas para preguntarlo a tus padres!</p>");
define("AYUDA_ANTIGUO","<p>&iquest;Desde hace cuanto tiempo que tienes este juguete?</p><p>Venga haz memoria. Seguro que te acuerdas.</p>");
define("AYUDA_CONSERVA","<p>&iexcl;&iexcl;Ni se te ocurra mentir!!</p><p>Si no dices la verdad, los otros ni&ntilde;os no van a querer cambiar juguetes contigo.</p><p>&iexcl;As&iacute; que saca tu lado m&aacute;s honesto y saldr&aacute;s ganando!</p>");
define("AYUDA_SUBIR","<p>Busca en tu ordenador las fotos que has hecho a tu juguete y escoge la m&aacute;s molona.</p><p>Si no sabes hacerlo, pide a alguien de casa que te ayude. &iexcl;Ya ver&aacute;s como es muy muy f&aacute;cil!</p>");
define("AYUDA_INTERCAMBIO","<p>&iquest;Te gustan las mu&ntilde;ecas? &iquest;Eres un apasionado de los coches con control remoto? &iquest;Te flipan los balones? Pues ponlo.</p><p>Escribiendo todo los juguetes que te gustar&iacute;a tener, les dar&aacute;s una pista muy &uacute;til a tus amigos de KIDDYTROC.</p><p>&iexcl;Empieza yaaaa!</p>");
define("AYUDA_VISIBLE","<p>&iquest;Quieres que todos los ni&ntilde;os vean este juguete?</p><p>&iquest;O prefieres que est&eacute; unos d&iacute;as invisible para pensar si finalmente lo quieres cambiar?</p><p>Si es as&iacute;, aparecer&aacute; en tu apartado mis juguetes pero nadie m&aacute;s que t&uacute; lo ver&aacute;, hasta que cambies esta opci&oacute;n.As&iacute; que, si quieres que todos lo vean, marca esta casilla y si quieres que sea invisible, d&eacute;jala en blanco. &iexcl;&iexcl;T&uacute; eliges!!</p>");
define("AYUDA_CONTRA","<p>&iquest; Quieres cambiar tu contrase&ntilde;a?<br />Dale vueltas al coco y escribe una palabra secreta que tenga m&aacute;ximo 6 letras y que la recuerdes con facilidad para que no se te olvide.</p><p>&iquest;&iquest;Quieres un truco?? Escribe en una libreta la pregunta cuya respuesta sea tu contrase&ntilde;a. Por ejemplo: &iquest;C&oacute;mo se llama tu juguete favorito? De esta forma, nadie m&aacute;s que tu sabr&aacute; tu secreto y a ti no se te va a olvidar nunca.<p>Mola, &iquest;no?</p>");

define("PADRE_NAME","<p>&iquest;Con qu&eacute; nombre quieres aparecer en KIDDYTROC? Escribe un seud&oacute;nimo.</p>");
define("PADRE_REGISTRO","<b>Tu contrase&ntilde;a se ha enviado a tu correo electronico.<br>Necesitas Tu contrase&ntilde;a * y tu Troc Name para entrar en tu cuenta KIDDYTROC, para vender y comprar.<br><br>&iexcl;&iexcl;ENHORAHBUENA!!</b><br>Te has registrado con &eacute;xito y ya puedes comprar.<br>Si quieres vender algunos de tus art&iacute;culos, antes debes subir una foto en el apartado '' a&ntilde;adir art&iacute;culo'' y seguir los pasos.&iexcl;Es muy f&aacute;cil!<br><br>*Puedes cambiar la contrase&ntilde;a cuando quieras en el partado ''mis datos'': s&oacute;lo tienes que escribir una palabra que recuerdes <br>con facilidad y que tenga un m&aacute;ximo de 6 letras.<br><br>Saludos de KING BUDDI");
define("PADRES_CONTRASENA","<p>Puedes cambiar la contrase&ntilde;a cuando quieras. S&oacute;lo tienes que escribir una palabra que recuerdes con facilidad y que tenga un m&aacute;ximo de 6 caracteres.</p>");
define("PADRES_CONTACTAR","<p>CONTACTAR CON EL VENDEDOR</p>");
define("PADRES_ANTIGUO","Desde hace cuanto tiempo lo tienes?");
define("PADRES_ARTICULOS","<b>&iquest;Qu&eacute; tal?</b><br /><br />Estos son los art&iacute;culos que tienes para vender.<br /><br /><em>RECUERDA</em> Siempre puedes modificar los datos o eliminarlos de la lista.");
define("PADRES_FAVORITO","&Eacute;sta es la lista de los art&iacute;culos que  te han gustado. Antes de decidir si los compras o se los truecas,  puedes clicar encima de cada uno y volver a ver sus caracter&iacute;sticas.");
define("PADRES_ADARTICULO","A&ntilde;adir un art&iacute;culo es muy f&aacute;cil.<br /><br />S&oacute;lo tienes que completar correctamente esta p&aacute;gina.");
define("PADRES_SUBIR","<p>Tienes que adjuntar una foto para que los usuarios de KIDDYTROC se hagan una idea de c&oacute;mo es tu art&iacute;culo.</p>");
define("PADRES_VISIBLE","<p>&iquest;Quieres que este art&iacute;culo se vea entre los usuarios de KIDDYTROC?<br />&iquest;O prefieres que durante unos d&iacute;as no sea visible?<br />Si eliges que sea visible, marca la casilla.<br />Si no quieres que se vea, d&eacute;jala en blanco.<br />T&uacute; lo ver&aacute;s en tu apartado &quot;Mis art&iacute;culos&quot; pero los dem&aacute;s no lo ver&aacute;n hasta que t&uacute; lo decidas y marques la casilla.</p>");
define("PADRES_ERROR","&iexcl;LO SIENTO !!<br>No has rellenado todos los campos del formulario.<br>&iexcl;VUELVE A INTENTARLO!<br>Los campos aparecen marcados en rojo");
define("PADRES_REL_1","Si un amigo o amiga te ha recomendado KIDDYTROC, escribe su TROC NAME aqu&iacute;");
define("PADRES_REL_2","IMPORTANTE:<br /><br />Si alguien de tu casa ya est&aacute; registrado, pon su TROC NAME para que KING BUDDI sepa que es de tu familia");
define("PADRES_AYUDA_VER","<p>Debes introducir el c&oacute;digo de verificaci&oacute;n que nosotros te damos. Tan solo f&iacute;jate en el c&oacute;digo y escribe lo mismo en el apartado correspondiente.</p>");

/*metas de la zona de ni�os, index y categorias principales*/
define("META_T_INDEX","KiddyTroc.es - Trueque de juguetes - Anuncios gratis ");
define("META_D_INDEX","Trueque y intercambios de juguetes y juegos en Espa&ntilde;a. Anuncios gratis. Trocar juguetes y juegos de segunda mano en Espa&ntilde;a, Europa.");
define("META_K_INDEX","trueque juegos, trueque juguetes, intercambiar juegos, intercambiar juguetes, trocar juegos, trocar juguetes, clasificados gratis para ninos y padres, trocar mis juguetes, intercambiar mis juguetes");

define("META_T_N_CONTACTA","KiddyTroc.es - Contacto");
define("META_T_N_REGISTRO","KiddyTroc.es - Inscripcion para Ni&ntilde;os");
define("META_T_N_LEGAL","KiddyTroc.es - Aviso Legal ");
define("META_T_N_USERS","KiddyTroc.es - Usuarios");

define("META_T_N_CAT_1","KiddyTroc.es - Trueque de Juguetes, Juegos: Aire Libre");
define("META_T_N_CAT_2","KiddyTroc.es - Trueque de Juguetes: Belleza, Joyas para Ni&ntilde;as");
define("META_T_N_CAT_3","KiddyTroc.es - Trueque de Juguetes: Cocinitas para Ni&ntilde;as");
define("META_T_N_CAT_4","KiddyTroc.es - Trueque de Juguetes: Construcciones");
define("META_T_N_CAT_5","KiddyTroc.es - Trueque de Disfraces para Ni&ntilde;os");
define("META_T_N_CAT_6","KiddyTroc.es - Trueque de Juegos Educativos / Rol para Ni&ntilde;os");
define("META_T_N_CAT_7","KiddyTroc.es - Trueque de Figuras de Accion");
define("META_T_N_CAT_8","KiddyTroc.es - Trueque de Juguetes Musicales");
define("META_T_N_CAT_9","KiddyTroc.es - Trueque de Juegos de Consola");
define("META_T_N_CAT_10","KiddyTroc.es - Trueque de Juegos de Mesa");
define("META_T_N_CAT_11","KiddyTroc.es - Trueque de Juegos de Ordenador");
define("META_T_N_CAT_12","KiddyTroc.es - Trueque de Libros / Comics para Ni&ntilde;os");
define("META_T_N_CAT_13","KiddyTroc.es - Trueque de Juguetes: Manualidades");
define("META_T_N_CAT_14","KiddyTroc.es - Trueque de Mu&ntilde;ecas / Peluches");
define("META_T_N_CAT_15","KiddyTroc.es - Trueque de Musica / Peliculas para Ni&ntilde;os");
define("META_T_N_CAT_16","KiddyTroc.es - Trueque de Juguetes Radio Control");
define("META_T_N_CAT_17","KiddyTroc.es - Trueque de Juguetes: Vehiculos");
define("META_T_N_CAT_18","KiddyTroc.es - Trueque de Juguetes: otros");

define("META_D_N_CAT_1","Trueque y intercambios de juegos y juguetes al aire libre: bicicletas, patinetas, patines, cometas, pelotas...de segunda mano");
define("META_D_N_CAT_2","Trueque y intercambios de juegos y juguetes de segunda mano.Trocar y intercambiar complementos de belleza, de maquillaje, joyas para Ni&ntilde;as.");
define("META_D_N_CAT_3","Trueque y intercambios de juguetes de segunda mano. Cocinitas para Ni&ntilde;as: fregaderos, hornos, neveras�");
define("META_D_N_CAT_4","Trueque y intercambios de juegos y juguetes de segunda mano de construccion, bricolaje para Ni&ntilde;os.");
define("META_D_N_CAT_5","Trueque y intercambios de disfraces de segunda mano: disfraces juveniles y disfraces para ni&ntilde;os.");
define("META_D_N_CAT_6","Trueque y intercambios de juegos y juguetes de segunda mano: juegos educativos, juegos cient�ficos, juegos de rol y estrategia para ni&ntilde;os.");
define("META_D_N_CAT_7","Trueque y intercambios de figuras de accion, mu&ntilde;ecos articulados, mu&ntilde;ecos transformables, figuras de peliculas, super heroe. Juguetes de seconda mano.");
define("META_D_N_CAT_8","Trueque y intercambios de juguetes musicales de segunda mano: tambores, flautas, pianos, guitarras, xilofonos�");
define("META_D_N_CAT_9","Trueque y intercambios juegos de consola de segunda mano: Wii, PlayStation 3, PS3, XBox, Gamecube y Dreamcast.");
define("META_D_N_CAT_10","Trueque y intercambios de juegos de mesa, juegos educativos, de habilidad o cient�ficos. Juegos de segunda mano.");
define("META_D_N_CAT_11","Trueque y intercambios de videojuegos, juegos para PC y Mac.Videojuegos de segunda mano.");
define("META_D_N_CAT_12","Trueque y intercambios de libros y comics para ni&ntilde;os. Libros y comics de segunda mano para ni&ntilde;os. ");
define("META_D_N_CAT_13","Trueque y intercambios de juegos y juguetes de segunda mano:arte, manualidades y puzzles.");
define("META_D_N_CAT_14","Trueque y intercambios de mu&ntilde;ecas y peluches de segunda mano, mu&ntilde;ecas antiguas.");
define("META_D_N_CAT_15","Trueque y intercambios de musica, peliculas, CD & DVD de segunda mano para ni&ntilde;os.");
define("META_D_N_CAT_16","Trueque y intercambios de juguetes de radio control de segunda mano: aviones, barcos, helicopteros, coches, trenes");
define("META_D_N_CAT_17","Trueque y intercambios de vehiculos juguete: bicicletas, triciclos, vehiculos electricos juveniles, vehiculos sin motor, kart infantiles");
define("META_D_N_CAT_18","Trueque de otros juegos y juguetes");

define("META_K_N_CAT_1","trueque bicicletas, trueque patinetas, trueque patines, trueque cometas, trueque pelotas, intercambiar bicicletas, intercambiar patinetas, intercambiar patines, intercambiar pelotas, trocar bicicleta, trocar pelotas, trocar patinetas");
define("META_K_N_CAT_2","trueque complementos belleza para ni&ntilde;as, trueque maquillaje para ni&ntilde;as , trueque joyas para ni&ntilde;as, trocar complementos belleza para ni&ntilde;as, trocar maquillaje para ni&ntilde;as , trocar joyas para ni&ntilde;as, intercambiar complementos belleza para ni&ntilde;as, intercambiar maquillaje para ni&ntilde;as, intercambiar joyas para ni&ntilde;as");
define("META_K_N_CAT_3","trueque cocinitas juguete, trueque hornos juguete, trueque neveras juguete, intercambiar cocinitas juguete, intercambiar hornos juguete, intercambiar neveras juguete, trocar cocinitas juguete, trocar hornos juguete, trocar neveras juguete");
define("META_K_N_CAT_4","trueque intercambiar juegos juguetes de construccion, trueque intercambio juegos juguetes de bricolaje, trueque trocar intercambiar Lego, trueque trocar intercambiar Playmobil, trueque trocar intercambiar bloques, trocar intercambiar juguetes madera, trueque trocar intercambiar Kapla, trueque trocar intercambiar Mega Bloks");
define("META_K_N_CAT_5","trueque disfraces infantiles juveniles, trueque disfraz de pirata, trocar intercambiar disfraces de pirata, trueque disfraz de princesa, intercambiar disfraces de princesa, trueque disfraz de hada, trueque intercambio disfraces de hada, trueque disfraz de sirena, intercambiar disfraces de sirena, trocar intercambiar disfraz de payaso, trocar intercambiar disfraces de payaso");
define("META_K_N_CAT_6","trueque juegos educativos, trueque juegos cient�ficos, trueque juegos rol, intercambio juegos educativos, intercambio juegos cient�ficos, intercambio juegos rol, trocar juegos educativos, trocar juegos cient�ficos, trocar juegos rol");
define("META_K_N_CAT_7","trocar figuras accion, trocar mu&ntilde;ecos galaxy defender, trocar mu&ntilde;ecos transformers, trueque figuras action man, trueque figuras hulk, trueque mu&ntilde;ecos articulados, trueque mu&ntilde;ecos transformers, intercambiar figuras de acci�n");
define("META_K_N_CAT_8","trueque tambores juguete, trueque flautas juguete, trueque pianos juguete, trocar guitarras juguete, trocar xilofonos juguete, trocar tambores juguete, trocar flautas juguete, intercambiar pianos juguete, intercambiar guitarras juguete, intercambiar xilofonos juguete, intercambiar tambores juguete");
define("META_K_N_CAT_9","trueque intercambio juegos consola, trueque intercambio playstation 3, trueque intercambio xbox, trueque intercambio gamecube, trueque intercambio dreamcast, trueque intercambio wii, trocar juegos consola en Espa&ntilde;a, trocar playstation 3 en Espa&ntilde;a, trocar xbox en Espa&ntilde;a, trocar gamecube en Espa&ntilde;a, trocar dreamcast en espana, trocar wii en Espa&ntilde;a");
define("META_K_N_CAT_10","trueque intercambio juegos mesa, trueque intercambio juegos educativos, trueque intercambio juegos habilidad, trueque intercambio juegos cient�ficos, trocar juegos mesa, trocar juegos educativos, trocar juegos habilidad, trueque juegos cientificos");
define("META_K_N_CAT_11","trueque intercambio viedojuegos, trueque intercambio juegos pc, trueque intercambio juegos mac, trocar viedojuegos, trocar juegos pc, trocar juegos mac");
define("META_K_N_CAT_12","trueque intercambio libros para ni&ntilde;os, trueque intercambio comics para ni&ntilde;os, libros de segunda mano para ni&ntilde;os, comics de segunda mano para ni&ntilde;os, trocar comics para ni&ntilde;os, trocar libros para ni&ntilde;os");
define("META_K_N_CAT_13","trueque intercambio juegos arte, trueque intercambio manualidades, trueque intercambio puzzles, trocar juegos arte, trocar manualidades, trocar puzzles");
define("META_K_N_CAT_14","trueque intercambio mu&ntilde;ecas, trueque intercambio peluches, trueque intercambio mu&ntilde;ecas antiguas, trocar peluches, trocar mu&ntilde;ecas, trueque intercambio teletubbies, trueque intercambio winnie, trueque intercambio peluche mickey mouse, trueque intercambio peluches walt disney");
define("META_K_N_CAT_15","trueque musica para ni&ntilde;os, trueque peliculas para ni&ntilde;os , trueque intercambio CD para ni&ntilde;os , trueque DVD, intercambio Cd y DVD segunda mano, trocar intercambiar DC DVD");
define("META_K_N_CAT_16","trueque juguetes radio control, trueque autos radio control, trueque aviones radio control, trueque barcos radio control, trueque helicopteros radio control, trueque coches radio control, trueque trenes radio control, intercambiar juguetes radio control");
define("META_K_N_CAT_17","trueque vehiculos juguete de segunda mano, trueque bicicletas de segunda mano, trueque triciclos de segunda mano, trueque vehiculos electricos juveniles de segunda mano, trueque vehiculos sin motor de segunda mano, intercambiar quad infantil, intercambiar kart infantil");
define("META_K_N_CAT_18","trueque otros juegos de segunda mano, trocar otros juguetes de segunda mano, intercambiar otros juegos de segunda mano, trueque otros juguetes, intercambiar juguetes de segunda mano, trocar juegos para Ni&ntilde;os");

/*metas de la zona de padres, index y categorias principales*/
define("META_T_PADRES","KiddyTroc.es - Compra Venta Trueque para ni&ntilde;os y padres");
define("META_D_PADRES","Anuncios gratis compra venta, trueque de articulos para bebes y ni&ntilde;os. Inscripcion y anuncios gratis para los usuarios en Espa&ntilde;a, Europa...");
define("META_K_PADRES","Compra venta trueque articulos para bebes, compra venta trueque articulos para ni&ntilde;os, compra venta trueque anuncios clasificados gratis");

define("META_T_REGISTRO","KiddyTroc.es - Inscripcion para Padres");

define("META_T_CAT_1","KiddyTroc.es Compra Venta Trueque de Juegos: Aire Libre");
define("META_T_CAT_2","KiddyTroc.es Compra Venta Trueque de Juguetes: Belleza para Ni&ntilde;as");
define("META_T_CAT_3","KiddyTroc.es Compra Venta Trueque de Juguetes: Cocinitas");
define("META_T_CAT_4","KiddyTroc.es Compra-Venta Trueque de Juguetes: Construcciones");
define("META_T_CAT_5","KiddyTroc.es Compra Venta Trueque: Disfraces para Ni&ntilde;os");
define("META_T_CAT_6","KiddyTroc.es Compra Venta Trueque: Juguetes Educativos ");
define("META_T_CAT_7","KiddyTroc.es Compra Venta Trueque de Figuras de Accion");
define("META_T_CAT_8","KiddyTroc.es Compra Venta Trueque de Juguetes Musicales");
define("META_T_CAT_9","KiddyTroc.es Compra Venta Trueque de Juegos de Consola");
define("META_T_CAT_10","KiddyTroc.es Compra Venta Trueque de Juegos de Mesa");
define("META_T_CAT_11","KiddyTroc.es Compra Venta Trueque de Juegos de Ordenador");
define("META_T_CAT_12","KiddyTroc.es Compra Venta Trueque de Comics para Ni&ntilde;os");
define("META_T_CAT_13","KiddyTroc.es Compra Venta Trueque de Juguetes: Manualidades");
define("META_T_CAT_14","KiddyTroc.es Compra venta y Trueque de Mu&ntilde;ecas / Peluches");
define("META_T_CAT_15","KiddyTroc.es Compra Venta Trueque de Peliculas para Ni&ntilde;os");
define("META_T_CAT_16","KiddyTroc.es Compra Venta Trueque Juguetes Radio Control");
define("META_T_CAT_17","KiddyTroc.es Compra Venta Trueque de Juguetes: Vehiculos ");
define("META_T_CAT_18","KiddyTroc.es Compra Venta Trueque de Juguetes: otros");
define("META_T_CAT_19","KiddyTroc.es Accesorios para el hogar: Bebes y Ni&ntilde;os ");
define("META_T_CAT_20","KiddyTroc.es Ropa para Bebes y Ni&ntilde;os");
define("META_T_CAT_21","KiddyTroc.es Ropa Premama - Ropa Embarazadas");
define("META_T_CAT_22","KiddyTroc.es Accesorios de viaje para Bebes y Ni&ntilde;os");

define("META_D_CAT_1","Compra venta, trueque y intercambios de juegos y juguetes al aire libre: bicicletas, patinetas, patines, cometas, pelotas...de segunda mano");
define("META_D_CAT_2","Compra venta, trueque de juegos y juguetes de segunda mano: Complementos de belleza, maquillaje, Joyas para Ni&ntilde;as.");
define("META_D_CAT_3","Compra venta, trueque de juguetes de segunda mano. Cocinitas para Ni&ntilde;as: fregaderos, hornos, neveras...");
define("META_D_CAT_4","Compra venta, trueque de juegos y juguetes de segunda mano de construccion, bricolaje para Ni&ntilde;os, juguetes de madera, bloques, Lego...");
define("META_D_CAT_5","Compra venta, trueque de disfraces de segunda mano: disfraces juveniles, disfraces para ni&ntilde;os. ");
define("META_D_CAT_6","Compra venta, trueque de juegos y juguetes de segunda mano. Intercambiar juguetes educativos, juegos cientificos, juegos de rol, juegos de estrategia para ni&ntilde;os. ");
define("META_D_CAT_7","Compra venta, trueque de figuras de accion, mu&ntilde;ecos articulados, mu&ntilde;ecos transformables, figuras de peliculas, super heroe. ");
define("META_D_CAT_8","Compra venta, trueque de juguetes musicales de segunda mano: tambores, flautas, pianos, guitarras, xilofono...");
define("META_D_CAT_9","Compra venta, trueque de juegos de consola de segunda mano: Wii, PlayStation 3, PS3, XBox, Gamecube y Dreamcast.");
define("META_D_CAT_10","Compra venta, trueque de juegos de mesa, juegos educativos, juegos de habilidad o cientificos. Juegos de segunda mano.");
define("META_D_CAT_11","Compra venta trueque de videojuegos, juegos para PC y Mac.Videojuegos de segunda mano.");
define("META_D_CAT_12","Compra venta, trueque de libros juveniles y comics para ni&ntilde;os. Libros y comics de segunda mano para ni&ntilde;os.");
define("META_D_CAT_13","Compra venta, trueque de juegos de segunda mano:arte, manualidades, puzzles...");
define("META_D_CAT_14","Compra venta, trueque, intercambio de mu&ntilde;ecas y peluches de segunda mano...mu&ntilde;ecas antiguas.");
define("META_D_CAT_15","Compra venta, trueque de musica y peliculas juveniles, CD &amp; DVD de segunda mano para Ni&ntilde;os");
define("META_D_CAT_16","Compra venta, trueque de juguetes de radio control de segunda mano: autos, aviones, barcos, helicopteros, coches, trenes...");
define("META_D_CAT_17","Compra Venta, trueque de vehiculos para ni&ntilde;os : bicicletas, triciclos, vehiculos electricos juveniles, vehiculos sin motor, quad, kart para ni&ntilde;os");
define("META_D_CAT_18","Compra venta, trueque de otros juegos y juguetes.");
define("META_D_CAT_19","Compra venta, trueque de accesorios para bebes y ni&ntilde;os. Accesorios infantiles y juveniles para el hogar: cu&ntilde;as, cambiadores, parques, tronas, hamacas...");
define("META_D_CAT_20","Compra venta, trueque de ropa para bebes y ni&ntilde;os, ropa infantil y juvenil");
define("META_D_CAT_21","Compra venta, trueque de ropa premama y ropa embarazadas");
define("META_D_CAT_22","Compra venta, trueque de accesorios para viajar con bebes y ni&ntilde;os: cochecitos, sillas para paseo, mochilas portabebes, trona de viaje.");

define("META_K_CAT_1","compra venta trueque bicicletas, compra venta trueque patinetas, compra venta trueque patines, compra venta trueque cometas, compra venta trueque pelotas, compra venta trueque bicicletas, compra venta trueque patinetas, compra venta trueque patines, trueque de juguetes al aire libre");
define("META_K_CAT_2","compra venta trueque complementos belleza para ni&ntilde;as, compra venta trueque maquillaje para ni&ntilde;as, compra venta trueque joyas para ni&ntilde;as. Intercambiar joyas para ni&ntilde;as");
define("META_K_CAT_3","compra venta trueque cocinitas juguete, compra venta trueque hornos juguete, compra venta trueque neveras juguete, compra venta trueque cocinitas juguete, compra venta trueque hornos juguete, compra venta trueque neveras juguete");
define("META_K_CAT_4","compra venta trueque juegos juguetes de construccion para ni&ntilde;os, compra venta trueque juegos juguetes de bricolaje para ni&ntilde;os, compra venta trueque Lego, compra venta trueque Playmobil, compra venta trueque de bloques, compra venta trueque de juguetes de madera, trueque de Kapla, trueque de Mega Bloks");
define("META_K_CAT_5","compra venta trueque disfraces infantiles juveniles, compra venta trueque disfraz de pirata, compra venta trueque disfraces de pirata, compra venta trueque disfraz de princesa, compra venta trueque disfraces de princesa, compra venta trueque disfraz de hada, compra venta trueque disfraces de hada, compra venta trueque disfraz de Sirena, compra venta trueque disfraces de sirena, compra venta trueque disfraz de payaso, compra venta trueque disfraces de payaso");
define("META_K_CAT_6","compra venta juguetes educativos, trueque juegos cient�ficos, trueque juegos rol, compra venta juguetes educativos, compra venta juegos cient�ficos, compra venta juegos rol, compra venta juegos estrategia, intercambiar juguetes, intercambiar juegos");
define("META_K_CAT_7","compra venta figuras accion, compra venta mu&ntilde;ecos galaxy defender, compra venta mu&ntilde;ecos transformers, trueque figuras action man, trueque figuras hulk, trueque mu&ntilde;ecos articulados, trueque mu&ntilde;ecos transformers");
define("META_K_CAT_8","compra venta tambores juguete, trueque flautas juguete, trueque pianos juguete, trueque guitarras juguete, trueque xilofonos juguete, compra venta tambores juguete, compra venta flautas juguete, compra venta pianos juguete, compra venta guitarras juguete, compra venta xilofonos juguete, intercambiar juguetes musicales");
define("META_K_CAT_9","compra ventajuegos consola, trueque playstation 3, trueque xbox, trueque gamecube, trueque dreamcast, trueque wii, compra venta wii, compra venta playstation 3, intercambiar wii en Espana, intercambiar playstation 3 en Espa&ntilde;a");
define("META_K_CAT_10","compra venta juegos mesa, trueque juegos educativos, trueque juegos habilidad, trueque juegos cient�ficos, compra venta juegos mesa, compra venta juegos educativos, compra venta juegos habilidad, compra venta juegos cientificos");
define("META_K_CAT_11","compra venta viedojuegos, trueque juegos pc, trueque juegos mac, compra venta viedojuegos, compra venta juegos pc, compra venta juegos mac");
define("META_K_CAT_12","compra venta libros juveniles, trueque comics para ni&ntilde;os, libros de segunda mano para ni&ntilde;os, comics de segunda mano para ni&ntilde;os, compra venta libros juveniles, compra venta comics para ni&ntilde;os");
define("META_K_CAT_13","compra venta arte, trueque manualidades, trueque puzzles, compra venta juegos arte, compra venta manualidades, compra venta puzzles, intercambiar juguetes de manualidades, intercambiar juegos de manualidades");
define("META_K_CAT_14","compra venta mu&ntilde;ecas, trueque peluches, trueque mu&ntilde;ecas antiguas, compra venta peluches, compra venta mu&ntilde;ecas, compra venta teletubbies, compra venta winnie, trueque mickey mouse, trueque peluches walt disney");
define("META_K_CAT_15","compra venta musica juvenil, trueque peliculas para ni&ntilde;os , trueque CD para ni&ntilde;os, trueque DVD para ni&ntilde;os, compra venta Cd y DVD para ni&ntilde;os segunda mano");
define("META_K_CAT_16","compra venta juguetes radio control, trueque autos radio control, trueque aviones radio control, trueque barcos radio control, trueque helicopteros radio control, trueque coches radio control, trueque trenes radio control, compra venta juguetes radio control");
define("META_K_CAT_17","compra venta vehiculos juguete de segunda mano, trueque bicicletas de segunda mano, trueque triciclos de segunda mano, trueque vehiculos electricos juveniles de segunda mano, trueque vehiculos sin motor de segunda mano, compra venta quad infantil, compra venta kart infantil");
define("META_K_CAT_18","compra venta otros juegos de segunda mano, trueque otros juguetes de segunda mano, intercambiar otros juegos de segunda mano, intercambiar otros juguetes de segunda mano, compra venta juguetes de segunda mano, compra venta juegos de segunda mano");
define("META_K_CAT_19","Compra venta trueque muebles para bebes ni&ntilde;os, compra venta trueque muebles infantiles juveniles, compra venta trueque cu&ntilde;as, compra venta trueque cambiadores, compra venta  trueque parques, compra venta trueque tronas, compra venta trueque vigilabebe");
define("META_K_CAT_20","Compra venta trueque ropa para bebes y ni&ntilde;os, compra venta trueque ropa infantil y juvenil, compra venta trueque ropa interior para bebes, compra venta trueque zapatitos botas para bebes y ni&ntilde;os");
define("META_K_CAT_21","Compra venta trueque ropa complementos premama, compra venta trueque ropa complementos embarazadas, compra venta trueque ropa de segunda mano embarazadas, trueque puericultura, compra venta puericultura");
define("META_K_CAT_22","compra venta trueque bicicletas, compra venta trueque patinetas, compra venta trueque patines, compra venta trueque cometas, compra venta trueque pelotas, compra venta trueque bicicletas, compra venta trueque patinetas, compra venta trueque patines, trueque de juguetes al aire libre");
?>
