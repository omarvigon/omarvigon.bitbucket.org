<?php
define("ELIGE_JUGUETE","jouets");
define("ELIGE_ARTICULO","articles");
define("ELIGE_CAT","Choisir une cat&eacute;gorie");

define("ENLACES","Liens");
define("SITEMAP","Plan du Site");
define("SALUDO","Bonjour");
define("SALUDO2","Troc de Jouets");
define("SALUDO3","Achat-Vente");
define("PGANTERIOR","Pr&eacute;c&eacute;dent");
define("PGSIGUIENTE","Suivant");
define("LO_ULTIMO","NOUVEAUT&Eacute;S !");
define("LO_ULTIMO2","Nouveaut&eacute;s");
define("QUIERO_0","Je donne");
define("QUIERO","Je veux faire un troc !");
define("QUIERO2","Je veux troquer...");
define("SMS_QUIEN","de qui/pour qui");
define("SMS_MENSAJE","objet");
define("SMS_FECHA","date");
define("SMS_ACCION","que faire avec le message");
define("SMS_RESPUESTA","Ta r&eacute;ponse");
define("OLVIDADO_1","J'ai oubli&eacute; mon Troc Name");
define("OLVIDADO_2","J'ai oubli&eacute; mon mot de passe");
define("OLVIDADO_3","Indique ton adresse mail nous  t'enverrons tes codes d'acc&egrave;s");
define("OLVIDADO_3_PADRES","Indiquez votre adresse mail nous vous  enverrons vos codes d'acc&egrave;s");

define("ALERT_FAVDEL","Ce jouet a &eacute;t&eacute; supprim&eacute; de tes favoris");
define("ALERT_FAVDEL2","Cet article a &eacute;t&eacute; supprim&eacute; de vos favoris");
define("ALERT_FAVADD","Le jouet a &eacute;t&eacute; ajout&eacute; a tes favoris");
define("ALERT_FAVADD2","Cet article a &eacute;t&eacute; ajout&eacute; &agrave; vos favoris");
define("ALERT_REGISTRO","<b>LE MOT DE PASSE KIDDYTROC VIENT D'&Ecirc;TRE ENVOYE SUR TON E-MAIL<br>Tu auras besoin de ton mot de passe* et de ton Troc Name pour te connecter sur ton compte Kiddytroc pour y troquer tes jeux et tes jouets.<br><br>F&Eacute;LICITATIONS !!</b><br>Tu t'es inscrit(e) avec succ&egrave;s sur KIDDYTROC:<br><br>Dans un premier temps, pour faire un troc tu dois ins&eacute;rer les photos de tes jouets sur le site dans la rubrique &quot;ajouter un jouet&quot; et suivre la proc&eacute;dure indiqu&eacute;e: tu verras c'est tr&egrave;s facile.<br>A toi de jouer maintenant !<br><br>* Lorsque tu seras dans ton compte KIDDYTROC, ''mes coordonn&eacute;es'': tu pourras remplacer le mot de passe envoy&eacute; par mail par un mot de passe de ton choix, plus facile &agrave; retenir.<br><br>A tr&egrave;s bient&ocirc;t,<br>KING BUDDI");
define("ALERT_JUGMOD","Le jouet a &eacute;t&eacute; correctement modifi&eacute;");
define("ALERT_JUGMOD2","L'article a &eacute;t&eacute; correctement modifi&eacute;");
define("ALERT_JUGDEL","Le jouet a &eacute;t&eacute; supprim&eacute;");
define("ALERT_JUGDEL2","Votre  article a &eacute;t&eacute; supprim&eacute;");
define("ALERT_JUGOK","L'OP&Eacute;RATION UPLOAD a &eacute;t&eacute; un vrai succ&egrave;s et ton jouet est pr&ecirc;t &agrave; &ecirc;tre troqu&eacute;.<br /><br />Souviens-toi : tu peux rajouter autant de jouets que tu veux.");
define("ALERT_JUGOK2","F&Eacute;LICITATIONS !<br />Votre article a &eacute;t&eacute; enregistr&eacute;  avec succ&egrave;s.<br /><br />Souvenez-vous que vous pouvez ajouter autant d&rsquo;articles que vous le souhaitez.");
define("ALERT_SMSOK","Le message a &eacute;t&eacute; envoy&eacute;");
define("ALERT_SMSDEL","Le message a &eacute;t&eacute; supprim&eacute;");
define("ALERT_DENEGAR","Op&eacute;ration r&eacute;alis&eacute;e");
define("ALERT_ERROR","OUPS !!<br />Tu n'as pas rempli tous les champs du formulaire<br /><br />R&Eacute;ESSAYE !<br />Les champs manquants apparaissent en rouge");
define("ALERT_EMAIL_1_N","<p>FELICITATIONS!<br /><br />Bienvenu(e)!</p><p>L'inscription dans la ZONE ENFANTS sur  KIDDYTROC est un succ&egrave;s.</p><p>Voici le mot de passe:");
define("ALERT_EMAIL_2_N","( ce mot  de passe peut &ecirc;tre chang&eacute; &agrave; tout moment dans le compte, mes  coordonn&eacute;es )</p><p>A tr&egrave;s bient&ocirc;t sur KIDDYTROC,<br /><br />King Buddi</p><p>Bonjour!</p>");
define("ALERT_EMAIL_1_P","<p>FELICITATIONS!<br /><br />Bienvenu(e)!</p><p>L'inscription dans la ZONE PARENTS sur  KIDDYTROC est un succ&egrave;s.</p><p>Voici le mot de passe:");
define("ALERT_EMAIL_2_P"," ( ce mot  de passe peut &ecirc;tre chang&eacute; &agrave; tout moment dans le compte, mes  coordonn&eacute;es )</p><p>A tr&egrave;s bient&ocirc;t sur KIDDYTROC,<br /><br />King Buddi<br /></p><p>Bonjour!</p>");
define("ALERT_ERROR_2","OUPS !! Le code de v&eacute;rification est incorrecte");
define("ALERT_ERROR_3","OUPS !! Ce Troc Name ou cet E-Mail est dej&agrave; r&eacute;serv&eacute;");
define("ALERT_ERROR_4","ATTENTION: l'e-mail indiqu&eacute; est erron&eacute;.");
define("ALERT_DATOS","Les donn&eacute;es ont &eacute;t&eacute; sauvegard&eacute;es correctement");

define("ZONA_PADRES","ZONE PARENTS");
define("ZONA_NINOS","ZONE ENFANTS");
define("ZONA_LEGAL","Avis l&eacute;gal");
define("ZONA_LEGAL2","Accepter l'avis l&eacute;gal");
define("ZONA_CONTACTO","Contact");
define("ZONA_CONTACTO_OK","Le commentaire a bien &eacute;t&eacute; envoy&eacute;. Merci.");
define("ZONA_REGISTRO","INSCRIS-TOI");
define("ZONA_REGISTRO2","INSCRIVEZ-VOUS");
define("ZONA_MENSAJES","Mes Messages");
define("ZONA_JUGUETES","Mes Jouets");
define("ZONA_ANADIR","Ajouter un Jouet");
define("ZONA_FAVORITOS","Mes Favoris");
define("ZONA_DATOS","Mes Coordonn&eacute;es");  
define("ZONA_DESCONECTAR","D&eacute;connection");
define("ZONA_ERROR","Retour au menu principal");
define("ZONA_BUSCAR","R&eacute;sultats de la Recherche");
define("ZONA_ARTICULOS","Mes Articles");
define("ZONA_ANADIR2","Ajouter un Article");

define("DATO_NOMBRE","Pr&eacute;nom");
define("DATO_NOMBRE2","D&eacute;nomination");
define("DATO_SEXO_1","Gar&ccedil;on");
define("DATO_SEXO_0","Fille");
define("DATO_NOM_NINO","Pr&eacute;nom");
define("DATO_NOM_PADRE","Nom du p&egrave;re / de la m&egrave;re");
define("DATO_SEXO","Gar&ccedil;on/Fille?");
define("DATO_SEXO_PADRES","Homme ou femme?");
define("DATO_SEXO_PADRES1","Homme");
define("DATO_SEXO_PADRES0","Femme");
define("DATO_APELLIDOS","Nom de famille");
define("DATO_NACIMIENTO","Date de naissance");
define("DATO_DIRECCION","Adresse");
define("DATO_CP","Code postal");
define("DATO_POBLACION","Ville");
define("DATO_PROVINCIA","D&eacute;partement ");
define("DATO_PAIS","Pays");
define("DATO_TFN","Num&eacute;ro de t&eacute;l&eacute;phone");
define("DATO_EMAIL","E-mail");
define("DATO_COMENTARIO","Commentaires");
define("DATO_CONTRA","Mot de passe");
define("DATO_TROCDESC","D&eacute;cris ton Troc Name");
define("DATO_JUGUETECOMO","Ajouter une description");
define("DATO_JUGUETEMOTIVO","Tu l'&eacute;changerais contre...");
define("DATO_IMAGEN","Choisis l'image que tu pr&eacute;f&egrave;res");
define("DATO_CATEGORIA","Cat&eacute;gorie");
define("DATO_EDADES","Pour quel &acirc;ge");
define("DATO_ANTIGUO","De quand date ce jouet ?"); //Anciennet&eacute;
define("DATO_MARCA","Marque");
define("DATO_CONSERVA","&Eacute;tat de conservation");
define("DATO_MOTIVO1","Regarde les jouets que recherche");
define("DATO_MOTIVO2","En connaissant  ses go&ucirc;ts, tu auras plus de chances que le troc se r&eacute;alise avec succ&egrave;s");
define("DATO_VISITAS","	Visites");
define("DATO_VISIBLE","Visible sur le site");
define("DATO_ANO","an"); 
define("DATO_ANOS","ans"); 
define("DATO_MAXIMO","255 caract&egrave;res maxi");
define("DATO_INCORRECTO","D&eacute;sol&eacute;! Donn&eacute;es incorrectes. Recommencer.");
define("DATO_OBLIGATORIO","Champs obligatoires");
define("DATO_VERIFICA","Saisis le code de v&eacute;rification");
define("DATO_VERIFICA_PADRES","Saisir le code de v&eacute;rification");
define("DATO_NINO","Renseignements concernant l'enfant");
define("DATO_ANADIR","Ajouter un jouet c'est tr&egrave;s facile.<br />Il te suffit de remplir correctement cette page.");
define("DATO_JUGUETE","Renseignements du jouet ");
define("DATO_JPG","La photo &agrave; ins&eacute;rer sur le site doit &ecirc;tre en format JPEG");
define("DATO_RECORDARME","M&eacute;moriser le mot de passe sur cet ordinateur"); 
define("DATO_PRECIO","Prix");
define("DATO_ELIGE_X","FAIRE UN TROC");
define("DATO_ELIGE_0","Vous  pouvez choisir 1 des 3 options (vendre,troquer ou donne)");
define("DATO_ELIGE_1","Je vends");
define("DATO_ELIGE_2","Je veux faire un troc");
define("DATO_ELIGE_3","Je donne");

define("FECHA_DIA","jour");
define("FECHA_MES","mois");
define("FECHA_ANO","ann&eacute;e");
define("MES_1","Janvier");
define("MES_2","F&eacute;vrier");
define("MES_3","Mars");
define("MES_4","Avril");
define("MES_5","Mai");
define("MES_6","Juin");
define("MES_7","Juillet");
define("MES_8","Ao&ucirc;t");
define("MES_9","Septembre");
define("MES_10","Octobre");
define("MES_11","Novembre");
define("MES_12","D&eacute;cembre");

define("EDAD2","0-2 ans");
define("EDAD5","2-5 ans");
define("EDAD8","5-8 ans");
define("EDAD12","8-12 ans");
define("EDAD14","plus de 12 ans");
define("DESDE_0","de moins de");
define("DESDE_1","de ");
define("DESDE_2","de plus de");
define("CONSERVA1","En mauvais &eacute;tat");
define("CONSERVA2","Normal");
define("CONSERVA3","En tr&egrave;s bon &eacute;tat");
define("CONSERVA4","Neuf jamais utilis&eacute;");

define("TXT_VERMIS_J","Voir mes jouets");
define("TXT_VERSUS_J","Voir ses jouets");
define("TXT_VERMIS_A","Voir mes articles");
define("TXT_VERSUS_A","Voir ses articles");
define("TXT_RESPONDER","KING BUDDI nous a mis en contact. C'est vraiment un crack ! J'aime bien un de tes jouets et j'aimerais te le troquer contre un des miens :");
define("TXT_BUSQUEDA_0","Aucun objet ne r&eacute;pond &agrave; la recherche");
define("TXT_FAVORITOS","Tu aimes tous ces jouets, mais... lequel vas-tu choisir ?");
define("TXT_SELECCIONAR","S&eacute;lectionner...");
define("TXT_YAESTAS","Tu es d&eacute;j&agrave; enregistr&eacute; comme utilisateur. Si tu d&eacute;sires cr&eacute;er un autre compte tu dois d'abord te d&eacute;connecter.");
define("TXT_REL_1","Si un ami t'a recommand&eacute; KIDDY TROC , &eacute;cris son TROC NAME ici");
define("TXT_REL_2","IMPORTANT:<br /><br />Si un membre de ta famille est d&eacute;j&agrave; enregistr&eacute;, indique son TROC NAME pour que KING BUDDI sache qu'il est de ta famille");
define("TXT_JUGUETES_1","Comment vas-tu?");
define("TXT_JUGUETES_2","Voici les jouets que tu veux troquer.<br /><br /><em>SOUVIENS-TOI</em> que tu peux modifier la description, &eacute;liminer des jouets de la liste, ou les rendre invisibles pour que personne ne puisse les voir &agrave; part toi .");
define("TXT_ACCESO1_N","<p>Bonjour!</p><p>Voici les codes  d&acute;acc&egrave;s KIDDYTROC - ZONE ENFANTS:</p>");
define("TXT_ACCESO2_N","<p>( ce mot de passe peut &ecirc;tre chang&eacute; &agrave; tout moment dans  le compte, mes coordonn&eacute;es )</p><p>A tr&egrave;s bient&ocirc;t sur KIDDYTROC,</p><p>King Buddi</p>");
define("TXT_ACCESO1_P","<p>Bonjour!</p><p>Voici les codes  d&acute;acc&egrave;s KIDDYTROC - ZONE PARENTS:</p>");
define("TXT_ACCESO2_P","<p>( ce mot de passe peut &ecirc;tre chang&eacute; &agrave; tout moment dans  le compte, mes coordonn&eacute;es )</p><p>A tr&egrave;s bient&ocirc;t sur KIDDYTROC,</p><p>King Buddi</p>");
define("TXT_OLVIDO1","Tes codes d'acc&egrave;s ont &eacute;t&eacute;  envoy&eacute;s &agrave; ta bo&icirc;te mail ");
define("TXT_OLVIDO2","Tu peux aussi les demander en envoyant  un mail &agrave;: ");
define("TXT_OTROS1","Voulez-vous voir vos jouets? Cliquez ici");
define("TXT_OTROS2","Vous voulez voir les autres jouets de ce type? Cliquez ici");
define("TXT_VENDER","Je veux vendre...");
define("TXT_COMPRAR","Je veux acheter...");

define("TXT_LEGALMAIL","<p style='font-size:9px;'>Conform&eacute;ment &agrave; la loi  informatique 15/1999 du D&eacute;cembre 13, sur la protection des donn&eacute;es  &agrave; caract&egrave;re personnel, nous vous informons que vos donn&eacute;es de  contact ont &eacute;t&eacute; incorpor&eacute;es dans les fichiers informatiques  appartenant &agrave; Karine Boveroux, qui sera la seule destinataire de ces  donn&eacute;es, dont le but est de g&eacute;rer les clients et les actions de  communication commerciale. En vertu de la loi, nous vous informons  que vous avez la possibilit&eacute; d'exercer les droits d'acc&egrave;s, de  rectification et d'opposition, par &eacute;crit &agrave; Karine Boveroux -  Kiddytroc R&eacute;f. Respect de la vie priv&eacute;e. Adresse: Apartado de  correos n &deg; 33 - 08192 S! ant Quirze del Valle - Espagne</p>");
define("TXT_AVISOLEGAL","<p><b>AVIS LEGAL</b></p>
<p>Karine  Boveroux est propri&eacute;taire du domaine <a href='http://www.kiddytroc.com/'>www.kiddytroc.com</a> </p>
<p>Adresse  Karine Boveroux ( Kiddytroc ) Apartado de correo N&deg;33- 08192 Sant  Quirze Del Valles &ndash; Espagne.</p>
<p>Nie X-9298978-D</p>
<p>Karine  Boveroux respecte et r&eacute;pond &agrave; toutes les conditions  l&eacute;gales et fiscales requises.</p>
<p>Conform&eacute;ment  &agrave; la loi informatique 15/1999 du D&eacute;cembre 13, sur la  protection des donn&eacute;es &agrave; caract&egrave;re personnel,  nous vous informons que vos donn&eacute;es de contact ont &eacute;t&eacute;  incorpor&eacute;es dans les fichiers informatiques appartenant &agrave;  Karine Boveroux, qui sera la seule destinataire de ces donn&eacute;es,  dont le but est de g&eacute;rer les clients et les actions de  communication commerciale. En  vertu de la loi, nous vous informons  que vous avez la possibilit&eacute; d'exercer les droits d'acc&egrave;s,  de rectification et d'opposition, par &eacute;crit &agrave; Karine  Boveroux - Kiddytroc R&eacute;f. Respect de la vie priv&eacute;e.  Adresse: Apartado de correos n &deg; 33 - 08192 Sant Quirze del Valle  - Espagne</p>
<p><strong>Propri&eacute;t&eacute;  Intellectuelle et Industrielle</strong></p>
<p>Conform&eacute;ment  aux conventions internationales relatives &agrave; la propri&eacute;t&eacute;  intellectuelle et industrielle KIDDYTROC SL est le seul et l'unique  propri&eacute;taire des droits de propri&eacute;t&eacute;  intellectuelle et industrielle du site KIDDYTROC.COM et de son  contenu.</p>
<p>Toute  reproduction totale ou partielle du site est totalement interdite  sous peine de poursuites judiciaires.</p>
<p><b>Conditions  contractuelles</b></p>
<p>Toute  personne inscrite sur le site KIDDYTROC.COM reconnait avoir pris  connaissance et  accept&eacute; les conditions qui suivent:</p>
<p>Selon  les dispositions l&eacute;gales r&eacute;gulant la protection de  l'enfance et des mineurs: tout utilisateur non majeur ou non &eacute;mancip&eacute;  est tenu de demander l'autorisation &agrave; un de ses parents ou  tuteur l&eacute;gal pour s'y inscrire, naviguer et troquer.</p>
<p>KIDDYTROC  se verra contraint d'annuler &agrave; vie toute inscription de toute  personne ne r&eacute;pondant pas aux crit&egrave;res &eacute;thiques  de KIDDYTROC &agrave; savoir:</p>
<p>Les  annonces, les mails ne devront en aucun cas contenir des termes  vulgaires, &agrave; caract&egrave;re sexuel ou pornographique, &agrave;  caract&egrave;re raciste ou discriminatoire.</p>
<p>En  cas de d&eacute;bordement KIDDYTROC s'accorde le droit d'avertir les  institutions judiciaires comp&eacute;tentes.</p>
<p>Nous  vous rappelons que la partie &laquo;&nbsp;petites annonces&nbsp;&raquo;  (troc, vente, achat) est r&eacute;serv&eacute;e aux particuliers.</p>
<p>Nous  vous demandons d'encourager et d'utiliser la courtoisie lors de vos  &eacute;changes mails.</p>
<p>Vous  pourrez &agrave; tout moment nous avertir par mail  (<a href='mailto:info@kiddytroc.com'>info@kiddytroc.com</a>) d'un  mauvais comportement de la part d'un utilisateur ne r&eacute;pondant  pas aux crit&egrave;res de respect qui sont les n&ocirc;tres.</p>
<p><b>Divulgation  de vos donn&eacute;es nominatives</b></p>
<p>KIDDYTROC  s'engage &agrave; ne jamais vendre, &agrave; ne jamais louer ou  divulguer vos donn&eacute;es personnelles collect&eacute;es lors de  l'inscription, &agrave; des tiers sauf dans les 2 cas qui figurent  ci-apr&egrave;s: accord &eacute;crit de votre part ou requ&ecirc;te  &eacute;crite &eacute;manant des autorit&eacute;s judiciaires.</p>
<p>Les  parties conviennent que les pr&eacute;sentes conditions sont soumises  au droit espagnol.</p>
<p>En  cas de contestation &eacute;ventuelle sur l'interpr&eacute;tation ou  sur l'ex&eacute;cution des pr&eacute;sentes conditions et en cas  d'&eacute;chec de toute tentative de recherche d'une solution  amiable, comp&eacute;tence expresse est attribu&eacute;e aux  tribunaux espagnols.</p>
<p><b>Cookie  publicitaire et r&egrave;gles de confidentialit&eacute; Google</b></p>
<p>En  tant que prestataire tiers, Google utilise des cookies de <a href='http://www.doubleclick.com/privacy/faq.aspx' target='_blank'>Dart</a> pour diffuser  des annonces sur Kiddytroc. Gr&acirc;ce au cookie DART, Google adapte les  annonces diffus&eacute;es aux utilisateurs en fonction de leur navigation  sur notre site Web ou d'autres sites. Les utilisateurs peuvent  d&eacute;sactiver l'utilisation du cookie DART en se rendant sur la page des <a href='http://www.google.com/privacy_ads.html' target='_blank'>r&egrave;gles de  confidentialit&eacute;</a> s'appliquant  au r&eacute;seau de contenu et aux annonces Google (disponible en anglais uniquement).</p>");

define("AYUDA_NAME","<p>Invente-toi un nom. Dor&eacute;navant, tes amis de KIDDYTROC t&rsquo;appelleront ainsi.</p><p>Attention ! Surtout, ne l&rsquo;oublie pas ! </p><p>Note-le dans un cahier, tu as vraiment besoin de ton TROC NAME pour rentrer dans tes pages personnelles et pouvoir faire des trocs</p>");
define("AYUDA_VERIFICA","<p>Tu dois entrer le code de v&eacute;rification que te donne King Buddi dans la case correspondante.</p>");
define("AYUDA_FAVORITO","<p>Alors ? Des petits soucis ?</p><p>Si tu cliques dans le cadre &ldquo;Ajouter &agrave; mes favoris&rdquo;, ce jouet se rajoutera automatiquement dans ta rubrique &ldquo;Mes favoris&rdquo; de KIDDYTROC. C&rsquo;est tr&egrave;s pratique : quand tu voudras montrer les jouets qui te plaisent &agrave; tes parents, tu n&rsquo;auras pas &agrave; les chercher un par un. Il te suffira de cliquer dans la rubrique &ldquo;Mes favoris&rdquo; pour tous les voir d&rsquo;un coup.</p><p>G&eacute;nial, non ? </p>");
define("AYUDA_MIS","<p>Avant de choisir, tu dois penser &agrave; tout !</p><p>Si l&acute;autre enfant habite pr&egrave;s de chez toi, ce sera plus facile pour faire connaissance et faire votre troc. Attention ! Avant de confirmer un troc, assure-toi bien que le jouet te convient et te plait r&eacute;ellement. Il est tr&egrave;s important que tu demandes conseil &agrave; tes parents avant de proposer un troc &agrave; quelqu&acute;un.</p><p>Ils t&rsquo;aideront &agrave; choisir le jouet le plus top !! </p>");
define("AYUDA_TRUEQUE","<p>Attention, attention!!</p><p>Essaie de faire en sorte que le jouet que tu proposes &agrave; ton nouvel ami ait une valeur similaire au sien, sinon, il ne voudra pas le troquer. Si ton jouet est d&rsquo;une valeur inf&eacute;rieure, tu peux lui proposer plusieurs de tes jouets dans la rubrique COMMENTAIRES.</p><p>Bonne chance et vas-y, d&eacute;marre ! </p>");
define("AYUDA_EDAD","<p>Tu ne sais pas quel &acirc;ge doit avoir un enfant pour pouvoir s&rsquo;amuser avec ton jouet?</p><p>Qu&rsquo;attends-tu ? Va vite demander conseil &agrave; tes parents !</p>");
define("AYUDA_ANTIGUO","<p>De quand date ce jouet ?<br /><br />Allez, fais un petit effort, essaie de te rappeler. Je suis s&ucirc;r que tu vas t&acute;en souvenir !</p>");
define("AYUDA_CONSERVA","<p>Ne mens surtout pas !! Si tu ne dis pas la v&eacute;rit&eacute;, les autres enfants ne vont pas vouloir troquer des jouets avec toi. Sois honn&ecirc;te, tu en sortiras gagnant !</p>");
define("AYUDA_SUBIR","<p>Cherche sur ton ordi les meilleures photos de ton jouet.</p><p>Si tu ne sais pas comment faire, demande &agrave; quelqu&rsquo;un de t&rsquo;aider &agrave; la maison.<br />Tu verras, c&rsquo;est super facile !</p>");
define("AYUDA_INTERCAMBIO","<p>Tu aimes les poup&eacute;es ?</p><p>Tu es un fan de voitures t&eacute;l&eacute;command&eacute;es?</p><p>Tu es un fou de ballons ?</p><p>Alors, note-le.</p><p>En indiquant tous les jouets que tu aimerais avoir, tu donneras une piste tr&egrave;s utile &agrave; tes amis de KIDDYTROC.</p><p>Allez, go !</p>");
define("AYUDA_VISIBLE","<p>Veux-tu que tous les enfants voient ce jouet ?</p><p>Tu pr&eacute;f&egrave;res peut-&ecirc;tre qu&rsquo;il reste invisible pendant quelques jours, le temps de savoir si tu veux vraiment le troquer ?</p><p>Si c&rsquo;est le cas, tu le verras dans la rubrique &quot;mes jouets&quot;, mais tu seras le seul &agrave; le voir, jusqu&rsquo;&agrave; ce que tu changes cette option.Donc, si tu veux que tout le monde puisse voir le jouet, coche cette case. Si au contraire tu veux qu&rsquo;il soit invisible, laisse-la vide. C&rsquo;est &agrave; toi de choisir !!</p>");
define("AYUDA_CONTRA","<p>Tu veux changer ton mot de passe? C&acute;est  possible!<br />R&eacute;fl&eacute;chis bien et &eacute;cris  un mot de passe de 6 lettres maximum dont tu te souviendras  facilement.<br /><br />Ne l&acute;oublies surtout pas!<br />Tu veux un truc ? &Eacute;cris dans un  carnet la question dont la r&eacute;ponse est ton mot de passe. Par exemple : comment  s&rsquo;appelle ton jouet favori ? De cette fa&ccedil;on, tu seras le seul &agrave;  conna&icirc;tre ton secret et tu ne l&rsquo;oublieras jamais.<br /><br />Super, non ?</p>");

define("PADRE_NAME","<p>Sous quel nom souhaitez-vous appara&icirc;tre sur KIDDYTROC ? Entrer un pseudonyme.</p>");
define("PADRE_REGISTRO","<b>LE MOT DE PASSE KIDDYTROC VIENT D'&Ecirc;TRE ENVOYE SUR VOTRE E-MAIL.<br /><br />Vous aurez besoin de votre mot de passe* et de votre Troc Name pour vous connecter sur votre compte Kiddytroc afin de vendre et d'acheter.<br /><br />F&Eacute;LICITATIONS !!</b><br /><br />Vous vous &ecirc;tes inscrit(e) avec succ&egrave;s sur KIDDYTROC: Vous pourrez y effectuer des achats; si vous souhaitez vendre un de vos objets, vous devrez ajouter une photo dans la section &quot;ajouter un article&quot; et suivre la proc&eacute;dure indiqu&eacute;e. C'est tr&egrave;s facile !<br /><br />* Lorsque vous serez dans votre compte KIDDYTROC, ''mes coordonn&eacute;es'': vous pourrez remplacer le mot de passe envoy&eacute; par mail par un mot de passe de votre choix, plus facile &agrave; retenir.<br /><br />A tr&egrave;s bient&ocirc;t,<br /><br />KING BUDDI");
define("PADRES_CONTRASENA","<p>Vous  pouvez changer de mot de passe &agrave; tout moment.<br /><br />Il  vous suffit de saisir un mot de 6 lettres maximum, que vous retiendrez  facilement.</p>");
define("PADRES_CONTACTAR","<p>CONTACTER LE VENDEUR</p>");
define("PADRES_ANTIGUO","De quand date cet article ?");
define("PADRES_ARTICULOS","<b>COMMENT  ALLEZ-VOUS?</b><br /><br />Voici  les articles que vous souhaitez vendre.<br /><br /><em>IMPORTANT:</em><br />Vous  pouvez, &agrave; tout moment modifier vos articles ou les &eacute;liminer de la liste.");
define("PADRES_FAVORITO","Voici la liste des articles que vous  avez pr&eacute;selectionn&eacute;s. Avant de d&eacute;cider si vous allez les acheter  ou les troquer, vous pouvez cliquer sur chaque article pour voir ses  caract&eacute;ristiques.");
define("PADRES_ADARTICULO","Il  est tr&egrave;s facile d' ajouter un article.<br /><br />Il vous suffit de compl&eacute;ter correctement cette page.");
define("PADRES_SUBIR","<p>Vous devez inclure une photo pour que les utilisateurs de KIDDYTROC puissent se faire une id&eacute;e de votre article.</p>");
define("PADRES_VISIBLE","<p>Souhaitez-vous  que cet article soit visible par les utilisateurs de KIDDYTROC?<br />Ou pr&eacute;f&eacute;rez-vous  que cet article reste invisible pendant quelques jours?<br />Si  vous souhaitez qu&rsquo;il soit  visible, cochez cette case.<br />Dans  le cas contraire ne la cochez pas: vous verrez votre article dans la section  &quot;mes articles&quot; mais les autres utilisateurs ne le verront pas, jusqu&acute;&agrave;  ce que vous le d&eacute;cidiez en cochant la case.</p>");
define("PADRES_ERROR","D&Eacute;SOL&Eacute;&nbsp;!!<br /><br />Vous  n'avez pas rempli tous les champs du formulaire.<br /><br />FAITES UN NOUVEL ESSAI&nbsp;!<br /><br />Les  champs manquants apparaissent en rouge.");
define("PADRES_REL_1","Si  un/e ami/e vous a recommand&eacute; KIDDYTROC, notez son TROC NAME ici");
define("PADRES_REL_2","IMPORTANT:<br /><br />Si un  membre de votre foyer est d&eacute;j&agrave; enregistr&eacute;, indiquez son TROC NAME pour que King  Buddi sache qu'il est de votre famille");
define("PADRES_AYUDA_VER","<p>Vous devez entrer le code de v&eacute;rification que nous vous fournissons, dans la case correspondante.</p>");

/*metas de la zona de ni�os, index y categorias principales*/
define("META_T_INDEX","KiddyTroc.fr - Troc et echanges articles pour enfants");
define("META_D_INDEX","KiddyTroc.com Petites annonces gratuites pour troquer et echanger accessoires et jouets d'enfants. Troc gratuit en ligne France, Belgique, Suisse, Europe");
define("META_K_INDEX","troc jouets enfants, troc jeux enfants, troc articles pour enfants, troquer articles pour enfants, troquer articles puericulture, troquer jouets enfants, echanger jouets, echanger jouets, petites annonces gratuites, petites annonces jouets");

define("META_T_N_CONTACTA","KiddyTroc.fr - Nous contacter");
define("META_T_N_REGISTRO","KiddyTroc.fr - Inscription pour enfants ");
define("META_T_N_LEGAL","KiddyTroc.fr - Avis Legal");
define("META_T_N_USERS","KiddyTroc.fr - Ajouter un Article");

define("META_T_N_CAT_1","KiddyTroc.fr - Troc et echanges de jouets de plein air ");
define("META_T_N_CAT_2","KiddyTroc.fr - Troc et echanges: beaute et bijoux fillettes");
define("META_T_N_CAT_3","KiddyTroc.fr - Troc et echanges: dinettes et jeux d'imitation");
define("META_T_N_CAT_4","KiddyTroc.fr - Troc et echanges: jeux et jouets de construction");
define("META_T_N_CAT_5","KiddyTroc.fr - Troc et echanges: deguisements enfants");
define("META_T_N_CAT_6","KiddyTroc.fr - Troc et echanges: jouets educatifs, jeux de role");
define("META_T_N_CAT_7","KiddyTroc.fr - Troc et echanges: personnages d'action");
define("META_T_N_CAT_8","KiddyTroc.fr - Troc et echanges: jouets musicaux");
define("META_T_N_CAT_9","KiddyTroc.fr - Troc et echanges: jeux pour consoles");
define("META_T_N_CAT_10","KiddyTroc.fr - Troc et echanges: jeux de societe");
define("META_T_N_CAT_11","KiddyTroc.fr - Troc et echanges: jeux pour ordinateur");
define("META_T_N_CAT_12","KiddyTroc.fr - Troc et echanges: livres et BD");
define("META_T_N_CAT_13","KiddyTroc.fr - Troc et echanges: jeux de travail manuel");
define("META_T_N_CAT_14","KiddyTroc.fr - Troc et echanges: poupees et peluches");
define("META_T_N_CAT_15","KiddyTroc.fr - Troc et echanges: films et musique");
define("META_T_N_CAT_16","KiddyTroc.fr - Troc et echanges: jouets telecommandes");
define("META_T_N_CAT_17","KiddyTroc.fr - Troc et echanges: vehicules");
define("META_T_N_CAT_18","KiddyTroc.fr - Troc et echanges: autres jouets");

define("META_D_N_CAT_1","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jeux et jouets de plein air: balan&ccedil;oires, velos... Troc en ligne en France, Belgique..");
define("META_D_N_CAT_2","KiddyTroc.com Petites annonces gratuites pour troquer et echanger accessoires de beaute et bijoux fillettes. Troc en ligne France, Belgique, Suisse, Europe");
define("META_D_N_CAT_3","KiddyTroc.com Petites annonces gratuites pour troquer et echanger dinettes, cuisinieres...Troc gratuit en ligne partout en France, Belgique, Espagne, Europe");
define("META_D_N_CAT_4","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jeux et jouets de construction. Troc gratuit en ligne partout en France, Belgique, Canada...");
define("META_D_N_CAT_5","KiddyTroc.com Petites annonces gratuites pour troquer et echanger deguisements pour enfants. Troc gratuit en ligne partout en France, Belgique, Europe...");
define("META_D_N_CAT_6","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jouets educatifs, jeux de role. Troc gratuit en ligne partout en France, Belgique, Suisse");
define("META_D_N_CAT_7","KiddyTroc.com Petites annonces gratuites pour troquer et echanger figurines Super Heros . Troc gratuit en ligne partout en France, Belgique, Suisse, Canada");
define("META_D_N_CAT_8","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jouets et jeux musicaux . Troc gratuit en ligne partout en France, Belgique, Suisse, Europe");
define("META_D_N_CAT_9","KiddyTroc.com Petites annonces gratuites pour echanger et troquer jeux pour consoles . Troc gratuit en ligne partout en France, Belgique, Suisse, Canada");
define("META_D_N_CAT_10","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jeux de societe. Troc gratuit en ligne partout en France, Belgique, Suisse, Europe");
define("META_D_N_CAT_11","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jeux video pour Mac et PC. Troc gratuit en ligne partout en France, Belgique, Suisse...");
define("META_D_N_CAT_12","KiddyTroc.com Petites annonces gratuites pour troquer et echanger livres et BD pour enfants. Troc gratuit en ligne partout en France, Belgique, Europe");
define("META_D_N_CAT_13","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jeux de loisirs creatifs pour enfants. Troc gratuit en ligne partout en France, Europe");
define("META_D_N_CAT_14","Kiddytroc.com Petites annonces gratuites pour troquer et echanger poupees et peluches pour enfants. Troc gratuit en ligne partout en France, Belgique, Europe");
define("META_D_N_CAT_15","KiddyTroc.com Petites annonces gratuites pour troquer et echanger musique, films pour enfants. Troc gratuit en ligne partout en France, Suisse, Europe");
define("META_D_N_CAT_16","KiddyTroc.com Petites annonces gratuites pour troquer et echanger jouets telecommandes. Troc gratuit en ligne partout en France, Belgique, Suisse, Europe");
define("META_D_N_CAT_17","KiddyTroc.com Petites annonces gratuites pour troquer et echanger petites voitures, velo, kart. Troc gratuit en ligne partout en France, Belgique, Europe");
define("META_D_N_CAT_18","KiddyTroc.com Petites annonces gratuites pour troquer et echanger accessoires et jouets d'enfants. Troc gratuit en ligne France, Suisse, Europe, Quebec..");

define("META_K_N_CAT_1","echange balan&ccedil;oire, echange skateboard, troc panier de basket, troc bicyclette enfant, troc velo enfant, troquer velo, troquer skateboard, echanger panier de basket, echanger on ballon, petites annonces gratuites, petites annonces jeux d'exterieurs");
define("META_K_N_CAT_2","echange bijoux fillettes, echange maquillage fillette, troc bijou enfant, troc bijoux enfant, troquer bijoux enfant, troquer bijou enfants, troquer maquillage fillette, echanger bijoux enfants, echanger maquillage fillette, petites annonces bijoux enfants, petites annonces gratuites");
define("META_K_N_CAT_3","echange dinette, echange dinettes, troc dinette, troc dinettes, troquer jouet cuisiniere, troquer dinette, petites annonces dinette enfants, troquer jeux d'imitation, echanger jeux d'imitation, petites annonces gratuites");
define("META_K_N_CAT_4","echange kapla, echange legos, troc lego, troc mega bloks, troquer jeux de construction, troquer lego, troc playmobil, petites annonces jeux de construction, petites annonces playmobil, petites annonces gratuites, petites annonces pour enfants");
define("META_K_N_CAT_5","echange deguisement de princesse, echange deguisement de pirate, troc deguisement de fee, troc deguisement de cendrillon, troquer deguisement enfants, troquer deguisement fillette, echanger deguisement de fee, echanger deguisements, petites annonces deguisements d'occasion, petites annonces gratuites");
define("META_K_N_CAT_6","echange jouets scientifiques, echange jouets educatifs, troc jeux de role, troc jeux educatif, troquer jeux scientifiques, troquer jeu de role, petites annonces jeux educatifs, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_7","echange galaxy defender, echange jouets transformers, troc hulk, troc action man, troquer poupees super heros, troquer figurines super heros, troquer figurine super heros, troquer figurine superman, troc batman, petites annonces super heros, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_8","echange tambour, echange xylophone, echange piano, troc tambour, troc guitare enfant, troquer piano enfant, troquer tambour, troquer guitare, petites annonces jouets musicaux, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_9","echange xbox, echange gamecube, echange dreamcast, troc playstation, troc wii, troquer wii, troquer dreamcast, troc jeu console, echanger wii, echanger playstation, petites annonces jeux console, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_10","echange scrable junior, echange cluedo junior, echange Maka Bana, troc flipper, troc bataille navale, troquer monopoli, troquer tic tac boum, echanger jeux de societe, echanger monopoli neuf contre, petites annonces jeux de societe, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_11","echange jeu PC, echange jeux PC, echange jeux Mac, troc jeux PC, troc jeux video, troquer jeux PC, troquer jeux Mac, echnager jeux Pc, echanger jeux pour Mac, petites annonces jeux video, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_12","echange bande dessinee, echange BD enfant, echange BD Tintin, troc BD, troc livres enfant, troquer BD, troquer bandes dessinees, echanger BD, petites annonces livres BD enfants, petites annonces BD, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_13","echange kit a peindre, echange peinture a doigt, echange ardoise magique, troc tube de gouache, troc papier origami, troquer tambourin tricotin, troquer tricotin, troquer ardoise magique, petites annonces loisirs creatifs, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_14","echange deglingos, echange poupee uglydoll, echange peluche baloo, echange winnie l'ourson, troc peluches mickey, troc poupees, troquer peluche minnie, troquer roi lion, troquer hello kitty, troquer uglydoll, petites annonces peluches et poupees, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_15","echange dvd dessin anime, echange cd musique pour enfants, echanger film walt disney, echanger dvd walt disney, troc film walt disney, troc dessins animes, troquer films pour enfants, troquer film super heros, troquer dessins animes, petites annonces dvd d'occasion, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_16","echange voiture RC, echange voiture telecommandee, echange helicoptere radio commande, echange avion telecommande, troc voiture radiocommandee, troc train telecommande, troquer avion RC, troquer jouets telecommandes, troquer jouets radio commandes, petites annonces jouets radio commandes, echanger voiture telecommandee, troc en ligne");
define("META_K_N_CAT_17","echange velo d'enfant, echange kart enfant, echange quad enfant, echange petites voitures, troc velo fillette, troc kart a pedales, troquer bicyclette enfant, troquer velo d'occasion, troquer camion de pompier, petites annonces petites voitures, echanger voitures miniatures, petites annonces gratuites, troc en ligne");
define("META_K_N_CAT_18","troc jouets enfants, troc jeux enfants, troc articles pour enfants, troquer vetements pour enfants, troquer articles puericulture, troquer jouets enfants, echanger jouets, echanger jeux enfants, petites annonces jouets d'occasion, petites annonces gratuites, troc en ligne");

/*metas de la zona de padres, index y categorias principales*/
define("META_T_PADRES","KiddyTroc.fr - Achat Vente Troc articles pour enfants");
define("META_D_PADRES","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer accessoires et jouets d'enfants. Troc gratuit en ligne France, Belgique...");
define("META_K_PADRES","achat vente jouets, achat vente jeux, achat vente articles enfants, troc jouets enfants, troc jeux enfants, troc articles pour enfants, troquer articles pour enfants, troquer articles puericulture, troquer jouets enfants");

define("META_T_REGISTRO","KiddyTroc.fr � Inscription pour parents");

define("META_T_CAT_1","KiddyTroc.fr - Achat Vente Troc jouets et jeux de plein air ");
define("META_T_CAT_2","KiddyTroc.fr - Achat Vente Troc: beaute et bijoux fillettes");
define("META_T_CAT_3","KiddyTroc.fr - Achat Vente Troc: dinettes et jeux d'imitation");
define("META_T_CAT_4","KiddyTroc.fr - Achat Vente Troc: jeux, jouets de construction");
define("META_T_CAT_5","KiddyTroc.fr - Achat Vente Troc: deguisements enfants");
define("META_T_CAT_6","KiddyTroc.fr - Achat Vente Troc: jouets educatifs, jeux de role");
define("META_T_CAT_7","KiddyTroc.fr - Achat Vente Troc: personnages d'action");
define("META_T_CAT_8","KiddyTroc.fr - Achat Vente Troc: jouets musicaux");
define("META_T_CAT_9","KiddyTroc.fr - Achat Vente Troc: jeux pour consoles");
define("META_T_CAT_10","KiddyTroc.fr - Achat Vente Troc: jeux de societe");
define("META_T_CAT_11","KiddyTroc.fr - Achat Vente Troc: jeux pour ordinateur");
define("META_T_CAT_12","KiddyTroc.fr - Achat Vente Troc: livres et BD ");
define("META_T_CAT_13","KiddyTroc.fr - Achat Vente Troc: travail manuel");
define("META_T_CAT_14","KiddyTroc.fr - Achat Vente Troc: poupees et peluches");
define("META_T_CAT_15","KiddyTroc.fr - Achat Vente Troc: films et musique");
define("META_T_CAT_16","KiddyTroc.fr - Achat Vente Troc: jouets telecommandes");
define("META_T_CAT_17","KiddyTroc.fr - Achat Vente Troc: vehicules");
define("META_T_CAT_18","KiddyTroc.fr - Achat Vente Troc: autres articles");
define("META_T_CAT_19","KiddyTroc.fr - Achat Vente Troc accessoires enfants: maison");
define("META_T_CAT_20","KiddyTroc.fr - Achat Vente Troc vetements enfants et bebes");
define("META_T_CAT_21","KiddyTroc.fr - Achat Vente Troc vetements futures mamans");
define("META_T_CAT_22","KiddyTroc.fr - Achat Vente Troc articles de promenade enfants");

define("META_D_CAT_1","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer jeux et jouets de plein air: balan&ccedil;oires, velos... Troquer en ligne en France...");
define("META_D_CAT_2","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer accessoires de beaute et bijoux fillettes. Troquer en ligne France, Belgique...");
define("META_D_CAT_3","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer dinettes, cuisinieres...Troc gratuit en ligne partout en France, Belgique, Espagne");
define("META_D_CAT_4","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer jeux et jouets de construction. Troc gratuit en ligne partout en France, Belgique");
define("META_D_CAT_5","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer deguisements pour enfants. Troc gratuit en ligne partout en France, Belgique...");
define("META_D_CAT_6","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et echanger jouets educatifs, jeux de role. Troc gratuit en ligne partout en France, Belgique");
define("META_D_CAT_7","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et echanger figurines Super Heros . Troc gratuit en ligne partout en France, Belgique, Suisse");
define("META_D_CAT_8","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et echanger jouets et jeux musicaux . Troc gratuit en ligne partout en France, Belgique...");
define("META_D_CAT_9","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer jeux pour consoles . Troc gratuit en ligne partout en France, Belgique, Suisse");
define("META_D_CAT_10","KiddyTroc.com Petites annonces gratuites pour acheter, vendre, troquer et echanger jeux de societe. Troc gratuit en ligne partout en France, Belgique...");
define("META_D_CAT_11","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer jeux video pour Mac et PC. Troc gratuit en ligne partout en France, Suisse...");
define("META_D_CAT_12","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et echanger livres et BD pour enfants. Troc gratuit en ligne partout en France, Belgique...");
define("META_D_CAT_13","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer jeux de loisirs creatifs pour enfants. Troc gratuit en ligne partout en France...");
define("META_D_CAT_14","Kiddytroc.com Petites annonces gratuites pour acheter, vendre et troquer poupees et peluches pour enfants. Troc gratuit en ligne partout en France, Belgique");
define("META_D_CAT_15","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et echanger musique, films pour enfants. Troc gratuit en ligne partout en France, Suisse...");
define("META_D_CAT_16","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer jouets telecommandes. Troc gratuit en ligne partout en France, Belgique, Suisse");
define("META_D_CAT_17","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer petites voitures, velo, kart. Troc gratuit en ligne partout en France, Belgique...");
define("META_D_CAT_18","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et echanger des accessoires et jouets d'enfants. Troc gratuit en ligne France, Suisse...");
define("META_D_CAT_19","KiddyTroc.com Acheter, vendre, troquer accessoires pour bebes: trotteurs, berceau, chaise bebe, table a langer, parc. Troc gratuit en ligne France...");
define("META_D_CAT_20","KiddyTroc.com Petites annonces gratuites pour acheter, vendre, troquer et echanger vetements d'enfants et de bebes. Troc gratuit en ligne France...");
define("META_D_CAT_21","KiddyTroc.com Petites annonces gratuites pour acheter, vendre, troquer et echanger des vetements de grossesse. Troc gratuit en ligne France, Belgique...");
define("META_D_CAT_22","KiddyTroc.com Petites annonces gratuites pour acheter, vendre et troquer accessoires de promenade et de voyage pour enfants et bebes: sieges auto, landau");

define("META_K_CAT_1","achat vente jeux d'exterieur, achat vente velo d'occasion, echange balan&ccedil;oire, echange skateboard, troc panier de basket, troc bicyclette enfant, troc velo enfant, troquer velo, troquer skateboard, echanger panier de basket");
define("META_K_CAT_2","achat vente maquillage fillette, achat vente bijoux fillettes, echange bijoux fillettes, echange maquillage fillette, troc bijou enfant, troc bijoux enfant, troquer bijoux enfant, troquer bijou enfants, petites annonces bijoux enfants");
define("META_K_CAT_3","achat vente dinette, achat vente dinettes, echange dinette, echange dinettes, troc dinette, troc dinettes, troquer jouet cuisiniere, troquer dinette, petites annonces dinette enfants, vendre jeux d'imitation, acheter jeux d'imitation, echanger jeux d'imitation");
define("META_K_CAT_4","achat vente lego, achat vente playmobil, echange kapla, echange legos, troc lego, troc mega bloks, troquer jeux de construction, troquer lego, petites annonces jeux de construction, petites annonces playmobil");
define("META_K_CAT_5","achat vente deguisement enfants, achat vente deguisements fillettes, echange deguisement de princesse, echange deguisement de pirate, troc deguisement de fee, troc deguisement de cendrillon, troquer deguisement enfants, troquer deguisement fillette, petites annonces deguisements d'occasion");
define("META_K_CAT_6","achat vente jeux educatifs, achat vente jeux de roles, echange jouets scientifiques, echange jouets educatifs, troc jeux de role, troc jeux educatif, troquer jeux scientifiques, troquer jeu de role, petites annonces jeux educatifs");
define("META_K_CAT_7","achat vente figurines super heros, achat vente superman, echange galaxy defender, echange jouets transformers, troc hulk, troc action man, troquer poupees super heros, troquer figurines super heros, petites annonces super heros");
define("META_K_CAT_8","achat vente jouets musicaux, achat vente jeux musicaux, echange tambour, echange xylophone, echange piano, troc tambour, troc guitare enfant, troquer piano enfant, troquer tambour, petites annonces jouets musicaux");
define("META_K_CAT_9","achat vente jeux console, achat vente playstation 3, echange xbox, echange gamecube, echange dreamcast, troc playstation, troc wii, troquer wii, troquer dreamcast, petites annonces jeux console");
define("META_K_CAT_10","achat vente jeux de societe, achat vente scrable, echange scrable junior, echange cluedo junior, echange Maka Bana, troc flipper, troc bataille navale, troquer monopoli, troquer tic tac boum, petites annonces jeux de societe");
define("META_K_CAT_11","achat vente jeux pour ordinateur, achat vente jeux video, echange jeu PC, echange jeux PC, echange jeux Mac, troc jeux PC, troc jeux video, troquer jeux PC, troquer jeux Mac, petites annonces jeux video");
define("META_K_CAT_12","achat vente livres enfants, achat vente livre enfant, echange bande dessinee, echange BD enfant, echange BD Tintin, troc BD, troc livres enfant, troquer BD, troquer bandes dessinees, petites annonces livres BD enfants");
define("META_K_CAT_13","achat vente puzzles enfants, achat vente jeu de perles, echange kit a peindre, echange peinture a doigt, echange ardoise magique, troc tube de gouache, troc papier origami, troquer tambourin tricotin, troquer tricotin, petites annonces loisirs creatifs");
define("META_K_CAT_14","achat vente peluches enfants, achat vente poupees, echange deglingos, echange poupee uglydoll, echange peluche baloo, echange winnie l'ourson, troc peluches mickey, troc poupees, troquer peluche minnie, troquer roi lion, troquer hello kitty, petites annonces peluches et poupees");
define("META_K_CAT_15","achat vente films pour enfants, achat vente musique pour enfants, echange dvd dessin anime, echange cd musique pour enfants, troc film walt disney, troc dvd walt disney, troquer films pour enfants, troquer film super heros, petites annonces dvd d'occasion");
define("META_K_CAT_16","achat vente jouets radiocommandes, achat vente jouets telecommandes, echange voiture RC, echange voiture telecommandee, echange helicoptere radio commande, echange avion telecommande, troc voiture radiocommandee, troc train telecommande, troquer avion RC, troquer jouets telecommandes, troquer jouets radio commandes, petites annonces jouets radio commandes");
define("META_K_CAT_17","achat vente vehicules jouets, achat vente kart enfants, echange velo d'enfant, echange kart enfant, echange quad enfant, echange petites voitures, troc velo fillette, troc kart a pedales, troquer bicyclette enfant, troquer velo d'occasion, troquer camion de pompier, petites annonces petites voitures");
define("META_K_CAT_18","achat vente jouets, achat vente jeux, achat vente articles enfants, troc jouets enfants, troc jeux enfants, troc articles pour enfants, troquer vetements pour enfants, troquer articles puericulture, troquer jouets enfants");
define("META_K_CAT_19","achat vente meubles bebes, acheter lits enfant d'occasion, vendre chaise bebe d'occasion, troc berceau enfant, troc table a langer, echanger table a langer, troquer trotteur, troc trotteur, echanger accessoires puericulture, petites annonces puericulture");
define("META_K_CAT_20","achat vente vetements enfants, achat vente vetements bebes, achat vente habits enfants, troc habits bebes, troc vetements enfants, troc habits enfants, troquer vetements enfants, troquer vetements bebe, echanger vetements enfants");
define("META_K_CAT_21","achat vente vetements de grossesse, achat vente vetements prenatal, achat vente habits grossesse, troc habits grossesse, troc vetements future maman, troc vetements grossesse, troquer vetements prenatal, troquer vetements grossesse, echanger vetements future maman, troc vetement femme enceinte");
define("META_K_CAT_22","achat vente poussette, achat vente landau, echange poussette, echange rehausseur auto, troc poussette, troc landau, troc siege voiture bebe, troquer poussette, troquer landau, echanger poussettes bebe");
?>