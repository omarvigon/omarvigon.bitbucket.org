<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php"); ?>
	<title><?=META_T_N_REGISTRO;?></title>
    <meta name="title" content="<?=META_T_N_REGISTRO;?>" />
    <meta name="description" content="<?=META_T_N_REGISTRO;?>" />
    <meta name="keywords" content="<?=META_T_N_REGISTRO;?>" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>

    <div class="cuerpo">
		<?php
		if($_SESSION["user_nombre"])
			echo "<p>".TXT_YAESTAS."</p>";
		else
		{
		
		if($_POST["correo"])
			nuevo_usuario();
		if($_SESSION["final"]!=true) { ?>
        	<div id="avisoko"><p><br /><?=ALERT_ERROR;?></p></div>
            <h6><?=DATO_NINO;?></h6>
            <div class="bajoh6">
            	<p>&nbsp;</p>
                <form method="post" action="http://<?=$_SERVER['SERVER_NAME']?>/register.php" name="form_registro" />
            	<table class="tabla_registro">
                <tr>
                	<td><label><?=DATO_NOM_NINO;?> (*):</label><br />
            		<input type="text" style="width:220px;" maxlength="32" name="nombre_nino" value="<?=$_POST["nombre_nino"]?>"/></td>
                    <td><label><?=DATO_NOM_PADRE;?> (*):</label><br />
            		<input type="text" size="36" maxlength="32" name="nombre_padre" value="<?=$_POST["nombre_padre"]?>"/></td>
                </tr>
                <tr>
                	<td><label><?=DATO_APELLIDOS;?> (*):</label><br />
            		<input type="text" style="width:220px;" maxlength="32" name="apellidos" value="<?=$_POST["apellidos"]?>"/></td>
                    <td><?=DATO_DIRECCION;?> :<br />
            		<input type="text" size="36" maxlength="32" name="direccion" value="<?=$_POST["direccion"]?>"/></td>
                </tr>
                <tr>
                	<td><label><?=DATO_POBLACION;?> (*):</label><br />
            		<input type="text" style="width:220px;" maxlength="32" name="poblacion" value="<?=$_POST["poblacion"]?>"/></td>
                    <td><?=DATO_CP;?> :<br />
            		<input type="text" style="width:120px;" maxlength="8" name="cp" value="<?=$_POST["cp"]?>"/></td>
                </tr>
                <tr>
                	<td><label><?=DATO_NACIMIENTO;?> (*):</label><br />
            		<select name='nacimiento_dia'>
            		<option value='00'><?=FECHA_DIA;?></option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option></select>
            		<select name='nacimiento_mes'>
            		<option value='00'><?=FECHA_MES;?></option><option value='01'><?=MES_1;?></option><option value='02'><?=MES_2;?></option><option value='03'><?=MES_3;?></option><option value='04'><?=MES_4;?></option><option value='05'><?=MES_5;?></option><option value='06'><?=MES_6;?></option><option value='07'><?=MES_7;?></option><option value='08'><?=MES_8;?></option><option value='09'><?=MES_9;?></option><option value='10'><?=MES_10;?></option><option value='11'><?=MES_11;?></option><option value='12'><?=MES_12;?></option></select>
            		<select name='nacimiento_ano'>
            		<option value='0000'><?=FECHA_ANO;?></option><option value='1992'>1992</option><option value='1993'>1993</option><option value='1994'>1994</option><option value='1995'>1995</option><option value='1996'>1996</option><option value='1997'>1997</option><option value='1998'>1998</option><option value='1999'>1999</option><option value='2000'>2000</option><option value='2001'>2001</option><option value='2002'>2002</option><option value='2003'>2003</option><option value='2004'>2004</option></select></td>
                    <td><?=DATO_SEXO;?> :<br />
           			<select name="sexo"><option value="1"><?=DATO_SEXO_1;?></option><option value="0"><?=DATO_SEXO_0;?></option></select></td>
                </tr>
                <tr>
                    <td><label><?=DATO_PAIS;?> (*):</label><br />
            			<select name="pais" onchange="poner_provincias()">
            			<option value="no" selected="selected"><?=DATO_PAIS;?></option>
            			<?php include("includes/inc.paises.php");?>
            			</select></td>
            		<td><?=DATO_PROVINCIA;?> (*):<br />
            		<span id="aux_prov">&nbsp;</span></td>
                </tr>
                <tr>
                	<td><?=DATO_TFN;?> :<br />
            		<input type="text" style="width:220px;" maxlength="32" name="tel" value="<?=$_POST["tel"]?>"/></td>
                    <td><label><?=DATO_EMAIL;?> (*):</label><br />
            		<input type="text" size="36" maxlength="32" name="correo" value="<?=$_POST["correo"]?>"/></td>
                </tr>
                <tr>
                	<td><label>Troc Name (*):</label> <a href="#" onMouseout="hideddrivetip()" onMouseover="ddrivetip('<?=AYUDA_NAME;?>')"><?=XKB2;?></a><br />
            		<input type="text" sstyle="width:220px;" maxlength="16" name="avatar_nombre" value="<?=$_POST["avatar_nombre"]?>" /></td>
                    <td><?=DATO_COMENTARIO;?> :<br />
            		<textarea name="avatar_desc" style="width:220px;" rows="4"><?=$_POST["avatar_desc"]?></textarea></td>
                </tr>
                <tr>
                	<td>(*) <?=DATO_OBLIGATORIO;?></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
                <p>&nbsp;</p>
            </div>
                
            <h6><?=DATO_IMAGEN;?> :</h6>
            <div class="bajoh6_2">
            <input type="hidden" name="avatar_img" value="0" />
            <div align="center"><br /><cite id="img_elegida">&nbsp;</cite><br /><br /></div>
            <p id="img_oculto">
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av1.gif" alt="Kiddytroc Avatar" onclick="elegir(1)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av2.gif" alt="Kiddytroc Avatar" onclick="elegir(2)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av3.gif" alt="Kiddytroc Avatar" onclick="elegir(3)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av4.gif" alt="Kiddytroc Avatar" onclick="elegir(4)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av5.gif" alt="Kiddytroc Avatar" onclick="elegir(5)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av6.gif" alt="Kiddytroc Avatar" onclick="elegir(6)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av7.gif" alt="Kiddytroc Avatar" onclick="elegir(7)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av8.gif" alt="Kiddytroc Avatar" onclick="elegir(8)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av9.gif" alt="Kiddytroc Avatar" onclick="elegir(9)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av10.gif" alt="Kiddytroc Avatar" onclick="elegir(10)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av11.gif" alt="Kiddytroc Avatar" onclick="elegir(11)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av12.gif" alt="Kiddytroc Avatar" onclick="elegir(12)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av13.gif" alt="Kiddytroc Avatar" onclick="elegir(13)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av14.gif" alt="Kiddytroc Avatar" onclick="elegir(14)"/>
    		<img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av15.gif" alt="Kiddytroc Avatar" onclick="elegir(15)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av16.gif" alt="Kiddytroc Avatar" onclick="elegir(16)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av17.gif" alt="Kiddytroc Avatar" onclick="elegir(17)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av18.gif" alt="Kiddytroc Avatar" onclick="elegir(18)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av19.gif" alt="Kiddytroc Avatar" onclick="elegir(19)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av20.gif" alt="Kiddytroc Avatar" onclick="elegir(20)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av21.gif" alt="Kiddytroc Avatar" onclick="elegir(21)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av22.gif" alt="Kiddytroc Avatar" onclick="elegir(22)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av23.gif" alt="Kiddytroc Avatar" onclick="elegir(23)"/>
            <img title="Avatar Kiddytroc" src="http://www.kiddytroc.com/img/avatar/av24.gif" alt="Kiddytroc Avatar" onclick="elegir(24)"/>
            </p>
            </div>
            
			<table class="tabla_registro2" cellpadding="0" cellspacing="0">
            <tr>
            	<td width="260"><img src="http://www.kiddytroc.com/img/fondo_i260.gif" alt=""/></td>
                <td width="30">&nbsp;</td>
                <td width="260"><img src="http://www.kiddytroc.com/img/fondo_i260.gif" alt=""/></td>
            </tr>
            <tr>
            	<td class="amarillo_reg"><?=TXT_REL_1;?> :</td>
                <td>&nbsp;</td>
                <td class="amarillo_reg"><?=TXT_REL_2;?> :</td>
            </tr>
            <tr>
            	<td class="amarillo_reg"><input type="text" style="width:220px;" maxlength="24" name="rel_amigo" value="<?=$_POST["rel_amigo"]?>"/></td>
                <td>&nbsp;</td>
                <td class="amarillo_reg"><input type="text" style="width:220px;" maxlength="24" name="rel_familia" value="<?=$_POST["rel_familia"]?>"/></td>
            </tr>
            <tr>
            	<td><img src="http://www.kiddytroc.com/img/fondo_d260.gif" alt=""/></td>
                <td>&nbsp;</td>
                <td><img src="http://www.kiddytroc.com/img/fondo_d260.gif" alt=""/></td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right">
                <iframe class="iframe_captcha" src="privado/funciones_captcha.php" frameborder="0"></iframe>
                <p><a href="#" onMouseout="hideddrivetip()" onMouseover="ddrivetip('<?=AYUDA_VERIFICA;?>')"><?=XKB;?></a> 
                <label><?=DATO_VERIFICA;?></label><br />
                <input type="text" size="18" maxlength="6" name="verificacion"></p>
                <p><input type="checkbox" name="aceptarlegal" style="margin:-4px 0 0 0;"> <a title="<?=ZONA_LEGAL;?>" href="legal.php" style="color:#000000;" target="_blank">
                <label><?=ZONA_LEGAL2;?></label></a></p>
                <p><a title="<?=ZONA_REGISTRO;?>" href="javascript:validar()"><img src="http://www.kiddytroc.com/img/bt_registrate_<?=substr($_SERVER['SERVER_NAME'],0,2)?>.gif" border="0" alt="<?=ZONA_REGISTRO;?>"></a></p>
                </td>
            </tr>
            </table>
            </form>
        <? } $_SESSION["final"]=false; } ?>	
    </div>
     
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php"); ?>