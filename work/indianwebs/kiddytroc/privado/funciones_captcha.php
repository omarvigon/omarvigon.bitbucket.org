<?php
session_start();
$string = substr(md5(microtime() * mktime()),0,5);
$captcha = imagecreatefrompng("../img/captcha.png");
//Configuramos los colores usados para generan las lineas (formato RGB)
$black = imagecolorallocate($captcha, 0, 0, 0);
$line = imagecolorallocate($captcha,128,128,128);

imageline($captcha,0,0,39,29,$line);
imageline($captcha,40,0,64,29,$line);
imageline($captcha,80,0,12,24,$line);
//Ahora escribimos la cadena generada aleatoriamente en la imagen
imagestring($captcha, 5, 20, 10, $string, $black);
//Encriptamos y almacenamos el valor en una variable de sesion.Devolvemos la imagen para mostrarla*
$_SESSION["verificacion"] = $string;
header("Content-type: image/png");
imagepng($captcha);
?>