<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php"); ?>
	<title>Kiddytroc.<?=$idioma?> <?=SITEMAP;?></title>
    <meta name="title" content="Kiddytroc <?=META_T_PADRES;?>" />
    <meta name="description" content="Kiddytroc <?=META_T_PADRES;?>" />
    <meta name="keywords" content="Kiddytroc <?=META_T_PADRES;?>" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>

    <div class="cuerpo">
        <h3>KIDDYTROC</h3>
        
        <p><a href="index.php"><?=ZONA_PADRES;?></a></p>
        <ul>
        	<li><a href="index.php"><?=LO_ULTIMO2;?></a></li>
        	<?php cargar_categorias("padres"); ?>
        </ul>
        
		<p><a href="../index.php"><?=ZONA_NINOS;?></a></p>
        <ul>
        	<li><a href="../index.php"><?=LO_ULTIMO2;?></a></li>
        	<?php cargar_categorias("ninos"); ?>
        </ul>
        
        <p><a href="register.php"><?=ZONA_REGISTRO;?></a></p>
        <ul>
        	<li><a href="forget.php"><?=OLVIDADO_1;?></a></li>
            <li><a href="forget.php"><?=OLVIDADO_2;?></a></li>
        </ul>
        <p><a href="faq.php">F.A.Q.</a></p>
       	<p><a href="legal.php"><?=ZONA_LEGAL;?></a></p>
        <p><a href="contact.php"><?=ZONA_CONTACTO;?></a></p>
    </div>
    
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php"); ?>