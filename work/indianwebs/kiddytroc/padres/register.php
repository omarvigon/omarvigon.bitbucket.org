<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php"); ?>
	<title><?=META_T_REGISTRO;?></title>
    <meta name="title" content="<?=META_T_REGISTRO;?>" />
    <meta name="description" content="<?=META_T_REGISTRO;?>" />
    <meta name="keywords" content="<?=META_T_REGISTRO;?>" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>

    <div class="cuerpo">
		<?php
		if($_SESSION["user_nombre"])
			echo "<p>".TXT_YAESTAS."</p>";
		else
		{
		
		if($_POST["correo"])
			nuevo_usuario();
		if($_SESSION["final"]!=true) { ?>
        	<div id="avisoko"><p><br /><?=PADRES_ERROR;?></p></div>
        	<h6><?=ZONA_DATOS;?></h6>
            <div class="bajoh6">
            	<p>&nbsp;</p>
                <form method="post" action="register.php" name="form_registro">
            	<table class="tabla_registro">
                <tr>
                  	<td><label><?=DATO_NOMBRE;?> (*) :</label><br />
            		<input type="text" size="36" maxlength="32" name="nombre_padre" value="<?=$_POST["nombre_padre"]?>"/></td>
                	<td><?=DATO_DIRECCION;?> :<br />
            		<input type="text" size="36" maxlength="32" name="direccion" value="<?=$_POST["direccion"]?>"/>
                    <input type="hidden" name="nombre_nino" value="no"/></td>
                </tr>
                <tr>
                	<td><label><?=DATO_APELLIDOS;?> (*):</label><br />
            		<input type="text" size="36" maxlength="32" name="apellidos" value="<?=$_POST["apellidos"]?>"/></td>
                    <td><?=DATO_CP;?> :<br />
            		<input type="text" size="12" maxlength="8" name="cp" value="<?=$_POST["cp"]?>"/></td>
                </tr>
                <tr>
                	<td><?=DATO_POBLACION;?> (*):<br />
            		<input type="text" size="36" maxlength="32" name="poblacion" value="<?=$_POST["poblacion"]?>"/></td>
                    <td><?=DATO_SEXO_PADRES;?> :<br />
           			<select name="sexo"><option value="1"><?=DATO_SEXO_PADRES1;?></option><option value="0"><?=DATO_SEXO_PADRES0;?></option></select></td>
                </tr>
                <tr>
                	<td><label><?=DATO_NACIMIENTO;?> (*):</label><br />
            		<select name='nacimiento_dia'>
            		<option value='0'><?=FECHA_DIA;?></option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option></select>
            		<select name='nacimiento_mes'>
            		<option value='00'><?=FECHA_MES;?></option><option value='01'><?=MES_1;?></option><option value='02'><?=MES_2;?></option><option value='03'><?=MES_3;?></option><option value='04'><?=MES_4;?></option><option value='05'><?=MES_5;?></option><option value='06'><?=MES_6;?></option><option value='07'><?=MES_7;?></option><option value='08'><?=MES_8;?></option><option value='09'><?=MES_9;?></option><option value='10'><?=MES_10;?></option><option value='11'><?=MES_11;?></option><option value='12'><?=MES_12;?></option></select>
            		<select name='nacimiento_ano'>
            		<option value='0000'><?=FECHA_ANO;?></option>
                    <option value='1930'>1930</option><option value='1931'>1931</option><option value='1932'>1932</option><option value='1933'>1933</option><option value='1934'>1934</option><option value='1935'>1935</option><option value='1936'>1936</option><option value='1937'>1937</option><option value='1938'>1938</option><option value='1939'>1939</option><option value='1940'>1940</option><option value='1941'>1941</option><option value='1942'>1942</option><option value='1943'>1943</option><option value='1944'>1944</option><option value='1945'>1945</option><option value='1946'>1946</option><option value='1947'>1947</option><option value='1948'>1948</option><option value='1949'>1949</option>
                    <option value='1950'>1950</option><option value='1951'>1951</option><option value='1952'>1952</option><option value='1953'>1953</option><option value='1954'>1954</option><option value='1955'>1955</option><option value='1956'>1956</option><option value='1957'>1957</option><option value='1958'>1958</option><option value='1959'>1959</option><option value='1960'>1960</option><option value='1961'>1961</option><option value='1962'>1962</option><option value='1963'>1963</option><option value='1964'>1964</option><option value='1965'>1965</option><option value='1966'>1966</option><option value='1967'>1967</option><option value='1968'>1968</option><option value='1969'>1969</option>
  					<option value='1970'>1970</option><option value='1971'>1971</option><option value='1972'>1972</option><option value='1973'>1973</option><option value='1974'>1974</option><option value='1975'>1975</option><option value='1976'>1976</option><option value='1977'>1977</option><option value='1978'>1978</option><option value='1979'>1979</option><option value='1980'>1980</option><option value='1981'>1981</option><option value='1982'>1982</option><option value='1983'>1983</option><option value='1984'>1984</option><option value='1985'>1985</option><option value='1986'>1986</option><option value='1987'>1987</option><option value='1988'>1988</option><option value='1989'>1989</option>
                    <option value='1990'>1990</option><option value='1991'>1991</option></select></td>
                    <td>&nbsp;</td>
                </tr>    
                    <td><label><?=DATO_PAIS;?> (*):<br /></label>
            			<select name="pais" onchange="poner_provincias()">
            			<option value="no" selected="selected"><?=DATO_PAIS;?></option>
            			<?php include("../includes/inc.paises.php");?>
            			</select></td>
                	<td><?=DATO_PROVINCIA;?> (*):<br />
            		<span id="aux_prov">&nbsp;</span></td>
                    </td>
                </tr>
                <tr>
                	<td><label><?=DATO_EMAIL;?> (*):</label><br />
            		<input type="text" size="36" maxlength="32" name="correo" value="<?=$_POST["correo"]?>"/>
                	<td><?=DATO_TFN;?> :<br />
            		<input type="text" size="36" maxlength="32" name="tel" value="<?=$_POST["tel"]?>"/></td>
                    <td></td>
                </tr>
                <tr>
                	<td><label>Troc Name (*):</label> <a href="#" onMouseout="hideddrivetip()" onMouseover="ddrivetip('<?=PADRE_NAME;?>')"><?=XKB2;?></a><br />
            		<input type="text" size="36" maxlength="32" name="avatar_nombre" value="<?=$_POST["avatar_nombre"]?>" /></td>
                    <td><?=DATO_COMENTARIO;?> :<br />
            		<textarea name="comentario" cols="36" rows="4"><?=$_POST["comentario"]?></textarea>
            		<input type="hidden" name="avatar_desc" value="no"/></td>
                </tr>
                <tr>
                	<td>(*) <?=DATO_OBLIGATORIO;?></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
                <p>&nbsp;</p>
            </div>
            <input type="hidden" name="avatar_img" value="0" />
            
			<table class="tabla_registro2" cellpadding="0" cellspacing="0" >
            <tr>
            	<td width="260"><img src="img/fondo_i260.gif" alt=""/></td>
                <td width="30">&nbsp;</td>
                <td width="260"><img src="img/fondo_i260.gif" alt=""/></td>
            </tr>
            <tr>
            	<td class="yell"><?=PADRES_REL_1;?> :</td>
                <td>&nbsp;</td>
                <td class="yell"><?=PADRES_REL_2;?> :</td>
            </tr>
            <tr>
            	<td class="yell"><input type="text" size="32" maxlength="24" name="rel_amigo" value="<?=$_POST["rel_amigo"]?>"/></td>
                <td>&nbsp;</td>
                <td class="yell"><input type="text" size="32" maxlength="24" name="rel_familia" value="<?=$_POST["rel_familia"]?>"/></td>
            </tr>
            <tr>
            	<td><img src="img/fondo_d260.gif" alt=""/></td>
                <td>&nbsp;</td>
                <td><img src="img/fondo_d260.gif" alt=""/></td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right">
                <p>&nbsp;</p><p>&nbsp;</p>
                <p>&nbsp;</p><p>&nbsp;</p>
                <p>&nbsp;</p><p>&nbsp;</p>
                <iframe class="iframe_captcha" src="../privado/funciones_captcha.php" frameborder="0"></iframe>
                <p><a href="#" onMouseout="hideddrivetip()" onMouseover="ddrivetip('<?=PADRES_AYUDA_VER;?>')"><?=XKB;?></a> <label><?=DATO_VERIFICA_PADRES;?></label><br /><input type="text" size="18" maxlength="6" name="verificacion"></p>
                <p><input type="checkbox" name="aceptarlegal" style="margin:-4px 0 0 0;"><a title="<?=ZONA_LEGAL;?>" href="legal.php" target="_blank"><?=ZONA_LEGAL2;?></a></p>
                <p><a title="<?=ZONA_REGISTRO;?>" href="javascript:validar_padres()"><img src="img/bt_registrate_<?=substr($_SERVER['SERVER_NAME'],0,2)?>.gif" border="0" alt="<?=ZONA_REGISTRO;?>"></a></p>
                </td>
            </tr>
            </table>
            </form>
        <? } 
		$_SESSION["final"]=false;
		} ?>	
    </div>
     
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php"); ?>