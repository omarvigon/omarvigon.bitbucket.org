<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php"); ?>
	<title>Kiddytroc.<?=$idioma?> <?=SITEMAP;?></title>
    <meta name="title" content="Kiddytroc Site Web" />
    <meta name="description" content="Kiddytroc Site Web" />
    <meta name="keywords" content="Kiddytroc Site Web" />
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/2cabecera.php");?>

    <div class="cuerpo">
        <h3>KIDDYTROC <?=SITEMAP;?></h3>
        <p><a title='Kiddytroc' href='http://<?=$_SERVER['SERVER_NAME']?>'>Kiddytroc</a> &raquo; <a href='<?=$_SERVER['PHP_SELF']?>'><?=SITEMAP?></a></p>
        
		<p><a href="index.php"><?=ZONA_NINOS;?></a></p>
        <ul>
        	<li><a href="index.php"><?=LO_ULTIMO2;?></a></li>
        	<?php cargar_categorias("ninos"); ?>
        </ul>    
        
        <p><a href="padres/index.php"><?=ZONA_PADRES;?></a></p>
        <ul>
        	<li><a href="padres/index.php"><?=LO_ULTIMO2;?></a></li>
        	<?php cargar_categorias("padres"); ?>
         </ul>   
        
        <p><a href="register.php"><?=ZONA_REGISTRO;?></a></p>
        <ul>
        	<li><a href="forget.php"><?=OLVIDADO_1;?></a></li>
            <li><a href="forget.php"><?=OLVIDADO_2;?></a></li>
        </ul>
        <p><a href="faq.php">F.A.Q.</a></p>
       	<p><a href="legal.php"><?=ZONA_LEGAL;?></a></p>
        <p><a href="contact.php"><?=ZONA_CONTACTO;?></a></p>
        <p><a href="links.php"><?=ENLACES;?></a></p>
    </div>
    
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/3piecera.php"); ?>