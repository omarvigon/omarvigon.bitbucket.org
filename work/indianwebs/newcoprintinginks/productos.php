<?php include("includes/0cabecera.php"); 
$camino=explode("/",$_SERVER["PHP_SELF"]); 
for($i=0;$i<(count($camino)-1);$i++)
{
	$camino_enlace.=$camino[$i]."/";
	$camino_txt.="<a href='http://www.newcoprintinginks.com".$camino_enlace."'>".$camino[$i]."</a> &raquo; ";
	$camino_titulo.=strtoupper($camino[$i])." ";
}
?>
    <title><?=$camino_titulo;?> Newco Printing Inks</title>
	<meta name="description" content="Barnices, Tintas, productos auxiliares, barnices de sobreimpresion, barnices acrilicos, barnices uv, tintas convencionales, tintas especiales, flexo base agua, offset convencional, tintas uvProductos NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="Barnices, Tintas, productos auxiliares, barnices de sobreimpresion, barnices acrilicos, barnices uv, tintas convencionales, tintas especiales, flexo base agua, offset convencional, tintas uv" />

<?php include("includes/1cabecera.php"); ?>
   
    <div id="productos">
		<h1><?=$camino_txt;?></h1>
        <div class="mini">
     		<h5>&iquest;Necesita m&aacute;s informaci&oacute;n?</h5>
            <form method="post" action="<?=$_SERVER['PHP_SELF']?>" target="_blank">
       		<input type="hidden" name="ruta" value="<?=$_SERVER['SCRIPT_NAME'];?>"/>
            <textarea name="comentarios" rows="14"></textarea>
       		<label>E-Mail:</label>
            <input type="text" name="email" class="bte" maxlength="64"/>
            <input type="submit" value="   ENVIAR   " />
            </form>
     	</div>
        
        <div class="derecha">
        <?php if($_POST["email"]) 
		{ 	
			foreach($_POST as $nombre_campo => $valor)
 				$cuerpo=$cuerpo."<b>".$nombre_campo ."</b> : ".$valor."<br>";
			if(mail("newco@newcoprintinginks.com","Informacion de productos web",$cuerpo,"MIME-Version:1.0\r\nContent-type:text/html\r\nBcc:omar@indianwebs.com\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
				echo "<p>&nbsp;</p><p>Su petici&oacute;n se ha enviado.</p><p>Nos pondremos en contacto con usted.</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";
		}
		?>
        
		<h4><a href="http://www.newcoprintinginks.com/productos/barnices/">BARNICES</a></h4>
        <?php if(strstr($_SERVER['PHP_SELF'],"/barnices/")) { ?>
        <ul class="lista">
            <li><a href="http://www.newcoprintinginks.com/productos/barnices/sobreimpresion/">Barnices de sobreimpresi&oacute;n</a></li>
				<?php if(strstr($_SERVER['PHP_SELF'],"/barnices/sobreimpresion/")) { ?>
                
                <?php if($_SERVER['PHP_SELF']=="/productos/barnices/sobreimpresion/index.php") { ?>
                <div class="intro1">Los barnices  de sobreimpresi&oacute;n son <b>de naturaleza  grasa</b>, adecuados para la <b>impresi&oacute;n  en Offset convencional</b>, especialmente indicados para <b>incrementar el efecto de brillo o mateado</b> de la impresi&oacute;n previa y  para <b>protegerla del roce y del rayado</b></div>
				<div class="intro1">Est&aacute;n especialmente  dise&ntilde;ados para la impresi&oacute;n en m&aacute;quinas de 5 y 6 cuerpos y para imprimir h&uacute;medo  sobre h&uacute;medo.</div>
                <div class="intro1">Newco Printing Inks dispone de tres tipos  de barnices:</div>	
                <? } ?>
                
                <ul class="sublista">
			    <li><a href="http://www.newcoprintinginks.com/productos/barnices/sobreimpresion/protecta/">Barniz Protecta F</a></li>
			    	
					<?php if(strstr($_SERVER['PHP_SELF'],"/sobreimpresion/protecta/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es un barniz de  sobreimpresi&oacute;n especialmente dise&ntilde;ado para obtener unos valores altos de brillo. Tambi&eacute;n se utiliza como barniz de protecci&oacute;n por su gran  resistencia al roce y al rayado.<br /><b>Soportes adecuados:</b> su uso es apto sobre tintas convencionales impresas en papel  estucado y no estucado.</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/sobreimpresion/mate/">Barniz Mate</a></li>
			    	
					<?php if(strstr($_SERVER['PHP_SELF'],"/sobreimpresion/mate/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales: </b>este barniz es ideal para dar un acabado mate a la impresi&oacute;n.  Se caracteriza (tambi&eacute;n) por su r&aacute;pido secado oxidativo,  que permite&nbsp; una r&aacute;pida manipulaci&oacute;n. Tambi&eacute;n  se utiliza como barniz de protecci&oacute;n por su resistencia al roce y al rayado. <br><b>Soportes adecuados:</b></a> es adecuado para la impresi&oacute;n sobre tintas convencionales impresas  en papel estucado, no estucado y cartoncillo.</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/sobreimpresion/satinado/">Barniz Satinado</a></li>
                	
					<?php if(strstr($_SERVER['PHP_SELF'],"/sobreimpresion/satinado/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales: </b>Este barniz ha sido formulado para darle a la impresi&oacute;n un efecto  sedoso. Es muy adecuado como barniz de protecci&oacute;n por su gran resistencia al  roce y al rayado.<br /><b>Soportes adecuados:</b> Su uso es apto  sobre tintas convencionales impresas en papel estucado y no estucado.</div>
                    <? } ?>
                    
              	</ul>
                <? } ?>
                
            <li><a href="http://www.newcoprintinginks.com/productos/barnices/acrilicos/">Barnices acr&iacute;licos</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/barnices/acrilicos/")) { ?>
                
                <?php if($_SERVER['PHP_SELF']=="/productos/barnices/acrilicos/index.php") { ?>
                <div class="intro1">(Los barnices acr&iacute;licos) son barnices formulados en  base agua pensados para m&aacute;quinas equipadas con grupo o torre de barniz con  secadores por infrarrojos, aire caliente y l&aacute;mparas ultravioleta.</div>
			 	<div class="intro1">Nuestros barnices  acr&iacute;licos dan unos acabados de gran calidad con una alta resistencia al frote y  al rayado.</div>
			  	<div class="intro1">Los acabados  que se obtienen con estos barnices son utilizados en las impresiones de alta  calidad de embalajes, publicidad y decoraci&oacute;n en general.<br>Newco Printing Inks dispone de cinco  tipos de barnices: </div>
                <? } ?>
                
                <ul class="sublista">
              	<li><a href="http://www.newcoprintinginks.com/productos/barnices/acrilicos/alto-brillo/">Newcryl alto brillo</a></li>
              			
					<?php if(strstr($_SERVER['PHP_SELF'],"/acrilicos/alto-brillo/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> est&aacute; especialmente recomendado para trabajos que requieren  altos valores de brillo. (Este barniz) presenta un r&aacute;pido secado con una  excelente formaci&oacute;n de film.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> se puede aplicar en l&iacute;nea sobre tintas convencionales h&uacute;medas o  bien ya secas<b>.</b><br /><b>Soportes adecuados:</b> (es) apto  para (aplicarlo sobre) papeles estucados y cartoncillo que no requieran de una  alta resistencia al roce.<br />&middot; Barniz Newcryl  alto brillo<br />&middot; Barniz Newcryl  alto brillo verso-reverso</div>
					<? } ?>
                        
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/acrilicos/mate/">Newcryl mate</a></li>
              		
                    <?php if(strstr($_SERVER['PHP_SELF'],"/acrilicos/mate/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> (es un barniz de sobreimpresi&oacute;n que) est&aacute; especialmente recomendado para  trabajos que requieren altos efectos mates. Es adecuado  para el barnizado tanto de una como de dos caras. Este barniz Tambi&eacute;n proporciona una buena resistencia al roce y al rayado.<br /><b>Caracter&iacute;sticas de  aplicaci&oacute;n:</b> se puede aplicar en l&iacute;nea  sobre tintas convencionales h&uacute;medas o bien ya secas<b>.</b><br /><b>Soportes  adecuados:</b> es apto para aplicarlo sobre papeles  estucados y cartoncillo.</div>
					<? } ?>
                
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/acrilicos/satinado/">Newcryl satinado</a></li>
              		
                	<?php if(strstr($_SERVER['PHP_SELF'],"/acrilicos/satinado/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> (es barniz est&aacute;) especialmente recomendado para trabajos donde se requiera dar  un aspecto sedoso a la   impresi&oacute;n. Tambi&eacute;n proporciona una  buena resistencia al roce y al rayado. <br /><b>Caracter&iacute;sticas de  aplicaci&oacute;n:</b> se puede aplicar en l&iacute;nea  sobre tintas convencionales h&uacute;medas o bien ya secas<b>.</b><br /><b>Soportes  adecuados:</b> es adecuado para aplicarlo sobre papeles  estucados y cartoncillo.</div>
					<? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/acrilicos/primer/">Newcryl primer</a></li>
                
                	<?php if(strstr($_SERVER['PHP_SELF'],"/acrilicos/primer/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> est&aacute; especialmente recomendado para trabajos que requieren acabados con alto brillo  en combinaci&oacute;n con barnices UV en l&iacute;nea.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>es un  barniz especialmente dise&ntilde;ado para la aplicaci&oacute;n h&uacute;medo-h&uacute;medo sobre tintas  offset y (su) posterior barnizado en l&iacute;nea o fuera de l&iacute;nea con barniz de  sobreimpresi&oacute;n UV. Es aplicable en m&aacute;quinas con doble torre de barniz equipadas  con secadores IR, aire caliente o l&aacute;mparas UV<br /><b>Soportes adecuados: </b>est&aacute; especialmente dise&ntilde;ado para su aplicaci&oacute;n sobre papeles  estucados y cartoncillo.</div>
					<? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/acrilicos/antideslizante/">Newcryl Antideslizante</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/acrilicos/antideslizante/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> est&aacute;  especialmente recomendado para trabajos que requieren un bajo deslizamiento y  no sea necesaria una alta resistencia al roce. Tambi&eacute;n le proporciona a la  impresi&oacute;n un acabado brillante.<b></b><br /><b>Caracter&iacute;sticas de  aplicaci&oacute;n:</b> se puede aplicar en l&iacute;nea  sobre tintas convencionales h&uacute;medas o bien ya secas<b>.</b><br /><b>Soportes adecuados: </b>es adecuado  para aplicarlo sobre papeles estucados y cartoncillo que no requieran de una  alta resistencia al roce.</div>
					<? } ?>
                    
                </ul>
                <? } ?>
                
            <li><a href="http://www.newcoprintinginks.com/productos/barnices/uv/">Barnices UV</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/barnices/uv/")) { ?>
                <ul class="sublista">
              	<li><a href="http://www.newcoprintinginks.com/productos/barnices/uv/adhesivo-cold-foil/">Adhesivo Cold Foil</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/uv/adhesivo-cold-foil/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es un  adhesivo de curado UV especialmente dise&ntilde;ado para el estampado en fr&iacute;o y para  la laminaci&oacute;n de papel, soportes pl&aacute;sticos y metalizados.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> se  aplica en una m&aacute;quina flexogr&aacute;fica equipada con sistema de curado UV.<br /><b>Soportes adecuados:</b> es apto  para su utilizaci&oacute;n sobre papel y soportes pl&aacute;sticos.</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/uv/barniz/">Barniz UV</a></li>
              		
                    <?php if(strstr($_SERVER['PHP_SELF'],"/uv/barniz/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es un barniz de sobreimpresi&oacute;n de curado  UV destinado a acabados de alta calidad. Est&aacute;  especialmente recomendado para trabajos que requieren altos valores de brillo y  resistencias f&iacute;sico-qu&iacute;micas con velocidades altas de impresi&oacute;n.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>puede ser aplicado en m&aacute;quina barnizadora  y flexogr&aacute;fica de alta velocidad, equipadas con sistema de curado UV. Se puede utilizar sobre tintas UV con o sin curado intermedio, <br /><b>Soportes adecuados: </b>se utilizan  sobre papel, cart&oacute;n y soportes  metalizados.</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/uv/stamping/">Barnix UV Stamping</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/uv/stamping/")) { ?>
                    <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es un  barniz de sobreimpresi&oacute;n de curado UV destinado a trabajos que, despu&eacute;s del  barnizado, vayan a ser estampados y/o encolados. Est&aacute; especialmente recomendado  para trabajos que requieren altos valores de brillo y acabados de alta calidad  impresos a altas velocidades. Adem&aacute;s presenta una gran resistencia a los  disolventes y por este motivo se recomienda para trabajos que requieran buenas  resistencias f&iacute;sico-qu&iacute;micas y resistencia al agua.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>Puede  ser aplicado en m&aacute;quina barnizadora y flexogr&aacute;fica de alta velocidad, equipadas  con sistema de curado UV. Se utiliza para imprimir sobre tintas UV con o sin  curado intermedio<br /><b>Soportes adecuados:</b> impresas en  papel, cart&oacute;n y soportes metalizados.</div>
                    <? } ?>
       			
                <li><a href="http://www.newcoprintinginks.com/productos/barnices/uv/braille/">Barniz Braille</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/uv/braille/")) { ?>
                    <div class="intro1">
                    <b>Caracter&iacute;sticas Principales: </b>es un  barniz desarrollado para la impresi&oacute;n de formas e im&aacute;genes de alto relieve. La  reolog&iacute;a ha sido establecida para obtener una buena imagen en Braille de hasta  280 micras de grosor.<br />
					<b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> se aplica en una m&aacute;quina rotativa de  serigraf&iacute;a con un horno de curado UV.<br />
					<b>Soportes adecuados:</b> puede imprimirse tanto sobre papel como sobre materiales no absorbentes como el  polietileno o el PVC.
                    </div>
                    <? } ?>
                </ul>
                <? } ?>
        </ul>
        <? } ?>
        

        <h4><a href="http://www.newcoprintinginks.com/productos/tintas/">TINTAS</a></h4>
        
		<?php if(strstr($_SERVER['PHP_SELF'],"/tintas/")) { ?>
        
        <?php if($_SERVER['PHP_SELF']=="/productos/tintas/index.php") { ?>
        <p>En el mundo de las Artes Gr&aacute;ficas, muchas veces nos  preguntamos si cabr&iacute;a la posibilidad de que una tinta pudiera servir para todos  los trabajos de impresi&oacute;n. Debido a los diferentes m&eacute;todos de impresi&oacute;n y de  secado que se han desarrollado, la tinta ha tenido que irse modificando para  adecuarse a ellos.</p>
		<p>El nivel de exigencia de los diferentes sectores tambi&eacute;n ha  ido aumentando. En el mundo de la <b>edici&oacute;n </b>se han complicado los dise&ntilde;os, se precisa de mayor intensidad en los  colores, grandes contrastes. Las garant&iacute;as sanitarias de las impresiones que  van dirigidas al mundo de la <b>alimentaci&oacute;n</b>.  La impresi&oacute;n directa sobre una plancha de aluminio u otro metal ha desarrollado  la <b>impresi&oacute;n metalgr&aacute;fica. </b>El gran  desarrollo que se ha producido en los &uacute;ltimos a&ntilde;os en las <b>tintas especiales y de seguridad</b>, que permiten el tratamiento de  informaci&oacute;n a alta velocidad y a&ntilde;aden dificultad a la alteraci&oacute;n o clonaci&oacute;n de  un documento o marca.</p>
		<p>Todos ellos son ejemplos del alto grado de especializaci&oacute;n y  del nivel de exigencia que han propiciado el desarrollo de nuevas tintas con un  elevado nivel de calidad.</p>
		<? } ?>
        
        <ul class="lista">
            <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/">Tintas convencionales</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas/convencionales/")) { ?>
                
                <?php if($_SERVER['PHP_SELF']=="/productos/tintas/convencionales/index.php") { ?>
                <p>Existen actualmente toda una serie de nuevas tecnolog&iacute;as que  nos aportan nuevos entornos de producci&oacute;n. A la hora de definir qu&eacute; tipo  impresi&oacute;n es el m&aacute;s adecuado para un cierto trabajo debemos de tener un  conocimiento claro del proyecto. Debemos conocer a la perfecci&oacute;n cu&aacute;les son las  exigencias de nuestro cliente y el soporte que se va a imprimir. Newco Printing Inks presenta una serie de propuestas para  todos los sistemas de impresi&oacute;n, as&iacute;:</p>
                <p>- Offset o flexograf&iacute;a para impresi&oacute;n sobre cartoncillo para la fabricaci&oacute;n  de cajas plegables para cosm&eacute;ticos, perfumes, alimentos, licores, tabacos, etc.<br />
                  - Offset bobina para la impresi&oacute;n de formatos comerciales, mailing,  y billetes de loter&iacute;a y otros.<br />
                  - Offset convencional o waterless, tipograf&iacute;a, flexograf&iacute;a o serigraf&iacute;a  sobre papel o soportes sint&eacute;ticos para etiquetas (encolables y autoadhesivas).<br />
                  - Offset convencional o waterless, tipograf&iacute;a, flexograf&iacute;a o  serigraf&iacute;a sobre soportes pl&aacute;sticos para la impresi&oacute;n de tarjetas de cr&eacute;dito,  tarjetas telef&oacute;nicas, tubos conformados, CDs, r&oacute;tulos de puntos de venta pl&aacute;sticos,  etc.<br />
                  - Offset para la impresi&oacute;n sobre metal para hacer latas, botes,  cajas, etc.</p>
                <? } ?>
                  
                <ul class="sublista">
            		<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/">Offset Convencional</a></li>
						<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/offset-convencional/")) { ?>
                    	
                        <?php if($_SERVER['PHP_SELF']=="/productos/tintas/convencionales/offset-convencional/index.php") { ?>
                        <div class="intro1">Las nuevas tendencias y la necesidad de aumentar la  productividad del proceso offset a trav&eacute;s de una mayor estandarizaci&oacute;n y eficiencia,  que obliga a los fabricantes de las materias primas a ajustar con menor grado  de tolerancia los par&aacute;metros b&aacute;sicos de sus productos, debido a su influencia  en el proceso de impresi&oacute;n y su resultado.<br />Newco Printing Inks dispone de una amplia  gama de tintas para Offset convencional. Nos adaptamos a los trabajos m&aacute;s  exigentes ofreciendo al mercado una soluci&oacute;n para cada tipo de m&aacute;quina de impresi&oacute;n.</div>
                        <? } ?>
                        
                        <ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/econew-fresh/">Gama Econew Fresh</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/econew-fresh/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es una  serie 100% vegetal, dotada de una gran fuerza colorante con un excelente  recorte de punto y un alto brillo. Esta gama ha sido formulada conforme a la  norma del color ISO12647-2.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> es  especialmente adecuada para m&aacute;quinas offset de 1, 2, o 4 cuerpos de impresi&oacute;n.  .Se  considera una tinta muy fresca. <br /><b>Soportes adecuados:</b> es adecuada para todo  tipo de papeles estucados.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/nsp-new/">Gama NSP New</a></li>
                   	 		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/nsp-new/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales: </b>es  una gama (muy) polivalente de gran estabilidad, que se adapta  perfectamente a diferentes tipos de trabajos, soportes y m&aacute;quinas.<b> </b>Su r&aacute;pido <em>setting</em> inicial permite al  impresor realizar retiraciones inmediatas sin ning&uacute;n tipo de problema. Al mismo  tiempo es una gama alta resistencia al roce.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> es  una gama semifresca que est&aacute; dise&ntilde;ada para la impresi&oacute;n en m&aacute;quinas de 4  colores.<b></b><br /><b>Soportes adecuados:</b> esta gama est&aacute; indicada para una gran variedad de  soportes, sobretodo para &nbsp;papeles estucados  brillantes, tambi&eacute;n para soportes estucados mates o semi-mates y &nbsp;papeles offset satinados y cartoncillo.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/npx-mayor/">Gama NPX Mayor</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/npx-mayor/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> se ha dise&ntilde;ado para satisfacer las exigencias m&aacute;s altas del impresor,  tanto en el comportamiento de la gama en m&aacute;quina como en su posterior  manipulaci&oacute;n. Posee un excelente brillo y un buen  recorte de punto. Su bajo tiro residual, evita la  acumulaci&oacute;n de tinta y reduce las paradas de m&aacute;quina.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b><b> </b>es una gama fresca  que est&aacute; especialmente dise&ntilde;ada para los procesos de impresi&oacute;n de tira y  retira, y tira-retira en l&iacute;nea (m&aacute;quinas de 8 y 10 cuerpos).<b> </b><br /><b>Soportes adecuados:</b> esta gama presenta unos  excelentes resultados tanto  para soportes estucados brillantes y mates, como papeles offset.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/high-rub/">Gama High Rub</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/high-rub/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> esta serie ha  sido dise&ntilde;ada para satisfacer las  exigencias del impresor en aquellos trabajos especialmente dif&iacute;ciles que  empleen soportes mates o semi-mates o los que requieran de una alta resistencia  al roce y al rayado. <br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> puede usarse con excelentes resultados en m&aacute;quinas de alta velocidad de 1 a 12 cuerpos de impresi&oacute;n. <br /><b>Soportes adecuados:</b> Su  excelente secado por oxidaci&oacute;n y su alto contenido en ceras, hace que esta  tinta sea &oacute;ptima para la impresi&oacute;n sobre papeles mates, semi-mates, satinados o  cartoncillos.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/novaplast/">Gama Novaplast (PVC)</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/novaplast/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales: </b>es  una gama que ha sido especialmente dise&ntilde;ada para la impresi&oacute;n de soportes no  porosos. Es una serie altamente oxidativa con un buen brillo y una excelente  resistencia al roce.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> es  especialmente adecuada para m&aacute;quinas offset de 1, 2, o 4 cuerpos de impresi&oacute;n.  Tambi&eacute;n puede usarse en todo tipo de m&aacute;quinas offset con sistemas de mojado  convencionales o de base alcohol.<br /><b>Soportes adecuados:</b> es una  tinta offset convencional ideal para la impresi&oacute;n de materiales pl&aacute;sticos,  aluminios y otros soportes no absorbentes.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/litho-pantone/">Pantones Base Litho</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/litho-pantone/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es una  serie polivalente para offset convencional que permite la impresi&oacute;n de trabajos  de alta calidad dentro de un amplio rango de soportes. Posee un excelente  brillo, un r&aacute;pido secado por oxidaci&oacute;n y alta resistencia al roce.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>puede  usarse con excelentes resultados en m&aacute;quinas de alta velocidad de 1 a 12 cuerpos de impresi&oacute;n.<br /><b>Soportes adecuados: </b>est&aacute;  dise&ntilde;ada para imprimir sobre papeles tanto estucados como cartoncillo. <br /><b>Newco Printing Inks </b>dispone tanto de los  colores Pantones Base como de los colores de la carta Pantone. Tambi&eacute;n dispone  de la capacidad de fabricar colores a la muestra.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-convencional/negros/">Negros especiales (Negro Perfecta, Negro Edici&oacute;n Alta Calidad, Negro Edici&oacute;n, negro coldset)</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/negros/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas  Principales: </b>Los negros especiales se  emplean en aquellos trabajos que necesitan de un gran impacto visual. Son  negros de alta calidad que se diferencia de los negros de cuatricom&iacute;a por su  mayor concentraci&oacute;n pigmentaria y profundidad de color. Su utilizaci&oacute;n est&aacute;  indicada en trabajos publicitarios, estucher&iacute;a de calidad, impresi&oacute;n de  interior de libros, folletos y revistas. <br />
                            <b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>La serie comprende una amplia variedad  de productos que incluyen desde los destinados a offset hoja hasta los  indicados para rotativa de bobina <br /><b>Soportes adecuados:</b> son adecuados para papeles estucados  brillantes y mates y (como soportes) Offset. Los negros edici&oacute;n est&aacute;n  especialmente dise&ntilde;ados para la impresi&oacute;n de papeles Offset. <br>&middot; Negro Perfecta<br>&middot; Negro Edici&oacute;n Alta Calidad<br>&middot; Negro Edici&oacute;n</div>
							<? } ?>
                            
                        </ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-seco/">Offset Seco</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/offset-seco/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-seco/novaless/">Novaless</a></li>
	                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/offset-seco/novaless/")) { ?>
                            <div class="intro1"><b>Caracter&iacute;sticas Principales:</b> es una  tinta 100 % vegetal que ha sido formulada para la impresi&oacute;n en offset seco. Se  caracteriza por poseer una buena estabilidad al calor, un bajo tack y un secado  muy r&aacute;pido.<br /><b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> Est&aacute;  especialmente indicada para m&aacute;quinas offset de 1, 2, o 4 cuerpos de impresi&oacute;n  provistas de rodillos refrigeradores o cualquier otro tipo de refrigeraci&oacute;n.<br /><b>Soportes adecuados:</b> adecuada  para todo tipo de papel y cartoncillo estucados y no estucados.</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-uv/">Offset UV</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/offset-uv/")) { ?>
                    	
                        <?php if($_SERVER['PHP_SELF']=="productos/tintas/convencionales/offset-uv/index.php") { ?>
                        <div class="intro1">El curado UV ofrece una de las mejores soluciones t&eacute;cnicas puesto que al ser un proceso de secado  instant&aacute;neo se obtiene una mayor productividad por velocidad de impresi&oacute;n y m&aacute;s  alta eficiencia del proceso por permitir una inmediata manipulaci&oacute;n posterior. <br />
						<br />La tecnolog&iacute;a UV se considera una de las m&aacute;s limpias desde el punto de  vista medioambiental, debido a que sus componentes reactivos participan en la  polimerizaci&oacute;n y este hecho hace que todo el material impreso permanezca en la  pel&iacute;cula seca. <br />
						<br />Unas de las  principales aplicaciones de estas tintas es la impresi&oacute;n sobre cartoncillo para la fabricaci&oacute;n de  cajas plegables para cosm&eacute;ticos, perfumes, alimentos, licores, tabacos, etc., sobre  papel o soportes sint&eacute;ticos para etiquetas (encolables y autoadhesivas), sobre  soportes pl&aacute;sticos para la impresi&oacute;n de tarjetas de cr&eacute;dito, tarjetas telef&oacute;nicas,  tubos conformados, CDs, etc.</div>
                        <? } ?>
                        
                        <ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-uv/newlam/">Gama Newlam</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-uv/newlam/")) { ?>
                            <div class="intro2">
                        	<b>Caracter&iacute;sticas  Principales: </b>La gama NEWLAM UV es una  tinta offset de curado UV, especialmente dise&ntilde;ada para la impresi&oacute;n en hoja  sobre una amplia gama de pl&aacute;sticos r&iacute;gidos. Esta serie presenta una gran  compatibilidad con la mayor&iacute;a de los recubrimientos de encolado que existen en  el mercado.<br />
							<b>Caracter&iacute;sticas de aplicaci&oacute;n:</b></a> Est&aacute; especialmente indicada para m&aacute;quinas offset de 1, 2, o 4  cuerpos de impresi&oacute;n provistas de un horno de curado UV.<br />
							<b>Soportes adecuados:</b> presenta  una excelente adhesi&oacute;n sobre una amplia gama de soportes pl&aacute;sticos del tipo:  PVC, PVC r&iacute;gido, PVC opaco, PVC transparente, Policarbonato, Polipropileno,  Polietileno, Poli&eacute;ster.
                        	</div>
                        	<? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/offset-uv/newpap/">Gama Newpap</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-uv/newpap/")) { ?>
                            <div class="intro2">
                            <b>Caracter&iacute;sticas Principales: </b>la  serie se caracteriza por su alta definici&oacute;n de impresi&oacute;n y alto brillo. Esta  serie cumple con la norma de color ISO 12647-2. Posee buenas resistencias  qu&iacute;micas, de deslizamiento y de resistencia al roce.<br />
  							<b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>est&aacute;  dise&ntilde;ada principalmente para su aplicaci&oacute;n en offset hoja y bobina para  m&aacute;quinas de 4 y 8 cuerpos.<br />
							<b>Soportes adecuados: </b>es  especialmente adecuada para imprimir sobre papel, cart&oacute;n y soportes metalizados  con recubrimiento de nitrocelulosa, tanto para hoja como para bobina.
                        	</div>
                        	<? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/waterless/">Waterless UV (offset seco)</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/waterless/")) { ?>
                    	<br />
                        <ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/waterless/uv/">Waterless UV</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/waterless/uv/")) { ?>
                        	<p>La impresi&oacute;n Waterless es un m&eacute;todo de impresi&oacute;n  Offset en el que no se utiliza un sistema de mojado. Las  planchas Waterless, est&aacute;n fabricadas con una capa de silicona adecuada para este tipo de tintas. La m&aacute;quina de impresi&oacute;n Waterless  est&aacute; equipada con un sistema de  control de temperatura.
							<br /><br />La ventaja principal de este sistema es que ofrece  unas impresiones n&iacute;tidas y de alto contraste, lo que aumenta el volumen y  brillo de la impresi&oacute;n. Debido a la menor ganancia de punto, el Waterless es  id&oacute;neo para tramado FM y estoc&aacute;stico  lo que aumenta la definici&oacute;n dando unas  impresiones de alta resoluci&oacute;n.
							<br /><br />Este sistema de impresi&oacute;n se utiliza  sobre pl&aacute;stico y otros soportes de alta calidad, etc.. utilizados para etiquetas  de alta gama. (p. ej. Vinos y Licores), tarjetas laminadas (bancarias/cr&eacute;dito,  clientes V.I.P), soportes audio-visuales, inform&aacute;ticos (CD&acute;s/  DVD&acute;s) y sobre papel y cart&oacute;n de todo tipo.</p>
                            <? } ?>
                        <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/waterless/new">NewWaterless UV</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/waterless/new/")) { ?>
                        	<p><b>Caracter&iacute;sticas Principales:</b> posee una alta definici&oacute;n de  punto y alto brillo.  Esta tinta es excelente para tramas FM y estoc&aacute;sticas dando unas impresiones de alta resoluci&oacute;n. Tambi&eacute;n&nbsp; presenta una  buena adhesi&oacute;n con excelentes propiedades de laminaci&oacute;n y sobreimpresi&oacute;n. Posee  un buen curado y una alta resistencia a los disolventes.<br />
  							<b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>est&aacute;  dise&ntilde;ada principalmente para su aplicaci&oacute;n en hoja a hoja en m&aacute;quinas de 4 cuerpos  con un sistema de curado UV.<br />
							<b>Soportes adecuados: </b>presenta<b> </b>muy  buena adhesi&oacute;n sobre un amplio rango de substratos pl&aacute;sticos de PVC r&iacute;gido y  plastificado, films pl&aacute;sticos pretratados, papel y cart&oacute;n brillantes y soportes  met&aacute;licos lacados.</p>
                            <? } ?>
                    	</ul>
                    	<? } ?>
               		<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/flexo-agua/">Flexo agua (flexograf&iacute;a)</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/flexo-agua/")) { ?>
                    	
                        <?php if($_SERVER['PHP_SELF']=="/productos/tintas/convencionales/flexo-agua/index.php") { ?>
                        <p>Desde  que sus inicios la industria flexogr&aacute;fica ha estado en constante cambio. Ning&uacute;n  otro sector de la industria gr&aacute;fica ha experimentado un ritmo de transformaci&oacute;n  e innovaci&oacute;n tan veloz como el sector de impresi&oacute;n de etiquetas, bolsas, papel  de envoltorio, etc. Cada d&iacute;a, el mercado se ampl&iacute;a con nuevas tintas, nuevos  sustratos, y mayores capacidades de los impresores.Es por  este motivo que Newco Printing Inks ofrece soluciones para todo tipo de  impresi&oacute;n flexogr&aacute;fica.</p>
                        <p><b>Etiquetas</b>: El mercado de las etiquetas  ha sido uno de los de mayor crecimiento para la impresi&oacute;n flexogr&aacute;fica. Las mejoras  tecnol&oacute;gicas conseguidas le han permitido conseguir el alto nivel de calidad de  impresi&oacute;n que antes pod&iacute;a conseguirse s&oacute;lo con el offset.<br />Hoy en d&iacute;a la diferenciaci&oacute;n de los productos de consumo empieza en su  etiqueta y envoltorio y su car&aacute;cter de exclusividad ya se aprecia en el  packaging. Esta personalizaci&oacute;n ha hecho que los trabajos de impresi&oacute;n tengan  cada vez menores vol&uacute;menes, y la flexograf&iacute;a se haya convertido en una de las  opciones razonablemente m&aacute;s econ&oacute;micas para muchos compradores de etiquetas. <i>Newco Printing Inks</i> dispone en  su cat&aacute;logo distintas series para cubrir las necesidades de sus clientes:</p>
                        <? } ?>
                        
                        <ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/flexo-agua/newfilm/">Newfilm</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-agua/newfilm/")) { ?>
                            <p><b>Caracter&iacute;sticas  Principales:</b> ha sido desarrollada para la impresi&oacute;n de materiales autoadhesivos,  especialmente para materiales sint&eacute;ticos. Presenta una muy buena adhesi&oacute;n y  resistencia al rayado en soportes sint&eacute;ticos. Adem&aacute;s de su elevada resistencia al agua. <br />
							<b>Caracter&iacute;sticas de  aplicaci&oacute;n:</b> esta serie funciona en todas las m&aacute;quinas flexogr&aacute;ficas existentes con excelente estabilidad en m&aacute;quina a elevadas velocidades.<b> </b> <br />
							<b>Soportes adecuados: P</b>apeles  recubiertos y films de polietileno, PET, BOPP y films con recubrimiento  acr&iacute;lico.</p>
                            <? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/flexo-agua/newtherm/">Newtherm</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-agua/newtherm/")) { ?>
                            <p><b>Caracter&iacute;sticas  Principales:</b> es una tinta flexo base agua desarrollada para materiales  autoadhesivos termoreactivos. Tiene una muy buena fuerza de color y posee un  elevado brillo y calidad de impresi&oacute;n tanto en colores planos como tramas y  cuatricrom&iacute;a, lo que hace apropiadas para la impresi&oacute;n flexogr&aacute;fica de  etiquetas de alta calidad.<br />
							<b>Caracter&iacute;sticas de aplicaci&oacute;n:</b>es adecuada para todas las  m&aacute;quinas flexogr&aacute;ficas existentes, Adem&aacute;s pueden ser barnizadas tanto con  barnices al agua como de curado UV y laminadas en l&iacute;nea.<br />
							<b>Soportes adecuados: </b>est&aacute; especialmente dise&ntilde;ada para la impresi&oacute;n de papeles  recubiertos y t&eacute;rmicos y cualquier otra aplicaci&oacute;n en la que se requiera  resistencia al agua y al calor.</p>
                            <? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/flexo-agua/newscreen/">Newscreen UV</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-agua/newscreen/")) { ?>
                            <p><b>Caracter&iacute;sticas Principales:</b> se  caracteriza por su alto brillo para la impresi&oacute;n de etiquetas. Presenta un  r&aacute;pido curado, gran flexibilidad y excelente nivelaci&oacute;n.<br />
							<b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> es excelente  para la impresi&oacute;n de etiquetas de muy alta velocidad bobina a bobina, tanto con  pantalla plana como rotativa. Esta tinta es adecuada para combinar distintos  sistemas de impresi&oacute;n en l&iacute;nea, ofreciendo excelentes resultados cuando se  sobreimprime con tintas de tipograf&iacute;a y flexograf&iacute;a.<br />
  							<b>Soportes adecuados:</b> tiene muy buena adhesi&oacute;n sobre polietileno y polipropileno con  tratamiento superficial corona o flameado, as&iacute; como tratamiento qu&iacute;mico o <em>primer</em>. Tambi&eacute;n tiene adhesi&oacute;n en PVC.</p>
  							<? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/flexo-uv/">Flexo UV (flexograf&iacute;a)</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/flexo-uv/")) { ?>
                    	
                        <?php if($_SERVER['PHP_SELF']=="/productos/tintas/convencionales/flexo-uv/index.php") { ?>
                        <p>La  flexograf&iacute;a UV es el proceso flexogr&aacute;fico tradicional al que se ha incorporado  la tecnolog&iacute;a UV para el secado de la tinta. Esto y otros avances en el mundo  flexogr&aacute;fico, han permitido ampliar el tipo de trabajos a realizar as&iacute; como los  soportes a utilizar.</p>
						<p>El desarrollo de las  tintas UV ha sufrido un gran avance en los &uacute;ltimos a&ntilde;os. La mejora de su  formulaci&oacute;n, el uso pigmentos m&aacute;s adecuados, fotoinicadores, etc., ha permitido  el desarrollo de tintas UV de muy bajo olor, adecuadas para la impresi&oacute;n de embalajes  destinados a la alimentaci&oacute;n y gracias a la investigaci&oacute;n tambi&eacute;n se ha  conseguido tintas con las que se obtienen resultados de calidad excepcional.</p>
						<p>Newco  Printing Inks ofrece una serie que se adapta a todas las necesidades del  impresor:</p>
                        <? } ?>
                        <ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/flexo-uv/newflex/">Newflex (Etiquetas)</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-uv/newflex/")) { ?>
                            <div class="intro1">
                        	<b>Caracter&iacute;sticas Principales:</b> esta serie  posee una excelente definici&oacute;n que, combinado con un alto brillo y un curado  r&aacute;pido, hacen que sea id&oacute;nea para este tipo de impresi&oacute;n. &nbsp;Esta serie tiene buenas resistencias qu&iacute;micas,  de deslizamiento y de resistencia al roce.<br />
  							<b>Caracter&iacute;sticas de aplicaci&oacute;n:</b> ha  sido dise&ntilde;ada para imprimir en m&aacute;quinas flexogr&aacute;ficas rotativas para narrow  web, es decir, para la impresi&oacute;n de etiquetas en m&aacute;quinas de alta velocidad.<br />
					  		<b>Soportes adecuados:</b> es &oacute;ptima  para imprimir sobre materiales sint&eacute;ticos de etiquetas, aunque tambi&eacute;n est&aacute;  dise&ntilde;ada para imprimir sobre papel y cart&oacute;n. Adem&aacute;s posee una muy buena  adhesi&oacute;n en un amplio rango de sustratos.
                        	</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/serigrafia/">Serigraf&iacute;a UV</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/serigrafia/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/serigrafia/newscreen/">Newscreen UV (Etiquetas)</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/serigrafia/newscreen/")) { ?>
                            <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/metalgrafia/">Metalgraf&iacute;a</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/convencionales/metalgrafia/")) { ?>
						
                        <?php if($_SERVER['PHP_SELF']=="/productos/tintas/convencionales/metalgrafia/index.php") { ?>
                    	<p>Cada vez m&aacute;s los envases  est&aacute;n incrementando su participaci&oacute;n en el sector de la metalgraf&iacute;a, no solo  por la conservaci&oacute;n de los alimentos sino tambi&eacute;n para fines comerciales. La serie que Newco  ofrece a este mercado, es la de mayor prestigio internacional por la alta  calidad de sus productos y el nivel de asistencia t&eacute;cnica que el fabricante,  INX International, nos brinda.</p>
						<p>Las tintas poseen una elevada intensidad, resistencia t&eacute;rmica y a la  abrasi&oacute;n con una magn&iacute;fica maquinabilidad, adem&aacute;s de una excelente adherencia y  propiedades de impresi&oacute;n excelentes. Las soluciones personalizadas que ofrecemos hacen que nos adaptemos  perfectamente a las necesidades del cliente:</p>
						<? } ?>

                        <ul>
                    		<li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/metalgrafia/tp-cure/">TP Cure</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/metalgrafia/tp-cure/")) { ?>
                            <p><b>Caracter&iacute;sticas Principales:</b> Presenta  un excelente brillo y una alta resistencia qu&iacute;mica y a la abrasi&oacute;n. Es excelente  para la impresi&oacute;n de 2 o 3 piezas.<br />
							<b>Caracter&iacute;sticas de  aplicaci&oacute;n: </b>presenta un r&aacute;pido ajuste  inicial en m&aacute;quina, con una gran estabilidad y maquinabilidad. Se utiliza en  m&aacute;quinas Offset de 4 a  6 cuerpos con un horno como sistema de secado.<br />
							<b>Soportes adecuados:</b> es ideal  para la impresi&oacute;n y posterior fabricaci&oacute;n de envases met&aacute;licos de aluminio o  acero de 2 o 3 piezas.</p>
                            <? } ?>
                    		
                            <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/metalgrafia/poly-novar/">Poly Novar</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/metalgrafia/poly-novar/")) { ?>
                            <p><b>Caracter&iacute;sticas Principales:</b> esta tinta tiene  una alta intensidad de color debido a su gran concentraci&oacute;n pigmentaria y muy  buenas propiedades de imprimibilidad. Posee grandes resistencias a la abrasi&oacute;n  y bajo <em>misting</em>. Es ideal para la  impresi&oacute;n de latas de 2 piezas.<br />
  							<b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>Alta  estabilidad en m&aacute;quina y muy buena transferencia. Se utiliza en m&aacute;quinas Offset  de 4 a 6  cuerpos con un sistema de curado UV.<br />
							<b>Soportes adecuados:</b> especialmente  indicada para la impresi&oacute;n de latas de aluminio y acero impresas con una base  de barniz o de blanco.</p>
                            <? } ?>
                    		                            
                            <li><a href="http://www.newcoprintinginks.com/productos/tintas/convencionales/metalgrafia/inx-cure/">Inx Cure</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/metalgrafia/inx-cure/")) { ?>
                            <p><b>Caracter&iacute;sticas Principales:</b> Gran  calidad y nitidez de imagen con un m&iacute;nimo engorde de punto. Presenta una  excelente adhesi&oacute;n sobre <em>primers</em>,  blancos opacos y otros colores mostrando una buena flexibilidad. Esta tinta  est&aacute; exenta de ITX. Es excelente para la impresi&oacute;n de 2 o 3 piezas.<br />
  							<b>Caracter&iacute;sticas de aplicaci&oacute;n: </b>esta  tinta funciona con una gran variedad de aguas de mojado. Se utiliza en m&aacute;quinas  Offset de 4 a  6 cuerpos con un sistema de curado UV.<br />
  							<b>Soportes adecuados:</b> es ideal  para la impresi&oacute;n y posterior fabricaci&oacute;n de envases met&aacute;licos de aluminio o  acero de 2 o 3 piezas.</p>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                 </ul>
                 <? } ?>
            <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/">Tintas especiales y de seguridad</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas/especiales/")) { ?>
                <ul class="sublista">
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fluorescentes/">Fluorescentes</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/fluorescentes/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fluorescentes/uv/">Fluorescentes UV</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/fluorescentes/uv/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fluorescentes/ir/">Fluorescentes IR</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/fluorescentes/ir/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>                
                    </ul>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fosforescentes/">Fosforescentes</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/fosforescentes/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fosforescentes/convencional/">Fosforescente Convencional (emisi&oacute;n &gt; 1s)</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/fosforescentes/convencional/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fosforescentes/seguridad/">Fosforescente de Seguridad (emisi&oacute;n &lt; 1s)</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/fosforescentes/seguridad/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/fotocromatica/">Fotocrom&aacute;tica</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/fotocromatica/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/iridiscentes/">Iridiscentes</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/iridiscentes/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/iridiscentes/estandar/">Iridiscente</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/iridiscentes/estandar/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/iridiscentes/irionew/">Irionew</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/iridiscentes/irionew/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/iridiscentes/newcrom/">Newcrom</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/iridiscentes/newcrom/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/tir/">Tintas TIR/no TIR</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/tir/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/tir/transparente/">Transparentes IR</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/especiales/tir/transparente/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/tir/no-transparente">No transparentes al IR</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/especiales/tir/no-transparente/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/termocromaticas/">Termocrom&aacute;ticas</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/termocromaticas/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/termocromaticas/15/">Termocrom&aacute;ticas 15&ordm;C</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/termocromaticas/15/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/termocromaticas/31/">Termocrom&aacute;ticas de 31&ordm;C</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/termocromaticas/31/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/termocromaticas/47/">Termocrom&aacute;ticas de 47&ordm;C</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/termocromaticas/47/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/reactivas-moneda/">Reactivas a la moneda</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/reactivas-moneda/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/reactivas-moneda/transparentes/">Transparentes Reactivas a la moneda</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/reactivas-moneda/transparentes/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/reactivas-moneda/opacas/">Opacas Reactivas a la moneda</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/reactivas-moneda/opacas/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/delebles/">Tintas delebles</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/delebles/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/marca-agua/"> Marca al agua</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/marca-agua/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/reactivas/">Tintas reactivas</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/reactivas/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/reactivas/agua/">Tintas reactivas al agua</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/reactivas/agua/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/reactivas/solventes/">Tintas reactivas a solventes</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/reactivas/solventes/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/conductivas/">Tintas conductivas</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/conductivas/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/magneticas/">Tintas magn&eacute;ticas</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/especiales/magneticas/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/magneticas/alta-coercitividad/">Tintas de alta coercitividad</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/magneticas/alta-coercitividad/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    <li><a href="http://www.newcoprintinginks.com/productos/tintas/especiales/magneticas/baja-coercitividad/">Tintas de baja coercitividad</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/magneticas/baja-coercitividad/")) { ?>
                    	<div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    	<? } ?>
                    </ul>
                    <? } ?>
                </ul>
                <? } ?>
        </ul>
        <? } ?>
        
        <h4><a href="http://www.newcoprintinginks.com/productos/auxiliares/">PRODUCTOS AUXILIARES</a></h4>
        <?php if(strstr($_SERVER['PHP_SELF'],"/productos/auxiliares/")) { ?>
        <ul class="lista">
            <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/flexo-agua/">Flexo base agua</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliares/flexo-agua/")) { ?>
                <ul class="sublista">
				<li><a href="http://www.newcoprintinginks.com/productos/auxiliares/flexo-agua/refresher">Aditivo refresher</a></li>
					<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-agua/refresher/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/flexo-agua/antiespumante/">Aditivo antiespumante</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-agua/antiespumante/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
				<li><a href="http://www.newcoprintinginks.com/productos/auxiliares/flexo-agua/limpiador/">Limpiador</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-agua/limpiador/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
                    <? } ?>
			  	</ul>
                <? } ?>
            <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/offset-convencional/">Offset convencional</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliares/offset-convencional/")) { ?>
                <ul class="sublista">
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/offset-convencional/antisecante/">Spray antisecante</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/antisecante/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/offset-convencional/secante/">Secante</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/secante/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/offset-convencional/speeddry/">Speeddry</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/speeddry/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/offset-convencional/viscosidad/">Reductor  de viscosidad</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-convencional/viscosidad/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>
              	</ul>
                <? } ?>
            <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/">Tintas UV</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliares/tintas-uv/")) { ?>
                <ul class="sublista">
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/reductor-viscosidad/">Reductor de viscosidad</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/reductor-viscosidad/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/antitack/">Antitack UV</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/antitack/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>                
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/antiroce/">Antiroce UV</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/antiroce/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>                
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/fotoiniciador-standard/">Fotoiniciador standard</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/fotoiniciador-standard/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>                
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/fotoiniciador-claro/">Fotoiniciador para colores claros</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/fotoiniciador-claro/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>                
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/nivelador/">Nivelador</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/nivelador/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>                
                <li><a href="http://www.newcoprintinginks.com/productos/auxiliares/tintas-uv/limpiador-uv/">Limpiador  UV</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/tintas-uv/limpiador-uv/")) { ?>
                    <div class="intro1">Si desea informaci&oacute;n referente a alg&uacute;n producto, puede utilizar el recuadro del margen derecho para plantear su consulta.</div>
					<? } ?>
              	</ul>
                <? } ?>
          </ul>
          <? } ?>
          </div>
     </div>
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>