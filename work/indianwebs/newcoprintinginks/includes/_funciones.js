function crearEvento(elemento, evento, funcion) 
{
	if (elemento.addEventListener) 							//firefox
		elemento.addEventListener(evento, funcion, false);
	else  													//explorer
		elemento.attachEvent("on" + evento, funcion);
}
function cargar()
{
	//funciona solo en IE
	//document.getElementsByTagName("a")[3].setAttribute("onmouseover","anim(this,'verde','on')");
	//document.getElementsByTagName("a")[3].setAttribute("onmouseout","anim(this,'verde','off')");
	
	//crearEvento(document.getElementsByTagName("a")[3], "mouseover", anim(this,'verde','on'));
	//crearEvento(document.getElementsByTagName("a")[3], "mouseout", anim(this,'verde','off'));
}
function anim(cual,color,estado)
{
	if(estado=="off")
		cual.style.color="#333333";
	else
		cual.style.color="#ffffff";
	cual.parentNode.style.backgroundImage="url(http://www.newcoprintinginks.com/img/bt_"+color+"_"+estado+".gif)";
}
function enviar()
{
	document.busqueda.q.value="site:www.newcoprintinginks.com "+document.busqueda.q.value;
	document.busqueda.submit();
	document.busqueda.q.value="";
}