
	<meta name="language" content="<?=$idioma;?>" />
	<link rel="stylesheet" type="text/css" href="http://www.newcoprintinginks.com/includes/_estilos.css" />
    <script type="text/javascript" src="http://www.newcoprintinginks.com/includes/_funciones.js"></script>
</head>

<body onLoad="cargar()">
<div id="central">
	<div class="top">
    	<?php if($idioma=="es") { ?>
        <a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/"><img class="logo" src="http://www.newcoprintinginks.com/img/logo_newco_printing.gif" alt="Newco Printing" /></a>
        <? } else { ?> 
		<a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/en/"><img class="logo" src="http://www.newcoprintinginks.com/img/logo_newco_printing.gif" alt="Newco Printing" /></a>        
        <? } ?>
        
        <div class="idioma">
        	<a title="Castellano" href="http://www.newcoprintinginks.com/index.php">Castellano</a> | <a title="English" href="http://www.newcoprintinginks.com/en/index.php">English</a></div>
        <div class="boton">
        	<?php if($idioma=="es") { ?>
			<div><a title="Empresa" href="http://www.newcoprintinginks.com/empresa.php" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')">Acerca de<br />Newco</a></div>
            <div><a title="Productos" href="http://www.newcoprintinginks.com/productos/" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')" class="pad">Productos</a></div>
            <div><a title="Expansion" href="http://www.newcoprintinginks.com/expansion.php" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')">Expansi&oacute;n<br />internacional</a></div>
            <div><a title="Contacto" href="http://www.newcoprintinginks.com/contacto.php" onmouseover="anim(this,'verde','on')" onMouseout="anim(this,'verde','off')" class="pad">Contacto</a></div>
  			<? } else { ?>          
			<div><a title="Company" href="http://www.newcoprintinginks.com/en/company.php" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')">About<br />Newco</a></div>
            <div><a title="Products" href="http://www.newcoprintinginks.com/en/products/" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')" class="pad">Products</a></div>
            <div><a title="Expansion" href="http://www.newcoprintinginks.com/en/expansion.php" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')">International<br />Expansion</a></div>
            <div><a title="Contact" href="http://www.newcoprintinginks.com/en/contact.php" onmouseover="anim(this,'verde','on')" onmouseout="anim(this,'verde','off')" class="pad">Contact</a></div>
            <? } ?>

        </div>
		<img title="<?=$tintas[$idioma]?>" class="tinta" src="http://www.newcoprintinginks.com/img/fondo_tintas.jpg" alt="<?=$tintas[$idioma]?>" />
        <div class="buscar">
        	<form method="get" name="busqueda" action="http://www.google.com/search" target="_blank">
			<input type="hidden" name="h1" value="<?=$idioma?>"></input>
            <input type="text" name="q" class="caja1" />
            <input type="button" onClick="enviar()" class="caja2" value="<?=$buscar[$idioma]?>" />
            </form>
        </div>        
    </div>