<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>Mapa del sitio NEWCO PRINTING INKS S.L.</title>
	<meta name="description" content="Mapa del sitio NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="inicio">        
        <h1><a title="Newco Printing" href="http://www.newcoprintinginks.com/">Newcoprinting</a></h1>
        <h2>Mapa del sitio</h2>
        <dl>
        <dt><a title="Empresa" href="http://www.newcoprintinginks.com/empresa.php">Acerca de Newco</a></dt>
        	<dd><a title="Valores Newco Printing" href="http://www.newcoprintinginks.com/empresa.php?sec=valores">Valores</a></dd>
            <dd><a title="Objetivo Newco Printing" href="http://www.newcoprintinginks.com/empresa.php?sec=objetivo">Objetivo</a></dd>
            <dd><a title="Compromiso Newco Printing" href="http://www.newcoprintinginks.com/empresa.php?sec=compromiso">Compromiso</a></dd>
            <dd><a title="Reach Newco Printing" href="http://www.newcoprintinginks.com/empresa.php?sec=reach">Reach</a></dd>

        <dt><a title="Productos" href="http://www.newcoprintinginks.com/productos/">Productos</a></dt>
            <dd><a title="Barnices Newco Printing Inks" href="http://www.newcoprintinginks.com/productos/barnices/">BARNICES</a></dd>
            <dd><a title="Tintas Printing Inks" href="http://www.newcoprintinginks.com/productos/tintas/">TINTAS</a></dd>
            <dd><a title="Productos Printing Inks" href="http://www.newcoprintinginks.com/productos/auxiliares/">PRODUCTOS AUXILIARES</a></dd>
   
        <dt><a title="Expansion" href="http://www.newcoprintinginks.com/expansion.php">Expasi&oacute;n internacional</a></dt>
        	<dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/expansion.php?sec=presencia">Presencia internacional</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/expansion.php?sec=trabaja">Trabaja con nosotros</a></dd>

        <dt><a title="Contacto" href="http://www.newcoprintinginks.com/contacto.php">Contacto</a></dt>
        	<dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/contacto.php?sec=datos">Datos de la empresa</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/contacto.php?sec=correo">Correo electr&oacute;nico</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/contacto.php?sec=localizacion">Localizaci&oacute;n geogr&aacute;fica</a></dd>

        <dt><a title="Legal" href="http://www.newcoprintinginks.com/legal.php">Aviso legal</a></dt>
        <dt><a title="Sitemap" href="http://www.newcoprintinginks.com/mapa.php">Mapa Web</a></dt>
        </dl>
     </div>
        
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>