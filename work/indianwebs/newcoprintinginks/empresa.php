<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>Empresa NEWCO PRINTING INKS S.L.</title>
	<meta name="description" content="Empresa NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="empresa">
		<div id="subsec">
        	<div><a title="Valores Newco Printing" href="empresa.php?sec=valores" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">Valores</a></div><br />
            <div><a title="Objetivo Newco Printing" href="empresa.php?sec=objetivo" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">Objetivo</a></div><br />
            <div><a title="Compromiso Newco Printing" href="empresa.php?sec=compromiso" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">Compromiso</a></div><br />
            <div><a title="Reach Newco Printing" href="empresa.php?sec=reach" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">REACH</a></div>            
        </div>
        <h1><a title="Newco Printing" href="http://www.newcoprintinginks.com/">Newcoprinting</a> &raquo; <a title="Newco Printing" href="empresa.php">Empresa</a> &raquo; 
        
        <?php switch($_GET["sec"])
		{
			default:
			case "valores":	?>
            <a title="Valores Newco Printing" href="empresa.php?sec=valores">Valores</a></h1>
			<h2>Valores</h2>
    		<p><em>&bull; Calidad del servicio</em> de forma permanente  adquirir el conocimiento de todas las variables que influyen en la aplicaci&oacute;n  de nuestros productos, y utilizar este conocimiento para ayudar a cada cliente  a obtener el mejor resultado en su actividad diaria.</p>
	  		<p><em>&bull; Innovaci&oacute;n e iniciativa</em> individual; el  crecimiento profesional y personal de todos nuestros colaboradores,  desarrollando nuestro trabajo diario como un equipo, con eficacia para cumplir  nuestros compromisos con todos nuestros clientes nacionales y extranjeros.</p>
			<p><em>&bull; Vivir y trabajar con integridad</em>, y con  respeto con el medio ambiente, con uso &eacute;tico de la ciencia y apoy&aacute;ndonos en el  sentido com&uacute;n para asegurarnos la sostenibilidad considerando las consecuencias de nuestras decisiones, en todas sus  formas, para el mejoramiento del medio ambiente, la comunidad, nuestros  empleados, proveedores y clientes.</p>
			<? break;
			
			case "objetivo": ?>
            <a title="Objetivo Newco Printing" href="empresa.php?sec=objetivo">Objetivo</a></h1>
            <h2>Objetivo</h2>
	        <p>Contribuir al  &eacute;xito de nuestros clientes en su labor de impresi&oacute;n, proporcionando la adecuada  formulaci&oacute;n, en el tiempo requerido y con su coste adecuado.</p>
        	<p>Nuestra  estrategia:</p>
    	    <ul>
    	    <li>Ofrecer al mercado productos superiores en la  calidad de sus formulaciones, propiedades intr&iacute;nsecas y aplicaci&oacute;n del producto  para obtener mejores im&aacute;genes impresas o con mayor eficacia.</li>
        	<li>Ofrecer al mercado productos superiores en la  calidad del servicio, desde el primer contacto del cliente con nuestra empresa.  Todo el equipo contribuye a incrementar la calidad de nuestro producto por  medio de su trabajo diario. </li>
          	<li>La aplicaci&oacute;n de nuestro conocimiento, sumado a  la aplicaci&oacute;n del conocimiento de nuestros proveedores nos permita una  constante mejora en la calidad ofrecida a nuestros clientes.</li>
	        </ul>
			<? break;
			
			case "compromiso": ?>
            <a title="Compromiso Newco Printing" href="empresa.php?sec=compromiso">Compromiso</a></h1>
            <h2>Compromiso</h2>
	        <p>Debido a la  filosof&iacute;a de nuestra empresa y a la naturaleza de algunos productos que  desarrollamos y vendemos, especialmente dirigidos a la impresi&oacute;n de documentos  de seguridad, es imperativo el hecho de mantener un elevado nivel de confidencialidad  con nuestros clientes. Por nuestra parte nos sentimos obligados a cumplir un  conjunto de normas para garantizar el nivel de discreci&oacute;n y de seguridad  exigido por el mercado:</p>
    	    <ul>
	        <li>Estricto  cumplimiento de la&nbsp;ley de protecci&oacute;n de datos personales&nbsp;y en general  de toda&nbsp;la legislaci&oacute;n vigente.</li>
    	    <li>Desarrollo de una pol&iacute;tica de seguridad  y c&oacute;digo de conducta internos.</li>
	        <li>Concreci&oacute;n de las condiciones de  nuestros servicios mediante contrato.</li>
    	    <li>M&aacute;xima transparencia y discreci&oacute;n en  la prestaci&oacute;n de nuestros servicios con el cliente.</li>
	        </ul>
			<? break;
			
			case "reach": ?>
            <a title="Reach Newco Printing" href="empresa.php?sec=reach">Reach</a></h1>
            <h2>REACH</h2>
            <p>REACH  es el Reglamento europeo (Reglamento n&ordm; 1907/2006 del Parlamento Europeo y del  Consejo) relativo al registro, evaluaci&oacute;n, autorizaci&oacute;n y restricci&oacute;n de las  sustancias y preparados qu&iacute;micos (<b>R</b>egistration, <b>E</b>valuation, <b>A</b>uthorisation and Restriction of <b>Ch</b>emicals).  Este Reglamento fue aprobado el 18 de diciembre de 2006 y entr&oacute; en vigor el 1  de junio de 2007.</p>
			<p>Este  texto supone una reforma total del marco legislativo sobre sustancias y  preparados qu&iacute;micos en la   Uni&oacute;n Europea. Su principal objetivo es garantizar un alto  nivel de protecci&oacute;n de la salud humana y del medio ambiente. REACH obliga a las  empresas que quieran comercializar sus productos en la Uni&oacute;n Europea a  registrar todas las sustancias qu&iacute;micas que se comercializan dentro del  territorio de la   Uni&oacute;n Europea. A partir de su entrada en vigor, no se podr&aacute;  comercializar ninguna sustancia que no se encuentre registrada.</p>
			<p>REACH  atribuye a la industria la responsabilidad de gestionar los riesgos asociados a  las sustancias qu&iacute;micas. Se basa en el principio de que corresponde a los  fabricantes, importadores y usuarios intermedios garantizar que s&oacute;lo fabrican,  comercializan o usan sustancias que no afectan negativamente a la salud humana  o el medio ambiente.</p>
            <p><img src="img/pdf.gif" align="absmiddle" alt="PDF" /> <a href="http://www.newcoprintinginks.com/img/documento_reach.pdf" target="_blank">Certificado Reach ........................... (11kb.)</a></p>
            <?
		}?>
     </div>
     
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>