<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>Expansion NEWCO PRINTING INKS S.L.</title>
	<meta name="description" content="Expansion NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="empresa">
		<div id="subsec">
        	<div><a title="Newco Printing" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')" href="expansion.php?sec=presencia">Presencia internacional</a></div><br />
            <div><a title="Newco Printing" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')" href="expansion.php?sec=trabaja">Trabaja con nosotros</a></div><br />
        </div>
        <h1><a title="Newco Printing" href="http://www.newcoprintinginks.com/">Newcoprinting</a> &raquo; <a title="Newco Printing" href="expansion.php">Expansi&oacute;n Internacional</a> &raquo; 
        
        <?php switch($_GET["sec"])
		{
			default:
			case "presencia":	?>
            <a title="Newco Printing" href="expansion.php?sec=presencia">Presencia internacional</a></h1>
			<h2>Presencia internacional</h2>
    		<p>Podemos  presumir de tener clientes en la actualidad en m&aacute;s de 20 pa&iacute;ses. Esta labor de  expansi&oacute;n internacional se ha logrado en menos de dos a&ntilde;os.</p>
			<p>M&aacute;s de la  mitad de nuestro volumen de negocio se est&aacute; realizando en los mercados de  exportaci&oacute;n.</p>
			<p><img title="Newco Printing Internacional" src="img/mapamundo2.gif" style="margin:-16px 0 0 0;" alt="Newco Printing Internacional" /></p>
			<? break;
			
			case "trabaja": ?>
            <a title="Newco Printing" href="expansion.php?sec=trabaja">Trabaja con nosotros</a></h1>
            <h2>Trabaja con nosotros</h2>
	        <p>El car&aacute;cter  diferencial de Newco est&aacute; en su equipo, en cada una de las personas que  desarrollan su labor profesional con nosotros. No existen m&aacute;s fronteras que  aquellas que queramos imponernos.</p>
			<p>Por ello:</p>
			<p>Si te  interesa nuestra empresa y ves posibilidades de introducci&oacute;n en tu pa&iacute;s, si  consideras que por tu experiencia y por tus conocimientos pueden aportarnos m&aacute;s  valor a Newco, si deseas representarnos, ponte en contacto con nosotros.</p>
			<p>Nuestra filosof&iacute;a  con los mercados: Adaptarnos a sus necesidades y forma de trabajar. No solo  ofrecemos nuestro producto, sino apoyo integral por parte de todos los departamentos  de nuestra empresa.</p>
			<p><a title="Contacta con Newco Printing" style="color:#009FEF;" href="contacto.php">&raquo; Contacta con nosotros</a></p>
			<? break;

		}?>
     </div>
     
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>