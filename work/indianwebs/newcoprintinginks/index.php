<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>NEWCO PRINTING INKS S.L.</title>
	<meta name="title" content="NEWCO PRINTING INKS S.L." />
	<meta name="description" content="NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="inicio">
		<div class="noticias">
     		<h5>Noticias</h5>
            <div>
            Graphics of the Americas 09 ha sido un punto de encuentro con nuestros clientes de  Latinoam&eacute;rica. En los  tres d&iacute;as de feria hemos  recibido la visita de cientos de clientes y amigos.
			<br /><br />Tambi&eacute;n  nos ha permitido iniciar el contacto con empresas que no conoc&iacute;amos.
			<br /><br />A  pesar de ser un a&ntilde;o dif&iacute;cil para la econom&iacute;a mundial, la afluencia de p&uacute;blico,  profesionales de las artes gr&aacute;ficas, nos permite ser optimistas y valorar de  forma positiva el esfuerzo que hemos realizado para estar presentes en la feria  de Miami.
			<br /><br />Desde  aqu&iacute; queremos agradecer a todos los que hab&eacute;is dedicado parte de vuestro tiempo  para visitarnos en nuestro stand e interesaros por nuestros productos.
            <br /><br /><a href="img/noticia1.jpg" target="_blank"><img src="img/noticia1.jpg" alt="Graphics of the Americas 09" /></a> <a href="img/noticia2.jpg" target="_blank"><img src="img/noticia2.jpg" alt="Graphics of the Americas 09" /></a>
            <br />
            </div>
     	</div>
        
        <h1><a title="Newco Printing" href="http://www.newcoprintinginks.com/">Newcoprinting</a></h1>
        <p><img title="Newco Printing Inks" src="http://www.newcoprintinginks.com/img/logo_inicio.gif" alt="Newco Printing Inks" /></p>
        <p>Newco est&aacute;  capacitada para suministrar barnices, tintas y productos auxiliares, en las m&aacute;s  exigentes condiciones del mercado, independientemente del m&eacute;todo de impresi&oacute;n  en que se vayan a aplicar y de la ubicaci&oacute;n geogr&aacute;fica de nuestro cliente.</p>
		<p>La experiencia  de nuestro departamento t&eacute;cnico, apoyado con los instrumentos de laboratorio  adecuados, as&iacute; como la experiencia del equipo comercial, hace que podamos  aplicar todo este conocimiento a la hora de suministrar la tinta m&aacute;s adecuada  para la industria gr&aacute;fica, atendiendo a cada caso concreto.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
     </div>
        
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>