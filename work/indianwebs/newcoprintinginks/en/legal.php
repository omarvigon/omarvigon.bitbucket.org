<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>NEWCO PRINTING INKS S.L.</title>
	<meta name="title" content="NEWCO PRINTING INKS S.L." />
	<meta name="description" content="NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="legal">
		<h1><a title="Newco Printing" href="http://www.newcoprintinginks.com/">Newcoprinting</a> &raquo; <a title="Aviso legal Newco Printing" href="legal.php">Aviso legal</a></h1>
		<h2>Aviso legal</h2>
		<p><b>&copy; Newco Printing Inks S.L. </b>/ Todos los derechos reservados.</p>
        <p><b>1.</b> <b>Newco  Printing Inks S.L.</b>, en cumplimiento de la Ley 34/2002, de 11 de julio, de servicios de la  sociedad de la informaci&oacute;n y de comercio electr&oacute;nico, le informa de que: </p>
        <ul>
          <li>Su denominaci&oacute;n social es Newco Printing Inks S. L.</li>
          <li>Su CIF es B64484512</li>
          <li>Inscrita en el Registro Mercantil de Barcelona al tomo 39.420; folio 176; hoja B-346679.</li>
          <li>Su domicilio social est&aacute; en C/ Carrer del Pintor Rusinyol n.&ordm; 14, 08213 Poliny&aacute;, Barcelona.</li>
        </ul>
        <p><b>2.</b> Newco Printing Inks S.L., informa que la Web www.newcoprintinginks.com  tiene por objeto facilitar, al p&uacute;blico en general, el conocimiento de las  actividades que esta empresa realiza y de los productos y servicios que presta.</p>
        <p><b>3.</b> Los derechos de propiedad intelectual de la Web newcoprintinginks.com y de  los distintos elementos en &eacute;l contenidos son titularidad de Newco Printing Inks S.L.</p>
        <p><b>5.</b> Corresponde a Newco Printing Inks S.L. el  ejercicio exclusivo de los derechos de explotaci&oacute;n de los mismos en cualquier  forma y, en especial, los derechos de reproducci&oacute;n, distribuci&oacute;n, comunicaci&oacute;n  p&uacute;blica y transformaci&oacute;n.</p>
        <p><b>6.</b> Se concede el derecho de reproducci&oacute;n  parcial de su contenido siempre que concurran las siguientes condiciones: </p>
        <ul>
          <li>Que sea compatible con los fines de la Web newcoprintinginks.com. </li>
          <li>Que se realice con &aacute;nimo de obtener la informaci&oacute;n contenida y no con prop&oacute;sito comercial.</li>
          <li>Que ninguno de los documentos o gr&aacute;ficos relacionados en esta Web sean modificados de ninguna manera.</li>
          <li>Que ning&uacute;n gr&aacute;fico disponible en esta Web sea utilizado, copiado o distribuido separadamente del texto o resto de im&aacute;genes que lo acompa&ntilde;an.</li>
          <li>Que sea comunicado previamente a newcoprintinginks.com.</li>
        </ul>
        <p><b>7.</b> Newco Printing Inks S.L. se reserva la  facultad de efectuar, en cualquier momento y sin necesidad de previo aviso,  modificaciones y actualizaciones de la informaci&oacute;n contenida en su Web o en la  configuraci&oacute;n y presentaci&oacute;n de &eacute;ste.</p>
        <p><b>8.</b> Newco Printing Inks S.L. no garantiza la  inexistencia de errores en el acceso al Web, en su contenido, ni que &eacute;ste se  encuentre actualizado, aunque desarrollar&aacute; sus mejores esfuerzos para, en su  caso, evitarlos, subsanarlos o actualizarlos. </p>
        <p><b>9.</b> Tanto el acceso a esta Web como el uso que  pueda hacerse de la informaci&oacute;n contenida en el mismo es de la exclusiva  responsabilidad de quien lo realiza. </p>
        <p><b>11.</b> Newco Printing Inks S.L. es titular de  los derechos de propiedad industrial referidos a sus productos y servicios, y  espec&iacute;ficamente de los relativos a la marca registrada  &quot;newcoprintinginks.com&quot;.</p>
        <p><b>12.</b> La utilizaci&oacute;n no autorizada de la  informaci&oacute;n contenida en esta Web, as&iacute; como la lesi&oacute;n de los derechos de  Propiedad Intelectual o Industrial de Newco Printing Inks S.L., dar&aacute; lugar a  las responsabilidades legalmente establecidas.</p>
        <p><b>13.</b> Newco Printing Inks S.L. no se hace  responsable de los posibles errores de seguridad que se puedan producir ni de  los posibles da&ntilde;os que puedan causarse al sistema inform&aacute;tico del usuario  (hardware y software), los ficheros o documentos almacenados en el mismo, como  consecuencia de la presencia de virus en el ordenador del usuario utilizado  para la conexi&oacute;n a los servicios y contenidos de newcoprintinginks.com, de un  mal funcionamiento del navegador o del uso de versiones no actualizadas del  mismo.</p>
     </div>
     
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>