<?php include($_SERVER['DOCUMENT_ROOT']."/includes/0cabecera.php"); 
$camino=explode("/",$_SERVER["PHP_SELF"]); 
for($i=2;$i<(count($camino)-1);$i++)
{
	$camino_enlace.=$camino[$i]."/";
	$camino_txt.="<a href='http://www.newcoprintinginks.com/en/".$camino_enlace."'>".$camino[$i]."</a> &raquo; ";
	$camino_titulo.=strtoupper($camino[$i])." ";
}
?>
    <title><?=$camino_titulo;?> Newco Printing Inks</title>
	<meta name="title" content="<?=$titulo?> Products NEWCO PRINTING INKS S.L." />
	<meta name="description" content="Products NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="Products NEWCO PRINTING INKS S.L." />

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/1cabecera.php"); ?>
   
    <div id="productos">
		<h1><?=$camino_txt;?></h1>
        <div class="mini">
     		<h5>Need more information?</h5>
            <form method="post" action="<?=$_SERVER['PHP_SELF']?>" target="_blank">
       		<input type="hidden" name="ruta" value="<?=$_SERVER['SCRIPT_NAME'];?>"/>
            <textarea name="comentarios" rows="14"></textarea>
       		<label>E-Mail:</label>
            <input type="text" name="email" class="bte" maxlength="64"/>
            <input type="submit" value="   SEND   " />
            </form>
     	</div>        
        
        <div class="derecha">
        
        <?php if($_POST["email"]) 
		{ 	
			foreach($_POST as $nombre_campo => $valor)
 				$cuerpo=$cuerpo."<b>".$nombre_campo ."</b> : ".$valor."<br>";
			if(mail("newco@newcoprintinginks.com","Informacion de productos web (EN)",$cuerpo,"MIME-Version:1.0\r\nContent-type:text/html\r\nBcc:omar@indianwebs.com\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
				echo "<p>&nbsp;</p><p>The request has been sent.</p><p>We will contact you.</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";
		
		}
		?>
        
        <h4><a href="http://www.newcoprintinginks.com/en/products/varnishes/">VARNISHES</a></h4>
        <?php if(strstr($_SERVER['PHP_SELF'],"/varnishes/")) { ?>
        <ul class="lista">
            <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/over-printing/">Over-printing Varnishes</a></li>
				<?php if(strstr($_SERVER['PHP_SELF'],"/varnishes/over-printing/")) { ?>
                <ul class="sublista">
			    <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/over-printing/protecta/">Protecta F Glossy Varnish</a></li>
			    	
					<?php if(strstr($_SERVER['PHP_SELF'],"/over-printing/protecta/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/over-printing/matt/">Matt Varnish</a></li>
			    	
					<?php if(strstr($_SERVER['PHP_SELF'],"/over-printing/matt/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/over-printing/glossy/">Semi-Glossy Varnish</a></li>
                	
					<?php if(strstr($_SERVER['PHP_SELF'],"/over-printing/glossy/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query</div>
                    <? } ?>
                    
              	</ul>
                <? } ?>
                
            <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/acrylics/">Acrylic Varnishes</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/varnishes/acrylics/")) { ?>
                <ul class="sublista">
              	<li><a href="http://www.newcoprintinginks.com/en/products/varnishes/acrylics/high-glossy/">Newcryl high glossy Varnish</a></li>
              			
					<?php if(strstr($_SERVER['PHP_SELF'],"/acrylics/high-glossy/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                        
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/acrylics/matt/">Newcryl Mat Varnish</a></li>
              		
                    <?php if(strstr($_SERVER['PHP_SELF'],"/acrylics/matt/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/acrylics/semi-glossy/">Newcryl Semi-gloosy Varnish</a></li>
              		
                	<?php if(strstr($_SERVER['PHP_SELF'],"/acrylics/semi-glossy/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/acrylics/primer/">Newcryl primer Varnish</a></li>
                
                	<?php if(strstr($_SERVER['PHP_SELF'],"/acrylics/primer/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/acrylics/antislip/">Newcryl Antislip Varnish</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/acrylics/antislip/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                    
                </ul>
                <? } ?>
                
            <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/uv/">UV Varnishes</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/varnishes/uv/")) { ?>
                <ul class="sublista">
              	<li><a href="http://www.newcoprintinginks.com/en/products/varnishes/uv/adhesive-cold-foil/">Cold Foil Adhesive</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/uv/adhesive-cold-foil/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/uv/varnish/"> UV Varnish</a></li>
              		
                    <?php if(strstr($_SERVER['PHP_SELF'],"/uv/varnish/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                    
                <li><a href="http://www.newcoprintinginks.com/en/products/varnishes/uv/stamping/">Stamping UV Varnish</a></li>
              		
					<?php if(strstr($_SERVER['PHP_SELF'],"/uv/stamping/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
       
                </ul>
                <? } ?>
        </ul>
        <? } ?>
        
        <h4><a href="http://www.newcoprintinginks.com/en/products/inks/">INKS</a></h4>
        <?php if(strstr($_SERVER['PHP_SELF'],"/inks/")) { ?>
        <ul class="lista">
            <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/">Conventional Inks</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/inks/conventional/")) { ?>
                <ul class="sublista">
            		<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/">Conventional Offset</a></li>
						<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/conventional-offset/")) { ?>
                        <ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/econew-fresh/">Econew Fresh</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/econew-fresh/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/nsp-new/">NSP New</a></li>
                   	 		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/nsp-new/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
						  <? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/npx-mayor/">NPX Mayor</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/npx-mayor/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/high-rub/">High Rub</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/high-rub/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/novaplast/">Novaplast (PVC)</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/novaplast/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/litho-pantone/">Litho F Pantone (Pantones Base)</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/litho-pantone/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                            
                        <li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/conventional-offset/negros/">Special Blacks (Perfecta Black, Edition Black Hight Quality, Edition Black, Cold Set Black)</a></li>
                    		
							<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/negros/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                        </ul>
				<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/dry-offset/">Dry Offset</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/dry-offset/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/dry-offset/novaless/">Novaless</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/dry-offset/novaless/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	</ul>
           	    <? } ?>
                	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/offset-uv/">UV Offset</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/offset-uv/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/offset-uv/newlam/">Newlam</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-uv/newlam/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/offset-uv/newpap/">Newpap</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/offset-uv/newpap/")) { ?>
                        	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/waterless/">UV Waterless</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/waterless/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/waterless/uv-waterless">UV Waterless</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/waterless/uv-waterless")) { ?>
                        	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
							<? } ?>
                    	</ul>
                    	<? } ?>
               		<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-water/">Flexo Water Base (flexography)</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/flexo-water/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-water/flexography">Wide Web flexography</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/flexography/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        	<? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-water/narrow/">Narrow Web</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/narrow/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        	<? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-water/newfilm/">Newfilm</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/newfilm/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        	<? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-water/newtherm/">Newtherm</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/newtherm/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        	<? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-water/newenvelope/">Newenvelope</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/newenvelope/")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        	<? } ?>
                    	</ul>
                        
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-uv/">UV Flexography</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/flexo-uv/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/flexo-uv/newflex/">Newflex</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-uv/newflex/")) { ?>
                        	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/uv-screen/">UV Screen</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/uv-screen/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/uv-screen/newscreen/">Newscreen UV</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-screen/newscreen/")) { ?>
                        	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
                	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/metal-decorating/">Metal Decorating</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional/metal-decorating/")) { ?>
                    	<ul>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/metal-decorating/inx-cure/">Inx Cure (conventional offset)</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/metal-decorating/inx-cure/")) { ?>
                        	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/metal-decorating/poly-novar/">Poly Novar (aluminium and steel Offset)</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/metal-decorating/poly-novar/")) { ?>
                        	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	<li><a href="http://www.newcoprintinginks.com/en/products/inks/conventional/metal-decorating/tp-cure/">Inx Cure (UV Offset)</a></li>
                        	<?php if(strstr($_SERVER['PHP_SELF'],"/metal-decorating/tp-cure")) { ?>
                            <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                            <? } ?>
                    	</ul>
                    	<? } ?>
          </ul>       
                 <? } ?>
            <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/">Special and Security Inks</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/inks/specials/")) { ?>
                <ul class="sublista">
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/fluorescent/">Fluorescent Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/fluorescent/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/fluorescent/uv/">UV Fluorescentes Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/fluorescent/uv/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/fluorescent/ir/">IR Fluorescentes Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/fluorescent/ir/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/phosphorescent/">Phosphorescent Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/phosphorescent/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/phosphorescent/conventional/">Conventional Phosphorescent</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/phosphorescent/conventional")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/phosphorescent/security/">Security Phosphorescent</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/phosphorescent/security")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/photocrhomic/">Photocrhomic Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/photocrhomic/")) { ?>
                    <ul class="sublista">
                    </ul>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/iridiscent/">Iridiscent Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/iridiscent/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/iridiscent/iridiscent/">Iridiscent&nbsp;Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/iridiscent/iridiscent/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/iridiscent/irionew/">Irionew Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/iridiscent/irionew/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/iridiscent/newcrom/">Newcrom Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/iridiscent/newcrom/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/tir/">Transparent IR/no Transparent Ir Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/tir/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/tir/transparent/">Transparents IR Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/tir/transparent/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/tir/no-transparent/">No transparents  IR Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/tir/no-transparent/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/thermocrhomic/">Thermocrhomic Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/thermocrhomic/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/thermocrhomic/15/">Thermocrhomic 15&ordm;C Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/thermocrhomic/15/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/thermocrhomic/31/">Thermocrhomic 31&ordm;C Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/thermocrhomic/31/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/thermocrhomic/47/">Thermocrhomic 47&ordm;C Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/thermocrhomic/47/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/coin-inks/">Coin Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/coin-inks/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/coin-inks/transparent/">Transparent coin Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/coin-inks/transparent/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/coin-inks/opaque/">Opaque Coin Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/coin-inks/opaque/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/fugitive/">Fugitive Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/fugitive/")) { ?>
                    <ul class="sublista">
                    </ul>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/water-mark/">Water Mark Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/water-mark/")) { ?>
                    <ul class="sublista">
                    </ul>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/reactive/">Reactive Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/reactive/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/reactive/water/">Water Reactive Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/reactive/water/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/reactive/solvent/">Solvent Reactive Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/reactive/solvent/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/conductive/">Conductive Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/conductive/")) { ?>
                    <ul class="sublista">
                    </ul>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/magnetic/">Magnetic Inks</a></li>
                    <?php if(strstr($_SERVER['PHP_SELF'],"/specials/magnetic/")) { ?>
                    <ul class="sublista">
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/magnetic/high/">High Coercitivy Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/specials/magnetic/high/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    <li><a href="http://www.newcoprintinginks.com/en/products/inks/specials/magnetic/low/">Low Coercitivy Inks</a></li>
                    	<?php if(strstr($_SERVER['PHP_SELF'],"/specials/magnetic/low/")) { ?>
                    	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                        <? } ?>
                    </ul>
                    <? } ?>
                </ul> 
                <? } ?>
        </ul>
        <? } ?>
        
        <h4><a href="http://www.newcoprintinginks.com/en/products/auxiliars/">AUXILIAR PRODUCTS</a></h4>
        <?php if(strstr($_SERVER['PHP_SELF'],"/products/auxiliars/")) { ?>
        <ul class="lista">
            <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/flexo-water/">Flexography Water Base</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliars/flexo-water/")) { ?>
                <ul class="sublista">
				<li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/flexo-water/refresher/">Refresher Additive</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/refresher/")) { ?>
                	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
				<li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/flexo-water/antifoaming/">Antifoaming agent</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/antifoaming/")) { ?>
                	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
				<li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/flexo-water/cleaner/">Cleaner product</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/flexo-water/cleaner/")) { ?>
                	<div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
			  	</ul>
                <? } ?>
            <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/conventional-offset/">Conventional Offset</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliars/conventional-offset/")) { ?>
                <ul class="sublista">
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/conventional-offset/antidrying/">Anti-drying Spray</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/antidrying/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/conventional-offset/dryer/">Dryer Additive</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/dryer/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/conventional-offset/speeddry/">Speeddry</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/speeddry")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/conventional-offset/viscosity">Viscosity reduce</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/conventional-offset/viscosity")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
					<? } ?>
              	</ul>
                <? } ?>
            <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/"> UV Inks</a></li>
            	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliars/uv-inks/")) { ?>
                <ul class="sublista">
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/viscosity-reducer/">Viscosity reducer</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-inks/viscosity-reducer/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/antitack/">UV Antitack</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-inks/antitack/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/antirubbing/">UV Anti-rubbing</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-inks/antirubbing/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/photoinitiator-standard/">Photoinitiator standard additive</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-inks/photoinitiator-standard")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/photoinitiator-high/">Photoinitiator  high shade colours additive</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-inks/photoinitiator-high/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/leveler-additive/">Leveler additive</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/auxiliars/uv-inks/leveler-additive/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
                <li><a href="http://www.newcoprintinginks.com/en/products/auxiliars/uv-inks/uv-cleaner/">UV Cleaner</a></li>
                	<?php if(strstr($_SERVER['PHP_SELF'],"/uv-inks/uv-cleaner/")) { ?>
                    <div class="intro1">For information about a product, you can use the box on the right side to raise their query.</div>
                    <? } ?>
              	</ul>                
                <? } ?>
      	</ul>
        <? } ?>
    	</div>
    </div>
	 
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>