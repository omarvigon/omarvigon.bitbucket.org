<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>Sitemap NEWCO PRINTING INKS S.L.</title>
	<meta name="description" content="Sitemap NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="inicio">        
        <h1><a title="Newco Printing" href="http://www.newcoprintinginks.com/en/">Newcoprinting</a></h1>
        <h2>Sitemap</h2>
        <dl>
        <dt><a title="Company" href="http://www.newcoprintinginks.com/en/company.php">Company</a></dt>
        	<dd><a title="Newco Printing" href="http://www.newcoprintinginks.com/en/company.php?sec=values">Values</a></dd>
            <dd><a title="Newco Printing" href="http://www.newcoprintinginks.com/en/company.php?sec=objective">Objective</a></dd>
            <dd><a title="Newco Printing" href="http://www.newcoprintinginks.com/en/company.php?sec=commitment">Commitment</a></dd>

        <dt><a title="Productos" href="http://www.newcoprintinginks.com/en/products/">Products</a></dt>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/products/barnices/">VARNISHES</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/products/tintas/">INKS</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/products/auxiliares/">AUXILIAR PRODUCTS</a></dd>
   
        <dt><a title="Expansion" href="http://www.newcoprintinginks.com/en/expansion.php">International Expansion </a></dt>
        	<dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/en/expansion.php?sec=where">Where we are settle</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/en/expansion.php?sec=work">Work with us</a></dd>

        <dt><a title="Contact" href="http://www.newcoprintinginks.com/en/contact.php">Contact</a></dt>
        	<dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/en/contact.php?sec=data">Company Data</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/en/contact.php?sec=email">E-Mail</a></dd>
            <dd><a title="Newco Printing Inks" href="http://www.newcoprintinginks.com/en/contact.php?sec=location">Location</a></dd>

        <dt><a title="Legal" href="http://www.newcoprintinginks.com/en/legal.php">Legal</a></dt>
        <dt><a title="Sitemap" href="http://www.newcoprintinginks.com/en/map.php">Sitemap</a></dt>
        </dl>
     </div>
        
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>