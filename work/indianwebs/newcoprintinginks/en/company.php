<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0cabecera.php");?>
    <title>NEWCO PRINTING INKS S.L.</title>
	<meta name="title" content="NEWCO PRINTING INKS S.L." />
	<meta name="description" content="NEWCO PRINTING INKS S.L." />
	<meta name="keywords" content="NEWCO PRINTING INKS S.L." />
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/1cabecera.php");?>
   
    <div id="empresa">
		<div id="subsec">
        	<div><a title="Values Newco Printing" href="company.php?sec=values" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">Values</a></div><br />
            <div><a title="Objective Newco Printing" href="company.php?sec=objective" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">Objective</a></div><br />
            <div><a title="Commitment Newco Printing" href="company.php?sec=commitment" onmouseover="anim(this,'azul','on')" onmouseout="anim(this,'azul','off')">Commitment</a></div>
        </div>
        <h1><a title="Newco Printing" href="index.php">Newcoprinting</a> &raquo; <a title="Newco Printing" href="company.php">Company</a> &raquo; 
        
        <?php switch($_GET["sec"])
		{
			default:
			case "values":	?>
            <a title="Valores Newco Printing" href="company.php?sec=values">Values</a></h1>
			<h2>Values</h2>
			<p><em>&bull; Quality &amp; Service</em> permanently we are updated about the different aspects that can  influence on the application of our products, and then we use our knowledge to  help every customer to obtain the best result at their daily activity.</p>
			<p><em>&bull;Individual Innovation &amp; initiative</em>, the professional and personal growth of our entire collaborators,  developing our daily job as a team makes us very efficiency to achieve our  commitments with our national and international customers.</p>
			<p><em>&bull;Live and work with integrity</em> and respecting the environment, using ethically the science and lean  on sense of common to be sure that our decision will improve the environment,  the community, our employers, suppliers and customers.</p>
			<? break;
			
			case "objective": ?>
            <a title="Objective Newco Printing" href="company.php?sec=objective">Objective</a></h1>
            <h2>Objective</h2>
	        <p>Contribute to the success of our clients in their printing job,  supplying the right formulation ink on the correct time with an adequate price.</p>
        	<p>Our strategy:</p>
    	    <ul>
		  	<li>Offer to the market higher  products on his quality formulation, intrinsic properties and product applications  to obtain better printing image with higher efficiency.</li>
		  	<li>Offer to the market higher  products on quality service from our first customer contact through our  company. All our team collaborate to increase the quality of our products due  to our daily work.</li>
		  	<li>Applying our 20 years knowledge  on the printing business plus the knowledge of our suppliers allow us to offer  a constant improvement in our quality that we translate to our customers.</li>
	        </ul>
			<? break;
			
			case "commitment": ?>
            <a title="Commitment Newco Printing" href="company.php?sec=commitment">Commitment</a></h1>
            <h2>Commitment</h2>
	        <p>Due to Newco philosophy and the nature of some of the products that  we develop and supply, specially the ones that are addressed to be printed on  Security documents, it is very important for us to keep a very high level of confidentiality  with our clients. From our side we are forced to carry out some rules to  guaranty the level of discretion and security that the market request:</p>
    	    <ul>
			<li>Strict fulfilment of the Personal Data Protection Law and all  current law related with the protection of personal information.</li>
			<li>Development of a security policy and internal behaviour code.</li>
			<li>Specify our services conditions through a contract.</li>
			<li>Maximum transparency and discretion during our service performance  to our customers.</li>
	        </ul>
			<? break;
		}?>
     </div>
     
<?php include($_SERVER["DOCUMENT_ROOT"]."/includes/0piecera.php");?>