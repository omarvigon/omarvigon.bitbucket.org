<?php include("includes/1cabecera.php");?>
    
    <div id="cuerpo">
    	<h1>Aviso Legal</h1>
		<div style="height:300px;overflow:auto;">
        <dl>
        <dt>POLITICA DE PRIVACIDAD</dt>
        <dd>La presente Pol&iacute;tica de Privacidad es aplicable a este sitio web, del que es titular CODINA - CAVESTANY abogados asociados, SCP. Al acceder y utilizar este sitio web, Ud. esta aceptando los t&eacute;rminos y condiciones de esta Pol&iacute;tica de Privacidad.</dd>
		<dt>SOLICITUD Y TRATAMIENTO DE DATOS</dt>
        <dd>Derecho de Informaci&oacute;n: CODINA - CAVESTANY abogados asociados SCP ha adoptado las medidas de &iacute;ndole t&eacute;cnica y organizativa que garantizan la seguridad de los datos de car&aacute;cter personal y evitan su alteraci&oacute;n, p&eacute;rdida, tratamiento o acceso no autorizado, habida cuenta del estado de la tecnolog&iacute;a, la naturaleza de los datos almacenados y los riesgos a que se exponen, todo ello de conformidad con lo establecido en el Real Decreto 994/1999, de 11 de junio.</dd>		<!--<dt>EXACTITUD DE LOS DATOS RECOGIDOS<br />CODINA - CAVESTANY abogados asociados SCP por propia iniciativa o a petici&oacute;n de Ud., rectificar&aacute; o borrar&aacute; cualquier dato personal incompleto, inexacto o anticuado conservado con relaci&oacute;n al uso de este Sitio Web.</dt>-->
		<dt>Exactitud de los datos recogidos</dt>
        <dd>CODINA - CAVESTANY abogados asociados SCP por propia iniciativa o a petici&oacute;n de Ud., rectificar&aacute; o borrar&aacute; cualquier dato personal incompleto, inexacto o anticuado conservado con relaci&oacute;n al uso de este Sitio Web.</dd>
        <dt>SEGURIDAD</dt>
        <dd>Debe tener en cuenta que, aunque siempre existen riesgos asociados a la comunicaci&oacute;n de datos personales, ya sea en persona, por tel&eacute;fono o a trav&eacute;s de Internet, y ning&uacute;n sistema tecnol&oacute;gico es completamente seguro, a prueba de intromisiones o de ataques, CODINA - CAVESTANY abogados asociados SCP aplica las medidas necesarias para impedir y reducir al m&iacute;nimo posible los riesgos de acceso no autorizado a su informaci&oacute;n personal.</dd>
		<dt>Identificaci&oacute;n de visitantes &quot;Cookies&quot;</dt>
        <dd>CODINA - CAVESTANY abogados asociados SCP no utiliza cookies para acceder a informaci&oacute;n relativa a los usuarios del sitio web.</dd>
        <dt>Derecho de Oposici&oacute;n, Correcci&oacute;n y Actualizaci&oacute;n de Datos</dt>
        <dd>El  usuario tiene derecho a acceder a esta informaci&oacute;n, a rectificarla si  los datos son err&oacute;neos y a darse de baja del fichero. En este ultimo  caso CODINA - CAVESTANY abogados asociados SCP cancelar&aacute; la informaci&oacute;n  autom&aacute;ticamente. Estos derechos pueden hacerse efectivos por medio de escrito dirigido a:<br />
		<br />CODINA - CAVESTANY abogados asociados SCP<br />DEPARTAMENTO ATENCI&Oacute;N AL CLIENTE<br />Av. Diagonal, 453bis, entlo. 08036 - Barcelona</dd>
        <br />
        <dd>El tratamiento de los datos personales y el env&iacute;o de comunicaciones por  medios electr&oacute;nicos est&aacute;n ajustados a la normativa establecida en la  Ley Org&aacute;nica 15/1999,13 de diciembre, de Protecci&oacute;n de Datos de  Car&aacute;cter Personal y en la Ley 34/2002, de 11 de julio, de Servicios de  la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico.</dd>
        </dl>
        </div>
    </div>
    
<?php include("includes/2piecera.php");?>