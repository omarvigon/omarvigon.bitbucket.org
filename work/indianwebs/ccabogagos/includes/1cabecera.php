<?php include("0cabecera.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
	<meta name="verify-v1" content="tlT/N4Tiah/5ZWG2wmfCGVhRIGp6SJVWRXZmBPPVdlM=" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Despacho de Abogados de Arturo Codina, Pablo Cavestany y asociados</title>
	<meta name="description" content="Web oficial del despacho de abogados Arturo Codina y Pablo Cavestany" />
	<meta name="keywords" content="abogados, Arturo Codina, Pablo Cavestany, cc abogados, abogados barcelona" />
	<meta name="language" content="es" />
	<link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
	<script language="javascript" type="text/javascript" src="includes/_funciones.js"></script>
</head>
<body>

<div id="central">
	<div id="idiomas">
    	<a title="Castellano" href="<?=$_SERVER['PHP_SELF']?>">Castellano</a> | 
        <a title="Catala" href="<?=$_SERVER['PHP_SELF']?>?lang=cat">Catal&agrave;</a>
    </div>
    <div id="direccion">
    	<p>Av.Diagonal 453 bis, entlo 08036 Barcelona<br />Tel. (+34) 93 439 08 00  - Fax (+34) 93 439 17 05<br /><a href="mailto:info@cc-abogados.com">info@cc-abogados.com</a></p>
    </div>
    <a title="Codina y Cavestany Abogados" href="index.php<?=$parametro?>"><img src="img/logo.gif" class="imglogo" alt="Codina y Cavestany Abogados" /></a>
    <div id="botonera">
    	<a title="Quienes" href="quienes.php<?=$parametro?>"><img src="img/bt_quienes_<?=$idioma?>.gif" class="bot1" alt="Quienes somos" /></a>
        <a title="Areas" href="areas.php<?=$parametro?>"><img src="img/bt_areas_<?=$idioma?>.gif" class="bot2" alt="Areas" /></a>
        <a title="Equipo" href="equipo.php<?=$parametro?>"><img src="img/bt_equipo_<?=$idioma?>.gif" class="bot3" alt="Equipo" /></a>
        <a title="Contacto" href="contacto.php<?=$parametro?>"><img src="img/bt_contacto_<?=$idioma?>.gif" class="bot4" alt="Contacto" /></a>
	</div>