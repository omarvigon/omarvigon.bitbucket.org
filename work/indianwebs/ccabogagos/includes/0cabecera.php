<?php
if($_GET["lang"]=="cat")
{
	$parametro="?lang=cat";
	$idioma="cat";
}
else
{
	$idioma="es";	
}
define("INICIO_es","<p>Bienvenidos a <b>Codina  Cavestany abogados asociados</b>, un despacho mutidisciplinar  fundado en 1988, integrado por profesionales de reconocido prestigio y con una  clara vocaci&oacute;n de servicio al cliente.</p>
<p>Desde aqu&iacute; podr&aacute; acceder a una  breve presentaci&oacute;n del despacho, conocer sus &aacute;reas de especialidad  y comunicarse con los miembros del equipo.</p>");
define("INICIO_cat","<p>Benvinguts a <b>Codina Cavestany abogados asociados</b>, un despatx multidisciplinar fundat l&rsquo;any 1988, integrat per  professionals de reconegut prestigi i amb una clara vocaci&oacute; de servei al  client.</p>
<p>Des de aqu&iacute; podr&agrave; accedir a una breu presentaci&oacute; del despatx, con&egrave;ixer  les seves &agrave;rees d&rsquo;especialitat i comunicar-se amb els membres del equip. </p>");
define("QUIENES_es","Qui&eacute;nes somos");
define("QUIENES_cat","Qui som");
define("QUIENES_txt_es","<p><b>CODINA-CAVESTANY Abogados Asociados</b>, es un despacho fundado en el a&ntilde;o 1988 integrado por nueve  abogados que cubren distintas especialidades con particular dedicaci&oacute;n al  Derecho de empresa.</p>
<p>Los principios que inspiran al despacho y a sus profesionales son:</p>
<ul>
<li>Una clara vocaci&oacute;n hacia el cliente, buscando la soluci&oacute;n m&aacute;s adecuada a cada caso y una atenci&oacute;n personalizada.</li>
<li>Un enfoque multidisciplinar donde cada profesional implicado aporta su experiencia para la resoluci&oacute;n de los problemas.</li>
<li>Un compromiso con la calidad en la tramitaci&oacute;n y prestaci&oacute;n del servicio ofrecido por los abogados de la firma, avalado por una s&oacute;lida formaci&oacute;n continuada.</li>
</ul>
<p>Entendemos que s&oacute;lo vertebrando el ejercicio de la abogac&iacute;a alrededor de estos principios es  posible ofrecer a nuestros clientes un servicio integral que resuelva sus  problemas legales.</p>");
define("QUIENES_txt_cat","<p><b>CODINA-CAVESTANY Abogados Asociados</b>, &eacute;s un despatx fundat  l&acute;any 1988 integrat per nou advocats que cobreixen diferents especialitats amb  particular dedicaci&oacute; al Dret d&rsquo;empresa.</p>
<p>Els principis que inspiren el despatx i als seus  professionals s&oacute;n:</p>
<ul>
<li>Una clara vocaci&oacute; envers el client, buscant la soluci&oacute;  m&eacute;s adequada a cada cas i una atenci&oacute; personalitzada. </li>
<li>Un enfoc  multidisciplinar on cada professional implicat aporta la seva experi&egrave;ncia       per la resoluci&oacute; dels problemes.</li>
<li>Un comprom&iacute;s amb la qualitat en la tramitaci&oacute; i prestaci&oacute; del servei ofert per els advocats de la       firma, avalat per una s&ograve;lida formaci&oacute; continuada.</li>
</ul>
<p>Entenem que nom&eacute;s vertebren l&rsquo;exercici de l&rsquo;advocacia  entorn a aquests principis &eacute;s fa possible oferir als nostres clients un servei  integra que resolgui els seus problemes legals. </p>");

define("AREAS_es","&Aacute;reas de especialidad");
define("AREAS_cat","&Agrave;rees d'especialitat");
define("AREAS_1_es","El despacho cuenta con una dilatada experiencia en Derecho concursal, societario y contrataci&oacute;n  mercantil, tanto desde una perspectiva de asesoramiento y operaciones  societarias como en procedimientos concursales ante los juzgados y tribunales.");
define("AREAS_1_cat","El despatx compta amb una dilatada experi&egrave;ncia en Dret  Concursal, societari i contractaci&oacute; mercantil, tant des de una perspectiva  d&rsquo;assessorament i operacions societ&agrave;ries com en procediments concursals davant jutjats  i tribunals.");
define("AREAS_2_es","El departamento asesora en materias de responsabilidad, contractual y  extracontractual, derecho inmobiliario, propiedad intelectual, cuestiones de  derecho sucesorio, contrataci&oacute;n civil e interviene en procedimientos  relacionados con estas materias ante los juzgados y tribunales.");
define("AREAS_2_cat","El departament assessora en mat&egrave;ries  de responsabilitat, contractual y extracontractual, dret immobiliari, propietat  intel&middot;lectual, q&uuml;estions de dret successori, contractaci&oacute; civil i interv&eacute; en  procediment relacionats amb aquestes mat&egrave;ries davant jutjats i tribunals.");
define("AREAS_3_es","El departamento ofrece asesoramiento en materias como la negociaci&oacute;n colectiva e intermediaci&oacute;n en  conflictos colectivos, contrataci&oacute;n  laboral y despidos individuales, contratos de alta direcci&oacute;n, prevenci&oacute;n de riesgos laborales y auditorias laborales e interviene en procedimientos ante la jurisdicci&oacute;n social.");
define("AREAS_3_cat","El departament ofereix assessorament en mat&egrave;ries  com la negociaci&oacute; col&middot;lectiva i  intermediaci&oacute; en conflictes col&middot;lectius, contractaci&oacute; laboral i acomiadaments individuals, contractes d&rsquo;alta direcci&oacute;, prevenci&oacute; de riscos laborals i auditories laborals i interv&eacute; en procediment davant la jurisdicci&oacute; social.");
define("AREAS_4_es","El Derecho penal que ofrece el despacho abarca la defensa o acusaci&oacute;n particular en procedimientos  relacionados con delitos econ&oacute;micos (apropiaci&oacute;n indebida, estafa, insolvencias  punibles), delitos relacionados con las nuevas tecnolog&iacute;as o contra las  relaciones familiares.");
define("AREAS_4_cat","El Dret Penal que ofereix el bufet als seus clients compren  la defensa o acusaci&oacute; particular en procediments relacionats amb delictes econ&ograve;mics  (apropiaci&oacute; indeguda, estafa, insolv&egrave;ncies punibles), delictes relacionats amb  les noves tecnologies o contra les relacions familiars.");
define("CONTACTO_es","Contacto");
define("CONTACTO_cat","Contacte");
define("EQUIPO_es","Equipo");
define("EQUIPO_cat","Equip");
define("EQUIPO_socios_es","Socios");
define("EQUIPO_socios_cat","Socis");
define("EQUIPO_colabor_es","Colaboradores");
define("EQUIPO_colabor_cat","Col.&middot;laboradors");
define("EQUIPO_admin_es","Administraci&oacute;n");
define("EQUIPO_admin_cat","Administraci&oacute;");
?>