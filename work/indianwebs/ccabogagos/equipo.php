<?php include("includes/1cabecera.php");?>
    
    <div id="cuerpo">
    	<h1><?=constant("EQUIPO_".$idioma);?></h1>
		<h2><?=constant("EQUIPO_socios_".$idioma);?></h2>
        <ul>
        	<li><a href="mailto:acodina@cc-abogados.com?subject=www.cc-abogados.com">Arturo Codina Serra</a></li>
			<li><a href="mailto:pcavestany@cc-abogados.com?subject=www.cc-abogados.com">Pablo Cavestany Freixas</a></li>
			<li><a href="mailto:pgarcia@cc-abogados.com?subject=www.cc-abogados.com">Pablo Garc&iacute;a Suqu&eacute;</a></li>
			<li><a href="mailto:adiaz@cc-abogados.com?subject=www.cc-abogados.com">Albert D&iacute;az P&eacute;rez</a></li>
			<li><a href="mailto:jestalella@cc-abogados.com?subject=www.cc-abogados.com">Jordi Estalella del Pino</a></li>
			<li><a href="mailto:cmonzo@cc-abogados.com?subject=asunto%20del%20mensaje">Carmen Monz&oacute; L&oacute;pez</a></li>
        </ul>
        <h2><?=constant("EQUIPO_colabor_".$idioma);?></h2>
        <ul>
        	<li><a href="mailto:nfurquet@cc-abogados.com?subject=www.cc-abogados.com">Noelia Furquet Monasterio</a></li>
            <li><a href="mailto:a.amigo@cc-abogados.com?subject=www.cc-abogados.com">&Aacute;lvaro Amig&oacute; Bengoechea</a></li>
            <li><a href="mailto:emagri@cc-abogados.com?subject=www.cc-abogados.com">Eduardo Magri Almirall</a></li>
        </ul>
        <h2><?=constant("EQUIPO_admin_".$idioma);?></h2>
        <ul>
            <li><a href="mailto:carme@cc-abogados.com?subject=www.cc-abogados.com">Carme Canaleta</a></li>
			<li><a href="mailto:consol@cc-abogados.com?subject=www.cc-abogados.com">Consol D&iacute;ez</a></li>
			<li><a href="mailto:pcortina@cc-abogados.com?subject=www.cc-abogados.com">Pilar Cortina</a></li>
        </ul>
    </div>
    
<?php include("includes/2piecera.php");?>