<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>TDF : Empresa : Historia</h2>
  <p><b>&raquo; A&ntilde;o  1997.-</b></p>
  <p>Se Constituye TDF, centrando su actividad en el &aacute;mbito de la  rehabilitaci&oacute;n de exteriores.</p>
  <p><b>&raquo; A&ntilde;o 2002.- </b></p>
  <p>Despu&eacute;s de a&ntilde;os de  crecimiento sostenido, implantaci&oacute;n de la marca y una fuerte integraci&oacute;n en el sector y en el mercado, TDF abre una nueva  l&iacute;nea de negocio, siendo esta la rehabilitaci&oacute;n integral de edificios, reforma y adecuaci&oacute;n de interiores, dot&aacute;ndonse tanto de, recursos econ&oacute;micos  como humanos que dicha actividad requiere.</p>
  <p><b>&raquo; A&ntilde;o 2005.-</b></p>
  <p>TDF es adjudicataria   para  la edificaci&oacute;n de un edificio de 4 viviendas en la calle Madrazo, inici&aacute;ndose  as&iacute; nuestra actividad en el sector de la Edificaci&oacute;n de obra nueva.</p>
  <p><b>&raquo; A&ntilde;o 2006.-</b></p>
  <p>Se Constituye TDF  Proyectos e instalaciones, empresa que se dedica a la realizaci&oacute;n de instalaciones el&eacute;ctricas,  mec&aacute;nicas, climatizaci&oacute;n, gas telecomunicaciones, y mantenimiento de centros.</p>
  <p>TDF proyectos e  instalaciones da servicio tanto a TDF como  a otras empresas del sector como pueden ser ingenier&iacute;as, despachos de  arquitectura, particulares, hoteles, etc.</p>
  <p>La integraci&oacute;n de TDF proyectos e instalaciones nos da la posibilidad de controlar  los procesos de instalaci&oacute;n de una manera m&aacute;s directa y eficaz, dar un  asesoramiento t&eacute;cnico de calidad y dotar de fluidez los canales de comunicaci&oacute;n  entre constructora e instaladora.</p>
  <p>Esta empresa est&aacute; dirigida  por un Ingeniero t&eacute;cnico con gran experiencia y capacitaci&oacute;n en el campo de las  instalaciones y el mantenimiento.</p>
  <p><b>&raquo; A&ntilde;o 2008.-</b></p>
  <p>TDF compra el 50% de ODL  empresa especializada en Edificaci&oacute;n y construcci&oacute;n de estructuras.</p>
  <p>&nbsp;</p>
</div>
<?php include("../00piecera.php");?>