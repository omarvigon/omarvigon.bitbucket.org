<?php include("../00cabecera.php");?>

<div class="centro">
   <br>
  <h2>TDF : Empresa : Compromiso Social</h2>
  <p><b>Calidad</b></p>
  <p>TDF  tiene como objetivo la realizaci&oacute;n de obras con un alto nivel de calidad asegurando  que se satisfagan todas les necesidades del sus clientes y el entorno, y que la  organizaci&oacute;n se beneficie del valor a&ntilde;adido creado en el desarrollo de  relaci&oacute;n con el cliente. Nuestras actuaciones se basan en la independencia,  imparcialidad legalidad, compromiso ambiental, desarrollo sostenible y la &eacute;tica  profesional de la organizaci&oacute;n.</p>
  <p>Nuestro  cliente puede sentirse seguro de encontrar la m&aacute;xima fiabilidad en la respuesta  desde el primer contacto hasta la finalizaci&oacute;n  del servicio.</p>
  <p>El objetivo de  la pol&iacute;tica de gesti&oacute;n de nuestra empresa &eacute;s consolidar la relaci&oacute;n con el cliente y le entorno mediante la mejora continua  del servicio.</p>
  <p>Creemos que estos son principios clave para desarrollar una gesti&oacute;n empresarial adecuada  a las necesidades de nuestros clientes, trabajadores, colaboradores y el  entorno.</p>
  <p><b>Seguridad y salud</b></p>
  <p>TDF realiza una  continua identificaci&oacute;n de riesgos, evaluaci&oacute;n de peligros y implantaci&oacute;n de medidas de control necesarias  para eliminar o minimizar los riesgos que comporta nuestra actividad</p>
  <p>La  siniestralidad en nuestra empresa  ha seguido una evoluci&oacute;n descendiente en los &uacute;ltimos a&ntilde;os gracias a la  formaci&oacute;n y compromiso de todos y cada  uno de nuestros trabajadores, que entienden que uno de los valores m&aacute;s importantes de TDF es la su seguridad.</p>
  <p>Todos los  nuevos trabajadores cuando se incorporar a nuestra empresa, son formados e informados de cada un de los riesgos  de su lugar de trabajo, y se pone a su alcance todos los recursos humanos y  materiales necesarios para cumplir con la pol&iacute;tica de seguridad de la empresa.</p>
  <p>La empresa est&aacute; en fase de implantaci&oacute;n de un Sistema de Gesti&oacute;n de la Prevenci&oacute;n de Riesgos  Laborales.</p>
  <p><b>Medio Ambiente</b></p>
  <p>TDF  se encuentra implicada directamente en el esfuerzo de hacer compatible el  desarrollo econ&oacute;mico con la protecci&oacute;n del medio ambiente, con objeto de  proporcionar a las futuras generaciones un entorno pr&oacute;spero y saludable. La  aplicaci&oacute;n a nuestra actividad de este concepto de sostenibilidad implica el  cumplimiento de la legislaci&oacute;n y normas promulgadas al efecto, as&iacute; como la  fijaci&oacute;n de objetivos a corto y medio plazo de reducci&oacute;n y aprovechamiento de  residuos generados y de minimizaci&oacute;n de impactos ambientales durante la  prestaci&oacute;n del servicio. </p>
  <p>La  empresa adquiere el compromiso de utilizar t&eacute;cnicas de prevenci&oacute;n en todas las  actividades potencialmente contaminantes que aseguren la protecci&oacute;n del Medio  Ambiente y la conservaci&oacute;n de los recursos naturales minimizando la emisi&oacute;n de  residuos y cualquier otro impacto ambiental dentro del marco legal que es  aplicable en su actividad. </p>

</div>
<?php include("../00piecera.php");?>