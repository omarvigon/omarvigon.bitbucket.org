<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>TDF : Empresa : TDF Projectes Instal.lacions</h2>
  <p><img src="http://www.tdfobras.com/img/logo_tdf0.gif" alt="TDF" title="TDF" align="right" style="margin:16px 0 16px 16px;"></p>
  <p>TDF Proyectos Instalaciones es una divisi&oacute;n del grupo TDF, enmarcado en el sector servicios, dividiendose en tres &aacute;reas; Instalaciones, Mantenimiento y Proyectos, actuando en los sectores residencial, terciario e industrial.</p>
  <p>Es una organizacion polivalente, &aacute;gil e innovadora, tando en sus procedimientos como en su filosof&iacute;a, buscando la m&aacute;xima interacci&oacute;n con el cliente, adecuando el servicio a las necesidades concretas del momento.</p>
  <p>Integrado por un equipo profesional capaz de desarrollar internamente las tareas propias de nuestra actividad, tales como proyectos, montajes y mantenimientos.</p>
  <p><b>Direcci&oacute;n :</b><br>
    c/ Proven&ccedil;a 238 3&ordm;2&ordf; <br>
    08008 Barcelona<br>
    tel. 932.100.159<br>
    fax. 932 130 159<br>
    <a href="http://www.tdfprojectes.com" target="_blank">http://www.tdfprojectes.com</a><br>
    <a href="mailto:info@tdfprojectes.com">info@tdfprojectes.com</a></p>
  <p>&nbsp;</p>
</div>

<?php include("../00piecera.php");?>