<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>TDF : Empresa : Misi&oacute;n, visi&oacute;n y valores</h2>
  <p><b>&raquo; MISI&Oacute;N</b></p>
  <p>Desde los inicios, la premisa fundamental de TDF ha sido dar respuesta a las necesidades concretas de nuestros clientes, adecu&aacute;ndonos a la tipolog&iacute;a y caracter&iacute;sticas concretas de cada uno de ellos,  ayud&aacute;ndonos de la tecnolog&iacute;a m&aacute;s puntera del mercado y dotando al personal de la formaci&oacute;n necesaria para realizar el trabajo de la forma m&aacute;s eficiente posible.</p>
  <p>Nuestra trayectoria y el reconocimiento del mercado a nuestro trabajo, avala el camino escogido y nos da fuerzas para seguir trabajando en la misma direcci&oacute;n, con la &uacute;nica finalidad de proporcionar a nuestros clientes servicios  y productos de calidad, respetuosos con el medio ambiente, y dando  a nuestros empleados la posibilidad de crecimiento personal y profesional. </p>
  <p><b>&raquo; VISI&Oacute;N</b></p>
  <p>TDF&nbsp;&nbsp;quiere ser reconocida como una de las empresas importantes dentro del sector  de la  rehabilitaci&oacute;n y edificaci&oacute;n, especializada en servicios globales personalizados. Pretendemos hacerlo&nbsp;gracias a la&nbsp;actitud positiva, al car&aacute;cter&nbsp;emprendedor&nbsp;y&nbsp;al talento&nbsp; de&nbsp;nuestros profesionales, los cuales potencian y hacen diferenciales las capacidades de&nbsp;TDF en lo referente a  calidad, eficiencia, innovaci&oacute;n y capacidad de gesti&oacute;n.</p>
  <p>La fidelidad del cliente es nuestra garant&iacute;a de futuro y por eso debe ser nuestra mayor prioridad.</p>
  <p><b>&raquo; VALORES</b></p>
  <ul>
    <li>La Atenci&oacute;n al cliente personalizada</li>
    <li>El Comportamiento &eacute;tico de nuestro personal</li>
    <li>El Respeto por las personas</li>
    <li>El Trabajo en equipo </li>
    <li>Innovaci&oacute;n, Dinamismo y Capacidad de Adaptaci&oacute;n</li>
    <li>Compromiso, Transparencia y Honestidad</li>
    <li>Cercan&iacute;a y flexibilidad</li>
    <li>Autonom&iacute;a,	libertad y responsabilidad&nbsp;</li>
  </ul>
</div>

<?php include("../00piecera.php");?>