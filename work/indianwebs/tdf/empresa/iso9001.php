<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>TDF : Empresa : ISO 9001</h2>
  <p><b><img src="../img/logo_ISO9001medio.jpg" width="200" height="367" hspace="20" vspace="20" align="right">&raquo; ISO 9001</b></p>
  <p>Siguiendo nuestra l&iacute;nea de ofrecer un mejor servicio a nuestros clientes, TDFOBRAS, S.L. ha obtenido este a&ntilde;o 2008 la certificaci&oacute;n de calidad internacional, conforme a la norma ISO 9001:2000, otorgada por la empresa APPLUS Certification Technological Center, entidad de prestigio por su experiencia en la Uni&oacute;n Europea, Estados Unidos y Asia, acreditada por la entidad Nacional de Acreditaci&oacute;n ENAC, y en Reino Unido por United Kingdom Accreditation Services UKAS.</p>
  <p>Con esta acreditaci&oacute;n, esperamos as&iacute; dar un mejor servicio y responder a las expectativas de nuestros apreciados clientes.</p>
  <p>TDFOBRAS, S.L. ha centrado sus esfuerzos en optimizar su capacidad de respuesta a las demandas del cliente, conseguir la m&aacute;xima implicaci&oacute;n de cada una de las personas que componen el equipo de trabajo, establecer una toma de decisiones en tiempo real, basada en datos objetivos, y mantener su compromiso por una mejora continua.</p>
  <p>Sabemos que este logro es tambi&eacute;n de nuestros clientes, ya que su colaboraci&oacute;n nos ha hecho mejorar. Por eso, en nombre de todo nuestro equipo, les damos las gracias.</p>
  <p>&nbsp;</p>
</div>

<?php include("../00piecera.php");?>