<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>TDF : Empresa : Presentaci&oacute;n</h2>
  <p><b>PRESENTACI&Oacute;N: MODELO DE EMPRESA Y FILOSOF&Iacute;A</b></p>
  <p>TDF es una empresa especializada en gesti&oacute;n y ejecuci&oacute;n de obras, instalaciones y mantenimiento, abarcando todas las actividades propias del sector.</p>
<p>Nuestros 11 a&ntilde;os de experiencia y los grandes cambios acaecidos en el sector de la construcci&oacute;n y las instalaciones,
  tanto normativos (ley de subcontrataci&oacute;n, c&oacute;digo t&eacute;cnico, ley de prevenci&oacute;n de riesgos laborales, responsabilidades
  cruzadas, garant&iacute;as, etc), como t&eacute;cnicos y tecnol&oacute;gicos, nos han llevado a replantear nuestra estructura y modelo de
  empresa, con la finalidad de poder dar respuesta a los nuevos retos y escenarios que el sector y nuestros clientes
demandan..</p>
<p>Creemos firmemente que el modelo de empresa constructora tradicional ha quedado obsoleto, que las empresas
  de hoy en d&iacute;a hemos de reciclarnos, que hemos de ser, adem&aacute;s de ejecutoras de obras, buenas gestoras, para poder
  gestionar con eficacia todos los procesos intervinientes en las diferentes fases de la obra, fase de estudio, planificaci&oacute;n,
contrataci&oacute;n, ejecuci&oacute;n, control de calidad, postventa, etc...</p>
<p>Para este nuevo modelo de empresa, TDF apuesta por una plantilla fija reducida, con capacidades polivalentes,
  compuesta por un buen departamento de estudios, buenos jefes de obra y encargados. Con un soporte tecnol&oacute;gico
  con capacidad para desarrollar todas las facetas de un obra (Estudio, mediciones, compras, seguimiento, dibujo gr&aacute;fico,
  etc...). Un equipo de obra vers&aacute;til, &aacute;gil y con capacidad de adaptaci&oacute;n a las realidades puntuales que los momentos
y ciclos econ&oacute;micos nos marcan.</p>
<p>Para llevar a cabo nuestro modelo es imprescindible ser un gran conocedor del mercado, para ello disponemos de
  una amplia cartera de industriales y proveedores, minuciosamente seleccionada y a la que hemos aplicado un sistema
  de homologaci&oacute;n muy estricto en el que identificamos el tipo de trabajo que pueden realizar, la carga de trabajo
  que pueden absorber, la solvencia de la empresa, que cumpla con toda la normativa vigente en materia de seguridad
y salud, ley de subcontrataci&oacute;n, etc...</p>
<p>TDF no contrata al industrial m&aacute;s barato, si no al m&aacute;s id&oacute;neo seg&uacute;n el trabajo a desarrollar, asegurando la calidad,
garant&iacute;a y servicio postventa que TDF requiere para sus clientes.</p>
<p>Nuestra m&aacute;xima premisa es la calidad, la mejora continuada y el servicio a nuestros clientes. Para conseguirlo
  aplicamos unos c&aacute;nones de calidad estrictos y concretos, habiendo superado la auditoria de la norma 9001 en
Septiembre del 2008.</p>
<p>Posiblemente no somos la mejor empresa, pero podemos asegurar que cada d&iacute;a luchamos para serlo. Cometemos
  errores, por supuesto, pero aprendemos de ellos para no volver a repetirlos. Creamos procedimientos y mecanismos
para minimizar la repetici&oacute;n de dichos errores y todo ello con la finalidad de buscar la excelencia empresarial.</p>
<p>Hay que ser consciente de la dificultad que conlleva gestionar procesos muy complejos, en el que intervienen muchos
  agentes internos y externos, causas de fuerza mayor, condiciones atmosf&eacute;ricas, etc. Pero esto no es excusa, ni nos
har&aacute; decaer en nuestro firme prop&oacute;sito de buscar la excelencia y la calidad por encima de todo.</p>

  <p>&nbsp;</p>
</div>

<?php include("../00piecera.php");?>