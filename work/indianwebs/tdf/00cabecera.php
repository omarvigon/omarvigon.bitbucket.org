<?
function mostrar_ultimas($ruta)
{
	$directorio=opendir($ruta);  
	while ($file = readdir($directorio))
	{
       if ($file != "." && $file != "..")
	   {
       		$ficheros[$i]=$file;
       		$i++;
       }
    }
	rsort($ficheros);
	for($i = 0; $i<3; $i++)
	{
		$abierto=fopen($ruta.$ficheros[$i],"r");
		$lector=fread($abierto,filesize($ruta.$ficheros[$i]));
		$lector=strip_tags($lector);
		$lector=substr($lector,0,64);
		
		echo "<p><a href='http://www.tdfobras.com/tdf-noticias.php?seccion=".$ficheros[$i]."'>&raquo; ".$lector."... <span style='color:#476f66;'>[+info]</span></a></p>";  
		fclose($abierto);
	}
	closedir($directorio); 
}
function mostrar_todas()
{
	if($_SERVER['PHP_SELF']=="/index.php" || $_SERVER['PHP_SELF']=="/tdf-noticias.php" || $_SERVER['PHP_SELF']=="/tdf-enlaces.php" || $_SERVER['PHP_SELF']=="/tdf-obras.php"){
	$directorio=opendir("./noticias/files/"); 
	$directorioreal="./noticias/files/";
}else{
		$directorio=opendir("../noticias/files/");
			$directorioreal="../noticias/files/";} 
	while ($file = readdir($directorio))
	{
       if ($file != "." && $file != "..")
	   {
       		$ficheros[$i]=$file;
       		$i++;
       }
    }
	rsort($ficheros);
	for($i = 0; $i<count($ficheros); $i++)
	{
		$abierto=fopen($directorioreal.$ficheros[$i],"r");
		$lector=fread($abierto,filesize($directorioreal.$ficheros[$i]));
		$lector=strip_tags($lector);
		$lector=substr($lector,0,90);
		$fecha=substr($archivo,0,8);
		echo "<p>".$fecha." <a href='http://www.tdfobras.com/tdf-noticias.php?seccion=".$ficheros[$i]."'>&raquo; ".$lector."... <span style='color:#476f66;'>[+info]</span></a></p>";
		fclose($abierto);
	}
	closedir($directorio); 
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
	<link rel="shortcut icon" href="favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>TDF Obras y Servicios Barcelona Rehabilitaciones y Reformas</title>
	<meta name="description" content="TDF Obras y Servicios Barcelona. Rehabilitacion de edificios, Reforma de interiores y Construcciones nuevas." >
	<meta name="keywords" content="TDF Obras y Servicios Barcelona. Rehabilitacion de edificios, Reforma de interiores y Construcciones nuevas">
    <meta name="lang" content="es">
    <meta name="author" content="www.indianwebs.com" >
	<link rel="stylesheet" type="text/css" href="/estilos.css">
	<script language="javascript" type="text/javascript" src="http://www.tdfobras.com/funciones.js"></script>
	<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>

<body>
<script>var tipobj=document.getElementById("dhtmltooltip")</script>

<div id="fondo">&nbsp;</div>
<div id="central">
<div class="superior">
  		<a href="http://www.tdfobras.com/index.php" title="TDF Obras"><img src="http://www.tdfobras.com/img/logo_tdf2.gif" title="TDF Obras" style="margin:20px 0 0 0;border:0;"></a>
        <div><a href="http://www.tdfobras.com/index.php" title="TDF Obras">&raquo; inicio</a>
   		  <a href="mailto:info@tdfobras.com" title="Contacta con TDF">&raquo; contacta</a><cite>TDF OBRAS | c/ proven&ccedil;a 238 2�4� 08008 barcelona | tel. 93.210.06.69 | fax. 93.213.01.59</cite>    	</div>
</div>
<div class="botones">
  	  <span class="boton1"><a href="http://www.tdfobras.com/empresa/index.php" title="Empresa TDF"><img src="http://www.tdfobras.com/img/bt_empresa.gif" alt="Empresa TDF" title="Empresa TDF"></a>
	  <div id="capa1">
        <a href="http://www.tdfobras.com/empresa/empresa.php" title="Empresa de TDF">&raquo; Presentaci&oacute;n</a>
       	<a href="http://www.tdfobras.com/empresa/historia.php" title="Historia de TDF">&raquo; Historia</a>
 		<a href="http://www.tdfobras.com/empresa/organigrama.php" title="Organigrama de TDF">&raquo; Organigrama</a>
		<a href="http://www.tdfobras.com/empresa/grupo.php" title="Otras empresas de TDF">&raquo; Empresas participadas</a>
        <a href="http://www.tdfobras.com/empresa/calidad.php" title="Mision,vision y valores de TDF">&raquo; Misi&oacute;n,visi&oacute;n y valores</a>
        <a href="http://www.tdfobras.com/empresa/compromiso-social.php" title="Compromiso Social de TDF">&raquo; Compromiso Social</a> 
      </div>
	  </span>
      <span class="boton2"><a href="http://www.tdfobras.com/actividades/index.php" title="Actividades TDF"><img src="http://www.tdfobras.com/img/bt_actividades.gif" alt="Actividades TDF" title="Actividades TDF"></a>
      	<div id="capa2">
		<a href="http://www.tdfobras.com/actividades/rehabilitacion-interiores.php" title="Rehabilitacion integral de edificios TDF">&raquo; Rehabilitaci&oacute;n integral de edificios</a>
        <a href="http://www.tdfobras.com/actividades/rehabilitacion-exteriores.php" title="Rehabilitacion de exteriores TDF">&raquo; Rehabilitaci&oacute;n de exteriores</a>
        <a href="http://www.tdfobras.com/actividades/reforma-pisos-locales.php" title="Reforma y adecuacion de pisos y locales TDF">&raquo; Reforma y adecuaci&oacute;n</a>
       	<a href="http://www.tdfobras.com/actividades/edificacion.php" title="Edificacion de TDF">&raquo; Edificaci&oacute;n</a>
        <a href="http://www.tdfobras.com/actividades/construccion.php" title="Construccion de estructuras de TDF">&raquo; Construcci&oacute;n de estructuras</a>
        <a href="http://www.tdfobras.com/actividades/instalaciones.php" title="Instalaciones de TDF">&raquo; Instalaciones y mantenimiento</a>
      	</div>
      </span> 
      <span class="boton3"><a href="http://www.tdfobras.com/recursoshumanos/index.php" title="Recursos Humanos TDF"><img src="http://www.tdfobras.com/img/bt_recursos2.gif" alt="Recursos humanos TDF" title="Recursos humanos TDF"></a>
      	<div id="capa3">
   	     <a href="http://www.tdfobras.com/recursoshumanos/empleo.php" title="Ofertas de empleo en TDF">&raquo; Ofertas de empleo</a>
         <a href="http://www.tdfobras.com/recursoshumanos/curriculum.php" title="Envia tu curriculum TDF">&raquo; Envia tu curriculum</a>
         </div>
      </span>
      <span><a href="http://www.tdfobras.com/tdf-enlaces.php" title="Enlaces de TDF"><img src="http://www.tdfobras.com/img/bt_enlaces.gif" alt="Enlaces TDF" title="Enlaces TDF"></a></span>

      <span id="boton4"><a href="http://www.tdfobras.com/tdf-noticias.php" title="Noticias de TDF"><img src="http://www.tdfobras.com/img/bt_noticias1.gif" alt="Noticias TDF" title="Noticias TDF"></a></span>
</div>
        
	

    <div id="cabecera">
    	<script type="text/javascript">
		new fadeshow(fadeimages, 758, 207, 0, 2000, 1)
		</script>
        
      <!--
      <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="758" height="190">
        <param name="movie" value="inicio_banner.swf">
        <param name="quality" value="high">
        <param name="wmode" value="opaque">
        <param name="swfversion" value="6.0.65.0">
        <param name="expressinstall" value="Scripts/expressInstall.swf">


        <object type="application/x-shockwave-flash" data="inicio_banner.swf" width="758" height="190">
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <param name="expressinstall" value="Scripts/expressInstall.swf">
          <div>
            <h4>El contenido de esta p&aacute;gina requiere una versi&oacute;n m&aacute;s reciente de Adobe Flash Player.</h4>
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
          </div>
        </object>
      </object>-->
</div>

	<div class="noticias">
		<p style="border-bottom:1px solid #000000;font-weight:bold;">NOTICIAS</p>
		<?php mostrar_todas($_SERVER['DOCUMENT_ROOT'].'/noticias/files/');?>
	</div>

<script type="text/javascript">
//swfobject.registerObject("FlashID");
</script>
