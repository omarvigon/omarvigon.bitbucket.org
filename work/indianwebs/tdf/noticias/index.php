<?php include("funciones.php"); ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)" />
    <meta name="revisit" content="7 days" />
    <meta name="robots" content="index,follow,all" />
	<meta name="description" content="TDF Obras y Servicios Barcelona. Rehabilitacion de edificios, Reforma de interiores y Construcciones nuevas." >
	<meta name="keywords" content="" />
    <meta name="lang" content="es,sp,spanish,espa&ntilde;ol,espanol,castellano" >
    <meta name="author" content="www.indianwebs.com" >
	<title>TDF Obras y Servicios Barcelona Rehabilitaciones y Reformas</title>
    <link rel="stylesheet" type="text/css" href="estilos.css">
	<script language="javascript" type="text/javascript" src="../../noticias/funciones.js"></script>
</head>
<body>

<div id="central">
    <div class="botones">	
    	<?php comprobacion(); ?>
    </div>
    <div class="centro">
    	<h1>TDF OBRAS</h1>
        <?php if($_SESSION["autentico"])
		{
			if($_POST["modo"])	call_user_func($_POST["modo"]);
			else				noticias_ver();	
		} ?>
    </div>
</div>

</body>
</html>