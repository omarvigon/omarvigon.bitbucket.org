<?php include("../00cabecera.php");?>

<div class="centro">
	<br />
  <h2>TDF : Recursos Humanos : Ofertas de empleo</h2>
  <p><b>TECNICO DE ESTUDIOS Y PROYECTOS</b></p>
  <p><b>Departamento:</b> Estudios y licitaciones<br>
    <b>Funciones a desempe&ntilde;ar</b>: Estudio de proyectos, realizaci&oacute;n de ofertas,  solicitud y comparativo de precios industriales y proveedores. <br>
    <b>Titulaci&oacute;n</b>: Arquitectura T&eacute;cnica/ FP2  edificaci&oacute;n, delineaci&oacute;n, etc., o experiencia contrastada.<br>
    <b>Experiencia</b>:  2 a&ntilde;os como m&iacute;nimo.<br>
    <b>Programas</b>: Dominio de Presto m&oacute;dulos,&nbsp;  Presupuesto, mediciones y compras, conocimiento de Autocad y Project.</p>
  <p><b>JEFE DE OBRA</b></p>
  <p><b>Departamento:</b> Producci&oacute;n<br>
    <b>Funciones a desempe&ntilde;ar</b>: Planificaci&oacute;n, coordinaci&oacute;n, control de costes,  contrataci&oacute;n industrial, seguridad y salud, control t&eacute;cnico. <br>
    <b>Titulaci&oacute;n:</b> Arquitectura T&eacute;cnica, carnet de  conducir<br>
    <b>Experiencia</b>:  3 a&ntilde;os como m&iacute;nimo.<br>
    <b>Programas</b>: Dominio de Presto m&oacute;dulos,&nbsp;  presupuesto, mediciones, compras y control de costes, conocimiento de  Autocad y Project.</p>
  <p><b>TECNICO COMERCIAL </b></p>
  <p><b>Departamento:</b> Estudios y licitaciones<br>
    <b>Funciones a desempe&ntilde;ar</b>: Captaci&oacute;n de obra<br>
    <b>Requisitos</b>: Estudios medios, carnet de conducir <br>
    <b>Experiencia</b>:  3 a&ntilde;os como m&iacute;nimo.<br>
    <b>Programas</b>: Ofim&aacute;tica. Se valorar&aacute; conocimiento de Presto</p>
  <p></p>

</div>

<?php include("../00piecera.php");?>