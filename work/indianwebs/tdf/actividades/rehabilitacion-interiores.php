<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>TDF : Actividades : Rehabilitaci&oacute;n integral de edificios</h2>
  <p>Nuestro amplio conocimiento del mercado y sus necesidades nos permite aportar  soluciones personalizadas a los problemas concretos de cada proyecto,  en vanguardia con las t&eacute;cnicas de trabajo m&aacute;s avanzadas.</p>
  <p>Nuestra trayectoria ascendente se basa en una organizaci&oacute;n eficiente y una  gesti&oacute;n din&aacute;mica y emprendedora, implantada a trav&eacute;s de la experiencia  adquirida con el trabajo diario. TDF ofrece siempre a sus clientes la  eficacia como norma de actuaci&oacute;n.</p>

  <div id="dhtmltooltip"></div>
  <script>var tipobj=document.getElementById("dhtmltooltip")</script>
  <p> 
<img src="../img/galeria_rehabilitaciones13.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones14.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones1.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones2.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones6.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones7.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones8.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones9.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones10.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones11.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones12.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones3.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones4.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> 
<img src="../img/galeria_rehabilitaciones5.jpg" alt="Rehabilitacion integral de edificios" title="Rehabilitacion integral de edificios" class="minis" onMouseover="ontip(this.src)"; onMouseout="hidetip()"> </p>

  <p>&nbsp;</p>
</div>

<?php include("../00piecera.php");?>