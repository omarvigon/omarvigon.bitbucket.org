<?php include("../00cabecera.php");?>

<div class="centro">
   <br>
  <h2>Actividades</h2>
  <img src="../img/actividades.jpg" alt="Actividades TDF" title="Actividades TDF" align="left" class="bordeverde">
  <p><a href="http://www.tdfobras.com/actividades/rehabilitacion-interiores.php" title="Rehabilitacion integral de edificios TDF">&raquo; Rehabilitaci&oacute;n integral de edificios</a></p>
  <p><a href="http://www.tdfobras.com/actividades/rehabilitacion-exteriores.php" title="Rehabilitacion de exteriores TDF">&raquo; Rehabilitaci&oacute;n de exteriores</a></p>
  <p><a href="http://www.tdfobras.com/actividades/reforma-pisos-locales.php" title="Reforma de pisos y locales TDF">&raquo; Reforma y adecuaci&oacute;n</a></p>
  <p><a href="http://www.tdfobras.com/actividades/edificacion.php" title="Edificacion de TDF">&raquo; Edificaci&oacute;n</a></p>
  <p><a href="http://www.tdfobras.com/actividades/construccion.php" title="Construccion de estructuras de TDF">&raquo; Construcci&oacute;n de estructuras</a></p>
  <p><a href="http://www.tdfobras.com/actividades/instalaciones.php" title="Instalaciones de TDF">&raquo; Instalaciones y mantenimiento</a></p>
  <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
</div>

<?php include("../00piecera.php");?>