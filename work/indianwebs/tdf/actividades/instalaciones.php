<?php include("../00cabecera.php");?>

<div class="centro">
  <br>
  <h2>Actividades : Instalaciones y mantenimiento</h2>
  <p>TDF realiza trabajos de instalaciones integrales, el&eacute;ctricas, mec&aacute;nicas, climatizaci&oacute;n, telecomunicaciones, alumbrado, protecci&oacute;n contra incendios, dom&oacute;tica y captaci&oacute;n solar.</p>
  <p>TDF ofrece sus servicios multidisciplinarios de gesti&oacute;n y mantenimiento integral de acuerdo con las necesidades del cliente y las singularidades de cada una de sus instalaciones. Lo que convierte a TDF en una empresa con vocaci&oacute;n de servicio, con especializaci&oacute;n  en los trabajos propios de conservaci&oacute;n, actuaciones de car&aacute;cter preventivo, predictivo, correctivo, normativo, reformas y obras relacionadas con las instalaciones que mantenemos.</p>
  <p>TDF gestiona proyectos adecuados a las necesidades de cada cliente, desarrollados en colaboraci&oacute;n con Arquitectos, Ingenieros y Fabricantes, para ofrecer una instalaci&oacute;n de m&aacute;xima calidad, a su medida.</p>
  <p>&nbsp;&nbsp;&nbsp; <img title="Instalaciones" src="../img/galeria_instalaciones1.jpg" alt="TDF Instalaciones" onMouseover="ontip(this.src)"; onMouseout="hidetip()" class="minis"> <img title="Instalaciones" src="../img/galeria_instalaciones2.jpg" alt="TDF Instalaciones" onMouseover="ontip(this.src)"; onMouseout="hidetip()" class="minis"> <img title="Instalaciones" src="../img/galeria_instalaciones3.jpg" alt="TDF Instalaciones" onMouseover="ontip(this.src)"; onMouseout="hidetip()" class="minis"> <img title="Instalaciones" src="../img/galeria_instalaciones4.jpg" alt="TDF Instalaciones" onMouseover="ontip(this.src)"; onMouseout="hidetip()" class="minis"> <img title="Instalaciones" src="../img/galeria_instalaciones5.jpg" alt="TDF Instalaciones" onMouseover="ontip(this.src)"; onMouseout="hidetip()" class="minis"> </p>
  <p><img src="../img/instalaciones.jpg" alt="Instalaciones" title="Instalaciones"></p>
  <p><img src="../img/mantenimiento.jpg" alt="Mantenimiento" title="Mantenimiento"></p>
  <p>&nbsp;</p>
</div>

<?php include("../00piecera.php");?>