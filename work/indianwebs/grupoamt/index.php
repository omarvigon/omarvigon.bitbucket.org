<?php include("funciones.php"); ?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<META NAME="DESCRIPTION" CONTENT="Restaurantes Barcelona Grupo AMT">
	<meta name="keywords" content="grupo amt, restaurantes barcelona">
	<title>Grupo AMT</title>
	<link rel="stylesheet" type="text/css" href="estilos.css">
    <script language="javascript" type="text/javascript">var xfecha1;</script>
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>
<body onload="iniciar()">

<table cellpadding="0" cellspacing="0" align="center" class="tabla1">
<tr>
	<td class="boton0">
    	<img src="img/logo.jpg" alt="Grupo AMT" title="Grupo AMT"></td>
	<td class="botonera">
    <?php if($_SESSION["autentico"]) 
	{ 
		if($_SESSION["idagencia"]==2)
		{
			echo '<table cellpadding="0" cellspacing="2">
			<tr>
			<td><a class="boton1" href="index.php?modo=agencias">ADMINISTRAR AGENCIAS</a></td>
			<td><a class="boton1" href="index.php?modo=menus">ADMINISTRAR<br>MENUS</a></td>
			<td><a class="boton1" href="index.php?modo=restaurantes">ADMINISTRAR RESTAURANTES</a></td>
			<td><a class="boton1" href="index.php?modo=listar_noticias">ADMINISTRAR NOTICIAS</a></td>
			</tr>
			<tr>
			<td><a class="boton2" href="index.php?modo=reservas_listar">ADMINISTRAR RESERVAS</a></td>
			<td><a class="boton2" href="index.php?modo=reservas_listar_anulada">RESERVAS<br>ANULADAS</a></td>
			<td><a class="boton2" href="index.php?modo=reservas_listar_finalizada">RESERVAS<br>FINALIZADAS</a></td>
			<td><a class="boton2" href="index.php?modo=informes">INFORMES</a></td>
			</tr>
			</table>';
		}
		else
		{
			echo '<table cellpadding="0" cellspacing="2">
			<tr>
			<td><a class="boton1" href="index.php?modo=reservas_nueva&aux=1">NUEVA RESERVA</a></td>
			<td><a class="boton1" href="index.php?modo=reservas_listar">VER RESERVAS</a></td>
			<td><a class="boton1" href="index.php?modo=reservas_listar_anulada">RESERVAS<br>ANULADAS</a></td>
			<td><a class="boton1" href="index.php?modo=reservas_listar_finalizada">RESERVAS<br>FINALIZADAS</a></td>
			</tr>
			<tr>
			<td><a class="boton2" href="index.php?modo=mod_agencia">MODIFICAR DATOS</a></td>
			<td><a class="boton2" href="index.php?modo=productos">VER PRODUCTOS ESPECIALES</a></td>
			<td><a class="boton2" href="http://www.grupoamt.es/menus.htm" target="_blank">VER MENUS</a></td>
			<td><a class="boton2" href="index.php?modo=noticias">VER NOTICIAS</a></td>
			</tr>
			</table>';
		}
		
	 } else { ?>
		<form name="form_acceso" method="post" action="index.php">
        <table cellpadding="0" cellspacing="2" style="margin:40px 0 0 40px;">
        <tr>
        	<td>Login :</td>
            <td><input size="24" maxlength="24" name="nombre" type="text"></td>
        </tr>
        <tr>
        	<td>Password :</td>
            <td><input size="24" maxlength="24" name="password" type="password"></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td align="right"><input type="submit" value="  ENTRAR  "></td>
        </tr>
        </table>
        </form>
	<? } ?>	
	</td>
</tr>
<tr><td colspan="2" class="filagris">&nbsp;</td></tr>

<? if($_SESSION["autentico"])
{
	if($_SESSION["idagencia"]==2)
		echo "<tr><td colspan='2' class='filaboto'><span style='padding:0 390px 0 0'><b>PANEL DE ADMINISTRACI&Oacute;N </b></span><a href='http://www.grupoamt.es'>Volver a GRUPOAMT.ES - </a><a href='index.php?modo=salir'>Salir</a></td></tr><tr>";
	else
		echo "<tr><td colspan='2' class='filaboto'><span style='padding:0 280px 0 0'><b>PANEL DE AGENCIAS :</b> Bienvenido </span><a href='http://www.grupoamt.es'>Volver a GRUPOAMT.ES - </a><a href='index.php?modo=salir'>Salir</a></td></tr><tr>";
	if($_GET["modo"])	
		call_user_func($_GET["modo"]);
	else
		noticias();
	echo "</tr>";
}
else
{
	if($_POST["nombre"] && $_POST["password"])	
		comprobacion();
}
?>

<tr><td colspan="2">&nbsp;</td></tr>
</table>
</body>
</html>