<html>
<head>
<title>Panel de Control :: Grupo AMT</title>
<style type="text/css">
	body		{font:10px Verdana, Arial, Helvetica, sans-serif;color:#C51931;margin-left:0;}
	td			{font:10px Verdana, Arial, Helvetica, sans-serif;color:#C51931;vertical-align:top;}
	a			{text-decoration:none;color:#990033;}
	input,select{font:10px Verdana, Arial, Helvetica, sans-serif;}
	
	h3			{background:#000000;padding:5px;}
	h3 a 		{padding-left:120px;}
	h3 a:hover	{color:#CCCCCC;}
	h4			{font:12px Verdana, Arial, Helvetica, sans-serif;color:#000000;font-weight:bold;margin-bottom:5px;}
	
	#im1		{filter:alpha(opacity=20);opacity:.20;}	
	#im2		{filter:alpha(opacity=20);opacity:.20;}	
	#im3		{filter:alpha(opacity=20);opacity:.20;}	
	#im4		{filter:alpha(opacity=20);opacity:.20;}	
	#im5		{filter:alpha(opacity=20);opacity:.20;}	
	#im6		{filter:alpha(opacity=20);opacity:.20;}	
	#im7		{filter:alpha(opacity=20);opacity:.20;}	
	#im8		{filter:alpha(opacity=20);opacity:.20;}	
	#im9		{filter:alpha(opacity=20);opacity:.20;}	
	#im10		{filter:alpha(opacity=20);opacity:.20;}	
	
	.borde		{border:1px solid #666666;width:100px;}
	
	#f1			{position:absolute;visibility:hidden;top:190;left:342;z-index:1;}
	#f2			{position:absolute;visibility:hidden;top:170;left:467;z-index:1;}
	#f3			{position:absolute;visibility:hidden;top:454;left:262;z-index:1;}
	#f4			{position:absolute;visibility:hidden;top:453;left:322;z-index:1;}
	#f5			{position:absolute;visibility:hidden;top:580;left:235;z-index:1;}
	#f6			{position:absolute;visibility:hidden;top:490;left:322;z-index:1;}
	#f7			{position:absolute;visibility:hidden;top:670;left:217;z-index:1;}
	#f8			{position:absolute;visibility:hidden;top:545;left:340;z-index:1;}
	#f9			{position:absolute;visibility:hidden;top:745;left:237;z-index:1;}
	#f10		{position:absolute;visibility:hidden;top:615;left:297;z-index:1;}
	
	#ventana1	{position:absolute;margin-left:225px;height:280px;
				 background:#CCCCCC;border:2px solid #000000;z-index:2;visibility:hidden;
				 filter:alpha(opacity=90);opacity:.90;}
	#ventana1 form{padding:0px 20px 10px 10px;}
</style>
<script>
function flotante()
{
	function instancia()
	{
		var lacapa=document.getElementById("ventana1");
		lacapa.coordenadas=function(y)
		{	
			if(y>198)
				this.style.top=y;
		};
		lacapa.x=3;
		lacapa.y=document.body.scrollTop + document.body.clientHeight-200;
		return lacapa;
	}
	window.colocar=function()
	{
		objeto.y=objeto.y+((document.body.scrollTop+document.body.clientHeight)-400-objeto.y)/8;
		objeto.coordenadas(objeto.y);
		setTimeout("colocar()",10);
	}
	objeto = instancia();
	colocar();
	
	for(var i=10;i<=100;i++)
	{
		document.form1.pax.options[i-10]=new Option(i,i);
	}
}
var preciototal=0;
var texto1,elegida;
var precios=new Array();
var hora=new Array();
var opciones=new Array();
var numtemp;
var cajaruta="";
function restaurante(num)
{
	if(document.getElementById("ventana1").style.visibility!="visible")
	{
		if(num==9) //si es el santamonica
		{
			if(document.form1.ano.value=="2007")
			{
				precios[0]=4.00;
				precios[1]=3.00;
			}
			else if(document.form1.ano.value=="2008")
			{
				precios[0]=4.50;
				precios[1]=3.50;
			}
		}	
		else	//si es el resto
		{
			if(document.form1.ano.value=="2007")
			{
				precios[0]=5.00;
				precios[1]=6.50;
			}
			else if(document.form1.ano.value=="2008")
			{
				precios[0]=5.55;
				precios[1]=7.20;
			}
		}
	
		switch(num)
		{
		case 1:
		texto1="Cafe Catalunya";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>1 Montadito de tomate con mozarella</li><li>1 Montadito de cangrejo</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>1 cazuelita de Pulpitos salteados</li></ul>";
		break;
		
		case 2:
		texto1="Navarra";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>1 montadito de lomo con pimiento guernica</li><li>1 montadito de esqueixada de bacalao</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Almejas a la marinera</li></ul>";
		break;
		
		case 3:
		texto1="Sukaldari";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>Montadito de col lombarda</li><li>Montadito de croqueta</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Alb&oacute;ndigas con salsa</li></ul>";
		
		break;
		
		case 4:
		texto1="El Choquito";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>1 Montadito de jam&oacute;n</li><li>1 Montadito de s&eacute;pia</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Choricitos a la sidra</li></ul>";
		
		break;
		
		case 5:
		texto1="Mikel Etxea";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>Montadito de queso feta</li><li>Montadito de pimiento del piquillo con at&uacute;n</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Caracoles</li></ul>";
		break;
		
		case 6:
		texto1="Xerinola";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>1 montadito de Soufl&eacute; de pera</li><li>1 montadito de salm&oacute;n con queso fresco</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Cazuelita de morcilla encebollada</li></ul>";
		break;
		
		case 7:
		texto1="Micky's";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>Montadito de queso</li><li>Montadito pincho tortilla Patata</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Champi&ntilde;ones con ajo y perejil</li></ul>";
		break;
		
		case 8:
		texto1="Mikel Etxea";
		opciones[0]="2 montaditos + 1 bebida "+precios[0]+" &euro; m&aacute;s IVA<ul><li>1 Montadito de Matrimonio de anchoas y boquerones</li><li>1 Montadito de Txistorra</li></ul>";
		opciones[1]="1 tapa + bebida "+precios[1]+" &euro; m&aacute;s IVA<ul><li>Mejillones a la marinera</li></ul>";
		break;
		
		case 9:
		texto1="Santa Monica";
		opciones[0]="Tarta + caf&eacute; "+precios[0]+" &euro; m&aacute;s IVA <ul><li>Tarta de la casa</li></ul>";
		opciones[1]="Copa de cava "+precios[1]+" &euro; m&aacute;s IVA <ul><li>Copa de cava Marqu&eacute;s de Monistrol</li></ul>";
		break;
		
		case 10:
		texto1="Restaurante Brasil";
		opciones[0]="<ul><li></li><li></li></ul>";
		opciones[1]="<ul><li></li><li></li></ul>";
		break;
		}
		document.getElementById("opcion0").innerHTML=opciones[0];
		document.getElementById("opcion1").innerHTML=opciones[1];
		document.getElementById("titulo").innerHTML=texto1+"<a href='javascript:cerrar()' title='Cerrar ventana'>[X]</a>";
		document.getElementById("ventana1").style.visibility="visible";
		numtemp=num;
	}
}
function validar()
{
	//pone el texto de la hora marcada en el restaurante
	hora[0]=document.form2.hora1.value;
	hora[1]=document.form2.hora2.value;

	if(document.form2.eleccion[0].checked)
	{
		//si escoge la primera
		elegida=opciones[0];
		preciototal=preciototal+precios[0];
	}
	else
	{
		//si escoge la segunda
		elegida=opciones[1];
		preciototal=preciototal+precios[1];
	}

	//muestra la flecha
	document.getElementById("f"+numtemp).style.visibility="visible";
	document.getElementById("im"+numtemp).style.filter="alpha(opacity=90)";
	document.getElementById("im"+numtemp).style.opacity=".9";
	
	if(numtemp!=10)	
		numtemp="0"+numtemp;	
	
	cajaruta=cajaruta+"<b>Hora :</b> "+hora[0]+":"+hora[1]+" <b>Restaurante : </b>"+texto1+"<br><b>Opci&oacute;n :</b> "+elegida;
	cerrar();

}
function borrar()
{
	if(window.confirm("�Esta seguro de eliminar toda la informacion de la ruta?"))
	{
		window.open("rutas1.php?agencia=<?php echo $_SESSION["idagencia"]; ?>","_self");
	}
}
function cambioano()
{
	for(var i=1;i<=10;i++)
	{
		document.getElementById("f"+i).style.visibility="hidden";
		document.getElementById("im"+i).style.filter="alpha(opacity=20)";
		document.getElementById("im"+i).style.opacity=".2";
	}	
	cajaruta="";
	preciototal=0;
}
function verpresupuesto()
{	
	document.form1.oculto_precio.value=preciototal;
	document.form1.oculto_textos.value=cajaruta;
	document.form1.submit();
}	
function cerrar()
{
	document.getElementById("ventana1").style.visibility="hidden";
}
function imprimir()
{
	window.print();
}
</script>
</head>
<body onLoad="flotante();">

<div id="ventana1">
	<h3 id="titulo">&nbsp;</h3>
	<form name="form2">
	<h4>HORA:</h4>
	<select name="hora1"><option value="10">10</option><option value="11">11</option><option value="12" selected="selected">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="00">00</option></select><b>:</b>
	<select name="hora2"><option value="00" selected="selected">00</option><option value="15">15</option><option value="30">30</option><option value="45">45</option></select>
	<h4>OPCIONES:</h4>
	<input type="radio" name="eleccion" value="1" checked="checked"><span id="opcion0">&nbsp;</span>
	<input type="radio" name="eleccion" value="2"><span id="opcion1">&nbsp;</span>
	<input type="button" value="ACEPTAR" onClick="validar()">
	<input type="button" value="CANCELAR" onClick="cerrar()"> 
	</form>
</div>

<table cellpadding="0" cellspacing="0">
<tr>
	<td colspan="3">
		<table cellspacing="0" cellpadding="2" align="center">
		<tr>
			<td>Fecha de la ruta</td>
			<td>
            <form name="form1" method="post" action="rutas2.php" target="_blank">
			<select name="dia"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
			<select name="mes"><option vaue="Enero">Enero</option><option vaue="Febrero">Febrero</option><option vaue="Marzo">Marzo</option><option vaue="Abril">Abril</option><option vaue="Mayo">Mayo</option><option vaue="Junio">Junio</option><option vaue="Julio">Julio</option><option vaue="Agosto">Agosto</option><option vaue="Septiembre">Septiembre</option><option vaue="Octubre">Octubre</option><option vaue="Noviembre">Noviembre</option><option vaue="Diciembre">Diciembre</option></select>
			<select name="ano" onChange="cambioano()"><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option></select>
			</td>
		</tr>
		<tr>
			<td>Numero de personas</td>
			<td><select name="pax"></select>Referencia <input type="text" name="referencia" size="35"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="hidden" name="oculto_precio">
				<input type="hidden" name="oculto_textos">
				<input type="hidden" name="oculto_nombre" value="<?php echo $_GET["id"]?>">
				<input type="button" value="imprimir ruta" class="borde" onClick="imprimir()">
				<input type="button" value="ver presupuesto" class="borde" onClick="verpresupuesto()">
				<input type="button" value="borrar todo" class="borde" onClick="borrar()">
				</form>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="logos" align="right">
		<a href="javascript:restaurante(1)"><img src="img/rutas_1.jpg" id="im1" border="0"/></a><br />
		<a href="javascript:restaurante(3)"><img src="img/rutas_2.jpg" id="im3" border="0"/></a><br />
		<a href="javascript:restaurante(5)"><img src="img/rutas_3.jpg" id="im5" border="0"/></a><br />
		<a href="javascript:restaurante(7)"><img src="img/rutas_4.jpg" id="im7" border="0"/></a><br />
		<a href="javascript:restaurante(9)"><img src="img/rutas_5.jpg" id="im9" border="0" vspace="4"></a><br />
	</td>
	<td>
		<img src="img/rutas_fondo.jpg">
		<div id="f1"><img src="img/flecha_1.gif"></div>
		<div id="f2"><img src="img/flecha_2.gif"></div>
		<div id="f3"><img src="img/flecha_3.gif"></div>
		<div id="f4"><img src="img/flecha_4.gif"></div>
		<div id="f5"><img src="img/flecha_5.gif"></div>
		<div id="f6"><img src="img/flecha_6.gif"></div>
		<div id="f7"><img src="img/flecha_7.gif"></div>
		<div id="f8"><img src="img/flecha_8.gif"></div>
		<div id="f9"><img src="img/flecha_9.gif"></div>
		<div id="f10"><img src="img/flecha_10.gif"></div> 
	</td>
	<td class="logos">
		<a href="javascript:restaurante(2)"><img src="img/rutas_6.jpg" id="im2" border="0"/></a><br />
		<a href="javascript:restaurante(4)"><img src="img/rutas_7.jpg" id="im4" border="0"/></a><br />
		<a href="javascript:restaurante(6)"><img src="img/rutas_8.jpg" id="im6" border="0"/></a><br />
		<a href="javascript:restaurante(8)"><img src="img/rutas_9.jpg" id="im8" border="0" vspace="4"></a><br />
		<a href="javascript:restaurante(10)"><img src="img/rutas_10.jpg" id="im10" border="0"/></a><br />
	</td>
</tr>
</table>
</body>
</html>