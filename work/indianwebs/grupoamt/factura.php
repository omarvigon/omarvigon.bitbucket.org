<?
include("funciones.php");
$registro=factura();
$fecha=explode("-",$registro["fecha"]);
$preciogrupo=$registro["ref_menuprecio"] * (($registro["comensales"])-$registro["gratuidades"]);
$preciototal=$preciogrupo+($preciogrupo*0.07);
$preciototal=round($preciototal,2);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Factura</title>
	<style type="text/css">
	body 			{margin:25px 0 10px 0;}
	td 				{font:12px Verdana, Arial, Helvetica, sans-serif;vertical-align:top;}
	p				{font:12px Verdana, Arial, Helvetica, sans-serif;}
	.tablacentral 	{border:1px solid #000000;}
	.raya			{margin:12px;border-bottom:1px solid #000000;}
	</style>
</head>

<body>
<table class="tablacentral" align="center" cellpadding="3" cellspacing="3">
<tr><td colspan="4">&nbsp;</td></tr>
<tr>
	<td width="340" colspan="2">
    	<?php 
		if($_GET["nologo"])		echo '&nbsp;';
		else					echo '<img src="img/logofactura.jpg" hspace="8"/>';
		?>
    </td>
    <td width="340" colspan="2">
    	<p style="border:1px solid #000000;padding:6px;margin:0 12px 6px 0;">
        <b><? echo $registro["ref_sociedad"];?>
        <br><? echo $registro["ref_sociedad_nif"];?><br></b>
  	    <? echo $registro["ref_sociedad_direccion"];?>
        <br> www.grupoamt.es / grupoamt@grupoamt.es
        </p>
    	<p style="border:1px solid #000000;padding:6px;margin:30px 12px 6px 0;">
        Datos del cliente:<br>
        <b><? echo $registro["ref_agencia"];?></b><br>
        <? echo $registro["ref_agencia_direccion"];?><br>
        <? echo $registro["ref_agencia_nif"];?>
        </p>
    </td>
</tr>
<tr><td colspan="4"><p class="raya">&nbsp;</p></td></tr>
<tr>
	<td align="right"><br><b>FECHA : </b></td>
    <td colspan="3"><br><? echo $fecha[2]."-".$fecha[1]."-".$fecha[0] ;?></td>
</tr>
<tr>
    <td align="right"><b>NUMERO DE FACTURA : </b></td>
    <td colspan="3"><? echo $registro["num_factura"];?></td>
</tr>
<tr>
	<td align="right"><b>NUM.FAC.PROFORMA : </b></td>
    <td colspan="3"><? echo $registro["num_proforma"];?></td>
</tr>
<tr><td colspan="4"><p class="raya">&nbsp;</p></td></tr>
<tr><td colspan="4"><p>&nbsp;</p></td></tr>
<tr>
	<td align="right"><b>CONCEPTO : </b></td>
    <td>Reserva</td>
    <td align="right"><b>FECHA RESERVA : </b></td>
    <td><? echo $fecha[2]."-".$fecha[1]."-".$fecha[0] ;?></td>
</tr>
<tr>
	<td align="right"><b>N&Uacute;MERO RESERVA : </b></td>
    <td><? echo $registro["id"];?></td>
    <td align="right"><b>HORA RESERVA: </b></td>
    <td><? echo $registro["hora"];?></td>
</tr>
<tr>
	<td align="right"><b>REFERENCIA GRUPO : </b></td>
    <td colspan="3"><? echo $registro["referencia"];?></td>
</tr>
<tr><td colspan="4">&nbsp;</td></tr>
<tr>
	<td align="right"><b>RESTAURANTE : </b></td>
    <td colspan="3"><? echo $registro["ref_restaurante"];?></td>
</tr>
<tr>
	<td align="right"><b>DIRECCI&Oacute;N : </b></td>
    <td><? echo $registro["ref_restaurante_direccion"];?></td>
    <td align="right"><b>TEL&Eacute;FONO : </b></td>
    <td><? echo $registro["ref_restaurante_tel"];?></td>
</tr>
<tr><td colspan="4">&nbsp;</td></tr>
<tr>
	<td align="right"><b>MEN&Uacute; : </b></td>
    <td colspan="3"><? echo $registro["ref_menu"];?></td>
</tr>
<tr>
	<td align="right"><b>TOTAL PAX : </b></td>
    <td><? echo $registro["comensales"];?></td>
    <td align="right"><b>GRATUIDADES : </b></td>
    <td><? echo $registro["gratuidades"];?></td>
</tr>
<tr>
	<td align="right"><b>PV.PAX : </b></td>
    <td><? echo number_format($registro["ref_menuprecio "],2,',','.'); ?> &euro;</td>
    <td align="right"><b>PAX :</b></td>
    <td><? echo $registro["comensales"]-$registro["gratuidades"];?></td>
</tr>
<tr><td colspan="4"><p class="raya">&nbsp;</p></td></tr>
<tr><td colspan="4"><p>&nbsp;</p></td></tr>
<tr>
	<td align="right"><b>FORMA DE PAGO : </b></td>
    <td colspan="3"><? echo $registro["formapago"];?></td>
</tr>
<tr>
	<td align="right"><b>NUM.CUENTA BANCARIA : </b></td>
    <td colspan="3"><? echo $registro["numcuenta"];?></td>
</tr>
<tr>
	<td align="right"><b>BASE IMPONIBLE : </b></td>
    <td><? echo number_format($preciogrupo,2,',','.');?>&euro;</td>
    <td align="right"><b>IVA :</b></td>
    <td><? echo number_format(($preciogrupo*0.07),2,',','.') ;?> &euro;</td>
</tr>
<tr><td colspan="4"><p>&nbsp;</p></td></tr>
<tr><td colspan="4"><p>&nbsp;</p></td></tr>
<tr>
	<td colspan="2">&nbsp;</td>
    <td colspan="2" style="border:1px solid #000000;background:#cccccc;" align="right">
    <b>TOTAL : </b><? echo number_format($preciototal, 2, ',', '.');?>&euro;</td>
</tr>
<tr><td colspan="4"><p>&nbsp;</p></td></tr>
<tr><td colspan="4"><p>&nbsp;</p></td></tr>
</table>
<p align="center"><? echo $registro["rm"];?></p>
</body>
</html>