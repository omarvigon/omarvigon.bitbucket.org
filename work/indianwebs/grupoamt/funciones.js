function iniciar()
{
	var ruta=window.location.href.substr(0,61);
	if(ruta=="http://www.grupoamt.es/reservas/index.php?modo=reservas_nueva")
	{
		if(xfecha1)
		{
			document.form_nueva_reserva.fecha_dia.value=xfecha1;
			document.form_nueva_reserva.fecha_mes.value=xfecha2;
			document.form_nueva_reserva.fecha_ano.value=xfecha3;
			document.form_nueva_reserva.hora_hora.value=xhora1;
			document.form_nueva_reserva.hora_minuto.value=xhora2;
		}
		else
		{
			var fecha=new Date();
			document.form_nueva_reserva.fecha_dia.value=fecha.getDate();
			document.form_nueva_reserva.fecha_mes.value=fecha.getMonth()+1;
			//document.form_nueva_reserva.fecha_mes.selectedIndex= va de 0 a N;
		}
		if(document.form_nueva_reserva.comensales.value)
			calcular_gratuidades();
	}	
}
function calcular_gratuidades()
{
	var comensales=parseInt(document.form_nueva_reserva.comensales.value);
	
	var gratuidades=parseInt(comensales/21);
	document.form_nueva_reserva.gratuidades.value=gratuidades;
	document.form_nueva_reserva.pax.value=comensales-gratuidades;
	
	if(document.form_nueva_reserva.menu.value>0)
		calcular_precio();
}
function calcular_precio()
{
	//document.getElementById("xpecial").style.display="block";
	
	document.form_nueva_reserva.picapica.value=xpicapica[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.primero.value=xprimero[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.segundo.value=xsegundo[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.postre.value=xpostre[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.bebidas.value=xbebidas[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.bodega.value=xbodega[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.otros.value=xotros[document.form_nueva_reserva.menu.value];
	document.form_nueva_reserva.precio.value=xprecio[document.form_nueva_reserva.menu.value];
	
	var preciogrupo=(document.form_nueva_reserva.pax.value)*(document.form_nueva_reserva.precio.value);
	preciogrupo=preciogrupo.toFixed(2);
	document.form_nueva_reserva.bi.value=preciogrupo;
	
	var precioiva=(preciogrupo*7)/100;
	precioiva=precioiva.toFixed(2);
	document.form_nueva_reserva.iva.value=precioiva;
	
	var preciototal=parseFloat(preciogrupo)+parseFloat(precioiva);
	preciototal=preciototal.toFixed(2);
	document.form_nueva_reserva.total.value=preciototal;
	
}
function cambio_restaurante()
{
	document.form_nueva_reserva.submit();
	//window.location.href="http://www.grupoamt.es/reservas/";
	//window.refresh();
}
function filtrar_reserva()
{
	var errores=0;
	if(isNaN(document.form_nueva_reserva.comensales.value))
	{
		alert("El dato 'Comensales' debe ser un numero");
		errores++;
	}
	if(document.form_nueva_reserva.responsable_email.value.search (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig) )
	{
		alert("Email no valido, sin forma: nombre@servidor.dominio");
		errores++;
	}
	
	if(errores==0)	return true;
	else			return false
}
function insertar_reserva()
{
	if(filtrar_reserva())
	{
		document.form_nueva_reserva.action="index.php?modo=insert_reserva";
		document.form_nueva_reserva.submit();
	}
}
function modif_reserva(cual)
{
	if(filtrar_reserva())
	{
		document.form_nueva_reserva.action="index.php?modo=reservas_mod&id="+cual;
		document.form_nueva_reserva.submit();
	}
}
function filtra_agencia()
{
	var errores=0;
	if(document.formedit_agencia.nombre.value=="")
	{
		alert("El dato 'NOMBRE' no es valido");
		errores++;
	}
	if(document.formedit_agencia.nif.value=="" || document.formedit_agencia.nif.value.length<9)
	{
		alert("El dato 'NIF' no es valido. Debe tener al menos 8 digitos");
		errores++;
	}
	if(document.formedit_agencia.cp.value=="" || isNaN(document.formedit_agencia.cp.value) || document.formedit_agencia.cp.value.length<5)
	{
		alert("El dato 'Codigo Postal' debe ser un numero de 5 caracteres");
		errores++;
	}
	if(isNaN(document.formedit_agencia.telefono.value) || document.formedit_agencia.telefono.value.length<9 )
	{
		alert("El dato 'Telefono' debe ser un numero de, al menos, 9 caracteres");
		errores++;
	}
	if(document.formedit_agencia.fax.value=="" || isNaN(document.formedit_agencia.fax.value)|| document.formedit_agencia.fax.value.length<9)
	{
		alert("El dato 'Fax' debe ser un numero de, al menos, 9 caracteres");
		errores++;
	}
	if(document.formedit_agencia.movil.value=="" || isNaN(document.formedit_agencia.movil.value) || document.formedit_agencia.movil.value.length<9)
	{
		alert("El dato 'Movil' debe ser un numero de, al menos, 9 caracteres");
		errores++;
	}
	if(document.formedit_agencia.email.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
	{
		alert("Email no valido, sin forma: nombre@servidor.dominio");
		errores++;
	}
	if(errores==0)
	{
		document.formedit_agencia.submit();
	}
}
function marcartodos()
{
	var valor;
	if(document.form_menu_edit.especial.checked)
		valor=true;
	else
		valor=false;
	
	for(var i=10;i<=document.form_menu_edit.elements.length-1;i++)
	{
		document.form_menu_edit.elements[i].checked=valor;
	}
}
function generar()
{
	document.formedit_agencia.contrasena.value=Math.round(Math.random()*100000);
}
function confirmarfin(id)
{
	if(confirm("Esta seguro de FINALIZAR DEFINITIVAMENTE esta reserva?"))
		window.open("index.php?modo=reservas_finalizar&id="+id,"_self");
}
function comprobarprecio()
{
	if(isNaN(document.form_menu_edit.precio.value))
		alert("El dato PRECIO debe ser numerico");
	else	
		document.form_menu_edit.submit();
}