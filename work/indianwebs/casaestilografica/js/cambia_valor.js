function incrementar(campo, maximo) {

	i = parseInt(campo.value) + 1;
	if(i > maximo)
		i = maximo;

	if(isNaN(i)) {
		campo.value = 1;
	} else {
		campo.value = i;
	}

}
function reducir(campo) {

	i = parseInt(campo.value) - 1;

	if(i < 1)
		i = 1;

	if(isNaN(i)) {
		campo.value = 1;
	} else {
		campo.value = i;
	}

}