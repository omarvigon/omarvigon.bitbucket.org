<?php include ("cabecera.php"); ?>

	<td class="cuerpo">
    <p>La Asociaci�n Profesional de LETRADOS del INEM  nace ante la necesidad de poder encauzar las inquietudes profesionales y  personales de los funcionarios que ocupan el puesto de trabajo de LETRADO del  INEM-Servicio P�blico de Empleo Estatal, organismo aut�nomo dependiente del  Ministerio de Trabajo y Asuntos Sociales.</p>
    <p>La creaci�n de esta p�gina y a trav�s de un  apartado restringido solo para los asociados tiene como finalidad la  divulgaci�n inmediata de toda la informaci�n que puede afectar a nuestros  intereses, as� como la comunicaci�n personal y directa entre los asociados  mediante el uso de un foro de conversaci�n en el que libremente se expresan las  ideas y se realizan las aportaciones, siempre necesarias para la mejora de  nuestras reivindicaciones.</p>
    <p>Igualmente, la creaci�n de esta p�gina viene a  cubrir la falta de comunicaci�n y de conocimiento existente entre los distintos  compa�eros ubicados en todas y cada una de las provincias del Estado, de modo  que se satisface una expectativa que la Asociaci�n estaba obligada a  implementar dentro de sus finalidades sindicales y de acuerdo con lo aprobado  en la �ltima Junta Ordinaria.</p>
    <p>Por �ltimo, y para aquellos que todav�a no son  parte de la Asociaci�n, hay que tener presente que la uni�n de todas las  fuerzas e ideas que entre todos pongamos en com�n tiene como objetivo la mejora  de nuestras condiciones laborales, que van a beneficiar a todos, por lo que la  aportaci�n, por m�nima que sea, es bienvenida.</p>
    </td>
            
<?php include ("piecera.php"); ?>