<?php include ("cabecera.php"); ?>

    <td class="cuerpo">
	<p>La Asociaci�n se reune mediante juntas  ordinarias cada dos a�os o extraordinarias cuando las circunstancias obliguen a  ello. </p>
	<p>Se elige, de entre los <b>Letrados del INEM</b>,� un presidente, un vicepresidente, cuatro  vocales, una tesorera y una secretaria que conforman una junta directiva.</p>
	<p>La toma de decisiones se realiza a trav�s de  las Juntas Generales debidamente convocadas sin perjuicio del desarrollo y de  la oportunidad en su desarrollo por parte de la Junta Directiva.</p>
	<p>Siendo el colectivo de <b>Letrados del INEM</b> muy  reducido, y en consecuencia el de miembros de la Asociaci�n, se obtiene la  ventaja de que la participaci�n es muy directa, y sobre todo desde la creaci�n  de esta p�gina, que es uno de sus objetivos, a parte de dar a conocer las  noticias que vayan sucediendo.</p>
    </td>
            
<?php include ("piecera.php"); ?>