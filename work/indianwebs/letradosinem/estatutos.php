<?php include ("cabecera.php"); ?>

        <td class="cuerpo2">
        <div id="barra">	
        <b>ESTATUTOS DE LA  ASOCIACI&Oacute;N PROFESIONAL DE LOS LETRADOS   DEL SERVICIO P&Uacute;BLICO DE EMPLEO ESTATAL- INEM</b>
        <p><b>T&Iacute;TULO PRIMERO</b><br />
            <b>DENOMINACI&Oacute;N, &Aacute;MBITO  TERRITORIAL Y PROFESIONAL. DURACI&Oacute;N, FINES Y DOMICILIO.</b></p>
        <p><b>Art&iacute;culo 1&ordm;. </b><br />
          Con el nombre de  ASOCIACI&Oacute;N PROFESIONAL DE LETRADOS DEL SERVICIO P&Uacute;BLICO DE EMPLEO ESTATAL- INEM, se  constituye una entidad profesional que se regir&aacute; por los presentes estatutos,  quedando sometida a la legislaci&oacute;n vigente, espec&iacute;ficamente a las  prescripciones recogidas en la ley org&aacute;nica 11/85 de 2 de agosto de libertad  sindical.</p>
        <p><b>Art&iacute;culo 2&ordm;.<br />
        </b>El &aacute;mbito de actuaci&oacute;n de la Asociaci&oacute;n se  extender&aacute; a todo el territorio nacional.</p>
        <p><b>Art&iacute;culo 3&ordm;.<br />
        </b>La Asociaci&oacute;n se constituye por tiempo  indefinido y tendr&aacute; plena capacidad jur&iacute;dica y de obrar, siendo su personalidad  jur&iacute;dica propia e independiente de la de sus asociados, pudiendo, en  consecuencia realizar y celebrar toda clase de actos y contratos.</p>
        <p><b>Art&iacute;culo 4&ordm;</b>.<br />
        La Asociaci&oacute;n tendr&aacute; su domicilio en la  direcci&oacute;n provincial del INEM de Barcelona en el Parc de l'estaci&oacute; del Nord  s/n. Dicho domicilio ir&aacute; variando y ajust&aacute;ndose al destino que tenga el  funcionario que vaya ocupando el cargo de presidente, tanto en las direcciones  provinciales como en los servicios centrales.</p>
        <p><b>Art&iacute;culo 5&ordm;.<br />
        </b>Son fines de la Asociaci&oacute;n los siguientes:</p>
        <p> a)  Representaci&oacute;n, gesti&oacute;n, defensa y fomento de los intereses econ&oacute;micos-sociales  y profesionales de sus miembros.</p>
        <p> b) El  establecimiento de servicios propios, de inter&eacute;s com&uacute;n para sus miembros, y en  especial la asesor&iacute;a t&eacute;cnica, jur&iacute;dica, econ&oacute;mica o fiscal que se estimen  adecuados para la mejor defensa de los intereses socio-.profesionales de sus  afiliados.<br /><br />
        c) Promover la  mejora de las condiciones generales y particulares para el desarrollo de la  actividad profesional de los asociados, as&iacute; como la unificaci&oacute;n de criterios y  sistemas.</p>
        <p> d) Cumplir y  hacer cumplir las normas y disposiciones legales dictadas por los &oacute;rganos  competentes a fin de subordinar en todo momento los intereses privados a los  generales.</p>
        <p>e) Representar a  los asociados en relaci&oacute;n con los fines de la asociaci&oacute;n defendiendo los  intereses de los miembros ante cualquier organismo, jurisdicci&oacute;n o instancia.</p>
        <p> f) Todas cuantas  funciones de an&aacute;loga naturaleza se consideren necesarias o conveniente para el  cumplimiento de sus fines y para la defensa de los leg&iacute;timos intereses de sus  miembros.</p>
        <p>g) La  administraci&oacute;n y disposici&oacute;n de los propios recursos presupuestarios o patrimoniales  y su aplicaci&oacute;n a los fines y actividades propios de la asociaci&oacute;n.<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
        h) La  colaboraci&oacute;n con la Direcci&oacute;n General del INEM en la consecuci&oacute;n de todos  aquellos fines que son propios de la Entidad Gestora.</p>
        <p><b>T&Iacute;TULO SEGUNDO </b><br />
            <b>DE LOS MIEMBROS DE LA  ASOCIACI&Oacute;N.</b></p>
        <p><b>Art&iacute;culo 6&ordm;.<br />
        </b>Podr&aacute;n ser miembros de la Asociaci&oacute;n todos los  funcionarios de carrera que ocupen un destino en la relaci&oacute;n de puestos de  trabajo del INEM catalogado como letrado.</p>
        <p><b>Art&iacute;culo 7&ordm;.<br />
        </b>El ingreso en la  Asociaci&oacute;n es absolutamente voluntario, debi&eacute;ndo solicitarse por escrito  dirigido a la Junta Directiva, &oacute;rgano permanente de gobierno, gesti&oacute;n,  administraci&oacute;n y direcci&oacute;n de la Asociaci&oacute;n.</p>
        <p>Presentada la  solicitud de ingreso en la asociaci&oacute;n ante un &oacute;rgano inferior y &eacute;sta fuera rechazada, podr&aacute; interponerse recurso ante la Asamblea General en  el plazo de 15 d&iacute;as desde la notificaci&oacute;n de la denegaci&oacute;n del ingreso.</p>
        <p>La condici&oacute;n de  miembro se pierde:<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
        a) Por baja  voluntaria.</p>
        <p> b) Por sanci&oacute;n  acordada por la Junta Directiva y ratificada por la Asamblea General por alguna  de las causas siguientes:</p>
        <p> 1.-  Incumplimiento de las obligaciones establecidas en los siguientes estatutos.<br /> 
          2.-  Incumplimiento de los acuerdos adoptados por las Asamblea General o por &nbsp;la Junta Directiva en la esfera de sus  respectivas competencias.<br />
        3.-  Incumplimiento de las obligaciones econ&oacute;micas que para el sostenimiento de la asociaci&oacute;n se hayan establecido  estatutariamente.</p>
        <p> c) Muerte,  incapacidad o jubilaci&oacute;n del asociado, as&iacute; como no ostentar o perder por  cualquier circunstancia el destino catalogado en la relaci&oacute;n de puestos de  trabajo del INEM de letrado.</p>
        <p> Los acuerdos de  la Junta Directiva decidiendo sobre la baja o expulsi&oacute;n de cualquier asociado  podr&aacute;n ser recurridos ante la primera Asamblea General que se celebre, despu&eacute;s  de notificado al interesado tal acuerdo, resolvi&eacute;ndose por acuerdo favorable de  las dos terceras partes de los socios presentes y representados, sin perjuicio  de las acciones legales que pudieran corresponder al asociado.</p>
        <p> Ning&uacute;n asociado  con la expulsi&oacute;n, podr&aacute; pedir el reingreso a la asociaci&oacute;n hasta transcurrir  cinco a&ntilde;os a contar desde la fecha de la expulsi&oacute;n.</p>
        <p><b>Art&iacute;culo 8&ordm;.<br />
        </b> Son derechos de  los miembros de la Asociaci&oacute;n los siguientes:</p>
        <p> 1.- Elegir y ser  elegido para puestos de representaci&oacute;n y ostentar cargos directivos. Para que  los miembros de la Asociaci&oacute;n puedan&nbsp;  usar este derecho, ser&aacute; preciso que est&eacute;n al d&iacute;a en el pago de sus  respectivas cuotas.</p>
        <p>2.- Ejercer la  representaci&oacute;n que en cada caso se les confiera.</p>
        <p> 3.- Proponer  candidatos en las elecciones de miembros de los &oacute;rganos de gobierno.</p>
        <p>4.- Informar y  ser informado oportunamente de las actuaciones y vida de la Asociaci&oacute;n y de las  cuestiones que les afecten.<br />
          &nbsp;&nbsp;<br />
          5.- Expresar  libremente por escrito o de palabra cualquier opini&oacute;n o punto de vista relacionado  con los asuntos profesionales que directamente les afecten o se discutan seg&uacute;n  el orden del d&iacute;a de la reuni&oacute;n, siempre que no vayan en contra de los  principios establecidos en los presentes estatutos o en las normas jur&iacute;dicas de  general observancia.</p>
        <p> 6.- Utilizar los  servicios t&eacute;cnicos de protecci&oacute;n y de asesoramiento de car&aacute;cter profesional, econ&oacute;micos y social de que disponga la Asociaci&oacute;n.</p>
        <p> 7.- Ejercitar las  acciones y recursos a que haya lugar en defensa de sus derechos asociativos o  instar a la Asociaci&oacute;n a que interponga las acciones y recursos oportunos para  la defensa de intereses profesionales comunes.</p>
        <p> 8.- Recurrir en  escrito razonado ante el presidente&nbsp; y la  junta directiva de la Asociaci&oacute;n cuando se considere perjudicado en sus  derechos de asociado, debiendo ser resuelto dicho recurso en el t&eacute;rmino de dos  meses.</p>
        <p> 9.- Examinar los  libros de contabilidad y actas, as&iacute; como censurar, mediante la oportuna moci&oacute;n  presentada a la asamblea general, la labor de esta o de cualquier miembro u  &oacute;rgano colegiado que act&uacute;e en nombre de la Asociaci&oacute;n.</p>
        <p> 10.- Asistir a  las reuniones de la asamblea general, y ejercer el derecho de voto directamente  o deleg&aacute;ndolo por escrito en otro asociado.</p>
        <p><b>Art&iacute;culo 9&ordm;.<br />
        </b>Son deberes de  los asociados, los siguientes:</p>
        <p> 1.- Ajustar su  actuaci&oacute;n a las leyes y a las normas de estos estatutos.<br /> 
        2.- Desempe&ntilde;ar  los cargos y comisiones que le confiera la asamblea general o la junta  directiva.<br />
        3.- Cumplir los  acuerdos v&aacute;lidamente adoptados legal y estatutariamente.<br /> 
        4.- Respetar la  libre manifestaci&oacute;n de pareceres y no entorpecer directa o indirectamente las  actividades de la Asociaci&oacute;n. 5.- Facilitar  informaci&oacute;n solvente y responsable sobre las cuestiones que no tengan  naturaleza reservada cuando le sea requerida por los &oacute;rganos de gobierno de la  Asociaci&oacute;n en cumplimiento de las normas estatutarias y reglamentarias.<br /> 
        6.- Satisfacer  puntualmente las cuotas, tanto ordinarias como las extraordinarias que puedan  establecerse.</p>
        <p><b>T&Iacute;TULO TERCERO</b><br />
            <b>DE LOS &Oacute;RGANOS DE  GOBIERNO</b></p>
        <p><b>Art&iacute;culo 10&ordm;.<br />
        </b>La Asamblea  General es el &oacute;rgano soberano de la Asociaci&oacute;n y estar&aacute; constituida por la totalidad de los miembros de la misma.</p>
        <p> La Asamblea  General se reunir&aacute; en sesi&oacute;n ordinaria una vez al a&ntilde;o, en el transcurso del  primer trimestre, para examinar y aprobar, en su caso, la memoria anual de la  junta directiva, las cuentas del a&ntilde;o anterior, y el presupuesto de ingresos y gastos del ejercicio corriente.</p>
        <p>Con car&aacute;cter  extraordinario podr&aacute; reunirse el veinte por ciento de los asociados, por  decisi&oacute;n del presidente, o a petici&oacute;n de la junta directiva.</p>
        <p><b>Art&iacute;culo 11&ordm;.<br />
        </b>Las convocatorias  de las sesiones ordinarias se har&aacute;n por el presidente al menos con tres d&iacute;as  h&aacute;biles de anticipaci&oacute;n, por circular a domicilio en que se consignar&aacute;n los  asuntos a tratar.</p>
        <p> La convocatoria  de las sesiones extraordinarias se har&aacute; procurando, si el asunto que las motiva  lo permite, guardar la misma forma y anticipaci&oacute;n.</p>
        <p><b>Art&iacute;culo 12&ordm;.<br />
        </b> Para poder  celebrar sesi&oacute;n en primera convocatoria, se requiere la presencia de asociados  que representen la mitad m&aacute;s uno del total de votos.</p>
        <p>En segunda  convocatoria, que se celebrar&aacute; media hora despu&eacute;s, con el n&uacute;mero de los que  asistan.</p>
        <p><b>Art&iacute;culo 13&ordm;.<br />
        </b> Los asociados  concurrentes a las asambleas generales deber&aacute;n, a medida que vayan entrando,  firmar en el libro de presentes. La retirada de alguno o algunos de ellos  despu&eacute;s de abierta la sesi&oacute;n, no rompe el qu&oacute;rum ni inhabilita a la asamblea  para seguir deliberando o tomar resoluciones.</p>
        <p><b>Art&iacute;culo 14&ordm;.<br />
        </b> Los acuerdos se  tomar&aacute;n por mayor&iacute;a de votos. En caso de empate se proceder&aacute; a una<br />
        nueva votaci&oacute;n y en caso de persistir el empate se har&aacute; una  tercera y &uacute;ltima votaci&oacute;n que de no solucionar el problema, decidir&aacute; el  presidente.</p>
        <p><b>Art&iacute;culo 15&ordm;.<br />
        </b> La Asamblea  General tendr&aacute;, entre otras, las siguientes funciones:</p>
        <p>1.- Aprobar y  reformar los presentes estatutos.</p>
        <p> 2.- Adoptar  acuerdos en relaci&oacute;n con la representaci&oacute;n, gesti&oacute;n y defensa&nbsp; de los intereses profesionales de sus miembros, sin perjuicio de la facultad de delegar en la junta directiva la realizaci&oacute;n de  aqu&eacute;llos que se encuentren en el marco de su competencia.</p>
        <p> 3.- Adoptar  acuerdos relativos a la comparecencia ante los organismos p&uacute;blicos y para la  interposici&oacute;n de toda clase de recursos, a fin de defender y fomentar en forma  adecuada y eficaz los intereses profesionales a su cargo.</p>
        <p> 4.- Aprobar los  programas y planes de actuaci&oacute;n.</p>
        <p>5.- Elegir los  componentes de la Junta Directiva.</p>
        <p>6.- Conocer la  gesti&oacute;n de la Junta Directiva.</p>
        <p>7.- Fijar, cuando  proceda, las cuotas que hayan de satisfacer los miembros de acuerdo con las  normas de estos estatutos y, en su caso, del reglamento de r&eacute;gimen interior.</p>
        <p> 8.- Aprobar los  presupuestos y liquidaciones de cuentas.</p>
        <p>9.- Aprobar la  memoria anual de actividades.</p>
        <p> 10.- Acordar o  desestimar las bajas de miembros de la Asociaci&oacute;n, propuestas como sanci&oacute;n por  la Junta Directiva.</p>
        <p>11.- Aprobar, en  su caso, un reglamento de r&eacute;gimen interior de la Asociaci&oacute;n que desarrollo los  presentes estatutos.</p>
        <p>12.- Aqu&eacute;llos  otros asuntos que, por su importancia, someta a su consideraci&oacute;n la Junta  Directiva.</p>
        <p>13.- Acordar, por  la mayor&iacute;a que se determina, la disoluci&oacute;n de la Asociaci&oacute;n.</p>
        <p><b>Art&iacute;culo 16&ordm;.<br />
        </b> La&nbsp; Junta&nbsp;  Directiva&nbsp; es&nbsp; el &oacute;rgano&nbsp;  permanente&nbsp; de gobierno,&nbsp; gesti&oacute;n, administraci&oacute;n y &nbsp;direcci&oacute;n de la Asociaci&oacute;n y estar&aacute; compuesta  por presidente, vicepresidente, tesorero, secretario y seis vocales como m&aacute;ximo  elegidos por la Asamblea General.</p>
        <p><b>Art&iacute;culo 17&ordm;.<br />
        </b> La Junta  Directiva, adem&aacute;s de las atribuciones que le delegue expresamente la Asamblea  General dentro de los l&iacute;mites autorizados por las disposiciones legales  vigentes, tendr&aacute;, entre otras, las siguientes funciones:</p>
        <p> 1.- Planificar y  dirigir las actividades de la Asociaci&oacute;n para el ejercicio y desarrollo de las  funciones que le son propias.</p>
        <p>2.- Proponer a la  Asamblea General la defensa, en forma adecuada y eficaz, de los intereses  profesionales a su cargo.</p>
        <p> 3.- Proponer a la  Asamblea General los programas de actuaci&oacute;n y realizar y dirigir los ya  aprobados, dando cuenta a aqu&eacute;lla de su cumplimiento.</p>
        <p> 4.- Velar por el  cumplimiento de los acuerdos de la Asamblea General y de la propia Junta  Directiva.</p>
        <p>5.- Decidir la  celebraci&oacute;n de reuniones extraordinarias de la Asamblea General y fijar el  orden del d&iacute;a de &eacute;stas en las ordinarias.</p>
        <p> 6.- Proponer a la  Asamblea General las cuotas que hayan de satisfacer los miembros.</p>
        <p> 7.- Presentar los  presupuestos, balances y liquidaciones de cuentas para su aprobaci&oacute;n a la  Asamblea General.</p>
        <p> 8.- Decidir en  materia de cobros y ordenaci&oacute;n de pagos, y expedici&oacute;n de libramientos.</p>
        <p>9.- Inspeccionar  la contabilidad, as&iacute; como la mec&aacute;nica de cobros y pagos.</p>
        <p>10.- Elaborar la  memoria anual de actividades, someti&eacute;ndola a la aprobaci&oacute;n de la Asamblea  General.</p>
        <p>11.- Inspeccionar  y velar por el normal funcionamiento de los servicios de la Asociaci&oacute;n.</p>
        <p>12.- Adoptar  acuerdos relacionados con la interposici&oacute;n de toda clase de acciones y recursos  ante cualquier Organismo o Jurisdicci&oacute;n, en ejecuci&oacute;n de lo dispuesto en el  punto segundo de este art&iacute;culo.</p>
        <p>13.- Ejercer la  potestad disciplinaria, conforme a lo establecido en estos estatutos.</p>
        <p>14.- Adoptar  acuerdos referentes a la adquisici&oacute;n y disposici&oacute;n de bienes.</p>
        <p>15.- Las que  puedan ser delegadas por la Asamblea General.</p>
        <p>16.- Cuantas  atribuciones no est&eacute;n expresamente encomendadas a la Asamblea General.<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
        <b>Art&iacute;culo 18</b>&ordm;<b>.<br />
        </b>El presidente,  vicepresidente, y tesorero de la Asociaci&oacute;n&nbsp;  lo ser&aacute;n a su vez de la Asamblea General de entre sus miembros.</p>
        <p>El presidente  tendr&aacute; las siguientes atribuciones:</p>
        <p>a) Presidir la Asamblea  General y la Junta Directiva.</p>
        <p> b) Dirigir los  debates y el orden de las reuniones y velar por la ejecuci&oacute;n de los acuerdos.</p>
        <p> c) Representar  legalmente a la Asociaci&oacute;n&nbsp; en cuantos  actos, contratos, personamientos y relaciones de todo orden y jurisdicci&oacute;n deba  intervenir la misma, ante los juzgados, tribunales y organismos de la  Administraci&oacute;n, de cualquier clase que fueren, pudiendo otorgar los poderes  necesarios a procuradores y abogados que se encarguen de instar, mantener y desistir  en las oportunas acciones o recursos que procedan en defensa de los intereses  comunes asociativos, profesionales o econ&oacute;micos de la actividad de la  Asociaci&oacute;n.</p>
        <p> d) Usar de la  firma en los t&eacute;rminos previstos en los presentes estatutos.</p>
        <p>e) Ordenar los  gastos, autorizar los pagos y justificantes de ingresos conjuntamente con el  vicepresidente, tesorero o secretario.</p>
        <p> f) Convocar las  reuniones de la Asamblea General y de la Junta Directiva.</p>
        <p>g) Cumplir y  hacer cumplir las normas estatutarias.<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
          h) Ejercer las  funciones espec&iacute;ficas que le atribuyan las normas estatutarias.<br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
          El presidente  rendir&aacute; anualmente informe de su actuaci&oacute;n y de la Junta Directiva, ante la  Asamblea General.</p>
        <p><b>Art&iacute;culo 19&ordm;.<br />
        </b>El vicepresidente  sustituir&aacute;, en caso de ausencia o enfermedad, al presidente, hall&aacute;ndose  investido en esta funci&oacute;n de la totalidad de atribuciones que aqu&eacute;l tiene  conferidas. Las mismas normas ser&aacute;n de aplicaci&oacute;n en caso de baja definitiva  del presidente y hasta tanto se verifique la elecci&oacute;n.</p>
        <p>El tesorero  cuidar&aacute; de la conservaci&oacute;n de los fondos en la forma que se disponga y firmar&aacute;  conjuntamente con el presidente, vicepresidente o secretario todos los  documentos de cobros y pagos.</p>
        <p> En las cuentas  bancarias de la Asociaci&oacute;n tendr&aacute;n reconocida su firma el presidente,  vicepresidente, el tesorero y el secretario, de forma que las operaciones  puedan realizarse alternativamente con dos de dichas firmas, cualesquiera que  ellas fueran.</p>
        <p><b>Art&iacute;culo 20&ordm;.</b><br />
         Corresponde al  secretario:</p>
        <p>a) Llevar y  custodiar los libros registros de los asociados, as&iacute; como las actas de la  Asamblea General y de la Junta Directiva.</p>
        <p> b) Redactar de  forma circunstanciada el acta de las sesiones de la Asamblea General y de la  Junta Directiva en las que act&uacute;e.</p>
        <p>c) Librar  certificaciones autorizadas con la firma del presidente con referencia a los  libros y documentos sociales.</p>
        <p> d) Efectuar las  notificaciones que procedan de los acuerdos adoptados por la Asamblea General  y/o por la Junta Directiva.</p>
        <p><b>Art&iacute;culo 21&ordm;.<br />
        </b> La Asociaci&oacute;n se  regir&aacute;, en todos en sus grados, por representantes libremente elegidos mediante  sufragio libre y secreto cada cuatro a&ntilde;os, renovables por mitad cada dos. La  primera renovaci&oacute;n ser&aacute; la del vicepresidente y tesorero m&aacute;s la mitad de los  vocales determinados por sorteo.</p>
        <p> Para elegir y ser  elegidos en puestos de representaci&oacute;n, ser&aacute; preciso gozar de la plenitud de  derechos y obligaciones de car&aacute;cter asociativo.</p>
        <p> Todos los cargos  podr&aacute;n ser reelegidos.</p>
        <p><b>T&Iacute;TULO CUARTO</b><br />
            <b>R&Eacute;GIMEN DISCIPLINARIO</b></p>
        <p><b>Art&iacute;culo 22&ordm;.  Instrucci&oacute;n de expedientes</b><u>.<br />
        </u> No se&nbsp; podr&aacute;&nbsp;  imponer&nbsp; sanci&oacute;n&nbsp; alguna&nbsp;  a&nbsp; ning&uacute;n &nbsp;afiliado&nbsp;  sin&nbsp; previa&nbsp; instrucci&oacute;n&nbsp;  de<br />
        expediente, que deber&aacute; acordarse por la Junta Directiva,  especificando los motivos de su decisi&oacute;n.</p>
        <p>Ser&aacute; instructor  de cada expediente el vicepresidente de la Asociaci&oacute;n o el miembro de la Junta  Directiva que se designe por este &oacute;rgano.</p>
        <p> El expediente se  iniciar&aacute; con el oportuno escrito de cargos, del que se dar&aacute; traslado al  expedientado, invit&aacute;ndole para que formule los pliegos de descargos,  alegaciones y proposici&oacute;n de pruebas, pudiendo el instructor acordar todo lo  concerniente a su admisi&oacute;n y pr&aacute;ctica, as&iacute; como la verificaci&oacute;n de cualesquiera  diligencias probatorias que estime adecuadas para mejor proveer en cualquier  momento del procedimiento, incluso con suspensi&oacute;n del tr&aacute;mite. A la vista de  todo lo actuado el instructor formular&aacute; propuesta de resoluci&oacute;n al pleno de la  Junta Directiva, que resolver&aacute; en definitiva.</p>
        <p>Tanto la  propuesta del instructor como la decisi&oacute;n final de la Junta Directiva se notificar&aacute;n  al expedientado, que podr&aacute; recurrir en alzada ante la Asamblea General, en el  t&eacute;rmino de quince d&iacute;as, contados desde la fecha de la notificaci&oacute;n.</p>
        <p>Formalizado el recurso quedar&aacute; en suspenso el  acuerdo, que no ser&aacute; ejecutivo hasta que se pronuncie sobre el mismo la  Asamblea General, continuando mientras tanto el expedientado en el pleno uso de  los derechos que como afiliado le corresponden.</p>
        <p><b>Art&iacute;culo 23&ordm;. Sanciones.<br />
        </b> Las sanciones que  podr&aacute;n imponerse a los miembros de la Asociaci&oacute;n, ser&aacute;n las siguientes:</p>
        <p> a) Amonestaci&oacute;n  privada o p&uacute;blica.</p>
        <p>b) Inhabilitaci&oacute;n  para el ejercicio de cargos directivos por un periodo no superior a dos a&ntilde;os.</p>
        <p>c) Multa hasta un  l&iacute;mite m&aacute;ximo de 25.000 ptas.</p>
        <p>d) Baja por  expulsi&oacute;n.</p>
        <p>De las sanciones  que se impongan, cuando adquieran firmeza, se dejar&aacute; constancia en el  expediente personal de cada afiliado. La Junta Directiva apreciar&aacute; con libertad  de criterio el grado de gravedad de los hechos objeto de expediente.</p>
        <p>En todo caso se reservan a la Asociaci&oacute;n los  derechos que le correspondan para el ejercicio de acciones en reclamaci&oacute;n de  da&ntilde;os y perjuicios contra cualesquiera personas, afiliadas o no, y entidades  p&uacute;blicas y privadas.</p>
        <p><b>T&Iacute;TULO QUINTO</b><br />
            <b>R&Eacute;GIMEN ECON&Oacute;MICO</b></p>
        <p><b>Art&iacute;culo 24&ordm;.<br />
        </b> La Asociaci&oacute;n  tendr&aacute; plena autonom&iacute;a para la administraci&oacute;n de sus propios recursos.</p>
        <p><b>Art&iacute;culo 25&ordm;.<br />
        </b> El funcionamiento  econ&oacute;mico de la Asociaci&oacute;n se regular&aacute; en r&eacute;gimen de presupuesto. El  Presupuesto ordinario ser&aacute; la expresi&oacute;n cifrada de las obligaciones contraidas  durante un a&ntilde;o en relaci&oacute;n con el c&aacute;lculo de los recursos y medios de que  disponga para cubrir aqu&eacute;llas atenciones. La Asamblea General aprobar&aacute; el  presupuesto ordinario para el a&ntilde;o siguiente y la liquidaci&oacute;n de las cuentas del  a&ntilde;o anterior.</p>
        <p>Para la  realizaci&oacute;n de obras y servicios no previstos en el presupuesto ordinario  podr&aacute;n formalizarse presupuestos extraordinarios.</p>
        <p><b>Art&iacute;culo 26.<br />
        </b> La gesti&oacute;n  econ&oacute;mica-administrativa de la Asociaci&oacute;n corresponde a la Junta Directiva, y  la tramitaci&oacute;n de los asuntos que se deriven de aqu&eacute;lla la realizar&aacute;n los  servicios t&eacute;cnico-administrativos competentes, atendi&eacute;ndose a las decisiones y  acuerdos que se adopten por dicha Junta.</p>
        <p><b>Art&iacute;culo 27&ordm;.<br />
        </b> El patrimonio de  la Asociaci&oacute;n estar&aacute; integrado por:</p>
        <p> a) Los bienes y  derechos que posea en el momento de su constituci&oacute;n.</p>
        <p> b) Las  donaciones, legados y cualquier otro derecho adquirido a t&iacute;tulo lucrativo.</p>
        <p> c) Las acciones y  t&iacute;tulos representativos de capital de empresa p&uacute;blicas o privadas y las  obligaciones o t&iacute;tulos an&aacute;logos de los que la Asociaci&oacute;n llegue a ser titular.</p>
        <p>d) Los derechos  reales de los que la Asociaci&oacute;n llegue a ser titular, as&iacute; como aqu&eacute;llos de  cualquier naturaleza que se deriven del dominio que, respectivamente, ejerza  sobre sus bienes patrimoniales.</p>
        <p> El inventario de  los bienes y derechos propiedad de la Asociaci&oacute;n ser&aacute; actualizado al menos  anualmente con car&aacute;cter ordinario, siendo aprobado por la Junta Directiva que  dar&aacute; conocimiento a la Asamblea General en la primera reuni&oacute;n que celebre.</p>
        <p><b>Art&iacute;culo 28&ordm;.<br />
        </b>La Asociaci&oacute;n  dispondr&aacute; de los siguientes recursos econ&oacute;micos:</p>
        <p>a) Las cantidades  recaudadas en concepto de cuotas.</p>
        <p> b) Los productos  y rentas de los bienes que llegue a poseer, los intereses de sus cuentas y los  dem&aacute;s productos financieros.</p>
        <p>c) Las  donaciones, subvenciones y aportaciones que reciba.</p>
        <p> d) Cualquier otro  recurso obtenido de conformidad con las disposiciones legales y los <br />
        preceptos reglamentarios.</p>
        <p><b>Art&iacute;culo 29&ordm;.<br />
        </b> Los&nbsp; asociados&nbsp;  tienen la obligaci&oacute;n de contribuir al sostenimiento&nbsp; de&nbsp; la&nbsp; Asociaci&oacute;n, satisfaciendo&nbsp; las&nbsp; cuotas establecidas a propuesta de la Junta  Directiva&nbsp; o por&nbsp; iniciativa de la Asamblea General, recayendo  el acuerdo definitivo en esta &uacute;ltima.</p>
        <p> Para el  establecimiento y modificaci&oacute;n de cuotas se exigir&aacute; el acuerdo favorable de la  Asamblea General.</p>
        <p><b>Art&iacute;culo 30&ordm;.<br />
        </b>La modificaci&oacute;n  de los estatutos de la Asociaci&oacute;n podr&aacute; ser propuesta a la Asamblea General,  &uacute;nico &oacute;rgano facultado para aprobarla, por la Junta Directiva o por un n&uacute;mero  de miembros de la Asamblea General superior al treinta por ciento de la misma.</p>
        <p>En este &uacute;ltimo  caso, la propuesta de modificaci&oacute;n se someter&aacute; a la Junta Directiva con dos  meses de antelaci&oacute;n a la sesi&oacute;n de la Asamblea General.</p>
        <p> En todos los  casos, la Asamblea General debe ser convocada a tal fin con quince d&iacute;as de  anticipaci&oacute;n y la convocatoria deber&aacute; contener el texto de la modificaci&oacute;n  propuesta.</p>
        <p> La Asamblea  General podr&aacute; acordar la aprobaci&oacute;n de una propuesta de modificaci&oacute;n de los  estatutos, con el voto favorable de un m&iacute;nimo superior a los dos tercios del  total de asociados, en primera convocatoria, o de las tres cuartas partes de  los asistentes en segunda convocatoria, que a este efecto se celebrar&aacute;  transcurrido un m&iacute;nimo de 10 d&iacute;as.</p>
        <p><b>Art&iacute;culo 31&ordm;.<br />
        </b>La&nbsp; disoluci&oacute;n&nbsp;  voluntaria&nbsp; de&nbsp; la&nbsp;  Asociaci&oacute;n s&oacute;lo podr&aacute; ser acordada por la Asamblea <br />
        General convocada reglamentariamente a este objeto.</p>
        <p> La propuesta de  disoluci&oacute;n podr&aacute; formularse por la Junta Directiva o el setenta y cinco por  ciento de miembros de la Asociaci&oacute;n. En este &uacute;ltimo caso, la Junta Directiva  someter&aacute; la petici&oacute;n a la Asamblea General en un plazo m&aacute;ximo de dos meses. En  todo caso, &eacute;sta debe ser convocada con quince d&iacute;as de antelaci&oacute;n y la  convocatoria deber&aacute; contener el texto de la propuesta de disoluci&oacute;n.</p>
        <p>Para poder tratar  la disoluci&oacute;n de la Asociaci&oacute;n, la Asamblea General deber&aacute; contar con la  asistencia m&iacute;nima de las tres cuartas partes de los votos. Si este qu&oacute;rum no se  obtiene, pasados quince d&iacute;as se proceder&aacute; a una segunda convocatoria, y en  &eacute;sta, la Asamblea General podr&aacute; decidir sobre la disoluci&oacute;n, por mayor&iacute;a de las  tres cuartas partes de los votos emitidos en la sesi&oacute;n, cualquiera que sea el  n&uacute;mero de asistentes a la misma.</p>
        <p> En caso de  disoluci&oacute;n, voluntaria o impuesta por la Ley de la Asociaci&oacute;n, la Asamblea  General nombrar&aacute; por mayor&iacute;a simple, una comisi&oacute;n liquidadora compuesta por no  menos de tres miembros, determinando sus poderes y el destino a dar al eventual  saldo de liquidaci&oacute;n que, en lo posible, deber&aacute; tener un destino af&iacute;n a las  actividades de la Asociaci&oacute;n.</p>
        </div>
        </td>
            
<?php include ("piecera.php"); ?>