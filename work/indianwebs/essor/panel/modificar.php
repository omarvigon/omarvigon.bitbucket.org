<?php require_once('../Connections/essor.php'); ?>
<?php require_once('../Connections/essor.php'); ?>
<?php require_once('../Connections/essor.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE cursos SET titulo=%s, tipo=%s, subtipo=%s, descripcion_es=%s, descripcion_en=%s, descripcion_de=%s, descripcion_it=%s, descripcion_pt=%s, descripcion_fr=%s WHERE id=%s",
                       GetSQLValueString($_POST['titulo'], "text"),
					   GetSQLValueString($_POST['tipo'], "text"),
                       GetSQLValueString($_POST['subtipo'], "text"),
                       GetSQLValueString($_POST['descripcion_es_1']."<hr>".$_POST['descripcion_es_2'], "text"),
                       GetSQLValueString($_POST['descripcion_en_1']."<hr>".$_POST['descripcion_en_2'], "text"),
                       GetSQLValueString($_POST['descripcion_de_1']."<hr>".$_POST['descripcion_de_2'], "text"),
                       GetSQLValueString($_POST['descripcion_it_1']."<hr>".$_POST['descripcion_it_2'], "text"),
                       GetSQLValueString($_POST['descripcion_pt_1']."<hr>".$_POST['descripcion_pt_2'], "text"),
                       GetSQLValueString($_POST['descripcion_fr_1']."<hr>".$_POST['descripcion_fr_2'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_essor, $essor);
  $Result1 = mysql_query($updateSQL, $essor) or die(mysql_error());

  $updateGoTo = "inicio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_essor, $essor);
$query_Recordset1 = sprintf("SELECT * FROM cursos WHERE id = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $essor) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$division_es=explode("<hr>",$row_Recordset1['descripcion_es']);
$division_en=explode("<hr>",$row_Recordset1['descripcion_en']);
$division_de=explode("<hr>",$row_Recordset1['descripcion_de']);
$division_it=explode("<hr>",$row_Recordset1['descripcion_it']);
$division_pt=explode("<hr>",$row_Recordset1['descripcion_pt']);
$division_fr=explode("<hr>",$row_Recordset1['descripcion_fr']);
?>
<?php include("_cabecera.php");?>
<p>Modificar curso <?php echo $row_Recordset1['id']; ?></p>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
  <table align="center">
    <tr>
      <td nowrap="nowrap" align="right">Tipo:</td>
      <td colspan="2"><select name="tipo" class="caja1">
      <option value="Beginner" <?php if($row_Recordset1['tipo']=="Beginner") echo "selected='selected'";?>>Beginner</option>
      <option value="Intermediate" <?php if($row_Recordset1['tipo']=="Intermediate") echo "selected='selected'";?>>Intermediate</option>
      <option value="Advanced" <?php if($row_Recordset1['tipo']=="Advanced") echo "selected='selected'";?>>Advanced</option>
      <option value="ActiveEnglish" <?php if($row_Recordset1['tipo']=="ActiveEnglish") echo "selected='selected'";?>>Active English</option>
      </select></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Subtipo:</td>
      <td colspan="2"><select name="subtipo" class="caja1">
      <option value="Grammar" <?php if($row_Recordset1['subtipo']=="Grammar") echo "selected='selected'";?>>Grammar</option>
      <option value="Vocabulary" <?php if($row_Recordset1['subtipo']=="Vocabulary") echo "selected='selected'";?>>Vocabulary</option>
      <option value="Meetings" <?php if($row_Recordset1['subtipo']=="Meetings") echo "selected='selected'";?>>Meetings</option>
      <option value="Presentations" <?php if($row_Recordset1['subtipo']=="Presentations") echo "selected='selected'";?>>Presentations</option>
      <option value="BasicSurvival" <?php if($row_Recordset1['subtipo']=="BasicSurvival") echo "selected='selected'";?>>Basic Survival</option>
      <option value="MovingOn" <?php if($row_Recordset1['subtipo']=="MovingOn") echo "selected='selected'";?>>Moving On</option>
      </select></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Titulo:</td>
      <td colspan="2"><input type="text" name="titulo" size="80" value="<?=$row_Recordset1['titulo'];?>" /></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Castellano:</td>
      <td><textarea name="descripcion_es_1"><?=$division_es[0];?></textarea></td>
      <td><textarea name="descripcion_es_2"><?=$division_es[1];?></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">English:</td>
      <td><textarea name="descripcion_en_1"><?=$division_en[0];?></textarea></td>
      <td><textarea name="descripcion_en_2"><?=$division_en[1];?></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Deustch:</td>
      <td><textarea name="descripcion_de_1"><?=$division_de[0];?></textarea></td>
      <td><textarea name="descripcion_de_2"><?=$division_de[1];?></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Italiano:</td>
      <td><textarea name="descripcion_it_1"><?=$division_it[0];?></textarea></td>
      <td><textarea name="descripcion_it_2"><?=$division_it[1];?></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Portugues:</td>
      <td><textarea name="descripcion_pt_1"><?=$division_pt[0];?></textarea></td>
      <td><textarea name="descripcion_pt_2"><?=$division_pt[1];?></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Fran&ccedil;ais:</td>
      <td><textarea name="descripcion_fr_1"><?=$division_fr[0];?></textarea></td>
      <td><textarea name="descripcion_fr_2"><?=$division_fr[1];?></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td>&nbsp;</td>
      <td><br /><br /><input type="submit" value="Modificar" /><br /><br /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?php echo $row_Recordset1['id']; ?>" />
  <input type="hidden" name="MM_update" value="form1" />
</form>
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
