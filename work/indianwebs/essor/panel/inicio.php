<?php if($_GET["salir"]=="on")
{
	session_unset();
	session_destroy();
	echo "<script language='javascript'>window.open('index.php','_self')</script>";	
}?>
<?php require_once('../Connections/essor.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_essor, $essor);
$query_Recordset1 = "SELECT id, titulo, tipo, subtipo, descripcion_en FROM cursos ORDER BY tipo,titulo ASC";
$Recordset1 = mysql_query($query_Recordset1, $essor) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<?php include("_cabecera.php");?>
<p>LISTADO <b><?php echo $totalRows_Recordset1 ?></b> Cursos</p>
<p><a href="insertar.php">[+] Crear nuevo curso</a></p>
<table cellpadding="4" cellspacing="1" class="tabla1">
  <tr>
    <td class="cab">Tipo</td>
    <td class="cab">Subtipo</td>
    <td class="cab">Titulo</td>
    <td class="cab">&nbsp;</td>
    <td class="cab">&nbsp;</td>
    <td class="cab">&nbsp;</td>
  </tr>
  <?php do { ?>
    <tr>
      <td class="cab2"><?php echo $row_Recordset1['tipo']; ?></td>
      <td class="cab2"><?php echo $row_Recordset1['subtipo']; ?></td>
      <td class="cab2" width="400"><?php echo $row_Recordset1['titulo']; ?></td>
      <td class="cab2"><a href="archivo.php?id=<?php echo $row_Recordset1['id']; ?>">Archivo</a></td>
      <td class="cab2"><a href="modificar.php?id=<?php echo $row_Recordset1['id']; ?>">Modificar</a></td>
      <td class="cab2"><a href="eliminar.php?id=<?php echo $row_Recordset1['id']; ?>">Eliminar</a></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>
</body>
</html>
<?php mysql_free_result($Recordset1);?>
