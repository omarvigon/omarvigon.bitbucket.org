<?php require_once('../Connections/essor.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO cursos (titulo, tipo, subtipo, descripcion_es, descripcion_en, descripcion_de, descripcion_it, descripcion_pt, descripcion_fr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['titulo'], "text"),
					   GetSQLValueString($_POST['tipo'], "text"),
                       GetSQLValueString($_POST['subtipo'], "text"),
                       GetSQLValueString($_POST['descripcion_es_1']."<hr>".$_POST['descripcion_es_2'], "text"),
                       GetSQLValueString($_POST['descripcion_en_1']."<hr>".$_POST['descripcion_en_2'], "text"),
                       GetSQLValueString($_POST['descripcion_de_1']."<hr>".$_POST['descripcion_de_2'], "text"),
                       GetSQLValueString($_POST['descripcion_it_1']."<hr>".$_POST['descripcion_it_2'], "text"),
                       GetSQLValueString($_POST['descripcion_pt_1']."<hr>".$_POST['descripcion_pt_2'], "text"),
                       GetSQLValueString($_POST['descripcion_fr_1']."<hr>".$_POST['descripcion_fr_2'], "text"));

  mysql_select_db($database_essor, $essor);
  $Result1 = mysql_query($insertSQL, $essor) or die(mysql_error());

  $insertGoTo = "inicio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<?php include("_cabecera.php");?>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
  <table align="center">
    <tr>
      <td align="right">Tipo:</td>
      <td><select name="tipo" class="caja1">
      <option value="Beginner">Beginner</option>
      <option value="Intermediate">Intermediate</option>
      <option value="Advanced">Advanced</option>
      <option value="ActiveEnglish">Active English</option>
      </select></td>
    </tr>
    <tr>
      <td align="right">Subtipo:</td>
      <td><select name="subtipo" class="caja1">
      <option value="Grammar">Grammar</option>
      <option value="Vocabulary">Vocabulary</option>
      <option value="Meetings">Meetings</option>
      <option value="Presentations">Presentations</option>
      <option value="BasicSurvival">Basic Survival</option>
      <option value="MovingOn">Moving On</option>
      </select>
      </td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">Titulo:</td>
      <td colspan="2"><input type="text" name="titulo" size="80" /></td>
    </tr>
    </table>
    
    <table align="center">
    <tr>
    	<td colspan="2">Castellano</td>
    </tr>
    <tr>
      <td><textarea name="descripcion_es_1"></textarea></td>
      <td><textarea name="descripcion_es_2"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2">English</td>
    </tr>
    <tr>
      <td><textarea name="descripcion_en_1"></textarea></td>
      <td><textarea name="descripcion_en_2"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2">Deustch</td>
    </tr>
    <tr>
      <td><textarea name="descripcion_de_1"></textarea></td>
      <td><textarea name="descripcion_de_2"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2">Italiano</td>
    </tr>
    <tr>
      <td><textarea name="descripcion_it_1"></textarea></td>
      <td><textarea name="descripcion_it_2"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2">Portugues</td>
    </tr>
    <tr>
      <td><textarea name="descripcion_pt_1"></textarea></td>
      <td><textarea name="descripcion_pt_2"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2">Fran&ccedil;ais</td>
    </tr>
    <tr>
      <td><textarea name="descripcion_fr_1"></textarea></td>
      <td><textarea name="descripcion_fr_2"></textarea></td>
    </tr>
    <tr>
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><br /><br /><input type="submit" value="Insertar" /><br /><br /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
</body>
</html>