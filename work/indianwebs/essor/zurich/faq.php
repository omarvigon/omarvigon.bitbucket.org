<?php include("1cabecera.php");?>
<div id="centro">
    <table cellspacing="8" cellpadding="0">
    <tr>
		<td style="background:#eeeeee;border-right:20px solid #ffffff;"><br /><img title="FAQ" src="img/faq.jpg" alt="FAQ" /></td>
        <td>
        <h2>F.A.Q.</h2>
		<?php switch($_SESSION["lang"])
        {
            case "es": ?>
        <p class="azul"><strong>&iquest;Para qui&eacute;n es el English Corner?</strong><br />
  Este sitio web est&aacute; dise&ntilde;ado como un recurso de  referencia para el personal de Zurich que utiliza el ingl&eacute;s. No intenta ser una  gu&iacute;a completa para hablar ingl&eacute;s, pero est&aacute; aqu&iacute; para asistirte mientras  progresas en tus estudios. </p>
<p class="azul"><strong>&iquest;C&oacute;mo puedo utilizar el English Corner?</strong> <br />
  Hay tres secciones principales, ordenadas seg&uacute;n tu  nivel de ingl&eacute;s:</p>
<p class="azul">Si eres nuevo en el aprendizaje de ingl&eacute;s, es mejor  empezar con la secci&oacute;n &lsquo;Principiante&rsquo;. Aqu&iacute;, el texto ha sido traducido a  espa&ntilde;ol, franc&eacute;s, alem&aacute;n, italiano y portugu&eacute;s para ayudarte a empezar. </p>
<p class="azul">Si tienes un nivel bastante bueno de vocabulario y  comprensi&oacute;n inglesa, al principio te recomendamos el uso de las p&aacute;ginas  &lsquo;Intermedio&rsquo;. Si las encuentras demasiado dif&iacute;ciles, siempre puedes volver a  &lsquo;Principiante&rsquo;, &iexcl;pero es importante que te pruebes!</p>
<p class="azul">Si est&aacute;s seguro con tu ingl&eacute;s, entonces ves  directamente a &lsquo;Avanzado&rsquo;. </p>
<p class="azul">Una vez has le&iacute;do una p&aacute;gina en cualquiera de estas  secciones, haz clic en &lsquo;test&rsquo; para ver lo bien que has recordado esta nueva  informaci&oacute;n. </p>
<p class="azul">La secci&oacute;n &lsquo;Active English&rsquo; proporciona vocabulario  y frases &uacute;tiles para situaciones cotidianas de la vida real; tales como  reuniones, presentaciones, viajes, etc. &iexcl;Tambi&eacute;n hay una gu&iacute;a de supervivencia  b&aacute;sica para tu primer viaje a un pa&iacute;s de habla inglesa!</p>
<p class="azul"><strong>&iquest;Tengo que seguir las p&aacute;ginas en orden?</strong> <br />
  No, no hay ninguna necesidad de estudiar cada p&aacute;gina  en orden. Esta es una gu&iacute;a de referencia para que la visites cuando necesites  un poco de ayuda extra con tus estudios. </p>
<p class="azul"><strong>&iquest;C&oacute;mo puedo conseguir el English Corner en mi lengua?</strong> <br />
  Todas las p&aacute;ginas de &lsquo;Principiante&rsquo; han sido  traducidas a espa&ntilde;ol, franc&eacute;s, alem&aacute;n, italiano y portugu&eacute;s. Esto es para  ayudarte a empezar con el ingl&eacute;s y asegurarnos de que no hay malentendidos. Una  vez llegas a &lsquo;Intermedio&rsquo;, no deber&iacute;as necesitar una traducci&oacute;n&hellip; </p>
<p class="azul"><strong>Una prueba sigue dici&eacute;ndome que mi respuesta es incorrecta, &iexcl;pero  estoy seguro que no lo es!</strong> <br />
  Nuestras pruebas son extremadamente estrictas&hellip; Es  bastante posible que est&eacute;s totalmente en lo cierto desde un punto de vista  ortogr&aacute;fico o gramatical, pero que hayas olvidado una coma o algo similar. Si  realmente no puedes encontrar la respuesta exacta que estamos buscando, no te  enojes, &iexcl;simplemente pasa a la siguiente pregunta!</p>
<p class="azul"><strong>&iquest;Puedo imprimir la informaci&oacute;n sobre el English Corner?</strong> <br />
  S&iacute;, muy f&aacute;cilmente. En cada p&aacute;gina ver&aacute;s un peque&ntilde;o  icono de impresi&oacute;n. Simplemente haz clic en este icono y el texto se  visualizar&aacute; en una ventana aparte como un documento para imprimir. Entonces  env&iacute;a este documento a imprimir de la forma habitual. </p>
<p class="azul"><strong>&iquest;Qu&eacute; sucede si quiero encontrar un tema de gram&aacute;tica en particular?  &iquest;Hay una forma m&aacute;s r&aacute;pida que hojeando las listas en cada secci&oacute;n?</strong><br />
  S&iacute;, en la p&aacute;gina de inicio hay una casilla de  b&uacute;squeda. S&oacute;lo introduce el tema de gram&aacute;tica que deseas (en ingl&eacute;s), por  ejemplo adjetivos, frases condicionales o pasado simple, y pulsa &lsquo;enter&rsquo;.  Aparecer&aacute; una lista de todas las p&aacute;ginas que cubren ese tema, y simplemente haz  clic sobre aquel que sea m&aacute;s pertinente para ti para obtener la informaci&oacute;n que  quieres. </p>
<p class="azul"><strong>&iquest;Qu&eacute; puedo encontrar en Active English?</strong><br />
  La secci&oacute;n &lsquo;Active English&rsquo; proporciona vocabulario  y frases &uacute;tiles para situaciones cotidianas de la vida real; tales como  reuniones, presentaciones, viajes, etc. &iexcl;Tambi&eacute;n hay una gu&iacute;a de supervivencia  b&aacute;sica para tu primer viaje a un pa&iacute;s de habla inglesa! La secci&oacute;n &lsquo;Moving On&rsquo;  tambi&eacute;n proporciona ayuda pr&aacute;ctica, especialmente para secciones de viajes y  sociales a un nivel un poco m&aacute;s avanzado. </p>
            <? break;
            
            case "fr":?>
<p class="azul"><strong>&Agrave; qui s&rsquo;adresse English Corner? </strong><br />
  Ce Site Internet est con&ccedil;u comme une ressource de  r&eacute;f&eacute;rence pour le personnel de Zurich qui utilise l&rsquo;Anglais. Il ne pr&eacute;tend pas  &ecirc;tre un guide complet pour parler Anglais, mais il est l&agrave; pour vous assister dans  vos progr&egrave;s.</p>
<p class="azul"><strong>Comment dois-je utiliser English Corner? </strong><br />
  Il y a trois sections principales, organis&eacute;es selon votre  niveau d&rsquo;Anglais&nbsp;:</p>
<p class="azul">Si vous d&eacute;butez dans l&rsquo;apprentissage de l&rsquo;Anglais, il  vaut mieux commencer par la section &lsquo;D&eacute;butant&rsquo;. Dans cette section, le texte a &eacute;t&eacute;  traduit en Espagnol, Fran&ccedil;ais, Allemand, Italien et Portugais pour vous aider &agrave;  d&eacute;marrer.</p>
<p class="azul">Si vous avez un assez bon niveau de compr&eacute;hension et de  vocabulaire en Anglais, nous vous recommandons d&rsquo;utiliser d&rsquo;abord les pages  &lsquo;Interm&eacute;diaire&rsquo;. Si vous trouvez que c&rsquo;est trop difficile, vous pouvez toujours  retourner &agrave; la section &lsquo;D&eacute;butant&rsquo; mais il est important de vous tester&nbsp;!</p>
<p class="azul">Si vous ma&icirc;trisez l&rsquo;Anglais, alors allez directement au  niveau &lsquo;Avanc&eacute;&rsquo;.</p>
<p class="azul">Une fois que vous avez lu une page de l&rsquo;une de ces trois sections,  cliquez sur &lsquo;test&rsquo; pour voir si vous avez bien retenu ces nouvelles  informations.</p>
<p class="azul">La section &lsquo;Active English&rsquo; vous fournit du vocabulaire  et des expressions utiles pour des situations r&eacute;elles de la vie quotidienne;  comme des r&eacute;unions, des pr&eacute;sentations, des voyages, etc. Il y a &eacute;galement un  manuel &eacute;l&eacute;mentaire de survie pour votre premier voyage dans un pays  Anglophone&nbsp;!</p>
<p class="azul"><strong>Dois-je suivre les pages dans l&rsquo;ordre?</strong><br />
  Non, il n&rsquo;est pas n&eacute;cessaire d&rsquo;&eacute;tudier chaque page dans  l&rsquo;ordre. C&rsquo;est un guide de r&eacute;f&eacute;rence &agrave; consulter si vous avez besoin d&rsquo;un petit  plus pour vous aider dans vos &eacute;tudes.</p>
<p class="azul"><strong>Comment puis-je acc&eacute;der &agrave; English Corner dans ma langue?</strong><br />
  Toutes les pages &lsquo;D&eacute;butant&rsquo; ont &eacute;t&eacute; traduites en  Espagnol, Fran&ccedil;ais, Allemand, Italien et Portugais. Ceci pour vous aider &agrave;  d&eacute;buter en Anglais et afin de s&rsquo;assurer qu&rsquo;il n&rsquo;y ait pas de malentendus. Une  fois que vous avez atteint le niveau &lsquo;Interm&eacute;diaire&rsquo;, vous ne devriez plus avoir  besoin de traduction&hellip;</p>
<p class="azul"><strong>Un test me dit sans cesse que ma r&eacute;ponse est incorrecte, mais je suis s&ucirc;r  que non&nbsp;!</strong><br />
  Nos tests sont extr&ecirc;mement stricts... Il est tout &agrave; fait possible  que vous ayez r&eacute;pondu correctement d&rsquo;un point de vue orthographique ou  grammatical, mais que vous ayez oubli&eacute; une virgule ou quelque chose dans le  genre. Si vous n&rsquo;arrivez vraiment pas &agrave; trouver la r&eacute;ponse exacte que nous  attendons, ne vous f&acirc;chez pas, passez simplement &agrave; la question suivante&nbsp;!</p>
<p class="azul"><strong>Puis-je imprimer les informations d&rsquo;English Corner?</strong><br />
Oui - tr&egrave;s facilement. Sur chaque page, vous verrez une  petite ic&ocirc;ne d&rsquo;impression. Il suffit de cliquer sur cette ic&ocirc;ne et le texte  appara&icirc;tra dans une fen&ecirc;tre &agrave; part comme document &agrave; imprimer. Envoyez ensuite ce  document &agrave; imprimer normalement.</p>
<p class="azul"><strong>Et si je veux trouver un point de grammaire sp&eacute;cifique&nbsp;? Y a t-il une  fa&ccedil;on plus rapide que de rechercher dans les listes de chaque section&nbsp;?</strong><br />
  Oui &ndash; sur la page d&rsquo;accueil il y a une case recherche. Il  suffit d&rsquo;y introduire le point de grammaire que vous recherchez (en Anglais),  par exemple adjectifs, phrases au conditionnel, pass&eacute; simple et de presser la  touche &lsquo;enter&rsquo;. Une liste de toutes les pages abordant ce th&egrave;me appara&icirc;tra et  il vous suffit de cliquer sur celle qui vous semble la plus pertinente pour obtenir  l&rsquo;information souhait&eacute;e.</p>
<p class="azul"><strong>Que puis-je trouver dans la section Active English?</strong><br />
  La section &lsquo;Active English&rsquo; fournit du vocabulaire et des  expressions utiles pour des situations r&eacute;elles de la vie quotidienne; comme des  r&eacute;unions, des pr&eacute;sentations, des voyages, etc.<br />
  Il y a m&ecirc;me une section &laquo;&nbsp;Basic Survival&nbsp;&raquo; pour  votre premier voyage dans un pays Anglophone&nbsp;! La Section &lsquo;Moving On&rsquo; vous  fournit &eacute;galement des conseils pratiques, notamment pour voyager et des  sections sociales &agrave; un niveau l&eacute;g&egrave;rement plus avanc&eacute;.</p>
            <? break;
            
            case "pt":?>
<p class="azul"><strong>Para  quem &eacute; o &ldquo;English Corner&rdquo;?</strong></p>
<p class="azul">Esta p&aacute;gina Web foi concebida para os  trabalhadores de Zurich que utilizam a l&iacute;ngua inglesa como um recurso de  refer&ecirc;ncia. N&atilde;o pretende ser um guia completo de conversa&ccedil;&atilde;o em ingl&ecirc;s, mas sim  uma ajuda para progredirem nos seus estudos.</p>
<p class="azul"><strong>Como  utilizar o &ldquo;English Corner&rdquo;?</strong> </p>
<p class="azul">Existem tr&ecirc;s sec&ccedil;&otilde;es principais, cada  uma organizada para o seu n&iacute;vel de ingl&ecirc;s</p>
<p class="azul">Se est&aacute; a iniciar-se na aprendizagem  de Ingl&ecirc;s, &eacute; recomend&aacute;vel que comece pela sec&ccedil;&atilde;o &ldquo;Beginner&rdquo;. Aqui poder&aacute;  encontrar os textos traduzidos para Espanhol, Franc&ecirc;s, Alem&atilde;o, Italiano e  Portugu&ecirc;s, como forma de, no in&iacute;cio, o ajudar a come&ccedil;ar.</p>
<p class="azul">Se j&aacute; possui um n&iacute;vel razo&aacute;vel de  compreens&atilde;o e vocabul&aacute;rio de Ingl&ecirc;s, recomendamos que comece pelas p&aacute;ginas de  &ldquo;Intermediate&rdquo;. Se estas lhe parecerem demasiado dif&iacute;ceis, pode sempre  regressar a &ldquo;Beginner&rdquo;, sendo que o importante &eacute; que examine o seu n&iacute;vel.</p>
<p class="azul">Se est&aacute; confiante com o seu n&iacute;vel de  Ingl&ecirc;s, ent&atilde;o passe directamente para &ldquo;Advanced&rdquo;.</p>
<p class="azul">Ap&oacute;s ler uma p&aacute;gina em qualquer uma  destas sec&ccedil;&otilde;es, clique em &ldquo;test&rdquo; para descobrir o que aprendeu com a nova  informa&ccedil;&atilde;o.</p>
<p class="azul">A Sec&ccedil;&atilde;o &ldquo;Active English&rdquo; fornece  vocabul&aacute;rio e frases &uacute;teis para situa&ccedil;&otilde;es da vida quotidiana, tais como  reuni&otilde;es, apresenta&ccedil;&otilde;es, viagens, etc. Encontrar&aacute; tamb&eacute;m aqui um guia b&aacute;sico de  sobreviv&ecirc;ncia para a sua primeira viagem a um pa&iacute;s de l&iacute;ngua inglesa.</p>
<p class="azul"><strong>Tenho  que seguir a ordem das p&aacute;ginas?</strong></p>
<p class="azul">N&atilde;o h&aacute; necessidade de estudar as  p&aacute;ginas por ordem. Este recurso funciona como um guia de refer&ecirc;ncia para  consulta quando necessitar de uma ajuda extra nos seus estudos.</p>
<p class="azul"><strong>Como  obtenho os conte&uacute;dos do &ldquo;English Corner&rdquo; na minha l&iacute;ngua?</strong></p>
<p class="azul">Todas as p&aacute;ginas da sec&ccedil;&atilde;o &ldquo;Beginner&rdquo;  se encontram traduzidas para Espanhol, Franc&ecirc;s, Alem&atilde;o, Italiano e Portugu&ecirc;s,  como uma ajuda para que se inicie no ingl&ecirc;s e para evitar erros de  interpreta&ccedil;&atilde;o. No momento em que atinja o n&iacute;vel &ldquo;Intermediate&rdquo; j&aacute; n&atilde;o  necessitar&aacute; de tradu&ccedil;&atilde;o&hellip;</p>
<p class="azul"><strong>Um  teste diz-me que a minha resposta &eacute; incorrecta, mas eu tenho a certeza que n&atilde;o  &eacute; assim!</strong></p>
<p class="azul">Os nossos testes s&atilde;o extremamente  rigorosos&hellip; &Eacute; poss&iacute;vel que, nesse caso, a resposta esteja perfeitamente correcta  de um ponto de vista ortogr&aacute;fico ou gramatical, mas que possa ter-se esquecido  de uma v&iacute;rgula ou algo semelhante. No caso em que n&atilde;o consiga realmente  encontrar a resposta exacta que procura, n&atilde;o se irrite e avance directamente  para a pergunta seguinte.</p>
<p class="azul"><strong>Posso  imprimir a informa&ccedil;&atilde;o de &ldquo;English Corner&rdquo;?</strong></p>
<p class="azul">Sim, &eacute; muito f&aacute;cil. Em cada p&aacute;gina  encontra um pequeno &iacute;cone de impressora. Clique simplesmente nesse &iacute;cone e o  texto aparecer&aacute; numa janela pop-up, em vers&atilde;o para impress&atilde;o.<br />
  Daqui poder&aacute; imprimir como  normalmente.</p>
<p class="azul"><strong>Se  procuro um item gramatical espec&iacute;fico, existe alguma forma mais r&aacute;pida do que  procurar nas listas de cada sec&ccedil;&atilde;o?</strong></p>
<p class="azul">Sim, na Homepage encontra um campo de  pesquisa. Insira o tema gramatical que deseja encontrar (em ingl&ecirc;s), como por  exemplo <em>adjectives</em>, <em>conditional sentences</em>, <em>past simple</em> e prima <em>Enter</em>. Aparecer&aacute; uma lista com todas as p&aacute;ginas existentes sobre  esse t&oacute;pico e simplesmente ter&aacute; que clicar no mais relevante para obter a informa&ccedil;&atilde;o  que deseja.</p>
<p class="azul"><strong>O  que posso encontrar em &ldquo;Active English&rdquo;?</strong></p>
<p class="azul">A sec&ccedil;&atilde;o &ldquo;Active English&rdquo; fornece  vocabul&aacute;rio e frases &uacute;teis para situa&ccedil;&otilde;es quotidianas, tais como reuni&otilde;es,  apresenta&ccedil;&otilde;es, viagens, etc. Encontrar&aacute; tamb&eacute;m um guia b&aacute;sico de sobreviv&ecirc;ncia  para a sua primeira viagem a um pa&iacute;s de l&iacute;ngua inglesa! A sec&ccedil;&atilde;o &ldquo;moving on&rdquo;  tamb&eacute;m lhe dar&aacute; ajuda pr&aacute;tica para viagens e situa&ccedil;&otilde;es sociais a um n&iacute;vel  ligeiramente mais avan&ccedil;ado.</p>
            <? break;
            
			case "de": ?>
<p class="azul"><strong>Wer kann <em>English Corner</em> benutzen?</strong><br />
Diese website ist als eine Hilfsquelle f&uuml;r die  Mitarbeiter von Z&uuml;rich Versicherung entstanden. Sie ist nicht als ein  kompletter englischer Sprachf&uuml;hrer konzipiert, sondern als ein Hilfsmittel zur  Aufbesserung der englischen Sprachkenntnisse</p>
<p class="azul"><strong>Wie benutze ich <em>English Corner</em>?</strong><br />
  Es gibt drei Abschnitte, die auf die unterschiedlichen  Sprachniveaus ausgerichtet sind:</p>
<p class="azul">Anf&auml;nger w&auml;hlen den Abschnitt &ldquo;Beginner&rdquo;. Die hier  vorliegenden Texte finden Sie auch in spanischer, franz&ouml;sischer, deutscher,  italienischer und portugiesischer &Uuml;bersetzung, sodass Sie problemlos in das  Lernprogramm einsteigen k&ouml;nnen.</p>
<p class="azul">Wenn Sie bereits &uuml;ber gute GrundKenntnisse in Englisch  verf&uuml;gen, gehen Sie zun&auml;chst auf den Abschnitt &bdquo;Intermediate&ldquo;. Sollte dieses  Niveau f&uuml;r Ihre Kenntnisse zu hoch sein, so k&ouml;nnen Sie immer auf &bdquo;Beginner&ldquo;  zur&uuml;ckgehen. Wichtig ist jedoch, dass Sie sich selbst testen und einsch&auml;tzen! </p>
<p class="azul">Verf&uuml;gen Sie bereits &uuml;ber sehr gute Englischkenntnisse,  so gehen Sie direkt auf &ldquo;Advanced&rdquo;. </p>
<p class="azul">Lesen Sie eine der Seiten aus dem ausgew&auml;hlten Abschnitt,  und klicken sie dann auf &ldquo;test&rdquo;, um zu erfahren, inwieweit Sie die neuen  Informationen verarbeitet haben.</p>
<p class="azul">In &ldquo;Avtive English&rdquo; finden Sie Vokabular und&nbsp; Redewendungen, die f&uuml;r Situationen im Alltag  und Gesch&auml;ftsleben ( z.B. bei Versammlungen, Pr&auml;sentationen oder auf Reisen)  sehr n&uuml;tzlich sein k&ouml;nnen. Hier finden Sie auch einen Sprachf&uuml;hrer, der Ihnen  hilft, Ihre erste Reise in ein englisch sprechendes Land ohne gro&szlig;e  Katastrophen zu &uuml;berstehen.</p>
<p class="azul"><strong>Muss ich die Reihenfolge der Seiten beachten?</strong><br />
Nein, Sie brauchen die Seiten nicht in der gegebenen  Reihenfolge zu bearbeiten. Falls Sie zus&auml;tzliche Hilfe bei einem der Themen  ben&ouml;tigen, k&ouml;nnen Sie immer den Referenzf&uuml;hrer zu Rate ziehen.</p>
<p class="azul"><strong>Ist English Corner auch  in andere Sprachen &uuml;bersetzt?</strong><br />
Alle Seiten aus der Sektion &ldquo;Beginner&rdquo; sind in spanisch,  franz&ouml;sisch, deutsch, italienisch und portugiesisch &uuml;bersezt. So k&ouml;nnen  Sie&nbsp; ohne Verst&auml;ndnisschwierigkeiten mit  dem englischen Sprachprogramm beginnen. Wenn Sie die Sektion &ldquo;Intermediate&rdquo;  erreicht haben, werden Sie keine &Uuml;bersetzungen mehr ben&ouml;tigen.</p>
<p class="azul"><strong>Laut Testergebnis habe ich einen Fehler gemacht, obwohl ich sicher bin, die  richtige Antwort gegeben zu haben!</strong><br />
Unsere Tests sind sehr streng! M&ouml;glicherweise haben Sie in  Ihrer grammatisch oder orthographisch korrekten Antwort einen Kommafehler oder  &auml;hnliches. &Auml;rgern Sie sich nicht, wenn Sie die gew&uuml;nschte Antwort nicht finden,  sondern gehen Sie einfach auf die n&auml;chste Frage.</p>
<p class="azul"><strong>Kann ich die Informationen zu English Corner auch ausdrucken?</strong><br />
Ja, nat&uuml;rlich! Auf jeder Seite finden Sie das Symbol zum  Ausdrucken. Klicken Sie darauf und der Text erscheint in einem pop-up-Fenster  zum Ausdrucken bereit.</p>
<p class="azul"><strong>Ich suche ein bestimmtes grammatisches Thema. Muss ich durch die ganzen  Listen der einzelnen Sektionen gehen oder gibt es einen schnelleren Weg?</strong><br />
Auf unserer home page finden Sie eine Suchmaschine. Geben  Sie einfach Ihr gew&uuml;nschtes Thema in englischer Sprache ein, z.B. adjectives,  conditional sentences, past simple und klicken Sie auf enter. Sie werden eine  Liste mit allen Seiten bekommen, die Ihr gew&uuml;naschtes Thema behandeln. Klicken  Sie auf die gew&uuml;nschte Seite und Sie werden die gesuchte Information  finden.&nbsp;</p>
<p class="azul"><strong>Was finde ich auf der Seite <em>Active  English</em>?</strong><br />
  In &ldquo;Avtive English&rdquo; finden Sie Vokabular und&nbsp; Redewendungen, die f&uuml;r Situationen im Alltag  und Gesch&auml;ftsleben ( z.B. bei Versammlungen, Pr&auml;sentationen oder auf Reisen)  sehr n&uuml;tzlich sein k&ouml;nnen. Hier finden Sie auch einen Sprachf&uuml;hrer, der Ihnen  hilft, Ihre erste Reise in ein englisch sprechendes Land ohne gro&szlig;e  Katastrophen zu &uuml;berstehen. In der Sektion <em>Moving  On </em>finden insbesondere fortgeschrittene Lerner praktische Ratschl&auml;ge, die  Ihnen auf Reisen und im&nbsp;  gesellschaftlichen Umgang behilflich sein k&ouml;nnen.</p>            
			<? break;
					
			case "it": ?>
<p class="azul"><strong>Per chi &egrave; l&rsquo;English Corner?</strong><br />
  Questa pagina &egrave; stata messa a punto come risorsa di  riferimento per personale Zurich che usa l&rsquo;inglese. Non intende essere una  guida completa per parlare inglese, quanto piuttosto offrire assistenza durante  lo studio.</p>
<p class="azul"><strong>Come uso l&rsquo;English Corner?</strong><br />
  Ci sono tre sezioni principali, organizzate in base al vostro  livello di inglese.</p>
<p class="azul">Se partite da zero con l&rsquo;apprendimento dell&rsquo;inglese, vi  conviene cominciare dalla sezione &lsquo;<em>Beginner</em>&rsquo;  (principianti), dove il testo &egrave; stato tradotto in spagnolo, francese, tedesco,  italiano e portoghese per facilitarvi l&rsquo;avvio.</p>
<p class="azul">Se la vostra comprensione dell&rsquo;inglese &egrave; abbastanza buona  e anche il vostro vocabolario &egrave; abbastanza ricco, vi consigliamo di iniziare  dalle pagine &lsquo;<em>Intermediate&rsquo;</em> (intermedi). Potete sempre tornare a &lsquo;<em>Beginner</em>&rsquo;  se vi sembrano troppo difficili, ma l&rsquo;importante &egrave; mettersi alla prova!</p>
<p class="azul">Se siete vi sentite sicuri del vostro inglese, andate  direttamente ad &lsquo;<em>Advanced</em>&rsquo;  (avanzati).</p>
<p class="azul">Dopo aver letto una pagina di ogni sezione, fate clic su  &lsquo;test&rsquo; per verificare quanto ricordate delle nuove informazioni.</p>
<p class="azul">La sezione &lsquo;<em>Active  English</em>&rsquo; (inglese attivo) propone un vocabolario e un frasario utili per  situazioni comuni di vita reale, come incontri, presentazioni, viaggi e cos&igrave;  via. Troverete anche una guida essenziale di sopravvivenza per il vostro primo  viaggio in un Paese di lingua inglese! </p>
<p class="azul"><strong>Devo seguire le pagine secondo il loro ordine? </strong><br />
  No, non &egrave; necessario seguire l&rsquo;ordine delle pagine.  Questa &egrave; una guida di riferimento che potete consultare quando vi serve un po&rsquo;  di aiuto extra per i vostri studi.</p>
<p class="azul"><strong>Come posso vedere l&rsquo;English Corner nella mia lingua?</strong><br />
  Tutte le pagine per principianti (<em>Beginner</em>) sono state tradotte in spagnolo, francese, tedesco,  italiano e portoghese per aiutarvi ad iniziare con questa lingua e perch&eacute; non  ci siano cattive malintesi. Quando sarete al livello intermedio non dovreste  aver pi&ugrave; bisogno di traduzioni...</p>
<p class="azul"><strong>Un test continua a dirmi che la mia risposta non &egrave; esatta, ma sono sicuro  che &egrave; corretta!</strong><br />
  I nostri test sono estremamente rigorosi&hellip; &Egrave; possibile ad  esempio che abbiate fatto tutto giusto dal punto di vista ortografico o grammaticale  ma che, ad esempio, abbiate dimenticato una virgola. Se davvero non potete  fornire proprio la risposta che vogliamo, non arrabbiatevi, passate alla  domanda successiva!</p>
<p class="azul"><strong>Posso stampare le informazioni su Zurich Corner?</strong><br />
  S&igrave;, &egrave; molto semplice. In ogni pagina vedrete una piccola  icona con l&rsquo;immagine di una stampante. Basta fare clic sull&rsquo;icona e il test sar&agrave;  visualizzato in un pop-up a parte come documento da stampare, dopo di che  potete procedere alla stampa come di consueto.</p>
<p class="azul"><strong>Cosa devo fare se voglio trovare un determinato punto grammaticale? C&rsquo;&egrave; un  sistema pi&ugrave; veloce che sfogliare tutte le liste di ogni sezione? </strong><br />
  S&igrave;. Sulla pagina iniziale troverete una casella per la ricerca  nella quale dovete semplicemente digitare il punto grammaticale che vi  interessa (in inglese), ad esempio <em>adjectives </em>(aggettivi), <em>conditional sentences</em> (frasi al condizionale), <em>past simple</em> e  premere invio. Apparir&agrave; una lista con tutte le pagine che trattano l&rsquo;argomento,  quindi non dovrete che fare clic sulla pagina pi&ugrave; appropriata per ottenere le  informazioni che desiderate.</p>
<p class="azul"><strong>Cosa posso trovare in Active English?</strong><br />
  La sezione &lsquo;<em>Active  English</em>&rsquo; propone un vocabolario e un frasario utili per situazioni comuni  di vita reale, come incontri, presentazioni, viaggi e cos&igrave; via. Troverete anche  una guida essenziale di sopravvivenza per il vostro primo viaggio in un Paese  di lingua inglese! Anche la sezione '<em>Moving  On&rsquo;</em> fornisce aiuto pratico, in particolare per situazioni di viaggio e  sociali di livello leggermente pi&ugrave; avanzato.</p>            
			<? break;
			
            case "en":
            default: ?>
<p class="azul"><strong>Who is English Corner for?</strong><br />
  This website is designed as a reference resource for Zurich personnel who use  English. It is not intended to be a complete guide to speaking English, but is  here to assist you as you progress through your studies.</p>
<p class="azul"><strong>How do I use English Corner?</strong><br />
  There are three main sections, organised by your  English level:</p>
<p class="azul">If you are new to English learning, it is best to  start with the &lsquo;Beginner&rsquo; section. Here, the text has been translated into  Spanish, French, German, Italian and Portuguese to help you get started.</p>
<p class="azul">If you have a fairly good level of English  understanding and vocabulary, we recommend using the &lsquo;Intermediate&rsquo; pages at  first. If you find it too difficult, you can always go back to &lsquo;Beginner&rsquo; but  it is important to test yourself!</p>
<p class="azul">If you are confident with your English, then go  straight for &lsquo;Advanced&rsquo;.</p>
<p class="azul">Once you have read a page in any of these sections,  click on &lsquo;test&rsquo; to see how well you have remembered this new information.</p>
<p class="azul">The &lsquo;Active English&rsquo; section provides vocabulary and useful  phrases for common real life situations; such as meetings, presentations,  travel etc. There is also a basic survival guide for your first trip to an  English-speaking country! </p>
<p class="azul"><strong>Do I have to follow the pages in order?</strong><br />
  No, there is no need to study each page in order. This  is a reference guide for you to visit when you need a little extra help with  your studies.</p>
<p class="azul"><strong>How can I get English Corner in my language?</strong><br />
  All of the &lsquo;Beginner&rsquo; pages have been translated into  Spanish, French, German, Italian and Portuguese. This is to help you get  started with English and to make sure there are no misunderstandings. Once you  reach &lsquo;Intermediate&rsquo; you shouldn&rsquo;t need a translation&hellip;</p>
<p class="azul"><strong>A test keeps telling me that my answer is incorrect, but I&rsquo;m sure it&rsquo;s  not!</strong><br />
  Our tests are extremely strict&hellip; It&rsquo;s quite possible  that you are entirely correct from a spelling or grammar point of view, but  have forgotten a comma or something similar. If you really can&rsquo;t find the exact  answer we&rsquo;re looking for, don&rsquo;t get angry, just move on to the next question! </p>
<p class="azul"><b>Can I print the information on Zurich  Corner</b><br />
  Yes &ndash; very easily. On each page you will see a small  print icon. Simply click on this icon and the text will display in a separate  pop-up as print friendly document. Then send this document to print in your  normal way.</p>
<p class="azul"><b>What if I want to find a particular grammar point? Is there a quicker  way than looking through the lists on each section?</b><br />
  Yes &ndash; on the home page there is a search box. Just  type in the grammar point you want (in English), for example adjectives, conditional  sentences, past simple and press enter. A list of all the pages covering that  topic will appear and simply click on the one which is most relevant to you to  get the information you want.</p>
<p class="azul"><b>What can I find in Active English?</b><br />
  The &lsquo;Active English&rsquo; section provides vocabulary and  useful phrases for common real life situations; such as meetings,  presentations, travel etc. There is also a basic survival guide for your first  trip to an English-speaking country! The Moving On section also provides practical  help, particularly for travel and social sections at a slightly more advanced  level.</p>  
            <? break;
        } ?> 
        </td>
    </tr>
    </table> 
</div>       
<?php include("2piecera.php");?>