<?php include("1cabecera.php");?>
<div id="centro">
    <table cellspacing="8" cellpadding="0">
    <tr>
		<td><img title="" src="img/index.jpg" alt="" /></td>
        <td>
        	<div style="margin:20px 0 20px 0;">
            	<form method="post" action="index.php">
                <input type="text" style="width:400px;border:1px solid #000000;" name="busqueda" />
                <input type="submit" style="border:1px solid #000000;" value="SEARCH" />
                </form>
            </div>
            <?
            if($_POST["busqueda"])
			{
				$resultado=ejecutar("select * from cursos where lower(titulo) like lower('%".$_POST["busqueda"]."%') ");
				if(mysql_num_rows($resultado)==0)
					$texto="<p>No match found <b>".$_POST["busqueda"]."</b></p>";
				while($fila=mysql_fetch_assoc($resultado))
				{
					if($fila["tipo"]=="Advanced" || $fila["tipo"]=="Intermediate")
						$idioma="en";
					else
						$idioma=($_SESSION["lang"]!="en")? $_SESSION["lang"]:"es";
					
					$texto.='<p class="busq"><a title="" href="cursos.php?tipo='.strtolower($fila["tipo"]).'&subtipo='.strtolower($fila["subtipo"]).'&curso='.$fila["id"].'&lang='.$idioma.'" target="_blank">'.$fila["tipo"].' -> '.$fila["subtipo"].' -> '.$fila["titulo"].'</a></p>';
				}
				echo '<div style="width:610px;padding:20px;">'.$texto.'</div>';
			}
			else
			{ ?>
                <div style="width:610px;height:210px;padding:20px;margin:0 0 0 40px;background:url(img/fondo_index.gif) no-repeat #e5e5f1;">
                <h2>English Corner</h2>
				<?php switch($_SESSION["lang"])
                {
                    case "es": ?>
            <p class="azul">Bienvenido al  English Corner, ofrecido por Zurich Academy en Europa, realizado por Zurich Academy en Espa&ntilde;a. </p>
            <p class="azul">Utiliza English  Corner como un recurso independiente como ayuda para llevar a cabo tu trabajo y  actividades sociales diarias en ingl&eacute;s. El English Corner est&aacute; disponible 24  horas 7 d&iacute;as a la semana a trav&eacute;s de Zurich Academy Online. </p>
			<p class="azul">Haz clic <a title="Help" href="faq.php">aqu&iacute; para m&aacute;s informaci&oacute;n</a> sobre c&oacute;mo utilizar este recurso, o cliquea  directamente sobre uno de los botones en la barra del navegador para empezar.</p>
                    <? break;
                    
					case "fr":?>
			<p class="azul">Bienvenue sur  English Corner, offert par Zurich Academy en Europe, fourni par Zurich Academy  en Espagne.</p>
            <p class="azul">Utilisez English Corner  comme une ressource ind&eacute;pendante pour vous aider au quotidien dans votre  profession et vos activit&eacute;s sociales en Anglais. English Corner  est disponible 24 heures sur 24, 7 jours sur 7 &agrave; travers Zurich Academy  Online.</p>
			<p class="azul">Cliquez <a title="Help" href="faq.php">ici pour  plus d&rsquo;informations</a> sur l&rsquo;utilisation de cette ressource, ou directement sur un  des boutons de la barre de navigation pour commencer.</p>
                    <? break;
					
					case "pt":?>
			<p class="azul">Bem-vindo ao  English Corner, uma p&aacute;gina oferecida pela Zurich Academy em&nbsp; Europa, disponibilizada pela Zurich Academy em  Espanha.</p>
            <p class="azul">Utilize o &ldquo;English Corner&rdquo; como um  recurso independente que o ajudar&aacute; no dia-a-dia a realizar os seus neg&oacute;cios e  actividades sociais em ingl&ecirc;s. O English Corner encontra-se  dispon&iacute;vel 24 horas por dia, 7 dias por semana, atrav&eacute;s da Zurich Academy  Online</p>
			<p class="azul">Clique <a title="Help" href="faq.php">aqui para obter informa&ccedil;&atilde;o</a> adicional sobre como utilizar este recurso  ou&nbsp;clique directamente num dos bot&otilde;es da barra de navega&ccedil;&atilde;o para come&ccedil;ar.</p>
                    <? break;
					
					case "de": ?>
           	<p class="azul">Willkommen  bei ENGLISH CORNER, ein Angebot der ZURICH ACADEMY in Europa, das von der  ZURICH ACADEMY in Spanien f&uuml;r Sie bereitgestellt wird.</p>
            <p class="azul">English Corner bietet Ihnen eine umfangreiche  Sammlung&nbsp; englischer Materialien, auf die  Sie sowohl bei Ihrer gesch&auml;ftlichen als auch privaten Kommunikation in  englischer Sprache jederzeit zur&uuml;ckgreifen k&ouml;nnen. English Corner ist jederzeit  durch Zurich Academy Online erreichbar.</p>
			<p class="azul">Klicken <a title="Help" href="faq.php">Sie hier, wenn Sie mehr &uuml;ber den Gebrauch</a> dieses Angebots wissen  m&ouml;chten oder aber gehen Sie direkt auf eine der Seiten im Navigationssystem und  beginnen Sie jetzt mit dem Programm.</p>       
					<? break;
					
					case "it": ?>
            <p class="azul">Benvenuti all&rsquo;English Corner offertovi dalla Zurich  Academy in Europe fornito da Zurich Academy  in Spain.</p>
            <p class="azul">Utilizzate l&rsquo;English Corner come risorsa indipendente su cui contare per svolgere in  inglese le vostre giornaliere attivit&agrave; professionali e sociali. English Corner  &egrave; disponibile 24 ore al giorno 7 giorni la settimana su Zurich Academy Online.</p>
			<p class="azul">Fate <a title="Help" href="faq.php">clic qui per leggere maggiori informazioni</a> su come utilizzare questa  risorsa o premete direttamente uno dei pulsanti nella barra di navigazione per  iniziare.</p>
					<? break;
					
                    case "en":
                    default: ?>
			<p class="azul">Welcome to  English Corner, brought to you by Zurich  Academy in Europe, provided by Zurich Academy  in Spain.</p>
            <p class="azul">Use English Corner as an independent resource to help you  carry out your day-to-day business and social activities in English. English  Corner is available 24 hours 7 days per week&nbsp;  through Zurich Academy Online.</p>
			<p class="azul">Click <a title="Help" href="faq.php">here for more information</a> on how to use this  resource or directly on one of the buttons in the navigation bar to get  started.</p>
                    <? break;
                } ?>
                </div>
             <? } ?>   
        </td>
    </tr>
    </table> 
</div>       
<?php include("2piecera.php");?>