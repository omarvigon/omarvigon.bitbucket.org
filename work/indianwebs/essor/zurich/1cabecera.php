<?php include("_funciones.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ESSOR</title>
    <meta name="title" content="ESSOR" />
    <meta name="description" content="ESSOR" />
    <meta name="keywords" content="essor" />
    <meta name="language" content="es" />
    <link rel="stylesheet" type="text/css" href="_estilos.css" />
    <script language="javascript" type="text/javascript" src="_funciones.js"></script>
</head>

<body onload="aleatorio()">
<div style="position:absolute;width:500px;left:50%;margin-left:-320px;margin-top:20px;">
	<script type="text/javascript">new fadeshow(fadeimages, 481, 85, 0, 4000, 1)</script>
</div>

<div id="arriba">
	<div class="bandera">
    	European Edition
        <? if(!strstr($_SERVER['PHP_SELF'],"cursos.php")){ ?>

			<a title="English" href="<?=$_SERVER['PHP_SELF']?>?lang=en&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_uk<? if($_SESSION["lang"]=="en") echo "_on";?>.gif" align="absmiddle" alt="English" /></a>
        	<a title="Francais" href="<?=$_SERVER['PHP_SELF']?>?lang=fr&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_fr<? if($_SESSION["lang"]=="fr") echo "_on";?>.gif" align="absmiddle" alt="Francais" /></a>
        	<a title="Deutsch" href="<?=$_SERVER['PHP_SELF']?>?lang=de&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_de<? if($_SESSION["lang"]=="de") echo "_on";?>.gif" align="absmiddle" alt="Deutsch" /></a>
        	<a title="Italiano" href="<?=$_SERVER['PHP_SELF']?>?lang=it&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_it<? if($_SESSION["lang"]=="it") echo "_on";?>.gif" align="absmiddle" alt="Italiano" /></a>
        	<a title="Portugues" href="<?=$_SERVER['PHP_SELF']?>?lang=pt&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_pt<? if($_SESSION["lang"]=="pt") echo "_on";?>.gif" align="absmiddle" alt="Portugues" /></a>
        	<a title="Castellano" href="<?=$_SERVER['PHP_SELF']?>?lang=es&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_es<? if($_SESSION["lang"]=="es") echo "_on";?>.gif" align="absmiddle" alt="Castellano" /></a>
			
		<? } else {
        
        	if($_GET["tipo"]=="intermediate" || $_GET["tipo"]=="advanced" || $_GET["tipo"]=="activeenglish") { ?>
			<a title="English" href="<?=$_SERVER['PHP_SELF']?>?lang=en&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_uk<? if($_SESSION["lang"]=="en") echo "_on";?>.gif" align="absmiddle" alt="English" /></a>
        	<? } else { ?>
        	<a title="Francais" href="<?=$_SERVER['PHP_SELF']?>?lang=fr&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_fr<? if($_SESSION["lang"]=="fr") echo "_on";?>.gif" align="absmiddle" alt="Francais" /></a>
        	<a title="Deutsch" href="<?=$_SERVER['PHP_SELF']?>?lang=de&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_de<? if($_SESSION["lang"]=="de") echo "_on";?>.gif" align="absmiddle" alt="Deutsch" /></a>
        	<a title="Italiano" href="<?=$_SERVER['PHP_SELF']?>?lang=it&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_it<? if($_SESSION["lang"]=="it") echo "_on";?>.gif" align="absmiddle" alt="Italiano" /></a>
        	<a title="Portugues" href="<?=$_SERVER['PHP_SELF']?>?lang=pt&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_pt<? if($_SESSION["lang"]=="pt") echo "_on";?>.gif" align="absmiddle" alt="Portugues" /></a>
        	<a title="Castellano" href="<?=$_SERVER['PHP_SELF']?>?lang=es&tipo=<?=$_GET["tipo"]?>&subtipo=<?=$_GET["subtipo"]?>&curso=<?=$_GET["curso"]?>"><img title="" src="img/bandera_es<? if($_SESSION["lang"]=="es") echo "_on";?>.gif" align="absmiddle" alt="Castellano" /></a>
        	<? } 
        
        } ?>
    </div>
	<div class="boton">
    	<a title="Home" href="index.php" onmouseover="mostrar(0,0)">Home</a>
    	<a title="Beginner" href="#" onmouseover="mostrar(1,100)">Beginner</a>
    	<a title="Intermediate" href="#" onmouseover="mostrar(2,220)">Intermediate</a>
   	 	<a title="Advanced" href="#" onmouseover="mostrar(3,360)">Advanced</a>
    	<a title="Active English" href="#" onmouseover="mostrar(4,380)">Active English</a>
        <a title="Links" href="links.php" onmouseover="mostrar(0,0)">Links</a>
        <a title="FAQ" href="faq.php" onmouseover="mostrar(0,0)">FAQ</a>
        <a title="Exit" href="javascript:window.close()">Exit</a>
    </div>
    <div class="suboton">
    	<span id="sub1">
    	<a title="" href="cursos.php?tipo=beginner&subtipo=grammar">&raquo; Grammar</a>
        <a title="" href="cursos.php?tipo=beginner&subtipo=vocabulary">&raquo; Vocabulary</a>
        </span>
        <span id="sub2">
    	<a title="" href="cursos.php?tipo=intermediate&subtipo=grammar">&raquo; Grammar</a>
        <a title="" href="cursos.php?tipo=intermediate&subtipo=vocabulary">&raquo; Vocabulary</a>
        </span>
        <span id="sub3">
    	<a title="" href="cursos.php?tipo=advanced&subtipo=grammar">&raquo; Grammar</a>
        <a title="" href="cursos.php?tipo=advanced&subtipo=vocabulary">&raquo; Vocabulary</a>
        </span>
        <span id="sub4">
    	<a title="" href="cursos.php?tipo=activeenglish&subtipo=meetings">&raquo; Meetings</a>
        <a title="" href="cursos.php?tipo=activeenglish&subtipo=presentations">&raquo; Presentations</a>
        <a title="" href="cursos.php?tipo=activeenglish&subtipo=basicsurvival">&raquo; Basic Survival</a>
        <a title="" href="cursos.php?tipo=activeenglish&subtipo=movingon">&raquo; Moving On</a>
        </span>
    </div>
</div>