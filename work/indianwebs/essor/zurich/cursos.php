<?php include("1cabecera.php");
if($_GET["curso"]) 
{
	$linea=cargar_curso();
	
	$division=explode("<hr>",$linea["descripcion_".$_SESSION["lang"]]); 

	$division[0]=str_replace("<!--[if !supportLists]-->","",$division[0]);
	$division[0]=str_replace("<!--[if !supportLineBreakNewLine]-->","",$division[0]);
	$division[0]=str_replace("<!--[endif]-->","",$division[0]);
	$division[0]=str_replace('<span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: x-small;">','<span style="font-size: 8pt; font-family: Tahoma">',$division[0]);
	$division[0]=str_replace('<span style="font-size: x-small;">','',$division[0]);
	
	$division[1]=str_replace("<!--[if !supportLists]-->","",$division[1]);
	$division[1]=str_replace("<!--[if !supportLineBreakNewLine]-->","",$division[1]);
	$division[1]=str_replace("<!--[endif]-->","",$division[1]);
	$division[1]=str_replace('<span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: x-small;">','<span style="font-size: 8pt; font-family: Tahoma">',$division[1]);
	$division[1]=str_replace('<span style="font-size: x-small;">','',$division[1]);
}
?>
<div id="centro">
    <table cellspacing="8" cellpadding="0">
    <tr>
    	<td class="imprimir"><a href="javascript:imprimir('<?=$_GET["curso"]?>')"><img title="Print" src="img/print.gif" align="absmiddle" alt="Print" /> Print Version</a></td>
        <td colspan="2"><h1>
		<? 
		if($_GET["tipo"]=="activeenglish") echo "Active English"; 
		else 							   echo $_GET["tipo"];?> -> 
		
		<? if($_GET["subtipo"]=="basicsurvival") 	echo "Basic Survival";
		else if($_GET["subtipo"]=="movingon") 		echo "Moving On"; 
		else 										echo $_GET["subtipo"]; ?> 
        -> <?=$linea["titulo"]?></h1></td>
    </tr>
    <tr>
		<td class="izquierda"><?php cargar();?></td>
        <td class="columna_curso"><?=$division[0];?> 
			<?php if($_GET["curso"]=="92") echo "<img title='Clock' src='http://www.essor.es/zurich/img/clock.gif' alt='Clock' style='margin:16px 0;border:13px solid #ffffff;'>";?>
        </td>
        <td class="columna_curso"><span id="poner_img"></span>
		<? if($division)
			echo $division[1];
		else
		{
			if($_GET["tipo"]=="beginner")	 	echo "<img class='imgside' src='img/curso23.jpg' alt='' /><br /><img class='imgside' src='img/curso24.jpg' alt='' />";
			if($_GET["tipo"]=="intermediate")	echo "<img class='imgside' src='img/curso19.jpg' alt='' /><br /><img class='imgside' src='img/curso20.jpg' alt='' />";
			if($_GET["tipo"]=="advanced")		echo "<img class='imgside' src='img/curso21.jpg' alt='' /><br /><img class='imgside' src='img/curso22.jpg' alt='' />";
			if($_GET["tipo"]=="activeenglish")	echo "<img class='imgside' src='img/curso16.jpg' alt='' /><br /><img class='imgside' src='img/curso18.jpg' alt='' />";
		}
		?></td>
    </tr>
    </table>    
</div>
<?php include("2piecera.php");?>