<? include ("cabecera2.php") ?>
	
	<h1>Duchamp, Man Ray, Picabia </h1>
<p>Exposici&oacute;n organizada por la Tate Modern de Londres.</p>
<p>Esta exposici&oacute;n, organizada por el &quot;Museu Nacional d'Art de Catalunya&quot; en colaboraci&oacute;n con la &quot;Tate Modern&quot; de Londres, est&aacute; dedicada a tres artistas clave del siglo XX: Marcel Duchamp (1887-1968), Man Ray (1890-1976) y Francis Picabia (1879-1953), tres personalidades provocadoras que revolucionaron los c&aacute;nones art&iacute;sticos establecidos, marcando un antes y un despu&eacute;s en el mundo del arte.</p>
<p>La muestra aplega m&aacute;s de 300 obras: pinturas, fotograf&iacute;as, dibujos, pel&iacute;culas y objetos, entre los cuales destacan los famosos ready mades de Duchamp. </p>
<p>Marcel Duchamp y Francis Picabia nacieron en Francia y se hicieron amigos en Par&iacute;s. Fu&eacute; en Nueva York, ciudad de exilio de los dos artistas franceses, donde, el 1915, junto con el americano Man Ray, impulsaron el movimiento y forjaron una amistad a tres bandas que dur&oacute; hasta el final de sus vidas. </p>
<p>A trav&eacute;s de sus obras vemos la fascinaci&oacute;n comuna por las m&aacute;quinas, el movimiento, el erotismo o la experimentaci&oacute;n con la luz y las transparencias. </p>
<p>Fecha: 26 de Junio de 2008 - 21 de Septiembre de 2008 <br />
  Precio: 5,50 &euro; </p>
<p>FUENTE: Museu Nacional d'Art de Catalunya<br />
  Fecha de publicaci&oacute;n: 04-07-2008 </p>
<? include ("piecera2.php") ?>