<? include ("cabecera2.php") ?>
	
	<h1>INDICE DE NOTICIAS</h1>
	<h3>
    <a href="http://www.lallossarestaurant.com/noticias/index.php" style="color:#666666;">NOTICIAS 2009</a> | 
    <a href="http://www.lallossarestaurant.com/noticias/index.php?ano=2008" style="color:#666666;">NOTICIAS 2008</a> | 
    <a href="http://www.lallossarestaurant.com/noticias/index.php?ano=2007"  style="color:#666666;">NOTICIAS 2007</a></h3>
    <?php switch($_GET["ano"])
	{
		default: ?>
        <p><b>Marzo 2009</b></p>
        <p><a href="musica-clasica-humor-romea.php" target="_top">M&uacute;sica cl&aacute;sica y humor, en el Romea (Barcelona)</a></p>
        
        <p><b>Febrero 2009</b></p>
        <p><a href="cenicienta-en-liceu-barcelona.php" target="_top">La Cenicienta en el Liceu (Barcelona)</a></p>
        
        <p><b>Enero 2009</b></p>
        <p><a href="alabama-gospel-choir.php" target="_top">Alabama Gospel Choir </a></p>
        <p><a href="poemas-visuales-espai-brossa.php" target="_top">Poemas visuales en el Espai Brossa</a></p>
        <? break;
		
        case "2008": ?>
        <p><b>Diciembre 2008</b></p>
        <p><a href="veinte-anos-revolve-razzmatazz.php" target="_top">Veinte a&ntilde;os de Rev&oacute;lver, en Razzmatazz</a></p>
        <p><a href="canto-vida-ismael-serrano.php" target="_top">El canto a la vida de Ismael Serrano</a></p>
        
        <p><b>Noviembre 2008</b></p>
        <p><a href="eugenio-lucas-mnac.php" target="_top">Eugenio Lucas, en el MNAC</a></p>
        <p><a href="festiva-euskal-herria.php" target="_top">Festival Euskal Herria Sona</a></p>
    
        <p><b>Octubre 2008</b></p>
        <p><a href="festival-lem.php" target="_top">Festival LEM</a></p>
            
        <p><b>Septiembre 2008</b></p>
        <p><a href="exposicion-pekin2008.php" target="_top">El Dal&iacute; m&aacute;s &iacute;ntimo en ojos de Descharnes</a></p>
        
        <p><b>Agosto 2008</b></p>
        <p><a href="dali-mas-intimo-en-ojos-de-descharnes.php" target="_top">El Dal&iacute; m&aacute;s &iacute;ntimo en ojos de Descharnes</a></p>
        <p><a href="duchamp-man-ray-picabia.php" target="_top">Duchamp, Man Ray, Picabia</a></p>
        
        <p><b>Julio 2008</b></p>
        <p><a href="jazz-se-apodera-parc-ciutadella.php" target="_top">El jazz se apodera del &quot;Parc de la Ciutadella&quot;</a></p>
        <p><a href="maria-pages-actua-en-el-liceu.php" target="_top">Maria Pag&eacute;s act&uacute;a por primera vez en el Liceu</a></p>
        
        <p><b>Junio 2008</b></p>
        <p><a href="lliberi-en-poliorama.php" target="_top">El llibert&iacute;, en el Poliorama</a></p>
        <p><a href="artes-marciales-clave-humor-en-teatro-victoria.php" target="_top">Artes marciales en clave de humor en el Teatro Victoria</a></p>
        
        <p><b>Mayo 2008</b></p>
        <p><a href="opera_flamenco.php" target="_top">�pera y flamenco</a></p>
        <p><a href="historia_palau_de_la_musica.php" target="_top">La historia del Palau de la M�sica</a></p>
        
        <p><b>Abril 2008</b></p>
        <p><a href="sergio-dalma-en-auditori.php" target="_top">Sergio Dalma, en L'Auditori</a></p>
        <p><a href="festival60x70.php" target="_top">Festival 60x70</a></p>
        <p><a href="un-enfermo-imaginario-llega-teatro-condal.php" target="_top">Un "Enfermo Imaginario" llega al Teatro Condal</a></p>
        
        <p><b>Marzo 2008</b></p>
        <p><a href="19festival-guitarra.php" target="_top">19 FESTIVAL DE GUITARRA; LA EDICI�N M�S COMPLETA DE SU HISTORIA </a></p>
        <p><a href="m-clan-en-bikini.php" target="_top">M-clan, en Bikini</a></p>
        <p><a href="magia-bollywood-llega-tivoli.php" target="_top">La magia de Bollywood llega al T�voli</a></p>     
        
        <p><b>Febrero 2008</b></p>
        <p><a href="vuelve-la-cubana.php" target="_top">Vuelve La Cubana</a></p>
        <p><a href="shaolin-wudang-en-tivoli.php" target="_top">SHAOLIN &amp; WUDANG KUNG FU en el T�voli</a></p>
                
        <p><b>Enero 2008</b></p>
        <p><a href="africa-rubianes-club-capitol.php" target="_top">El �frica de Rubianes, en el Club Capitol</a></p>
        <p><a href="rutas-del-mar-llega-museo-maritimo.php" target="_top">Las "rutas del mar" llega al Museo Mar�timo</a></p>
		<? break;
		
		case "2007": ?>
        <p><b>Diciembre 2007</b></p>
    	<p><a href="mamma-mia-btm.php" target="_top">'Mamma mia!', al BTM</a></p>
		<p><a href="cabaret-musical-teatre-apolo.php" target="_top">'Cabaret, el musical', al Teatre Apolo</a></p>	
		<p><b>Noviembre 2007</b></p>
		<p><a href="voces_femeninas_festival_mileni_barcelona.php" target="_top">Las voces femeninas dar�n calidez al Festival del Mil.lenni de Barcelona</a></p>	
    	<p><b>Octubre 2007</b></p>
		<p><a href="visites_nocturnes_dramatitzades_museu_egipcio.php" target="_top">Visites nocturnes dramatitzades al Museu Egipci</a></p>	
		<p><b>Septiembre 2007</b></p>
		<p><a href="stalin_josepmariaflotats.htm" target="_top">El Stalin de Josep Maria Flotats llega al Teatre T�voli</a></p>
		<p><a href="dospajaros_deun_tiro.php" target="_top">Dos p�jaros de un tiro</a></p>	
	    <p><b>Agosto 2007</b></p>
		<p><a href="RafaelAmargocelebralosdiezanosdesucompaniaenelLiceu.htm">Rafael Amargo celebra los diez a&ntilde;os de su compa&ntilde;&iacute;a en el Liceu</a></p>
		<p><a href="ElMediterraneovistoporlosfotografos.htm">El Mediterr&aacute;neo visto por los fot&oacute;grafos, en el Palau Robert </a></p>
	    <p><b>Julio 2007</b></p>
		<p><a href="TAPEPLAS-BOOMBACHmuycercadenuestrosrestaurantes.htm">TAPEPLAS - BOOMBACH muy cerca de nuestros restaurantes!!</a></p>
		<p><a href="NochesdeMusicaenlaFundacionMiro.htm">Noches de M&uacute;sica en la Fundaci&oacute;n Mir&oacute; </a></p>
		<? break;
	}?>

<? include ("piecera2.php") ?>