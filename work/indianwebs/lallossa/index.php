<? include ("cabecera.php") ?>
		
    <? if($_GET["lang"]=="cat") { ?>
	<p><b>Restaurant La Llossa</b> &eacute;s un restaurant agradable i absolutament familiar del &ldquo;savoir faire &ldquo;del qual&rdquo; d&oacute;na fe una fiabilitat guanyada a pols durant m&eacute;s de dotze anys. </p>
	<p>Gens m&eacute;s arribar, pica-pica per a matar el gusanillo i despr&eacute;s&hellip; a gaudir de menjars i sopars casolans amb amanides, carpaccios, excel&middot;lents carns a la brasa i plats a for&ccedil;a de peix com el seu Bacall&agrave; a la crema d'escalivada. Interessants s&oacute;n els seus men&uacute;s per a grups i els pica-piques nocturns per a dos.</p>
    <? } else { ?>
	<p><b>Restaurant La Llossa</b> es un restaurante agradable y absolutamente familiar de cuyo  &ldquo;savoir faire&rdquo; da fe una fiabilidad ganada a pulso durante m&aacute;s de doce a&ntilde;os.</p> 
	<p>Nada m&aacute;s llegar, pica-pica para matar el gusanillo y despu&eacute;s&hellip; a disfrutar de  comidas y cenas caseras con ensaladas, carpaccios, excelentes carnes a la brasa  y platos a base de pescado como su Bacalao a la crema de escalivada. Interesantes son sus men&uacute;s para grupos y los pica-picas nocturnos para dos.</p>
    <? } ?>    
        
<? include ("piecera.php") ?> 