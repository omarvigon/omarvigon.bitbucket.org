<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta name="verify-v1" content="BPNeMbEA9zcJ2IGtIiJ0CgXFbKk0RBx9TQsWjXzaAUU=" />
	<meta name="title" content="LA LLOSA RESTAURANT BARCELONA LA LLOSSA RESTAURANTE BARCELONA" />
    <meta name="description" content="RESTAURANT LA LLOSSA BARCELONA - LLOSA BARCELONA RESTAURANTE Menus especiales para grupos - Roger de Ll�ria, 24  - 08010 Barcelona - Tel.934.125.983 info@lallossarestaurant.com - Menus de grupo, Restaurante La Llossa es un restaurante agradable y absolutamente familiar de cuyo 'savoir faire' da fe una fiabilidad ganada a pulso durante m�s de doce a�os">
	<meta name="keywords" content="LA LLOSSA, LA LLOSA, RESTAURANT LLOSA, LLOSSA RESTAURANT, LLOSA RESTAURANTE BARCELONA, LLOSSA BARCELONA RESTAURANTE, LLOSA BARCELONA, LLOSSA BARCELONA, menus especiales para grupos, menus grupo, menu de grupo.">
    <meta name="lang" content="es,sp,spanish,espa�ol,espanol,castellano,cat,catalan,catala">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.9)">
	<title>MENUS DE GRUPO BARCELONA, ESPECIAL MENUS PARA GRUPOS Y EMPRESAS LA LLOSA RESTAURANT LA LLOSSA BARCELONA</title>
    <meta name="revisit-after" content="7 Days" />
	<meta name="revisit" content="7 days" />
	<meta name="robot" content="Index,Follow" />
	<meta name="robots" content="All" />
	<meta name="distribution" content="Global" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name="ratinh" content="general" />
	<meta name="language" content="ES" />
	<meta name="abstract" content="la llosa barcelona" />
	<meta name="subject" content="restaurant la llosa barcelona" />
	<meta name="author" content="la llosa barcelona restaurant" />
	<meta name="copyright" content="llosa restaurant barcelona" />
	<link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="shortcut icon" href="favicon.ico">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>
<body>

<table cellpadding="0" cellspacing="0" align="center">
<tr>
	<td colspan="3">
		<div id="idiomas">
			<a title="La llosa Barcelona" href="<?php $_SERVER['PHP_SELF'] ?>?lang=esp"><img title="Restaurante La Llosa" src="img/icon_espana.gif" alt="La Llossa Restaurante" ></a>
			<a title="La llosa Barcelona" href="<?php $_SERVER['PHP_SELF'] ?>?lang=cat"><img title="Restaurante La Llosa" src="img/icon_cataluna.gif" alt="La Llossa Restaurant"></a>
		</div>
		<!--<a href="javascript:especial()">-->
        <img title="Restaurante Llosa Barcelona" src="img/cabecera_1.jpg" alt="Restaurant La Lossa Barcelona Menus para grupos" border="0">
        <!--</a>-->
  </td>
</tr>
<tr>
	<td colspan="3">
    <?php 
	$temp = $_SERVER['PHP_SELF'];
	$temp= substr ($temp,1,strlen($temp)-5);
	?>
	<img title="Restaurante Llosa Barcelona" src="img/lallossa_<?php echo $temp;?>.jpg" id="imagen" alt="Restaurant La Lossa (Barcelona) - Menus especiales para grupos">
	</td>
</tr>
<tr>
	<td id="co1">	
        <? if($_GET["lang"]=="cat") { ?>
        <a title="LA LLOSA RESTAURANT" href="index.php?lang=cat">presentaci&oacute;</a>
		<a title="FOTOS LA LLOSA" href="galeria.php?lang=cat">galeria de fotos</a>
		<a title="CARTA DE LA LLOSSA" href="carta-lallosa.php?lang=cat">la nostra carta</a>
		<a title="MENUS GRUP LLOSA" href="menus-de-grupo.php?lang=cat">menus de grup</a>
		<a title="LOCALITZACIO LLOSA" href="localizacion.php?lang=cat">localitzaci&oacute;</a>
        <a title="NOTICIES LLOSA" href="http://www.lallossarestaurant.com/noticias/">not&iacute;cies</a>
		<a title="RESERVES LA LLOSA" href="reservas-lallosa.php?lang=cat">reserves</a>
		<a title="CONTACTE LA LLOSA" href="contacto-lallosa.php?lang=cat">contacte</a>
        
		<? } else { ?>
        <a title="RESTAURANTE LA LLOSSA" href="index.php">presentaci&oacute;n</a>
		<a title="FOTOS LA LLOSSA" href="galeria.php">galeria de fotos</a>
		<a title="CARTA LA LLOSA" href="carta-lallosa.php">nuestra carta</a>
		<a title="MENUS DE GRUPO LA LLOSSA" href="menus-de-grupo.php">menus de grupo</a>
		<a title="LA LLOSA LOCALIZACION" href="localizacion.php">localizaci&oacute;n</a>
        <a title="NOTICIAS LLOSA" href="http://www.lallossarestaurant.com/noticias/">noticias</a>
		<a title="LA LLOSA RESERVAS" href="reservas-lallosa.php">reservas</a>
		<a title="CONTACTO LA LLOSSA" href="contacto-lallosa.php">contacto</a>
        <? } ?>
        
        <h1>NOTICIAS MARZO 2009</h1>        
        <p><a href="noticias/musica-clasica-humor-romea.php" target="_top">M&uacute;sica cl&aacute;sica y humor, en el Romea (Barcelona)</a></p>
	</td>
	<td id="co2">