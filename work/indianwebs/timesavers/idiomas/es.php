<?
define("FECHA_1","Fecha");
define("FECHA_DIA","dia");
define("FECHA_MES","mes");
define("FECHA_ANO","a&ntilde;o");
define("MES_1","Enero");
define("MES_2","Febrero");
define("MES_3","Marzo");
define("MES_4","Abril");
define("MES_5","Mayo");
define("MES_6","Junio");
define("MES_7","Julio");
define("MES_8","Agosto");
define("MES_9","Septiembre");
define("MES_10","Octubre");
define("MES_11","Noviembre");
define("MES_12","Diciembre");

define("BT1","Sobre<br />TimeSavers");
define("BT2","Servicios");
define("BT2A","Ahorros de Tiempo");
define("BT2B","Cuidado Personal");
define("BT2C","Mejora del Hogar");
define("BT2D","Cuidado del Hogar");
define("BT3A","Credits");
define("BT3B","Precios");
define("BT3C","Gesti&oacute;n");
define("BT4","Prensa");
define("BT4A","Noticias TimeSavers");
define("BT4B","TimeSavers en los medios");
define("BT5A","Contacto");
define("BT5B","Contacto Proveedores");
define("BT6","Aviso Legal &amp; Condiciones de Uso");
define("BT7","Olvid&eacute; mi password");
define("BT8A","&iquest;Qu&eacute; hace TimeSavers?");
define("BT8B","&iquest;Qui&eacute;nes son TimeSavers?");
define("BT9","Reg&iacute;strese");
define("BT10","Entrar");
define("BT11","CAMBIAR IMAGEN");
define("XHORA","Hora");
define("XMAPA","Ver mapa");
define("XCOMPRAR","Comprar");
define("XCANCEL","Rechazar");

define("REG_INICIO","<p>&iexcl;Bienvenido a <em class='blanco'>TimeSavers - Quality Personal Concierge</em>! Est&aacute; a punto de entrar en una nueva etapa de su vida, en la que esperamos poder reducir su nivel de estr&eacute;s y aumentar su tiempo libre.</p>
		<p>La cuota de alta es de 50,- &euro;, reembolsable con la compra de TimeSavers Credits.</p>
		<p>A continuaci&oacute;n le pedimos  unos datos personales.</p>");
define("REG_ELEGIR","(Elegir)");
define("REG_TRATA","Tratamiento");
define("REG_TRATA_1","Sr.");
define("REG_TRATA_2","Sra.");
define("REG_TRATA_3","Dr.");
define("REG_NOMBRE","Nombre");
define("REG_APELLIDOS","Apellidos");
define("REG_SEXO","Sexo");
define("REG_SEXO_1","Hombre");
define("REG_SEXO_0","Mujer");
define("REG_NACIMIENTO","Fecha de Nacimiento");
define("REG_NACIONALIDAD","Nacionalidad");
define("REG_DOCUMENTO","Documento de identidad");
define("REG_DOCNUM","N&ordm; de Documento");
define("REG_DIRECCION","Direcci&oacute;n");
define("REG_POBLACION","Poblaci&oacute;n");
define("REG_CP","C.P.");
define("REG_TEL","Tel&eacute;fono");
define("REG_MOVIL","M&oacute;vil");
define("REG_IDIOMA","Idioma preferido de comunicaci&oacute;n");
define("REG_MODO","Modo preferido de comunicaci&oacute;n");
define("REG_CONOCIO","&iquest;C&oacute;mo nos ha conocido ?");
define("REG_CONCIO_1","Proveedor");
define("REG_CONCIO_2","Amigo");
define("REG_CONCIO_3","Anuncio");
define("REG_CONCIO_4","Folleto");
define("REG_CONCIO_5","Televisi&oacute;n");
define("REG_CONCIO_6","Otro");
define("REG_COMENTA","Comentarios");
define("REG_FOTO","Su foto : (Formato JPG)");
define("REG_CONTRA","Contrase&ntilde;a");
define("REG_CONTRA2","Cambiar Contrase&ntilde;a");
define("REG_CONTRA3","Recibira su contrase&ntilde;a por e-mail registrado");
define("REG_MODIF","Modificar");
define("REG_BAJADAR","QUIERO DARME DE BAJA");
define("REG_BAJA","Se puede dar de baja en cualquier momento envi&aacute;ndonos un email a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, llam&aacute;ndonos al +34 93 304 0032 o a trav&eacute;s de un simple click en su cuenta. Productos comprados y no utilizados se reembolsar&aacute;n hasta un plazo de 30 d&iacute;as a partir de la fecha de compra.");
define("REG_REGISTRAR","REGISTRAR");
define("REG_DESCRIP","Descripci&oacute;n");
define("REG_LUGAR","Lugar");
define("REG_DIFERENTE","si es diferente del registrado");
define("REG_CARRITO","A&Ntilde;ADIR AL CARRITO");
define("REG_FECHASER","Fecha deseado de servicio");
define("REG_CODIGO","Si dispone de un  c&oacute;digo de promoci&oacute;n, introd&uacute;zcalo aqu&iacute;. El descuento se aplicar&aacute; a su primera  compra de TimeSavers Credits.");
define("REG_FIN","&iexcl;Bienvenido a <em>TimeSavers - Quality Personal Concierge</em>!<br /><br />Se le ha enviado la contrase&ntilde;a a la cuenta");

define("OLVIDO","<h3>Olvid&oacute; sus datos</h3>
        <p>En caso de que no recuerde su e-mail de registro/usuario, por favor, p&oacute;ngase en contacto con nosotros por tel&eacute;fono al <b>+34 93 304 00 32</b> o por e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");

define("USER_MI","Mi Timesavers");
define("USER_PEDIDO","Solicitar servicio");
define("USER_PEDIR","REALIZAR PEDIDO");
define("USER_SALDO_1","TimeSavers<br />Credits<br />utilizados");
define("USER_SALDO_2","Saldo<br />TimeSavers<br />Credits");
define("USER_ACCION","Acciones");
define("USER_HISTORIAL_1","Presupuestos");
define("USER_HISTORIAL_2","Facturas");
define("USER_IMPORTE","Importe");
define("USER_DETALLES","Detalle");
define("USER_DATOS","Datos Personales");
define("USER_NOTICIAS","Noticias");
define("USER_RECOMENDA","Recomendaciones");
define("USER_RECOMENDA_2","Tipo de recomendaci&oacute;n");
define("USER_RECOMENDA_10","Servicio deseado");
define("USER_RECOMENDA_20","Servicio ofrecido");
define("USER_RECOMENDA_30","Incidencia solicitud");
define("USER_RECOMENDA_40","Forma de trabajo");
define("USER_RECOMENDA_3","RECOMENDAR");
define("USER_RECOMENDACION","<p>En <em>TimeSavers - Quality Personal Concierge</em> valoramos su opini&oacute;n. La m&aacute;xima satisfacci&oacute;n de nuestros miembros es nuestro objetivo primordial. Por supuesto, la mejor manera de conseguirlo es de escucharle y de pedirle su colaboraci&oacute;n.</p>
	<p>Con esta finalidad hemos puesto a su disposici&oacute;n este formulario de sugerencias. Conf&iacute;e en que le contactaremos con absoluta inmediatez.</p>
	<p>Desde luego, nos puede tambi&eacute;n contactar por tel&eacute;fono al n&uacute;mero <b>+34 93 304 00 32</b> o enviarnos un e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> para hablar directamente con uno de nuestros asesores.</p>");

define("IND_VIDEO","<object width='220' height='220'><param name='allowfullscreen' value='true' /><param name='allowscriptaccess' value='always' /><param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=1839349&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' /><embed src='http://vimeo.com/moogaloop.swf?clip_id=1839349&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='220' height='220'></embed></object>
			<p><a href='http://vimeo.com/1839349?pg=embed&amp;sec=1839349' target='_blank'>Video de Bienvenida: TimeSavers - Quality Personal Concierge</a>.</p>");
define("IND0","Bienvenidos a TimeSavers - Quality Personal Concierge");
define("IND1","<p>Seg&uacute;n  la Acad&eacute;mie Fran&ccedil;aise, la palabra &ldquo;concierge&rdquo; proviene de la palabra &ldquo;cumcerges&rdquo;  del siglo XII, que significa &ldquo;guardi&aacute;n&rdquo;. Se define como una persona encargada de  cuidar un inmueble, edificio p&uacute;blico o palacio.</p>
		<p>Los &quot;concierge services&quot; modernos se  iniciaron, donde si no, en Nueva York, la ciudad que nunca duerme, en los a&ntilde;os  80. Respond&iacute;an a la necesidad de la gente de conciliaci&oacute;n de la vida personal y  laboral, adem&aacute;s de poder disfrutar de m&aacute;s tiempo libre. </p>		
		<p>Desde entonces el concepto ha ido conquistando el mundo y ahora, por fin, <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> proporciona este servicio en Barcelona. </p>");
define("IND2","&iexcl;Imagine una vida sin estr&eacute;s!");
define("IND3","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> ha nacido con vocaci&oacute;n de simplificarle la vida, consiguiendo de este modo que  disfrute de m&aacute;s tiempo de ocio, elevando as&iacute; su calidad de vida y la de su familia. </p>
		<p>En <em>TimeSavers &ndash; Quality Personal Concierge</em> somos proveedores de tiempo, para que lo dedique a lo que m&aacute;s le interesa.</p>
		<p>La vida cotidiana en la ciudad, las gestiones diarias, las citas, compras, tr&aacute;mites burocr&aacute;ticos lentos, y nunca suficiente tiempo para hacerlo todo.</p>
		<p>&iquest;Imagina una vida donde pueda disfrutar de su propio conserje o asistente personal, siempre a su disposici&oacute;n con una llamada o un simple clic? </p>
		<p>En <em>TimeSavers &ndash; Quality Personal Concierge</em> contamos con profesionales eficaces y discretos para hacer realidad este sue&ntilde;o del proprio asistente personal.</p>");
define("IND4","Consulte nuestro cat&aacute;logo de productos para visitantes haciendo click aqu&iacute;!");

define("LIN1","<p>Aqu&iacute;  encontrar&aacute;n links de inter&eacute;s general, p&aacute;ginas que nos gustan personalmente y/o  sitios que podr&iacute;an emocionarle. </p>
		<p style='font-size:10px;'>Timesavers &ndash; Quality Personal Concierge no se responsabiliza por la informaci&oacute;n contenida en  las p&aacute;ginas web de terceros a las que se pueda acceder a trav&eacute;s de dichos  enlaces.</p>");
define("LIN2","Secci&oacute;n en construcci&oacute;n...");

define("LEG1","<p>Esta  p&aacute;gina web es propiedad de <b>TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L. </b>en lo adelante, <b>TIMESAVERS,</b> el hecho de acceder a esta p&aacute;gina implica el  conocimiento y aceptaci&oacute;n de la pol&iacute;tica de privacidad. Esta pol&iacute;tica de  privacidad forma parte de las condiciones y t&eacute;rminos de usos.<br />
          En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio,  de Servicios de la Sociedad  de la Informaci&oacute;n y Comercio Electr&oacute;nico (LSSICE) se exponen los datos  identificativos.</p>");
define("LEG2","<p><b>Condiciones  de Uso y Pol&iacute;tica de Privacidad </b></p>
        <p>1. <u>Datos Identificativos de la Empresa</u></p>
        <p>Identidad y Direcci&oacute;n:</p>
        <p>Empresa: TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.<br />CIF: B64806862 - Nacionalidad: Espa&ntilde;ola <br />Domicilio: Ronda Universitat, 15, 1&ordm; 1&ordf;,  08007 - Barcelona (Espa&ntilde;a)<br />Tel&eacute;fono: +34 304 00 32 - Fax: +34 550 44 66<br />Correo electr&oacute;nico: <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em></p>
        <p><b>TIMESAVERS</b>, es titular del  sitio web <em>www.timesavers.es</em> y responsable de los  ficheros generados con los datos de car&aacute;cter personal suministrados por los  usuarios a trav&eacute;s de este sitio web. </p>
        <p>2. <u>Tratamiento de los datos de car&aacute;cter personal</u>.</p>
        <p>El usuario de la web autoriza a <b>TIMESAVERS</b>, el tratamiento automatizado  de los datos personales que suministre voluntariamente, a trav&eacute;s de formularios  y correo electr&oacute;nico, para: </p>
        <p>-Presupuestos relacionados con los  productos y servicios de <b>TIMESAVERS</b>.<br />-Venta, env&iacute;o y gesti&oacute;n con relaci&oacute;n a los productos y  servicios de <b>TIMESAVERS </b>comprados  por el usuario en la web.<b></b></p>
        <p>Asimismo, le  informamos que sus datos podr&aacute;n ser utilizados para enviarle informaci&oacute;n sobre  nuestros productos, ofertas y servicios en servicios personales, salvo que  usted manifieste lo contrario indic&aacute;ndolo expresamente en el formulario  correspondiente.</p>
		<p>Asimismo, queda usted  informado que parte de la informaci&oacute;n que usted nos facilite puede ser  comunicada a terceras empresas y colaboradores que TIMESAVERS pueda  subcontratar con la &uacute;nica y exclusiva finalidad de poder llevar a cabo los  servicios por usted contratados. Puede manifestarse en el formulario  correspondiente, si no quiere que se conserven sus datos m&aacute;s tiempo del  legalmente exigido.</p>
		<p>Del mismo modo, si  TIMESAVERS lo considera oportuno, y salvo que usted se manifieste en contrario,  indic&aacute;ndolo expresamente en el formulario correspondiente, conservar&aacute; sus datos  m&aacute;s del tiempo legalmente exigido, aunque no contin&uacute;e existiendo una relaci&oacute;n  comercial entre usted y TIMESAVERS, para poder llevar un control de los  servicios llevados a cabo y, si usted as&iacute; lo desea, poder continuar envi&aacute;ndole  informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS.</p>
        
        <p>El fichero creado est&aacute; ubicado en la Ronda  Universitat, 15, 1&ordm; 1&ordf;, 08007 - Barcelona (Espa&ntilde;a) bajo la supervisi&oacute;n y  control de <b>TIMESAVERS</b> quien asume la  responsabilidad en la adopci&oacute;n de medidas de seguridad de &iacute;ndole t&eacute;cnica y  organizativa de Nivel B&aacute;sico para proteger la confidencialidad e integridad de  la informaci&oacute;n, de acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999  de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal y su normativa  de desarrollo.</p>
        <p>El usuario responder&aacute;, en cualquier caso,  de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reserv&aacute;ndose <b>TIMESAVERS</b> el derecho a excluir de  los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados,  inexactos, sin perjuicio de las dem&aacute;s acciones que procedan en Derecho. </p>
        <p>Cualquier usuario puede ejercitar sus  derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos de  car&aacute;cter personal suministrados a trav&eacute;s de la web <em>www.timesavers.es</em> o por correo  electr&oacute;nico, mediante comunicaci&oacute;n escrita dirigida a <b>TIMESAVERS </b>ubicada en la Ronda Universitat, 15, 1&ordm; 1&ordf;, 08007 -  Barcelona (Espa&ntilde;a) o al correo electr&oacute;nico <em>info@timesavers.es</em>. </p>
        <p>3. <u>Propiedad Intelectual e Industrial:</u> El nombre de dominio <em>www.timesavers.es</em> de esta p&aacute;gina web  es propiedad de <b>TIMESAVERS</b>. El sitio  web <em>www.timesavers.es</em> en su totalidad,  incluyendo su dise&ntilde;o, estructura, distribuci&oacute;n, textos, contenidos, logotipos,  botones, im&aacute;genes, dibujos, c&oacute;digo fuente, bases de datos, as&iacute; como todos los  derechos de propiedad intelectual e industrial y cualquier otro signo  distintivo, pertenecen a <b>TIMESAVERS</b>,<b> </b>o, en su caso, a las personas o empresas que  figuran como autores o titulares de los derechos y que han autorizado a su uso a <b>TIMESAVERS</b>.<b> </b></p>
        <p>Queda prohibida la reproducci&oacute;n o  explotaci&oacute;n total o parcial, por cualquier medio, de los contenidos del portal  de <b>TIMESAVERS</b> para usos diferentes  de la leg&iacute;tima informaci&oacute;n o contrataci&oacute;n por los usuarios de los servicios  ofrecidos. En tal sentido, usted no puede copiar, reproducir, recompilar,  descompilar, desmontar, distribuir, publicar, mostrar, ejecutar, modificar,  volcar, realizar trabajos derivados, transmitir o explotar partes del servicio,  exceptuando el volcado del material del servicio y/o hacer copia para su uso  personal no lucrativo, siempre y cuando usted reconozca todos los derechos de  autor y Propiedad Industrial. La modificaci&oacute;n del contenido de los servicios  representa una violaci&oacute;n a los leg&iacute;timos derechos de <b>TIMESAVERS</b>.</p>
        <p>4. <u>Responsabilidades</u>. <b>TIMESAVERS </b>no se responsabiliza de cualquier da&ntilde;o o  perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor,  como: error en las l&iacute;neas de comunicaciones, defectos en el Hardware y Software  de los usuarios, fallos en la red de Internet (de conexi&oacute;n, en las p&aacute;ginas  enlazadas).</p>
        <p>No se garantiza que el sitio web vaya a  funcionar constante, fiable y correctamente, sin retrasos o interrupciones.</p>
        <p><b>TIMESAVERS</b> se reserva el  derecho a modificar la presente pol&iacute;tica de privacidad con objeto de adaptarla  a las posibles novedades legislativas, as&iacute; como a las que pudieran derivarse de  los c&oacute;digos tipo existentes en la materia o por motivos estrat&eacute;gicos. <b>TIMESAVERS </b>declina cualquier  responsabilidad respecto a la informaci&oacute;n de esta web procedente de fuentes  ajenas a <b>TIMESAVERS.</b> </p>
        <p><b>TIMESAVERS</b> no se  responsabiliza del mal uso que se realice de los contenidos de su p&aacute;gina web,  siendo responsabilidad exclusiva de la persona que accede a ellos o los  utiliza. De igual manera, no es responsable de que menores de edad realicen  pedidos, rellenando el formulario con datos falsos, o haci&eacute;ndose pasar por un  miembro de su familia mayor de edad o cualquier tercero mayor de edad. Debiendo  de responder ante dicho hecho el mayor de edad bajo el cual se encuentra dicho  menor, teniendo <b>TIMESAVERS</b> siempre  la posibilidad de actuar por las v&iacute;as legales que considere oportunas con la  finalidad de sancionar dicho hecho. 
        En todo caso, <b>TIMESAVERS</b>, al momento de entrega del pedido, entregar&aacute; s&oacute;lo y  &uacute;nicamente a un mayor de edad, que deber&aacute; acreditar sus datos mediante  documento de identificaci&oacute;n legal. Tampoco asume responsabilidad alguna por la  informaci&oacute;n contenida en las p&aacute;ginas web de terceros a las que se pueda acceder  a trav&eacute;s de enlaces desde la p&aacute;gina <em>www.timesavers.es</em>. La presencia de  estos enlaces tiene una finalidad informativa, y no constituyen en ning&uacute;n caso  una invitaci&oacute;n a la contrataci&oacute;n de productos o servicios que se puedan ofrecer  en la p&aacute;gina web de destino. <b>TIMESAVERS</b> no responde ni se hace cargo de ning&uacute;n tipo de responsabilidad por los da&ntilde;os y  perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y  continuidad de los sitios enlazados. 
		De igual manera, <b>TIMESAVERS</b> por este medio comunica a quien le pueda interesar que  en caso de que un tercero este interesado en colocar como enlace de su p&aacute;gina  web la direcci&oacute;n de <b>TIMESAVERS</b>, &eacute;ste  deber&aacute; de comunicarse v&iacute;a correo electr&oacute;nico <em>info@timesavers.es</em> solicitando la autorizaci&oacute;n para tal  efecto. </p>
        <p>5. <u>Navegaci&oacute;n con cookies</u>. El web <em>www.timesavers.es</em> utiliza cookies,  peque&ntilde;os ficheros de datos que se generan en el ordenador del usuario y que nos  permiten conocer su frecuencia de visitas, los contenidos m&aacute;s seleccionados y  los elementos de seguridad que pueden intervenir en el control de acceso a &aacute;reas  restringidas, as&iacute; como la visualizaci&oacute;n de publicidad en funci&oacute;n de criterios  predefinidos por <b>TIMESAVERS </b>y que se  activan por cookies servidas por dicha entidad o por terceros que prestan estos  servicios por cuenta de <b>TIMESAVERS</b>.</p>
        <p>El usuario tiene la opci&oacute;n de impedir la  generaci&oacute;n de cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su  programa navegador.</p>
        <p>El usuario autoriza a <b>TIMESAVERS</b> a obtener y tratar la informaci&oacute;n que se genere como  consecuencia de la utilizaci&oacute;n del Web <em>www.timesavers.es</em>, con la &uacute;nica  finalidad de ofrecerle una navegaci&oacute;n m&aacute;s personalizada.</p>
        <p>6. <u>Seguridad.</u> Este sitio web en cuanto a la confidencialidad y tratamiento de datos de  car&aacute;cter personal, por un lado, responde a las medidas de seguridad de nivel  medio establecidas en virtud de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, sobre  Protecci&oacute;n de Datos de Car&aacute;cter Personal y su Reglamento de Medidas de  Seguridad vigente, y por otro lado responde,  en cuanto a las medidas t&eacute;cnicas a la instalaci&oacute;n de &ldquo;Antivirus y  filtros antispam&rdquo; y el Secure Socket Layers &ldquo;SSL&rdquo; como plataforma segura. Todo  lo anterior a sabiendas que en la red de redes la seguridad absoluta no existe.</p>

		<p>7. <u>Confirmaci&oacute;n  de Recepci&oacute;n Pedido.</u> Una vez recibido el pedido, TimeSavers enviar&aacute; por  correo electr&oacute;nico la confirmaci&oacute;n de recepci&oacute;n del pedido; posteriormente, y  en un plazo m&aacute;ximo de 24 horas, se enviar&aacute;, mediante correo electr&oacute;nico, la  propuesta, el presupuesto y el plazo de entrega del/los servicio/s  solicitado/s. Los pedidos efectuados los viernes, o v&iacute;spera de festivo, se  tramitar&aacute;n el siguiente d&iacute;a laborable (los d&iacute;as laborables no incluyen fines de  semana ni d&iacute;as festivos) al menos que indique que necesita una gesti&oacute;n  inmediata. Se aplicar&aacute; un incremento de 25% sobre las tarifas normales para  gestiones fuera del horario comercial de lunes a viernes de 10h a 20h.</p>
  		<p>8. <u>Formas  y condiciones de pago (impuestos).</u> En cuanto al pago, el usuario debe  elegir entre: tarjeta de cr&eacute;dito, transferencia bancaria y/o Paypal. En el caso  de compras y/o gestiones con un importe superior a 100&euro;, se deber&aacute; pagar un 50%  del importe por adelantado, con la observaci&oacute;n de que en ning&uacute;n caso nos  llegar&aacute; informaci&oacute;n sobre el numero de la tarjeta de cr&eacute;dito o cuenta, estos  datos pasan directamente a la entidad financiera, que se encargar&aacute; de  certificar la autenticidad de la tarjeta y realizar el cobro. Todo a sabiendas  que en la red de redes la seguridad absoluta no existe.</p>
  		<p>Todos los  precios reflejados en el portal <strong>www.timesavers.es</strong> incluyen  el IVA (16%). Los precios est&aacute;n sujetos a cambios sin previo aviso.</p>

        <p>9. <u>Plazos, Formas  y Gastos de env&iacute;o.</u><br />
          A partir del momento en el que sea efectiva  la recepci&oacute;n del pedido y se confirme el pago del mismo por parte del cliente,  se proceder&aacute; a la realizaci&oacute;n efectiva de la prestaci&oacute;n de los servicios  contratados en un plazo marcado en la confirmaci&oacute;n del pedido.</p>
        <p>En caso de no disponibilidad del producto o  servicio que el cliente haya solicitado a trav&eacute;s de la web <em>www.timesavers.es</em>, se proceder&aacute; a la  comunicaci&oacute;n del incidente al usuario en un m&aacute;ximo de 3 (tres) d&iacute;as, utilizando  como canal de comunicaci&oacute;n la direcci&oacute;n de correo electr&oacute;nico, o n&uacute;mero de  tel&eacute;fono de contacto, que el usuario nos haya facilitado a trav&eacute;s del  formulario de compra. En esta comunicaci&oacute;n se le informar&aacute; al cliente de la fecha  en la que el producto o servicio comprado estar&aacute; disponible o de la  imposibilidad de disponer del mismo. En caso de que el cliente no est&eacute; de  acuerdo con los nuevos plazos o sea imposible disponer del producto o servicio  contratados, se le dar&aacute; otra alternativa a la compra o se proceder&aacute; a la  devoluci&oacute;n del importe total de la misma a elecci&oacute;n del cliente.</p>
        <p>Antes de finalizar la compra y confirmar el  pedido, se podr&aacute; conocer la cuant&iacute;a de este concepto y, en caso de desearlo, el  cliente podr&aacute; desestimar el pedido.</p>
        <p>10. <u>Pago mediante tarjeta de cr&eacute;dito</u>.  (Repudio) </p>
        <p>Cuando el importe de una compra hubiese  sido cargado utilizando el n&uacute;mero de una tarjeta de cr&eacute;dito, sin que &eacute;sta  hubiese sido presentada directamente o identificada electr&oacute;nicamente, su titular  podr&aacute; exigir la inmediata anulaci&oacute;n del cargo. En tal caso, las  correspondientes anotaciones de adeudo y reabono en las cuentas del proveedor y  del titular se efectuar&aacute;n a la mayor brevedad.</p>
        <p>Sin embargo, si la compra hubiese sido efectivamente realizada por el titular  de la tarjeta y, por lo tanto, hubiese exigido indebidamente la anulaci&oacute;n del  correspondiente cargo, aqu&eacute;l quedar&aacute; obligado frente al vendedor al  resarcimiento de los da&ntilde;os y perjuicios ocasionados como consecuencia de dicha  anulaci&oacute;n. </p>
        <p>11. <u>Desistimiento.</u></p>
        <p>El comprador podr&aacute; desistir libremente del  contrato dentro del plazo de siete d&iacute;as contados desde la fecha de recepci&oacute;n  del producto. <br />El ejercicio del derecho o desistimiento no  estar&aacute; sujeto a formalidad alguna, bastando que se acredite, en cualquier forma  admitida en Derecho.<br />
        El derecho de desistimiento del comprador  no puede implicar la imposici&oacute;n de penalidad alguna, si bien, el comprador  deber&aacute; satisfacer los gastos directos de devoluci&oacute;n y en su caso, indemnizar  los desperfectos del objeto de la compra. </p>
        <p>Lo dispuesto anteriormente no ser&aacute; de  aplicaci&oacute;n a las ventas de objetos que puedan ser reproducidos o copiados con  car&aacute;cter inmediato, que se destinen a la higiene corporal o que, en raz&oacute;n de su  naturaleza, no puedan ser devueltos. </p>
        <p>12. <u>Devoluciones.</u></p>
        <p>En cuanto a las gestiones compradas a  TimeSavers, tienen una validez de 1 a&ntilde;o para ser utilizadas, aunque existe la  posibilidad de devoluci&oacute;n del importe de las gestiones compradas y NO  UTILIZADAS tras 30 dias naturales de haber realizado la compra de dichas  gestiones.<br />
        As&iacute; mismo, se pone en conocimiento del usuario  que algunos de los servicios ofrecidos por TimeSavers, ser&aacute;n subcontratados a  terceras empresas proveedoras de dichos servicios, con lo cual el usuario tendr&aacute;  el derecho a la devoluci&oacute;n de algunos de los productos finales en un plazo  m&aacute;ximo de diez (10) d&iacute;as tras la recepci&oacute;n del mismo, en virtud de la Ley de  Ordenamiento del Comercio Minorista. Pudiendo el usuario, solicitar la  sustituci&oacute;n del producto, la deducci&oacute;n del importe equivalente en su pr&oacute;ximo  pedido o el reembolso de su dinero, previa confirmaci&oacute;n por parte de <b>TIMESAVERS </b>de que no se ha utilizado,  da&ntilde;ado y/o quebrantado el producto. En cuanto a los dem&aacute;s productos (alimentos,  artesan&iacute;a) por su fragilidad y especial conservaci&oacute;n no se podr&aacute;n devolver,  excepto cuando el error se halla presentado por parte del vendedor.</p>
        <p><u>Proceso de devoluci&oacute;n:</u> el proceso de  devoluci&oacute;n es el siguiente: </p>
        <p>12.1. Escribir a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> solicitando un n&uacute;mero  de autorizaci&oacute;n de devoluci&oacute;n (se le da un No. de devoluci&oacute;n que determin&eacute; la  empresa, para llevar control de dichas actuaciones); 
        12.2. Hay que enviarle  junto con el pedido un formulario de devoluci&oacute;n adjunto, para que expliquen  motivos de la devoluci&oacute;n, productos a devolver (el producto s&oacute;lo se podr&aacute;  devolver en caso de da&ntilde;os en el mismo, equivocaci&oacute;n o no conformidad y en el  caso de productos alimenticios, artesan&iacute;as s&oacute;lo se podr&aacute; devolver por no  conformidad del comprador por error del vendedor y caso de da&ntilde;os en el mismo); 
        12.3.  Adjunto con el env&iacute;o del producto en su embalaje original, debidamente  protegido para su transporte, anexe copia de la factura m&aacute;s el formulario de  devoluci&oacute;n; 12.4. El reenv&iacute;o del producto ser&aacute; a su s&oacute;lo coste y deber&aacute;  enviarlo dentro de un plazo no superior a diez (10) d&iacute;as desde la recepci&oacute;n del  producto. Para el caso de que el producto este da&ntilde;ado o no sea lo que ha  solicitado y dicho error sea imputable al vendedor, todos los gastos correr&aacute;n  porcuenta de &eacute;ste.</p>
        <p>13. <u>Legislaci&oacute;n  y jurisdicci&oacute;n aplicable</u><u>.</u> Con car&aacute;cter general, las relaciones con los usuarios,  derivadas de la prestaci&oacute;n de servicios contenidos en esta p&aacute;gina web, con  renuncia expresa a cualquier fuero que pudiera corresponderles, se someten a la  legislaci&oacute;n y jurisdicci&oacute;n espa&ntilde;ola, espec&iacute;ficamente a los tribunales de la  ciudad de Barcelona (Espa&ntilde;a). Los usuarios de esta p&aacute;gina web son conscientes  de todo lo que se ha expuesto y lo aceptan voluntariamente.</p>");

define("EMP1","&iexcl;Un equipo a su servicio!");
define("EMP2","<em>TimeSavers &ndash; Quality Personal Concierge</em> lo forman un equipo de profesionales, procedentes de distintos &aacute;mbitos laborales y formativos, que, gracias a su amplia experiencia, est&aacute;n sobradamente preparados para servirles de conserje o asistente personal. De esta manera, podr&aacute;n afrontar, incluso, las necesidades de sus miembros m&aacute;s exigentes.");
define("EMP3","Con doble nacionalidad Francesa y Alemana, es licenciado por Yale University. Ciudadano del mundo, ha vivido, entre otras, en Berl&iacute;n , New York y Capetown. En 2002 llega a Barcelona para estudiar un MBA en ESADE y decide instalarse en Barcelona. Christoph ha tenido un recorrido profesional diversificado, ha trabajado con artistas famosos en discogr&aacute;ficas internacionales, ha liderado equipos multinacionales y campa&ntilde;as de marketing para una cadena de joyer&iacute;a de renombre, as&iacute; como la coordinaci&oacute;n y proyecci&oacute;n de grandes superficies de bricolaje.");
define("EMP4","Barcelon&eacute;s, es diplomado en Figurinismo por CETE (Escuela Superior de Moda de Paris). Cosmopolita por vocaci&oacute;n, ha residido en Par&iacute;s, Madrid y Barcelona. Su carrera profesional le ha llevado a colaborar estrechamente en ferias de moda internacionales y con dise&ntilde;adores de alta costura. Posteriormente, se ha centrado en el campo de la log&iacute;stica y los procesos de post-producci&oacute;n industriales para importantes empresas l&iacute;deres de sector.");
define("EMP5","&iexcl;D&eacute;jenos trabajar y descanse");
define("EMP6","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> da soluciones a sus necesidades personales:  la organizaci&oacute;n de un viaje, el mantenimiento de su casa, la planificaci&oacute;n de su fiesta de cumplea&ntilde;os.</p>
<p>Permita que actuemos como su asistente o conserje personal, que nos encarguemos de llevar su coche a la revisi&oacute;n, que busquemos un regalo adecuado y lo entreguemos, o simplemente rel&aacute;jese, le encontraremos un buen masajista.</p>
<p>Oc&uacute;pese de tareas m&aacute;s productivas, prop&oacute;nganos que le recojamos la ropa de la tintorer&iacute;a, le encontremos un paseador para su perro, le solicitemos y optimicemos los presupuestos de las obras de reforma de su casa, y adem&aacute;s le presentaremos un interiorista si lo necesita.</p>
<p>Podr&aacute; marcharse de vacaciones tranquilo, su hogar estar&aacute; atendido, le recogeremos el correo, cuidaremos de sus plantas o atenderemos gestiones en su ausencia.</p>
<p>Para los servicios que no proporcionamos directamente, trabajamos con proveedores cualificados y testados por nosotros continuamente. Adem&aacute;s podr&aacute; disfrutar de tarifas y promociones exclusivas para los miembros de <em>TimeSavers &ndash; Quality Personal Concierge</em>.</p>");

define("CT0","<p>Ante todo queremos agradecer su inter&eacute;s en <em>TimeSavers &ndash; Quality Personal Concierge</em>.  Le enviaremos una respuesta a su consulta con la mayor brevedad posible.</p>
	<p>Atentamente<br /><em>TimeSavers  &ndash; Quality Personal Concierge</em></p>
	<p style='font-size:10px;'>AVISO LEGAL: La informaci&oacute;n contenida en este  mensaje de correo electr&oacute;nico es confidencial y puede revestir el car&aacute;cter de  reservada. Est&aacute; destinada exclusivamente a su destinatario. El acceso o uso de  este mensaje, por parte de cualquier otra persona que no est&eacute; autorizada,  pueden ser ilegales. Si no es Ud. la persona destinataria, le rogamos que  proceda a eliminar su contenido. Conforme a la Ley Org&aacute;nica 15/1999, de 13 de  diciembre, le comunicamos que su direcci&oacute;n de correo electr&oacute;nico forma parte de  nuestro fichero automatizado con el objetivo de poder mantener el contacto con  Ud. Si desea oponerse, acceder, cancelar o rectificar sus datos, p&oacute;ngase en  contacto con TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. al tel&eacute;fono +34 93 304  00 32.</p>");
define("CT1A","<p>En <em>TimeSavers &ndash; Quality Personal Concierge</em> buscamos proveedores dedicados a un servicio excelente y con productos impecables para satisfacer las necesidades de nuestros miembros.</p>
		<p>M&aacute;s que una simple relaci&oacute;n comercial queremos establecer una estrecha colaboraci&oacute;n y desarrollar juntos productos exclusivos que incentivar&aacute;n nuestros miembros a utilizarlos.</p>
		<p>&iquest;Interesado? &iexcl;Entonces no dude en rellenar el formulario y nos pondremos en contacto lo m&aacute;s r&aacute;pido posible!</p>");
define("CT1B","En <em>TimeSavers &ndash; Quality Personal Concierge</em> estamos a su disposici&oacute;n y apostamos por la total transparencia. As&iacute; que no dude en contactarnos y exponernos sus dudas o consultas. Estaremos encantados de atenderle con la m&aacute;xima celeridad.");
define("CT2","Los datos que nos aporta ser&aacute;n tratados de manera confidencial,  conforme a nuestra <a href='legal.php'>Pol&iacute;tica  de Privacidad</a> y a lo establecido en la Ley Org&aacute;nica 15/99 de Protecci&oacute;n  de Datos Personales.");
define("CT3","He le&iacute;do y acepto el <a href='legal.php'>Aviso Legal y las Condiciones de Uso</a> de la p&aacute;gina web de TimeSavers &ndash; Quality Personal Concierge.");
define("CT4","No deseo recibir informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS QUALITY CONCIERGE SERVICE, S.L.");
define("CT5","No deseo que mis datos sean conservados en TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. m&aacute;s tiempo del legalmente exigido.");
define("CT6","No deseo recibir informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. una vez finalizada nuestra relaci&oacute;n comercial.");
define("CONT1","Nombre y apellidos");
define("CONT2","Nombre de Empresa");
define("CONT3","Tipo de Servicio");
define("CONT6","Campos obligatorios");
define("CONT7","ENVIAR");

define("FAQ1","&iquest;Por qu&eacute; TimeSavers?");
define("FAQ2","&iquest;Hay algo que TimeSavers no puede organizar?");
define("FAQ3","&iquest;TimeSavers aplica un incremento a la tarifa de sus proveedores?");
define("FAQ4","&iquest;Se admiten pedidos de &uacute;ltima hora?");
define("FAQ5","&iquest;Cu&aacute;les son los pasos para hacer un pedido?");
define("FAQ6","&iquest;D&oacute;nde presta sus servicios TimeSavers?");
define("FAQ7","&iquest;Puedo contactar con TimeSavers a cualquier hora?");
define("FAQ8","&iquest;Es TimeSavers el proveedor de todos  los servicios ofrecidos?");
define("FAQ9","&iquest;Qu&eacute; garant&iacute;as tengo, si el servicio es  subcontratado a un tercero?");
define("FAQ10","&iquest;C&oacute;mo me puedo dar de alta/baja?");
define("FAQ11","&iquest;Una vez comprado, cuando tengo que utilizar las gestiones TimeSavers?");
define("FAQ12","&iquest;Cual es la pol&iacute;tica de protecci&oacute;n de datos de TimeSavers?");
define("FAQ13","&iquest;C&oacute;mo  funciona TimeSavers?");

define("FAQ1_X","Es un servicio asequible que le permitir&aacute; delegar tareas necesarias que no tiene el tiempo o la posibilidad de hacer Ud. mismo. En consecuencia, podr&aacute; reducir su nivel de estr&eacute;s y aumentar su tiempo libre. <em>TimeSavers &ndash; Quality Personal Concierge</em> se compromete a ejecutar cada pedido seg&uacute;n los deseos e instrucciones de sus miembros a menos que tengamos sugerencias de mejora. Esperamos que con el tiempo y ganando su confianza nos vea como una parte integral de su sistema de gesti&oacute;n de la vida.");
define("FAQ2_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> intentar&aacute; satisfacer al m&aacute;ximo cualquier deseo de sus miembros mientras que sea legal y moral. Somos un equipo con un una amplia y diversa experiencia dedicados a 100% a buscar la soluci&oacute;n a todo pedido. En el caso de que llegue, por ejemplo, un pedido de &uacute;ltima hora para una mesa en uno de los restaurantes de moda para viernes noche, haremos, a trav&eacute;s de nuestra extensa red de contactos, todo lo posible para conseguir su mesa. Y en caso de no lograrlo, le presentaremos una soluci&oacute;n que le encantar&aacute; de igual manera.");
define("FAQ3_X","La respuesta es un no rotundo. Nuestros miembros al menos disfrutar&aacute;n de las tarifas oficiales. Adem&aacute;s buscaremos acuerdos con nuestros proveedores para ofrecer promociones continuas. Siempre se tendr&aacute; tambi&eacute;n en cuenta el perfil e historial de cada miembro a la hora de sugerir una soluci&oacute;n adecuada a sus posibilidades.");
define("FAQ4_X","No hace falta comentar que proporcion&aacute;ndonos m&aacute;s tiempo permitir&aacute; afinar y mejorar nuestra propuesta. Sin embargo, trataremos de satisfacer su deseo sea cual sea la antelaci&oacute;n. &iexcl;En caso de no poder conseguirlo, conf&iacute;e en nosotros, le haremos una sugerencia equivalente o mejorada!");
define("FAQ5_X","H&aacute;ganos llegar su consulta o encargo a trav&eacute;s  de e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>,  ll&aacute;menos al tel&eacute;fono +34 93 304 00 32 o simplemente acceda al apartado &ldquo;Mi  TimeSavers&rdquo;, le proporcionaremos una soluci&oacute;n y un presupuesto adaptados a sus  necesidades lo m&aacute;s r&aacute;pido posible. Si se  trata de un servicio que necesita la actuaci&oacute;n de uno de nuestros colaboradores  externos el presupuesto diferenciar&aacute; claramente entre el coste del servicio y  los cr&eacute;ditos requeridos por su gesti&oacute;n. Tendr&aacute; que aprobar este presupuesto por  e-mail, tel&eacute;fono o a trav&eacute;s de &ldquo;Mi TimeSavers&rdquo; y pagar un 50% del importe (si  el importe es superior a 100 &euro;). 
Una vez finalizado el servicio le enviaremos  una factura que refleja los TimeSavers Credits y el servicio/producto realmente  utilizados. Para la factura final se tendr&aacute; en cuenta toda compra de paquete  TimeSavers Credits realizada antes del inicio del servicio solicitado y se  aplicar&aacute; a dicha factura. En caso de que no haya comprado TimeSavers Credits le  facturaremos el n&uacute;mero exacto de cr&eacute;ditos utilizados en la gesti&oacute;n del  servicio.");
define("FAQ6_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> opera principalmente en Barcelona ciudad.");
define("FAQ7_X","Nuestro  horario comercial es de lunes a viernes de 10h a 20h. Sin embargo, existe la  posibilidad de atenderle las 24 horas del d&iacute;a llam&aacute;ndonos al +34 93 304 00 32. A  gestiones fuera del horario comercial se aplicar&aacute; un 25% sobre la tarifa.");
define("FAQ8_X","Ofrecemos todos nuestros servicios  con un compromiso de excelencia y sin fallos. Existen servicios  prestados directamente por <em>TimeSavers - Quality Personal Concierge</em>  y otros subcontratados a terceros. Todos los servicios  externalizados se llevar&aacute;n a cabo por nuestros cualificados y testados proveedores.");
define("FAQ9_X","Por lo que respecta a las garant&iacute;as,  <em>TimeSavers - Quality Personal Concierge</em>  opera con un seguro de  responsabilidad civil, que cubre cualquier transacci&oacute;n, y exigimos lo mismo a nuestros proveedores.");
define("FAQ10_X","El alta se  efect&uacute;a con un pago de 50 &euro; durante el registro. Este importe se descontar&aacute;  posteriormente a la compra de gesti&oacute;n TimeSavers. Se puede dar de baja en  cualquier momento envi&aacute;ndonos un e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> o a trav&eacute;s de un simple click en el apartado de &ldquo;Mi TimeSavers&rdquo; de nuestra  p&aacute;gina web. Gestiones compradas y no utilizadas se reembolsar&aacute;n hasta un plazo  de 30 d&iacute;as naturales a partir de la fecha de compra.");
define("FAQ11_X","A partir del d&iacute;a de la compra toda gesti&oacute;n TimeSavers ser&aacute; v&aacute;lida durante 1 a&ntilde;o.");
define("FAQ12_X","Le remitimos a la secci&oacute;n <a title='Legal' href='legal.php'>&ldquo;Aviso Legal &amp; Condiciones de Uso&rdquo;.");
define("FAQ13_X","<em>TimeSavers &ndash; Quality  Personal Concierge</em> funciona a base de cr&eacute;ditos. La gesti&oacute;n de cada servicio por  parte nuestra requiere un cierto n&uacute;mero de TimeSavers Credits. Dichos cr&eacute;ditos  se compran en el apartado &ldquo;Mi TimeSavers&rdquo; una vez que se haya dado de alta como  miembro TimeSavers. Para m&aacute;s informaci&oacute;n sobre TimeSavers Credits haga click <a href='http://www.timesavers.es/gestiones.php'>aqu&iacute;</a>.");

define("GEST0A","<em class='blanco'>TimeSavers &ndash; Quality Personal  Concierge</em> funciona a base de cr&eacute;ditos y la gesti&oacute;n de cada servicio requiere un  cierto n&uacute;mero de cr&eacute;ditos. Los cr&eacute;ditos se compran en el apartado &ldquo;Mi  TimeSavers&rdquo; una vez que se haya dado de alta como miembro TimeSavers.<br />
  <br />
  Cuando haga una solicitud de servicio le haremos llegar por e-mail y/o a su  cuenta &ldquo;Mi TimeSavers&rdquo; un presupuesto sin ning&uacute;n compromiso que detalla el  n&uacute;mero de cr&eacute;ditos previsto para su gesti&oacute;n. En el apartado &quot;Mi  TimeSavers&quot; tiene la posibilidad de adquirir paquetes de TimeSavers  Credits m&aacute;s importantes para conseguir un precio m&aacute;s atractivo por cr&eacute;dito. <br />
  Tendr&aacute; que aprobar este presupuesto  por e-mail, tel&eacute;fono o a trav&eacute;s de &ldquo;Mi TimeSavers&rdquo; y pagar un 50% del importe  (si el importe es superior a 100 &euro;). Si se trata de un servicio que necesita la  actuaci&oacute;n de uno de nuestros colaboradores externos el presupuesto diferenciar&aacute;  claramente entre el coste del servicio y los cr&eacute;ditos requeridos por su  gesti&oacute;n.<br />
  <br />Una vez finalizado el servicio le  enviaremos una factura que refleja los TimeSavers Credits y el  servicio/producto realmente utilizados. Para la factura final se tendr&aacute; en  cuenta toda compra de paquete TimeSavers Credits realizada antes del inicio del  servicio solicitado y se aplicar&aacute; a dicha factura. En caso de que no haya  comprado TimeSavers Credits le facturaremos el n&uacute;mero exacto de cr&eacute;ditos  utilizados en la gesti&oacute;n del servicio.<br />
  Todos los pagos se pueden realizar por  transferencia bancaria o directamente en &ldquo;Mi TimeSavers&rdquo;.<br />
  <br />Cabe destacar que con casi todos nuestros colaboradores externos hemos  conseguido un valor a&ntilde;adido para nuestros miembros, ya sea un descuento, una  oferta especial u otro beneficio exclusivo.<br />
  <br />Todos los precios incluyen IVA.<br />
  TimeSavers Credits tienen una validez de un a&ntilde;o a partir de la fecha de compra.");
define("GEST0","
	   <p><em>F&aacute;cil y r&aacute;pido: H&aacute;ganos llegar su consulta o encargo  a trav&eacute;s de e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>,  ll&aacute;menos al tel&eacute;fono +34 93 304 00 32 o simplemente acceda a &ldquo;Mi TimeSavers&rdquo;,  le proporcionaremos una soluci&oacute;n adaptada a sus necesidades lo antes posible.</em><br><br><b>Precios TimeSavers Credits</b><br>A continuaci&oacute;n enumeramos  los diferentes paquetes de TimeSavers Credits que pueden adquirir:</p>
	    <table cellpadding='4' cellspacing='1'>
		<tr class='filazul_1'><td>1  TimeSavers Credit</td><td>12,00 &euro;</td></tr>
		<tr class='filazul_2'><td>5  TimeSavers Credits</td><td>58,00 &euro;</td></tr>
		<tr class='filazul_1'><td>10  TimeSavers Credits</td><td>112,00 &euro;</td></tr>
		<tr class='filazul_2'><td>25  TimeSavers Credits</td><td>265,00 &euro;</td></tr>
		<tr class='filazul_1'><td>50  TimeSavers Credits</td><td>480,00 &euro;</td></tr>
		<tr class='filazul_2'><td>100 TimeSavers  Credits</td><td>840,00 &euro;</td></tr>
		</table>
	    <p>Si est&aacute; interesado en  contratar un paquete m&aacute;s importante de TimeSavers Credits, por favor, p&oacute;ngase  en contacto con nosotros llam&aacute;ndonos al +34 93 304 00 32 o envi&aacute;ndonos un mail  a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");
define("GEST1","<p>Ofrecemos la posibilidad  de una suscripci&oacute;n mensual. Puede elegir entre dos paquetes y tambi&eacute;n podemos  desarrollar un paquete apto para sus necesidades particulares. La suscripci&oacute;n  TimeSavers Monthly incluye como beneficio reservas de restaurante y gestiones  de agenda gratis sin, por supuesto, renunciar a las ventajas que reciben todos  los miembros <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em>.</p>
<p><b>TimeSavers Monthly Basic</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Hasta 16 TimeSavers Credits</td><td>120,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. reservas de restaurante y gesti&oacute;n de agenda personal gratis. </td></tr>
<tr class='filazul_1'><td colspan='2'>Atenci&oacute;n al cliente de lunes a viernes de 10h a 20h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Gastos de desplazamiento en Barcelona ciudad incluidos.</td></tr>
</table>
<p><b>TimeSavers Monthly Premium</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Hasta 40 TimeSavers Credits</td><td>250,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. reservas de restaurante y gesti&oacute;n de agenda personal gratis. </td></tr>
<tr class='filazul_1'><td colspan='2'>Atenci&oacute;n al cliente las 24h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Gastos de desplazamiento en Barcelona ciudad incluidos.</td></tr>
</table>
<p>Las suscripciones  mensuales tienen una validez de 30 d&iacute;as naturales a partir de la fecha de  compra. TimeSavers Credits no utilizados durante ese periodo no se abonan a  siguientes suscripciones.</p>
<p><b>Ejemplos</b><br>Para darles una idea de cuantos TimeSavers Credits puede costar una gesti&oacute;n le proporcionamos los siguientes ejemplos:</p>
<ul>
  <li>1 reserva de restaurante le costar&aacute; 1 TimeSavers Credit. Tal como se  ha comentado anteriormente, le enviaremos un presupuesto por mail y/o al  apartado &ldquo;Mi TimeSavers&rdquo; cual tendr&aacute; que aprobar por una de estas dos v&iacute;as o  por tel&eacute;fono. Dado que el importe es inferior a 100 &euro; no es necesario pagar un  50% por adelantado. A continuaci&oacute;n le expedimos la factura que podr&aacute; pagar por  transferencia bancaria o a trav&eacute;s de &ldquo;Mi TimeSavers&rdquo;. Sepa que con todos los  restaurantes con los que colaboramos hemos negociado un plus para nuestros  clientes. &iexcl;D&eacute;jese sorprender! </li>
  <li>Enviarle un peluquero a casa supone 2 TimeSavers Credits la primera  vez que solicita el servicio m&aacute;s el coste del estilista. Le remitiremos un  presupuesto detallando los TimeSavers Credits requeridos y el coste del  servicio seg&uacute;n la informaci&oacute;n proporcionada. Tiene que aprobar el presupuesto y  pagar un 50% por adelantado. Una vez que el peluquero acude a su casa puede que  se decida de a&ntilde;adir un tratamiento cuidador al tinte inicialmente solicitado.  &iexcl;No se preocupe! No supone ning&uacute;n problema. Por eso le enviamos una factura  final que refleja los TimeSavers Credits y los servicios realmente utilizados.  Asimismo cuando repita con el mismo profesional, le haremos una gesti&oacute;n de  agenda personal que es de 1 TimeSavers Credit. </li>
  <li>Encargarnos la renovaci&oacute;n de la ITV suele tardar entre 2 y 3 horas por  lo cual valdr&iacute;a entre 8 y 12 TimeSavers Credits. Antes de iniciar el servicio,  como siempre, le enviamos un presupuesto con una estimaci&oacute;n de coste TimeSavers  Credits calculando&nbsp; un promedio. Se  repiten los pasos de la aprobaci&oacute;n y una vez finalizado el servicio  contabilizamos los cr&eacute;ditos realmente utilizados y le pasamos una factura,  menos el pago inicial, que podr&aacute; pagar por las dos v&iacute;as mencionadas. </li>
</ul>
<p>Los presupuestos siempre se calculan con el  n&uacute;mero de TimeSavers Credits m&aacute;s apropiado a la gesti&oacute;n del servicio. Sin  embargo, pueden en cualquier momento antes del inicio del servicio acceder a  &ldquo;Mi TimeSavers&rdquo; para comprar paquetes m&aacute;s importantes de TimeSavers Credits y  de esta manera conseguir un precio por cr&eacute;dito m&aacute;s atractivo. Tal compra de  TimeSavers Credits se aplicar&aacute; a la factura final del servicio solicitado.</p>");
define("GEST2","&iexcl;Disfrute de Barcelona de otra  manera y aproveche al m&aacute;ximo su estancia! Ofrecemos a nuestros amigos  visitantes una amplia gama de experiencias y aventuras &uacute;nicas. Por supuesto,  existe adem&aacute;s la posibilidad de dise&ntilde;ar un programa a su medida seg&uacute;n sus  deseos.<br />
  <br />Si est&aacute; interesado o quiere m&aacute;s informaci&oacute;n, por favor, p&oacute;ngase en contacto con  nosotros llam&aacute;ndonos al +34 93 304 00 32, envi&aacute;ndonos un mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> o haciendo click <a href='files/timesavers_aviso_visit.pdf' target='_blank'>aqu&iacute;</a>.");
define("GEST3","&iquest;Porque no regalar algo  realmente &uacute;til? La b&uacute;squeda de un regalo interesante, innovador y diferente  puede ser algo muy estresante. Aparte de que puede encargar <em>TimeSavers &ndash;  Quality Personal Concierge</em> con esta b&uacute;squeda, existe adem&aacute;s la posibilidad de  sorprender a su pareja, familiar o amigo con un bono TimeSavers Coupon.<br><br>Cuando compre su paquete  de TimeSavers Credits podr&aacute; elegir la opci&oacute;n &ldquo;TimeSavers Coupon&rdquo;.  Facilit&aacute;ndonos el e-mail o la direcci&oacute;n del destinatario le haremos llegar su  vale (el vale solo mostrar&aacute; el n&uacute;mero de TimeSavers Credits).");
define("GEST4","Seg&uacute;n un estudio de Reuters  dedicamos un 10% de nuestro tiempo laboral a gestionar tareas domesticas. <em>TimeSavers - Quality Personal Concierge</em> puede servir a su empresa como herramienta para  mejorar la conciliaci&oacute;n de la vida familiar y laboral de sus empleados. Sus  empleados tendr&aacute;n menos preocupaciones por lo cual podr&aacute;n mejorar su dedicaci&oacute;n  laboral. De hecho un estudio PriceWaterhouseCoopers hall&oacute; que empresas con una  buena pol&iacute;tica y pr&aacute;cticas de conciliaci&oacute;n consiguen un beneficio de 99  c&eacute;ntimos por cada euro invertido en Recursos Humanos comparado con un retorno  de solo 14 c&eacute;ntimos por el promedio de empresas europeas.<br><br>Todos los precios incluyen IVA.");
define("SER_1","B&uacute;squeda de internet");
define("SER_2","Organizaci&oacute;n cena");
define("SER_3","Catering");
define("SER_4","Espera entrega a domicilio");
define("SER_5","Paseador perro");
define("SER_6","Tr&aacute;mite burocr&aacute;tico");
define("SER_7","Organizaci&oacute;n eventos");
define("SER_8","Gesti&oacute;n agenda personal");
define("SER_9","Personal shopper");
define("SER_10","Organizaci&oacute;n viajes");
define("SER_11","Reserva restaurante");
define("SER_12","Reserva espect&aacute;culo");
define("SER_13","Servicio contestador llamadas");
define("SER_14","Compra regalos");
define("SER_15","Mudanza");
define("SER_16","Canguro ni&ntilde;o");
define("SER_17","Cuidado llaves de casa");
define("SER_18","Chofer");
define("SER_19","Recogida correo");
define("SER_20","Mensajer&iacute;a");
define("SER_21","Pedicura/ manicura");
define("SER_22","Peluqueria");
define("SER_23","Nutricionista");
define("SER_24","Entrenador personal");
define("SER_25","Spa");
define("SER_26","Masajista");
define("SER_27","Asesor de imagen");
define("SER_28","Interirismo/exteriorismo");
define("SER_29","Fontanero");
define("SER_30","Alba&ntilde;il");
define("SER_31","Electricista");
define("SER_32","Pintor");
define("SER_33","Cerrajero");
define("SER_34","Limpieza del hogar");
define("SER_35","Lavado de ropa");
define("SER_36","Planchado de ropa");
define("SER_37","Limpieza de veh&iacute;culos");
define("SER_38","Jardinero");
define("SER_39","Limpieza cena/fiesta");
define("SER_40","Tintorer&iacute;a");
define("SER_41","Cuidado de mascota");

define("SER0","Si est&aacute; interesado en alg&uacute;n otro servicio que no aparece en nuestro listado, no dude en llamarnos al n&uacute;mero de tel&eacute;fono +34 93 304 00 32 o contactarnos a trav&eacute;s del correo electr&oacute;nico <a href='mailto:info@timesavers.es' class='blanco'><b>info@timesavers.es</b></a> , le buscaremos una soluci&oacute;n con la mayor brevedad posible.");
define("SER1","<b>B&Uacute;SQUEDA DE INTERNET</b><br />Filtrar las millones de  entradas para una simple b&uacute;squeda de informaci&oacute;n en internet puede ser un  proceso muy largo. Nosotros contamos con expertos que se encargar&aacute;n de hacerlo  en su lugar.");
define("SER2","<b>ORGANIZACI&Oacute;N CENA</b><br />&iquest;Necesita recibir a un grupo  de gente en casa y le falta la vajilla apropiada, las copas, el centro de mesa  y el don de cocinar? Nos encargamos de toda la organizaci&oacute;n para que solo tenga  que disfrutar del men&uacute; y de sus invitados.");
define("SER3","<b>CATERING</b><br />&iquest;La cocina le resulta ser una  pesadilla? Contamos con chefs con experiencia y servicios de catering  excelentes para quitarle esta  preocupaci&oacute;n.");
define("SER4","<b>ESPERA ENTREGA A DOMICILIO</b><br />Si alguna vez le han tenido que entregar algo en casa, sabe que suelen dar intervalos de al menos 3 horas para hacerlo. &iquest;Y c&oacute;mo gestiona eso un d&iacute;a  laboral? Nos ofrecemos a esperar en su lugar para que no pierda el tiempo.");
define("SER5","<b>PASEADOR PERRO</b><br />&iquest;Su amigo favorito necesita  correr y jugar el en parque? Tenemos a profesionales para pasear a su perro.");
define("SER6","<b>TR&Aacute;MITE BUROCR&Aacute;TICO</b><br />Los tr&aacute;mites burocr&aacute;ticos  suelen ser lentos y pesados. &iexcl;Nosotros los tramitamos! Tenemos profesionales  con experiencia para gestionarlo todo.");
define("SER7","<b>ORGANIZACI&Oacute;N EVENTOS</b><br />&iquest;Necesita organizar un  aniversario especial, una boda, y no tiene el tiempo suficiente para  gestionarlo todo? Tenemos a su servicio profesionales que le proporcionar&aacute;n un  evento inolvidable.");
define("SER8","<b>GESTI&Oacute;N AGENDA PERSONAL</b><br />Recordar todas las  reuniones, cumplea&ntilde;os y cenas puede resultar muy estresante. Deje que nos  ocupemos de la gesti&oacute;n de su agenda y nunca m&aacute;s perder&aacute; ninguna cita  importante.");
define("SER9","<b>PERSONAL SHOPPER</b><br />&iquest;Ir de compras le resulta un  calvario? &iquest;O simplemente no tiene el tiempo de hacerlo? &iquest;O quiz&aacute;s quiere una  opini&oacute;n externa para renovar su armario? Contamos con profesionales para  asesorarle y hacer las compras con o por usted.");
define("SER10","<b>ORGANIZACI&Oacute;N VIAJES</b><br />Buscar los vuelos m&aacute;s  baratos, un hotel bonito, unas excursiones interesantes para que tenga unas  vacaciones especiales puede resultar bastante dif&iacute;cil. Oc&uacute;pese de hacer las  maletas y nosotros nos encargaremos del resto.");
define("SER11","<b>RESERVA RESTAURANTE</b><br />&iquest;Quiere descubrir nuevos  restaurantes o tiene dificultades en conseguir una mesa a &uacute;ltima hora? Poseemos  una amplia cartera para satisfacer a todos los gustos.");
define("SER12","<b>RESERVA ESPECT&Aacute;CULO</b><br />&iquest;Le cuesta conseguir las  plazas para ese espect&aacute;culo que le gusta tanto a su pareja? Conf&iacute;e en que  haremos todo lo posible para obtenerlas y qu&iacute;tese esta preocupaci&oacute;n de encima.");
define("SER13","<b>SERVICIO CONTESTADOR LLAMADAS</b><br />&iquest;Necesita que  alguien atienda a sus llamadas en su ausencia? Nosotros ofrecemos un servicio  de contestador de llamadas y le remitiremos solamente las importantes.");
define("SER14","<b>COMPRA REGALOS</b><br />Puede ser dif&iacute;cil encontrar el  regalo de cumplea&ntilde;os adecuado. Conf&iacute;e en nosotros y le localizaremos el regalo  id&oacute;neo.");
define("SER15","<b>MUDANZA</b><br />Una mudanza siempre es una cosa  laboriosa. Podemos gestionarla para que sea lo menos penosa posible.");
define("SER16","<b>&ldquo;CANGURO&rdquo; NI&Ntilde;O</b><br />&iquest;Quiere regalarse una noche  rom&aacute;ntica con su pareja sin tener que preocuparse por su ni&ntilde;o? Contamos con  &ldquo;canguros&rdquo; profesionales y con credenciales.");
define("SER17","<b>CUIDADO LLAVES DE CASA</b><br />Si ha perdido alguna  vez sus llaves de casa, sabe cu&aacute;nto puede costar un servicio de cerrajero.  Cuidamos una copia de sus llaves de casa para cualquier emergencia.");
define("SER18","<b>CHOFER</b><br />Llega su familia al aeropuerto y no  tiene tiempo de ir a buscarlos. Tiene una cena con amigos y no quiere  preocuparse de controles de alcoholemia. No hay problema, le enviamos un chofer  para que no tenga que preocuparse.");
define("SER19","<b>RECOGIDA CORREO</b><br />Si no quiere que se desborde  su buz&oacute;n durante sus vacaciones, enviaremos a alguien para recoger y guardar su  correo en su ausencia.");
define("SER20","<b>MENSAJER&Iacute;A</b><br />&iquest;Necesita hacer un env&iacute;o urgente en  Barcelona ciudad? Vamos a recogerlo y lo entregamos.");
define("SER21","<b>PEDICURA/ MANICURA</b><br />No repare en atenciones para s&iacute; mismo,  solic&iacute;tenos el servicio y se lo enviaremos.");
define("SER22","<b>PELUQUERIA</b><br />&iquest;Alguna urgencia? &iquest;Reuniones no programadas, o simplemente necesita una atenci&oacute;n a domicilio? Un estilista resolver&aacute;  cualquiera de estos inconvenientes.");
define("SER23","<b>NUTRICIONISTA</b><br />D&eacute;jese asesorar solo por profesionales  acreditados. Contamos con nutricionistas que le asesorar&aacute;n y har&aacute;n  seguimiento de su caso.");
define("SER24","<b>ENTRENADOR PERSONAL</b><br />&iquest;Falta de tiempo para asistir a  gimnasios? &iquest;Necesita un programa personalizado? Cont&aacute;ctenos y se lo  proporcionaremos.");
define("SER25","<b>SPA</b><br />Perm&iacute;tase un momento de relax. Deje que se ocupen de  usted, simplemente rel&aacute;jese, nosotros le daremos acceso a las  mejores instalaciones.");
define("SER26","<b>MASAJISTA</b><br />&iquest;S&iacute;ntomas de estr&eacute;s, molestias localizadas?  &iquest;O necesita una atenci&oacute;n terap&eacute;utica?, Un profesional sabr&aacute; tratar  cada uno de los casos, se sentir&aacute; mejor.");
define("SER27","<b>ASESOR DE IMAGEN</b><br />Si tiene un acto social y se encuentra  perdido, o le gustar&iacute;a realizar un cambio de imagen radical, le  asesorar&aacute;n profesionales dedicados a la imagen, buscando siempre su bienestar.");
define("SER28","<b>INTERIRISMO/EXTERIORISMO</b><br />&iquest;Necesita un par de consejos sobre interiorismo &frasl; exteriorismo, o prefiere que nos encarguemos personalmente de todo? Escoja la mejor opci&oacute;n para su casa y disfrute de los resultados.");
define("SER29","<b>FONTANERO</b><br />Cuando la urgencia prima y no hay tiempo que perder, le  encontraremos el profesional que solventar&aacute; sus aver&iacute;as de una manera  r&aacute;pida y eficaz.");
define("SER30","<b>ALBA&Ntilde;IL</b><br />Los peque&ntilde;os arreglos en casa no tienen por qu&eacute; ser  un problema. Nosotros le daremos soluciones y contactaremos con el  profesional adecuado.");
define("SER31","<b>ELECTRICISTA</b><br />Deje solamente a profesionales acceder y  realizar modificaciones en su instalaci&oacute;n el&eacute;ctrica. El buen funcionamiento de  la misma depende de ello. Cons&uacute;ltenos.");
define("SER32","<b>PINTOR</b><br />Olv&iacute;dese de las jornadas interminables de pintura en  casa. Le conseguiremos un servicio profesional, nos ocuparemos que todo  funcione y que el resultado sea impecable.");
define("SER33","<b>CERRAJERO</b><br />En momentos de urgencia, no dude. Rapidez y  eficacia. P&oacute;ngase en contacto con TimeSavers.");
define("SER34","<b>LIMPIEZA DEL HOGAR</b><br />Un personal eficiente  y profesional para un servicio de uso continuado, o simplemente para  fechas puntuales, cons&uacute;ltenos y encontraremos la soluci&oacute;n adecuada.");
define("SER35","<b>LAVADO DE ROPA</b><br />Deje que nos  encarguemos de su colada, le garantizamos un servicio puntual, un tratamiento  adecuado y un resultado impecable.");
define("SER36","<b>PLANCHADO DE ROPA</b><br />Deje que recojamos su  ropa y nos encarguemos de la plancha, le garantizamos un tratamiento y  resultados impecables.");
define("SER37","<b>LIMPIEZA DE VEH&Iacute;CULOS</b><br />Nos  encargaremos de la limpieza rutinaria de su veh&iacute;culo, o si lo desea , se  tratar&aacute; en profundidad para prolongar el buen estado del veh&iacute;culo.");
define("SER38","<b>JARDINERO</b><br />Deje sus plantas en manos de profesionales, manteni&eacute;ndolas saludables o tr&aacute;telas de los peque&ntilde;os problemas que puedan surgir.");
define("SER39","<b>LIMPIEZA CENA/FIESTA</b><br />Olv&iacute;dese del d&iacute;a  despu&eacute;s. Nos encargaremos de que todo quede impecable. Conserve solamente  los buenos momentos de sus reuniones y fiestas.");
define("SER40","<b>TINTORER&Iacute;A</b><br />Olv&iacute;dese de los peque&ntilde;os  contratiempos con sus prendas, buscaremos la soluci&oacute;n m&aacute;s adecuada y respetuosa  con su ropa.");
define("SER41","<b>CUIDADO DE MASCOTA</b><br />Deje sus  mascotas en las mejores manos. Profesionales de confianza que cuidar&aacute;n de  su mascota como solamente usted podr&iacute;a hacerlo.");
?>