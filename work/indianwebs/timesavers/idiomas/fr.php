<?
define("FECHA_1","Date");
define("FECHA_DIA","jour");
define("FECHA_MES","mois");
define("FECHA_ANO","ann&eacute;e");
define("MES_1","Enero");
define("MES_2","Febrero");
define("MES_3","Marzo");
define("MES_4","Abril");
define("MES_5","Mayo");
define("MES_6","Junio");
define("MES_7","Julio");
define("MES_8","Agosto");
define("MES_9","Septiembre");
define("MES_10","Octubre");
define("MES_11","Noviembre");
define("MES_12","Diciembre");

define("BT1","About<br />TimeSavers");
define("BT2","Services");
define("BT2A","Time Savers");
define("BT2B","Personal Care");
define("BT2C","Home Improvement");
define("BT2D","Home Care");
define("BT3A","Credits");
define("BT3B","Prix");
define("BT3C","Gesti&oacute;n");
define("BT4","Presse");
define("BT4A","TimeSavers News");
define("BT4B","TimeSavers dans les M&eacute;dias");
define("BT5A","Contact");
define("BT5B","Contact Partenaire");
define("BT6","Legal &amp; Conditions d'utilisation");
define("BT7","Mot de passe oubli&eacute;?");
define("BT8A","Que fait TimeSavers?");
define("BT8B","Qui sont  TimeSavers?");
define("BT9","Inscrire");
define("BT10","Entrer");
define("BT11","CHANGER PHOTO");
define("XHORA","Heure");
define("XMAPA","Voir Plan");
define("XCOMPRAR","Acheter");
define("XCANCEL","Rechazar");

define("REG_INICIO","<p>Bienvenue chez <em>TimeSavers &ndash; Quality  Personal Concierge</em>! Vous &ecirc;tes sur le point de commencer une nouvelle &eacute;tape  de vie, une dans laquelle nous esp&eacute;rons r&eacute;duire votre niveau de stress et  augmenter votre temps libre.</p>
	<p>S&rsquo;il vous pla&icirc;t, abonnez-vous maintenant comme membre TimeSavers. La  cotisation de souscription est de 50,- &euro; et sera cr&eacute;dit&eacute;e aux achats de  TimeSavers Credits.</p>
	<p>A cet effet, nous vous demandons de remplir le formulaire ci-dessous.</p>");
define("REG_ELEGIR","(Choisir)");
define("REG_TRATA","Titre");
define("REG_TRATA_1","Mr.");
define("REG_TRATA_2","Mme.");
define("REG_TRATA_3","Dr.");
define("REG_NOMBRE","Pr&eacute;nom");
define("REG_APELLIDOS","Nom de famille");
define("REG_SEXO","Sexe");
define("REG_SEXO_1","Hommme");
define("REG_SEXO_0","Femmme");
define("REG_NACIMIENTO","Date de naissance");
define("REG_NACIONALIDAD","Nationalit&eacute;");
define("REG_DOCUMENTO","Type de document d'identit&eacute;");
define("REG_DOCNUM","N&ordm; document d'identit&eacute;");
define("REG_DIRECCION","Adresse");
define("REG_POBLACION","Ville");
define("REG_CP","C.P.");
define("REG_TEL","T&eacute;l&eacute;phone");
define("REG_MOVIL","Portable");
define("REG_IDIOMA","Langue pr&eacute;f&eacute;r&eacute;e de communication");
define("REG_MODO","Mode pr&eacute;f&eacute;r&eacute;e de communication");
define("REG_CONOCIO","Comment nous avez-vous connu?");
define("REG_CONCIO_1","Partenaire");
define("REG_CONCIO_2","Ami");
define("REG_CONCIO_3","Annonce");
define("REG_CONCIO_4","Flyer");
define("REG_CONCIO_5","TV");
define("REG_CONCIO_6","Autre");
define("REG_COMENTA","Remarques");
define("REG_FOTO","Votre photo  : (Format JPG)");
define("REG_CONTRA","Mot de passe");
define("REG_CONTRA2","Changer mot de passe");
define("REG_CONTRA3","Le mot de passe a &eacute;t&eacute; envoy&eacute; au mail");
define("REG_MODIF","UPDATE");
define("REG_BAJADAR","RESILIER AFFILIATION");
define("REG_BAJA","Se puede dar de baja en cualquier momento envi&aacute;ndonos un email a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, llam&aacute;ndonos al +34 93 304 0032 o a trav&eacute;s de un simple click en su cuenta. Productos comprados y no utilizados se reembolsar&aacute;n hasta un plazo de 30 d&iacute;as a partir de la fecha de compra.");
define("REG_REGISTRAR","INSCRIRE");
define("REG_DESCRIP","Description");
define("REG_LUGAR","Lieu");
define("REG_DIFERENTE","si diff&eacute;rent de l&rsquo;enregistr&eacute;");
define("REG_CARRITO","AJOUTER AU PANIER");
define("REG_FECHASER","Date d&eacute;sir&eacute;e du service");
define("REG_CODIGO","Au cas o&ugrave; vous  ayez un code promotionnel, inscrivez-le ici. La remise sera appliqu&eacute;e &agrave; votre  premier achat de TimeSavers Credits.");
define("REG_FIN","Bienvenue chez <em>TimeSavers &ndash; Quality Personal Concierge</em>!<br /><br />Le mot de passe a &eacute;t&eacute; envoy&eacute; au mail");

define("OLVIDO","<h3>Oubli&eacute; votre mot de passe</h3>
        <p>Au cas o&ugrave; vous  auriez oubli&eacute; votre e-mail de user/mot de passe, veuillez bien nous contacter  par t&eacute;l&eacute;phone au +34 93 304 00 32 ou en nous envoyant un mail &agrave; <a href='mailto:info@timesavers.es'>info@timesavers.es</a></p>");

define("USER_MI","Mon Timesavers");
define("USER_PEDIDO","Demande de Service");
define("USER_PEDIR","REALIZAR PEDIDO");
define("USER_SALDO_1","TimeSavers<br />Credits<br />utilis&eacute;s");
define("USER_SALDO_2","Solde<br />TimeSavers<br />Credits");
define("USER_ACCION","Actions");
define("USER_HISTORIAL_1","Devis");
define("USER_HISTORIAL_2","Factures");
define("USER_IMPORTE","Montant");
define("USER_DETALLES","D&eacute;tails");
define("USER_DATOS","Donn&eacute;es Personnelles");
define("USER_NOTICIAS","News");
define("USER_RECOMENDA","Recommandations");
define("USER_RECOMENDA_2","Type de recommandation");
define("USER_RECOMENDA_10","Service d&eacute;sir&eacute;");
define("USER_RECOMENDA_20","Service offert");
define("USER_RECOMENDA_30","Incident en cas de demande");
define("USER_RECOMENDA_40","M&eacute;thode de travail");
define("USER_RECOMENDA_3","RECOMMANDER");
define("USER_RECOMENDACION","<p>Chez <em>TimeSavers &ndash; Quality Personal  Concierge</em>, nous appr&eacute;cions votre opinion. La satisfaction maximale de nos  membres est notre objectif primordial. Naturellement, la meilleure fa&ccedil;on de  l&rsquo;obtenir est de vous &eacute;couter et de vous demander votre collaboration. </p>
	<p>Dans ce but, nous avons mis &agrave; votre disposition ce &laquo;&nbsp;questionnaire  satisfaction&nbsp;&raquo;. Soyez assur&eacute; que nous y r&eacute;pondrons imm&eacute;diatement. </p>
	<p>&Eacute;videmment, vous pouvez aussi nous contacter par t&eacute;l&eacute;phone au +34 93 304 00  32 ou nous envoyer un courriel &agrave; <a href='mailto:info@timesavers.es'>info@timesavers.es</a> pour parler directement avec un de nos conseillers.</p>");

define("IND_VIDEO","<object width='220' height='220'><param name='allowfullscreen' value='true' /><param name='allowscriptaccess' value='always' /><param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=2941521&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=59a5d1&amp;fullscreen=1' /><embed src='http://vimeo.com/moogaloop.swf?clip_id=2941521&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=59a5d1&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='220' height='220'></embed></object>
            <p><a href='http://vimeo.com/' target='_blank'>Vid&eacute;o de Bienvenue: TimeSavers - Quality Personal Concierge</a>.</p>");
define("IND0","Bienvenue chez TimeSavers - Quality Personal Concierge");
define("IND1","<p>Selon l&lsquo;Acad&eacute;mie Fran&ccedil;aise, l&rsquo;origine du terme &ldquo;concierge&rdquo; remonte au mot  m&eacute;di&eacute;val &bdquo;cumcerges, qui signifiait gardien. Par d&eacute;finition, le concierge est une  personne qui s&rsquo;occupe d&rsquo;une maison, d&rsquo;un b&acirc;timent public ou d&rsquo;un palais. </p>
	<p>Les &laquo;services de concierge&nbsp;&raquo; modernes apparurent dans les ann&eacute;es 80, o&ugrave;  sinon &agrave; New York, la ville qui ne dort jamais. Ils &eacute;taient et sont la r&eacute;ponse  au stress dans la vie quotidienne et au d&eacute;sir de concilier au mieux &nbsp;vie professionnelle et vie priv&eacute;e. </p>
	<p>Depuis le concept a conquis le monde et maintenant, TimeSavers &ndash; Quality  Personal Concierge offre enfin ce service &agrave; Barcelone. </p>");
define("IND2","&iexcl;Imaginez une vie sans stress!");
define("IND3","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> a &eacute;t&eacute; cr&eacute;&eacute; avec la vocation de vous  simplifier la vie, vous permettant de profiter de plus de temps libre et ainsi  d&rsquo;augmenter votre qualit&eacute; de vie et celle de votre famille. </p>
		<p>Chez TimeSavers, nous sommes des fournisseurs de temps, pour que vous  puissiez vous consacrer &agrave; ce qui vous int&eacute;resse r&eacute;ellement.&nbsp; </p>
		<p>La vie quotidienne dans une grande ville est remplie de t&acirc;ches  fastidieuses, de meetings, de courses, de d&eacute;marches bureaucratiques lentes et  p&eacute;nibles&hellip;..</p>
		<p>Imaginez-vous une vie dans laquelle vous&nbsp;  b&eacute;n&eacute;ficiez de votre concierge personnel, toujours &agrave; votre disposition  sur simple appel ou click? </p>
		<p>Avec <em>TimeSavers &ndash; Quality Personal Concierge</em>, vous pouvez compter sur une  &eacute;quipe d&rsquo;experts efficaces et discrets, qui feront une r&eacute;alit&eacute; de ce r&ecirc;ve  d&rsquo;avoir son propre assistant personnel ou concierge. </p>");
define("IND4","Informez-vous de notre catalogue de produits pour visiteurs en faisant click ici!");

define("LIN1","<p>Ici vous trouverez les links aux sites internet qui nous fascinent  personnellement et/ou qui vous enthousiasmeront. </p>
	<p style='font-size:10px;'>TimeSavers &ndash; Quality Personal Concierge ne peut pas &ecirc;tre rendu responsable pour le contenu de  sites internet d&rsquo;autres fournisseurs, qui sont accessibles &agrave; travers ces links.</p>");
define("LIN2","Secci&oacute;n en construcci&oacute;n...");

define("LEG1","<p>Esta  p&aacute;gina web es propiedad de <b>TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L. </b>en lo adelante, <b>TIMESAVERS,</b> el hecho de acceder a esta p&aacute;gina implica el  conocimiento y aceptaci&oacute;n de la pol&iacute;tica de privacidad. Esta pol&iacute;tica de  privacidad forma parte de las condiciones y t&eacute;rminos de usos.<br />
          En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio,  de Servicios de la Sociedad  de la Informaci&oacute;n y Comercio Electr&oacute;nico (LSSICE) se exponen los datos  identificativos.</p>");
define("LEG2","<p><b>Condiciones  de Uso y Pol&iacute;tica de Privacidad </b></p>
        <p>1. <u>Datos Identificativos de la Empresa</u></p>
        <p>Identidad y Direcci&oacute;n:</p>
        <p>Empresa: TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.<br />CIF: B64806862 - Nacionalidad: Espa&ntilde;ola <br />Domicilio: Ronda Universitat, 15, 1&ordm; 1&ordf;,  08007 - Barcelona (Espa&ntilde;a)<br />Tel&eacute;fono: +34 304 00 32 - Fax: +34 550 44 66<br />Correo electr&oacute;nico: <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em></p>
        <p><b>TIMESAVERS</b>, es titular del  sitio web <em>www.timesavers.es</em> y responsable de los  ficheros generados con los datos de car&aacute;cter personal suministrados por los  usuarios a trav&eacute;s de este sitio web. </p>
        <p>2. <u>Tratamiento de los datos de car&aacute;cter personal</u>.</p>
        <p>El usuario de la web autoriza a <b>TIMESAVERS</b>, el tratamiento automatizado  de los datos personales que suministre voluntariamente, a trav&eacute;s de formularios  y correo electr&oacute;nico, para: </p>
        <p>-Presupuestos relacionados con los  productos y servicios de <b>TIMESAVERS</b>.<br />-Venta, env&iacute;o y gesti&oacute;n con relaci&oacute;n a los productos y  servicios de <b>TIMESAVERS </b>comprados  por el usuario en la web.<b></b></p>
        <p>Asimismo, le  informamos que sus datos podr&aacute;n ser utilizados para enviarle informaci&oacute;n sobre  nuestros productos, ofertas y servicios en servicios personales, salvo que  usted manifieste lo contrario indic&aacute;ndolo expresamente en el formulario  correspondiente.</p>
		<p>Asimismo, queda usted  informado que parte de la informaci&oacute;n que usted nos facilite puede ser  comunicada a terceras empresas y colaboradores que TIMESAVERS pueda  subcontratar con la &uacute;nica y exclusiva finalidad de poder llevar a cabo los  servicios por usted contratados. Puede manifestarse en el formulario  correspondiente, si no quiere que se conserven sus datos m&aacute;s tiempo del  legalmente exigido.</p>
		<p>Del mismo modo, si  TIMESAVERS lo considera oportuno, y salvo que usted se manifieste en contrario,  indic&aacute;ndolo expresamente en el formulario correspondiente, conservar&aacute; sus datos  m&aacute;s del tiempo legalmente exigido, aunque no contin&uacute;e existiendo una relaci&oacute;n  comercial entre usted y TIMESAVERS, para poder llevar un control de los  servicios llevados a cabo y, si usted as&iacute; lo desea, poder continuar envi&aacute;ndole  informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS.</p>
        
        <p>El fichero creado est&aacute; ubicado en la Ronda  Universitat, 15, 1&ordm; 1&ordf;, 08007 - Barcelona (Espa&ntilde;a) bajo la supervisi&oacute;n y  control de <b>TIMESAVERS</b> quien asume la  responsabilidad en la adopci&oacute;n de medidas de seguridad de &iacute;ndole t&eacute;cnica y  organizativa de Nivel B&aacute;sico para proteger la confidencialidad e integridad de  la informaci&oacute;n, de acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999  de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal y su normativa  de desarrollo.</p>
        <p>El usuario responder&aacute;, en cualquier caso,  de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reserv&aacute;ndose <b>TIMESAVERS</b> el derecho a excluir de  los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados,  inexactos, sin perjuicio de las dem&aacute;s acciones que procedan en Derecho. </p>
        <p>Cualquier usuario puede ejercitar sus  derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos de  car&aacute;cter personal suministrados a trav&eacute;s de la web <em>www.timesavers.es</em> o por correo  electr&oacute;nico, mediante comunicaci&oacute;n escrita dirigida a <b>TIMESAVERS </b>ubicada en la Ronda Universitat, 15, 1&ordm; 1&ordf;, 08007 -  Barcelona (Espa&ntilde;a) o al correo electr&oacute;nico <em>info@timesavers.es</em>. </p>
        <p>3. <u>Propiedad Intelectual e Industrial:</u> El nombre de dominio <em>www.timesavers.es</em> de esta p&aacute;gina web  es propiedad de <b>TIMESAVERS</b>. El sitio  web <em>www.timesavers.es</em> en su totalidad,  incluyendo su dise&ntilde;o, estructura, distribuci&oacute;n, textos, contenidos, logotipos,  botones, im&aacute;genes, dibujos, c&oacute;digo fuente, bases de datos, as&iacute; como todos los  derechos de propiedad intelectual e industrial y cualquier otro signo  distintivo, pertenecen a <b>TIMESAVERS</b>,<b> </b>o, en su caso, a las personas o empresas que  figuran como autores o titulares de los derechos y que han autorizado a su uso a <b>TIMESAVERS</b>.<b> </b></p>
        <p>Queda prohibida la reproducci&oacute;n o  explotaci&oacute;n total o parcial, por cualquier medio, de los contenidos del portal  de <b>TIMESAVERS</b> para usos diferentes  de la leg&iacute;tima informaci&oacute;n o contrataci&oacute;n por los usuarios de los servicios  ofrecidos. En tal sentido, usted no puede copiar, reproducir, recompilar,  descompilar, desmontar, distribuir, publicar, mostrar, ejecutar, modificar,  volcar, realizar trabajos derivados, transmitir o explotar partes del servicio,  exceptuando el volcado del material del servicio y/o hacer copia para su uso  personal no lucrativo, siempre y cuando usted reconozca todos los derechos de  autor y Propiedad Industrial. La modificaci&oacute;n del contenido de los servicios  representa una violaci&oacute;n a los leg&iacute;timos derechos de <b>TIMESAVERS</b>.</p>
        <p>4. <u>Responsabilidades</u>. <b>TIMESAVERS </b>no se responsabiliza de cualquier da&ntilde;o o  perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor,  como: error en las l&iacute;neas de comunicaciones, defectos en el Hardware y Software  de los usuarios, fallos en la red de Internet (de conexi&oacute;n, en las p&aacute;ginas  enlazadas).</p>
        <p>No se garantiza que el sitio web vaya a  funcionar constante, fiable y correctamente, sin retrasos o interrupciones.</p>
        <p><b>TIMESAVERS</b> se reserva el  derecho a modificar la presente pol&iacute;tica de privacidad con objeto de adaptarla  a las posibles novedades legislativas, as&iacute; como a las que pudieran derivarse de  los c&oacute;digos tipo existentes en la materia o por motivos estrat&eacute;gicos. <b>TIMESAVERS </b>declina cualquier  responsabilidad respecto a la informaci&oacute;n de esta web procedente de fuentes  ajenas a <b>TIMESAVERS.</b> </p>
        <p><b>TIMESAVERS</b> no se  responsabiliza del mal uso que se realice de los contenidos de su p&aacute;gina web,  siendo responsabilidad exclusiva de la persona que accede a ellos o los  utiliza. De igual manera, no es responsable de que menores de edad realicen  pedidos, rellenando el formulario con datos falsos, o haci&eacute;ndose pasar por un  miembro de su familia mayor de edad o cualquier tercero mayor de edad. Debiendo  de responder ante dicho hecho el mayor de edad bajo el cual se encuentra dicho  menor, teniendo <b>TIMESAVERS</b> siempre  la posibilidad de actuar por las v&iacute;as legales que considere oportunas con la  finalidad de sancionar dicho hecho. 
        En todo caso, <b>TIMESAVERS</b>, al momento de entrega del pedido, entregar&aacute; s&oacute;lo y  &uacute;nicamente a un mayor de edad, que deber&aacute; acreditar sus datos mediante  documento de identificaci&oacute;n legal. Tampoco asume responsabilidad alguna por la  informaci&oacute;n contenida en las p&aacute;ginas web de terceros a las que se pueda acceder  a trav&eacute;s de enlaces desde la p&aacute;gina <em>www.timesavers.es</em>. La presencia de  estos enlaces tiene una finalidad informativa, y no constituyen en ning&uacute;n caso  una invitaci&oacute;n a la contrataci&oacute;n de productos o servicios que se puedan ofrecer  en la p&aacute;gina web de destino. <b>TIMESAVERS</b> no responde ni se hace cargo de ning&uacute;n tipo de responsabilidad por los da&ntilde;os y  perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y  continuidad de los sitios enlazados. 
		De igual manera, <b>TIMESAVERS</b> por este medio comunica a quien le pueda interesar que  en caso de que un tercero este interesado en colocar como enlace de su p&aacute;gina  web la direcci&oacute;n de <b>TIMESAVERS</b>, &eacute;ste  deber&aacute; de comunicarse v&iacute;a correo electr&oacute;nico <em>info@timesavers.es</em> solicitando la autorizaci&oacute;n para tal  efecto. </p>
        <p>5. <u>Navegaci&oacute;n con cookies</u>. El web <em>www.timesavers.es</em> utiliza cookies,  peque&ntilde;os ficheros de datos que se generan en el ordenador del usuario y que nos  permiten conocer su frecuencia de visitas, los contenidos m&aacute;s seleccionados y  los elementos de seguridad que pueden intervenir en el control de acceso a &aacute;reas  restringidas, as&iacute; como la visualizaci&oacute;n de publicidad en funci&oacute;n de criterios  predefinidos por <b>TIMESAVERS </b>y que se  activan por cookies servidas por dicha entidad o por terceros que prestan estos  servicios por cuenta de <b>TIMESAVERS</b>.</p>
        <p>El usuario tiene la opci&oacute;n de impedir la  generaci&oacute;n de cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su  programa navegador.</p>
        <p>El usuario autoriza a <b>TIMESAVERS</b> a obtener y tratar la informaci&oacute;n que se genere como  consecuencia de la utilizaci&oacute;n del Web <em>www.timesavers.es</em>, con la &uacute;nica  finalidad de ofrecerle una navegaci&oacute;n m&aacute;s personalizada.</p>
        <p>6. <u>Seguridad.</u> Este sitio web en cuanto a la confidencialidad y tratamiento de datos de  car&aacute;cter personal, por un lado, responde a las medidas de seguridad de nivel  medio establecidas en virtud de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, sobre  Protecci&oacute;n de Datos de Car&aacute;cter Personal y su Reglamento de Medidas de  Seguridad vigente, y por otro lado responde,  en cuanto a las medidas t&eacute;cnicas a la instalaci&oacute;n de &ldquo;Antivirus y  filtros antispam&rdquo; y el Secure Socket Layers &ldquo;SSL&rdquo; como plataforma segura. Todo  lo anterior a sabiendas que en la red de redes la seguridad absoluta no existe.</p>

		<p>7. <u>Confirmaci&oacute;n  de Recepci&oacute;n Pedido.</u> Una vez recibido el pedido, TimeSavers enviar&aacute; por  correo electr&oacute;nico la confirmaci&oacute;n de recepci&oacute;n del pedido; posteriormente, y  en un plazo m&aacute;ximo de 24 horas, se enviar&aacute;, mediante correo electr&oacute;nico, la  propuesta, el presupuesto y el plazo de entrega del/los servicio/s  solicitado/s. Los pedidos efectuados los viernes, o v&iacute;spera de festivo, se  tramitar&aacute;n el siguiente d&iacute;a laborable (los d&iacute;as laborables no incluyen fines de  semana ni d&iacute;as festivos) al menos que indique que necesita una gesti&oacute;n  inmediata. Se aplicar&aacute; un incremento de 25% sobre las tarifas normales para  gestiones fuera del horario comercial de lunes a viernes de 10h a 20h.</p>
  		<p>8. <u>Formas  y condiciones de pago (impuestos).</u> En cuanto al pago, el usuario debe  elegir entre: tarjeta de cr&eacute;dito, transferencia bancaria y/o Paypal. En el caso  de compras y/o gestiones con un importe superior a 100&euro;, se deber&aacute; pagar un 50%  del importe por adelantado, con la observaci&oacute;n de que en ning&uacute;n caso nos  llegar&aacute; informaci&oacute;n sobre el numero de la tarjeta de cr&eacute;dito o cuenta, estos  datos pasan directamente a la entidad financiera, que se encargar&aacute; de  certificar la autenticidad de la tarjeta y realizar el cobro. Todo a sabiendas  que en la red de redes la seguridad absoluta no existe.</p>
  		<p>Todos los  precios reflejados en el portal <strong>www.timesavers.es</strong> incluyen  el IVA (16%). Los precios est&aacute;n sujetos a cambios sin previo aviso.</p>

        <p>9. <u>Plazos, Formas  y Gastos de env&iacute;o.</u><br />
          A partir del momento en el que sea efectiva  la recepci&oacute;n del pedido y se confirme el pago del mismo por parte del cliente,  se proceder&aacute; a la realizaci&oacute;n efectiva de la prestaci&oacute;n de los servicios  contratados en un plazo marcado en la confirmaci&oacute;n del pedido.</p>
        <p>En caso de no disponibilidad del producto o  servicio que el cliente haya solicitado a trav&eacute;s de la web <em>www.timesavers.es</em>, se proceder&aacute; a la  comunicaci&oacute;n del incidente al usuario en un m&aacute;ximo de 3 (tres) d&iacute;as, utilizando  como canal de comunicaci&oacute;n la direcci&oacute;n de correo electr&oacute;nico, o n&uacute;mero de  tel&eacute;fono de contacto, que el usuario nos haya facilitado a trav&eacute;s del  formulario de compra. En esta comunicaci&oacute;n se le informar&aacute; al cliente de la fecha  en la que el producto o servicio comprado estar&aacute; disponible o de la  imposibilidad de disponer del mismo. En caso de que el cliente no est&eacute; de  acuerdo con los nuevos plazos o sea imposible disponer del producto o servicio  contratados, se le dar&aacute; otra alternativa a la compra o se proceder&aacute; a la  devoluci&oacute;n del importe total de la misma a elecci&oacute;n del cliente.</p>
        <p>Antes de finalizar la compra y confirmar el  pedido, se podr&aacute; conocer la cuant&iacute;a de este concepto y, en caso de desearlo, el  cliente podr&aacute; desestimar el pedido.</p>
        <p>10. <u>Pago mediante tarjeta de cr&eacute;dito</u>.  (Repudio) </p>
        <p>Cuando el importe de una compra hubiese  sido cargado utilizando el n&uacute;mero de una tarjeta de cr&eacute;dito, sin que &eacute;sta  hubiese sido presentada directamente o identificada electr&oacute;nicamente, su titular  podr&aacute; exigir la inmediata anulaci&oacute;n del cargo. En tal caso, las  correspondientes anotaciones de adeudo y reabono en las cuentas del proveedor y  del titular se efectuar&aacute;n a la mayor brevedad.</p>
        <p>Sin embargo, si la compra hubiese sido efectivamente realizada por el titular  de la tarjeta y, por lo tanto, hubiese exigido indebidamente la anulaci&oacute;n del  correspondiente cargo, aqu&eacute;l quedar&aacute; obligado frente al vendedor al  resarcimiento de los da&ntilde;os y perjuicios ocasionados como consecuencia de dicha  anulaci&oacute;n. </p>
        <p>11. <u>Desistimiento.</u></p>
        <p>El comprador podr&aacute; desistir libremente del  contrato dentro del plazo de siete d&iacute;as contados desde la fecha de recepci&oacute;n  del producto. <br />El ejercicio del derecho o desistimiento no  estar&aacute; sujeto a formalidad alguna, bastando que se acredite, en cualquier forma  admitida en Derecho.<br />
        El derecho de desistimiento del comprador  no puede implicar la imposici&oacute;n de penalidad alguna, si bien, el comprador  deber&aacute; satisfacer los gastos directos de devoluci&oacute;n y en su caso, indemnizar  los desperfectos del objeto de la compra. </p>
        <p>Lo dispuesto anteriormente no ser&aacute; de  aplicaci&oacute;n a las ventas de objetos que puedan ser reproducidos o copiados con  car&aacute;cter inmediato, que se destinen a la higiene corporal o que, en raz&oacute;n de su  naturaleza, no puedan ser devueltos. </p>
        <p>12. <u>Devoluciones.</u></p>
        <p>En cuanto a las gestiones compradas a  TimeSavers, tienen una validez de 1 a&ntilde;o para ser utilizadas, aunque existe la  posibilidad de devoluci&oacute;n del importe de las gestiones compradas y NO  UTILIZADAS tras 30 dias naturales de haber realizado la compra de dichas  gestiones.<br />
        As&iacute; mismo, se pone en conocimiento del usuario  que algunos de los servicios ofrecidos por TimeSavers, ser&aacute;n subcontratados a  terceras empresas proveedoras de dichos servicios, con lo cual el usuario tendr&aacute;  el derecho a la devoluci&oacute;n de algunos de los productos finales en un plazo  m&aacute;ximo de diez (10) d&iacute;as tras la recepci&oacute;n del mismo, en virtud de la Ley de  Ordenamiento del Comercio Minorista. Pudiendo el usuario, solicitar la  sustituci&oacute;n del producto, la deducci&oacute;n del importe equivalente en su pr&oacute;ximo  pedido o el reembolso de su dinero, previa confirmaci&oacute;n por parte de <b>TIMESAVERS </b>de que no se ha utilizado,  da&ntilde;ado y/o quebrantado el producto. En cuanto a los dem&aacute;s productos (alimentos,  artesan&iacute;a) por su fragilidad y especial conservaci&oacute;n no se podr&aacute;n devolver,  excepto cuando el error se halla presentado por parte del vendedor.</p>
        <p><u>Proceso de devoluci&oacute;n:</u> el proceso de  devoluci&oacute;n es el siguiente: </p>
        <p>12.1. Escribir a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> solicitando un n&uacute;mero  de autorizaci&oacute;n de devoluci&oacute;n (se le da un No. de devoluci&oacute;n que determin&eacute; la  empresa, para llevar control de dichas actuaciones); 
        12.2. Hay que enviarle  junto con el pedido un formulario de devoluci&oacute;n adjunto, para que expliquen  motivos de la devoluci&oacute;n, productos a devolver (el producto s&oacute;lo se podr&aacute;  devolver en caso de da&ntilde;os en el mismo, equivocaci&oacute;n o no conformidad y en el  caso de productos alimenticios, artesan&iacute;as s&oacute;lo se podr&aacute; devolver por no  conformidad del comprador por error del vendedor y caso de da&ntilde;os en el mismo); 
        12.3.  Adjunto con el env&iacute;o del producto en su embalaje original, debidamente  protegido para su transporte, anexe copia de la factura m&aacute;s el formulario de  devoluci&oacute;n; 12.4. El reenv&iacute;o del producto ser&aacute; a su s&oacute;lo coste y deber&aacute;  enviarlo dentro de un plazo no superior a diez (10) d&iacute;as desde la recepci&oacute;n del  producto. Para el caso de que el producto este da&ntilde;ado o no sea lo que ha  solicitado y dicho error sea imputable al vendedor, todos los gastos correr&aacute;n  porcuenta de &eacute;ste.</p>
        <p>13. <u>Legislaci&oacute;n  y jurisdicci&oacute;n aplicable</u><u>.</u> Con car&aacute;cter general, las relaciones con los usuarios,  derivadas de la prestaci&oacute;n de servicios contenidos en esta p&aacute;gina web, con  renuncia expresa a cualquier fuero que pudiera corresponderles, se someten a la  legislaci&oacute;n y jurisdicci&oacute;n espa&ntilde;ola, espec&iacute;ficamente a los tribunales de la  ciudad de Barcelona (Espa&ntilde;a). Los usuarios de esta p&aacute;gina web son conscientes  de todo lo que se ha expuesto y lo aceptan voluntariamente.</p>");

define("EMP1","Une &eacute;quipe &agrave; votre service!");
define("EMP2","TimeSavers est constitu&eacute; d&rsquo;une &eacute;quipe de professionnels, de formation et de  m&eacute;tiers divers. Combinant la richesse de leurs exp&eacute;riences personnelles et leur  esprit d&rsquo;entreprise, ils poss&egrave;dent tous les atouts pour toujours trouver des solutions  rapides et adapt&eacute;es &nbsp;aux besoins des  membres TimeSavers, m&ecirc;me les plus exigeants.");
define("EMP3","Fondateur &amp; Managing Director est Christoph Kraemer, de double  nationalit&eacute; fran&ccedil;aise et allemande. Il est dipl&ocirc;m&eacute; de Yale University. Un vrai  citoyen du monde, il a v&eacute;cu entre autre &agrave; Berlin, &agrave; New York et au Cap. En  2002, il arrive &agrave; Barcelone. Apr&egrave;s l&rsquo;obtention d&rsquo;un MBA &agrave; l&rsquo;ESADE, il d&eacute;cide de  s&rsquo;installer dans cette ville. Le parcours professionnel de Christoph est marqu&eacute;  par des exp&eacute;riences diverses. Il a travaill&eacute; avec des artistes connus dans des  grandes maisons de disques, dirig&eacute; des &eacute;quipes internationales et des campagnes  de marketing pour une cha&icirc;ne europ&eacute;enne de joaillerie, et coordonn&eacute; la gestion  de projets pour une cha&icirc;ne de grands magasins de bricolage.");
define("EMP4","Fondateur &amp; Operations Director est Toni Delgado, Barcelonais de  naissance. Il est licenci&eacute; en Dessin de Mode de la CETE et de l&rsquo;&Eacute;cole  Sup&eacute;rieure de la Mode. Cosmopolite par vocation, il a v&eacute;cu &agrave; Paris, Madrid et  Barcelone. Sa carri&egrave;re professionnelle d&eacute;bute en travaillant pour des foires  internationales de mode et des cr&eacute;ateurs de la Haute Couture. Ces derni&egrave;res  ann&eacute;es, il s&rsquo;est reconverti dans le domaine de la logistique et des processus  de postproduction industrielle pour des entreprises, leaders dans leurs  secteurs.");
define("EMP5","Laissez-nous travailler pour vous et d&eacute;tendez-vous!");
define("EMP6","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> apporte des  solutions &agrave; vos besoins personnels, &agrave; la planification de vos vacances, &agrave;  l&rsquo;entretien de votre maison et de votre voiture, &agrave; la coordination du catering  pour votre prochaine f&ecirc;te, ou &agrave; l&rsquo;organisation d&rsquo;un service de chauffeur pour  aller chercher votre famille ou vos amis &agrave; l&rsquo;a&eacute;roport.</p>
<p>Chargez-nous d&rsquo;emmener votre voiture au garage, de  trouver et de livrer le cadeau appropri&eacute; pour votre partenaire ou simplement  relaxez-vous et nous vous enverrons un bon masseur.</p>
<p>Occupez-vous des t&acirc;ches plus productives pour vous-m&ecirc;me  et d&eacute;l&eacute;guez-nous le souci d&rsquo;aller chercher vos v&ecirc;tements au pressing, de  trouver un &laquo;baby-sitter&raquo; pour votre animal de compagnie. On vous trouvera aussi  le meilleur devis pour la r&eacute;novation de votre appartement, et si vous le  d&eacute;sirez, nous nous chargerons de trouver un top d&eacute;corateur d&rsquo;int&eacute;rieur et nous  superviserons les travaux.</p>
<p>Vous pourrez partir tranquillement en vacances sachant  que nous nous occuperons de votre maison. Nous ramasserons le courrier,  arroserons les plantes et nous nous soucierons de t&acirc;ches importantes lors de  votre absence. </p>
<p>Pour les services que <em>TimeSavers &ndash; Quality Personal Concierge</em> n&rsquo;effectue pas directement, nous travaillons exclusivement avec des  partenaires renomm&eacute;s, que nous avons s&eacute;lectionn&eacute;s m&eacute;ticuleusement. En outre,  nous &nbsp;proposons des prix int&eacute;ressants et  des offres exclusives pour les membres TimeSavers.</p>");

define("CT0","<p>Ante todo queremos agradecer su inter&eacute;s en <em>TimeSavers &ndash; Quality Personal Concierge</em>.  Le enviaremos una respuesta a su consulta con la mayor brevedad posible.</p>
	<p>Atentamente<br /><em>TimeSavers  &ndash; Quality Personal Concierge</em></p>
	<p style='font-size:10px;'>AVISO LEGAL: La informaci&oacute;n contenida en este  mensaje de correo electr&oacute;nico es confidencial y puede revestir el car&aacute;cter de  reservada. Est&aacute; destinada exclusivamente a su destinatario. El acceso o uso de  este mensaje, por parte de cualquier otra persona que no est&eacute; autorizada,  pueden ser ilegales. Si no es Ud. la persona destinataria, le rogamos que  proceda a eliminar su contenido. Conforme a la Ley Org&aacute;nica 15/1999, de 13 de  diciembre, le comunicamos que su direcci&oacute;n de correo electr&oacute;nico forma parte de  nuestro fichero automatizado con el objetivo de poder mantener el contacto con  Ud. Si desea oponerse, acceder, cancelar o rectificar sus datos, p&oacute;ngase en  contacto con TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. al tel&eacute;fono +34 93 304  00 32.</p>");
define("CT1A","<p>Chez <em>TimeSavers &ndash; Quality  Personal Concierge</em>, nous cherchons des partenaires d&eacute;di&eacute;s &agrave; un service sans  faille et avec des produits parfaits pour satisfaire les besoins de nos  membres.</p>
	<p>Plus qu&rsquo;une simple relation commerciale,  nous voulons &eacute;tablir une collaboration &eacute;troite pour d&eacute;velopper ensemble des produits  exclusifs qui inciteront nos membres &agrave; les utiliser. </p>
	<p>Int&eacute;ress&eacute;? Alors n&rsquo;h&eacute;sitez pas  &agrave; remplir le formulaire et nous nous mettrons imm&eacute;diatement en contact!</p>");
define("CT1B","Chez <em>TimeSavers &ndash; Quality  Personal Concierge</em>, nous sommes &agrave; votre disposition et nous nous engageons &agrave;  une transparence totale. S&rsquo;il vous pla&icirc;t, n&rsquo;h&eacute;sitez donc pas &agrave; nous contacter  et &agrave; nous communiquer vos questions et doutes. Nous nous ferons un plaisir de  vous r&eacute;pondre le plus rapidement possible.");
define("CT2","Les donn&eacute;es apport&eacute;es seront trait&eacute;es de mani&egrave;re confidentielle, conforme  avec notre <a href='http://www.timesavers.es/legal.php' target='_blank'>avis l&eacute;gal  et nos conditions d&rsquo;utilisation</a> et ce qu&rsquo;&eacute;tablit la Ley  Org&aacute;nica 15/99 de Protecci&oacute;n de Datos Personales.");
define("CT3","J&rsquo;ai lu l&lsquo;<a href='http://www.timesavers.es/legal.php' target='_blank'>avis  l&eacute;gal et les conditions d&rsquo;utilisation</a> et je suis d&rsquo;accord.");
define("CT4","Je ne voudrais pas recevoir d&rsquo;information concernant des  produits, offres ou services de la part de TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.");
define("CT5","Je ne voudrais pas que mes donn&eacute;es personnelles soient gard&eacute;es  plus longtemps que pr&eacute;vu par la l&eacute;gislation espagnole aupr&egrave;s de TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L.");
define("CT6","Je ne voudrais pas recevoir d&rsquo;information concernant des  produits, offres ou services de la part de TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L. une fois termin&eacute;e notre relation commerciale.");
define("CONT1","Pr&eacute;nom et Nom de Famille");
define("CONT2","Nom d'entreprise");
define("CONT3","Type de service/produit");
define("CONT6","Champs obligatoires");
define("CONT7","ENVOYER");

define("FAQ1","Pourquoi TimeSavers &ndash; Quality Personal Concierge?");
define("FAQ2","Y-a-t-il quelque chose que TimeSavers &ndash; Quality Personal Concierge ne  puisse pas organiser?");
define("FAQ3","TimeSavers appliquera  une augmentation de prix sur les tarifs de ses partenaires?");
define("FAQ4","Les commandes de dernier instant seront permises?");
define("FAQ5","Quel est la marche &agrave; suivre pour passer la commande d&rsquo;un  service?");
define("FAQ6","O&ugrave; est-ce que sera le champ d&rsquo;action de  TimeSavers?");
define("FAQ7","Puis-je contacter TimeSavers &agrave; n&rsquo;importe  quelle heure?");
define("FAQ8","TimeSavers est le fournisseur de tous les  services offerts?");
define("FAQ9","Quelles garanties ai-je si le service est sous-trait&eacute;?");
define("FAQ10","Comment faire pour devenir membre/r&eacute;silier mon abonnement?");
define("FAQ11","Une fois achet&eacute;s, quand dois-je utiliser les produits TimeSavers?");
define("FAQ12","Quelle est la politique de protection de donn&eacute;es de TimeSavers?");
define("FAQ13","Comment fonctionne TimeSavers?");

define("FAQ1_X","C&rsquo;est un service &agrave; un prix abordable  qui vous permet de d&eacute;l&eacute;guer des t&acirc;ches in&eacute;vitables pour lesquelles vous n&rsquo;avez  pas le temps ou la possibilit&eacute; de les accomplir vous-m&ecirc;me. En cons&eacute;quence, vous  r&eacute;duirez votre niveau de stress et augmenterez votre temps libre. <strong>TimeSavers  &ndash; Quality Personal Concierge</strong> s&rsquo;engage &agrave; r&eacute;aliser chaque commande selon vos  d&eacute;sirs et vos instructions&nbsp; &ndash; &agrave; moins que  nous puissions vous sugg&eacute;rer une meilleure solution. Nous esp&eacute;rons qu&rsquo;avec le  temps et en gagnant votre confiance, vous nous verrez comme une partie int&eacute;grale  de votre syst&egrave;me de gestion de vie.");
define("FAQ2_X","<em>TimeSavers  &ndash; Quality Personal Concierge</em> tentera  de satisfaire au maximum n&rsquo;importe quelle commande de nos membres pourvu  qu&rsquo;elle soit l&eacute;gale et &eacute;thique. Nous sommes une &eacute;quipe avec de vastes et  diverses exp&eacute;riences d&eacute;di&eacute;e &agrave; 100% &agrave;&nbsp;  trouver la solution pour chaque commande. Au cas o&ugrave; il nous arrive, par  exemple, une commande de derni&egrave;re heure pour une table un vendredi soir dans le  restaurant le plus &laquo;&nbsp;in&nbsp;&raquo; de la ville, nous ferons, &agrave; travers  notre&nbsp; r&eacute;seau &eacute;tendu de contacts, tout  notre possible pour vous r&eacute;server cette table. En cas d&rsquo;insucc&egrave;s, nous vous  proposerons une autre&nbsp; solution qui vous fera  au moins autant plaisir. ");
define("FAQ3_X","La r&eacute;ponse est un non cat&eacute;gorique. Nos membres profiteront  au moins des prix normaux de nos partenaires. En plus, nous chercherons des  accords avec nos partenaires pour vous offrir des rabais et des promotions.  Nous tiendrons aussi toujours compte du profil et des possibilit&eacute;s de chaque  membre &agrave; l&rsquo;heure de lui pr&eacute;senter une solution ad&eacute;quate.");
define("FAQ4_X","Il est &eacute;vident qu&rsquo;en nous donnant le plus d&rsquo;avance  possible, nous serons capables de mieux affiner et donc d&rsquo;am&eacute;liorer&nbsp; notre proposition. N&eacute;anmoins, nous essayerons  de satisfaire votre d&eacute;sir,&nbsp;  ind&eacute;pendamment du d&eacute;lai d&rsquo;avis. Au cas o&ugrave; nous ne pourrions pas  l&rsquo;accomplir, faites nous confiance, nous vous trouverons une solution &eacute;quivalente  ou meilleure!");
define("FAQ5_X","Envoyez-nous  votre demande de service par mail &agrave; <a href='mailto:info@timesavers.es'>info@timesavers.es</a>,  appelez-nous au +34 93 304 00 32, ou inscrivez-vous et acc&eacute;dez &agrave; la zone  &laquo;Mon TimeSavers&raquo;. 
  Nous vous proposerons une solution adapt&eacute;e &agrave; vos  besoins le plus rapidement possible. Au cas o&ugrave; le service d&eacute;sir&eacute; requiert le  concours d&rsquo;un de nos partenaires, le devis fera clairement la diff&eacute;rence entre  les co&ucirc;ts de notre partenaire et les TimeSavers Credits n&eacute;cessaires. Ce devis  devra &ecirc;tre approuv&eacute; par mail, t&eacute;l&eacute;phone ou votre compte &laquo;Mon  Timesavers&raquo; et pay&eacute; un 50% d&rsquo;avance (pour les montants sup&eacute;rieurs &agrave; 100  &euro;). Une fois r&eacute;alis&eacute; le service, <strong>TimeSavers  &ndash; Quality Personal Concierge</strong> vous enverra une facture, d&eacute;taillant le co&ucirc;t  du service et les TimeSavers Credits qui se sont r&eacute;ellement produits, moins le  payement d&rsquo;avance. 
  Pour la facture finale tout achat de forfait TimeSavers  Credits r&eacute;alis&eacute; avant le d&eacute;but du service en question est consid&eacute;r&eacute; et appliqu&eacute;  &agrave; la facture mentionn&eacute;e. Au cas o&ugrave; vous vous d&eacute;cidez &agrave; ne pas acheter de forfait  TimeSavers Credits nous vous facturerons le nombre exact de TimeSavers Credits  utilis&eacute;s pour le traitement du service.");
define("FAQ6_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> offre ses services dans la  ville de Barcelone.");
define("FAQ7_X","Notre horaire commercial est du lundi au vendredi, de 10h00  &agrave; 20h00. Cependant, il existe la possibilit&eacute; de vous servir 24h/24h en nous  appelant au +34 93 304 00 32. Une augmentation de 25% sur le tarif normal de  TimeSavers Credits sera appliqu&eacute;e sur les gestions en dehors de l&rsquo;horaire  commercial.");
define("FAQ8_X","<em>TimeSavers  &ndash; Quality Personal Concierge</em> s&rsquo;engage &agrave; ce que toutes ses prestations de service s&rsquo;effectuent sans faille.  Certains services seront g&eacute;r&eacute;s directement pour <em>TimeSavers &ndash; Quality Personal Concierge</em> et d&rsquo;autres seront  sous-trait&eacute;s &agrave; un partenaire. Tous les services externes seront r&eacute;alis&eacute;s par  des partenaires qualifi&eacute;s et test&eacute;s par nous-m&ecirc;mes.");
define("FAQ9_X","En ce qui concerne les garanties, <em>TimeSavers &ndash; Quality Personal Concierge</em> travaille avec une  assurance de responsabilit&eacute; civile (&laquo;seguro de responsabilidad  civil&raquo;), qui prot&egrave;ge toutes transactions, et exige la m&ecirc;me chose pour ses  partenaires.");
define("FAQ10_X","L&rsquo;abonnement s&rsquo;effectue &agrave; travers une cotisation unique  de 50,- &euro;. Ce montant sera d&eacute;duit des futurs achats de TimeSavers Credits.&nbsp; Nous vous demanderons des donn&eacute;es  personnelles d&eacute;taill&eacute;es pour mieux pouvoir vous conseiller et m&ecirc;me anticiper  vos besoins. <br />Vous pouvez r&eacute;silier votre abonnement &agrave; tout moment en  nous envoyant un e-mail ou par un simple click sur notre page internet. Les cr&eacute;dits  achet&eacute;s seront rembours&eacute;s jusqu&rsquo;&agrave; un d&eacute;lai de 30 jours ouvrables &agrave; partir de la  date d&rsquo;achat.");
define("FAQ11_X","&Agrave; partir de la date d&rsquo;achat tout TimeSavers Credits sera  valable pendant 1 an.");
define("FAQ12_X","Les donn&eacute;es personnelles fournies par le membre seront  inclues dans une base de donn&eacute;es, qui se conservera chez TimeSavers Quality  Concierge Service, S.L. de mani&egrave;re confidentielle et en accord avec la Ley  Org&aacute;nica de Protecci&oacute;n de Datos de Car&aacute;cter Personal n&ordm; 15/1.1999, del 13 de  diciembre de 1999, ainsi que pour toute autre avenant l&eacute;gal en vigueur. Le membre pourra exercer son droit  d&rsquo;acc&egrave;s, de rectification ou d&rsquo;annulation &agrave; n&rsquo;importe quel moment. Pour le  faire, veuillez bien nous contacter par t&eacute;l&eacute;phone ou par e-mail. Pour des  informations plus d&eacute;taill&eacute;s, s&rsquo;il vous pla&icirc;t, consultez notre politique de  protection de donn&eacute;es (&laquo;<a href='http://www.timesavers.es/legal.php'>Aviso  Legal &amp; Condiciones de Uso</a>&raquo;) ou adressez-vous directement &agrave; nous.");
define("FAQ13_X","<em>TimeSavers &ndash; Quality Personal  Concierge</em> fonctionne &agrave; base d&rsquo;un  syst&egrave;me de cr&eacute;dits, dans lequel l&rsquo;ex&eacute;cution d&rsquo;un service co&ucirc;te un certain  nombre de TimeSavers Credits. Ces cr&eacute;dits s&rsquo;ach&egrave;tent dans la zone &bdquo;Mon  TimeSavers&ldquo; une fois que vous &ecirc;tes inscrit comme membre TimeSavers. Vous  trouverez d&rsquo;avantage d&rsquo;information sur les TimeSavers Credits en cliquant <a href='http://www.timesavers.es/gestiones.php'>ici</a>.");

define("GEST0A","<em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> est bas&eacute; sur un syst&egrave;me de cr&eacute;dits, dans lequel le traitement de chaque  service exige un certain nombre de cr&eacute;dits. Les cr&eacute;dits s&rsquo;ach&egrave;tent dans  &laquo;Mon TimeSavers&raquo;, apr&egrave;s vous &ecirc;tre inscrit comme membreTimeSavers.
<br /><br />Chaque fois que vous demandez un certain service, nous  vous envoyons par mail et/ou &agrave; votre compte &laquo;Mon TimeSavers&raquo; un  devis d&eacute;taillant le nombre pr&eacute;vu de cr&eacute;dits pour son traitement. Il va de soi  que ces devis sont absolument gratuits. Dans &laquo;Mon TimeSavers &raquo; vous  avez aussi la possibilit&eacute; d&rsquo;acqu&eacute;rir des forfaits plus importants de TimeSavers  Credits &agrave; fin d&rsquo;obtenir un prix plus int&eacute;ressant par cr&eacute;dit. Le devis doit &ecirc;tre  approuv&eacute; par mail, t&eacute;l&eacute;phone ou &agrave; travers de &laquo;Mon TimeSavers&raquo; et  vous devrez payer 50% d&rsquo;avance (pour les montants d&eacute;passant 100 &euro;).
<br /><br />Si le service d&eacute;sir&eacute; requiert le concours d&rsquo;un de nos  partenaires, le devis fera clairement la diff&eacute;rence entre les co&ucirc;ts de notre  partenaire et les TimeSavers Credits n&eacute;cessaires. Une fois r&eacute;alis&eacute; le service, <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> vous enverra une facture, d&eacute;taillant le co&ucirc;t du service et les TimeSavers  Credits qui se sont r&eacute;ellement produits. Pour la facture finale tout achat de forfait  TimeSavers Credits r&eacute;alis&eacute; avant le d&eacute;but du service en question est consid&eacute;r&eacute;  et appliqu&eacute; &agrave; la facture mentionn&eacute;e. Au cas o&ugrave; vous vous d&eacute;cidez &agrave; ne pas  acheter de forfait TimeSavers Credits nous vous facturerons le nombre exact de  TimeSavers Credits utilis&eacute;s pour le traitement du service.
<br /><br />Tous les payements s&rsquo;effectuent directement par transfert  bancaire ou dans la zone de membres &laquo;Mon TimeSavers&raquo;.
<br /><br />Nous nous permettons de faire valoir que <em class='blanco'>TimeSavers &ndash;  Quality Personal Concierge</em> a n&eacute;goci&eacute; avec la quasi totalit&eacute; de ses partenaires  des avantages pour nos membres TimeSavers, que se soit une remise, une offre  sp&eacute;ciale ou un privil&egrave;ge exclusif.
<br /><br />Tous les prix sont incl. TVA.
<br /><br />Les TimeSavers Credits sont valables pendant une ann&eacute;e de  calendrier &agrave; partir de la date d&rsquo;achat.");
define("GEST0","<p><em>Rapide et simple&nbsp;:  Envoyez-nous votre demande de service par mail &agrave; <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, appelez-nous au +34 93  304 00 32 ou acc&eacute;dez &agrave; &laquo;&nbsp;Mon TimeSavers&nbsp;&raquo;. Nous vous r&eacute;pondrons avec  une proposition de solution faite sur mesure dans les plus brefs d&eacute;lais </em><br><br><b>Prix TimeSavers Credits</b><br>Veuillez trouver ci-dessous une  liste des diff&eacute;rents forfaits de TimeSavers Credits::</p>
	    <table cellpadding='4' cellspacing='1'>
		<tr class='filazul_1'><td>1  TimeSavers Credit</td><td>12,00 &euro;</td></tr>
		<tr class='filazul_2'><td>5  TimeSavers Credits</td><td>58,00 &euro;</td></tr>
		<tr class='filazul_1'><td>10  TimeSavers Credits</td><td>112,00 &euro;</td></tr>
		<tr class='filazul_2'><td>25  TimeSavers Credits</td><td>265,00 &euro;</td></tr>
		<tr class='filazul_1'><td>50  TimeSavers Credits</td><td>480,00 &euro;</td></tr>
		<tr class='filazul_2'><td>100 TimeSavers  Credits</td><td>840,00 &euro;</td></tr>
		</table>
	    <p>Devriez-vous &ecirc;tre int&eacute;ress&eacute; par  un forfait plus important de TimeSavers Credits, nous vous prions de bien  vouloir nous contacter par t&eacute;l&eacute;phone au +34 93 304 00 32 ou par mail &agrave; <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");
define("GEST1","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> vous offre la possibilit&eacute; d&rsquo;un abonnement  mensuel. Vous pouvez choisir entre les deux options pr&eacute;sent&eacute;es ou bien nous  pouvons dessiner un forfait sur mesure selon vos besoins particuliers. Les abonnements  TimeSavers Monthly offrent en prime les r&eacute;servations de restaurant et la  gestion de votre agenda personnel gratuitement, sans que vous ayez &agrave; renoncer a  quelconque avantage disponible aux autres membre TimeSavers.</p>
<p><b>TimeSavers Monthly Basic</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Jusqu'&agrave; 16 TimeSavers Credits</td><td>120,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. r&eacute;serves de restaurant et gestion agenda personnel gratuites.</td></tr>
<tr class='filazul_1'><td colspan='2'>Service client de lundi &agrave; vendredi de 10h &agrave; 20h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. frais de d&eacute;placement dans Barcelone.</td></tr>
</table>
<p><b>TimeSavers Monthly Premium</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Jusqu'&agrave; 40 TimeSavers Credits</td><td>250,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. r&eacute;serves de restaurant et gestion agenda personnel gratuites.</td></tr>
<tr class='filazul_1'><td colspan='2'>Service client 24h/24h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. frais de d&eacute;placement dans Barcelone.</td></tr>
</table>
<p>Les abonnements mensuels sont  valables 30 jours &agrave; partir de la date d&rsquo;achat. Les cr&eacute;dits non-utilis&eacute;s pendant  cette p&eacute;riode ne peuvent pas &ecirc;tre report&eacute;s sur des abonnements ult&eacute;rieurs.</p>
<p><b>Exemples</b><br>Pour que vous puissiez vous  faire une id&eacute;e, combien de TimeSavers Credits co&ucirc;te un certain service, voici  quelques exemples:</p>
<ul>
<li>1 r&eacute;serve de restaurant co&ucirc;te 1 TimeSavers Credit. Comme mentionn&eacute; auparavant, nous vous enverrons un devis par mail et/ou &agrave; votre compte &laquo;Mon TimeSavers&raquo;, que  vous devrez approuver par un de ces deux canaux ou par t&eacute;l&eacute;phone. &Eacute;tant donn&eacute; que le montant est inf&eacute;rieur &agrave; 100 &euro;, il n&rsquo;est pas n&eacute;cessaire de payer 50% d&rsquo;avance. Ensuite vous recevrez la facture qui faudra r&eacute;gler par transfert bancaire ou &agrave; travers de &laquo;Mon TimeSavers&raquo;. Nous avons convenu un boni pour nos membres avec tous les &eacute;tablissements avec lesquels nous collaborons. Laissez-vous surprendre! </li>
<li>La premi&egrave;re fois que nous vous envoyons un coiffeur chez vous, le co&ucirc;t sera de 2 TimeSavers Credits plus le co&ucirc;t du service du sp&eacute;cialiste. Nous vous ferons parvenir le devis  qui diff&eacute;renciera clairement entre les TimeSavers Credits n&eacute;cessaires pour le traitement du service et le co&ucirc;t probable du service selon les  informations proportionn&eacute;es. Le devis doit &ecirc;tre approuv&eacute; et 50% pay&eacute;d&rsquo;avance. Il se peut que sur place vous vous d&eacute;cidiez &agrave; ajouter un traitement de soin &agrave; la teinture command&eacute;e. Ne vous faites pas de soucis! Ceci ne repr&eacute;sente aucun probl&egrave;me. C&rsquo;est pr&eacute;cis&eacute;ment pour cette raison que la facture finale d&eacute;taille le nombre de TimeSavers Credits et le co&ucirc;t du service qui se sont r&eacute;ellement produits. 
De plus, si vous d&eacute;sirez &ecirc;tre resservi par ce m&ecirc;me sp&eacute;cialiste, <em>TimeSavers &ndash; Quality Personal Concierge</em> vous facturera comme une gestion d&rsquo;agenda  personnel au prix de 1 TimeSavers Credit. </li>
<li>La renouvellement du contr&ocirc;le technique de votre voiture dure normalement de 2 &agrave; 3 heures et, en cons&eacute;quence, co&ucirc;te 8 &agrave; 12 TimeSavers Credits. Avant de traiter cette  demande, nous vous envoyons un devis d&eacute;taillant la moyenne des TimeSavers Credits estim&eacute;s n&eacute;cessaire pour l&rsquo;ex&eacute;cution de ce service. Ensuite se r&eacute;p&egrave;te les pas connus de la r&eacute;ception et de l&rsquo;approbation de devis. Une fois le contr&ocirc;le technique pass&eacute;, nous comptabilisons les TimeSavers Credits r&eacute;ellement utilis&eacute;s et vous envoyons une facture, moins le montant du payement initial. Tous les payements se r&eacute;alisent par transfert bancaire ou dans la zone de membres &laquo;Mon TimeSavers&raquo;. </li>
</ul>
<p>Le devis est toujours calcul&eacute;  avec la moyenne de TimeSavers Credits estim&eacute;s n&eacute;cessaire pour g&eacute;rer le service.  N&eacute;anmoins, vous avez la possibilit&eacute; d&rsquo;acheter un forfait plus important de  TimeSavers Credits &agrave; fin d&rsquo;obtenir un prix plus int&eacute;ressant par cr&eacute;dit. Tout  achat effectu&eacute; avant le d&eacute;but du service en question sera consid&eacute;r&eacute; et appliqu&eacute; &agrave; la facture mentionn&eacute;. </p>");
define("GEST2","Apprenez &agrave; conna&icirc;tre Barcelone  d&rsquo;une autre mani&egrave;re et tirez le maximum de votre s&eacute;jour ici! <em>TimeSavers &ndash;  Quality Personal </em>offre une vaste gamme d&rsquo;exp&eacute;rience diff&eacute;rentes et  d&rsquo;aventures uniques aux visiteurs de Barcelone. Par ailleurs nous proposons d&rsquo;&eacute;laborer un forfait fait sur mesure tenant en compte vos besoins particuliers.
<br />Si vous &ecirc;tes int&eacute;ress&eacute;, nous  vous prions de bien vouloir nous contacter en appelant le +34 93 304 00 32,  nous envoyer un mail &agrave; <a href='mailto:info@timesavers.es'>info@timesavers.es</a> ou de cliquez <a href='files/timesavers_aviso_visit.pdf' target='_blank'>ici</a>.");
define("GEST3","Pourquoi ne pas faire un cadeau  v&eacute;ritablement pratique? Trouver un cadeau captivant et innovateur peut vite se  transformer en quelque chose de stressant. <em>TimeSavers &ndash; Quality Personal Concierge</em> vous offre la possibilit&eacute; de s&rsquo;en charger pour vous. Cela dit,  pourquoi ne pas surprendre votre partenaire, la famille ou un ami avec un bon &laquo;TimeSavers Coupon;&raquo;?
<br />Lors de l&rsquo;achat de forfait  TimeSavers Credits, vous avez l&rsquo;occasion de choisir l&rsquo;option &laquo;TimeSavers Coupon &raquo;. Communiquez-nous l&rsquo;adresse ou le mail du destinataire et nous  lui enverrons un bon qui ne montrera que le nombre de TimeSavers Credits.");
define("GEST4","Selon une &eacute;tude Reuter, nous d&eacute;dions 10% de notre  temps de travail &agrave; g&eacute;rer des t&acirc;ches personnelles ou domestiques. <en>TimeSavers  &ndash; Quality Personal Concierge</em> peut servir d&rsquo;outil pour que vos employ&eacute;s  puissent concilier au mieux vie professionnelle et vie priv&eacute;e. En utilisant  notre service, vos employ&eacute;s auront moins de soucis et de stress, ce qui r&eacute;sultera  en un engagement accru sur le lieu de travail. En effet, une &eacute;tude PriceWaterhouseCoopers  d&eacute;montre que les entreprises avec les meilleures conditions de travail atteignent  un profit de 99 centimes pour chaque euro investit en Ressources Humaines,  quand la moyenne des entreprises europ&eacute;ennes ne gagne que 14 centimes pour le  m&ecirc;me investissement.
<br /><br />En plus, <em>TimeSavers &ndash; Quality Personal Concierge</em>  peut &ecirc;tre un excellent moyen pour gagner ou fid&eacute;liser des clients VIP.");

define("SER_1","Recherche internet");
define("SER_2","Organisation  d&icirc;ner");
define("SER_3","Catering");
define("SER_4","Attente livraison");
define("SER_5","Promeneur de chien");
define("SER_6","Demarches bureaucratiques");
define("SER_7","Organisation d&rsquo;evenement");
define("SER_8","Agenda personnel");
define("SER_9","Personal shopper");
define("SER_10","Organisation voyage");
define("SER_11","Reservation restaurant");
define("SER_12","Reservation billet");
define("SER_13","Service repondeur");
define("SER_14","Achat cadeaux");
define("SER_15","Demenagement");
define("SER_16","Babysitter");
define("SER_17","Garde clefs maison");
define("SER_18","Chauffeur");
define("SER_19","Ramassage  courrier");
define("SER_20","Service coursier");
define("SER_21","Pedicure / manicure");
define("SER_22","Coiffeur");
define("SER_23","Nutritioniste");
define("SER_24","Entra&icirc;neur  personnel");
define("SER_25","Spa");
define("SER_26","Masseur");
define("SER_27","Consultant d&rsquo;image");
define("SER_28","Decoration interieure &amp; exterieure");
define("SER_29","Plombier");
define("SER_30","Ma&ccedil;on");
define("SER_31","Electricien");
define("SER_32","Peintre");
define("SER_33","Serrurier");
define("SER_34","Service menage");
define("SER_35","Lessive");
define("SER_36","Repassage");
define("SER_37","Lavage voiture");
define("SER_38","Jardinier");
define("SER_39","Menage apres diner/fete");
define("SER_40","Pressing");
define("SER_41","Soin animal de compagnie");

define("SER0","Si vous &ecirc;tes int&eacute;ress&eacute; par un service qui ne figure pas dans notre liste,  n&rsquo;h&eacute;sitez pas &agrave; nous appeler au +34 93 304 00 32 ou &agrave; nous envoyer un mail &agrave; <a href='mailto:info@timesavers.es' class='blanco'><b>info@timesavers.es</b></a>. Nous vous promettons  que nous vous pr&eacute;senterons une solution dans les plus brefs d&eacute;lais.");
define("SER1","<b>RECHERCHE INTERNET</b><br />Le filtrage de millions de r&eacute;sultats pour trouver les bonnes informations  peut &ecirc;tre long et fatigant. Nous avons des experts qui peuvent vous d&eacute;charger  de ce travail.");
define("SER2","<b>ORGANISATION  D&Icirc;NER</b><br />Vous attendez un groupe d&rsquo;invit&eacute;s et  il vous manque la vaisselle, les verres, la d&eacute;coration de table et le talent  d&rsquo;un chef d&lsquo;&eacute;toile Michelin? Permettez-nous de tout organiser et savourez  le menu et la compagnie de vos invit&eacute;s.");
define("SER3","<b>CATERING</b><br />La cuisine est un cauchemar pour  vous? Nous collaborons avec des chefs r&eacute;put&eacute;s et d&rsquo;excellents services de  catering pour que vous n&rsquo;ayez pas &agrave; vous faire de souci.");
define("SER4","<b>ATTENTE LIVRAISON</b><br />Si vous avez d&eacute;j&agrave; d&ucirc; attendre la  livraison, par exemple d&rsquo;une nouvelle t&eacute;l&eacute;vision, vous saurez que normalement  on vous donne un d&eacute;lai de 4 &agrave; 6 heures. Que faire si c&rsquo;est une journ&eacute;e normale  de travail pour vous? Si vous ne pouvez ou ne voulez pas prendre un jour  de libre, nous serons enchant&eacute;s d&rsquo;attendre cette livraison pour que vous ne  perdiez pas de temps.");
define("SER5","<b>PROMENEUR DE CHIEN</b><br />Votre meilleur ami a besoin de se  d&eacute;fouler dans le parc? Nous travaillons avec des professionnels qui s&rsquo;en  chargeront.");
define("SER6","<b>DEMARCHES BUREAUCRATIQUES</b><br />Les d&eacute;marches bureaucratiques sont  souvent fastidieuses. D&eacute;l&eacute;guez-les-nous! Nous mettons &agrave; votre disposition un  personnel qualifi&eacute; avec des ann&eacute;es d&rsquo;exp&eacute;rience.");
define("SER7","<b>ORGANISATION D&rsquo;EVENEMENT</b><br />Devez-vous organiser un anniversaire  important ou un &eacute;v&eacute;nement comm&eacute;moratif, peut-&ecirc;tre m&ecirc;me un mariage, et il vous  manque le temps n&eacute;cessaire? Notre &eacute;quipe professionnelle est pr&eacute;par&eacute;e pour  assurer que l&rsquo;&eacute;v&eacute;nement soit inoubliable.");
define("SER8","<b>AGENDA PERSONNEL</b><br />Se rappeler tous les meetings,  anniversaires et &eacute;v&eacute;nements peut &ecirc;tre stressant. Chargez-nous de la gestion de  votre agenda personnel et vous n&rsquo;oublierez plus jamais un rendez-vous  important.");
define("SER9","<b>PERSONAL SHOPPER</b><br />Faire du shopping est pour vous une torture? Ou n&rsquo;avez-vous  simplement pas le temps pour le faire? Ou peut-&ecirc;tre vous aimeriez avoir  un autre avis pour renouveler votre garde-robe? Nous coop&eacute;rons avec des experts  qui vous conseillerons et vous  accompagnerons, ou effectuerons vos achats.");
define("SER10","<b>ORGANISATION VOYAGE</b><br />Trouver pour des vacances de r&ecirc;ve  les meilleurs vols, un h&ocirc;tel agr&eacute;able, des excursions int&eacute;ressantes peut s&rsquo;av&eacute;rer  plus difficile que pr&eacute;vu. Contentez-vous de faire vos valises et nous occupons  du reste.");
define("SER11","<b>RESERVATION RESTAURANT</b><br />Aimeriez-vous d&eacute;couvrir un nouveau  restaurant ou avez-vous des probl&egrave;mes  avec une r&eacute;servation de derni&egrave;re minute?  Choisissez dans notre liste d&rsquo;&eacute;tablissements.");
define("SER12","<b>RESERVATION BILLET</b><br />Constatez-vous des difficult&eacute;s au  moment de d&eacute;nicher les billets pour le spectacle auquel aimerait tant assister votre  partenaire? Soyez assur&eacute; que nous tenterons tout pour les obtenir!");
define("SER13","<b>SERVICE REPONDEUR</b><br />Avez-vous besoin de quelqu&rsquo;un pour  r&eacute;pondre &agrave; des appels en votre absence? Nous offrons un service de r&eacute;pondeur  pour filtrer et transmettre seulement  les appels importants.");
define("SER14","<b>ACHAT CADEAUX</b><br />Parfois il est difficile de rep&eacute;rer le bon cadeau pour quelqu&rsquo;un. Faites-nous  confiance, nous vous le trouverons.");
define("SER15","<b>DEMENAGEMENT</b><br />Un d&eacute;m&eacute;nagement est fatigant et  demande beaucoup de temps. Nous serons ravis d&rsquo;en prendre en main  l&rsquo;organisation pour r&eacute;duire au maximum votre peine.");
define("SER16","<b>BABYSITTER</b><br />Aimeriez-vous surprendre votre  partenaire avec un d&icirc;ner romantique sans devoir vous faire de souci pour votre  enfant? Confiez-le &agrave; une de nos Babysitters qualifi&eacute;es.");
define("SER17","<b>GARDE CLEFS MAISON</b><br />Si vous avez d&eacute;j&agrave; perdu une fois vos  clefs de maison, vous saurez qu&rsquo;un serrurier peut vous couter cher. Nous  offrons d&rsquo;en garder un double en cas d&rsquo;urgence.");
define("SER18","<b>CHAUFFEUR</b><br />Votre famille arrive d&rsquo;ici peu &agrave; l&rsquo;a&eacute;roport  et vous n&rsquo;avez pas le temps d&rsquo;aller les chercher vous-m&ecirc;me. Vous avez un d&icirc;ner  entre amis et vous ne voulez pas, en rentrant chez vous, tomber dans un  contr&ocirc;le d&rsquo;alcool&eacute;mie. Pas de probl&egrave;me, nous vous envoyons un chauffeur pour  que vous puissiez vous d&eacute;tendre.");
define("SER19","<b>RAMASSAGE  COURRIER</b><br />Si vous ne voulez pas que votre  bo&icirc;te &agrave; lettres d&eacute;borde pendant vos vacances, nous envoyons quelqu&rsquo;un pour  ramasser le courrier et le garder en votre absence.");
define("SER20","<b>SERVICE COURSIER</b><br />Devez-vous envoyer quelque chose  rapidement dans Barcelone? Nous passons prendre le paquet et le livrons &agrave;  votre destinataire.");
define("SER21","<b>PEDICURE / MANICURE</b><br />Accordez-vous  un peu de luxe et faites appel &agrave; ce service. Nous vous envoyons une  professionnelle.");
define("SER22","<b>COIFFEUR</b><br />Une urgence? Une r&eacute;union importante  impr&eacute;vue? Ou avez-vous besoin de quelqu&rsquo;un qui vous coupe les cheveux &agrave;  domicile? Un de nos expert r&eacute;soudra ce probl&egrave;me.");
define("SER23","<b>NUTRITIONISTE</b><br />Laissez-vous conseiller par un sp&eacute;cialiste  dipl&ocirc;m&eacute; que ce soit pour r&eacute;cup&eacute;rer la taille parfaite pour l&rsquo;&eacute;t&eacute; ou am&eacute;liorer  votre alimentation.");
define("SER24","<b>ENTRA&Icirc;NEUR  PERSONNEL</b><br />N&rsquo;avez-vous pas le temps d&rsquo;aller en salle de  sport? Avez-vous besoin d&rsquo;un programme personnalis&eacute;? Prenez contact avec nous  et nous vous enverrons un entra&icirc;neur professionnel.");
define("SER25","<b>SPA</b><br />Pourquoi ne pas d&eacute;connecter un moment pour  vous d&eacute;tendre? Allongez-vous et savourez que quelqu&rsquo;un s&rsquo;occupe de vous. Nous vous  donnons acc&egrave;s aux meilleurs &eacute;tablissements de Barcelone.");
define("SER26","<b>MASSEUR</b><br />Sympt&ocirc;me de stress? Douleurs locales? Ou  souhaitez-vous un traitement th&eacute;rapeutique? Un sp&eacute;cialiste sait traiter tous  ces cas et vous permettra de vous sentir mieux.");
define("SER27","<b>CONSULTANT D&rsquo;IMAGE</b><br />Devez-vous assister &agrave; un &eacute;v&eacute;nement sp&eacute;cial  et vous manquez d&rsquo;assurance en ce qui concerne votre pr&eacute;sentation, ou avez-vous  tout simplement envie de changer de look? Notre &eacute;quipe vous conseille et  se charge de votre bien-&ecirc;tre.");
define("SER28","<b>DECORATION INTERIEURE &amp; EXTERIEURE </b><br />Avez-vous  besoin de quelques conseils pour d&eacute;corer votre maison, jardin ou terrasse? Ou  voulez-vous que nous nous occupons de la r&eacute;novation? Choisissez l&rsquo;option  ad&eacute;quate et r&eacute;jouissez-vous du r&eacute;sultat.");
define("SER29","<b>PLOMBIER</b><br />En cas  d&rsquo;urgence quand il n&rsquo;y a pas de temps &agrave; perdre, nous mettons &agrave; votre  disposition des professionnels pour solutionner rapidement le probl&egrave;me.");
define("SER30","<b>MA&Ccedil;ON</b><br />Les petites  r&eacute;parations &agrave; la maison ne doivent pas constituer un probl&egrave;me. Nous vous  cherchons une solution et vous trouvons le ma&ccedil;on ad&eacute;quat.");
define("SER31","<b>ELECTRICIEN</b><br />Confiez seulement &agrave; un sp&eacute;cialiste l&rsquo;installation &eacute;lectrique de votre maison.  Nous vous envoyons quelqu&rsquo;un.");
define("SER32","<b>PEINTRE</b><br />Oubliez les heures interminables que dure  peindre une maison soi-m&ecirc;me. Nous vous faisons appel &agrave; un professionnel et nous assurons que vous  obtenez le r&eacute;sultat d&eacute;sir&eacute;.");
define("SER33","<b>SERRURIER</b><br />N&rsquo;h&eacute;sitez  pas en cas d&rsquo;urgence. Rapidit&eacute; et efficacit&eacute; sont la r&eacute;ponse. S&rsquo;il vous pla&icirc;t,  contactez-nous.");
define("SER34","<b>SERVICE MENAGE</b><br />Peu importe si vous avez besoin d&rsquo;un service continu ou occasionnel, nous  vous proposons un professionnel  comp&eacute;tent.");
define("SER35","<b>LESSIVE</b><br />Chargez-nous de votre lessive. Nous vous garantissons un service  rapide, un traitement et des r&eacute;sultats impeccables.");
define("SER36","<b>REPASSAGE</b><br />Laissez-nous prendre en main votre  repassage. Vous ne serez pas d&eacute;&ccedil;us.");
define("SER37","<b>LAVAGE VOITURE</b><br />Nous nous chargons aussi bien d&rsquo;un lavage normal que d&rsquo;un nettoyage &agrave;  fond pour assurer que votre voiture continue &agrave; fonctionner sans probl&egrave;me.");
define("SER38","<b>JARDINIER</b><br />Confiez le soin de vos plantes &agrave; un  expert qui sait aussi traiter les petits probl&egrave;mes qui peuvent se produire.");
define("SER39","<b>MENAGE APRES DINER/FETE</b><br />Oubliez le jour d&rsquo;apr&egrave;s. Nous nous  occupons de tout le nettoyage afin que vous n&rsquo;ayez &agrave; vous souvenir que des bons  moments de la f&ecirc;te.");
define("SER40","<b>PRESSING</b><br />N&rsquo;avez-vous jamais le temps de  porter vos v&ecirc;tements au pressing? Nous nous chargeons de trouver la solution et  de vous rendre vos v&ecirc;tements en &eacute;tat impeccable.");
define("SER41","<b>SOIN ANIMAL DE COMPAGNIE</b><br />Confiez votre animal de compagnie aux meilleurs. Des sp&eacute;cialistes  qualifi&eacute;s s&rsquo;en occuperont comme s&rsquo;il &eacute;tait le leur.");
?>