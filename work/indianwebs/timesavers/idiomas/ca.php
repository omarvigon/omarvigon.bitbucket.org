<?
define("FECHA_1","Data");
define("FECHA_DIA","dia");
define("FECHA_MES","mes");
define("FECHA_ANO","any");
define("MES_1","Gener");
define("MES_2","Febrer");
define("MES_3","Mar&ccedil;");
define("MES_4","Abril");
define("MES_5","Maig");
define("MES_6","Juny");
define("MES_7","Julio");
define("MES_8","Agost");
define("MES_9","Setembre");
define("MES_10","Octubre");
define("MES_11","Novembre");
define("MES_12","Desembre");

define("BT1","Sobre<br />TimeSavers");
define("BT2","Serveis");
define("BT2A","Estalvis de temps");
define("BT2B","Cura Personal");
define("BT2C","Millora de la LLar");
define("BT2D","Cura de la LLar");
define("BT3A","Credits");
define("BT3B","Preus");
define("BT3C","Gesti&oacute;");
define("BT4","Premsa");
define("BT4A","Not&iacute;cies TimeSavers");
define("BT4B","TimeSavers als medis");
define("BT5A","Contacte");
define("BT5B","Contacte Prove&iuml;dors");
define("BT6","Av&iacute;s legal i condicions d'&uacute;s ");
define("BT7","Contrasenya oblidada");
define("BT8A","Qu&egrave;&nbsp;fa TimeSavers?");
define("BT8B","Qui son TimeSavers?");
define("BT9","Registri's");
define("BT10","Entra");
define("BT11","CANVIAR IMATGE");
define("XHORA","Hora");
define("XMAPA","Veure el mapa");
define("XCOMPRAR","Comprar");
define("XCANCEL","Rechazar");

define("REG_INICIO","<p>&iexcl;Bienvenido a <em class='blanco'>TimeSavers - Quality Personal Concierge</em>! Est&aacute; a punto de entrar en una nueva etapa de su vida, en la que esperamos poder reducir su nivel de estr&eacute;s y aumentar su tiempo libre.</p>
		<p>La cuota de alta es de 50,- &euro;, reembolsable con la compra de TimeSavers Credits.</p>
		<p>A continuaci&oacute; le pedimos  unos datos personales.</p>");
define("REG_ELEGIR","(Escollir)");
define("REG_TRATA","Tractament");
define("REG_TRATA_1","Sr.");
define("REG_TRATA_2","Sra.");
define("REG_TRATA_3","Dr.");
define("REG_NOMBRE","Nom");
define("REG_APELLIDOS","Cognoms");
define("REG_SEXO","Sexe");
define("REG_SEXO_1","Home");
define("REG_SEXO_0","Dona");
define("REG_NACIMIENTO","Data de Naixement");
define("REG_NACIONALIDAD","Nacionalitat");
define("REG_DOCUMENTO","Document d'identitat");
define("REG_DOCNUM","N&ordm; de document");
define("REG_DIRECCION","Adre&ccedil;a");
define("REG_POBLACION","Poblaci&oacute;");
define("REG_CP","C.P.");
define("REG_TEL","Tel&eacute;fon");
define("REG_MOVIL","Mobil");
define("REG_IDIOMA","Idima preferit comunicaci&oacute;");
define("REG_MODO","Modus preferit comunicaci&oacute;");
define("REG_CONOCIO","Com ens ha conegut ?");
define("REG_CONCIO_1","Prove&iuml;dor");
define("REG_CONCIO_2","Amic");
define("REG_CONCIO_3","Anunci");
define("REG_CONCIO_4","Follet");
define("REG_CONCIO_5","Televisi&oacute;");
define("REG_CONCIO_6","Altre");
define("REG_COMENTA","Comentaris");
define("REG_FOTO","La seva foto: (Format JPG)");
define("REG_CONTRA","Contrasenya");
define("REG_CONTRA2","Cambiar Contrasenya");
define("REG_CONTRA3","Rebr&agrave; la seva contrasenya per e-mail registrada");
define("REG_MODIF","Modificar");
define("REG_BAJADAR","QUIERO DARME DE BAJA");
define("REG_BAJA","Es pot donar de baixa en qualsevol moment enviant-nos un correu a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, trucant al +34 93 304 0032 o a trav&eacute;s d'un simple click en el seu compte. Productes comprats i no utilitzats es reemborsaran fins a un termini de 30 dies a partir de la data de compra.");
define("REG_REGISTRAR","REGISTRAR");
define("REG_DESCRIP","Descripci&oacute;");
define("REG_LUGAR","Lloc");
define("REG_DIFERENTE","si &eacute;s diferent del registrat ");
define("REG_CARRITO","AFEGIR AL CARRITO");
define("REG_FECHASER","Data desitjat de servei");
define("REG_CODIGO","Si disposa d'un codi de promoci&oacute;,  introdu&iuml;u aqu&iacute;. El descompte s'aplicar&agrave; a la seva primera compra d'TimeSavers  Credits.");
define("REG_FIN","&iexcl;Benvingut a <em>TimeSavers - Quality Personal Concierge</em>!<br /><br />Se li ha enviat la contrasenya al compte");

define("OLVIDO","<h3>Ha Oblidat les seves dades</h3>
        <p>En cas que no recordeu el  seu e-mail de registre / usuari, poseu-vos en contacte amb nosaltres per  tel&egrave;fon al +34 93 304 00 32 o per e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a></p>");

define("USER_MI","El meu Timesavers");
define("USER_PEDIDO","Sol&middot;licitar servici");
define("USER_HISTORIAL_1","Presupost");
define("USER_HISTORIAL_2","Facturas");
define("USER_PEDIR","REALITZAR COMANDA");
define("USER_SALDO_1","TimeSavers<br />Credits<br />utilizados");
define("USER_SALDO_2","Saldo<br />TimeSavers<br />Credits");
define("USER_ACCION","Accions");
define("USER_IMPORTE","Import");
define("USER_DETALLES","Detall");
define("USER_DATOS","Dades Personales");
define("USER_NOTICIAS","Noticies");
define("USER_RECOMENDA","Recomendacions");
define("USER_RECOMENDA_2","Tipo de recomendacio");
define("USER_RECOMENDA_10","Servicio deseado");
define("USER_RECOMENDA_20","Servicio ofrecido");
define("USER_RECOMENDA_30","Incidencia solicitud");
define("USER_RECOMENDA_40","Forma de trabajo");
define("USER_RECOMENDA_3","RECOMENDAR");
define("USER_RECOMENDACION","<p>En <em>TimeSavers - Quality Personal Concierge</em> valoramos su opini&oacute;n. La m&aacute;xima satisfacci&oacute;n de nuestros miembros es nuestro objetivo primordial. Por supuesto, la mejor manera de conseguirlo es de escucharle y de pedirle su colaboraci&oacute;n.</p>
	<p>Con esta finalidad hemos puesto a su disposici&oacute;n este formulario de sugerencias. Conf&iacute;e en que le contactaremos con absoluta inmediatez.</p>
	<p>Desde luego, nos puede tambi&eacute;n contactar por tel&eacute;fono al n&uacute;mero <b>+34 93 304 00 32</b> o enviarnos un e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> para hablar directamente con uno de nuestros asesores.</p>");

define("IND_VIDEO","<object width='220' height='220'><param name='allowfullscreen' value='true' /><param name='allowscriptaccess' value='always' /><param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=1787718&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' /><embed src='http://vimeo.com/moogaloop.swf?clip_id=1787718&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='220' height='220'></embed></object>
			<p><a href='http://vimeo.com/1787718?pg=embed&amp;sec=1787718' target='_blank'>Benvinguts a TimeSavers - Quality Personal Concierge</a>.</p>");
define("IND0","Benvinguts a TimeSavers - Quality Personal Concierge");
define("IND1","<p>Segons la Acad&eacute;mie Fran&ccedil;aise, la paraula &ldquo;concierge&rdquo; es va originar de la paraula &ldquo;cumcerges&rdquo; del segle XII, significant &ldquo;guardi&agrave;&rdquo;. Es defineix com una persona encarregada de cuidar un immoble, edifici p&uacute;blic o palau. </p>
	<p>Els &quot;concierge services&quot;  moderns es van iniciar, on si no, a Nova York, la ciutat que mai dorm , als  anys 80. Responien a la necessitat de la gent de conciliaci&oacute; de la vida  personal i laboral, a m&eacute;s de poder gaudir de m&eacute;s temps lliure.</p>
	<p>Des de llavors el concepte ha anat conquistant el m&oacute;n i ara, per fi, <em>TimeSavers &ndash; Quality Personal Concierge</em> proporciona aquest servei a Barcelona.</p>");
define("IND2","Imagini&acute;s&nbsp;una vida sense estr&egrave;s!");
define("IND3","<p>TimeSavers &ndash; Quality Personal Concierge ha nascut amb vocaci&oacute; de simplificar-li la vida, aconseguint d'aquesta manera que gaudeixi de m&eacute;s temps d'oci, elevant aix&iacute; la seva qualitat de vida i la de la seva fam&iacute;lia. </p>
	<p>A TimeSavers &ndash; Quality Personal Concierge som prove&iuml;dors de temps, perqu&egrave; ho dediqui al que m&eacute;s l'interessa. </p>
	<p>La vida quotidiana a la ciutat, les gestions di&agrave;ries, les cites, compres, tr&agrave;mits burocr&agrave;tics lents, i mai suficient temps per a fer-ho tot. </p>
	<p>Imagini&acute;s una vida on pugui gaudir del seu propi conserge o assistent personal, sempre a la seva disposici&oacute; amb una trucada&nbsp;o un simple clic? </p>
	<p>A&nbsp;TimeSavers &ndash; Quality Personal Concierge&nbsp;comptem amb professionals efica&ccedil;os i discrets per a fer realitat aquest somni del propi assistent personal.</p>");
define("IND4","&iquest;Consulti el nostre cat&agrave;leg de productes per a visitants fent click aqu&iacute;!");

define("LIN1","<p>Aqu&iacute; trobaran links d'inter&egrave;s general, p&agrave;gines que ens agraden personalment i/o llocs que podrien emocionar-li.</p>
		<p style='font-size:10px;'>Timesavers &ndash; Quality Personal Concierge no es responsabilitza per la informaci&oacute; continguda en les p&agrave;gines web de tercers a les quals es pugui accedir a trav&eacute;s d'aquests enlla&ccedil;os.</p>");
define("LIN2","En construcci&oacute;...");

define("LEG1","<p>Esta  p&aacute;gina web es propiedad de <b>TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L. </b>en lo adelante, <b>TIMESAVERS,</b> el hecho de acceder a esta p&aacute;gina implica el  conocimiento y aceptaci&oacute;n de la pol&iacute;tica de privacidad. Esta pol&iacute;tica de  privacidad forma parte de las condiciones y t&eacute;rminos de usos.<br />
          En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio,  de Servicios de la Sociedad  de la Informaci&oacute;n y Comercio Electr&oacute;nico (LSSICE) se exponen los datos  identificativos.</p>");
define("LEG2","<p><b>Condiciones  de Uso y Pol&iacute;tica de Privacidad </b></p>
        <p>1. <u>Datos Identificativos de la Empresa</u></p>
        <p>Identidad y Direcci&oacute;n:</p>
        <p>Empresa: TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.<br />CIF: B64806862 - Nacionalidad: Espa&ntilde;ola <br />Domicilio: Ronda Universitat, 15, 1&ordm; 1&ordf;,  08007 - Barcelona (Espa&ntilde;a)<br />Tel&eacute;fono: +34 304 00 32 - Fax: +34 550 44 66<br />Correo electr&oacute;nico: <em>info@timesavers.es</em> </p>
        <p><b>TIMESAVERS</b>, es titular del  sitio web <em>www.timesavers.es</em> y responsable de los  ficheros generados con los datos de car&aacute;cter personal suministrados por los  usuarios a trav&eacute;s de este sitio web. </p>
        <p>2. <u>Tratamiento de los datos de car&aacute;cter personal</u>.</p>
        <p>El usuario de la web autoriza a <b>TIMESAVERS</b>, el tratamiento automatizado  de los datos personales que suministre voluntariamente, a trav&eacute;s de formularios  y correo electr&oacute;nico, para: </p>
        <p>-Presupuestos relacionados con los  productos y servicios de <b>TIMESAVERS</b>.<br />-Venta, env&iacute;o y gesti&oacute;n con relaci&oacute;n a los productos y  servicios de <b>TIMESAVERS </b>comprados  por el usuario en la web.<b></b></p>
        <p>Asimismo, le  informamos que sus datos podr&aacute;n ser utilizados para enviarle informaci&oacute;n sobre  nuestros productos, ofertas y servicios en servicios personales, salvo que  usted manifieste lo contrario indic&aacute;ndolo expresamente en el formulario  correspondiente.</p>
		<p>Asimismo, queda usted  informado que parte de la informaci&oacute;n que usted nos facilite puede ser  comunicada a terceras empresas y colaboradores que TIMESAVERS pueda  subcontratar con la &uacute;nica y exclusiva finalidad de poder llevar a cabo los  servicios por usted contratados. Puede manifestarse en el formulario  correspondiente, si no quiere que se conserven sus datos m&aacute;s tiempo del legalmente exigido.</p>
		<p>Del mismo modo, si  TIMESAVERS lo considera oportuno, y salvo que usted se manifieste en contrario,  indic&aacute;ndolo expresamente en el formulario correspondiente, conservar&aacute; sus datos  m&aacute;s del tiempo legalmente exigido, aunque no contin&uacute;e existiendo una relaci&oacute;n  comercial entre usted y TIMESAVERS, para poder llevar un control de los  servicios llevados a cabo y, si usted as&iacute; lo desea, poder continuar envi&aacute;ndole  informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS.</p>
		
        <p>El fichero creado est&aacute; ubicado en la Ronda  Universitat, 15, 1&ordm; 1&ordf;, 08007 - Barcelona (Espa&ntilde;a) bajo la supervisi&oacute;n y  control de <b>TIMESAVERS</b> quien asume la  responsabilidad en la adopci&oacute;n de medidas de seguridad de &iacute;ndole t&eacute;cnica y  organizativa de Nivel B&aacute;sico para proteger la confidencialidad e integridad de  la informaci&oacute;n, de acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999  de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal y su normativa  de desarrollo.</p>
        <p>El usuario responder&aacute;, en cualquier caso,  de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reserv&aacute;ndose <b>TIMESAVERS</b> el derecho a excluir de  los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados,  inexactos, sin perjuicio de las dem&aacute;s acciones que procedan en Derecho. </p>
        <p>Cualquier usuario puede ejercitar sus  derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos de  car&aacute;cter personal suministrados a trav&eacute;s de la web <em>www.timesavers.es</em> o por correo  electr&oacute;nico, mediante comunicaci&oacute;n escrita dirigida a <b>TIMESAVERS </b>ubicada en la Ronda Universitat, 15, 1&ordm; 1&ordf;, 08007 -  Barcelona (Espa&ntilde;a) o al correo electr&oacute;nico <em>info@timesavers.es</em>. </p>
        <p>3. <u>Propiedad Intelectual e Industrial:</u> El nombre de dominio <em>www.timesavers.es</em> de esta p&aacute;gina web  es propiedad de <b>TIMESAVERS</b>. El sitio  web <em>www.timesavers.es</em> en su totalidad,  incluyendo su dise&ntilde;o, estructura, distribuci&oacute;n, textos, contenidos, logotipos,  botones, im&aacute;genes, dibujos, c&oacute;digo fuente, bases de datos, as&iacute; como todos los  derechos de propiedad intelectual e industrial y cualquier otro signo  distintivo, pertenecen a <b>TIMESAVERS</b>,<b> </b>o, en su caso, a las personas o empresas que  figuran como autores o titulares de los derechos y que han autorizado a su uso a <b>TIMESAVERS</b>.<b> </b></p>
        <p>Queda prohibida la reproducci&oacute;n o  explotaci&oacute;n total o parcial, por cualquier medio, de los contenidos del portal  de <b>TIMESAVERS</b> para usos diferentes  de la leg&iacute;tima informaci&oacute;n o contrataci&oacute;n por los usuarios de los servicios  ofrecidos. En tal sentido, usted no puede copiar, reproducir, recompilar,  descompilar, desmontar, distribuir, publicar, mostrar, ejecutar, modificar,  volcar, realizar trabajos derivados, transmitir o explotar partes del servicio,  exceptuando el volcado del material del servicio y/o hacer copia para su uso  personal no lucrativo, siempre y cuando usted reconozca todos los derechos de  autor y Propiedad Industrial. La modificaci&oacute;n del contenido de los servicios  representa una violaci&oacute;n a los leg&iacute;timos derechos de <b>TIMESAVERS</b>.</p>
        <p>4. <u>Responsabilidades</u>. <b>TIMESAVERS </b>no se responsabiliza de cualquier da&ntilde;o o  perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor,  como: error en las l&iacute;neas de comunicaciones, defectos en el Hardware y Software  de los usuarios, fallos en la red de Internet (de conexi&oacute;n, en las p&aacute;ginas  enlazadas).</p>
        <p>No se garantiza que el sitio web vaya a  funcionar constante, fiable y correctamente, sin retrasos o interrupciones.</p>
        <p><b>TIMESAVERS</b> se reserva el  derecho a modificar la presente pol&iacute;tica de privacidad con objeto de adaptarla  a las posibles novedades legislativas, as&iacute; como a las que pudieran derivarse de  los c&oacute;digos tipo existentes en la materia o por motivos estrat&eacute;gicos. <b>TIMESAVERS </b>declina cualquier  responsabilidad respecto a la informaci&oacute;n de esta web procedente de fuentes  ajenas a <b>TIMESAVERS.</b> </p>
        <p><b>TIMESAVERS</b> no se  responsabiliza del mal uso que se realice de los contenidos de su p&aacute;gina web,  siendo responsabilidad exclusiva de la persona que accede a ellos o los  utiliza. De igual manera, no es responsable de que menores de edad realicen  pedidos, rellenando el formulario con datos falsos, o haci&eacute;ndose pasar por un  miembro de su familia mayor de edad o cualquier tercero mayor de edad. Debiendo  de responder ante dicho hecho el mayor de edad bajo el cual se encuentra dicho  menor, teniendo <b>TIMESAVERS</b> siempre  la posibilidad de actuar por las v&iacute;as legales que considere oportunas con la  finalidad de sancionar dicho hecho. 
        En todo caso, <b>TIMESAVERS</b>, al momento de entrega del pedido, entregar&aacute; s&oacute;lo y  &uacute;nicamente a un mayor de edad, que deber&aacute; acreditar sus datos mediante  documento de identificaci&oacute;n legal. Tampoco asume responsabilidad alguna por la  informaci&oacute;n contenida en las p&aacute;ginas web de terceros a las que se pueda acceder  a trav&eacute;s de enlaces desde la p&aacute;gina <em>www.timesavers.es</em>. La presencia de  estos enlaces tiene una finalidad informativa, y no constituyen en ning&uacute;n caso  una invitaci&oacute;n a la contrataci&oacute;n de productos o servicios que se puedan ofrecer  en la p&aacute;gina web de destino. <b>TIMESAVERS</b> no responde ni se hace cargo de ning&uacute;n tipo de responsabilidad por los da&ntilde;os y  perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y  continuidad de los sitios enlazados. 
		De igual manera, <b>TIMESAVERS</b> por este medio comunica a quien le pueda interesar que  en caso de que un tercero este interesado en colocar como enlace de su p&aacute;gina  web la direcci&oacute;n de <b>TIMESAVERS</b>, &eacute;ste  deber&aacute; de comunicarse v&iacute;a correo electr&oacute;nico <em>info@timesavers.es</em> solicitando la autorizaci&oacute;n para tal  efecto. </p>
        <p>5. <u>Navegaci&oacute;n con cookies</u>. El web <em>www.timesavers.es</em> utiliza cookies,  peque&ntilde;os ficheros de datos que se generan en el ordenador del usuario y que nos  permiten conocer su frecuencia de visitas, los contenidos m&aacute;s seleccionados y  los elementos de seguridad que pueden intervenir en el control de acceso a &aacute;reas  restringidas, as&iacute; como la visualizaci&oacute;n de publicidad en funci&oacute;n de criterios  predefinidos por <b>TIMESAVERS </b>y que se  activan por cookies servidas por dicha entidad o por terceros que prestan estos  servicios por cuenta de <b>TIMESAVERS</b>.</p>
        <p>El usuario tiene la opci&oacute;n de impedir la  generaci&oacute;n de cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su  programa navegador.</p>
        <p>El usuario autoriza a <b>TIMESAVERS</b> a obtener y tratar la informaci&oacute;n que se genere como  consecuencia de la utilizaci&oacute;n del Web <em>www.timesavers.es</em>, con la &uacute;nica  finalidad de ofrecerle una navegaci&oacute;n m&aacute;s personalizada.</p>
        <p>6. <u>Seguridad.</u> Este sitio web en cuanto a la confidencialidad y tratamiento de datos de  car&aacute;cter personal, por un lado, responde a las medidas de seguridad de nivel  medio establecidas en virtud de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, sobre  Protecci&oacute;n de Datos de Car&aacute;cter Personal y su Reglamento de Medidas de  Seguridad vigente, y por otro lado responde,  en cuanto a las medidas t&eacute;cnicas a la instalaci&oacute;n de &ldquo;Antivirus y  filtros antispam&rdquo; y el Secure Socket Layers &ldquo;SSL&rdquo; como plataforma segura. Todo  lo anterior a sabiendas que en la red de redes la seguridad absoluta no existe.</p>
        <p>7. <u>Confirmaci&oacute;n  de Recepci&oacute;n Pedido.</u> Una vez recibido el pedido, TimeSavers enviar&aacute; por  correo electr&oacute;nico la confirmaci&oacute;n de recepci&oacute;n del pedido; posteriormente, y  en un plazo m&aacute;ximo de 24 horas, se enviar&aacute;, mediante correo electr&oacute;nico, el  presupuesto y el plazo de entrega del/los servicio/s solicitado/s. Los pedidos  efectuados los viernes, o v&iacute;spera de festivo, se tramitar&aacute;n el siguiente d&iacute;a  laborable (los d&iacute;as laborables no incluyen fines de semana ni d&iacute;as festivos).</p>
        <p>8. <u>Formas y  condiciones de pago (impuestos).</u> En cuanto al pago, el usuario debe elegir  entre: tarjeta de cr&eacute;dito, transferencia bancaria y/o Paypal.En el  caso de compras y/o gestiones con un importe superior a 1000&euro; , se aplicar&aacute; una  preautorizaci&oacute;n por este importe ( consultar condiciones Cyberpack &ldquo;La Caixa&rdquo;  ), con la observaci&oacute;n de que en ning&uacute;n caso nos llegar&aacute; informaci&oacute;n sobre el  numero de la tarjeta de cr&eacute;dito o cuenta, estos datos pasan directamente a la  entidad financiera, que se encargar&aacute; de certificar la autenticidad de la  tarjeta y realizar el cobro.Todo a sabiendas que en la red de redes la  seguridad absoluta no existe.</p>
        <p>Todos los precios  reflejados en el portal <em>www.timesavers.es</em> incluyen el IVA (16%). Los precios est&aacute;n sujetos a cambios sin  previo aviso.</p>
        <p>9. <u>Plazos, Formas  y Gastos de env&iacute;o.</u><br />
          A partir del momento en el que sea efectiva  la recepci&oacute;n del pedido y se confirme el pago del mismo por parte del cliente,  se proceder&aacute; a la realizaci&oacute;n efectiva de la prestaci&oacute;n de los servicios  contratados en un plazo marcado en la confirmaci&oacute;n del pedido.</p>
        <p>En caso de no disponibilidad del producto o  servicio que el cliente haya solicitado a trav&eacute;s de la web <em>www.timesavers.es</em>, se proceder&aacute; a la  comunicaci&oacute;n del incidente al usuario en un m&aacute;ximo de 3 (tres) d&iacute;as, utilizando  como canal de comunicaci&oacute;n la direcci&oacute;n de correo electr&oacute;nico, o n&uacute;mero de  tel&eacute;fono de contacto, que el usuario nos haya facilitado a trav&eacute;s del  formulario de compra. En esta comunicaci&oacute;n se le informar&aacute; al cliente de la fecha  en la que el producto o servicio comprado estar&aacute; disponible o de la  imposibilidad de disponer del mismo. En caso de que el cliente no est&eacute; de  acuerdo con los nuevos plazos o sea imposible disponer del producto o servicio  contratados, se le dar&aacute; otra alternativa a la compra o se proceder&aacute; a la  devoluci&oacute;n del importe total de la misma a elecci&oacute;n del cliente.</p>
        <p>Antes de finalizar la compra y confirmar el  pedido, se podr&aacute; conocer la cuant&iacute;a de este concepto y, en caso de desearlo, el  cliente podr&aacute; desestimar el pedido.</p>
        <p>10. <u>Pago mediante tarjeta de cr&eacute;dito</u>.  (Repudio) </p>
        <p>Cuando el importe de una compra hubiese  sido cargado utilizando el n&uacute;mero de una tarjeta de cr&eacute;dito, sin que &eacute;sta  hubiese sido presentada directamente o identificada electr&oacute;nicamente, su titular  podr&aacute; exigir la inmediata anulaci&oacute;n del cargo. En tal caso, las  correspondientes anotaciones de adeudo y reabono en las cuentas del proveedor y  del titular se efectuar&aacute;n a la mayor brevedad.</p>
        <p>Sin embargo, si la compra hubiese sido efectivamente realizada por el titular  de la tarjeta y, por lo tanto, hubiese exigido indebidamente la anulaci&oacute;n del  correspondiente cargo, aqu&eacute;l quedar&aacute; obligado frente al vendedor al  resarcimiento de los da&ntilde;os y perjuicios ocasionados como consecuencia de dicha  anulaci&oacute;n. </p>
        <p>11. <u>Desistimiento.</u></p>
        <p>El comprador podr&aacute; desistir libremente del  contrato dentro del plazo de siete d&iacute;as contados desde la fecha de recepci&oacute;n  del producto. <br />El ejercicio del derecho o desistimiento no  estar&aacute; sujeto a formalidad alguna, bastando que se acredite, en cualquier forma  admitida en Derecho.<br />
        El derecho de desistimiento del comprador  no puede implicar la imposici&oacute;n de penalidad alguna, si bien, el comprador  deber&aacute; satisfacer los gastos directos de devoluci&oacute;n y en su caso, indemnizar  los desperfectos del objeto de la compra. </p>
        <p>Lo dispuesto anteriormente no ser&aacute; de  aplicaci&oacute;n a las ventas de objetos que puedan ser reproducidos o copiados con  car&aacute;cter inmediato, que se destinen a la higiene corporal o que, en raz&oacute;n de su  naturaleza, no puedan ser devueltos. </p>
        <p>12. <u>Devoluciones.</u></p>
        <p>En cuanto a las gestiones compradas a  TimeSavers, tienen una validez de 1 a&ntilde;o para ser utilizadas, aunque existe la  posibilidad de devoluci&oacute;n del importe de las gestiones compradas y NO  UTILIZADAS tras 30 dias naturales de haber realizado la compra de dichas  gestiones.<br />
        As&iacute; mismo, se pone en conocimiento del usuario  que algunos de los servicios ofrecidos por TimeSavers, ser&aacute;n subcontratados a  terceras empresas proveedoras de dichos servicios, con lo cual el usuario tendr&aacute;  el derecho a la devoluci&oacute;n de algunos de los productos finales en un plazo  m&aacute;ximo de diez (10) d&iacute;as tras la recepci&oacute;n del mismo, en virtud de la Ley de  Ordenamiento del Comercio Minorista. Pudiendo el usuario, solicitar la  sustituci&oacute;n del producto, la deducci&oacute;n del importe equivalente en su pr&oacute;ximo  pedido o el reembolso de su dinero, previa confirmaci&oacute;n por parte de <b>TIMESAVERS </b>de que no se ha utilizado,  da&ntilde;ado y/o quebrantado el producto. En cuanto a los dem&aacute;s productos (alimentos,  artesan&iacute;a) por su fragilidad y especial conservaci&oacute;n no se podr&aacute;n devolver,  excepto cuando el error se halla presentado por parte del vendedor.</p>
        <p><u>Proceso de devoluci&oacute;n:</u> el proceso de  devoluci&oacute;n es el siguiente: </p>
        <p>12.1. Escribir a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> solicitando un n&uacute;mero  de autorizaci&oacute;n de devoluci&oacute;n (se le da un No. de devoluci&oacute;n que determin&eacute; la  empresa, para llevar control de dichas actuaciones); 
        12.2. Hay que enviarle  junto con el pedido un formulario de devoluci&oacute;n adjunto, para que expliquen  motivos de la devoluci&oacute;n, productos a devolver (el producto s&oacute;lo se podr&aacute;  devolver en caso de da&ntilde;os en el mismo, equivocaci&oacute;n o no conformidad y en el  caso de productos alimenticios, artesan&iacute;as s&oacute;lo se podr&aacute; devolver por no  conformidad del comprador por error del vendedor y caso de da&ntilde;os en el mismo); 
        12.3.  Adjunto con el env&iacute;o del producto en su embalaje original, debidamente  protegido para su transporte, anexe copia de la factura m&aacute;s el formulario de  devoluci&oacute;n; 12.4. El reenv&iacute;o del producto ser&aacute; a su s&oacute;lo coste y deber&aacute;  enviarlo dentro de un plazo no superior a diez (10) d&iacute;as desde la recepci&oacute;n del  producto. Para el caso de que el producto este da&ntilde;ado o no sea lo que ha  solicitado y dicho error sea imputable al vendedor, todos los gastos correr&aacute;n  porcuenta de &eacute;ste.</p>
        <p>13. <u>Legislaci&oacute;n  y jurisdicci&oacute;n aplicable</u><u>.</u> Con car&aacute;cter general, las relaciones con los usuarios,  derivadas de la prestaci&oacute;n de servicios contenidos en esta p&aacute;gina web, con  renuncia expresa a cualquier fuero que pudiera corresponderles, se someten a la  legislaci&oacute;n y jurisdicci&oacute;n espa&ntilde;ola, espec&iacute;ficamente a los tribunales de la  ciudad de Barcelona (Espa&ntilde;a). Los usuarios de esta p&aacute;gina web son conscientes  de todo lo que se ha expuesto y lo aceptan voluntariamente.</p>");

define("EMP1","Un equip al seu servei!");
define("EMP2","<em>TimeSavers &ndash; Quality Personal Concierge</em> el forma un equip professional, procedent de diferents &agrave;mbits laborals i formatius, que sumats al bagatge personal d'experi&egrave;ncies l'han preparat &agrave;mpliament per servir-lo com a conserge o assistent personal i afrontar qualsevol de les necessitats dels nostres membres m&eacute;s exigents.");
define("EMP3","Founder &amp; Managing Director Christoph Kraemer, de nacionalitat francesa i alemanya, es&nbsp;graduat per L&acute;Universitat Yale. Un ciutad&agrave; genu&iacute; del m&oacute;n, ell ha viscut, per exemple, a Berl&iacute;n, Nova York i Ciutat del Cap. Christoph parla l'angl&egrave;s fluid, el franc&egrave;s, l'alemany i l'espanyol. En 2002 arriba a Barcelona per a estudiar&nbsp;un MBA a ESADE i, despr&eacute;s de la graduaci&oacute;, decideix instal&middot;lar-se Barcelona. En aquest punt, els camins de Christoph Kraemer i Toni Delgado es creuen. Els antecedents professionals de Christoph s&oacute;n marcats per un alt grau de flexibilitat adquirida en&nbsp;posicions tan diferents com treballant amb artistes famosos a discogr&aacute;fiques de renom, equips principals multinacionals i campanyes publicit&agrave;ries per a una cadena de botigues de joieria, aix&iacute; com la gesti&oacute; de projecte de coordinaci&oacute; per a Grans superficies de DIY.");
define("EMP4","Founder &amp; Operations Director Toni Delgado, un nadiu de Barcelona, &eacute;s un llicenciat de moda disseny de CETE i &Eacute;cole Sup&eacute;rieure de la Mode de Paris. Un cosmopolita per vocaci&oacute;, ha viscut a Par&iacute;s, Madrid i Barcelona i parla, a m&eacute;s a m&eacute;s d'espanyol i catal&agrave;, franc&egrave;s i angl&egrave;s. La seva carrera professional l'ha pres de col&middot;laborar de prop en fires de moda internacionals i amb dissenyadors d&acute;Haute Couture per concentrar-se m&eacute;s tard en log&iacute;stica i processos postproducci&oacute; industrials per a companyies i empreses importants, l&iacute;ders en el seu sector industrial. El resultat de tals experi&egrave;ncies professionals i personals clares &eacute;s un equip|grup equilibrat i complementari m&eacute;s que qualificat per encarar-se amb qualsevol de les necessitats dels nostres membres m&eacute;s exigents.");
define("EMP5","Deixin&acute;s&nbsp;treballar i descansi!");
define("EMP6","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> d&oacute;na solucions a les seves necessitats personals: l'organitzaci&oacute; d'un viatge, el manteniment de casa seva, la planificaci&oacute; de la seva festa d'aniversari.</p>
<p>Permeti que actuem com el seu assistent o conserge personal, que ens encarreguem de portar el seu cotxe a la revisi&oacute;, que busquem un regal adequat i el lliurem, o simplement relaxi&acute;s, li trobarem un bon massatgista. </p>
<p>Ocupi&acute;s de tasques m&eacute;s productives, proposi que nosaltres li recollim la roba de la tintoreria, li trobem un passejador per al seu gos, li sol&middot;licitem i optimitzem els pressuposts de les obres de reforma de casa seva, i a m&eacute;s li presentarem un interiorista si ho necessita.</p>
<p>Podr&agrave; marxar de vacances tranquil, la seva llar estar&agrave; atesa, li recollirem el correu, ens cuidarem de les seves plantes o atendrem gestions en la seva abs&egrave;ncia.Per als serveis que no proporcionem nosaltres mateixos, treballem amb prove&iuml;dors qualificats i testats per nosaltres cont&iacute;nuament. </p>
<p>A m&eacute;s podr&agrave; gaudir de tarifes i promocions exclusives per als membres de <em>TimeSavers &ndash; Quality Personal Concierge</em>.</p>");

define("CT0","<p>Abans de tot  volem agrair el seu inter&egrave;s en TimeSavers - Quality Personal Concierge. Us enviarem una resposta a la seva consulta  amb la major brevetat possible.<br />
<br />Atentament <br />TimeSavers - Quality Personal Concierge</p>");
define("CT1A","<p>En <em>TimeSavers &ndash; Quality Personal Concierge</em> vam buscar prove&iuml;dors dedicats a un servei excel&middot;lent i amb productes impecables per a satisfer les necessitats dels nostres membres.</p>
	<p>Mes que una simple relaci&oacute; comercial volem establir una estreta col&middot;laboraci&oacute; i desenvolupar junts productes exclusius que incentivaran els nostres membres a utilitzar-los.</p>
	<p>Interessat? Llavors no dubti a omplir el formulari i ens posarem en contacte el m&eacute;s aviat possible!.</p>");
define("CT1B","A <em>TimeSavers &ndash; Quality Personal Concierge</em> estem a la seva disposici&oacute; i apostem per la total transpar&egrave;ncia. Aix&iacute; que no dubti en contactar-nos i exposar-nos els seus dubtes o consultes. Estarem encantats d'atendre-li amb la m&agrave;xima celeritat.");
define("CT2","Les dades que ens aporta seran tractades de manera confidencial, conforme  a la nostra <a href='legal.php'>Pol&iacute;tica de Privacitat</a> i a l'establert en la Llei  Org&agrave;nica 15/99 de Protecci&oacute; de Dades Personals.");
define("CT3","He llegit i accepto <a href='legal.php'>l'Av&iacute;s Legal i les Condicions d'&Uacute;s</a> de la p&agrave;gina web de TimeSavers &ndash; Quality Personal Concierge.");
define("CT4","No desitjo rebre informaci&oacute; sobre els productes, ofertes i serveis de  TIMESAVERS QUALITY CONCIERGE SERVICE, S.L.");
define("CT5","No desitjo que les meves dades siguin conservats en TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. m&eacute;s temps del legalment exigit.");
define("CT6","No desitjo rebre informaci&oacute; sobre els productes, ofertes i serveis de TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. una vegada finalitzada la nostra relaci&oacute; comercial.");
define("CONT1","Nom I cognoms");
define("CONT2","Nom de l'Empresa");
define("CONT3","Tipus de servei/producte");
define("CONT6","Camps obligatoris");
define("CONT7","ENVIEU");

define("FAQ1","Per qu&egrave; TimeSavers?");
define("FAQ2","Hi ha alguna cosa que TimeSavers no pot organitzar?");
define("FAQ3","TimeSavers incrementa les tarifes dels seus prove&iuml;dors?");
define("FAQ4","S&acute;admeten comandes de darrera hora?");
define("FAQ5","Quins s&oacute;n els passos per fer una comanda?");
define("FAQ6","On presta els seus serveis TimeSavers ?");
define("FAQ7","Puc contactar amb TimeSavers a qualsevol hora?");
define("FAQ8","Es TimeSavers el prove&iuml;dor de tots els serveis?");
define("FAQ9","Quines garanties tinc si el servei ho fa un prove&iuml;dor extern?");
define("FAQ10","Com em puc donar d'alta/baixa?");
define("FAQ11","Un cop comprats,quant de temps tinc per fer server els productes TimeSavers ?");
define("FAQ12","Quina &egrave;s la politica de privacitat de TimeSavers ?");
define("FAQ13","Com funciona TimeSavers?");

define("FAQ1_X","&Eacute;s un servei assequible que li permetr&agrave; delegar tasques necess&agrave;ries que no t&eacute; el temps o la possibilitat de fer V&egrave;. mateix. En conseq&uuml;&egrave;ncia, podr&agrave; reduir el seu nivell d'estr&egrave;s i augmentar el seu temps lliure. TimeSavers &ndash; Quality Personal Concierge es compromet a executar cada comanda segons els desitjos i instruccions dels seus membres tret que tinguem suggeriments de millora. Esperem que amb el temps i guanyant la seva confian&ccedil;a ens vegi com un part integral del seu sistema de gesti&oacute; de la vida.");
define("FAQ2_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> intentar&agrave; satisfer al m&agrave;xim qualsevol desig dels seus membres mentre que sigui legal i moral. Som un equip amb un una &agrave;mplia i diversa experi&egrave;ncia dedicats a 100% a buscar la soluci&oacute; a tota comanda. En el cas que arribi, per exemple, una comanda d'&uacute;ltima hora per a una taula en un dels restaurants de moda per a divendres nit, farem, a trav&eacute;s de la nostra extensa xarxa de contactes, tot el possible per a aconseguir la seva taula. I en cas de no assolir-lo, li presentarem amb una soluci&oacute; que li encantar&agrave; d'igual manera.");
define("FAQ3_X","La resposta &eacute;s un no rotund. Els nostres membres almenys gaudiran de les tarifes oficials. A m&eacute;s buscarem acords amb els nostres prove&iuml;dors per a oferir promocions cont&iacute;nues. Sempre es tindr&agrave; tamb&eacute; en compte el perfil i historial de cada membre a l'hora de suggerir una soluci&oacute; adequada a les seves possibilitats.");
define("FAQ4_X","No fa falta comentar que proporcionant-nos m&eacute;s temps permetr&agrave; afinar i millorar la nostra proposta. No obstant aix&ograve;, tractarem de satisfer la seva desitja sigui el que sigui l'antelaci&oacute;. En cas de no poder aconseguir-lo, confi&iuml; en nosaltres, li farem un suggeriment equivalent o millorada!");
define("FAQ5_X","Faci'ns arribar  la seva consulta o enc&agrave;rrec a trav&eacute;s d'e-mail a info@timesavers.es, truqui al  tel&egrave;fon +34 93 304 00 32 o simplement accedeixi a l'apartat &quot;El meu  TimeSavers&quot;, li proporcionarem una soluci&oacute; i un pressupost adaptats a les  seves necessitats ho m&eacute;s r&agrave;pid possible. Si es tracta d'un servei que necessita  l'actuaci&oacute; d'un dels nostres col laboradors externs el pressupost diferenciar&agrave;  clarament entre el cost del servei i els cr&egrave;dits requerits per la seva gesti&oacute;.  Haur&agrave; d'aprovar aquest pressupost per e-mail, tel&egrave;fon oa trav&eacute;s de &quot;El meu  TimeSavers&quot; i pagar un 50% de l'import (si l'import &eacute;s superior a 100 &euro;).  Un cop finalitzat el servei li enviarem una factura que reflecteix els  TimeSavers Credits i el servei / producte realment utilitzats. 
Per la factura  final es tindr&agrave; en compte tota compra de paquet TimeSavers Credits realitzada  abans de l'inici del servei sol licitat i s'aplicar&agrave; a aquesta factura. En cas  que no hagi comprat TimeSavers Credits us carregarem el nombre exacte de  cr&egrave;dits utilitzats en la gesti&oacute; del servei.");
define("FAQ6_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> opera principalment a Barcelona capital.");
define("FAQ7_X","El nostre horari  comercial &eacute;s de dilluns a divendres de 10h a 20h. Tanmateix, hi ha la  possibilitat d'atendre les 24 hores del dia trucant al +34 93 304 00 32. A gestions fora de l'horari  comercial s'aplicar&agrave; un 25% sobre la tarifa.");
define("FAQ8_X","Oferim tots els nostres serveis amb un comprom&iacute;s a l'excel&middot;l&egrave;ncia i sense errors. Alguns serveis seran donats directament per TimeSavers - i uns altres seran externalitzats a tercers. Tots els serveis externalitzats seran realitzats pels nostres prove&iuml;dors qualificats i provats.Com a garanties, TimeSavers exigim Segur de Responsabilitat Civil.");
define("FAQ9_X","Pel que respecta a les garanties, <em>TimeSavers - Quality Personal Concierge</em> opera amb un segur de responsabilitat civil, que cubreix qualsevol transacci&ograve;, a  m&egrave;s, exigim el mateix als nostres prove&iuml;dors.");
define("FAQ10_X","L'alta es fa amb  un pagament de 50 &euro; durant el registre. Aquest import es descomptar&agrave;  posteriorment a la compra de gesti&oacute; TimeSavers. Es pot donar de baixa en  qualsevol moment enviant-nos un e-mail a info@timesavers.es o a trav&eacute;s d'un  simple clic en l'apartat de &quot;El meu TimeSavers&quot; de la nostra p&agrave;gina  web. Gestions comprades i no utilitzades es reemborsaran fins a un termini de  30 dies naturals a partir de la data de compra.");
define("FAQ11_X","A partir del dia de la compra tota gesti&oacute; TimeSavers ser&agrave; v&agrave;lida durant 1 any.");
define("FAQ12_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> complix amb la llei espanyola en protecci&oacute; de dades. Vost&egrave; pot tenir acc&eacute;s, rectificar, oposar-se o cancel&middot;lar les seves dades personals en qualsevol moment per enviant-nos un correu a info@timesavers.es o cridant-nos en +34 93304 00 32. Per a la informaci&oacute; m&eacute;s detallada, per favor consulti la nostra secci&oacute; <a title='Legal' href='legal.php'>Av&iacute;s Legal i Condicions d'&uacute;s</a>. Si vost&egrave; necessita l'ajuda amb aquesta secci&oacute;, no deixi de posar-se en contacte amb nosaltres.");
define("FAQ13_X","<em>TimeSavers &ndash; Quality  Personal Concierge</em> funciona a base de cr&egrave;dits. La gesti&oacute;  de cada servei per part nostra requereix un cert nombre de TimeSavers Credits.  Aquests cr&egrave;dits es compren a l'apartat &quot;El meu TimeSavers&quot; una vegada  que s'hagi donat d'alta com a membre TimeSavers. Per a m&eacute;s informaci&oacute; sobre  TimeSavers Credits faci click <a href='http://www.timesavers.es/gestiones.php'>aqu&iacute;</a>.");

define("GEST0A","<em class='blanco'>TimeSavers - Quality Personal Concierge</em> funciona a base de cr&egrave;dits i la gesti&oacute; de cada  servei requereix un cert nombre de cr&egrave;dits. Els cr&egrave;dits es compren a l'apartat  &quot;El meu TimeSavers&quot; una vegada que s'hagi donat d'alta com a membre  TimeSavers. <br />
  <br />Quan feu una sol licitud de servei li farem arribar  per correu electr&ograve;nic i / o al seu compte &quot;El meu TimeSavers&quot; un  pressupost sense cap comprom&iacute;s que detalla el nombre de cr&egrave;dits previst per la  seva gesti&oacute;. En l'apartat &quot;El meu TimeSavers&quot; t&eacute; la possibilitat  d'adquirir paquets de TimeSavers Credits m&eacute;s importants per aconseguir un preu  m&eacute;s atractiu per cr&egrave;dit.<br />
  <br />Haur&agrave; d'aprovar aquest pressupost per e-mail,  tel&egrave;fon oa trav&eacute;s de &quot;El meu TimeSavers&quot; i pagar un 50% de l'import  (si l'import &eacute;s superior a 100 &euro;). Si es tracta d'un servei que necessita  l'actuaci&oacute; d'un dels nostres col laboradors externs el pressupost diferenciar&agrave;  clarament entre el cost del servei i els cr&egrave;dits requerits per la seva gesti&oacute;.<br />
  <br />Un cop finalitzat el servei li enviarem una factura  que reflecteix els TimeSavers Credits i el servei / producte realment  utilitzats. Per la factura final es tindr&agrave; en compte tota compra de paquet  TimeSavers Credits realitzada abans de l'inici del servei sol licitat i  s'aplicar&agrave; a aquesta factura. En cas que no hagi comprat TimeSavers Credits us  carregarem el nombre exacte de cr&egrave;dits utilitzats en la gesti&oacute; del servei.<br />
  <br />Tots els pagaments es poden realitzar per  transfer&egrave;ncia banc&agrave;ria o directament a &quot;El meu TimeSavers&quot;.<br />
  <br />Cal destacar que amb gaireb&eacute; tots els nostres col  laboradors externs hem aconseguit un valor afegit per als nostres membres, ja  sigui un descompte, una oferta especial o un altre benefici exclusiu. <br />
  <br />Tots els preus inclouen IVA. <br />TimeSavers Credits tenen una validesa d'un any a partir de la data de compra.");

define("GEST0","<p><em>F&agrave;cil i r&agrave;pid: Faci'ns arribar la  seva consulta o enc&agrave;rrec a trav&eacute;s d'e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, truqui al  tel&egrave;fon &nbsp;+34&nbsp;93&nbsp;304&nbsp;00&nbsp;32&nbsp; o simplement accediu a  &quot;El meu TimeSavers&quot;, li proporcionarem una soluci&oacute; adaptada a les  seves necessitats el m&egrave;s aviat possible.</em><br><br><b>Preus TimeSavers Credits</b><br>A continuaci&oacute; enumerem els diferents paquets de TimeSavers Credits que poden  adquirir:</p>
	    <table cellpadding='4' cellspacing='1'>
		<tr class='filazul_1'><td>1  TimeSavers Credit</td><td>12,00 &euro;</td></tr>
		<tr class='filazul_2'><td>5  TimeSavers Credits</td><td>58,00 &euro;</td></tr>
		<tr class='filazul_1'><td>10  TimeSavers Credits</td><td>112,00 &euro;</td></tr>
		<tr class='filazul_2'><td>25  TimeSavers Credits</td><td>265,00 &euro;</td></tr>
		<tr class='filazul_1'><td>50  TimeSavers Credits</td><td>480,00 &euro;</td></tr>
		<tr class='filazul_2'><td>100 TimeSavers  Credits</td><td>840,00 &euro;</td></tr>
		</table>
	    <p>Si esteu interessats en  contractar un paquet m&eacute;s important de TimeSavers Credits, poseu-vos en contacte  amb nosaltres trucant al +34 93 304 00 32  o  enviant-nos un mail a<a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");
define("GEST1","<p>Oferim la possibilitat  d'una subscripci&oacute; mensual. Pot escollir entre dos paquets i tamb&eacute; podem  desenvolupar un paquet apte per a les seves necessitats particulars. La subscripci&oacute; TimeSavers Monthly inclou com a  benefici reserves de restaurant i gestions d'agenda gratu&iuml;ts sense, per  descomptat, renunciar a les avantatges que reben tots els membres <em>TimeSavers &ndash;  Quality Personal Concierge.</em></p>
<p><b>TimeSavers Monthly Basic</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Fins 16 TimeSavers Credits</td><td>120,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl reserves de restaurant i gesti&oacute; d'agenda personal de franc.</td></tr>
<tr class='filazul_1'><td colspan='2'>Atenci&oacute; al client de dilluns a divendres de 10h a 20h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Despeses de despla&ccedil;ament inclosos a Barcelona ciutat.</td></tr>
</table>
<p><b>TimeSavers Monthly Premium</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Fins 40 TimeSavers Credits</td><td>250,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl reserves de restaurant i gesti&oacute; d'agenda personal de franc..</td></tr>
<tr class='filazul_1'><td colspan='2'>Atenci&oacute; al client les 24h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Despeses de despla&ccedil;ament inclosos a Barcelona ciutat.</td></tr>
</table>
<p>Les subscripcions mensuals  tenen una validesa de 30 dies naturals a partir de la data de compra.  TimeSavers Credits no utilitzats durant aquest per&iacute;ode no es paguen a seg&uuml;ents  sucripcions.<br><br><b>Exemples</b><br>
Per  donar una idea de quants TimeSavers Credits pot costar una gesti&oacute; li  proporcionem els seg&uuml;ents exemples:</p>
<ul>
<li>1 reserva de restaurant li costar&agrave; 1 TimeSavers Credit. Tal com s'ha comentat anteriorment, us enviarem un pressupost per mail i /  o l'apartat &quot;El meu TimeSavers&quot; qual haur&agrave; d'aprovar per una  d'aquestes dues vies o per tel&egrave;fon. At&egrave;s que l'import &eacute;s inferior a 100 &euro; no &eacute;s  necessari pagar un 50% per avan&ccedil;at. A continuaci&oacute; li lliurarem la factura que  podr&agrave; pagar per transfer&egrave;ncia banc&agrave;ria o mitjan&ccedil;ant &quot;El meu  TimeSavers&quot;. S&agrave;piga que amb tots els restaurants amb els quals col  laborem hem negociat un plus per als nostres clients. Deixi's sorprendre!<br />
<li>Enviar un perruquer a casa suposa 2  TimeSavers Credits la primera vegada que sol licita el servei m&eacute;s el cost de  l'estilista. Li remetrem un pressupost detallant els TimeSavers Credits  requerits i el cost del servei segons la informaci&oacute; proporcionada. Ha d'aprovar  el pressupost i pagar un 50% per avan&ccedil;at. Una vegada que el perruquer va a casa  seva pot ser que es decideixi d'afegir un tractament cuidador al tint  inicialment sol licitat. &iexcl;No us preocupeu! No suposa cap problema. Per aix&ograve; li  enviem una factura final que reflecteix els TimeSavers Credits i els serveis  realment utilitzats. Aix&iacute; mateix quan repeteixi amb el mateix professional, li  farem una gesti&oacute; d'agenda personal que &eacute;s d'1 TimeSavers Credit.</p>
<li>Encarregarnos la renovaci&oacute; de la  ITV sol tardar entre 2 i 3 hores per la qual cosa valdria entre 8 i 12  TimeSavers Credits. Abans d'iniciar el servei, com sempre, li enviem un  pressupost amb una estimaci&oacute; de cost TimeSavers Credits calculant una mitjana.  Es repeteixen els passos de l'aprovaci&oacute; i un cop finalitzat el servei  comptabilitzar els cr&egrave;dits realment utilitzats i li passem una factura, menys  el pagament inicial, que podr&agrave; pagar per les dues vies esmentades</li>
</ul>");
define("GEST2","Gaudeixi de  Barcelona d'una altra manera i aprofiti al m&agrave;xim la seva estada! Oferim als  nostres amics visitants i turistes diferents paquets establerts com, per  exemple, &quot;Barcelona by Sea&quot; o &quot;Barcelona by Air&quot;.A m&eacute;s a  m&eacute;s, cap &nbsp;la possibilitat de dissenyar un programa a la seva mida segons  els seus desitjos.<br><br>Si  est&aacute; interessat o vol m&eacute;s informaci&oacute;, contacti &nbsp;amb nosaltres trucant al +34 93 304 00 32 o enviant-nos un mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.");
define("GEST3","&iquest;Perqu&egrave; no regalar una  cosa realment &uacute;til? La recerca d'un regal interessant, innovador i diferent pot  ser una cosa molt estressant. A banda que pot encarregar <em>TimeSavers - Quality  Personal Concierge</em> amb aquesta cerca, hi ha a m&eacute;s la possibilitat de sorprendre  a la seva parella, familiar o amic amb un bo TimeSavers coupon.<br><br>Quan compri el seu paquet  d'TimeSavers Credits podr&agrave; triar l'opci&oacute; &quot;TimeSavers coupon&quot;.  Facilitant l'e-mail o l'adre&ccedil;a del destinatari li farem arribar el seu val (el  val nom&eacute;s mostrar&agrave; el nombre de TimeSavers Credits).");
define("GEST4","Segons un estudi  de Reuters dediquem un 10% del nostre temps laboral a gestionar tasques  dom&egrave;stiques. <em>TimeSavers - Quality Personal Concierge</em> pot servir a la seva  empresa com a eina per millorar la conciliaci&oacute; de la vida familiar i laboral  dels seus empleats. Els  seus empleats tindran menys preocupacions per la qual cosa podran millorar la  seva dedicaci&oacute; laboral. De fet un estudi PriceWaterhouseCoopers trobar que  empreses amb una bona pol&iacute;tica i pr&agrave;ctiques de conciliaci&oacute; aconsegueixen un  benefici de 99 c&egrave;ntims per cada euro invertit en Recursos Humans comparat amb  un retorn de sols 14 c&egrave;ntims per la mitjana d'empreses europees.
<br><br>Si esteu interessats o voleu m&eacute;s informaci&oacute;, poseu-vos en contacte amb  nosaltres trucant al +34 93 304 00 32 o  enviant-nos un mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.<br><br>Tots els preus inclouen IVA.");

define("SER_1","Recerca internet");
define("SER_2","Organitzaci&oacute; sopar");
define("SER_3","Catering");
define("SER_4","Espera lliurament a domicili");
define("SER_5","Pasejador gos");
define("SER_6","Tr&agrave;mit burocr&agrave;tic");
define("SER_7","Organitzaci&oacute; esdeveniments");
define("SER_8","Gesti&oacute; agenda personal");
define("SER_9","Personal shopper");
define("SER_10","Organitzaci&oacute; viatges");
define("SER_11","Reserva restaurant");
define("SER_12","Reserva espectacle");
define("SER_13","Servei contestador trucades");
define("SER_14","Compra regals");
define("SER_15","Mudances");
define("SER_16","Cangur nen");
define("SER_17","Cura claus de casa");
define("SER_18","Chofer");
define("SER_19","Recollida correu");
define("SER_20","Missatgeria");
define("SER_21","Pedicura/ manicura");
define("SER_22","Perruqueria");
define("SER_23","Nutricionista");
define("SER_24","Entrenador personal");
define("SER_25","Spa");
define("SER_26","Massatgista");
define("SER_27","Assessor d'imatge");
define("SER_28","Interiorisme/exteriorisme");
define("SER_29","Lampista");
define("SER_30","Paleta");
define("SER_31","Electricista");
define("SER_32","Pintor");
define("SER_33","Serraller");
define("SER_34","Neteja del llar");
define("SER_35","Rentat de roba");
define("SER_36","Planxat de roba");
define("SER_37","Neteja de vehicles");
define("SER_38","Jardiner");
define("SER_39","Neteja sopar/festa");
define("SER_40","Tintoreria");
define("SER_41","Cura de mascota");

define("SER0","Si est&agrave; interessat en un altre servei que no apareix a la llista, no ho dubti, truquin's al tel&egrave;fon +349 93 304 00 32  o be contactin's mitjan&ccedil;ant el nostre correu electr&ograve;nic <a href='mailto:info@timesavers.es' class='blanco'><b>info@timesavers.es</b></a>, li trobarem una sol.luci&ograve; el m&egrave;s aviat posible.");
define("SER1","<b>RECERCA INTERNET</b><br />Filtrar les milions d'entrades per a una simple recerca d'informaci&oacute; en internet pot ser un proc&eacute;s molt llarg. Nosaltres comptem amb experts que s'encarregaran de fer-ho.");
define("SER2","<b>ORGANITZACI&Oacute; SOPAR</b><br />Necessita rebre a un grup de gent a casa i li falta la vaixella apropiada, les copes, el centre de taula i el do de cuinar? Ens encarreguem de tota l'organitzaci&oacute; per a que sol tingui que gaudir del men&uacute; i dels seus convidats.");
define("SER3","<b>CATERING</b><br />La cuina li resulta ser un malson? Comptem amb xefs amb experi&egrave;ncia i serveis de caterings excel&middot;lents per treure-li aquesta preocupaci&oacute;.");
define("SER4","<b>ESPERA LLIURAMENT A DOMICILI</b><br />Si alguna vegada li han hagut de lliurar alguna cosa a casa, sap que solen donar intervals d'almenys 3 hores per fer-ho. I com gestiona aix&ograve; un dia laboral? Ens oferim esperar al seu lloc perqu&egrave; no perdi el temps.");
define("SER5","<b>PASEJADOR GOS</b><br />El seu amic favorit necessita c&oacute;rrer i jugar l'en parc? Tenim professionals per passejar el seu gos.");
define("SER6","<b>TR&Agrave;MIT BUROCR&Agrave;TIC</b><br />Els tr&agrave;mits burocr&agrave;tics solen ser lents i pesats. Els delegui a nosaltres! Tenim professionals amb experi&egrave;ncia per gestionar-lo tot.");
define("SER7","<b>ORGANITZACI&Oacute; ESDEVENIMENTS</b><br />Necessita organitzar un aniversari especial, un casament, i no t&eacute; el temps suficient per gestionar-lo tot? Tenim al seu servei professionals que li proporcionaran un esdeveniment inoblidable.");
define("SER8","<b>GESTI&Oacute; AGENDA PERSONAL</b><br />Recordar totes les reunions, aniversari i sopars pot resultar molt estressant. Deixi que ens ocupem de la gesti&oacute; de la seva agenda i mai m&eacute;s perdr&agrave; cap cita important.");
define("SER9","<b>PERSONAL SHOPPER</b><br />Anar de compres li resulta un calvari? O simplement no t&eacute; el temps de fer-ho? O potser vol una opini&oacute; externa per renovar el seu armari? Comptem amb professionals per assessorar-los i fer les compres amb o per vost&egrave;.");
define("SER10","<b>ORGANITZACI&Oacute; VIATGES</b><br />Buscar els vols m&eacute;s barats, un hotel bonic, unes excursions interessants perqu&egrave; tingui unes vacances especials pot resultar bastant dif&iacute;cil. Ocupi's de fer les maletes i nosaltres ens encarregarem de la resta.");
define("SER11","<b>RESERVA RESTAURANT</b><br />Vol descobrir nous restaurants o t&eacute; dificultats en aconseguir una taula a &uacute;ltima hora? Posse&iuml;m una &agrave;mplia cartera per satisfer tots els gusts.");
define("SER12","<b>RESERVA ESPECTACLE</b><br />Li costa aconseguir les places per a aquest espectacle que li agrada tant a la seva parella? Confi&iuml; que farem tot el possible per obtenir-les i tregui's aquesta preocupaci&oacute; de sobre.");
define("SER13","<b>SERVEI CONTESTADOR TRUCADES</b><br />Necessita que alg&uacute; at&eacute;n les seves trucades en la seva abs&egrave;ncia? Nosaltres oferim un servei de contestaci&oacute; a trucades i li remetrem nom&eacute;s les importants.");
define("SER14","<b>COMPRA REGALS</b><br />Pot ser dif&iacute;cil trobar el regal d'aniversari adequat. Confi&iuml; en nosaltres i li localitzarem el regal idoni.");
define("SER15","<b>MUDANCES</b><br />Una mudan&ccedil;a sempre &eacute;s una cosa laboriosa. Podem gestionar-la perqu&egrave; sigui el menys penosa possible.");
define("SER16","<b>&quot;CANGUR&quot; NEN</b><br />Vol regalar-se una nit rom&agrave;ntica amb la seva parella sense haver de preocupar-se pel seu nen? Comptem amb &quot;cangurs&quot; professionals i amb credencials.");
define("SER17","<b>CURA CLAUS DE CASA</b><br />Si ha perdut alguna vegada les seves claus de casa, sap quant pot costar un servei de many&agrave;. Cuidem una c&ograve;pia de les seves claus de casa per a qualsevol emerg&egrave;ncia");
define("SER18","<b>CHOFER</b><br />Arriba la seva fam&iacute;lia a l'aeroport i no t&eacute; el temps d'anar a buscar-los. T&eacute; un sopar amb amics i no vol preocupar-se de controls d'alcohol&egrave;mia. No hi ha problema, li enviem un chofer perqu&egrave; no s'hagi de preocupar.");
define("SER19","<b>RECOLLIDA CORREU</b><br />Si no vol que es desbordi la seva b&uacute;stia durant les seves vacances, enviarem alg&uacute; per recollir i guardar el seu correu en la seva abs&egrave;ncia.");
define("SER20","<b>MISSATGERIA</b><br />Necessita fer una tramesa urgent a Barcelona ciutat? Venim a recollir-lo i lliurar-lo.");
define("SER21","<b>PEDICURA/ MANICURA</b><br />No repari en atencions per a si mateix, ens soliciti'ns el servei i l'hi enviarem.");
define("SER22","<b>PERRUQUERIA</b><br />Alguna urg&egrave;ncia? Reunions no programades? O simplement necessita una atenci&oacute; a domicili Un estilista resoldr&agrave; qualsevol d'aquests inconvenients.");
define("SER23","<b>NUTRICIONISTA</b><br />Deixi's assessorar sol per professionals acreditats. Expliquem amb Nutricionistes que l'assessoraran i faran seguiment del seu cas.");
define("SER24","<b>ENTRENADOR PERSONAL</b><br />Falta de temps per assistir al gimnas? Necessita un programa personalitzat? Contacti'ns i l'hi proporcionarem.");
define("SER25","<b>SPA</b><br />Permeti's un moment de relax. Deixi que s'ocupin de vost&egrave;, simplement es relaxi, nosaltres li donarem acc&eacute;s a les millors instal&middot;lacions.");
define("SER26","<b>MASSATGISTA</b><br />S&iacute;mptomes de stress?, mol&egrave;sties localitzades? O necessita una atenci&oacute; terap&egrave;utica?, Un professional sabr&agrave; tractar dels casos, es sentir&agrave; millor.");
define("SER27","<b>ASSESSOR D'IMATGE</b><br />  Si t&eacute; un acte social i es troba perdut, o li agradaria realitzar un canvi d'imatge radical? L'assessoraran professionals dedicats a la imatge, buscant sempre el seu benestar.");
define("SER28","<b>INTERIORISME/EXTERIORISME</b><br />Necessita un parell de consells sobre interiorisme/exteriorisme, o prefereix que ens encarreguem personalment de tot?, esculli la millor opci&oacute; per a la seva casa i gaudeixi dels resultats.");
define("SER29","<b>LAMPISTA</b><br />Quan la urgencia prima i no hi ha temps que perdre. Li trobarem el professional que solucionar&agrave; les seves avaries d'una manera r&agrave;pida i polida.");
define("SER30","<b>PALETA</b><br />Els petits arranjaments a casa no tenen per qu&egrave; ser un problema. Nosaltres li donarem solucions i contactarem amb el professional adequat.");
define("SER31","<b>ELECTRICISTA</b><br />Deixi solament a professionals accedir i realitzar modificacions en la seva instal&middot;laci&oacute; el&egrave;ctrica. El bon funcionament de la mateixa dep&egrave;n d'aix&ograve;. Consulti'ns. ");
define("SER32","<b>PINTOR</b><br />Oblidi's de les jornades interminables de pintura a casa. Li aconseguirem un servei professional, ens ocuparem que tot funcioni i que el resultat sigui impecable.");
define("SER33","<b>SERRALLER</b><br />En moments d'urg&egrave;ncia, no dubti. Rapidesa i efic&agrave;cia. Posi's en contacte amb TimeSavers.");
define("SER34","<b>NETEJA DEL LLAR</b><br />Un personal eficient i professional per a un servei d'&uacute;s continuat, o simplement per a dates puntuals, consulti'ns i trobarem la soluci&oacute; adequada.");
define("SER35","<b>RENTAT DE ROBA</b><br />Deixi que ens encarreguem de la seva bugada, li garantim un servei puntual, un tractament adequat i un resultat impecable.");
define("SER36","<b>PLANXAT DE ROBA</b><br />Deixi que recollim la seva roba i ens encarreguem de la planxa, li garantim un tractament i resultats impecables.");
define("SER37","<b>NETEJA DE VEHICLES</b><br />Ens encarregarem de la neteja rutin&agrave;ria del seu vehicle, o si ho desitja , es tractar&agrave; en profunditat per a perllongar el bon estat del vehicle.");
define("SER38","<b>JARDINER</b><br />Deixi les seves plantes en mans de professionals, mantenint-les saludables o tracti-les dels petits problemes que puguin sorgir.");
define("SER39","<b>NETEJA SOPAR/FESTA</b><br />Oblidi's del dia despr&eacute;s. Ens encarregarem que tot quedi impecable. Conservi solament els bons moments de les seves reunions i festes.");
define("SER40","<b>TINTORERIA</b><br />Oblidi's dels petits contratemps amb les seves peces, buscarem la soluci&oacute; m&eacute;s adequada i respectuosa amb la seva roba.");
define("SER41","<b>CURA DE MASCOTA</b><br />Confi&iuml; les seves mascotes en les millors mans. Professionals de confian&ccedil;a que cuidaran de la seva mascota com solament vost&egrave; podria fer-lo.");
?>