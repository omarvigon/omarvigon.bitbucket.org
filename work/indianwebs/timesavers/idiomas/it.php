<?
define("FECHA_1","Dati");
define("FECHA_DIA","giorno");
define("FECHA_MES","mese");
define("FECHA_ANO","anno");
define("MES_1","Gennaio");
define("MES_2","Febbraio");
define("MES_3","Marzo");
define("MES_4","Aprile");
define("MES_5","Maggio");
define("MES_6","Giugno");
define("MES_7","Luglio");
define("MES_8","Agosto");
define("MES_9","Settembre ");
define("MES_10","Ottobre ");
define("MES_11","Novembre");
define("MES_12","Dicembre");

define("BT1","Cos&rsquo;&egrave;<br />TimeSavers");
define("BT2","Servizi");
define("BT2A","Risparmio di Tempo");
define("BT2B","Cura personale");
define("BT2C","Miglioramento del domestico");
define("BT2D","Cura della casa");
define("BT3A","Credits");
define("BT3B","Prezzi");
define("BT3C","Gesti&oacute;n");
define("BT4","Mezzi");
define("BT4A","News TimeSavers");
define("BT4B","TimeSavers nei mezzi");
define("BT5A","Contatto");
define("BT5B","Contatto fornitori");
define("BT6","Aviso Legal &amp; Condiciones de Uso");
define("BT7","Olvid&eacute; mi password");
define("BT8A","Cosa fa TimeSavers?");
define("BT8B","Chi sono TimeSavers");
define("BT9","Reg&iacute;strese");
define("BT10","Entrar");
define("BT11","BROWSE");
define("XHORA","Ora");
define("XMAPA","Vedere la piantina");
define("XCOMPRAR","Comprar");
define("XCANCEL","Rechazar");

define("REG_INICIO","<p>&iexcl;Bienvenido a <em class='blanco'>TimeSavers - Quality Personal Concierge</em>! Est&aacute; a punto de entrar en una nueva etapa de su vida, en la que esperamos poder reducir su nivel de estr&eacute;s y aumentar su tiempo libre.</p>
		<p>La cuota de alta es de 50,- &euro;, reembolsable con la compra de TimeSavers Credits.</p>
		<p>A continuaci&oacute;n le pedimos  unos datos personales.</p>");
define("REG_ELEGIR","(Selezionare)");
define("REG_TRATA","Trattamento");
define("REG_TRATA_1","Sr.");
define("REG_TRATA_2","Sra.");
define("REG_TRATA_3","Dr.");
define("REG_NOMBRE","Nome");
define("REG_APELLIDOS","Cognome");
define("REG_SEXO","Sesso");
define("REG_SEXO_1","Maschio");
define("REG_SEXO_0","Femmina");
define("REG_NACIMIENTO","Data di nascita");
define("REG_NACIONALIDAD","Nazionalit&agrave;");
define("REG_DOCUMENTO","Documento de identidad");
define("REG_DOCNUM","Numero del documento");
define("REG_DIRECCION","Indirizzo");
define("REG_POBLACION","Popolazione");
define("REG_CP","C.P.");
define("REG_TEL","Telefono");
define("REG_MOVIL","Mobile Phone");
define("REG_IDIOMA","Scegli una lingua di comunicazione");
define("REG_MODO","Modalit&agrave; di comunicazione preferiti");
define("REG_CONOCIO","&iquest;C&oacute;mo nos ha conocido ?");
define("REG_CONCIO_1","Proveedor");
define("REG_CONCIO_2","Amigo");
define("REG_CONCIO_3","Anuncio");
define("REG_CONCIO_4","Folleto");
define("REG_CONCIO_5","Televisi&oacute;n");
define("REG_CONCIO_6","Altri");
define("REG_COMENTA","Commenti");
define("REG_FOTO","Su foto : (Formato JPG)");
define("REG_CONTRA","Password");
define("REG_CONTRA2","Cambia password");
define("REG_CONTRA3","Ricevera il suo password per e-mail registrato");
define("REG_MODIF","Modificare");
define("REG_BAJADAR","Voglio darmi basso");
define("REG_BAJA","Se puede dar de baja en cualquier momento envi&aacute;ndonos un email a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, llam&aacute;ndonos al +34 93 304 0032 o a trav&eacute;s de un simple click en su cuenta. Productos comprados y no utilizados se reembolsar&aacute;n hasta un plazo de 30 d&iacute;as a partir de la fecha de compra.");
define("REG_REGISTRAR","REGISTRAR");
define("REG_DESCRIP","Descrizione");
define("REG_LUGAR","Sito web");
define("REG_DIFERENTE","se diverso da quello registrato)");
define("REG_CARRITO","Aggiungi al Carrello");
define("REG_FECHASER","Data di servizio desiderato");
define("REG_CODIGO","Se lei ha un c&oacute;dice di promozione, puo interodurrerlo qui. Los sconto si applichera nel suo primero acquisto di Time Savers Credits.");
define("REG_FIN","&iexcl;Benvenuti a <em> TimeSavers - Quality Personal Concierge!</em>!<br /><br />Abbiamo inviato la sua password all&rsquo;indirizzo");

define("OLVIDO","<h3>Olvid&oacute; sus datos</h3>
        <p>Nel caso di no ricordare la sua e-mail di registro/utente ritracciateci al +34 93 304 00 32  o per e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");

define("USER_MI","Il mio Timesavers");
define("USER_PEDIDO","Richiesta di Servizio");
define("USER_HISTORIAL_1","Quotazioni");
define("USER_HISTORIAL_2","Fatture");
define("USER_PEDIR","REALIZAR PEDIDO");
define("USER_SALDO_1","Credits<br />TimeSavers<br />utilizzati");
define("USER_SALDO_2","Crediti<br />Saldo<br />TimeSavers");
define("USER_ACCION","Azioni");
define("USER_IMPORTE","Import");
define("USER_DETALLES","Dettaglio");
define("USER_DATOS","Dati Personali");
define("USER_NOTICIAS","News");
define("USER_RECOMENDA","Consigli");
define("USER_RECOMENDA_2","Raccomandazione");
define("USER_RECOMENDA_10","Arrivo servizio");
define("USER_RECOMENDA_20","Offerta di servizi");
define("USER_RECOMENDA_30","Incidencia richiesta");
define("USER_RECOMENDA_40","Modus Occupazione");
define("USER_RECOMENDA_3","RACCOMANDATO");
define("USER_RECOMENDACION","<p><em>TimeSavers - Qualit&agrave; Personal Concierge</em> valore la tua opinione. Massima soddisfazione dei nostri membri &egrave; il nostro obiettivo primario.</p>
	<p>Naturalmente, il modo migliore per raggiungere questo obiettivo &egrave; quello di ascoltare e di chiedere per la vostra collaborazione.</p>
	<p>A tal fine abbiamo messo a disposizione questo modulo di feedback. Abbiamo fiducia che si metter&agrave; in contatto con assoluta immediatezza. Naturalmente, possiamo anche contattarci telefonicamente al <b>+34 93 304 00 32</b> oppure inviateci una e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> di parlare con uno dei nostri consulenti.</p>");

define("IND_VIDEO","<object width='220' height='220'><param name='allowfullscreen' value='true' /><param name='allowscriptaccess' value='always' /><param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=1839349&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' /><embed src='http://vimeo.com/moogaloop.swf?clip_id=1839349&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='220' height='220'></embed></object>
			<p><a href='http://vimeo.com/1839349?pg=embed&amp;sec=1839349' target='_blank'>Video di benvenuta: Time-Saber Quality Personal Concierge</a>.</p>");
define("IND0","Benvenuto a  TimeSavers - Quality Personal Concierge");
define("IND1","<p>Secondo l&rsquo;Acad&eacute;mie Fran&ccedil;aise, la parola  &ldquo;concierge&bdquo; viene dalla parola &ldquo;cumcerges&bdquo; del secolo XII, che significa &ldquo;il  guardiano&bdquo;. &Egrave; definito come persona in carica per prendere la cura di un  palazzo o di un palazzo pubblico.</p>
	<p>I &ldquo;Concierge Services&rdquo; moderni sono nati, dove senno, a New York, la citta che mai dorme, negli anni 80. Rispondevano alle necesita della gente di conciliazione della vita familiare e di lavoro e pet poter in piu godere di piu tempo libero.</p>
	<p>D&rsquo;allora il concetto sta conquistando il  mondo ed ora, per concludere, <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> fornisce  questo servizio a Barcellona.</p>");
define("IND2","&iexcl;Immagini una vita  senza stress!");
define("IND3","<p><em>TimeSavers - Quality  Personal Concierge&nbsp; &egrave;</em>  stato nato con l&rsquo;idea di rendere piu  facile, verificandosi che gode di pi&ugrave; periodo di svago,&nbsp; e far salire la sua qualit&agrave; di vita e quella  della sua famiglia.</p>
  <p>In <em>TimeSavers - Quality  Personal Concierge</em> siamo fornitori di tempo, di modo che lo dedica a quello che pi&ugrave;  interessa a lei.</p>
  <p>La vita quotidiana  nella citt&agrave;, le amministrazioni, gli appuntamenti lenti, gli acquisti, gli atti  burocratici e mai tempo abastanza per fare tutto.</p>
  <p>Immagini una vita  dove poter godere del suo proprio personal assistant, sempre a sua disposizione  con una telefonata oppure un semplice click?</p>
  <p>In <em>TimeSavers - Quality  Personal Concierge</em> abbiamo dei professionisti  efficaci e discreti per rendere reale il sogno del proprio personal assitant.</p>");
define("IND4","&iexcl;Consulti el nostre cat&agrave;leg de productes per a visitants fent click aqu&iacute;!");

define("LIN1","<p>Qui troveranno links di  interesse generale, delle web che ci piacciono.</p>
		<p style='font-size:10px;'>TimeSavers Quality Personal  Concierge non si risponsabilizza del contenuto dei siti di altri ai quali si  puo accedere da questi link.</p>");
define("LIN2","in costruzione...");

define("LEG1","<p>Esta  p&aacute;gina web es propiedad de <b>TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L. </b>en lo adelante, <b>TIMESAVERS,</b> el hecho de acceder a esta p&aacute;gina implica el  conocimiento y aceptaci&oacute;n de la pol&iacute;tica de privacidad. Esta pol&iacute;tica de  privacidad forma parte de las condiciones y t&eacute;rminos de usos.<br />
          En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio,  de Servicios de la Sociedad  de la Informaci&oacute;n y Comercio Electr&oacute;nico (LSSICE) se exponen los datos  identificativos.</p>");
define("LEG2","<p><b>Condiciones  de Uso y Pol&iacute;tica de Privacidad </b></p>
        <p>1. <u>Datos Identificativos de la Empresa</u></p>
        <p>Identidad y Direcci&oacute;n:</p>
        <p>Empresa: TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.<br />CIF: B64806862 - Nacionalidad: Espa&ntilde;ola <br />Domicilio: Ronda Universitat, 15, 1&ordm; 1&ordf;,  08007 - Barcelona (Espa&ntilde;a)<br />Tel&eacute;fono: +34 304 00 32 - Fax: +34 550 44 66<br />Correo electr&oacute;nico: <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em></p>
        <p><b>TIMESAVERS</b>, es titular del  sitio web <em>www.timesavers.es</em> y responsable de los  ficheros generados con los datos de car&aacute;cter personal suministrados por los  usuarios a trav&eacute;s de este sitio web. </p>
        <p>2. <u>Tratamiento de los datos de car&aacute;cter personal</u>.</p>
        <p>El usuario de la web autoriza a <b>TIMESAVERS</b>, el tratamiento automatizado  de los datos personales que suministre voluntariamente, a trav&eacute;s de formularios  y correo electr&oacute;nico, para: </p>
        <p>-Presupuestos relacionados con los  productos y servicios de <b>TIMESAVERS</b>.<br />-Venta, env&iacute;o y gesti&oacute;n con relaci&oacute;n a los productos y  servicios de <b>TIMESAVERS </b>comprados  por el usuario en la web.<b></b></p>
        <p>Asimismo, le  informamos que sus datos podr&aacute;n ser utilizados para enviarle informaci&oacute;n sobre  nuestros productos, ofertas y servicios en servicios personales, salvo que  usted manifieste lo contrario indic&aacute;ndolo expresamente en el formulario  correspondiente.</p>
		<p>Asimismo, queda usted  informado que parte de la informaci&oacute;n que usted nos facilite puede ser  comunicada a terceras empresas y colaboradores que TIMESAVERS pueda  subcontratar con la &uacute;nica y exclusiva finalidad de poder llevar a cabo los  servicios por usted contratados. Puede manifestarse en el formulario  correspondiente, si no quiere que se conserven sus datos m&aacute;s tiempo del  legalmente exigido.</p>
		<p>Del mismo modo, si  TIMESAVERS lo considera oportuno, y salvo que usted se manifieste en contrario,  indic&aacute;ndolo expresamente en el formulario correspondiente, conservar&aacute; sus datos  m&aacute;s del tiempo legalmente exigido, aunque no contin&uacute;e existiendo una relaci&oacute;n  comercial entre usted y TIMESAVERS, para poder llevar un control de los  servicios llevados a cabo y, si usted as&iacute; lo desea, poder continuar envi&aacute;ndole  informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS.</p>
        
        <p>El fichero creado est&aacute; ubicado en la Ronda  Universitat, 15, 1&ordm; 1&ordf;, 08007 - Barcelona (Espa&ntilde;a) bajo la supervisi&oacute;n y  control de <b>TIMESAVERS</b> quien asume la  responsabilidad en la adopci&oacute;n de medidas de seguridad de &iacute;ndole t&eacute;cnica y  organizativa de Nivel B&aacute;sico para proteger la confidencialidad e integridad de  la informaci&oacute;n, de acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999  de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal y su normativa  de desarrollo.</p>
        <p>El usuario responder&aacute;, en cualquier caso,  de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reserv&aacute;ndose <b>TIMESAVERS</b> el derecho a excluir de  los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados,  inexactos, sin perjuicio de las dem&aacute;s acciones que procedan en Derecho. </p>
        <p>Cualquier usuario puede ejercitar sus  derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos de  car&aacute;cter personal suministrados a trav&eacute;s de la web <em>www.timesavers.es</em> o por correo  electr&oacute;nico, mediante comunicaci&oacute;n escrita dirigida a <b>TIMESAVERS </b>ubicada en la Ronda Universitat, 15, 1&ordm; 1&ordf;, 08007 -  Barcelona (Espa&ntilde;a) o al correo electr&oacute;nico <em>info@timesavers.es</em>. </p>
        <p>3. <u>Propiedad Intelectual e Industrial:</u> El nombre de dominio <em>www.timesavers.es</em> de esta p&aacute;gina web  es propiedad de <b>TIMESAVERS</b>. El sitio  web <em>www.timesavers.es</em> en su totalidad,  incluyendo su dise&ntilde;o, estructura, distribuci&oacute;n, textos, contenidos, logotipos,  botones, im&aacute;genes, dibujos, c&oacute;digo fuente, bases de datos, as&iacute; como todos los  derechos de propiedad intelectual e industrial y cualquier otro signo  distintivo, pertenecen a <b>TIMESAVERS</b>,<b> </b>o, en su caso, a las personas o empresas que  figuran como autores o titulares de los derechos y que han autorizado a su uso a <b>TIMESAVERS</b>.<b> </b></p>
        <p>Queda prohibida la reproducci&oacute;n o  explotaci&oacute;n total o parcial, por cualquier medio, de los contenidos del portal  de <b>TIMESAVERS</b> para usos diferentes  de la leg&iacute;tima informaci&oacute;n o contrataci&oacute;n por los usuarios de los servicios  ofrecidos. En tal sentido, usted no puede copiar, reproducir, recompilar,  descompilar, desmontar, distribuir, publicar, mostrar, ejecutar, modificar,  volcar, realizar trabajos derivados, transmitir o explotar partes del servicio,  exceptuando el volcado del material del servicio y/o hacer copia para su uso  personal no lucrativo, siempre y cuando usted reconozca todos los derechos de  autor y Propiedad Industrial. La modificaci&oacute;n del contenido de los servicios  representa una violaci&oacute;n a los leg&iacute;timos derechos de <b>TIMESAVERS</b>.</p>
        <p>4. <u>Responsabilidades</u>. <b>TIMESAVERS </b>no se responsabiliza de cualquier da&ntilde;o o  perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor,  como: error en las l&iacute;neas de comunicaciones, defectos en el Hardware y Software  de los usuarios, fallos en la red de Internet (de conexi&oacute;n, en las p&aacute;ginas  enlazadas).</p>
        <p>No se garantiza que el sitio web vaya a  funcionar constante, fiable y correctamente, sin retrasos o interrupciones.</p>
        <p><b>TIMESAVERS</b> se reserva el  derecho a modificar la presente pol&iacute;tica de privacidad con objeto de adaptarla  a las posibles novedades legislativas, as&iacute; como a las que pudieran derivarse de  los c&oacute;digos tipo existentes en la materia o por motivos estrat&eacute;gicos. <b>TIMESAVERS </b>declina cualquier  responsabilidad respecto a la informaci&oacute;n de esta web procedente de fuentes  ajenas a <b>TIMESAVERS.</b> </p>
        <p><b>TIMESAVERS</b> no se  responsabiliza del mal uso que se realice de los contenidos de su p&aacute;gina web,  siendo responsabilidad exclusiva de la persona que accede a ellos o los  utiliza. De igual manera, no es responsable de que menores de edad realicen  pedidos, rellenando el formulario con datos falsos, o haci&eacute;ndose pasar por un  miembro de su familia mayor de edad o cualquier tercero mayor de edad. Debiendo  de responder ante dicho hecho el mayor de edad bajo el cual se encuentra dicho  menor, teniendo <b>TIMESAVERS</b> siempre  la posibilidad de actuar por las v&iacute;as legales que considere oportunas con la  finalidad de sancionar dicho hecho. 
        En todo caso, <b>TIMESAVERS</b>, al momento de entrega del pedido, entregar&aacute; s&oacute;lo y  &uacute;nicamente a un mayor de edad, que deber&aacute; acreditar sus datos mediante  documento de identificaci&oacute;n legal. Tampoco asume responsabilidad alguna por la  informaci&oacute;n contenida en las p&aacute;ginas web de terceros a las que se pueda acceder  a trav&eacute;s de enlaces desde la p&aacute;gina <em>www.timesavers.es</em>. La presencia de  estos enlaces tiene una finalidad informativa, y no constituyen en ning&uacute;n caso  una invitaci&oacute;n a la contrataci&oacute;n de productos o servicios que se puedan ofrecer  en la p&aacute;gina web de destino. <b>TIMESAVERS</b> no responde ni se hace cargo de ning&uacute;n tipo de responsabilidad por los da&ntilde;os y  perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y  continuidad de los sitios enlazados. 
		De igual manera, <b>TIMESAVERS</b> por este medio comunica a quien le pueda interesar que  en caso de que un tercero este interesado en colocar como enlace de su p&aacute;gina  web la direcci&oacute;n de <b>TIMESAVERS</b>, &eacute;ste  deber&aacute; de comunicarse v&iacute;a correo electr&oacute;nico <em>info@timesavers.es</em> solicitando la autorizaci&oacute;n para tal  efecto. </p>
        <p>5. <u>Navegaci&oacute;n con cookies</u>. El web <em>www.timesavers.es</em> utiliza cookies,  peque&ntilde;os ficheros de datos que se generan en el ordenador del usuario y que nos  permiten conocer su frecuencia de visitas, los contenidos m&aacute;s seleccionados y  los elementos de seguridad que pueden intervenir en el control de acceso a &aacute;reas  restringidas, as&iacute; como la visualizaci&oacute;n de publicidad en funci&oacute;n de criterios  predefinidos por <b>TIMESAVERS </b>y que se  activan por cookies servidas por dicha entidad o por terceros que prestan estos  servicios por cuenta de <b>TIMESAVERS</b>.</p>
        <p>El usuario tiene la opci&oacute;n de impedir la  generaci&oacute;n de cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su  programa navegador.</p>
        <p>El usuario autoriza a <b>TIMESAVERS</b> a obtener y tratar la informaci&oacute;n que se genere como  consecuencia de la utilizaci&oacute;n del Web <em>www.timesavers.es</em>, con la &uacute;nica  finalidad de ofrecerle una navegaci&oacute;n m&aacute;s personalizada.</p>
        <p>6. <u>Seguridad.</u> Este sitio web en cuanto a la confidencialidad y tratamiento de datos de  car&aacute;cter personal, por un lado, responde a las medidas de seguridad de nivel  medio establecidas en virtud de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, sobre  Protecci&oacute;n de Datos de Car&aacute;cter Personal y su Reglamento de Medidas de  Seguridad vigente, y por otro lado responde,  en cuanto a las medidas t&eacute;cnicas a la instalaci&oacute;n de &ldquo;Antivirus y  filtros antispam&rdquo; y el Secure Socket Layers &ldquo;SSL&rdquo; como plataforma segura. Todo  lo anterior a sabiendas que en la red de redes la seguridad absoluta no existe.</p>
        <p>7. <u>Confirmaci&oacute;n  de Recepci&oacute;n Pedido.</u> Una vez recibido el pedido, TimeSavers enviar&aacute; por  correo electr&oacute;nico la confirmaci&oacute;n de recepci&oacute;n del pedido; posteriormente, y  en un plazo m&aacute;ximo de 24 horas, se enviar&aacute;, mediante correo electr&oacute;nico, el  presupuesto y el plazo de entrega del/los servicio/s solicitado/s. Los pedidos  efectuados los viernes, o v&iacute;spera de festivo, se tramitar&aacute;n el siguiente d&iacute;a  laborable (los d&iacute;as laborables no incluyen fines de semana ni d&iacute;as festivos).</p>
        <p>8. <u>Formas y  condiciones de pago (impuestos).</u> En cuanto al pago, el usuario debe elegir  entre: tarjeta de cr&eacute;dito, transferencia bancaria y/o Paypal.En el  caso de compras y/o gestiones con un importe superior a 1000&euro; , se aplicar&aacute; una  preautorizaci&oacute;n por este importe ( consultar condiciones Cyberpack &ldquo;La Caixa&rdquo;  ), con la observaci&oacute;n de que en ning&uacute;n caso nos llegar&aacute; informaci&oacute;n sobre el  numero de la tarjeta de cr&eacute;dito o cuenta, estos datos pasan directamente a la  entidad financiera, que se encargar&aacute; de certificar la autenticidad de la  tarjeta y realizar el cobro.Todo a sabiendas que en la red de redes la  seguridad absoluta no existe.</p>
        <p>Todos los precios  reflejados en el portal <em>www.timesavers.es</em> incluyen el IVA (16%). Los precios est&aacute;n sujetos a cambios sin  previo aviso.</p>
        <p>9. <u>Plazos, Formas  y Gastos de env&iacute;o.</u><br />
          A partir del momento en el que sea efectiva  la recepci&oacute;n del pedido y se confirme el pago del mismo por parte del cliente,  se proceder&aacute; a la realizaci&oacute;n efectiva de la prestaci&oacute;n de los servicios  contratados en un plazo marcado en la confirmaci&oacute;n del pedido.</p>
        <p>En caso de no disponibilidad del producto o  servicio que el cliente haya solicitado a trav&eacute;s de la web <em>www.timesavers.es</em>, se proceder&aacute; a la  comunicaci&oacute;n del incidente al usuario en un m&aacute;ximo de 3 (tres) d&iacute;as, utilizando  como canal de comunicaci&oacute;n la direcci&oacute;n de correo electr&oacute;nico, o n&uacute;mero de  tel&eacute;fono de contacto, que el usuario nos haya facilitado a trav&eacute;s del  formulario de compra. En esta comunicaci&oacute;n se le informar&aacute; al cliente de la fecha  en la que el producto o servicio comprado estar&aacute; disponible o de la  imposibilidad de disponer del mismo. En caso de que el cliente no est&eacute; de  acuerdo con los nuevos plazos o sea imposible disponer del producto o servicio  contratados, se le dar&aacute; otra alternativa a la compra o se proceder&aacute; a la  devoluci&oacute;n del importe total de la misma a elecci&oacute;n del cliente.</p>
        <p>Antes de finalizar la compra y confirmar el  pedido, se podr&aacute; conocer la cuant&iacute;a de este concepto y, en caso de desearlo, el  cliente podr&aacute; desestimar el pedido.</p>
        <p>10. <u>Pago mediante tarjeta de cr&eacute;dito</u>.  (Repudio) </p>
        <p>Cuando el importe de una compra hubiese  sido cargado utilizando el n&uacute;mero de una tarjeta de cr&eacute;dito, sin que &eacute;sta  hubiese sido presentada directamente o identificada electr&oacute;nicamente, su titular  podr&aacute; exigir la inmediata anulaci&oacute;n del cargo. En tal caso, las  correspondientes anotaciones de adeudo y reabono en las cuentas del proveedor y  del titular se efectuar&aacute;n a la mayor brevedad.</p>
        <p>Sin embargo, si la compra hubiese sido efectivamente realizada por el titular  de la tarjeta y, por lo tanto, hubiese exigido indebidamente la anulaci&oacute;n del  correspondiente cargo, aqu&eacute;l quedar&aacute; obligado frente al vendedor al  resarcimiento de los da&ntilde;os y perjuicios ocasionados como consecuencia de dicha  anulaci&oacute;n. </p>
        <p>11. <u>Desistimiento.</u></p>
        <p>El comprador podr&aacute; desistir libremente del  contrato dentro del plazo de siete d&iacute;as contados desde la fecha de recepci&oacute;n  del producto. <br />El ejercicio del derecho o desistimiento no  estar&aacute; sujeto a formalidad alguna, bastando que se acredite, en cualquier forma  admitida en Derecho.<br />
        El derecho de desistimiento del comprador  no puede implicar la imposici&oacute;n de penalidad alguna, si bien, el comprador  deber&aacute; satisfacer los gastos directos de devoluci&oacute;n y en su caso, indemnizar  los desperfectos del objeto de la compra. </p>
        <p>Lo dispuesto anteriormente no ser&aacute; de  aplicaci&oacute;n a las ventas de objetos que puedan ser reproducidos o copiados con  car&aacute;cter inmediato, que se destinen a la higiene corporal o que, en raz&oacute;n de su  naturaleza, no puedan ser devueltos. </p>
        <p>12. <u>Devoluciones.</u></p>
        <p>En cuanto a las gestiones compradas a  TimeSavers, tienen una validez de 1 a&ntilde;o para ser utilizadas, aunque existe la  posibilidad de devoluci&oacute;n del importe de las gestiones compradas y NO  UTILIZADAS tras 30 dias naturales de haber realizado la compra de dichas  gestiones.<br />
        As&iacute; mismo, se pone en conocimiento del usuario  que algunos de los servicios ofrecidos por TimeSavers, ser&aacute;n subcontratados a  terceras empresas proveedoras de dichos servicios, con lo cual el usuario tendr&aacute;  el derecho a la devoluci&oacute;n de algunos de los productos finales en un plazo  m&aacute;ximo de diez (10) d&iacute;as tras la recepci&oacute;n del mismo, en virtud de la Ley de  Ordenamiento del Comercio Minorista. Pudiendo el usuario, solicitar la  sustituci&oacute;n del producto, la deducci&oacute;n del importe equivalente en su pr&oacute;ximo  pedido o el reembolso de su dinero, previa confirmaci&oacute;n por parte de <b>TIMESAVERS </b>de que no se ha utilizado,  da&ntilde;ado y/o quebrantado el producto. En cuanto a los dem&aacute;s productos (alimentos,  artesan&iacute;a) por su fragilidad y especial conservaci&oacute;n no se podr&aacute;n devolver,  excepto cuando el error se halla presentado por parte del vendedor.</p>
        <p><u>Proceso de devoluci&oacute;n:</u> el proceso de  devoluci&oacute;n es el siguiente: </p>
        <p>12.1. Escribir a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> solicitando un n&uacute;mero  de autorizaci&oacute;n de devoluci&oacute;n (se le da un No. de devoluci&oacute;n que determin&eacute; la  empresa, para llevar control de dichas actuaciones); 
        12.2. Hay que enviarle  junto con el pedido un formulario de devoluci&oacute;n adjunto, para que expliquen  motivos de la devoluci&oacute;n, productos a devolver (el producto s&oacute;lo se podr&aacute;  devolver en caso de da&ntilde;os en el mismo, equivocaci&oacute;n o no conformidad y en el  caso de productos alimenticios, artesan&iacute;as s&oacute;lo se podr&aacute; devolver por no  conformidad del comprador por error del vendedor y caso de da&ntilde;os en el mismo); 
        12.3.  Adjunto con el env&iacute;o del producto en su embalaje original, debidamente  protegido para su transporte, anexe copia de la factura m&aacute;s el formulario de  devoluci&oacute;n; 12.4. El reenv&iacute;o del producto ser&aacute; a su s&oacute;lo coste y deber&aacute;  enviarlo dentro de un plazo no superior a diez (10) d&iacute;as desde la recepci&oacute;n del  producto. Para el caso de que el producto este da&ntilde;ado o no sea lo que ha  solicitado y dicho error sea imputable al vendedor, todos los gastos correr&aacute;n  porcuenta de &eacute;ste.</p>
        <p>13. <u>Legislaci&oacute;n  y jurisdicci&oacute;n aplicable</u><u>.</u> Con car&aacute;cter general, las relaciones con los usuarios,  derivadas de la prestaci&oacute;n de servicios contenidos en esta p&aacute;gina web, con  renuncia expresa a cualquier fuero que pudiera corresponderles, se someten a la  legislaci&oacute;n y jurisdicci&oacute;n espa&ntilde;ola, espec&iacute;ficamente a los tribunales de la  ciudad de Barcelona (Espa&ntilde;a). Los usuarios de esta p&aacute;gina web son conscientes  de todo lo que se ha expuesto y lo aceptan voluntariamente.</p>");

define("EMP1","Un Team al suo servizio! ");
define("EMP2","<em>TimeSavers  - Quality Personal Concierge</em> &egrave; formato da una team di professionisti, venuti da  ambiti differenti e che, grazie alla loro larga esperienza, sono preparati per essere il suo personal  assistant. Tutto un team al suo servizio!");
define("EMP3","Con  la doppia nazionalit&agrave; francese e tedesca, &egrave; laureato dall'universit&agrave; di Yale.  Un cittadino del mondo, ha vissuto, tra altri, a Berlino, New York e Citt&agrave; del  Capo. In 2002 arriva a Barcellona per studiare un MBA a ESADE e decide fermarsi  a Barcelona. Christoph ha avuto un percorso professionale sfacciettato, ha  lavorato con degli artisti famosi in discografiche internazionali, ha  condotto&nbsp; dei team multinazionali e ha  fatto del marketing per una famosa diotta di gioelli, cos&igrave; come la  coordinazione di grandi superfici di bricolage.");
define("EMP4","Nato  a Barcelona, &egrave; laureato in Figurinismo da CETE (scuola superiore alla moda di  Parigi). Cosmopolita convinto, ha vissuto a Parigi, Madrid e Barcelona. El suo  percorso professionale ha fatto che abbia collaborato&nbsp; spesso nelle fiere internazionali con degli  stilisti di Haut couture. Pi&uacute;  tardi, si &egrave; centrato nell&rsquo;area logistica e nei  processi industriali di post-produzione per delle importanti ditte.");
define("EMP5","Ci lasci lavorare e riposi!");
define("EMP6","<p><em>TimeSavers - Quality Personal Concierge</em> gli d&agrave; le soluzioni alle sue necessit&agrave; personali:  l'organizzazione di un viaggio, la manutenzione della sua casa, la  organizzazione della festa del suo compleanno.</p>
<p>Permetta  che facciamo come il suo personal assistant, che ci occupiamo di portare la sua  macchina alla revisione, che cerchiamo il regalo giusto e lo facciamo arrivare,  o semplicemente si rilassi, troveremo un buon massaggista per lei.</p>
<p>Si occupi delle mansioni pi&ugrave; produttive, ci proponga andare a prendere i capi alla  tintoria, trovare qualcuno per passeggiare il suo cane, chiedere prenvetivi per  fare lavori a casa sua e se lei ne ha bisogno possiamo anche cercare un  arredatore.</p>
<p>Vada tranquillo in vacanza, avremo cura della sua casa, noi prenderemo la posta,  avremo cura delle sue piante e faremo le mansioni mentre lei sia fuori.</p>
<p>Per i servizi che non possiamo assicurare direttamente, abbiamo rapporti  continuamente con dei fornitori qualificati e testati da noi. In pi&ugrave; potr&agrave;  godere delle tariffe e le promozioni esclusive per i membri di <em>TimeSavers &ndash; Quality Personal concierge</em>.</p>");

define("CT0","<p>Prima di tutto vogliamo ringraziare il suo interesse per TimeSavers - Quality Personal Concierge. Vi faremo arrivare una risposta alla vostra domanda al piu presto. <br /><br />TimeSavers - Quality Personal Concierge</p>
<p>AVVERTENZE LEGALI: La informazione di questa e-mail e confidenziale e puo essere riservato. &Eacute; destinan solamente al suo destinatario. L&rsquo;accesso o uso di questo messaggio, da qualunque non autorizzata, possono essere fuori legge. Se lei non &eacute; la persona destinataria la preghiamo di cancellare il suo contenuto. Secondo la legge organica 15/1999, di 13 dicembre, la communichiamo che il suo indirizzo elettronico fa parte del nostro achivio automatizzato, con lo scopo di poter avere il contatto con lei. Se lei desidera oporrersi, acceder&eacute;, cancellare o modificare i suoi dati, rintracciateci a TIMESAVERS QUALITY concierge service, SL al telefono +34 93 304 00 32.</p>");
define("CT1A","<p><em>TimeSavers Quality Personal Concierge</em> cerchiamo dei fornitori dedicati a un servizio eccellente e con dei  prodotti impeccabili da soddisfare le necessita dei nostril membri.</p>
	<p>Inoltre che un rapporto  commerciale vogliamo stabilire una stretta collaborazione e sviluppare insieme  dei prodotti esclusivi che stimoleranno ai nostri membri ad usarli.</p>
	<p>Interessato? Azllora non dubitare  a riempire il nostro foma e ci metteremo in contatto al piu preto!</p>");
define("CT1B","<em>TimeSavers Quality Personal Concierge</em> siamo a sua disposizione e scommettiamo per la totale trasparenza.  Perci&ograve; non dubitare e si rivolga a noi per esporre i suoi dubbi. Saremo lieti  di servirlo col meno tempo possibile.");
define("CT2","I dati che lei ci fornisce sar a quello  stabilito nella &ldquo;Ley Organica 15/99 di protezione dei dati personali.");
define("CT3","Ho letto e accetto il <a href='legal.php'>&ldquo;Aviso  legal &amp; Condiciones de uso&rdquo;</a> del sito web TimeSavers Quality Personal  Concierge.");
define("CT4","Non voglio ricevere delle  informazioni sui prodotti, offerte e servizi si TimeSavers Quality Concierge  Service S.L.");
define("CT5","Non volgio che i miei dati siano a TimeSavers Quality Concierge Service S.L. piu tempo di  quello legalmente richiesto.");
define("CT6","Non volgiio ricevere informazione sui  prodotti offerte e servizi di TimeSavers Quality  Concierge Service S.L. una volta sia finito il nosotros rapporto commerciale.");
define("CONT1","Nome e cognomi");
define("CONT2","Nome dell dita");
define("CONT3","Tipo di servizio");
define("CONT6","Dati obbligatori");
define("CONT7","INVIARE");

define("FAQ1","Perch&egrave;  TimeSavers?");
define("FAQ2","C&rsquo;e  qualcosa che TimeSavers no possa organizzare?");
define("FAQ3","TimeSaver applica un aumento  sulle tariffe dei fornitori?");
define("FAQ4","Si accettano ordini di  ultim&rsquo;ora?");
define("FAQ5","Quali sono i passi per fare un&rsquo;ordine?");
define("FAQ6","Dove lavora TimeSavers?");
define("FAQ7","Posso contattare con TimeSavers a qualsiasi ora?");
define("FAQ8","TimeSavers &eacute; il fornitore di tutti I servizi offerti?");
define("FAQ9","Quali sono le garanzie se I  servizi sono dati ad altri?");
define("FAQ10","Come mi posso registrare/cancellare il conto?");
define("FAQ11","Una volta fatto l&rsquo;aquisto, quando posso realizzare i servizi TimeSavers?");
define("FAQ12","Qual&rsquo;&egrave; la pol&iacute;tica di  protezione dei dati di TimeSavers?");
define("FAQ13","Come Funziona TimeSavers?");

define("FAQ1_X","&Eacute; un servicio econ&oacute;micamente  ragionevole che lui permettr&agrave; delegare delle amnsioni necessarie che lei non  abbia il tempo o la possibilita di farle lei stesso. Perci&ograve; potra ridurre il  suo livello di stress e cosi aumentare il suo tempo. TimeSavers si compromette  a eseguire ogni servizio secondo i desideri e le istruzioni dei suoi membri.  Esperiamo che con un po di tempo e se abbiamo la sua fiducia lei ci possa  vedere come parte integrales el suo sistema di vita.");
define("FAQ2_X","<em>TimeSavers Quality Personal Concierge</em> provera a soddisfare al Massimo tutti I suoi  desideri dei suoi membri basta che sia legale. Siamo un team con un&rsquo;ampia e diversa esperienza dedicati 100% a  cercare la soluzione di ogni ordine. Nel caso di, ad esempio, un&rsquo;ordine di  ultima ora per una prenotazione in un ristorante di moda un venerdi ser&aacute;,  faremo il nostro meglio per avere la sua tavola. Nel caso di non riuscire  troveremo una soluzione che li piacera nello stesso modo.");
define("FAQ3_X","La risposta &eacute; assolumente no.  I nostri membri almeno godranno delle tariffe ufficiali. In piu, cerchero degli  accordi coi nostri fornitori in modo di poter offrire delle promozioni  continue. Avremo sempre in conto il profilo di ogni membro per i suggerimenti  di soluzioni adatte.");
define("FAQ4_X","Non &eacute; necesario dire che se  lei ci da piu tempo potremo migliorare la nostra proposta. Tuutavia, cercheremo  di soddisfare il suoi desideri qualunque sia l&rsquo;antecipazione. Nel caso di non  poter riuscire, abbia fiducia in noi, faremo un suggerimento uguale o anche  migliore.");
define("FAQ5_X","Ci ponga la sua domanda via e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, ci chiami al telefono +34 93 304 00 32 o semplicemente nell&rsquo;aria il mio Time Savers, li daremmo una solizione ed un preventivo addatti alle sue necesit&aacute; al piu presto. Se si trata di un servizio che ha bisogno di uno dei nostri collaboratori esterni, il preventivo fara differenza tral costo del servizio e i Credits che ci vogliono per la gestione. Lei dovr&aacute; approvare il preventivo per e-mail, tel&eacute;fono o attraverso il mio Time Savers e pagare un 50% dell&rsquo;importo (se l&rsquo;importo &eacute; superiore a 100 &euro;). Una volta il servizio si sia svolto inviaremo una fattura che riflettera i Time Savers Credits ed il servizio/prodotto veramente usati. 
  Per la fattura finale si terra conto di tutto acquisto di pack Time Savers Credits eseguita prima dell&rsquo;inizio del servizio desiderato e si apllichera in questa fattura. Nel caso che lei non abbia acquisito Credits vi fattureremo il numero esatto di Credits usati nella gestione del servizio.");
define("FAQ6_X","<em>TimeSavers Quality Personal Concierge</em> lavora principalemente nella citta di Barcelona.");
define("FAQ7_X","Il nostro orario commerciale &eacute; da lunedi a venerdi dalle 10:00 alle 20:00. Esiste anche la possibilita di avere servizi le 24 ore se chiama al:  +34 93 304 00 32 . Alle gestioni fuori orario commerciale si applichera un 25% sulla tarifa.");
define("FAQ8_X","Offriamo tutti I nostril  servizi con un impegno nella eccelenza e senza errori. Esistono dei servizi  eseguiti direttamente da TimeSavers, edanche appaltati ad altri. Tutti i  servizi esterni saranno eseguiti dai nostri qualificati e testati fornitori.");
define("FAQ9_X","Per quanto riguarda le garanzie, TimeSavers funciona con un&rsquo;assicurazione di responsabilita civile,  che copre ogni transazione, e chiediamo lo stesso ai nostri fornitori.");
define("FAQ10_X","Il registro si fa con un pagamento di 50 &euro; durante il registro. Questo importo si scontera dopo dall&rsquo;acquisto di gestioni Time-Savers. Si puo cancellare il conto in qualsia momento via e-mail a info@timesavers.es o con un semplice click nel &ldquo;Mio Time Savers nel nostro sito web. Gestioni acquisite e non usate si rimborseranno fino a 30 giorni naturali dalla data dell&rsquo;acquisto.");
define("FAQ11_X","Dal giorno dell&rsquo;aquisto tutti  i servizi TimeSavers saranno validi per un anno.");
define("FAQ12_X","Puo guardare nell&rsquo;area &ldquo;Aviso  legal &amp; Condiciones de uso&rdquo;");
define("FAQ13_X","<em>TimeSavers - Quality Personal Concierge</em> funziona coi Credits. La gestione di ogni servizio nostro vuole un certo numero di Credits. Essi si acquistono nell&rsquo;area &ldquo;Il Mio Time-Savers&rdquo; una volta si sia registrato come Membro Time Savers. Se vuole piu informazione su TimeSavers Credits faccia <a href='http://www.timesavers.es/gestiones.php'>click qui</a>.");

define("GEST0A","<em class='blanco'>TimeSavers - Quality Personal Concierge</em> funciona coi Credits e la gestione di ogni servizio vuole di un certo numero di Credits. Essi si aquistano nell&rsquo;area &ldquo;Il mio Time- Savers&rdquo; una volta si sia registrato come membro Time-Savers.<br />
<br />Quando lei faccia un&rsquo;ordine di servizio faremmo arrivare per e-mail i/o nel suo conto &laquo; Il moi Time-Savers &raquo;, un preventivo dettagliato col numero de Credits previsto per la gestione. Nell&rsquo;aria &laquo; Il Mio Time-Savers &raquo; ha la possibilit&aacute; di acquistare pacchi di Time Savers Credits piu importanti per avere un prezzo piu buono per Credit. Lei dovra approvare questo preventivo per e-mail, telefono o attraverso del &laquo; Mio Time Savers &raquo; e pagare un 50% dell&rsquo;importo (se l&rsquo;importo &eacute; superiore a 100 &euro;). Se si tratta di un servizio dove ci vuole l&rsquo;attuazione di uno dei nostri collaboratori esterni il preventivo fara differenza tra il costo del servizio e i Credits per la sua gestione.<br />
<br />Una volta finalizzato il servizio manderemo una fattura che riflettera i Time-Savers Credits ed il servizio/prodotto realmente usati. Nella fatturaq finale si terra conto di tutti gli acquisti dei packs Time-Savers Credits eseguiti prima dell&rsquo;inizio del servizio desiderato e si applichera in questa fattura. Se lei non ha acquisito Time-Savers Credits vi saranno posti il numero esatto di Credits usati nella gestione del servizio. Un cop finalitzat el servei li enviarem una factura que reflecteix els TimeSavers Credits i el servei / producte realment utilitzats. Per la factura final es tindr&agrave; en compte tota compra de paquet TimeSavers Credits realitzada abans de l'inici del servei sol licitat i s'aplicar&agrave; a aquesta factura.
En cas que no hagi comprat TimeSavers Credits us carregarem el nombre exacte de cr&egrave;dits utilitzats en la gesti&oacute; del servei. Tutti i pagamenti si potranno eseguire per trasferenzi bancaria o direttamente nel &laquo; Moi Time-Savers &raquo;. Dobbiamo rimarcare che pratticamente con tutti i nostri collaboratori esterni siamo riusciti ad avere un valore in piu per i nostri membri, gia con uno sconto, un offerta especiale oppure altre exclusivita.<br />
<br />Tutti i prezzi sono con IVA. <br />Time Savers Credits sono validi per un anno dal momento del suo acquisto.");
define("GEST0","<p><em>Facile e veloce: Ci invii la sua domanda oppure l&rsquo;ordine via e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, oppure ci puo telefonare al +34 93 304 00 32  o vada nel &ldquo;Mio Time-Savers, li daremmo una soluzione addatta alle sue necesita al piu presto.</em>
	   <br /><br /><b>Precios TimeSavers Credits</b><br />Adesso facciamo un elenco dei  differenti pack di TimeSavers Credit che puo acquirire:</p>
	    <table cellpadding='4' cellspacing='1'>
		<tr class='filazul_1'><td>1  TimeSavers Credit</td><td>12,00 &euro;</td></tr>
		<tr class='filazul_2'><td>5  TimeSavers Credits</td><td>58,00 &euro;</td></tr>
		<tr class='filazul_1'><td>10  TimeSavers Credits</td><td>112,00 &euro;</td></tr>
		<tr class='filazul_2'><td>25  TimeSavers Credits</td><td>265,00 &euro;</td></tr>
		<tr class='filazul_1'><td>50  TimeSavers Credits</td><td>480,00 &euro;</td></tr>
		<tr class='filazul_2'><td>100 TimeSavers  Credits</td><td>840,00 &euro;</td></tr>
		</table>
	    <p>Se lei e interessato in acquirire un pack piu importante di TimeSavers Credit,  prego ci rintracci al +34933940032, oppure via e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");
define("GEST1","<p>Offriamo anche la possibilita  di una suscrizione mensile. Puo scegliere tra due pack e possiamo anche  svolgere un pack adatto alle sue necessitta particolari. La suscrizione  TimeSavers Monthly comprende come beneficio prenotazioni in ristoranti e  gestioni di agenda gratis, senza rinnunciare ai vantaggi che ricevono tutti i  membri <em class='blanco'>TimeSavers Personal Concierge</em>.</p>
<p><b>TimeSavers Monthly Basic</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Fino a 16 TimeSavers Credits</td><td>120,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. Prenotazioni nei ristoranti e gestione di agenda personale gratis</td></tr>
<tr class='filazul_1'><td colspan='2'>Attenzione ai clienti da  lunedi a venerdi dalle ore 10.00 alle 20.00</td></tr>
<tr class='filazul_2'><td colspan='2'>Spese di viaggio in Barcelona  citta comprese.</td></tr>
</table>
<p><b>TimeSavers Monthly Premium</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Fino a 40 TimeSavers Credits</td><td>250,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Incl. Prenotazioni nei ristoranti e gestione di agenda personale gratis</td></tr>
<tr class='filazul_1'><td colspan='2'>Attenzione  ai clienti 24 ore su 24</td></tr>
<tr class='filazul_2'><td colspan='2'>Spese di viaggio in Barcelona  citta comprese.</td></tr>
</table>
<p>Lesuscrizioni mensili hanno  una validita di 30 giorni naturali dal momento dell'acquisto. TimeSavers  Credits non usati in questo periodo non si abbonano piu avanti.</p>
<p><b>Ad esempio</b><br />Per rendere l&rsquo;idea di quanti Time-Savers Credits puo costare una gestione porriamo questi esempi.</p>
  <ul>
  <li>1 prenotazione di restorante costera 1 Time-Savers Credit. Come abbiamo detto anteriormente invieremo un preventivo per e-mail e/o nel Mio Time Savers che lei dovra approvare in uno di questi due mezzi oppure per tel&eacute;fono. Nel caso que l&rsquo;importo sia inferiore a 100 &euro; non &eacute; necesario pagare un 50% antecipato. Dopodicche li invieremo la fattura che lei potra pagare per versamento bancario o attraverso &ldquo;Il mio Time-Savers. Italiano Con tutti i ristorante coi quali collaboriamo abbiamo negoziato un plus per i nostri clienti. Si lasci soprendrere.</li>
  <li>2. Mandare un parrucchiere da lei costa 2 Time-Savers Credits la prima volta che il servizio sia domandato, piu il costo dello stilista. Li manderemo un preventivo dettagliando i Time Savers Credits che ci vogliono ed il costo del servizio secondo l&rsquo;informazione propozionata. Lei debe approvare il preventivo e pagare un 50% in antecipo. Una volta che il parrucchiere va a casa sua puo darsi che lei desideri aggiungere un trattamento di cura in piu dell&rsquo;acconciatura. Non si preoccupi, non c&rsquo;e nessun problema. Per questo inviaremo una fattura finale coi Time Savers Credits e i servizi realmente usati. Cosi quando lei ripetta con lo stesso professionale, faremo una gestione di agenda personale che vale 1 Time Savers Credit.</li>
  <li>3. Se lei ci incarica la rinnovazione dell&rsquo;ITV puo essere due o tre, per cui costerebbe tra i 8 e 12 Credits. Prima di iniziare il servizio, come sempre invieremo un preventivo con l&rsquo;estimazione del costo in Time Savers Credits, col calcolo medio. Si ripettono i passi dell&rsquo;aprovazione e dopo di essere finalizzato il servizio contare i Credits veramente usati e inviamo una fattura, tranne il pagamento inziale, che lei potra pagare per i due mezzzi primadetti.</li>
  </ul>
<p>I previntivi si calcolano sempre col numero di Time Savers Credits piu addatto alla gestione del servizio. Anche se prima dell&rsquo;inzio dal servizio acceder&eacute; al Mio Time Savers per acquisire pack piu importante di Time Savers Credits, e cosi avere un prezzo per Credit piu buono. Questo acquisto di Time-Savers Credits si applicheranno nella fattura finale del sevizio sollecitato. </p>");
define("GEST2","Sfrutti Barcelona in un altro modo e profiti al massimo il suo stage. Offriamo ai nostri amici visitante un&rsquo;ampia gamma di esperinze ed avventure uniche. Ovviamente, c&acute;`e anche il modo di disegnare un programma alla sua misura e secondo i suoi desideri. Se siete interessati oppure volete piu info, potete contattare con noi chiamando al +34 93 304 00 32 , oppure inviando un mail <a href='mailto:info@timesavers.es'>info@timesavers.es</a> o facendo click qui.");
define("GEST3","Perche non regalare qualcosa di veramente utile. La ricerca di un  regalo interessante, innovatore e differente puo essere molto stressante. In  piu di potere fare questa ricerca esiste anche la possibilita di fare una  sopresa al suo partner, familiare o amico con un Bono TimeSavers Coupon. <br /><br />Quando lei abbia il suo pack di TimeSavers Credit potra scegliere  la opzione &quot;TimeSavers Coupon&quot;.  Dandoci la e-mail l'indirizzo del destinatario, gli faremo arrivare il  suo Coupon (il Coupon fara vedere unicamente il numero di TimeSavers Credits).");
define("GEST4","Secondo uno studio di Reuters dedichiamo un 10% del nostro tempo di lavoro a gestire lavori di casa. Time_saver Quality Personal Concierge puo essere utile alla sua ditta per migliorare la conciliazione della vita familiare e di lavoro dei suoi impiegati. Essi avranno meno preoccupazioni e per questo potranno migliorare la sua dedicazione al lavoro. Di fatti, uno studio di Price Waterhouse Coopers ha scoperto che ditte con una buona Italiano pol&iacute;tica di conciliazione riescono ad avere un beneficio di 99 cts. Per ogni euro investito nel personale, come un ritorno di solo 14 cts. Per la media delle ditte europee.
  <br /><br />In piu, <em>TimeSavers Personal Concierge</em> puo essere un modo eccelente  di fiduciare un cliente VIP.
  <br /><br />Se lei e interessato o vuole piu informazione, per favore si  rivolga a noi al TF: +34933040032 oppure via email a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.<br /><br />Tutti i prezzi IVA compreso.");

define("SER_1","RICERCA INTERNET");
define("SER_2","ORGANIZZARE UNA CENA");
define("SER_3","CATERIN");
define("SER_4","ATTENDERE LA CONSEGNA");
define("SER_5","PASSEGGIARE IL CANE");
define("SER_6","LE PROCEDURE BUROCRATICHE");
define("SER_7","ORGANIZZARE EVENTI");
define("SER_8","GESTIONE AGENDA PERSONALE");
define("SER_9","PERSONAL SHOPPER");
define("SER_10","ORGANIZZARE VIAGGI");
define("SER_11","PRENOTARE IL RISTORANTE");
define("SER_12","PRENOTAZIONI SPETTACOLI");
define("SER_13","SERVIZIO DI SEGRETERIA TELEFONICA");
define("SER_14","REGALI");
define("SER_15","TRASLOCHI");
define("SER_16","BABYSITTER");
define("SER_17","CURA PRESA DELLE CHIAVI DEL CASA");
define("SER_18","Il DRIVER");
define("SER_19","POSTA");
define("SER_20","SPEDIZIONI");
define("SER_21","PEDICURE/MANICURE");
define("SER_22","PARRUCCHIERE");
define("SER_23","DIETISTA");
define("SER_24","PERSONAL TRAINER");
define("SER_25","SPA");
define("SER_26","MASSAGGISTI");
define("SER_27","STILISMO");
define("SER_28","INTERIORISMO/EXTERIORISMO");
define("SER_29","IDRAULICO");
define("SER_30","MURATORE");
define("SER_31","L'ELETTRICISTA");
define("SER_32","PITTORE");
define("SER_33","FABBRO");
define("SER_34","LA PULIZIA DI CASA");
define("SER_35","BUCATO");
define("SER_36","FERRO DA STIRO");
define("SER_37","AUTOLAVAGGIO");
define("SER_38","GIARDINAIO");
define("SER_39","PULIZIA CENA/CELEBRAZIONE");
define("SER_40","TINTORIA");
define("SER_41","CURA DEGLI ANIMALI");

define("SER0","Servizi, risparmio di Tempo, preso cura di personale, miglioramento del domestico, preso cura della sede.<br />Se &egrave; interessato in un&rsquo;altro servizio che  non compare nel nostro elenco, chiami pure al  tf  +34 93 304 00 32 o ci puo  scrivere anche nella e-mail <a href='mailto:info@timesavers.es' class='blanco'><b>info@timesavers.es</b></a>, cercheremo una  soluzione per lei al piu presto.");
define("SER1","<b>RICERCA INTERNET</b><br />Cercare tra milioni di entrate puo essere un  processo molto lungo. Abbiamo degli Sperry che lo faranno per lei.");
define("SER2","<b>ORGANIZZARE UNA CENA</b><br />Sta per ricevere un gruppo di persone in casa  e ha bisogno di tutto quanto per avere una magnifica tavola? Noi ci  incarichiamo di tutto perch&egrave; lei possa stare per i suoi ospiti.");
define("SER3","<b>CATERIN</b><br />La  cucina &egrave; per lei un incubo? Abbiamo dei Cheff con esperienza e dei servizi di  catering stupendi per togliere le preoccupazioni a lei.");
define("SER4","<b>ATTENDERE LA CONSEGNA</b><br />Se  ha mai dovuto aspettare in casa la consegna di qualcosa, sapr&agrave; che gli danno  intervalli di almeno tre ore. E come fa  lei se debe andare a lavoro? Ci offriamo per aspettaren el suo posto.");
define("SER5","<b>PASSEGGIARE IL CANE</b><br />il  suo amico favorito ha bisogno di correre e giocare? Abbiamo dei professionisti  per fare una passeggiata al suo cane.");
define("SER6","<b>LE PROCEDURE BUROCRATICHE</b><br />gli atti burocratici sono solitamente lenti e  pesanti. Noi li faremo! Abbiamo  professionisti con esperienza per controllare tutto.");
define("SER7","<b>ORGANIZZARE EVENTI</b><br />Deve organizzare un evento oppure un  anniversario speciale e non ha il tempo sufficiente per controllare tutto?  Abbiamo dei professionisti al suo  servizio che organizzeranno un evento indimenticabile.");
define("SER8","<b>GESTIONE AGENDA PERSONALE</b><br />ricordarsi di tutte le riunioni,  compleanni pu&ograve; essere molto stressante. Noi ci occuperemo di tutto e cos&iacute; non dimenticher&agrave; mai piu un  appuntamento.");
define("SER9","<b>PERSONAL SHOPPER</b><br />Anadare a fare shopping e un incubo per  lei? Oppure  non ha semplicemente il momento di farlo? Forse desidera un'opinione esterna  per rinnovare il suo armadio? Abbiamo dei professionisti che faranno gli  acquisti per lei, o con lei.");
define("SER10","<b>ORGANIZZARE VIAGGI</b><br />per cercare i voli meno costosi, un bel  albergo, escursioni interessanti in modo che abbia une vacanze speciali puo  essere abbastanza difficile. Lei prenda la valigia e noi Oc&uacute;pese per farlo i  suitcases e sar&agrave; incaricato del resto.");
define("SER11","<b>PRENOTARE IL RISTORANTE</b><br />Vuoi scoprire i nuovi ristoranti o hai  difficolt&agrave; per prenotare un tavolo nell'ultimo momento? Abbiamo un ampio elenco  da soddisfare tutti i gusti.");
define("SER12","<b>PRENOTAZIONI SPETTACOLI</b><br />Ha  difficolt&agrave; di riuscire a trovare biglietti per lo SPETTACOLO che tanto piace al  suo partner? Abbia fiducia in noi, faremo tutto il possibile per averli, e cosi  si tolga le preoccupazioni.");
define("SER13","<b>SERVIZIO DI SEGRETERIA TELEFONICA</b><br />Ha bisogno di qualcuno che risponda le  telefonate nella sua assenza? Offriamo un servizio di segreteria telefonica e  trasmetteremo soltanto quelle piu importanti per lui.");
define("SER14","<b>REGALI</b><br />Puo essere  difficile trovare il regalo di compleanno addatto. Lasci  a noi crecare il regalo perfetto.");
define("SER15","<b>TRASLOCHI</b><br />I traslochi sono sempre noiosi. Possiamo gestirli per farli i meno complicato  possibile.");
define("SER16","<b>BABYSITTER</b><br />Vuole passare una serata indimenticabile con  il suo partner senza preoccuparsi dei bambini? Abbiamo delle babysitter  professioniste e con delle referenze.");
define("SER17","<b>CURA PRESA DELLE CHIAVI DEL CASA</b><br />Ha  mai perso le chivi di casa? Sa quanto costa un servizio di fabbro? Noi avremo  una copia delle chiavi per qualsiasi emergenza.");
define("SER18","<b>Il DRIVER</b><br />Arriva la sua famiglia all'aeroporto e non ha  il tempo di andare a prendrerli? Ha appuntamento con degli amici e non vuole  preoccuparsi dei controlli di alcolemia. Non c'&egrave; nessun problema, noi gli  faremo arrivare un autista.");
define("SER19","<b>POSTA</b><br />Se  non desidera che la sua sua cassetta postale sia piena mentre lei &egrave; in vacanza,  noi ci occuperemo di mandare qualcuno a prenderla e custodirla nella sua  assenza.");
define("SER20","<b>SPEDIZIONI</b><br />Ha bisogno d'inviare qualcosa dentro  della citt&agrave; di Barcelona? Noi stessi veniamo da lei a prenderlo e lo consegnamo  al pi&ugrave; presto.");
define("SER21","<b>PEDICURE/MANICURE</b><br />Non dubbi a chiederci il servizio desiderato,  noi glielo forniremo.");
define("SER22","<b>PARRUCCHIERE</b><br />Ha una urgenza? Appuntamenti non  programmati, o semplicemente vuol essere conciato a casa sua. Uno stilista  risolvera qulasiasi di questi svantaggi.");
define("SER23","<b>DIETISTA</b><br />Si faccia consigliare dai nostri  professionisti, abbiamo degli specialisti. Abbiamo contatto con dei diestisti  che seguiranno il suo caso.");
define("SER24","<b>PERSONAL TRAINER</b><br />non ha il tempo di andare in palestra? Ha  bisogno di un programma personalizzato? Noi saremo in grado di fornirglielo.");
define("SER25","<b>SPA</b><br />Si  rilassi per un momento. Lasci che gli altri si occupino di lei, noi troveremo  le migliori stazioni termali per lei.");
define("SER26","<b>MASSAGGISTI</b><br />Stress? Fatica?  Bisgno di attenzione terapeutica? Uno specialista terra  cura di lei, si sentira meglio.");
define("SER27","<b>STILISMO</b><br />Ha  un appuntamento sociale e si trova perso, oppure desidera cambiare radicalmente  il suo look? Degli stilisti consigleranno sempre avendo cura del suo benessere.");
define("SER28","<b>INTERIORISMO/EXTERIORISMO</b><br />Ha bisogno di consiglio per arredameno  interiore ed esteriore oppure preferisce che ci occupiamo di tutto? Scelga la  megliore opzione per la sua casa e goda dei risultati.");
define("SER29","<b>IDRAULICO</b><br />Se ha una urgenza noi troveremo il  proffessionale addatto per soluzionare i suoi guasti in modo veloce ed  efficace.");
define("SER30","<b>MURATORE</b><br />I piccoli lavori in casa non debbono  essere un problema, noi gli daremo soluzioni e rintracceremo il professionale  addatto.");
define("SER31","<b>L'ELETTRICISTA</b><br />Lasci solamente ai proffesionali fare  modifiche nel suo impianto elettrico. Il buon funzinamento di essa dipende di  loro. Ci faccia le sue domande.");
define("SER32","<b>PITTORE</b><br />Puo gia dimenticare dei giorni senza fine  di pittura in casa. Noi troveremo un servizio proffessionale, ci occuperemo che  tutto vada bene,, con un risultato impecabile.");
define("SER33","<b>FABBRO</b><br />Nei momenti di urgenza, non dubbi,  velocita ed efficacia. Rintracci Time  Savers.");
define("SER34","<b>LA PULIZIA DI CASA</b><br />Il personale efficace e professionale per  un servizio di uso continuato, oppure per delle date puntuale, ci rintracci e  troveremo la soluzione adatta.");
define("SER35","<b>BUCATO</b><br />Noi faremo il bucato, gli diamo garanzia  di un servizio puntuale, un tratto adatto, ed un risultato impecabile.");
define("SER36","<b>FERRO DA STIRO</b><br />Lascia  a noi prendere i suoi capi ed stirarli, garanzia di tratto e risultato  impeccabili.");
define("SER37","<b>AUTOLAVAGGIO</b><br />noi faremo il lavaggio rutinaria del su  veicolo, o se lei desidera, lo tratteremo in profondita per allungare il buon  stato del veicolo.");
define("SER38","<b>GIARDINAIO</b><br />Lasci le sue piante in mani dei  professionali, per una buona salute oppure trattele dei piccoli problemi che ci  possano essere.");
define("SER39","<b>PULIZIA CENA/CELEBRAZIONE</b><br />Dimentichi il giorno dopo. Noi faremo  sicche tutto si renda impeccabile. Ricordi solamente i buon momenti delle sue  riunioni e celebrazioni.");
define("SER40","<b>TINTORIA</b><br />Dimentichi i piccoli problemi con i suoi  capi, troveremo la soluzione piu adatta e rispettuosa con i suoi vestiti.");
define("SER41","<b>CURA DEGLI ANIMALI</b><br />Lasci le sue mascotte nelle migliori  mani. Professianili di fiducia avranno cura della sua mascotte come soltanto  lei potrebbe fare.");
?>