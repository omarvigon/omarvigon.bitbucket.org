<?
define("FECHA_1","Date");
define("FECHA_DIA","day");
define("FECHA_MES","month");
define("FECHA_ANO","year");
define("MES_1","January");
define("MES_2","February");
define("MES_3","March");
define("MES_4","April");
define("MES_5","May");
define("MES_6","June");
define("MES_7","July");
define("MES_8","August");
define("MES_9","September");
define("MES_10","October");
define("MES_11","November");
define("MES_12","December");

define("BT1","About<br />TimeSavers");
define("BT2","Services");
define("BT2A","TimeSavers");
define("BT2B","Personal Care");
define("BT2C","Home Improvement");
define("BT2D","Home Care");
define("BT3A","Credits");
define("BT3B","Prices");
define("BT3C","reservation");
define("BT4","Press");
define("BT4A","TimeSavers News");
define("BT4B","TimeSavers in the Media");
define("BT5A","Contact");
define("BT5B","Business Partner Contact");
define("BT6","Legal Notice &amp; Terms of Use");
define("BT7","Forgotten password");
define("BT8A","What does TimeSavers do?");
define("BT8B","Who are TimeSavers?");
define("BT9","Registration");
define("BT10","Enter");
define("BT11","BROWSE");
define("XHORA","Hour");
define("XMAPA","See map");
define("XCOMPRAR","Buy");
define("XCANCEL","Cancel");

define("REG_INICIO","<p>Welcome to <em class='blanco'>TimeSavers - Quality Personal Concierge</em>! You are about to begin a new chapter in your life, one in which we aspire to reduce your level of stress and increase your free time.</p>
	<p>The registration fee is 50&euro; and will be credited towards the TimeSavers Credits you decide to buy.</p>
	<p>Please fill out the following form.</p>");
define("REG_ELEGIR","(Choose)");
define("REG_TRATA","Form of address");
define("REG_TRATA_1","Mr.");
define("REG_TRATA_2","Mrs.");
define("REG_TRATA_3","Dr.");
define("REG_NOMBRE","First Name");
define("REG_APELLIDOS","Last Name");
define("REG_SEXO","Gender");
define("REG_SEXO_1","Male");
define("REG_SEXO_0","Female");
define("REG_NACIMIENTO","Date of birth");
define("REG_NACIONALIDAD","Nationality");
define("REG_DOCUMENTO","Type of identity card");
define("REG_DOCNUM","N&ordm; identity card");
define("REG_DIRECCION","Address");
define("REG_POBLACION","City");
define("REG_CP","Postal Code");
define("REG_TEL","Telephone");
define("REG_MOVIL","Mobile");
define("REG_IDIOMA","Preferred language for communication");
define("REG_MODO","Preferred manner of communication ");
define("REG_CONOCIO","How did you hear about us ?");
define("REG_CONCIO_1","Supplier");
define("REG_CONCIO_2","Friend");
define("REG_CONCIO_3","Advertising");
define("REG_CONCIO_4","Flyer");
define("REG_CONCIO_5","Television");
define("REG_CONCIO_6","Other");
define("REG_COMENTA","Remarks");
define("REG_FOTO","Your picture (JPG format)");
define("REG_CONTRA","Password");
define("REG_CONTRA2","Change Password");
define("REG_CONTRA3","Recibira su contrase&ntilde;a por e-mail registrado");
define("REG_MODIF","Update");
define("REG_BAJADAR","CANCEL MEMBERSHIP");
define("REG_BAJA","You can cancel your membership at any time by sending us an e-mail to <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, calling us at +34 93 304 00 32 or by a simple click in 'My TimeSavers'. Already bought and unused TimeSavers Credits will be reimbursed up to 30 days after the purchase date.");
define("REG_REGISTRAR","REGISTER");
define("REG_DESCRIP","Description");
define("REG_LUGAR","Place");
define("REG_DIFERENTE","if different from registered");
define("REG_CARRITO","ADD TO CART");
define("REG_FECHASER","Date of service desired");
define("REG_CODIGO","Si dispone de un  c&oacute;digo de promoci&oacute;n, introd&uacute;zcalo aqu&iacute;. El descuento se aplicar&aacute; a su primera  compra de TimeSavers Credits.");
define("REG_FIN","&iexcl;Bienvenido a <em>TimeSavers - Quality Personal Concierge</em>!<br /><br />Se le ha enviado la contrase&ntilde;a a la cuenta");

define("OLVIDO","<h3>Olvid&oacute; sus datos</h3>
        <p>Esta secci&oacute;n se encuentra temporalmente fuera de servicio. Para realizar su registro, p&oacute;ngase en contacto a trav&eacute;s del n&uacute;mero de tel&eacute;fono <b>+34 93 304 00 32</b> o env�enos un e-mail a <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>
        <p>Disculpe las molestias.</p>");

define("USER_MI","My Timesavers");
define("USER_PEDIDO","Service Query");
define("USER_PEDIR","BUY");
define("USER_SALDO_1","TimeSavers<br />Credits<br />Used");
define("USER_SALDO_2","TimeSavers<br />Credits<br />Balance");
define("USER_ACCION","Actions");
define("USER_HISTORIAL_1","Quotes");
define("USER_HISTORIAL_2","Invoices");
define("USER_IMPORTE","Price");
define("USER_DETALLES","Details");
define("USER_DATOS","Personal Information");
define("USER_NOTICIAS","News");
define("USER_RECOMENDA","Recommendations");
define("USER_RECOMENDA_2","Type of recommendation");
define("USER_RECOMENDA_10","Servicio deseado");
define("USER_RECOMENDA_20","Servicio ofrecido");
define("USER_RECOMENDA_30","Incidencia solicitud");
define("USER_RECOMENDA_40","Forma de trabajo");
define("USER_RECOMENDA_3","RECOMMEND");
define("USER_RECOMENDACION","<p>In <em>TimeSavers &ndash; Quality Personal Concierge</em> we care  about your opinion. Our members&rsquo; maximum satisfaction is our primordial goal. Of course, the best way to achieve this is to listen to you and ask for you  collaboration.</p>
		<p>To this end we have put at your disposal this  recommendations form. Rest assured that we will respond immediately.</p>
		<p>Naturally, you may also contact us via telephone at <b>+34  93 304 00 32</b> or send us an e-mail at <a href='mailto:info@timesavers.es'>info@timesavers.es</a> to  speak directly to one of our consultants.</p>");

define("IND_VIDEO","<object width='220' height='220'><param name='allowfullscreen' value='true' /><param name='allowscriptaccess' value='always' /><param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=1839525&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' /><embed src='http://vimeo.com/moogaloop.swf?clip_id=1839525&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='220' height='220'></embed></object>
            <p><a href='http://vimeo.com/1839525?pg=embed&amp;sec=1839525' target='_blank'>Welcome Video: TimeSavers - Quality Personal Concierge</a>.</p>");
define("IND0","Welcome to TimeSavers - Quality Personal Concierge");
define("IND1","<p>According to the Acad&eacute;mie Fran&ccedil;aise &ldquo;concierge&rdquo; originates from the 12th  century word &ldquo;cumcerges&rdquo;, meaning &ldquo;guardian&rdquo;. A concierge is defined as a  person in charge of the maintenance of a house, public building or palace.</p>
		<p>The modern &ldquo;concierge service&rdquo; started in the 1980&rsquo;s - where else but in  New York, the  city that never sleeps. It provided a solution to the growing difficulty people  experienced in handling their work-life balance.</p>
		<p>Since then the concept has been conquering the world and, at last,  <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> will offer the service in Barcelona.</p>");
define("IND2","Imagine a life without stress!");
define("IND3","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> has been created with the mission to simplify your life in order for you to be able to enjoy more free time, thereby increasing your and your family's quality of life.</p>
		<p><em>TimeSavers &ndash; Quality Personal Concierge</em> are providers of time, so that you may dedicate it to the things you are most interested in.</p>
		<p>The everyday grind of city life: meetings, shopping, work, household tasks, tiring and tedious bureaucratic procedures, and never enough time to get it all done.</p>
		<p>Can you picture a life where you can take pleasure in having your own personal assistant or concierge, always available and only a phone call or click away?</p>
		<p>In <em>TimeSavers &ndash; Quality Personal Concierge</em> we rely on efficient and discrete lifestyle professionals to make this dream of having your own personal assistant a reality.</p>");
define("IND4","&iquest;Consulte nuestro cat&aacute;logo de productos para visitantes haciendo click aqu&iacute;!");

define("LIN1","<p>Here you will find general interest links, homepages we personally are fond of and/or sites that may thrill you.</p>
		<p style='font-size:10px;'>TimeSavers &ndash; Quality Personal Concierge cannot be held responsible for the content of third party websites that can be accessed via these links.</p>");
define("LIN2","Under construction...");

define("LEG1","<p>Esta  p&aacute;gina web es propiedad de <b>TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L. </b>en lo adelante, <b>TIMESAVERS,</b> el hecho de acceder a esta p&aacute;gina implica el  conocimiento y aceptaci&oacute;n de la pol&iacute;tica de privacidad. Esta pol&iacute;tica de  privacidad forma parte de las condiciones y t&eacute;rminos de usos.<br />
          En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio,  de Servicios de la Sociedad  de la Informaci&oacute;n y Comercio Electr&oacute;nico (LSSICE) se exponen los datos  identificativos.</p>");
define("LEG2","<p><b>Condiciones  de Uso y Pol&iacute;tica de Privacidad </b></p>
        <p>1. <u>Datos Identificativos de la Empresa</u></p>
        <p>Identidad y Direcci&oacute;n:</p>
        <p>Empresa: TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.<br />CIF: B64806862 - Nacionalidad: Espa&ntilde;ola <br />Domicilio: Ronda Universitat, 15, 1&ordm; 1&ordf;,  08007 - Barcelona (Espa&ntilde;a)<br />Tel&eacute;fono: +34 304 00 32 - Fax: +34 550 44 66<br />Correo electr&oacute;nico: <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em> </p>
        <p><b>TIMESAVERS</b>, es titular del  sitio web <em>www.timesavers.es</em> y responsable de los  ficheros generados con los datos de car&aacute;cter personal suministrados por los  usuarios a trav&eacute;s de este sitio web. </p>
        <p>2. <u>Tratamiento de los datos de car&aacute;cter personal</u>.</p>
        <p>El usuario de la web autoriza a <b>TIMESAVERS</b>, el tratamiento automatizado  de los datos personales que suministre voluntariamente, a trav&eacute;s de formularios  y correo electr&oacute;nico, para: </p>
        <p>-Presupuestos relacionados con los  productos y servicios de <b>TIMESAVERS</b>.<br />-Venta, env&iacute;o y gesti&oacute;n con relaci&oacute;n a los productos y  servicios de <b>TIMESAVERS </b>comprados  por el usuario en la web.<b></b></p>
        <p>Asimismo, le  informamos que sus datos podr&aacute;n ser utilizados para enviarle informaci&oacute;n sobre  nuestros productos, ofertas y servicios en servicios personales, salvo que  usted manifieste lo contrario indic&aacute;ndolo expresamente en el formulario  correspondiente.</p>
		<p>Asimismo, queda usted  informado que parte de la informaci&oacute;n que usted nos facilite puede ser  comunicada a terceras empresas y colaboradores que TIMESAVERS pueda  subcontratar con la &uacute;nica y exclusiva finalidad de poder llevar a cabo los  servicios por usted contratados. Puede manifestarse en el formulario  correspondiente, si no quiere que se conserven sus datos m&aacute;s tiempo del  legalmente exigido.</p>
		<p>Del mismo modo, si  TIMESAVERS lo considera oportuno, y salvo que usted se manifieste en contrario,  indic&aacute;ndolo expresamente en el formulario correspondiente, conservar&aacute; sus datos  m&aacute;s del tiempo legalmente exigido, aunque no contin&uacute;e existiendo una relaci&oacute;n  comercial entre usted y TIMESAVERS, para poder llevar un control de los  servicios llevados a cabo y, si usted as&iacute; lo desea, poder continuar envi&aacute;ndole  informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS.</p>
		
        <p>El fichero creado est&aacute; ubicado en la Ronda  Universitat, 15, 1&ordm; 1&ordf;, 08007 - Barcelona (Espa&ntilde;a) bajo la supervisi&oacute;n y  control de <b>TIMESAVERS</b> quien asume la  responsabilidad en la adopci&oacute;n de medidas de seguridad de &iacute;ndole t&eacute;cnica y  organizativa de Nivel B&aacute;sico para proteger la confidencialidad e integridad de  la informaci&oacute;n, de acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999  de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal y su normativa  de desarrollo.</p>
        <p>El usuario responder&aacute;, en cualquier caso,  de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reserv&aacute;ndose <b>TIMESAVERS</b> el derecho a excluir de  los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados,  inexactos, sin perjuicio de las dem&aacute;s acciones que procedan en Derecho. </p>
        <p>Cualquier usuario puede ejercitar sus  derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos de  car&aacute;cter personal suministrados a trav&eacute;s de la web <em>www.timesavers.es</em> o por correo  electr&oacute;nico, mediante comunicaci&oacute;n escrita dirigida a <b>TIMESAVERS </b>ubicada en la Ronda Universitat, 15, 1&ordm; 1&ordf;, 08007 -  Barcelona (Espa&ntilde;a) o al correo electr&oacute;nico <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em>. </p>
        <p>3. <u>Propiedad Intelectual e Industrial:</u> El nombre de dominio <em>www.timesavers.es</em> de esta p&aacute;gina web  es propiedad de <b>TIMESAVERS</b>. El sitio  web <em>www.timesavers.es</em> en su totalidad,  incluyendo su dise&ntilde;o, estructura, distribuci&oacute;n, textos, contenidos, logotipos,  botones, im&aacute;genes, dibujos, c&oacute;digo fuente, bases de datos, as&iacute; como todos los  derechos de propiedad intelectual e industrial y cualquier otro signo  distintivo, pertenecen a <b>TIMESAVERS</b>,<b> </b>o, en su caso, a las personas o empresas que  figuran como autores o titulares de los derechos y que han autorizado a su uso a <b>TIMESAVERS</b>.<b> </b></p>
        <p>Queda prohibida la reproducci&oacute;n o  explotaci&oacute;n total o parcial, por cualquier medio, de los contenidos del portal  de <b>TIMESAVERS</b> para usos diferentes  de la leg&iacute;tima informaci&oacute;n o contrataci&oacute;n por los usuarios de los servicios  ofrecidos. En tal sentido, usted no puede copiar, reproducir, recompilar,  descompilar, desmontar, distribuir, publicar, mostrar, ejecutar, modificar,  volcar, realizar trabajos derivados, transmitir o explotar partes del servicio,  exceptuando el volcado del material del servicio y/o hacer copia para su uso  personal no lucrativo, siempre y cuando usted reconozca todos los derechos de  autor y Propiedad Industrial. La modificaci&oacute;n del contenido de los servicios  representa una violaci&oacute;n a los leg&iacute;timos derechos de <b>TIMESAVERS</b>.</p>
        <p>4. <u>Responsabilidades</u>. <b>TIMESAVERS </b>no se responsabiliza de cualquier da&ntilde;o o  perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor,  como: error en las l&iacute;neas de comunicaciones, defectos en el Hardware y Software  de los usuarios, fallos en la red de Internet (de conexi&oacute;n, en las p&aacute;ginas  enlazadas).</p>
        <p>No se garantiza que el sitio web vaya a  funcionar constante, fiable y correctamente, sin retrasos o interrupciones.</p>
        <p><b>TIMESAVERS</b> se reserva el  derecho a modificar la presente pol&iacute;tica de privacidad con objeto de adaptarla  a las posibles novedades legislativas, as&iacute; como a las que pudieran derivarse de  los c&oacute;digos tipo existentes en la materia o por motivos estrat&eacute;gicos. <b>TIMESAVERS </b>declina cualquier  responsabilidad respecto a la informaci&oacute;n de esta web procedente de fuentes  ajenas a <b>TIMESAVERS.</b> </p>
        <p><b>TIMESAVERS</b> no se  responsabiliza del mal uso que se realice de los contenidos de su p&aacute;gina web,  siendo responsabilidad exclusiva de la persona que accede a ellos o los  utiliza. De igual manera, no es responsable de que menores de edad realicen  pedidos, rellenando el formulario con datos falsos, o haci&eacute;ndose pasar por un  miembro de su familia mayor de edad o cualquier tercero mayor de edad. Debiendo  de responder ante dicho hecho el mayor de edad bajo el cual se encuentra dicho  menor, teniendo <b>TIMESAVERS</b> siempre  la posibilidad de actuar por las v&iacute;as legales que considere oportunas con la  finalidad de sancionar dicho hecho. 
        En todo caso, <b>TIMESAVERS</b>, al momento de entrega del pedido, entregar&aacute; s&oacute;lo y  &uacute;nicamente a un mayor de edad, que deber&aacute; acreditar sus datos mediante  documento de identificaci&oacute;n legal. Tampoco asume responsabilidad alguna por la  informaci&oacute;n contenida en las p&aacute;ginas web de terceros a las que se pueda acceder  a trav&eacute;s de enlaces desde la p&aacute;gina <em>www.timesavers.es</em>. La presencia de  estos enlaces tiene una finalidad informativa, y no constituyen en ning&uacute;n caso  una invitaci&oacute;n a la contrataci&oacute;n de productos o servicios que se puedan ofrecer  en la p&aacute;gina web de destino. <b>TIMESAVERS</b> no responde ni se hace cargo de ning&uacute;n tipo de responsabilidad por los da&ntilde;os y  perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y  continuidad de los sitios enlazados. 
		De igual manera, <b>TIMESAVERS</b> por este medio comunica a quien le pueda interesar que  en caso de que un tercero este interesado en colocar como enlace de su p&aacute;gina  web la direcci&oacute;n de <b>TIMESAVERS</b>, &eacute;ste  deber&aacute; de comunicarse v&iacute;a correo electr&oacute;nico <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em> solicitando la autorizaci&oacute;n para tal  efecto. </p>
        <p>5. <u>Navegaci&oacute;n con cookies</u>. El web <em>www.timesavers.es</em> utiliza cookies,  peque&ntilde;os ficheros de datos que se generan en el ordenador del usuario y que nos  permiten conocer su frecuencia de visitas, los contenidos m&aacute;s seleccionados y  los elementos de seguridad que pueden intervenir en el control de acceso a &aacute;reas  restringidas, as&iacute; como la visualizaci&oacute;n de publicidad en funci&oacute;n de criterios  predefinidos por <b>TIMESAVERS </b>y que se  activan por cookies servidas por dicha entidad o por terceros que prestan estos  servicios por cuenta de <b>TIMESAVERS</b>.</p>
        <p>El usuario tiene la opci&oacute;n de impedir la  generaci&oacute;n de cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su  programa navegador.</p>
        <p>El usuario autoriza a <b>TIMESAVERS</b> a obtener y tratar la informaci&oacute;n que se genere como  consecuencia de la utilizaci&oacute;n del Web <em>www.timesavers.es</em>, con la &uacute;nica  finalidad de ofrecerle una navegaci&oacute;n m&aacute;s personalizada.</p>
        <p>6. <u>Seguridad.</u> Este sitio web en cuanto a la confidencialidad y tratamiento de datos de  car&aacute;cter personal, por un lado, responde a las medidas de seguridad de nivel  medio establecidas en virtud de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, sobre  Protecci&oacute;n de Datos de Car&aacute;cter Personal y su Reglamento de Medidas de  Seguridad vigente, y por otro lado responde,  en cuanto a las medidas t&eacute;cnicas a la instalaci&oacute;n de &ldquo;Antivirus y  filtros antispam&rdquo; y el Secure Socket Layers &ldquo;SSL&rdquo; como plataforma segura. Todo  lo anterior a sabiendas que en la red de redes la seguridad absoluta no existe.</p>
        <p>7. <u>Confirmaci&oacute;n  de Recepci&oacute;n Pedido.</u> Una vez recibido el pedido, TimeSavers enviar&aacute; por  correo electr&oacute;nico la confirmaci&oacute;n de recepci&oacute;n del pedido; posteriormente, y  en un plazo m&aacute;ximo de 24 horas, se enviar&aacute;, mediante correo electr&oacute;nico, el  presupuesto y el plazo de entrega del/los servicio/s solicitado/s. Los pedidos  efectuados los viernes, o v&iacute;spera de festivo, se tramitar&aacute;n el siguiente d&iacute;a  laborable (los d&iacute;as laborables no incluyen fines de semana ni d&iacute;as festivos).</p>
        <p>8. <u>Formas y  condiciones de pago (impuestos).</u> En cuanto al pago, el usuario debe elegir  entre: tarjeta de cr&eacute;dito, transferencia bancaria y/o Paypal.En el  caso de compras y/o gestiones con un importe superior a 1000&euro; , se aplicar&aacute; una  preautorizaci&oacute;n por este importe ( consultar condiciones Cyberpack &ldquo;La Caixa&rdquo;  ), con la observaci&oacute;n de que en ning&uacute;n caso nos llegar&aacute; informaci&oacute;n sobre el  numero de la tarjeta de cr&eacute;dito o cuenta, estos datos pasan directamente a la  entidad financiera, que se encargar&aacute; de certificar la autenticidad de la  tarjeta y realizar el cobro.Todo a sabiendas que en la red de redes la  seguridad absoluta no existe.</p>
        <p>Todos los precios  reflejados en el portal <em>www.timesavers.es</em> incluyen el IVA (16%). Los precios est&aacute;n sujetos a cambios sin  previo aviso.</p>
        <p>9. <u>Plazos, Formas  y Gastos de env&iacute;o.</u><br />
          A partir del momento en el que sea efectiva  la recepci&oacute;n del pedido y se confirme el pago del mismo por parte del cliente,  se proceder&aacute; a la realizaci&oacute;n efectiva de la prestaci&oacute;n de los servicios  contratados en un plazo marcado en la confirmaci&oacute;n del pedido.</p>
        <p>En caso de no disponibilidad del producto o  servicio que el cliente haya solicitado a trav&eacute;s de la web <em>www.timesavers.es</em>, se proceder&aacute; a la  comunicaci&oacute;n del incidente al usuario en un m&aacute;ximo de 3 (tres) d&iacute;as, utilizando  como canal de comunicaci&oacute;n la direcci&oacute;n de correo electr&oacute;nico, o n&uacute;mero de  tel&eacute;fono de contacto, que el usuario nos haya facilitado a trav&eacute;s del  formulario de compra. En esta comunicaci&oacute;n se le informar&aacute; al cliente de la fecha  en la que el producto o servicio comprado estar&aacute; disponible o de la  imposibilidad de disponer del mismo. En caso de que el cliente no est&eacute; de  acuerdo con los nuevos plazos o sea imposible disponer del producto o servicio  contratados, se le dar&aacute; otra alternativa a la compra o se proceder&aacute; a la  devoluci&oacute;n del importe total de la misma a elecci&oacute;n del cliente.</p>
        <p>Antes de finalizar la compra y confirmar el  pedido, se podr&aacute; conocer la cuant&iacute;a de este concepto y, en caso de desearlo, el  cliente podr&aacute; desestimar el pedido.</p>
        <p>10. <u>Pago mediante tarjeta de cr&eacute;dito</u>.  (Repudio) </p>
        <p>Cuando el importe de una compra hubiese  sido cargado utilizando el n&uacute;mero de una tarjeta de cr&eacute;dito, sin que &eacute;sta  hubiese sido presentada directamente o identificada electr&oacute;nicamente, su titular  podr&aacute; exigir la inmediata anulaci&oacute;n del cargo. En tal caso, las  correspondientes anotaciones de adeudo y reabono en las cuentas del proveedor y  del titular se efectuar&aacute;n a la mayor brevedad.</p>
        <p>Sin embargo, si la compra hubiese sido efectivamente realizada por el titular  de la tarjeta y, por lo tanto, hubiese exigido indebidamente la anulaci&oacute;n del  correspondiente cargo, aqu&eacute;l quedar&aacute; obligado frente al vendedor al  resarcimiento de los da&ntilde;os y perjuicios ocasionados como consecuencia de dicha  anulaci&oacute;n. </p>
        <p>11. <u>Desistimiento.</u></p>
        <p>El comprador podr&aacute; desistir libremente del  contrato dentro del plazo de siete d&iacute;as contados desde la fecha de recepci&oacute;n  del producto. <br />El ejercicio del derecho o desistimiento no  estar&aacute; sujeto a formalidad alguna, bastando que se acredite, en cualquier forma  admitida en Derecho.<br />
        El derecho de desistimiento del comprador  no puede implicar la imposici&oacute;n de penalidad alguna, si bien, el comprador  deber&aacute; satisfacer los gastos directos de devoluci&oacute;n y en su caso, indemnizar  los desperfectos del objeto de la compra. </p>
        <p>Lo dispuesto anteriormente no ser&aacute; de  aplicaci&oacute;n a las ventas de objetos que puedan ser reproducidos o copiados con  car&aacute;cter inmediato, que se destinen a la higiene corporal o que, en raz&oacute;n de su  naturaleza, no puedan ser devueltos. </p>
        <p>12. <u>Devoluciones.</u></p>
        <p>En cuanto a las gestiones compradas a  TimeSavers, tienen una validez de 1 a&ntilde;o para ser utilizadas, aunque existe la  posibilidad de devoluci&oacute;n del importe de las gestiones compradas y NO  UTILIZADAS tras 30 dias naturales de haber realizado la compra de dichas  gestiones.<br />
        As&iacute; mismo, se pone en conocimiento del usuario  que algunos de los servicios ofrecidos por TimeSavers, ser&aacute;n subcontratados a  terceras empresas proveedoras de dichos servicios, con lo cual el usuario tendr&aacute;  el derecho a la devoluci&oacute;n de algunos de los productos finales en un plazo  m&aacute;ximo de diez (10) d&iacute;as tras la recepci&oacute;n del mismo, en virtud de la Ley de  Ordenamiento del Comercio Minorista. Pudiendo el usuario, solicitar la  sustituci&oacute;n del producto, la deducci&oacute;n del importe equivalente en su pr&oacute;ximo  pedido o el reembolso de su dinero, previa confirmaci&oacute;n por parte de <b>TIMESAVERS </b>de que no se ha utilizado,  da&ntilde;ado y/o quebrantado el producto. En cuanto a los dem&aacute;s productos (alimentos,  artesan&iacute;a) por su fragilidad y especial conservaci&oacute;n no se podr&aacute;n devolver,  excepto cuando el error se halla presentado por parte del vendedor.</p>
        <p><u>Proceso de devoluci&oacute;n:</u> el proceso de  devoluci&oacute;n es el siguiente: </p>
        <p>12.1. Escribir a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> solicitando un n&uacute;mero  de autorizaci&oacute;n de devoluci&oacute;n (se le da un No. de devoluci&oacute;n que determin&eacute; la  empresa, para llevar control de dichas actuaciones); 
        12.2. Hay que enviarle  junto con el pedido un formulario de devoluci&oacute;n adjunto, para que expliquen  motivos de la devoluci&oacute;n, productos a devolver (el producto s&oacute;lo se podr&aacute;  devolver en caso de da&ntilde;os en el mismo, equivocaci&oacute;n o no conformidad y en el  caso de productos alimenticios, artesan&iacute;as s&oacute;lo se podr&aacute; devolver por no  conformidad del comprador por error del vendedor y caso de da&ntilde;os en el mismo); 
        12.3.  Adjunto con el env&iacute;o del producto en su embalaje original, debidamente  protegido para su transporte, anexe copia de la factura m&aacute;s el formulario de  devoluci&oacute;n; 12.4. El reenv&iacute;o del producto ser&aacute; a su s&oacute;lo coste y deber&aacute;  enviarlo dentro de un plazo no superior a diez (10) d&iacute;as desde la recepci&oacute;n del  producto. Para el caso de que el producto este da&ntilde;ado o no sea lo que ha  solicitado y dicho error sea imputable al vendedor, todos los gastos correr&aacute;n  porcuenta de &eacute;ste.</p>
        <p>13. <u>Legislaci&oacute;n  y jurisdicci&oacute;n aplicable</u><u>.</u> Con car&aacute;cter general, las relaciones con los usuarios,  derivadas de la prestaci&oacute;n de servicios contenidos en esta p&aacute;gina web, con  renuncia expresa a cualquier fuero que pudiera corresponderles, se someten a la  legislaci&oacute;n y jurisdicci&oacute;n espa&ntilde;ola, espec&iacute;ficamente a los tribunales de la  ciudad de Barcelona (Espa&ntilde;a). Los usuarios de esta p&aacute;gina web son conscientes  de todo lo que se ha expuesto y lo aceptan voluntariamente.</p>");

define("EMP1","Our team at your service!");
define("EMP2","<em>TimeSavers &ndash; Quality Personal Concierge</em> has a highly qualified team with a rich and varied professional and academic background to serve as your personal assistant or concierge. Adding to this equation a diversified personal experience and entrepreneurial spirit, our team features all the resources to provide you with quick and efficient solutions.");
define("EMP3","Founder &amp; Managing Director Christoph Kraemer, of French and German  nationality, is a graduate of Yale   University. A genuine  citizen of the world, he has lived, for example, in Berlin,  New York and Cape Town. Christoph speaks fluent English,  French, German and Spanish. In 2002 he arrives in Barcelona  to study for an MBA at ESADE and, after graduation, decides to settle in Barcelona. At this point,  the paths of Christoph Kraemer and Toni Delgado cross.Christoph&rsquo;s professional background is marked by a high degree of  flexibility acquired in such different positions as working with famous artists  in renowned record companies, leading multinational teams and marketing  campaigns for a chain of jewelry stores, as well as coordinating project  management for DIY megastores.");
define("EMP4","Founder &amp; Operations Director Toni Delgado, a native of Barcelona, is a graduate  in fashion design of CETE and &Eacute;cole Sup&eacute;rieure de la Mode. A cosmopolitan by  vocation, he has lived in Paris, Madrid and Barcelona  and speaks, besides Spanish and Catalan, French and English. His professional  career has taken him from collaborating closely in international fashion fairs  and with Haute Couture designers to later concentrate in logistics and  industrial post-production processes for important companies, leaders in their  industry sector.<br /><br />The result of such distinct professional and personal experiences is a  balanced and complementary team more than qualified to face any of the needs of  our most demanding members.");
define("EMP5","Put us to work and relax!");
define("EMP6","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> provides solutions to your personal needs, travel and holiday arrangements, home care, catering for a special party or limousine service to pick up your family and friends from the airport and find them an exciting place to stay.</p>
<p>Allow us to act as your personal assistant or concierge and take your car for a check-up, to look for a present and deliver it, or simply lie back and relax while we send you an experienced masseur.</p>
<p>Dedicate yourself to more rewarding and productive tasks, and let us take your clothes to the dry cleaner, find a pet-sitter, look for and optimize the cost estimate for the renovation of your house. Additionally, we will suggest an interior decorator, should you need one.</p>
<p>Disconnect and enjoy your holidays knowing someone will be watching over your home, collecting the mail, watering your plants and responding to any urgent calls in your absence.</p>
<p><em>TimeSavers &ndash; Quality Personal Concierge</em> has been created to simplify your life. We work with business partners with a proven track record and continuously test them. Additionally, we offer special prices and promotions which are exclusively for <em>TimeSavers &ndash; Quality Personal Concierge</em> members.</p>");

define("CT0","<p>His comment has been sent.<br /><br />Thanks for your interest.<br /><br />Contact you as soon as possible.</p>");
define("CT1A","<p>In <em>TimeSavers &ndash; Quality Personal Concierge</em> we  are looking for dedicated business partners with excellent service and proven  products to satisfy the needs of our members.</p>
		<p>More than a simple commercial relationship, we  are hoping to establish a close collaboration and develop together exclusive  products that will give our members the incentive to use them.</p>
		<p>Interested? Then don&rsquo;t hesitate to fill out  the form and we&rsquo;ll get back to you immediately!</p>");
define("CT1B","In <em>TimeSavers &ndash; Quality Personal Concierge</em> our mission is to be at your service and we are committed to total transparency. So please do not hesitate to contact us and explain your doubts or queries. We will get back to you immediately.");
define("CT2","Any data given to us will be handled confidentially, in accordance with our <a href='legal.php'>Privacy Policy</a> and the Spanish Ley Org&aacute;nica 15/99 de Protecci&oacute;n de Datos Personales.");
define("CT3","I have read and accept the <a href='legal.php'>Legal Note and Terms of Use</a> of the website TimeSavers &ndash; Quality Personal Concierge.");
define("CT4","I don&rsquo;t want to receive information concerning products, offers or services from TIMESAVERS QUALITY CONCIERGE SERVICE, S.L.");
define("CT5","I don&rsquo;t want my data to be kept in TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. any longer than is required by Spanish law.");
define("CT6","I don&rsquo;t want to receive information concerning products, offers or services from TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. once our commercial relationship has ended.");
define("CONT1","First & Last Name");
define("CONT2","Company Name");
define("CONT3","Type of service/product");
define("CONT6","Mandatory fields");
define("CONT7","SEND");

define("FAQ1","Why TimeSavers ?");
define("FAQ2","Is there something TimeSavers cannot organize?");
define("FAQ3","Does TimeSavers apply any surcharge to the prices of its suppliers?");
define("FAQ4","Is it possible to place last minute orders?");
define("FAQ5","What steps are to be followed in order to place an order?");
define("FAQ6","Where will TimeSavers operate?");
define("FAQ7","Can I contact TimeSavers at any time?");
define("FAQ8","Is TimeSavers the provider of all offered services?");
define("FAQ9","What guarantees do I have, if the service is outsourced to a third party?");
define("FAQ10","How do I become a member/cancel my membership?");
define("FAQ11","Once bought how much time do I have to use TimeSavers products?");
define("FAQ12","What is TimeSavers data protection policy?");
define("FAQ13","&iquest;C&oacute;mo  funciona TimeSavers?");

define("FAQ1_X","It is an affordable service that will permit you to delegate necessary tasks, which you don't have the time or possibility to take care of yourself. As a result, you will be able to reduce your stress level and increase your free time. <em>TimeSavers &ndash; Quality Personal Concierge</em> is committed to carrying out your orders according to your wishes and instructions as long as we don't have any suggestions for a better solution. We hope that in time we will win your trust and will become an integral part of your life management system.");
define("FAQ2_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> will endeavor to the utmost to satisfy any wish of its members as long as it's legal and moral. Our team has a broad and diversified background and we are 100% dedicated to finding a solution to any order placed. In case, for example, we receive a last minute order for a reservation in one of the hippest restaurants on Friday night, we will, through our extensive network of contacts, do everything possible to get you that table. Should we despite all not be able to obtain your reservation, we will present you with a solution that will please you at least as much.");
define("FAQ3_X","The answer is a definite NO! Our members will at the very least benefit from the official tariffs of our suppliers. Moreover, we are committed to establishing agreements to provide our members with rebates and promotional offers on our services. We will also always take into account a member's profile and history in suggesting a solution that is adequate to his/her possibilities.");
define("FAQ4_X","It goes without saying that the more time we have to prepare a solution to an order, the better our proposal will be. Nevertheless, we will attempt to satisfy any wish no matter the advance notice. And in case of not being able to execute your order 100% according to your desires, trust us to make an equivalent or even better suggestion!");
define("FAQ5_X","The process is easy and fast. Send us your query or order via e-mail at <a href='mailto:info@timesavers.es'>info@timesavers.es</a> or simply call us at +34 93 304 00 32, we will provide you with a solution customized to your needs as quickly as possible. Once you validate the solution/quote via mail/phone and we receive payment, we will proceed to carry out your order.");
define("FAQ6_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> will offer its services in Barcelona Capital. ");
define("FAQ7_X","Our business hours are Monday to Friday from 9 am to 8 pm. Of course, a 24/24 service is possible. Orders carried out outside of our business hours will receive a 20% mark-up.");
define("FAQ8_X","We offer all our services with a commitment to excellence and without mistakes. Some services will be rendered directly by <em>TimeSavers &ndash; Quality Personal Concierge</em> and some will be outsourced to third parties. All outsourced services will be carried out by our qualified and tested business partners. ");
define("FAQ9_X","As far as guarantees, <em>TimeSavers &ndash; Quality Personal Concierge</em> operates with a Civil Responsibility Insurance ('seguro de responsabilidad civil'), which protects any transaction, and will demand the same of its business partners.");
define("FAQ10_X","You become a member by paying a one-time inscription fee of 50&euro; during the inscription process. This fee is subsequently credited to your purchase of TimeSavers Credits. <br>You can cancel your membership at any time by sending us an email, calling us or by a simple click in 'My TimeSavers'. Already bought and unused TimeSavers Credits will be reimbursed up to 30 days after their purchase date. ");
define("FAQ11_X","From the purchase date any TimeSavers product is valid for 1 calendar year.");
define("FAQ12_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> complies with Spanish law in regards to data protection. You may access, rectify, oppose or cancel your personal data at any time by sending us a mail to <a href='mailto:info@timesavers.es'>info@timesavers.es</a> or calling us at +34 93 304 00 32. For more detailed information, please consult our section <a title='Legal' href='legal.php'>Legal Notice &amp; Terms of Use</a>. If you need help with this section, please do not hesitate to contact us.");
define("FAQ13_X","<em>TimeSavers &ndash; Quality  Personal Concierge</em> funciona a base de cr&eacute;ditos. La gesti&oacute;n de cada servicio por  parte nuestra requiere un cierto n&uacute;mero de TimeSavers Credits. Dichos cr&eacute;ditos  se compran en el apartado &ldquo;Mi TimeSavers&rdquo; una vez que se haya dado de alta como  miembro TimeSavers. Para m&aacute;s informaci&oacute;n sobre TimeSavers Credits haga click <a href='http://www.timesavers.es/gestiones.php'>aqu&iacute;</a>.");

define("GEST0A","<em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> is based on  a credits system and the handling of each service requires a certain number of  credits. Credits  can be bought in the &ldquo;My TimeSavers&rdquo; area once you have signed up as a  TimeSavers member.<br><br>Each  time you request a service, we will send you a quote which details the expected  number of credits required for the handling of the service. It goes without  saying that our quotes are absolutely free of charge. If the requested service  entails the contribution of one of our external collaborators the quote will  clearly differentiate between the cost of the collaborator and the TimeSavers  Credits needed for handling it. 
You will be asked to approve the quote by mail  or via &ldquo;My TimeSavers&rdquo; and pay 50% up front. Once the service is completed,  <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> will send you an invoice stating the  TimeSavers Credits and the service cost in fact used. All payments can be made  directly through &ldquo;My TimeSavers&rdquo;.<br><br><em class='blanco'>TimeSavers  &ndash; Quality Personal Concierge</em> would like to highlight that with almost all of  our external collaborators we have negotiated an added value for our members,  be it a discount, a special offer or an exclusive benefit.<br><br>All  prices are including VAT.<br>TimeSavers  Credits are valid for one calendar year starting from the purchase date.");
define("GEST0","<p><em>Fast and easy: Send us your query or order via e-mail at <a href='mailto:info@timesavers.es'>info@timesavers.es</a> or simply call us at +34 93 304 00 32, we  will provide you with a solution customized to your needs as quickly as  possible.</em><br><br><b>TimeSavers Credits Prices</b><br><br>Next you will find a list of TimeSavers Credit  packages:</p>
	    <table cellpadding='4' cellspacing='1'>
		<tr class='filazul_1'><td>1  TimeSavers Credit</td><td>12,00 &euro;</td></tr>
		<tr class='filazul_2'><td>5  TimeSavers Credits</td><td>58,00 &euro;</td></tr>
		<tr class='filazul_1'><td>10  TimeSavers Credits</td><td>112,00 &euro;</td></tr>
		<tr class='filazul_2'><td>25  TimeSavers Credits</td><td>265,00 &euro;</td></tr>
		<tr class='filazul_1'><td>50  TimeSavers Credits</td><td>480,00 &euro;</td></tr>
		<tr class='filazul_2'><td>100 TimeSavers  Credits</td><td>840,00 &euro;</td></tr>
		</table>
	    <p>If you are interested in purchasing a more substantial  package of TimeSavers Credits, please contact us by calling +34 93 304 00 32 or  send us an e-mail at <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");
define("GEST1","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> offers a  monthly subscription option. You can chose between two existing packages or we  can design a package specifically tailored to your needs. The TimeSavers  Monthly subscription includes as benefits free restaurant reservations and free  handling of your personal agenda without, of course, having to renounce any of  the advantages available to other TimeSavers members.</p>
<p><b>TimeSavers Monthly Basic</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Up to 16 TimeSavers Credits</td><td>120,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Includes free restaurant reservations and free handling of personal agenda</td></tr>
<tr class='filazul_1'><td colspan='2'>Customer service Mondays to Fridays from 10 am to 8 pm.</td></tr>
<tr class='filazul_2'><td colspan='2'>Travel expenses in Barcelona city included.</td></tr>
</table>
<p><b>TimeSavers Monthly Premium</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Up to 40 TimeSavers Credits</td><td>250,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Includes free restaurant reservations and free handling of personal agenda</td></tr>
<tr class='filazul_1'><td colspan='2'>Customer service 24/7.</td></tr>
<tr class='filazul_2'><td colspan='2'>Travel expenses in Barcelona city included.</td></tr>
</table>
<p>Monthly subscriptions are valid for 30 days starting  from the purchase date. TimeSavers Credits not used during this period are not  credited to subsequent subscriptions.<br><br><b>Examples</b><br>In order to give you an idea of the how many  TimeSavers Credits the handling of a particular service may cost, we provide  you with the following examples:</p>
<ul>
  <li>1 restaurant reservation will cost 1 TimeSavers Credit. We have negotiated an added value for our members with all the establishments we collaborate with. Let yourself be surprised!</li>
  <li>Sending a hairdresser to your place represents 2 TimeSavers Credits the first time you ask for the service plus the cost of the professional. When you repeat the same service with the same company or professional, the handling fee will be 1 TimeSavers Credit for the planning of your personal agenda. </li>
  <li>Taking care of your car&rsquo;s ITV renewal usually takes 2 to 3 hours signifying an approximate cost of 8 to 12 TimeSavers Credits. Before initiating the process, we will provide you with a quote and an estimate of required TimeSavers Credits. You will have to approve this quote and pay 50% up front. Once the ITV renewal is completed, we check the credits in fact used and send you a corresponding invoice, minus the initial payment, which can be paid directly in &ldquo;My TimeSavers&rdquo;.</li>
</ul>");
define("GEST2","Enjoy Barcelona in a different way and make the most of your stay!</em><em> TimeSavers &ndash; Quality Personal Concierge</em> offers visitors to Barcelona different established packages at extremely  competitive prices, like &ldquo;Barcelona by Sea&rdquo; or &ldquo;Barcelona by Air&rdquo;, as well as  special packages designed according to your personal needs and desires.<br><br>If you are interested or would like more information, please contact us by  calling +34 93 304 00 32 or by sending us a mail to <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.");
define("GEST3","&iquest;Why not give a present that is truly useful? Finding  an interesting, innovative and different present can be quite stressful. Aside  from calling us to help you in the search for that particular present, <em>TimeSavers &ndash; Quality Personal Concierge</em> also offers the possibility to surprise  your partner, family member or friend with a voucher TimeSavers Coupon.<br><br>When you buy a package of TimeSavers Credits, you be  given the option to select &ldquo;TimeSavers Coupon&rdquo;. Giving us the mail or address  of the recipient, we will send the person their voucher, which will only bear  the number of TimeSavers Credits.");
define("GEST4","According to Reuters we tend to dedicate 10% of our work time to handling  personal or domestic chores. <em>TimeSavers &ndash; Quality Personal Concierge</em> can be a  tool in your company to improve the work-life balance of your employees. With  our service your employees will have less worries and stress which will allow  them to be more dedicated at the work place. In fact a study by the Russell  Investment Group found that the 100 US companies with the best work environment  increased their stock market value 230% more than other Wall Street companies  (1986-2006).<br><br><em>TimeSavers &ndash; Quality Personal Concierge</em> can, moreover, be an excellent  way to captivate and maintain VIP clients.<br><br>If you are interested or would like more  information, please contact us by calling +34 93 304 00 32 or by sending us a  mail to <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.<br><br>All  prices are including VAT.");

define("SER_1","Internet search");
define("SER_2","Dinner organization");
define("SER_3","Catering");
define("SER_4","Wait for delivery");
define("SER_5","Dog walker");
define("SER_6","Bureaucratric procedures");
define("SER_7","Event organization");
define("SER_8","Personal agenda");
define("SER_9","Personal shopper");
define("SER_10","Travel organization");
define("SER_11","Restaurant reservation");
define("SER_12","Ticket reservation");
define("SER_13","Call answering service");
define("SER_14","Buying gifts");
define("SER_15","Moving");
define("SER_16","Babysitter");
define("SER_17","Safe-keeping house keys");
define("SER_18","Chauffeur");
define("SER_19","Mail pickup");
define("SER_20","Courier service");
define("SER_21","Pedicure / manicure");
define("SER_22","Hairdresser");
define("SER_23","Nutritionist");
define("SER_24","Personal trainer");
define("SER_25","Spa");
define("SER_26","Masseur / masseuse");
define("SER_27","Image adviser");
define("SER_28","Interior / exterior decorator");
define("SER_29","Plumber");
define("SER_30","Mason");
define("SER_31","Electrician");
define("SER_32","Painter");
define("SER_33","Locksmith");
define("SER_34","Cleaning service");
define("SER_35","Washing clothes");
define("SER_36","Ironing");
define("SER_37","Car wash");
define("SER_38","Gardener");
define("SER_39","Cleaning after dinner/party");
define("SER_40","Dry-cleaner");
define("SER_41","Pet care");

define("SER0","Should you be interested in a service that does not appear in our list, please do not hesitate to call us at +34 93 304 00 32 or e-mail us at <a href='mailto:info@timesavers.es' class='blanco'><b>info@timesavers.es</b></a>. We promise to find you a solution in the shortest possible delay.");
define("SER1","<b>INTERNET SEARCH</b><br />Filtering millions of results looking for relevant information may be a long and tedious process. We rely on efficient experts who will take care of it for you.");
define("SER2","<b>DINNER ORGANIZATION</b><br />Do you need to entertain a group of people at home and are you missing the adequate china, glasses, table center piece and the talent of a Michelin-Chef? Let us organize it all for you, so that all you have to do is enjoy the menu and your guests.");
define("SER3","<b>CATERING</b><br />Kitchen equals nightmare for you? We rely on experienced chefs and excellent catering services so that you don&rsquo;t have to worry.");
define("SER4","<b>WAIT FOR DELIVERY</b><br />If you&rsquo;ve ever had to wait for a home delivery of, for example, a new television, you know that the delivery company usually tells you they&rsquo;ll stop by between 9 am and 3 pm. But what if it&rsquo;s a working day for you? We offer to wait in your place for the delivery in order for you not to lose any time.");
define("SER5","<b>DOG WALKER</b><br />Your best friend needs to run and play in the park? We have professionals to do it for you.");
define("SER6","<b>BUREAUCRATRIC PROCEDURES</b><br />Bureaucratic procedures are usually long and tedious. Delegate them to us! We count on professionals with experience to handle it all. ");
define("SER7","<b>EVENT ORGANIZATION</b><br />Do you need to organize a special birthday or anniversary, a wedding, and do you lack the necessary time to handle it all? We have a team of professionals at your service who will organize an unforgettable event for you.");
define("SER8","<b>PERSONAL AGENDA</b><br />Remembering all your reunions, birthdays and dinners can result quite stressful. Leave it to us to organize your personal agenda and you will never forget an important date again.");
define("SER9","<b>PERSONAL SHOPPER</b><br />Going shopping is torture for you? Or do you simply lack the time to do so? We work with professionals who will advise and accompany you on a shopping tour or do the shopping for you.");
define("SER10","<b>TRAVEL ORGANIZATION</b><br />Finding the best flights, a pleasant hotel, interesting excursions in order to experience an interesting holiday often is more difficult than imagined. Just focus on doing your suitcases and let us take care of the rest. ");
define("SER11","<b>RESTAURANT RESERVATION</b><br />Are you interested in discovering a new restaurant or are you having problems in obtaining a table at the last minute? We have a diversified portfolio of establishments to choose from.");
define("SER12","<b>TICKET RESERVATION</b><br />Are you encountering problems in getting tickets to that show your partner is so interested in seeing? Trust us to move heaven and earth to find them for you.");
define("SER13","<b>CALL ANSWERING SERVICE</b><br />Do you need someone to answer your calls in your absence? We offer a call answering service and will only forward the important tones.");
define("SER14","<b>BUYING GIFTS</b><br />It can be quite difficult to find the right birthday gift at times. Trust us, we will locate the suitable gift.");
define("SER15","<b>MOVING</b><br />Moving is always tiring and time intensive. Let us handle it for you and reduce the arduousness as much as possible.");
define("SER16","<b>BABYSITTER</b><br />Would you like to surprise your partner with a romantic evening without having to worry about your child? We rely on experienced babysitters with credentials.");
define("SER17","<b>SAFE-KEEPING HOUSE KEYS</b><br />If you&rsquo;ve ever lost your house keys, you know how expensive a locksmith is. We will safe-guard a copy of your house keys for any emergency.");
define("SER18","<b>CHAUFFEUR</b><br />Your family is arriving at the airport and you lack the time to pick them up personally. You have a dinner with friends and don&rsquo;t want to preoccupy yourself with police controls. No problem, we&rsquo;ll send a chauffeur so that you don&rsquo;t have to worry.");
define("SER19","<b>MAIL PICKUP</b><br />If you do not want your mailbox to burst while you&rsquo;re on holiday, we will send someone to pick up your mail and guard it in your absence. ");
define("SER20","<b>COURIER SERVICE</b><br />Do you need to send something urgently in Barcelona Capital? We will pick up your package and deliver it.");
define("SER21","<b>PEDICURE / MANICURE</b><br />Permit yourself a little luxury and ask for this service. We will send someone to you.");
define("SER22","<b>HAIRDRESSER</b><br />An emergency? Unforeseen meetings? Or do you need someone to provide service at home? A professional will resolve any one of these situations.");
define("SER23","<b>NUTRITIONIST</b><br />Let yourself be advised by a certified professional. We rely on nutritionists who will advise and follow up on you.");
define("SER24","<b>PERSONAL TRAINER</b><br />Not enought time on your hands to go to the gym? Do you need a personalized program? Contact us and we will provide the service.");
define("SER25","<b>SPA</b><br />Permit yourself to disconnect for a moment and let someone take care of you. All you'll have to do is simply lie back and relax. We will provide you with access to Barcelona's top establishments.");
define("SER26","<b>MASSEUR / MASSEUSE</b><br />Stress symptoms? Local pain? Or do you need therapeutic care? A professional will know how to treat any one of these cases and allow you to feel better.");
define("SER27","<b>IMAGE ADVISER</b><br />Do you have an important social event to attend and feel lost, or would you like to renew your image? We count on dedicated professionals to advise you and care for your well-being.");
define("SER28","<b>INTERIOR / EXTERIOR DECORATOR</b><br />Do you need a few tips on decorating your home, or would you like us to take care of it all? Choose the best option for your home and enjoy the results.");
define("SER29","<b>PLUMBER</b><br /> When there is an emergency and no time to lose. We have professionals on hand to fix your problems quickly and expertly.");
define("SER30","<b>MASON</b><br />The small touch-ups at home do not need to constitute a problem. We will provide you with solutions and look for the adequate professional.");
define("SER31","<b>ELECTRICIAN</b><br />Let only trained professionals deal with and carry out modifications in your home&rsquo;s electrical installation. Its flawless function is dependent on it.");
define("SER32","<b>PAINTER</b><br />Forget about those endless days painting your entire house. We will obtain professional service for you and make sure that everything is taken care of and the result as desired.");
define("SER33","<b>LOCKSMITH</b><br />In times of an emergency, do not hesitate. Quickness and efficiency are the answer. Please contact us.");
define("SER34","<b>CLEANING SERVICE</b><br />A competent and professional person for continuous service or only for an occasional service, consult us and we will provide you with the adequate solution.");
define("SER35","<b>WASHING CLOTHES</b><br />Let us be in charge of your washing, we guarantee you a punctual service, and an impeccable treatment and results.");
define("SER36","<b>IRONING</b><br />Let us pick up your clothes and take care of your ironing for you, we guarantee you an impeccable process and results.");
define("SER37","<b>CAR WASH</b><br />Let us be responsible for the routine care of your car, or if you wish, we can give it an in depth treatment to assure its continued well-functioning.");
define("SER38","<b>GARDENER</b><br />Leave your plants in the hands of a professional in order to keep them healthy or treat them for those little problems that may come up.");
define("SER39","<b>CLEANING AFTER DINNER/PARTY</b><br />Forget about the day after. We will be responsible to leave everything spick and span. Remember only those good and wonderful moments of your celebrations and parties.");
define("SER40","<b>DRY-CLEANER</b><br />Forget about finding the time to take your clothes to the dry-cleaner. We will find the adequate solution and assure that your clothes are returned in a flawless state.");
define("SER41","<b>PET CARE</b><br />Entrust your pets to the best. Trusted professionals will take care of them as if they were you.");
?>