<?
define("FECHA_1","Datum");
define("FECHA_DIA","tag");
define("FECHA_MES","monat");
define("FECHA_ANO","jahr");
define("MES_1","Jan");
define("MES_2","Februar");
define("MES_3","M&auml;rz");
define("MES_4","April");
define("MES_5","Mai");
define("MES_6","Juni");
define("MES_7","Juli");
define("MES_8","August");
define("MES_9","September");
define("MES_10","Oktober");
define("MES_11","November");
define("MES_12","Dezember");

define("BT1","About<br />TimeSavers");
define("BT2","Service");
define("BT2A","TimeSavers");
define("BT2B","Personal Care");
define("BT2C","Home Improvement");
define("BT2D","Home Care");
define("BT3A","Credits");
define("BT3B","Tarife");
define("BT3C","Gesti&oacute;n");
define("BT4","Presse");
define("BT4A","TimeSavers News");
define("BT4B","TimeSavers in den Medien");
define("BT5A","Kontakt");
define("BT5B","Gesch&auml;ftspartner-Kontakt");
define("BT6","Aviso Legal &amp; Condiciones de Uso");
define("BT7","Passwort vergessen?");
define("BT8A","Was macht TimeSavers?");
define("BT8B","Wer sind TimeSavers?");
define("BT9","Registrieren");
define("BT10","Enter");
define("BT11","FOTO &Auml;NDERN");
define("XHORA","Zeit");
define("XMAPA","Siehe Karte");
define("XCOMPRAR","Buy");
define("XCANCEL","Cancel");

define("REG_INICIO","<p>Willkommen bei <em>TimeSavers &ndash; Quality Personal Concierge!</em>. Sie sind kurz davor, einen neuen Lebensabschnitt zu beginnen, einen, in dem  wir hoffen, Ihren Stresslevel zu senken und Ihnen mehr freie Zeit zu  erm&ouml;glichen.</p>
  <p>Bitte melden Sie sich nun als TimeSavers-Mitglied an. Der einmalige  Mietgliedsbeitrag betr&auml;gt 50&euro; und ist mit dem Kauf von TimeSavers Credits  verrechenbar.</p>
  <p>Bitte f&uuml;llen Sie nun nachfolgendes Formular aus.</p>");
define("REG_ELEGIR","(w&auml;hlen)");
define("REG_TRATA","Anrede");
define("REG_TRATA_1","Herr");
define("REG_TRATA_2","Frau.");
define("REG_TRATA_3","Dr.");
define("REG_NOMBRE","Vorname");
define("REG_APELLIDOS","Nachname");
define("REG_SEXO","Geschlecht");
define("REG_SEXO_1","Mann");
define("REG_SEXO_0","Frau");
define("REG_NACIMIENTO","Geburtstag");
define("REG_NACIONALIDAD","Nationalit&auml;t");
define("REG_DOCUMENTO","Ausweisdokument");
define("REG_DOCNUM","Ausweis N&ordm;");
define("REG_DIRECCION","Adresse");
define("REG_POBLACION","Stadt");
define("REG_CP","PLZ");
define("REG_TEL","Telefon");
define("REG_MOVIL","Handy");
define("REG_IDIOMA","Bevorzugte Sprache f&uuml;r Kommunikation");
define("REG_MODO","Bevorzugte Kommunikationsweise");
define("REG_CONOCIO","Wie haben Sie von uns erfahren?");
define("REG_CONCIO_1","Gesch&auml;ftspartner");
define("REG_CONCIO_2","Freund");
define("REG_CONCIO_3","Anzeige");
define("REG_CONCIO_4","Flyer");
define("REG_CONCIO_5","TV");
define("REG_CONCIO_6","Anders");
define("REG_COMENTA","Bemerkungen");
define("REG_FOTO","Ihr Foto : (JPG Format)");
define("REG_CONTRA","Passwort");
define("REG_CONTRA2","Passwort &auml;ndern");
define("REG_CONTRA3","Ein Passwort wird an die registrierte E-mail geschickt");
define("REG_MODIF","Update");
define("REG_BAJADAR","MITGLIEDSCHAFT BEENDEN");
define("REG_BAJA","Sie k&ouml;nnen jederzeit Ihre Mitgliedschaft  beenden, in dem Sie uns eine Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a> schicken  oder durch einen simplen Klick in &ldquo;Mein TimeSavers&rdquo;. Gekaufte und noch nicht  benutzte TimeSavers Credits werden bis zu 30 Tage nach Kaufdatum zur&uuml;ckerstattet.");
define("REG_REGISTRAR","REGISTRIEREN");
define("REG_DESCRIP","Beschreibung");
define("REG_LUGAR","Ort");
define("REG_DIFERENTE","falls anders, als registriert");
define("REG_CARRITO","EINKAUFSWAGEN ZUF&Uuml;GEN");
define("REG_FECHASER","Gew&uuml;nschtes Datum<br />des Service");
define("REG_CODIGO","Sollten Sie einen  Promotionkode haben, geben Sie ihn bitte hiere in. Der Rabatt wird auf Ihren  erste Kauf von TimeSavers Credits angewandt.");
define("REG_FIN","Willkommen bei  <em>TimeSavers &ndash; Quality Personal Concierge!</em><br /><br />Ein Passwort  wurde an die E-mail-Adresse geschickt.");

define("OLVIDO","<h3>Sollten Sie Ihr User E-mail/Passwort </h3>
        <p>Sollten Sie Ihr  User E-mail/Passwort vergessen haben, rufen Sie uns bitte unter +34 93 304 00  32 an oder schicken Sie uns ein Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a>.</p>");

define("USER_MI","Mein Timesavers");
define("USER_PEDIDO","Serviceanfrage");
define("USER_PEDIR","BESTELLEN");
define("USER_SALDO_1","Benutzte<br />TimeSavers<br />Credits");
define("USER_SALDO_2","Saldo<br />TimeSavers<br />Credits");
define("USER_ACCION","Aktion");
define("USER_HISTORIAL_1","Kostenvoranschl&auml;ge");
define("USER_HISTORIAL_2","Rechnungen");
define("USER_IMPORTE","Betrag");
define("USER_DETALLES","Details");
define("USER_DATOS","Pers&ouml;nliche Daten");
define("USER_NOTICIAS","News");
define("USER_RECOMENDA","Empfehlungen");
define("USER_RECOMENDA_2","Art der Empfehlung");
define("USER_RECOMENDA_10","Erw&uuml;nschte Services");
define("USER_RECOMENDA_20","Angebotene Services");
define("USER_RECOMENDA_30","Anfragevorfall");
define("USER_RECOMENDA_40","Arbeitsweise");
define("USER_RECOMENDA_3","EMPFEHLEN");
define("USER_RECOMENDACION","<p>F&uuml;r <em>TimeSavers &ndash; Quality Personal Concierge</em> z&auml;hlt Ihre Meinung. Die  Zufriedenheit unserer Mitglieder ist unsere Mission. Selbstverst&auml;ndlich ist die  beste Art, dies zu erreichen, Ihnen zuzuh&ouml;ren und Sie um Ihre Mitarbeit zu  bitten.</p>
  <p>Mit diesem Ziel erm&ouml;glichen wir Ihnen, anhand dieses Formulars mit uns in  Kontakt zu treten, um uns Ihre Verbesserungsvorschl&auml;ge zu machen. Vertrauen Sie  darauf, dass wir Sie daraufhin umgehen kontaktieren.</p>
  <p>Gerne k&ouml;nnen Sie sich auch mit uns telefonisch unter der Telefonnummer <b>+34 93 304 00 32</b> oder per Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a> in Verbindung setzen und direkt mit einem unserer Berater sprechen.</p>");

define("IND_VIDEO","<object width='220' height='220'><param name='allowfullscreen' value='true' /><param name='allowscriptaccess' value='always' /><param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=2941497&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=59a5d1&amp;fullscreen=1' /><embed src='http://vimeo.com/moogaloop.swf?clip_id=2941497&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=59a5d1&amp;fullscreen=1' type='application/x-shockwave-flash' allowfullscreen='true' allowscriptaccess='always' width='220' height='220'></embed></object>
            <p><a href='http://vimeo.com/'>Willkommensvideo: Timesavers - Quality Personal Concierge</a>.</p>");
define("IND0","Willkommen bei TimeSavers &ndash; Quality Personal Concierge");
define("IND1","<p>Laut Acad&eacute;mie Fran&ccedil;aise hat &ldquo;Concierge&rdquo; seinen Ursprung in dem mittelalterlichen  Wort &ldquo;cumcerges&rdquo;, dessen Bedeutung H&uuml;ter ist. Ein Concierge wiederum wird als Person  definitiert, die sich um ein Haus, ein &ouml;ffentliches Geb&auml;ude oder einen Palast  k&uuml;mmert.</p>
  <p>Die modernen &bdquo;Concierge Services&ldquo; begannen in den 80er Jahren wo sonst als in  New York, der Stadt die niemals schl&auml;ft. Sie waren und sind die Antwort auf den  steigenden Stress im Alltagsleben und den Wunsch, Arbeits- und Privatleben verst&auml;rkt  in Einklang zu bringen.</p>
  <p>Seitdem hat das Konzept die Welt erobert und jetzt, endlich, bietet <em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> diesen Service in Barcelona an.</p>");
define("IND2","Stellen Sie sich ein Leben ohne Stress vor!");
define("IND3","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> ist mit der Mission gegr&uuml;ndet  worden, Ihr Leben leichter zu machen, damit Sie sich an mehr freier Zeit  erfreuen und Ihre Lebensqualit&auml;t, sowie die Ihrer Familie, verbessern k&ouml;nnen.</p>
  <p><em>TimeSavers &ndash; Quality Personal Concierge</em> sind Lieferanten von Zeit, damit  Sie dieses heutzutage kostbare Gut dem widmen k&ouml;nnen, was Sie am meisten  interessiert.</p>
  <p>Der Alltag in der Gro&szlig;stadt: voll von t&auml;glichen Pflichten, Meetings,  Eink&auml;ufen, m&uuml;hsamen Beh&ouml;rdeng&auml;ngen&hellip;.</p>
  <p>K&ouml;nnen Sie Sich ein Leben vorstellen, in dem Sie auf Ihren eigenen  pers&ouml;nlichen Assistenten oder Concierge zur&uuml;ckgreifen k&ouml;nnen, immer bereit und  nur einen einfachen Anruf oder Klick weit entfernt?</p>
  <p>Bei <em>TimeSavers &ndash; Quality Personal Concierge</em> k&ouml;nnen Sie sich Sie sich auf  ein effizientes und diskretes Team von Experten verlassen, die diesen Traum vom  eigenen pers&ouml;nlichen Assistenten oder Concierge wahr machen. </p>");
define("IND4","Bitte informieren Sie sich  &uuml;ber unseren Produktkatalog f&uuml;r Besucher mit einem Klick hier!");

define("LIN1","<p>Hier finden Sie Links zu den Homepages, die uns pers&ouml;nlich faszinieren  und/oder von denen Sie vielleicht begeistert sein warden.</p>
  <p style='font-size:10px;'>TimeSavers &ndash; Quality Personal Concierge kann nicht f&uuml;r den Inhalt der  Websites von Fremdanbietern, der &uuml;ber die Links zug&auml;nglich ist, verantwortlich  gemacht werden.</p>");
define("LIN2","Under construction...");

define("LEG1","<p>Esta  p&aacute;gina web es propiedad de <b>TIMESAVERS  QUALITY CONCIERGE SERVICE, S.L. </b>en lo adelante, <b>TIMESAVERS,</b> el hecho de acceder a esta p&aacute;gina implica el  conocimiento y aceptaci&oacute;n de la pol&iacute;tica de privacidad. Esta pol&iacute;tica de  privacidad forma parte de las condiciones y t&eacute;rminos de usos.<br />
          En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio,  de Servicios de la Sociedad  de la Informaci&oacute;n y Comercio Electr&oacute;nico (LSSICE) se exponen los datos  identificativos.</p>");
define("LEG2","<p><b>Condiciones  de Uso y Pol&iacute;tica de Privacidad </b></p>
        <p>1. <u>Datos Identificativos de la Empresa</u></p>
        <p>Identidad y Direcci&oacute;n:</p>
        <p>Empresa: TIMESAVERS QUALITY CONCIERGE  SERVICE, S.L.<br />CIF: B64806862 - Nacionalidad: Espa&ntilde;ola <br />Domicilio: Ronda Universitat, 15, 1&ordm; 1&ordf;,  08007 - Barcelona (Espa&ntilde;a)<br />Tel&eacute;fono: +34 304 00 32 - Fax: +34 550 44 66<br />Correo electr&oacute;nico: <em><a href='mailto:info@timesavers.es'>info@timesavers.es</a></em></p>
        <p><b>TIMESAVERS</b>, es titular del  sitio web <em>www.timesavers.es</em> y responsable de los  ficheros generados con los datos de car&aacute;cter personal suministrados por los  usuarios a trav&eacute;s de este sitio web. </p>
        <p>2. <u>Tratamiento de los datos de car&aacute;cter personal</u>.</p>
        <p>El usuario de la web autoriza a <b>TIMESAVERS</b>, el tratamiento automatizado  de los datos personales que suministre voluntariamente, a trav&eacute;s de formularios  y correo electr&oacute;nico, para: </p>
        <p>-Presupuestos relacionados con los  productos y servicios de <b>TIMESAVERS</b>.<br />-Venta, env&iacute;o y gesti&oacute;n con relaci&oacute;n a los productos y  servicios de <b>TIMESAVERS </b>comprados  por el usuario en la web.<b></b></p>
        <p>Asimismo, le  informamos que sus datos podr&aacute;n ser utilizados para enviarle informaci&oacute;n sobre  nuestros productos, ofertas y servicios en servicios personales, salvo que  usted manifieste lo contrario indic&aacute;ndolo expresamente en el formulario  correspondiente.</p>
		<p>Asimismo, queda usted  informado que parte de la informaci&oacute;n que usted nos facilite puede ser  comunicada a terceras empresas y colaboradores que TIMESAVERS pueda  subcontratar con la &uacute;nica y exclusiva finalidad de poder llevar a cabo los  servicios por usted contratados. Puede manifestarse en el formulario  correspondiente, si no quiere que se conserven sus datos m&aacute;s tiempo del  legalmente exigido.</p>
		<p>Del mismo modo, si  TIMESAVERS lo considera oportuno, y salvo que usted se manifieste en contrario,  indic&aacute;ndolo expresamente en el formulario correspondiente, conservar&aacute; sus datos  m&aacute;s del tiempo legalmente exigido, aunque no contin&uacute;e existiendo una relaci&oacute;n  comercial entre usted y TIMESAVERS, para poder llevar un control de los  servicios llevados a cabo y, si usted as&iacute; lo desea, poder continuar envi&aacute;ndole  informaci&oacute;n acerca de los productos, ofertas y servicios de TIMESAVERS.</p>
        
        <p>El fichero creado est&aacute; ubicado en la Ronda  Universitat, 15, 1&ordm; 1&ordf;, 08007 - Barcelona (Espa&ntilde;a) bajo la supervisi&oacute;n y  control de <b>TIMESAVERS</b> quien asume la  responsabilidad en la adopci&oacute;n de medidas de seguridad de &iacute;ndole t&eacute;cnica y  organizativa de Nivel B&aacute;sico para proteger la confidencialidad e integridad de  la informaci&oacute;n, de acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999  de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal y su normativa  de desarrollo.</p>
        <p>El usuario responder&aacute;, en cualquier caso,  de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reserv&aacute;ndose <b>TIMESAVERS</b> el derecho a excluir de  los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados,  inexactos, sin perjuicio de las dem&aacute;s acciones que procedan en Derecho. </p>
        <p>Cualquier usuario puede ejercitar sus  derechos de acceso, rectificaci&oacute;n, oposici&oacute;n y cancelaci&oacute;n de sus datos de  car&aacute;cter personal suministrados a trav&eacute;s de la web <em>www.timesavers.es</em> o por correo  electr&oacute;nico, mediante comunicaci&oacute;n escrita dirigida a <b>TIMESAVERS </b>ubicada en la Ronda Universitat, 15, 1&ordm; 1&ordf;, 08007 -  Barcelona (Espa&ntilde;a) o al correo electr&oacute;nico <em>info@timesavers.es</em>. </p>
        <p>3. <u>Propiedad Intelectual e Industrial:</u> El nombre de dominio <em>www.timesavers.es</em> de esta p&aacute;gina web  es propiedad de <b>TIMESAVERS</b>. El sitio  web <em>www.timesavers.es</em> en su totalidad,  incluyendo su dise&ntilde;o, estructura, distribuci&oacute;n, textos, contenidos, logotipos,  botones, im&aacute;genes, dibujos, c&oacute;digo fuente, bases de datos, as&iacute; como todos los  derechos de propiedad intelectual e industrial y cualquier otro signo  distintivo, pertenecen a <b>TIMESAVERS</b>,<b> </b>o, en su caso, a las personas o empresas que  figuran como autores o titulares de los derechos y que han autorizado a su uso a <b>TIMESAVERS</b>.<b> </b></p>
        <p>Queda prohibida la reproducci&oacute;n o  explotaci&oacute;n total o parcial, por cualquier medio, de los contenidos del portal  de <b>TIMESAVERS</b> para usos diferentes  de la leg&iacute;tima informaci&oacute;n o contrataci&oacute;n por los usuarios de los servicios  ofrecidos. En tal sentido, usted no puede copiar, reproducir, recompilar,  descompilar, desmontar, distribuir, publicar, mostrar, ejecutar, modificar,  volcar, realizar trabajos derivados, transmitir o explotar partes del servicio,  exceptuando el volcado del material del servicio y/o hacer copia para su uso  personal no lucrativo, siempre y cuando usted reconozca todos los derechos de  autor y Propiedad Industrial. La modificaci&oacute;n del contenido de los servicios  representa una violaci&oacute;n a los leg&iacute;timos derechos de <b>TIMESAVERS</b>.</p>
        <p>4. <u>Responsabilidades</u>. <b>TIMESAVERS </b>no se responsabiliza de cualquier da&ntilde;o o  perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor,  como: error en las l&iacute;neas de comunicaciones, defectos en el Hardware y Software  de los usuarios, fallos en la red de Internet (de conexi&oacute;n, en las p&aacute;ginas  enlazadas).</p>
        <p>No se garantiza que el sitio web vaya a  funcionar constante, fiable y correctamente, sin retrasos o interrupciones.</p>
        <p><b>TIMESAVERS</b> se reserva el  derecho a modificar la presente pol&iacute;tica de privacidad con objeto de adaptarla  a las posibles novedades legislativas, as&iacute; como a las que pudieran derivarse de  los c&oacute;digos tipo existentes en la materia o por motivos estrat&eacute;gicos. <b>TIMESAVERS </b>declina cualquier  responsabilidad respecto a la informaci&oacute;n de esta web procedente de fuentes  ajenas a <b>TIMESAVERS.</b> </p>
        <p><b>TIMESAVERS</b> no se  responsabiliza del mal uso que se realice de los contenidos de su p&aacute;gina web,  siendo responsabilidad exclusiva de la persona que accede a ellos o los  utiliza. De igual manera, no es responsable de que menores de edad realicen  pedidos, rellenando el formulario con datos falsos, o haci&eacute;ndose pasar por un  miembro de su familia mayor de edad o cualquier tercero mayor de edad. Debiendo  de responder ante dicho hecho el mayor de edad bajo el cual se encuentra dicho  menor, teniendo <b>TIMESAVERS</b> siempre  la posibilidad de actuar por las v&iacute;as legales que considere oportunas con la  finalidad de sancionar dicho hecho. 
        En todo caso, <b>TIMESAVERS</b>, al momento de entrega del pedido, entregar&aacute; s&oacute;lo y  &uacute;nicamente a un mayor de edad, que deber&aacute; acreditar sus datos mediante  documento de identificaci&oacute;n legal. Tampoco asume responsabilidad alguna por la  informaci&oacute;n contenida en las p&aacute;ginas web de terceros a las que se pueda acceder  a trav&eacute;s de enlaces desde la p&aacute;gina <em>www.timesavers.es</em>. La presencia de  estos enlaces tiene una finalidad informativa, y no constituyen en ning&uacute;n caso  una invitaci&oacute;n a la contrataci&oacute;n de productos o servicios que se puedan ofrecer  en la p&aacute;gina web de destino. <b>TIMESAVERS</b> no responde ni se hace cargo de ning&uacute;n tipo de responsabilidad por los da&ntilde;os y  perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y  continuidad de los sitios enlazados. 
		De igual manera, <b>TIMESAVERS</b> por este medio comunica a quien le pueda interesar que  en caso de que un tercero este interesado en colocar como enlace de su p&aacute;gina  web la direcci&oacute;n de <b>TIMESAVERS</b>, &eacute;ste  deber&aacute; de comunicarse v&iacute;a correo electr&oacute;nico <em>info@timesavers.es</em> solicitando la autorizaci&oacute;n para tal  efecto. </p>
        <p>5. <u>Navegaci&oacute;n con cookies</u>. El web <em>www.timesavers.es</em> utiliza cookies,  peque&ntilde;os ficheros de datos que se generan en el ordenador del usuario y que nos  permiten conocer su frecuencia de visitas, los contenidos m&aacute;s seleccionados y  los elementos de seguridad que pueden intervenir en el control de acceso a &aacute;reas  restringidas, as&iacute; como la visualizaci&oacute;n de publicidad en funci&oacute;n de criterios  predefinidos por <b>TIMESAVERS </b>y que se  activan por cookies servidas por dicha entidad o por terceros que prestan estos  servicios por cuenta de <b>TIMESAVERS</b>.</p>
        <p>El usuario tiene la opci&oacute;n de impedir la  generaci&oacute;n de cookies, mediante la selecci&oacute;n de la correspondiente opci&oacute;n en su  programa navegador.</p>
        <p>El usuario autoriza a <b>TIMESAVERS</b> a obtener y tratar la informaci&oacute;n que se genere como  consecuencia de la utilizaci&oacute;n del Web <em>www.timesavers.es</em>, con la &uacute;nica  finalidad de ofrecerle una navegaci&oacute;n m&aacute;s personalizada.</p>
        <p>6. <u>Seguridad.</u> Este sitio web en cuanto a la confidencialidad y tratamiento de datos de  car&aacute;cter personal, por un lado, responde a las medidas de seguridad de nivel  medio establecidas en virtud de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, sobre  Protecci&oacute;n de Datos de Car&aacute;cter Personal y su Reglamento de Medidas de  Seguridad vigente, y por otro lado responde,  en cuanto a las medidas t&eacute;cnicas a la instalaci&oacute;n de &ldquo;Antivirus y  filtros antispam&rdquo; y el Secure Socket Layers &ldquo;SSL&rdquo; como plataforma segura. Todo  lo anterior a sabiendas que en la red de redes la seguridad absoluta no existe.</p>
        <p>7. <u>Confirmaci&oacute;n  de Recepci&oacute;n Pedido.</u> Una vez recibido el pedido, TimeSavers enviar&aacute; por  correo electr&oacute;nico la confirmaci&oacute;n de recepci&oacute;n del pedido; posteriormente, y  en un plazo m&aacute;ximo de 24 horas, se enviar&aacute;, mediante correo electr&oacute;nico, el  presupuesto y el plazo de entrega del/los servicio/s solicitado/s. Los pedidos  efectuados los viernes, o v&iacute;spera de festivo, se tramitar&aacute;n el siguiente d&iacute;a  laborable (los d&iacute;as laborables no incluyen fines de semana ni d&iacute;as festivos).</p>
        <p>8. <u>Formas y  condiciones de pago (impuestos).</u> En cuanto al pago, el usuario debe elegir  entre: tarjeta de cr&eacute;dito, transferencia bancaria y/o Paypal.En el  caso de compras y/o gestiones con un importe superior a 1000&euro; , se aplicar&aacute; una  preautorizaci&oacute;n por este importe ( consultar condiciones Cyberpack &ldquo;La Caixa&rdquo;  ), con la observaci&oacute;n de que en ning&uacute;n caso nos llegar&aacute; informaci&oacute;n sobre el  numero de la tarjeta de cr&eacute;dito o cuenta, estos datos pasan directamente a la  entidad financiera, que se encargar&aacute; de certificar la autenticidad de la  tarjeta y realizar el cobro.Todo a sabiendas que en la red de redes la  seguridad absoluta no existe.</p>
        <p>Todos los precios  reflejados en el portal <em>www.timesavers.es</em> incluyen el IVA (16%). Los precios est&aacute;n sujetos a cambios sin  previo aviso.</p>
        <p>9. <u>Plazos, Formas  y Gastos de env&iacute;o.</u><br />
          A partir del momento en el que sea efectiva  la recepci&oacute;n del pedido y se confirme el pago del mismo por parte del cliente,  se proceder&aacute; a la realizaci&oacute;n efectiva de la prestaci&oacute;n de los servicios  contratados en un plazo marcado en la confirmaci&oacute;n del pedido.</p>
        <p>En caso de no disponibilidad del producto o  servicio que el cliente haya solicitado a trav&eacute;s de la web <em>www.timesavers.es</em>, se proceder&aacute; a la  comunicaci&oacute;n del incidente al usuario en un m&aacute;ximo de 3 (tres) d&iacute;as, utilizando  como canal de comunicaci&oacute;n la direcci&oacute;n de correo electr&oacute;nico, o n&uacute;mero de  tel&eacute;fono de contacto, que el usuario nos haya facilitado a trav&eacute;s del  formulario de compra. En esta comunicaci&oacute;n se le informar&aacute; al cliente de la fecha  en la que el producto o servicio comprado estar&aacute; disponible o de la  imposibilidad de disponer del mismo. En caso de que el cliente no est&eacute; de  acuerdo con los nuevos plazos o sea imposible disponer del producto o servicio  contratados, se le dar&aacute; otra alternativa a la compra o se proceder&aacute; a la  devoluci&oacute;n del importe total de la misma a elecci&oacute;n del cliente.</p>
        <p>Antes de finalizar la compra y confirmar el  pedido, se podr&aacute; conocer la cuant&iacute;a de este concepto y, en caso de desearlo, el  cliente podr&aacute; desestimar el pedido.</p>
        <p>10. <u>Pago mediante tarjeta de cr&eacute;dito</u>.  (Repudio) </p>
        <p>Cuando el importe de una compra hubiese  sido cargado utilizando el n&uacute;mero de una tarjeta de cr&eacute;dito, sin que &eacute;sta  hubiese sido presentada directamente o identificada electr&oacute;nicamente, su titular  podr&aacute; exigir la inmediata anulaci&oacute;n del cargo. En tal caso, las  correspondientes anotaciones de adeudo y reabono en las cuentas del proveedor y  del titular se efectuar&aacute;n a la mayor brevedad.</p>
        <p>Sin embargo, si la compra hubiese sido efectivamente realizada por el titular  de la tarjeta y, por lo tanto, hubiese exigido indebidamente la anulaci&oacute;n del  correspondiente cargo, aqu&eacute;l quedar&aacute; obligado frente al vendedor al  resarcimiento de los da&ntilde;os y perjuicios ocasionados como consecuencia de dicha  anulaci&oacute;n. </p>
        <p>11. <u>Desistimiento.</u></p>
        <p>El comprador podr&aacute; desistir libremente del  contrato dentro del plazo de siete d&iacute;as contados desde la fecha de recepci&oacute;n  del producto. <br />El ejercicio del derecho o desistimiento no  estar&aacute; sujeto a formalidad alguna, bastando que se acredite, en cualquier forma  admitida en Derecho.<br />
        El derecho de desistimiento del comprador  no puede implicar la imposici&oacute;n de penalidad alguna, si bien, el comprador  deber&aacute; satisfacer los gastos directos de devoluci&oacute;n y en su caso, indemnizar  los desperfectos del objeto de la compra. </p>
        <p>Lo dispuesto anteriormente no ser&aacute; de  aplicaci&oacute;n a las ventas de objetos que puedan ser reproducidos o copiados con  car&aacute;cter inmediato, que se destinen a la higiene corporal o que, en raz&oacute;n de su  naturaleza, no puedan ser devueltos. </p>
        <p>12. <u>Devoluciones.</u></p>
        <p>En cuanto a las gestiones compradas a  TimeSavers, tienen una validez de 1 a&ntilde;o para ser utilizadas, aunque existe la  posibilidad de devoluci&oacute;n del importe de las gestiones compradas y NO  UTILIZADAS tras 30 dias naturales de haber realizado la compra de dichas  gestiones.<br />
        As&iacute; mismo, se pone en conocimiento del usuario  que algunos de los servicios ofrecidos por TimeSavers, ser&aacute;n subcontratados a  terceras empresas proveedoras de dichos servicios, con lo cual el usuario tendr&aacute;  el derecho a la devoluci&oacute;n de algunos de los productos finales en un plazo  m&aacute;ximo de diez (10) d&iacute;as tras la recepci&oacute;n del mismo, en virtud de la Ley de  Ordenamiento del Comercio Minorista. Pudiendo el usuario, solicitar la  sustituci&oacute;n del producto, la deducci&oacute;n del importe equivalente en su pr&oacute;ximo  pedido o el reembolso de su dinero, previa confirmaci&oacute;n por parte de <b>TIMESAVERS </b>de que no se ha utilizado,  da&ntilde;ado y/o quebrantado el producto. En cuanto a los dem&aacute;s productos (alimentos,  artesan&iacute;a) por su fragilidad y especial conservaci&oacute;n no se podr&aacute;n devolver,  excepto cuando el error se halla presentado por parte del vendedor.</p>
        <p><u>Proceso de devoluci&oacute;n:</u> el proceso de  devoluci&oacute;n es el siguiente: </p>
        <p>12.1. Escribir a <a href='mailto:info@timesavers.es'>info@timesavers.es</a> solicitando un n&uacute;mero  de autorizaci&oacute;n de devoluci&oacute;n (se le da un No. de devoluci&oacute;n que determin&eacute; la  empresa, para llevar control de dichas actuaciones); 
        12.2. Hay que enviarle  junto con el pedido un formulario de devoluci&oacute;n adjunto, para que expliquen  motivos de la devoluci&oacute;n, productos a devolver (el producto s&oacute;lo se podr&aacute;  devolver en caso de da&ntilde;os en el mismo, equivocaci&oacute;n o no conformidad y en el  caso de productos alimenticios, artesan&iacute;as s&oacute;lo se podr&aacute; devolver por no  conformidad del comprador por error del vendedor y caso de da&ntilde;os en el mismo); 
        12.3.  Adjunto con el env&iacute;o del producto en su embalaje original, debidamente  protegido para su transporte, anexe copia de la factura m&aacute;s el formulario de  devoluci&oacute;n; 12.4. El reenv&iacute;o del producto ser&aacute; a su s&oacute;lo coste y deber&aacute;  enviarlo dentro de un plazo no superior a diez (10) d&iacute;as desde la recepci&oacute;n del  producto. Para el caso de que el producto este da&ntilde;ado o no sea lo que ha  solicitado y dicho error sea imputable al vendedor, todos los gastos correr&aacute;n  porcuenta de &eacute;ste.</p>
        <p>13. <u>Legislaci&oacute;n  y jurisdicci&oacute;n aplicable</u><u>.</u> Con car&aacute;cter general, las relaciones con los usuarios,  derivadas de la prestaci&oacute;n de servicios contenidos en esta p&aacute;gina web, con  renuncia expresa a cualquier fuero que pudiera corresponderles, se someten a la  legislaci&oacute;n y jurisdicci&oacute;n espa&ntilde;ola, espec&iacute;ficamente a los tribunales de la  ciudad de Barcelona (Espa&ntilde;a). Los usuarios de esta p&aacute;gina web son conscientes  de todo lo que se ha expuesto y lo aceptan voluntariamente.</p>");

define("EMP1","Ein Team zu Ihren Diensten!");
define("EMP2","<em>TimeSavers &ndash; Quality Personal Concierge</em> besteht aus einem Team von Experten  mit einem breiten F&auml;cher an beruflichem Know-How, akademischer Ausbildung und  Lebenserfahrung. Diese Vorbereitung, gepaart mit ihrem Unternehmergeist,  erlaubt, Ihnen bestens als pers&ouml;nlicher Assistent oder Concierge dienen zu  k&ouml;nnen sowie effiziente und schnelle L&ouml;sungen f&uuml;r jedwede Aufgabenstellung auch  unser anspruchsvollsten Mitglieder zu finden.");
define("EMP3","Unternehmensgr&uuml;nder &amp; Managing Director ist Christoph Kraemer, der  deutscher und franz&ouml;sischer Staatsb&uuml;rger ist, und ein Diplom der Yale  University besitzt. Ein wahrer Weltb&uuml;rger, hat er unter anderem schon in Berlin,  New York und Kapstadt gelebt. 2002 kommt er nach Barcelona, um an ESADE einen  MBA zu absolvieren und entschliesst sich zu bleiben. Christophs beruflicher  Lebenslauf ist &auml;usserst vielf&auml;ltig. Er beinhaltet die Betreuung von bekannten  K&uuml;nstlern in gro&szlig;en Schallplattenfirmen, die F&uuml;hrung eines multinationalen  Teams f&uuml;r die Erstellung von Marketingkampagnen f&uuml;r eine europ&auml;ische Kette von  Juweliergesch&auml;ften, bis hin zur Koordination und zum Projektmanagement f&uuml;r eine  Baumarktkette.");
define("EMP4","Unternehmensgr&uuml;nder &amp; Operations Director  Toni Delgado, geboren in Barcelona, hat ein Diplom in Fashion Design der CETE  und der &Eacute;cole Sup&eacute;rieure de la Mode. Von Berufung her Kosmopolit, hat er in  Paris, Madrid und Barcelona gewohnt. In seinem beruflichen Leben hat er&nbsp;  Erfahrung bei der Organisation von internationalen Modemessen und mit  internationalen Haute Couture Designern zusammenzuarbeiten. Danach hat er sich  auf Logistik und industrielle Post-Produktion f&uuml;r Unternehmen spezialisiert,  die Marktf&uuml;hrer in ihrem Segment sind.");
define("EMP5","Lassen Sie uns arbeiten und ruhen Sie sich aus!");
define("EMP6","<p><em>TimeSavers &ndash; Quality Personal Concierge</em> bietet Ihnen  L&ouml;sungen f&uuml;r Ihre pers&ouml;nlichen Angelegenheiten, Ihre Urlaubsplanung, das  Catering, welches Sie f&uuml;r die n&auml;chste Dinnerparty ben&ouml;tigen. Wir k&uuml;mmern uns um  Ihr Zuhause und Ihr Auto, holen Besucher vom Flughafen ab und finden die  ad&auml;quate Unterkunft f&uuml;r sie.</p>
  <p>Erlauben Sie uns, Ihr Auto zur Inspektion zu bringen,  lassen Sie uns das geeignete Geschenk f&uuml;r Ihren Partner suchen und es auch  &uuml;berbringen, oder lehnen Sie sich einfach zur&uuml;ck und erfreuen Sie sich einer  angenehmen Massage.</p>
  <p>Konzentrieren Sie sich auf die Anliegen, die Sie am  liebsten selbst erledigen und die Ihnen Freude bereiten. Gestatten Sie uns,  Ihre W&auml;sche zur Reinigung zu bringen, einen Haustier-Sitter zu finden, einen  Kostenvoranschlag f&uuml;r die Renovierung Ihrer Wohnung zu erstellen und zu  optimieren, und Ihnen au&szlig;erdem einen Top Interior Designer vorzustellen.</p>
  <p>Fahren Sie beruhigt in den Urlaub mit dem Wissen, dass  sich jemand um Ihr Zuhause k&uuml;mmert. Wir gie&szlig;en die Blumen, leeren den  Briefkasten und erledigen wichtige Besorgungen in Ihrer Abwesenheit.</p>
  <p>F&uuml;r die Services, die <em>TimeSavers &ndash; Quality Personal Concierge</em> nicht direkt selber ausf&uuml;hrt, arbeiten wir ausschlie&szlig;lich mit  bew&auml;hrten Gesch&auml;ftspartnern zusammen, die wir laufend &uuml;berpr&uuml;fen. Ausserdem  bieten wir interessante Preise und Exklusivangebote f&uuml;r TimeSavers-Mitglieder.</p>");

define("CT0","<p>Sein Kommentar wurde gesendet. <br /> <br /> Vielen Dank f&uml;r Ihr Interesse. <br /><br />Kontakt mit Ihnen so bald wie m&ouml;glich.</p>");
define("CT1A","<p>Bei <em>TimeSavers &ndash; Quality Personal Concierge</em> suchen wir nach Gesch&auml;ftspartnern, die einen exzellenten Service und  einwandfreie Produkte bieten, um die Bed&uuml;rfnisse unserer Mitglieder zu  erf&uuml;llen.</p>
  <p>Mehr als eine einfache Gesch&auml;ftsbeziehung sind  wir an einer engen Zusammenarbeit und der Entwicklung von Exklusivprodukten  interessiert, die unsere Mitglieder animieren sollen, Ihr Angebot zu nutzen.</p>
  <p>Interessiert? Dann z&ouml;gern Sie nicht und f&uuml;llen  Sie dieses Formular aus! Wir melden uns umgehend bei Ihnen.</p>");
define("CT1B","Bei <em>TimeSavers &ndash; Quality Personal Concierge</em> sind  wir Ihnen zu Diensten und einer total Transparenz verpflichtet. Z&ouml;gern Sie  daher bitte nicht, uns zu kontaktieren und uns Ihre Zweifel oder Anfragen  mitzuteilen. Wir sind immer hocherfreut, Sie umgehend zu betreuen.");
define("CT2","Die von Ihnen angegebenen Daten werden, in &Uuml;bereinstimmung mit unseren <a href='legal.php'>AGB</a> und der spanischen Ley Org&aacute;nica 15/99 de Protecci&oacute;n de Datos, vertraulich behandelt.");
define("CT3","Ich habe die <a href='legal.php'>legalen  Hinweise und AGB</a> gelesen und bin damit einverstanden.");
define("CT4","Ich m&ouml;chte keine Informationen bez&uuml;glich Produkte, Angebote oder Service  seitens TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. erhalten.");
define("CT5","Ich m&ouml;chte nicht, dass meine pers&ouml;nlichen Daten l&auml;nger als laut dem  spanischen Gesetzgeber vorgesehen bei TIMESAVERS QUALITY CONCIERGE SERVICE,  S.L. aufbewahrt werden.");
define("CT6","Ich m&ouml;chte keine Informationen bez&uuml;glich Produkte, Angebote oder Service  seitens TIMESAVERS QUALITY CONCIERGE SERVICE, S.L. nach Beendigung unserer  Gesch&auml;ftsbeziehung erhalten.");
define("CONT1","Vor-und Nachname");
define("CONT2","Firmennamen");
define("CONT3","Art des Service/Produkts");
define("CONT6","Obligatorische Felder");
define("CONT7","SENDEN");
  
define("FAQ1","Warum TimeSavers &ndash; Quality Personal Concierge?");
define("FAQ2","Wie werde ich Mitglied/beende ich meine Mitgliedschaft?");
define("FAQ3","Gibt es etwas was TimeSavers nicht realisieren kann?");
define("FAQ4","Verteuert TimeSaverse den normalen  Verkaufspreis seiner Gesch&auml;ftspartner?");
define("FAQ5","Werden Last Minute Bestellungen angenommen?");
define("FAQ6","Wie ist die Vorgehensweise, um einen Service zu bestellen?");
define("FAQ7","Wo wird TimeSavers t&auml;tig sein?");
define("FAQ8","Kann ich TimeSavers &ndash; Quality Personal Concierge jederzeit kontaktieren?");
define("FAQ9","Ist TimeSavers der Anbieter aller  Dienstleistungen?");
define("FAQ10","Welche Garantien besitze ich, wenn ein Service an Dritte vergeben wird?");
define("FAQ11","Wie lange sind einmal gekaufte TimeSavers-Credits g&uuml;ltig?");
define("FAQ12","Wie sieht TimeSavers Datenschutz-Politik aus?");
define("FAQ13","Wie funktioniert TimeSavers?");

define("FAQ1_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> ist ein erschwinglicher Service, der Ihnen erlaubt,  bestimmte Belange zu delegieren, f&uuml;r die Sie keine Zeit haben oder nicht die  M&ouml;glichkeit besitzen, sie selbst zu erledigen. Infolgedessen k&ouml;nnen Sie Ihren  Stresslevel reduzieren und Ihre freie Zeit vermehren. <em>TimeSavers &ndash; Quality Personal Concierge</em> verpflichtet sich, jede  Bestellung entsprechend Ihren W&uuml;nschen und Anweisungen auszuf&uuml;hren &ndash;&nbsp; Verbesserungsvorschl&auml;ge behalten wir uns vor.  Wir hoffen, mit der Zeit Ihr zu Vertrauen gewinnen und ein wesentlicher  Bestandteil Ihres Lebensmanagement-Systems zu werden.");
define("FAQ2_X","F&uuml;r die Registrierung ist ein einmaliger Mitgliedsbeitrag in Betrag von  50,- &euro; zu zahlen. Dieser Betrag wird nachfolgend mit dem Kauf von weiteren  TimeSavers-Credits verrechnet. <br />Sie k&ouml;nnen jederzeit Ihre Mitgliedschaft beenden, in dem Sie uns ein E-mail  an <a href='mailto:info@timesavers.es'>info@timesavers.es</a> schicken oder einen  einfachen Klick in &bdquo;Mein TimeSavers&ldquo; t&auml;tigen. Schon gekaufte TimeSavers Credits  werden bis zu 30 Tagen ab dem Kaufdatum zur&uuml;ckerstattet.");
define("FAQ3_X","<em>TimeSavers &ndash; Quality Personal Concierge</em> wird zu jeder Zeit aufs &auml;usserste darauf bedacht sein,  die Bed&uuml;rfnisse seiner Mitglieder zu befriedigen, solange diese legal und  moralisch sind. Wir sind ein Team mit umfassenden und vielseitigen Backgrounds  100% engagiert, die beste L&ouml;sung f&uuml;r jede Bestellung zu finden. Sollte zum  Beispiel eine Bestellung Freitag nachmittags f&uuml;r eine Tischreservierung in dem  hippsten Restaurant der Stadt f&uuml;r den selbigen Abend eintreffen, werden wir,  &uuml;ber unser ausgedehntes Netzwerk, alles M&ouml;gliche unternehmen, um diesen Tisch  f&uuml;r Sie zu ergattern. Und sollte es trotz allem nicht funktionieren, bieten wir  Ihnen eine L&ouml;sung an, die Ihnen mindestens genauso gut gefallen wird.");
define("FAQ4_X","Die Antwort ist ein klares Nein! Unsere Mitglieder bezahlen in der Regel  den normalen Verkaufspreis. Au&szlig;erdem werden wir mit allen Gesch&auml;ftspartner  versuchen, Preisvorteile und Angebote f&uuml;r unsere Mitglieder zu verhandeln. Bei  jedem unserer L&ouml;sungsvorschl&auml;ge werden wir zus&auml;tzlich das Mitgliedsprofil und  seine/ihre M&ouml;glichkeiten in Betracht ziehen.");
define("FAQ5_X","Es er&uuml;brigt sich zu erw&auml;hnen, dass wir, je mehr Zeit wir haben, Ihnen eine  bessere L&ouml;sung pr&auml;sentieren k&ouml;nnen. Nichtsdestotrotz, werden wir immer  versuchen, jeden Wunsch zu erf&uuml;llen unabh&auml;ngig der Vorank&uuml;ndigung. Vertrauen  Sie darauf, dass, sollte es uns nicht m&ouml;glich sein, die Bestellung 1:1  auszuf&uuml;hren, wir Ihnen einen gleichwertigen oder sogar besseren Vorschlag  machen werden.");
define("FAQ6_X","Die Vorgehensweise ist einfach und schnell. Schicken Sie  uns Ihre Anfrage per Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a>,  rufen Sie uns unter +34 93 304 00 32 an oder registrieren Sie sich als Mitglied  und bestellen Sie &uuml;ber &bdquo;Mein TimeSavers&ldquo;. 
  Wir antworten Ihnen umgehend mit  einer auf Ihre Bed&uuml;rfnisse und W&uuml;nsche zugeschnittenen L&ouml;sung und einem  Kostenvoranschlag. Sollte der gew&uuml;nschte Service die Mitarbeit einer unserer  Gesch&auml;ftspartner erfodern, wird der Kostenvoranschlag klar unter den  Service-/Produktkosten und den f&uuml;r die Bearbeitung notwendigen TimeSavers  Credits unterscheiden. Der Kostenvoranschlag muss dann per Telefon, Mail oder  &bdquo;Mein TimeSavers&ldquo; bewilligt werden und (bei Betr&auml;gen &uuml;ber 100 &euro;) muss 50% im  voraus bezahlt werden. 
  Ist der Service einmal abgeschlossen, erhalten Sie eine  Rechnung, mit den tats&auml;chlich angefallenen Kosten und TimeSavers Credits,  abz&uuml;glich der Vorauszahlung. F&uuml;r die Rechnung werden vor Beginn des besagten  Service gekaufte TimeSavers Credit-Pakete ber&uuml;cksichtigt und auf diese Rechnung  angewandt. Sollten Sie sich entscheiden, kein TimeSavers Credit-Paket zu  kaufen, berechnen wir die genaue Anzahl der f&uuml;r die Bearbeitung des Service  notwendigen Credits.");
define("FAQ7_X","Der Servicebereich von <em>TimeSavers &ndash;  Quality Personal Concierge</em> erstreckt sich auf den Raum Barcelona-City.");
define("FAQ8_X","Unsere Gesch&auml;ftszeiten sind Montag bis Freitag von 10h bis 20h.  Selbstverst&auml;ndlich sind wir 24 Stunden am Tag unter der Nummer +34 93 304 00 32  erreichbar. Die Tarife f&uuml;r eine Bearbeitung ausserhalb der normalen  Gesch&auml;ftszeiten verteuern sich um 25%.");
define("FAQ9_X","Alle unsere Dienstleistungen werden mit der Verpflichtung f&uuml;r einen  exzellenten Service ohne Probleme angeboten. Manche Dienstleistungen erledigt <em>TimeSavers &ndash; Quality Personal Concierge</em> selber und andere werden an einen unserer Gesch&auml;ftspartner vergeben. Alle  ausgelagerten Service werden von einem qualifizierten und von uns getesteten  Gesch&auml;ftspartner ausgef&uuml;hrt.");
define("FAQ10_X","In Bezug auf Garantien arbeitet <em>TimeSavers  &ndash; Quality Personal Concierge</em> mit einer zivilen Haftpflichtversicherung  (&bdquo;seguro de responsabilidad civil&ldquo;), die alle Transaktionen besch&uuml;tzt, und  verlangt desgleichen von allen Gesch&auml;ftspartnern.");
define("FAQ11_X","Ab dem Kaufdatum haben Sie 1 Kalenderjahr, um Ihre TimeSavers Credits  einzul&ouml;sen.");
define("FAQ12_X","Alle von unserem Mitglied angegebenen pers&ouml;nlichen Daten werden in einer  Datenbank gespeichert, die von TimeSavers Quality Concierge Service, S.L. vertraulich  und unter Einhaltung der Ley Org&aacute;nica de Protecci&oacute;n de Datos de Car&aacute;cter  Personal n&ordm; 15/1.1999, del 13 de diciembre de 1999, sowie jedwedem anderem in  dieser Materie geltenden Beschluss, gehandhabt wird. Unser Mitglied kann  jederzeit sein Recht aus&uuml;ben, seine Daten einzusehen, zu ver&auml;ndern oder sie zu  l&ouml;schen. Um dies zu tun, gen&uuml;gt, uns per Telefon oder E-mail zu kontaktieren. F&uuml;r  ausf&uuml;hrlichere Ausk&uuml;nfte konsultieren Sie bitte unsere Datenschutz-Politik (&bdquo;<a href='http://www.timesavers.es/legal.php'>Aviso Legal &amp; Condiciones de Uso</a>&ldquo;)  oder wenden Sie sich direkt an uns.");
define("FAQ13_X","<em>TimeSavers &ndash; Quality Personal  Concierge</em> funktioniert aufgrund eines  Kreditsystem, in dem die Bearbeitung eines Service eine bestimmte Anzahl an  TimeSavers Credits verlangt. Besagte Credits k&ouml;nnen im &bdquo;Mein  TimeSavers&ldquo;-Bereich erworben werden, sobald Sie als TimeSavers-Mitglied  registriert sind. Bez&uuml;glich mehr Information zu den TimeSavers Credits, klicken  Sie bitte <a href='http://www.timesavers.es/gestiones.php'>hier</a>.");

define("GEST0A","<em class='blanco'>TimeSavers &ndash; Quality Personal Concierge</em> basiert  auf einem Credits-System, in dem die Bearbeitung jedes Service eine bestimmte  Anzahl an Credits verlangt. Credits erhalten Sie im &bdquo;Mein TimeSavers&ldquo;-Bereich,  sobald Sie sich als TimeSavers-Mitglied registrieren.<br />
<br />Jedes Mal  wenn Sie um einen bestimmten Service anfragen, erhalten Sie von uns einen  Kostenvoranschlag per Mail und/oder Ihr &bdquo;Mein TimeSavers&ldquo;-Konto, der die  voraussichtliche Anzahl an f&uuml;r die Bearbeitung notwendigen Credits detalliert.  Selbstverst&auml;ndlich erstellen wir diese Kostenvoranschl&auml;ge kostenlos. Im &bdquo;Mein  TimeSavers&ldquo;-Bereich haben Sie die M&ouml;glichkeit, gr&ouml;ssere TimeSavers  Credit-Pakete zu kaufen, um einen interessanteren Preis pro Credit zu erhalten.  Der Kostenvoranschlag muss dann via Mail, Telefon oder &bdquo;Mein TimeSavers&ldquo;  genehmigt und 50% vorabbezahlt werden (bei Betr&auml;gen &uuml;ber 100 &euro;).<br />
<br />Sollte der  gew&uuml;nschte Service den Beitrag einer unserer Gesch&auml;ftspartner erfordern, wird  der Kostenvoranschlag klar zwischen den Kosten unseres Partners und den f&uuml;r die  Bearbeitung notwendigen TimeSavers Credits unterscheiden. Nachdem der Service  erfolgt ist, schickt Ihnen <strong>TimeSavers &ndash;  Quality Personal Concierge</strong> eine Rechnung zu, in der die letztendlich  angefallenen Kosten und TimeSavers Credits aufgez&auml;hlt sind. F&uuml;r die Rechnung  werden vor Beginn des besagten Service gekaufte TimeSavers Credit-Pakete  ber&uuml;cksichtigt und auf diese Rechnung angewandt. Sollten Sie sich entscheiden,  kein TimeSavers Credit-Paket zu kaufen, berechnen wir die genaue Anzahl der f&uuml;r  die Bearbeitung des Service notwendigen Credits. &nbsp;<br />
<br />Alle  Zahlungen k&ouml;nnen per Bank&uuml;berweisung oder direkt im Mitgliedsbereich &bdquo;Mein  TimeSavers&ldquo; geleistet werden.<br />
<br />An dieser Stelle erlauben wir uns hervorzuheben, dass <strong>TimeSavers &ndash; Quality Personal Concierge</strong> mit fast all seinen  Gesch&auml;ftspartnern Mehrwerte f&uuml;r TimeSavers-Mitglieder verhandelt hat, sei es  ein Rabatt, ein spezielles Angebot oder ein exlusiver Vorteil.<br />
<br />Alle  Preise sind inkl. MwSt.<br />
<br />TimeSavers  Credits sind ab dem Kaufdatum f&uuml;r ein Kalenderjahr g&uuml;ltig.");
define("GEST0","<p><em>Schnell und einfach: Schicken Sie uns Ihre Anfrage  per Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a>, rufen Sie uns unter +34 93 304 00 32 an oder begeben Sie sich in den  Mitgliedsbereich &bdquo;Mein TimeSavers&ldquo;. Wir schicken Ihnen umgehend einen auf Ihre  Bed&uuml;rfnisse zugeschneiderten L&ouml;sungsvorschlag.</em><br><br>Nachfolgend finden Sie eine  Liste der verschiedenen TimeSavers Credits-Pakete:</p>
	    <table cellpadding='4' cellspacing='1'>
		<tr class='filazul_1'><td>1  TimeSavers Credit</td><td>12,00 &euro;</td></tr>
		<tr class='filazul_2'><td>5  TimeSavers Credits</td><td>58,00 &euro;</td></tr>
		<tr class='filazul_1'><td>10  TimeSavers Credits</td><td>112,00 &euro;</td></tr>
		<tr class='filazul_2'><td>25  TimeSavers Credits</td><td>265,00 &euro;</td></tr>
		<tr class='filazul_1'><td>50  TimeSavers Credits</td><td>480,00 &euro;</td></tr>
		<tr class='filazul_2'><td>100 TimeSavers  Credits</td><td>840,00 &euro;</td></tr>
		</table>
	    <p>Sollten Sie an einem  bedeutenderen TimeSavers Credits-Paket interessiert sein, bitten wir Sie uns  unter +34 93 304 00 32 anzurufen oder uns ein Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a> zu schicken.</p>");
define("GEST1","<p><em>TimeSavers &ndash; Quality  Personal Concierge</em> bietet monatliche Abonnements an. Sie k&ouml;nnen zwischen zwei bestehenden Optionen  ausw&auml;hlen oder wir erstellen Ihnen ein auf Ihre pers&ouml;nlichen Bed&uuml;rfnisse  ausgerichtetes Paket. TimeSavers Monthly Abonnements enthalten als Mehrwert gratis  Restaurantreservierungen und das kostenlose Handeln Ihrer pers&ouml;nlichen Agenda  ohne dass Sie auf irgendeinen anderen TimeSavers-Mitgliedern zur Verf&uuml;gung  stehenden Vorteil verzichten m&uuml;ssen.</p>
<p><b>TimeSavers Monthly Basic</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Bis zu  16 TimeSavers Credits</td><td>120,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Inkl. kostenlose Restaurantreservierungen und Handeln pers&ouml;nliche Agenda</td></tr>
<tr class='filazul_1'><td colspan='2'>Kundenservice Montag bis Freitag von 10h bis 20h.</td></tr>
<tr class='filazul_2'><td colspan='2'>Inkl. Reisekosten innerhalb von Barcelona.</td></tr>
</table>
<p><b>TimeSavers Monthly Premium</b></p>
<table cellpadding='4' cellspacing='1'>
<tr class='filazul_1'><td>Bis zu  40 TimeSavers Credits</td><td>250,00 &euro;</td></tr>
<tr class='filazul_2'><td colspan='2'>Inkl. kostenlose Restaurantreservierungen und Handeln pers&ouml;nliche Agenda</td></tr>
<tr class='filazul_1'><td colspan='2'>Kundenservice 24/7.</td></tr>
<tr class='filazul_2'><td colspan='2'>Inkl. Reisekosten innerhalb von Barcelona.</td></tr>
</table>
<p>Monatsabonnements gelten ab dem Kaufdatum f&uuml;r 30 Tage. W&auml;hrend dieses Zeitraums nicht benutzte TimeSavers Credits werden nicht f&uuml;r nachfolgende Abonnements gut geschrieben.</p>
<p><b>Beispiele</b><br />Um sich vorstellen zu k&ouml;nnen, wie viele TimeSavers Credits die  Bearbeitung eines bestimmten Service kostet, stellen wir Ihnen nachfolgend  einige Beispiele zur Verf&uuml;gung: </p>
<ul>
<li>1 Restaurantreservierung kostet 1 TimeSavers Credit. Wie schon  erw&auml;hnt, schicken wir Ihnen einen Kostenvoranschlag per Mail und/oder an   Ihr &bdquo;Mein TimeSavers&ldquo;-Konto, der &uuml;ber einen dieser beiden Kan&auml;le oder per Telefon genehmigt werden muss. Da der Gesamtbetrag f&uuml;r den Service unter   100 &euro; liegt, ist es nicht n&ouml;tig, 50% vorabzubezahlen. Der n&auml;chste Schritt  ist, dass wir Ihnen die Rechnung zukommen lassen, die per Bank&uuml;berweisung   oder &bdquo;Mein TimeSavers&ldquo; zu begleichen ist. Wir haben mit allen Etablissements,   mit denen wir zusammenarbeiten, ein Plus f&uuml;r unsere Mitglieder verhandelt. Lassen Sie sich &uuml;berraschen! </li>
<li>Das erste Mal, dass wir Ihnen einen Friseur nach Hause schicken, kostet es Sie 2 TimeSavers Credits und den Preis unseres Spezialisten. Wir   schicken Ihnen einen Kostenvorschlag, der klar zwischen den f&uuml;r die  Bearbeitung notwendigen TimeSavers Credits und dem anhand der vorliegenden   Information voraussichtlichen Preis des Service unterscheidet. Der   Kostenvoranschlag muss genehmigt und 50% vorab bezahlt werden. Es kann nat&uuml;rlich sein, dass Sie sich w&auml;hrend des Friseurbesuchs spontan   entscheiden, der bestellten Haart&ouml;nung noch eine Pflegebehandlung   zuzuf&uuml;gen. Machen Sie sich keine Sorgen! Das stellt &uuml;berhaupt kein Problem dar. 
Pr&auml;zise aus diesem Grund dr&uuml;ckt die Rechnung immer die letztendlich angefallenen TimeSavers Credits und Kosten des Service, abz&uuml;glich der   Vorauszahlung, aus. Ausserdem: Sollten Sie beim gleichen Gesch&auml;ftspartner   wiederholen, stellen wir Ihnen nur 1 TimeSavers Credit f&uuml;r das Handeln Ihrer pers&ouml;nlichen Agenda in Rechnung. </li>
<li>Die Erneuerung des T&Uuml;V f&uuml;r Ihren Wagen dauert normalerweise 2 bis   3 Stunden und kostet demnach 8 bis 12 TimeSavers Credits. Bevor wir mit der Bearbeitung beginnen, schicken wir Ihnen einen Kostenvoranschlag mit dem Durchschnitt der zur Ausf&uuml;hrung voraussichtlich notwendigen TimeSavers   Credits. Danach erfolgen die bekannten Schritte des Kostenvoranschlags und seiner Genehmigung. Sobald die T&Uuml;V-Erneuerung erfolgt ist, &uuml;berpr&uuml;fen wir   die in Wirklichkeit benutzten Credits und schicken Ihnen dementsprechend   eine endg&uuml;ltige Rechnung, abz&uuml;glich der Anzahlung, die per Bank&uuml;berweisung   oder direkt in &bdquo;Mein TimeSavers bezahlt werden kann.</li>
</ul>
<p>Der Kostenvoranschlag wird immer mit der f&uuml;r die Bearbeitung des  Service angemessenen Anzahl an TimeSavers Credits berechnet. Nichtsdestotrotz,  k&ouml;nnen Sie jederzeit vor Beginn des Service im &bdquo;Mein TimeSavers&ldquo;-Bereich ein  bedeutenderes TimeSavers Credit-Paket kaufen, um einen attraktiveren Preis pro  Credit zu erhalten. Ein solcher Kauf wird dann auf die Rechnung des angefragten  Service angewandt.</p>");
define("GEST2","Erleben Sie Barcelona  auf andere Weise und machen Sie das Beste aus Ihrem Aufenthalt! <em>TimeSavers &ndash; Quality Personal Concierge</em> bietet Barcelona-Besuchern ein breitgef&auml;chertes Angebot an einmaligen  Erfahrungen an. Ausserdem besteht nat&uuml;rlich die M&ouml;glichkeit, Ihnen ein auf Ihre  Bed&uuml;rfnisse ausgerichtetes Paket masszuschneidern.
<br /><br />Sollten Sie interessiert sein oder mehr Information wollen, bitten wir Sie, uns  unter +34 93 304 00 32 anzurufen, uns ein Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a> zu  schicken oder hier zu klicken.");
define("GEST3","Warum nicht ein wirklich  praktisches Geschenk verschenken? Ein interessantes, innovatives Geschenk zu  finden, kann ziemlich stressig sein. Abgsehen davon, dass Sie uns mit der  Geschenksuche beauftragen k&ouml;nnen, bietet <strong>TimeSavers  &ndash; Quality Personal Concierge</strong> auch die M&ouml;glichkeit, Ihren Partner, ein  Familienmitglied oder einen Freund mit einem Gutschein &bdquo;TimeSavers Coupon&ldquo; zu  &uuml;berraschen.<br /><br />Wenn Sie ein TimeSavers  Credits-Paket erwerben, erhalten sie Gelegenheit, die Option &bdquo;TimeSavers  Coupon&ldquo; zu w&auml;hlen. Geben Sie uns die Mail oder die Adresse des Empf&auml;ngers an,  schicken wir der Person einen Gutschein zu, der nur die Anzahl an TimeSavers  Credits aufweist.");
define("GEST4","Einer Reuters-Studie  nach verwenden wir 10% der Arbeitszeit damit, uns um pers&ouml;nliche oder h&auml;usliche  Angelegenheiten zu k&uuml;mmern. <em>TimeSavers &ndash; Quality Personal Concierge</em> kann in Ihrer Firma ein Mittel sein, um die Vereinbarkeit von Berufs- und  Privatleben Ihrer Angestellten zu verbessern. Durch die Benutzung unseres  Service werden Ihre Angestellten weniger Sorgen und Stress haben und  engagierter am Arbeitsplatz sein. In der Tat zeigt eine PriceWaterhouseCoopers-Studie,  dass Firmen mit guten Arbeitsplatzbedingungen, f&uuml;r jeden in Personal  investierten Euro, 99 Cents Profit erzielen, w&auml;hrend ein durchschnittliches  europ&auml;isches Unternehmen f&uuml;r besagten Euro nur 14 Cents Gewinn erbringt.<br /><br />Ausserdem kann TimeSavers &ndash; Quality Personal  Concierge eine hervorragende Massnahme sein, um einen VIP-Kunden zu gewinnen  und/oder beizubehalten.
<br /><br />Sollten Sie interessiert sein  oder mehr Information wollen, bitten wir Sie, uns unter +34 93 304 00 32  anzurufen oder uns ein Mail an <a href='mailto:info@timesavers.es'>info@timesavers.es</a> zu schicken.");
define("SER_1","INTERNETSUCHE");
define("SER_2","DINNER ORGANISATION");
define("SER_3","CATERING");
define("SER_4","AUF LIEFERUNGEN WARTEN");
define("SER_5","DOG WALKER");
define("SER_6","BEH&Ouml;RDENG&Auml;NGE");
define("SER_7","EVENT ORGANISATION");
define("SER_8","PERS&Ouml;NLICHE AGENDA");
define("SER_9","PERSONAL SHOPPER");
define("SER_10","REISEPLANUNG");
define("SER_11","RESTAURANT RESERVIERUNG");
define("SER_12","TICKET RESERVIERUNG");
define("SER_13","ANRUFBEANTWORTERSERVICE");
define("SER_14","GESCHENKEKAUF");
define("SER_15","UMZ&Uuml;GE");
define("SER_16","BABYSITTER");
define("SER_17","AUFBEWAHRUNG HAUSSCHL&Uuml;SSEL");
define("SER_18","CHAUFFEUR");
define("SER_19","POSTABHOLUNG");
define("SER_20","KURIERDIENST");
define("SER_21","PEDIK&Uuml;RE / MANIK&Uuml;RE");
define("SER_22","FRISEUR");
define("SER_23","DI&Auml;TETIKER");
define("SER_24","PERSONAL TRAINER");
define("SER_25","SPA");
define("SER_26","MASSEUR");
define("SER_27","IMAGEBERATER");
define("SER_28","INNEN-/AUSSEN-DEKORATION");
define("SER_29","KLEMPNER");
define("SER_30","HANDWERKER");
define("SER_31","ELEKTRIKER");
define("SER_32","MALER");
define("SER_33","SCHLOSSER");
define("SER_34","PUTZSERVICE");
define("SER_35","W&Auml;SCHE WASCHEN");
define("SER_36","B&Uuml;GELN");
define("SER_37","AUTO WASCHEN");
define("SER_38","G&Auml;RTNER");
define("SER_39","PUTZEN NACH DINNER/PARTY");
define("SER_40","REINIGUNG");
define("SER_41","HAUSTIER-BETREUUNG");

define("SER0","Sollten Sie an einem Service interessiert sein, der nicht in unserer Liste  erscheint, z&ouml;gern Sie bitte nicht, uns unter der Nummer +34 93 304 00 32  anzurufen oder uns ein Mail an <a href='mailto:info@timesavers.es' class='blanco'><b>info@timesavers.es</b></a> zu schicken. Wir versprechen Ihnen, schnellstm&ouml;glich eine L&ouml;sung zu  pr&auml;sentieren.");
define("SER1","<b>INTERNETSUCHE:</b><br />Das Filtern von Millionen von Ergebnissen nach relevanten Resultaten kann  langwierig und erm&uuml;dend sein. Wir verlassen uns auf Experten, die Ihnen diese  Arbeit abnehmen.");
define("SER2","<b>DINNER ORGANISATION:</b><br />Erwarten Sie eine Gruppe von G&auml;sten und es fehlt Ihnen an  angemessenem Geschirr, Gl&auml;sern, Tischdekoration und dem Talent eines  Michelin-Chefs? Gestatten Sie uns, dies f&uuml;r Sie zu organisieren, und geniessen  Sie das Menu und die Gesellschaft Ihrer G&auml;ste.");
define("SER3","<b>CATERING:</b><br />Ist K&uuml;che gleich Alptraum f&uuml;r Sie? Wir arbeiten mit erfahrenen Chefs und  exzellenten Catering Services zusammen, damit Sie sich keine Sorgen machen  m&uuml;ssen.");
define("SER4","<b>AUF LIEFERUNGEN WARTEN:</b><br />Wenn Sie jemals auf die Lieferung, z. Bsp. eines neuen  Fernsehers, haben warten m&uuml;ssen, dann wissen Sie genau, dass die Lieferdienste  normalerweise ein Zeitfenster von 4-6 Stunden angeben. Was aber, wenn dies ein  normaler Arbeitstag f&uuml;r Sie ist? Wenn Sie sich nicht freinehmen k&ouml;nnen oder  wollen, warten wir gerne f&uuml;r Sie auf die Lieferung, damit Sie keine Zeit  verlieren.");
define("SER5","<b>DOG WALKER:</b><br />Ihr bester Freund ben&ouml;tigt Auslauf und im Park zu spielen? Wir bieten Ihnen  Spezialisten an, die dies &uuml;bernehmen.");
define("SER6","<b>BEH&Ouml;RDENG&Auml;NGE:</b><br />Beh&ouml;rdeng&auml;nge sind zumeist ein Graus. Beauftragen Sie uns damit! Wir stellen  Ihnen Fachkr&auml;fte zur Verf&uuml;gung die langj&auml;hrige Erfahrung damit haben.");
define("SER7","<b>EVENT ORGANISATION:</b><br />M&uuml;ssen Sie einen wichtigen Geburtstag oder ein Jubil&auml;um,  vielleicht sogar eine Hochzeit, organisieren, und es fehlt Ihnen an der n&ouml;tigen  Zeit? Unser professionelles Team steht Ihnen zu Diensten, um sicher zu gehen,  dass es ein unvergessliches Event wird.");
define("SER8","<b>PERS&Ouml;NLICHE AGENDA:</b><br />Sich an alle Meetings, Geburtstage und Veranstaltungen zu erinnern,  kann durchaus stressing sein. Delegieren Sie das Verwalten Ihrer pers&ouml;nlichen  Agenda an uns und Sie werden nie wieder eine wichtige Verabredung verpassen.");
define("SER9","<b>PERSONAL SHOPPER:</b><br />Ist shoppen zu gehen eine Tortur f&uuml;r Sie? Oder haben Sie einfach nicht  die Zeit dazu? Oder sind Sie vielleicht an einer anderen Meinung interessiert,  um Ihren Kleiderschrank zu erneuern? Wir  arbeiten mit Profis, die sie angemessen beraten and Sie auf der Shopping Tour  begleiten, oder direkt die Eink&auml;ufe f&uuml;r Sie erledigen.");
define("SER10","<b>REISEPLANUNG:</b><br />Die besten Fl&uuml;ge, ein angenehmes Hotel, interessante Ausfl&uuml;ge zu finden, damit  Ihr Urlaub aussergew&ouml;hnlich wird, ist oft schwieriger als erwartet. Konzentrieren  Sie sich darauf, die Koffer zu packen, und lassen Sie uns den Rest erledigen.");
define("SER11","<b>RESTAURANT RESERVIERUNG:</b><br />Sind Sie daran interessiert. ein neues Restaurant zu entdecken  oder haben Sie Probleme bei der Tischreservierung in letzter Minute? Wir haben ein breitgef&auml;chertes  Angebot zur Auswahl.");
define("SER12","<b>TICKET RESERVIERUNG:</b><br />Haben Sie Schwierigkeiten Tickets f&uuml;r die Show zu finden, auf die  Ihr Partner so versessen ist? Vertrauen Sie darauf, dass alles in Bewegung  setzen, um die Tickets f&uuml;r Sie zu ergattern.");
define("SER13","<b>ANRUFBEANTWORTERSERVICE:</b><br />Brauchen Sie jemanden, der in Ihrer Abwesenheit Anrufe beantwortet? Wir bieten  Ihnen einen Anrufbeantworterservice an und leiten Ihnen nur die wichtigsten  weiter.");
define("SER14","<b>GESCHENKEKAUF:</b><br />Manchmal ist es ziemlich schwierig, das richtige Geburtstagsgeschenk ausfindig  zu machen. Vertrauen  Sie uns, wir entdecken es f&uuml;r Sie.");
define("SER15","<b>UMZ&Uuml;GE:</b><br />Ein Umzug ist immer erm&uuml;dend und zeitaufwendig. Wir &uuml;bernehmen die Leitung gerne f&uuml;r Sie, um die  Anstregung f&uuml;r Sie bestm&ouml;glich zu reduzieren.");
define("SER16","<b>BABYSITTER:</b><br />W&uuml;rden Sie Ihren Partner gerne mit einem romantischen Abendessen &uuml;berraschen,  ohne sich Sorgen um Ihr Kind machen zu m&uuml;ssen? Vertrauen Sie unseren erfahrenen Babysittern  mit Referenzen.");
define("SER17","<b>AUFBEWAHRUNG HAUSSCHL&Uuml;SSEL:</b><br />Wenn Sie jemals die Hausschl&uuml;ssel verloren haben, wissen Sie,  wie teuer ein Schlosser sein kann. Wir bewahren gerne eine Kopie auf, um Ihnen  bei Bedarf zu helfen.");
define("SER18","<b>CHAUFFEUR:</b><br />Ihre Familie kommt in K&uuml;rze am Flughafen an und Ihnen  fehlt es an Zeit, sie selbst abzuholen. Nach einem Essen bei Freunden m&ouml;chten  Sie sich keine Sorgen um Polizeikontrollen machen m&uuml;ssen. Kein Problem, wir  schicken Ihnen einen Chauffeur, damit Sie sich relaxen k&ouml;nnen.");
define("SER19","<b>POSTABHOLUNG:</b><br />Wenn Sie nicht wollen, dass w&auml;hrend Ihres Urlaubs Ihr Briefkasten &uuml;berquillt,  schicken wir jemanden vorbei, der Ihre Post abholt und sie in Ihrer Abwesenheit  verwahrt.");
define("SER20","<b>KURIERDIENST:</b><br />M&uuml;ssen Sie etwas dringend in Barcelona verschicken? Wir kommen das Paket  abholen und liefern es dann aus.");
define("SER21","<b>PEDIK&Uuml;RE / MANIK&Uuml;RE:</b><br />Erlauben Sie sich ein bisschen Luxus und bestellen Sie diesen  Service. Wir schicken Ihnen jemanden vorbei.");
define("SER22","<b>FRISEUR:</b><br />Ein  Notfall? Ein unangek&uuml;ndigtes, wichtiges Meeting? Oder brauchen Sie jemanden,  der Ihnen zuhause die Haare schneidet? Einer unserer Profis kann jeden dieser F&auml;lle  l&ouml;sen.");
define("SER23","<b>DI&Auml;TETIKER:</b><br />Lassen  Sie sich von einem qualifizierten Experten beraten. Wir verlassen uns auf  Di&auml;tetiker, die Sie betreuen.");
define("SER24","<b>PERSONAL TRAINER:</b><br />Fehlt Ihnen die Zeit, sich ins Fitness Studio zu begeben?  Brauchen Sie ein pers&ouml;nliches Programm? Setzen Sie sich mit uns in Verbindung  und wir schicken Ihnen einen Trainer vorbei.");
define("SER25","<b>SPA:</b><br />Erlauben  Sie es sich, einen Moment abzuschalten und zu relaxen. Lehnen Sie sich zur&uuml;ck  und geniessen Sie es, dass sich jemand um sie k&uuml;mmert. Wir erm&ouml;glichen Ihnen  Zugang zu Barcelonas Top-Etablissements.");
define("SER26","<b>MASSEUR:</b><br />Stress-Symptome?  Lokale Schmerzen? Oder ben&ouml;tigen Sie eine therapeutische Behandlung? Ein  Spezialist weiss jeden dieser F&auml;lle zu behandeln und erm&ouml;glicht Ihnen, sich danach  sehr viel besser zu f&uuml;hlen.");
define("SER27","<b>IMAGEBERATER:</b><br />Steht  ein wichtiges Event an und sind Sie sich Ihres Auftritts unsicher, oder m&ouml;chten  Sie einfach Ihre Garderobe erneuern? Unsere Spezialisten beraten Sie und sorgen  sich um ihr Wohlbefinden.</li>");
define("SER28","<b>INNEN-/AUSSEN-DEKORATION:</b><br />Ben&ouml;tigen Sie  ein paar Tips, wie Sie Ihr Haus und Ihren Garten oder Ihre Terrasse gestalten  sollen? Oder wollen Sie, dass wir uns um die Instandhaltung k&uuml;mmern? W&auml;hlen Sie  die f&uuml;r Sie am geeigneste Option und erfreuen Sie sich des Ergebnisses.");
define("SER29","<b>KLEMPNER:</b><br />Wenn es sich um einen Notfall handelt und keine Zeit zu verlieren ist. Uns  stehen Profis zur Verf&uuml;gung, die Ihr Problem schnell zu l&ouml;sen wissen.");
define("SER30","<b>HANDWERKER:</b><br />Die kleinen Reparaturen zuhause m&uuml;ssen kein Problem sein. Wir bieten Ihnen  L&ouml;sungen und finden f&uuml;r Sie den richtigen Handwerker.");
define("SER31","<b>ELEKTRIKER:</b><br />Lassen  Sie nur ausgebildete Spezialisten sich mit der elektrischen Installation Ihres  Zuhauses befassen. Wir  schicken Ihnen jemanden vorbei.");
define("SER32","<b>MALER:</b><br />Vergessen Sie die endlosen Stunden, die es dauert, selber ein Haus zu  streichen. Wir suchen Ihnen einen professionellen Service und k&uuml;mmern uns  darum, dass Sie das gew&uuml;nchste Resultat erhalten.");
define("SER33","<b>SCHLOSSER:</b><br />Z&ouml;gern Sie nicht in einer Notsituation. Schnelligkeit und Effizienz sind die  Antwort. Bitte kontaktieren  Sie uns.</li>");
define("SER34","<b>PUTZSERVICE:</b><br />Egal, ob Sie eine kompetente und professionelle Person f&uuml;r einen st&auml;ndigen oder gelegentlichen Service ben&ouml;tigen, fragen Sie uns, wir arbeiten mit       erfahrenen Profis.");
define("SER35","<b>W&Auml;SCHE WASCHEN:</b><br />Beauftragen Sie uns mit dem W&auml;sche waschen. Wir garantieren Ihnen einen p&uuml;nktlichen Service, eine einwandfreie Behandlung und Resultate");
define("SER36","<b>B&Uuml;GELN:</b><br />Lassen Sie uns das l&auml;stige B&uuml;geln f&uuml;r Sie &uuml;bernehmen. Sie werden nicht entt&auml;uscht sein.");
define("SER37","<b>AUTO WASCHEN:</b><br />Wir &uuml;bernehmen sowohl die Routinew&auml;sche als auch eine gr&uuml;ndliche Reinigung, um zu gew&auml;hrleisten, dass Ihr Auto weiterhin einwandfrei funktioniert.");
define("SER38","<b>G&Auml;RTNER:</b><br />&Uuml;berlassen Sie  Ihre Pflanzen der Pflege eines Experten, der auch mit den kleinen Problemen,  die anfallen k&ouml;nnen, umzugehen weiss.");
define("SER39","<b>PUTZEN NACH DINNER/PARTY:</b><br />Vergessen Sie den Tag danach. Wir &uuml;bernehmen die Verantwortung daf&uuml;r, dass alles wieder sauber ist, damit Sie sich nur an die sch&ouml;nen Momente Ihrer Party erinnern       brauchen.");
define("SER40","<b>REINIGUNG:</b><br />Fehlt Ihnen immer die Zeit, Ihre Kleidung zur Reinigung zu bringen? Wir garantieren Ihnen die richtige L&ouml;sung und, dass Sie Ihre Kleidung einwandfrei zur&uuml;ckbekommen.");
define("SER41","<b>HAUSTIER-BETREUUNG:</b><br />Vertrauen Sie Ihre Haustiere den Besten an. Erwiesene Experten k&uuml;mmern sich um sie, als w&auml;ren sie ihre eigenen.");
?>