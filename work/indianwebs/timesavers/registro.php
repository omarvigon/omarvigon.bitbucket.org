<?php include("0cabecera.php");?>

	<h2><?=BT9;?></h2>
    <div class="colum1">
    	<img src="img/fondo_lineazul1.gif" class="bloque" alt=""/>
		<?=REG_INICIO;?>
        <img src="img/fondo_lineazul2.gif" class="bloque" alt=""/>
    </div>
    <div class="colum2_larga">
    <?php 
	if($_POST["correo"])
		usuario_nuevo();
	else{ ?>
        <form method="post" action="registro.php" name="form_registro" enctype="multipart/form-data">
        <table cellspacing="0" cellpadding="2">
        <tr>
        	<td><?=REG_TRATA;?> :</td>
            <td><select name="tratamiento" class="registro"><option value="<?=REG_TRATA_1;?>"><?=REG_TRATA_1;?></option><option value="<?=REG_TRATA_2;?>"><?=REG_TRATA_2;?></option><option value="<?=REG_TRATA_3;?>"><?=REG_TRATA_3;?></option></select></td>
        </tr>
        <tr>
        	<td><?=REG_NOMBRE;?> :</td>
            <td><input type="text" name="nombre" class="datos"/></td>
        </tr>
        <tr>
        	<td><?=REG_APELLIDOS;?> :</td>
            <td><input type="text" name="apellidos" class="datos"/></td>
        </tr>
        <tr>
        	<td><?=REG_SEXO;?> :</td>
            <td><select name="sexo" class="registro"><option value="1"><?=REG_SEXO_1;?></option><option value="0"><?=REG_SEXO_0;?></option></select></td>
        </tr>
        <tr>
        	<td><?=REG_NACIMIENTO;?> :</td>
            <td><select name="nacimiento_dia">
        		<option value='00'><?=FECHA_DIA;?></option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option></select>
        		<select name="nacimiento_mes">
        		<option value='00'><?=FECHA_MES;?></option><option value='01'><?=MES_1;?></option><option value='02'><?=MES_2;?></option><option value='03'><?=MES_3;?></option><option value='04'><?=MES_4;?></option><option value='05'><?=MES_5;?></option><option value='06'><?=MES_6;?></option><option value='07'><?=MES_7;?></option><option value='08'><?=MES_8;?></option><option value='09'><?=MES_9;?></option><option value='10'><?=MES_10;?></option><option value='11'><?=MES_11;?></option><option value='12'><?=MES_12;?></option></select>
        		<select name="nacimiento_ano">
                <option value='0000'><?=FECHA_ANO;?></option>
        		<option value='1930'>1930</option><option value='1931'>1931</option><option value='1932'>1932</option><option value='1933'>1933</option><option value='1934'>1934</option><option value='1935'>1935</option><option value='1936'>1936</option><option value='1937'>1937</option><option value='1938'>1938</option><option value='1939'>1939</option><option value='1940'>1940</option><option value='1941'>1941</option><option value='1942'>1942</option><option value='1943'>1943</option><option value='1944'>1944</option><option value='1945'>1945</option><option value='1946'>1946</option><option value='1947'>1947</option><option value='1948'>1948</option><option value='1949'>1949</option>
                <option value='1950'>1950</option><option value='1951'>1951</option><option value='1952'>1952</option><option value='1953'>1953</option><option value='1954'>1954</option><option value='1955'>1955</option><option value='1956'>1956</option><option value='1957'>1957</option><option value='1958'>1958</option><option value='1959'>1959</option><option value='1960'>1960</option><option value='1961'>1961</option><option value='1962'>1962</option><option value='1963'>1963</option><option value='1964'>1964</option><option value='1965'>1965</option><option value='1966'>1966</option><option value='1967'>1967</option><option value='1968'>1968</option><option value='1969'>1969</option>
  				<option value='1970'>1970</option><option value='1971'>1971</option><option value='1972'>1972</option><option value='1973'>1973</option><option value='1974'>1974</option><option value='1975'>1975</option><option value='1976'>1976</option><option value='1977'>1977</option><option value='1978'>1978</option><option value='1979'>1979</option><option value='1980'>1980</option><option value='1981'>1981</option><option value='1982'>1982</option><option value='1983'>1983</option><option value='1984'>1984</option><option value='1985'>1985</option><option value='1986'>1986</option><option value='1987'>1987</option><option value='1988'>1988</option><option value='1989'>1989</option>
                <option value='1990'>1990</option><option value='1991'>1991</option><option value='1992'>1992</option></select></td>
        </tr>
        <tr>
        	<td><?=REG_NACIONALIDAD;?> :</td>
            <td><select name="pais" class="registro"><option value="0">Seleccionar...</option><?php include("privado/paises.php");?></select></td>
        </tr>
        <tr>
        	<td><?=REG_DOCUMENTO;?> :</td>
            <td><select name="dni_tipo" class="registro"><option value="DNI">DNI</option><option value="NIE">NIE</option><option value="PAS">Pasaporte</option></select></td>
        </tr>
        <tr>
        	<td><?=REG_DOCNUM;?> : </td>
            <td><input type="text" name="dni" class="datos" maxlength="16"/></td>
        </tr>
        <tr>
        	<td><?=REG_DIRECCION;?> :</td>
            <td><input type="text" name="direccion" class="datos"/></td>
        </tr>
        <tr>
        	<td><?=REG_POBLACION;?> : </td>
            <td><input type="text" name="poblacion" class="datos"/></td>
        </tr>
        <tr>
        	<td><?=REG_CP;?> :</td>
            <td><input type="text" name="cp" class="datos"/></td>
        </tr>
        <tr>
        	<td><?=REG_TEL;?> :</td>
            <td><input type="text"  name="telefono" class="datos" maxlength="16"/></td>
        </tr>
        <tr>
        	<td><?=REG_MOVIL;?> :</td>
            <td><input type="text" name="movil" class="datos" maxlength="16"/></td>
        </tr>
        <tr>
        	<td>Fax :</td>
            <td><input type="text" name="fax" class="datos" maxlength="16"/></td>
        </tr>
        <tr>
        	<td>E-mail :(*)</td>
            <td><input type="text" name="correo" class="datos" maxlength="48"/></td>
        </tr>
        <tr>
        	<td><?=REG_CONTRA;?> :</td>
            <td><input type="text" name="password" class="datos" value="<?=REG_CONTRA3;?>" readonly="readonly"/></td>
        </tr>
        <tr>
        	<td><?=REG_IDIOMA;?> :</td>
            <td><select name="idioma" class="registro"><option value="0"><?=REG_ELEGIR;?></option><option value="en">English</option><option value="es">Castellano</option><option value="ca">Catal&agrave;</option><option value="de">Deutsch</option><option value="fr">Fran&ccedil;ais</option><option value="it">Italiano</option></select></td>
        </tr>
        <tr>
        	<td><?=REG_MODO;?>  :</td>
            <td><select name="comunica" class="registro"><option value="email">E-Mail</option><option value="telefono"><?=REG_TEL;?></option><option value="movil"><?=REG_MOVIL;?></option></select></td>
        </tr>
        <tr>
        	<td><?=REG_CONOCIO;?></td>
            <td><select name="conocio" class="registro"><option value="0"><?=REG_ELEGIR;?></option><option value="internet">Internet</option><option value="proveedor"><?=REG_CONCIO_1;?></option><option value="amigo"><?=REG_CONCIO_2;?></option><option value="anuncio"><?=REG_CONCIO_3;?></option><option value="folleto"><?=REG_CONCIO_4;?></option><option value="tv"><?=REG_CONCIO_5;?></option><option value="radio">Radio</option><option value="otro"><?=REG_CONCIO_6;?></option></select></td>
        </tr>
        <tr>
        	<td><?=REG_COMENTA;?> :</td>
            <td><textarea name="comentarios" class="registro" rows="4"></textarea></td>
        </tr>
        <tr>
        	<td><?=REG_FOTO;?></td>
            <td><input type="file" name="foto" size="36"></td>
        </tr>
        </table>
        <p><input type="checkbox" name="acepto0"/> (*) <?=CT3;?></p>
        <p style="font-size:10px;"><input type="checkbox" name="acepto1" value="1"/> <?=CT4;?></p>
        <p style="font-size:10px;"><input type="checkbox" name="acepto2" value="1"/> <?=CT5;?></p>
        <p style="font-size:10px;"><input type="checkbox" name="acepto3" value="1"/> <?=CT6;?></p>
        <p>(*) <?=CONT6;?></p> 
        <p><b><?=REG_CODIGO;?></b></p>
        <p><input type="text" name="codigo" style="margin:0 0 0 120px;" class="datos"/></p> 
        
        <p style="margin:0 0 0 220px;"><input type="button" onclick="validar_registro()" value=" <?=REG_REGISTRAR;?> " /></p>
        <p><?=REG_BAJA;?></p>
        </form>
	<? }?>	
    </div>

<?php include("0piecera.php");?>