<?php
session_start(); 
if($_GET["lang"])				$_SESSION["lang"]=$_GET["lang"];
else if(!$_SESSION["lang"])		$_SESSION["lang"]="es";	

include("idiomas/".$_SESSION["lang"].".php");
include("privado/funciones.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>TimeSavers Quality Personal Concierge</title>
	<meta name="description" content="Timesavers Quality Personal Concierge" />
	<meta name="keywords" content="Timesavers Quality Personal Concierge" />
    <meta name="lang" content="es" />
    <meta name="author" content="www.indianwebs.com" />
	<link rel="stylesheet" type="text/css" href="http://www.timesavers.es/privado/1estilos.css" />
    <script language="javascript" type="text/javascript" src="http://www.timesavers.es/privado/1funciones.js"></script>
    <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>

<div id="idiomas">
	<span><a title="TimeSavers" href="http://www.timesavers.es">Home</a></span>
	<p><a title="Castellano Timesavers" href="<?=$_SERVER['PHP_SELF']?>?lang=es">Castellano</a> | <a title="Catala Timesavers" href="<?=$_SERVER['PHP_SELF']?>?lang=ca">Catal&agrave;</a> | <a title="English Timesavers" href="<?=$_SERVER['PHP_SELF']?>?lang=en">English</a> 
    | <a title="Deutsch Timesavers" href="<?=$_SERVER['PHP_SELF']?>?lang=de">Deutsch</a> | <a title="Italiano Timesavers" href="<?=$_SERVER['PHP_SELF']?>?lang=it">Italiano</a>  | <a title="Francais Timesavers" href="<?=$_SERVER['PHP_SELF']?>?lang=fr">Fran&ccedil;ais</a></p>
</div>
<div id="logo">
	<a title="TimeSavers" href="index.php"><img title="TimeSavers" src="img/logo_timesavers.gif" class="izq" alt="TimeSavers"/></a>
	<p><img title="Tfn" src="img/ico_tel.gif" align="absmiddle" alt="Tel"/> +34 93 304 00 32<br /><a title="Contact" href="mailto:info@timesavers.es">info@timesavers.es</a></p>
</div>
<div id="botonera">
	<form method="post" action="usuarios.php?modo=datos" name="form_comprobar">
		<a href="usuarios.php"><?=USER_MI;?></a><br />
	<?php if($_SESSION["user_id"]) {?>
    	<a href="usuarios.php?modo=salir" class="exit"><img title="Logout" src="img/ico_exit.gif" align="absmiddle" alt="Logout" /></a>
    <? } else { ?>
    	
    	<input type="text" name="login" value="Login..." maxlength="24" class="caja1"/> 
    	<input type="password" name="password" value="password" maxlength="24" class="caja3"/>
        <div style="position:absolute;left:2px;top:36px;width:240px;">
    		<a title="Register" href="registro.php"><?=BT9;?></a>&nbsp;&nbsp;&nbsp;
    		<a title="Data" href="datos.php"><?=BT7;?></a>
            <input type="submit" value=" <?=BT10;?> " class="caja2"/>
    	</div>
    <? } ?>
    </form>
    
	<a title="Timesavers" class="boton1" href="empresa.php"><?=BT1;?></a> 
    <a title="<?=BT2;?>" class="boton2" href="servicios.php"><?=BT2;?></a> 
	<a title="<?=BT3;?>" class="boton3" href="gestiones.php"><?=BT3A;?><br />&amp; <?=BT3B;?></a> 
    <a title="<?=BT4;?>" class="boton4" href="prensa.php"><?=BT4;?></a> 
	<a title="Links" class="boton5" href="links.php">Links</a> 
    <a title="FAQ" class="boton6" href="faq.php">FAQ</a> 
	<a title="<?=BT5A;?>" class="boton7" href="contacto.php"><?=BT5A;?></a>
    
    <div style="margin:70px 0 0 0;">
    <?php switch($_SESSION["lang"]) { 
	
		case "en": ?>
        <script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','900','height','160','title','TimeSavers_en','src','img/banner_timesavers_en','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','img/banner_timesavers_en' ); //end AC code
		</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="900" height="160" title="TimeSavers_en">
    	<param name="movie" value="img/banner_timesavers_en.swf" /><param name="quality" value="high" />
    	<embed src="img/banner_timesavers_en.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="900" height="160"></embed>
    	</object></noscript>
		<? break;
		
		case "de": ?>
        <script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','900','height','160','title','TimeSavers_de','src','img/banner_timesavers_de','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','img/banner_timesavers_de' ); //end AC code
		</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="900" height="160" title="TimeSavers_de">
    	<param name="movie" value="img/banner_timesavers_de.swf" /><param name="quality" value="high" />
    	<embed src="img/banner_timesavers_de.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="900" height="160"></embed>
    	</object></noscript>
		<? break;
		
		default: ?>
        <script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','900','height','160','title','TimeSavers','src','img/banner_timesavers','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','img/banner_timesavers' ); //end AC code
		</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="900" height="160" title="TimeSavers">
    	<param name="movie" value="img/banner_timesavers.swf" /><param name="quality" value="high" />
    	<embed src="img/banner_timesavers.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="900" height="160"></embed>
    	</object></noscript>
		<? break;
	}?>
    </div>
</div>
<div id="cuerpo">