<?php include("0cabecera.php")?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Panel de control</title>
    <link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
    <script type="text/javascript" src="includes/_funciones.js"></script>
	<!--
	<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
	<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
	-->
    <script type="text/javascript" src="editor_tiny/tiny_mce.js"></script>
	<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		//theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	})
	</script>
</head>

<body>
<h2><img src="http://www.timesavers.es/img/logo_timesavers.gif" alt="" style="margin:-12px 12px 4px -4px;" align="absmiddle" /> Panel de control</h2>
<?php if($_SESSION["autentico"]) { ?>
	
    <div class="botonera">
    [+] <a href="index.php">Inicio</a>
    [+] <a href="editar.usuario.php">Usuarios</a>
    [+] <a href="editar.pedido.php">Pedidos</a>
    [+] <a href="editar.noticias.php">Editar Noticias</a>
    [+] <a href="contenidos.php?pag=enlaces&lang=es">Editar Links</a>
    [+] <a href="contenidos.php?pag=prensa&lang=es">Editar Prensa</a>
    [+] <a href="contenidos.php?pag=medios&lang=es">Editar Medios</a>
    [+] <a href="<?=$_SERVER['PHP_SELF']."?logout=on" ?>"> Salir</a>
	</div>
    
<? } else {?>
    <form name="form1" style="margin:16px 0;" method="post" action="<?=$_SERVER['PHP_SELF']?>">
    <table cellpadding="4" cellspacing="0">
    <tr>
        <td>Usuario</td>
        <td><input type="text" name="login" size="32" maxlength="32" /></td>
    </tr>
    <tr>
        <td>Contrase&ntilde;a</td>
        <td><input type="password" name="password" size="32" maxlength="32" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><input type="submit" value="  ENVIAR  " /></td>
    </tr>
    </table>
    </form>
<? } ?>

<?php if($_GET["borrar"])
{
	if(ejecutar("delete from ".$tabla_actual." where id='".$_GET["borrar"]."'"))
		echo "<h4>El registro se ha borrado</h4>";	
}
?>