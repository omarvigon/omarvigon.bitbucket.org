<?php include("0cabecera.php");?>

    <div id="cuerpo">
    	<h1>CFR M&Egrave;TODE</h1>
        <?php switch($_SESSION["lang"])
		{
			case "esp": ?>
			<h2>Apoyo metodol&oacute;gico</h2>
			<p>Est&aacute;is considerando  realizar un estudio relacionado con medicamentos? Os hace falta escoger el dise&ntilde;o m&aacute;s  adecuado? Necesit&aacute;is un <i>medical writer</i> experimentado  para escribir el protocolo del estudio, y poder presentarlo  con &eacute;xito a los Comit&eacute;s de &Eacute;tica o a las agencias de financiaci&oacute;n? Dejad que os  ayudemos. </p>
			<p>Podemos dise&ntilde;ar un  estudio de casos y controles que os ayude a evaluar reacciones adversas u otros  acontecimientos poco frecuentes de vuestro producto. Separad lo real de  lo coincidente. </p>
			<p>Podemos dise&ntilde;ar un estudio de cohortes que os ofrezca una forma eficiente de evaluar los  efectos de los f&aacute;rmacos que os interesen. Entrad en contacto con los individuos  reales que usan vuestro medicamento.</p>
			<p>Os podemos ayudar a dise&ntilde;ar un ensayo cl&iacute;nico, la referencia en la investigaci&oacute;n farmacol&oacute;gica. Desde la concepci&oacute;n del plan de desarrollo del ensayo  cl&iacute;nico, que es necesario presentar a las Autoridades Reguladoras, hasta las  especificidades del dise&ntilde;o del estudio.</p>
			<p>Podemos realizar un meta-an&aacute;lisis, probablemente el dise&ntilde;o cient&iacute;fico con m&aacute;s impacto  cient&iacute;fico potencial. Se trata de un m&eacute;todo extremadamente eficiente de evaluar  vuestro f&aacute;rmaco y el de los competidores y de buscar nuevas indicaciones para  vuestro producto.</p>
			<? break;
			
			case "cat": ?>
            <h2>Suport metodol&ograve;gic</h2>
			<p>Esteu considerant de realitzar un estudi  relacionat amb medicaments? Us cal triar el disseny m&eacute;s adequat? Necessiteu un <i>medical writer</i> experimentat per a  escriure el protocol de l&rsquo;estudi, per tal que pugueu presentar-lo amb &egrave;xit als  Comit&egrave;s d&rsquo;&Egrave;tica o les ag&egrave;ncies de finan&ccedil;ament? Deixeu que us ajudem.</p>
			<p>Podem dissenyar un estudi de casos i controls  que us ajudi a avaluar reaccions adverses o altres esdeveniments poc freq&uuml;ents del vostre producte.  Trieu all&ograve; que &eacute;s real d&rsquo;all&ograve; que &eacute;s coincid&egrave;ncia.</p>
			<p>Podem dissenyar un estudi de cohorts que us  ofereixi una forma eficient d&rsquo;avaluar els efectes dels f&agrave;rmacs que us  interessin. Entreu en contacte amb els individus reals que prenen el vostre  medicament.</p>
			<p>Us podem ajudar a dissenyar un assaig cl&iacute;nic,  la refer&egrave;ncia en la recerca farmacol&ograve;gica. Des de la concepci&oacute; del pla de  desenvolupament de l&rsquo;assaig cl&iacute;nic que us caldr&agrave; presentar a les Autoritats  Reguladores, fins a les especificitats del disseny de l&rsquo;estudi.</p>
			<p>Podem realitzar una metan&agrave;lisi, probablement  el disseny cient&iacute;fic amb m&eacute;s impacte cient&iacute;fic potencial. Es tracta d&rsquo;un m&egrave;tode  extremadament eficient d&rsquo;avaluar el vostre f&agrave;rmac i el dels competidors i de  cercar noves indicacions per al vostre producte.</p>
			<? break;
			
			case "ing":
			default: ?>
            <h2>Methodological support</h2>
    	    <p>Are you considering conducting a clinical  study? Do you need help determining the best study design? Do you need an  experienced medical writer to draft the study protocol so that you can  successfully present it to the IRB or funding agencies?&nbsp; Let us help you.</p>
			<p>1.- We can perform a meta-analysis, the  research design with <b>the highest  potential scientific impact</b>. It is an extremely efficient tool for  assessing your drug and the competitors, and to search for new indications for  your product. Get efficacy evidence when direct head-to-head comparisons are  missing.</p>
			<p>2.- We can help you design a clinical  trial, <b>the gold standard in  pharmacological research</b>. From the inception of the clinical trial  development plan that you need to present to the Regulatory Agencies, to the  specificities of the actual trial design, with special emphasis on the study protocol.</p>
			<p>3.- We can design a cohort study that will  offer you an efficient way of evaluating the effect of the drugs you are  interested in. <b>Get in touch with the  real individuals</b> who are using your drug.</p>
			<p>4.- We can design a case-control study that  will help you better assess the safety profile of your drug or find out about  events that may jeopardize your product. <b>Sort  out what is real from what is coincidental</b>.</p>
			<? break;
		} ?>
        
        <p>&nbsp;</p>
        
<?php include("0piecera.php");?>