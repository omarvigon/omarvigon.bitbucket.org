<?php include("0cabecera.php");?>

    <div id="cuerpo">
    	<h1>CFR M&Egrave;TODE</h1>
        <?php switch($_SESSION["lang"])
		{
			case "esp": ?>
			<h2>Idea global</h2>
            <p>La investigaci&oacute;n m&eacute;dica  es compleja, y la especializaci&oacute;n una necesidad. Esta situaci&oacute;n la comparten a  la vez los promotores de la investigaci&oacute;n p&uacute;blica y privada: conoc&eacute;is bien  vuestro campo, ten&eacute;is ideas brillantes que quer&eacute;is tirar adelante, pero a  menudo los plazos son justos y el tiempo limitado.</p>
            <p>Estamos preparados para  ayudaros.</p>
            <p>CFR M&egrave;tode es una compa&ntilde;&iacute;a joven con ganas de ayudaros siempre  que no teng&aacute;is el tiempo suficiente o los recursos humanos para hacer frente a aquel  esfuerzo extra hacia la excelencia en investigaci&oacute;n. </p>
            <p>En CFR M&egrave;tode somos un equipo joven de investigadores que puede  escribir un art&iacute;culo r&aacute;pida y adecuadamente, preparar un p&oacute;ster o dise&ntilde;ar aquel estudio que os hace falta por lograr  vuestros objetivos. Es nuestro trabajo y queremos hacerlo bien.</p>
            <p>Os ayudamos?</p>
            <ul>
            <li><a title="Soporte  metodologico" href="apoyo-metodologico.php">Soporte  metodol&oacute;gico</a></li>
            <li><a title="Evaluacion de la evidencia" href="pruebas-evaluacion.php">Evaluaci&oacute;n de la evidencia</a></li>
            <li><a title="Medical writing" href="medical-writing.php">Medical writing</a></li>
            </ul>
			<? break;
			
			case "cat": ?>
            <h2>Idea global</h2>
            <p>La recerca m&egrave;dica &eacute;s complexa, i  l&rsquo;especialitzaci&oacute; una necessitat. Aquesta situaci&oacute; la comparteixen alhora els  impulsors de la recerca p&uacute;blica i privada: coneixeu b&eacute; el vostre camp, teniu  idees brillants que voleu tirar endavant, per&ograve; massa sovint els terminis s&oacute;n  justos i el temps limitat. </p>
            <p>Estem preparats per a ajudar-vos. </p>
            <p>CFR M&egrave;tode &eacute;s una  companyia jove amb ganes d&rsquo;ajudar-vos sempre que us manquin temps o recursos  humans per a fer aquell esfor&ccedil; extra cap a l&rsquo;excel&middot;l&egrave;ncia en la recerca. </p>
            <p>A CFR  M&egrave;tode som un equip jove d&rsquo;investigadors que pot escriure un article r&agrave;pida i  adequadament, preparar un p&ograve;ster o dissenyar aquell estudi que us fa falta per  assolir els vostres objectius. &Eacute;s la nostra feina i volem fer-la b&eacute;.</p>
            <p>Us ajudem?</p>
            <ol>
              <li><a title="Suport metodologic" href="apoyo-metodologico.php">Suport metodol&ograve;gic</a></li>
              <li><a title="Avaluacio de evidencia" href="pruebas-evaluacion.php">Avaluaci&oacute; de l&rsquo;evid&egrave;ncia</a></li>
              <li><a title="Medical writing" href="medical-writing.php">Medical writing</a></li>
            </ol>
			<? break;
			
			case "ing":
			default: ?>
            <h2>Overview</h2>
            <p>Medical research is complex, and  specialization is a must. Both public and private research contractors and  individual researchers share this situation: you know your field well, you have  brilliant ideas that you are want to carry forward, but all too often schedules  are tight and time is limited. </p>
        	<p><b>We are ready to help you.</b></p>
        	<p><b>CFR  M&egrave;tode</b> is a young company eager to help you  whenever you don&rsquo;t have the time or human resources to make that extra effort  towards excellence in research. </p>
        	<p>In <b>CFR  M&egrave;tode</b> we are a team of clinical pharmacologists and medical writers who  can rapidly and accurately design a study, draft a paper, or prepare a poster  just in time so you can achieve your goals. It&rsquo;s our job and we are eager to do  it right.</p>
        	<p>Let us help you.</p>
        	<ul>
        	<li><a title="Methodological support" href="apoyo-metodologico.php">Methodological support</a></li>
        	<li><a title="Evidence assessment" href="pruebas-evaluacion.php">Evidence assessment</a></li>
        	<li><a title="Medical writing" href="medical-writing.php">Medical writing</a></li>
        	</ul>
        	<p>&nbsp;</p>
			<? break;
		} ?>
        
<?php include("0piecera.php");?>