<?php include("0cabecera.php");?>

    <div id="cuerpo">
    	<h1>CFR M&Egrave;TODE</h1>
        <h2>Home</h2>
        <p>&ldquo;Ready to help you&rdquo;</p>
        <?php switch($_SESSION["lang"])
		{
			case "esp": ?>
			<p>Estamos preparados para  ayudarles a conseguir sus metas cient&iacute;ficas. Ya sea a trav&eacute;s de soporte  metodol&oacute;gico global, de la evaluaci&oacute;n precisa de la evidencia cient&iacute;fica o de  tareas de <i>medical writing</i> de m&aacute;ximo  nivel, estamos preparados para ayudarles a avanzar.</p>
			<? break;
			
			case "cat": ?>
            <p>Estem llestos per ajudar-vos a aconseguir les  vostres fites cient&iacute;fiques. Ja sigui a trav&eacute;s de suport metodol&ograve;gic global, de  l&rsquo;avaluaci&oacute; precisa de l&rsquo;evid&egrave;ncia cient&iacute;fica o de tasques de <i>medical writing</i> de m&agrave;xim nivell, estem  preparats per ajudar-vos a avan&ccedil;ar.</p>
			<? break;
			
			case "ing":
			default: ?>
			<p>We are ready to help you achieve your  scientific goals. Either through global methodological support, precise  assessment of the scientific evidence, or top quality medical writing tasks, we  are ready to help you move ahead.</p>
			<? break;
		} ?>
        
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        
<?php include("0piecera.php");?>
