<?php include("0cabecera.php");?>

    <div id="cuerpo">
    	<h1>CFR M&Egrave;TODE</h1>
        <h2>Medical writing</h2> 
        <?php switch($_SESSION["lang"])
		{
			case "esp": ?>
			<p><i>Art&iacute;culos en revistas:</i><b>&quot;Publicar o morir&quot;.</b> Dejar vuestros  resultados sin publicar no es justo ni h&aacute;bil. Para que vuestra carrera avance  ten&eacute;is que publicar, es un hecho. Pero para publicar hace falta tiempo, y eso  es a menudo una gran limitaci&oacute;n.</p>
            <p>Os podemos preparar  un manuscrito de m&aacute;xima calidad que puede ayudaros a avanzar tanto a vosotros  como a vuestra compa&ntilde;&iacute;a. Os podemos ayudar en todas las fases: desde la  concepci&oacute;n del art&iacute;culo hasta su publicaci&oacute;n en una  revista internacional revisada.</p>
			<p>Presentaciones a congresos y p&oacute;steres: dadnos el mensaje que quer&eacute;is transmitir  a vuestra audiencia y os ayudaremos a darle forma y a transmitirlo con  claridad. <b>Dejad un impacto duradero en vuestra audiencia.</b></p>
			<? break;
			
			case "cat": ?>
            <p><i>Articles en  revistes:</i><b>&ldquo;Publicar o morir&rdquo;.</b> Deixar els  vostres resultats sense publicar no &eacute;s just ni h&agrave;bil. Perqu&egrave; la vostra carrera  avanci heu de publicar, aix&ograve; &eacute;s un fet. Per&ograve; per a publicar us cal temps, i  aix&ograve; &eacute;s sovint una gran limitaci&oacute;.</p>
            <p>Us podem preparar un manuscrit de m&agrave;xima  qualitat que pot ajudar-vos a avan&ccedil;ar tant a vosaltres com a la vostra  companyia. Us podem ajudar en totes les fases: des de la concepci&oacute; de l&rsquo;article  fins a la seva publicaci&oacute; en una revista internacional revisada.</p>
			<p>Presentacions a congressos  i p&ograve;sters: digueu-nos el missatge que voleu transmetre a la vostra audi&egrave;ncia i  us ajudarem a donar-li forma i a transmetre&rsquo;l amb claredat. <b>Deixeu un impacte  durador en la vostra audi&egrave;ncia.</b></p>
			<? break;
			
			case "ing":
			default: ?>
            <p><i>Journal papers</i>:<b>&ldquo;Publish or perish&rdquo;.</b> Leaving your results  unpublished in neither fair nor smart. For your career to advance you have to  publish. But this takes time, and often this is a major limitation. We can  draft a top quality paper that can help you and your company or institution  move ahead.</p>
			<p>We can help you  draft your article through all its phases: from its inception to its  publication in an international peer-reviewed journal.</p>
			<p><i>Congress presentations and posters</i>: tell us the message you want to convey to your audience, and we  will help you shaping it and transmitting it clearly. <b>Leave a lasting impact in your audience</b>.</p>
			<? break;
		} ?>
        
        <p>&nbsp;</p><p>&nbsp;</p>
        <p>&nbsp;</p><p>&nbsp;</p>
        <p>&nbsp;</p><p>&nbsp;</p>
        
<?php include("0piecera.php");?>