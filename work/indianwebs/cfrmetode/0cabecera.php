<?php
session_start();

if($_GET["lang"])			
	$_SESSION["lang"]=$_GET["lang"];
else	
{
	if(!$_SESSION["lang"])	
		$_SESSION["lang"]="ing";
}
define("boton1_esp","Idea global");
define("boton2_esp","Apoyo metodol&oacute;gico");
define("boton3_esp","Evaluaci&oacute;n de evidencia");
define("boton1_ing","Overview");
define("boton2_ing","Methodological support");
define("boton3_ing","Evidence assessment");
define("boton1_cat","Idea global");
define("boton2_cat","Suport metodol&ograve;gic");
define("boton3_cat","Avaluaci&oacute; de l'evid&egrave;ncia");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)">
    <meta name="revisit" content="7 days">
    <meta name="robots" content="index,follow,all">
	<meta name="description" content="CFR Metode Escritura Medica Barcelona (Medical Writing)">
	<meta name="keywords" content="CFR Metode,Medical Writing,medical writing barcelona,Escritura medica, escritura medica barcelona">
    <meta name="lang" content="es,sp,spanish,español,espanol,castellano">
    <meta name="author" content="www.indianwebs.com">
	<title>CFR METODE Medical Writing Barcelona</title>
	<link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="shortcut icon" href="img/cfr_metode.ico" type="image/x-icon">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>

<body>
<div id="central">
	<div id="top">
    	<cite>
        <a href="<?php echo $_SERVER['PHP_SELF']?>?lang=ing" title="CFR Metode English">english&nbsp;</a>
        <a href="<?php echo $_SERVER['PHP_SELF']?>?lang=cat" title="CFR Metode Catala">catal&agrave;&nbsp;</a>
        <a href="<?php echo $_SERVER['PHP_SELF']?>?lang=esp" title="CFR Metode Castellano">castellano&nbsp;</a>
        </cite>
    	<span>
    	<a href="http://www.cfrmetode.com/descripcion.php" title="CFR Metode Overview"><? echo constant("boton1_".$_SESSION["lang"]);?></a>
    	<a href="http://www.cfrmetode.com/apoyo-metodologico.php" title="CFR Metode Methodological support"><? echo constant("boton2_".$_SESSION["lang"]);?></a>
        <a href="http://www.cfrmetode.com/pruebas-evaluacion.php" title="CFR Metode Evidence assessment"><? echo constant("boton3_".$_SESSION["lang"]);?></a>
        <a href="http://www.cfrmetode.com/medical-writing.php" title="CFR Metode Medical writing">Medical writing</a>
        <a href="http://www.cfrmetode.com/clientes.php" title="CFR Metode Clients">Clients</a>
        </span>
    </div>
    <div id="imagen">
    	<a href="http://www.cfrmetode.com/index.php"><img title="CFR Metode" src="img/cfr_cabecera3.jpg" alt="CFR Metode" border="0"></a>
    </div> 