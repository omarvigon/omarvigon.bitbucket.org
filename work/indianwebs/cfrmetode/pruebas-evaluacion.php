<?php include("0cabecera.php");?>

    <div id="cuerpo">
    	<h1>CFR M&Egrave;TODE</h1>
        
        <?php switch($_SESSION["lang"])
		{
			case "esp": ?>
			<h2>Evaluaci&oacute;n de la  evidencia</h2>
            <p>CFR M&eacute;todo os puede ayudar a evaluar la evidencia  disponible sobre vuestro producto y  sobre el de vuestros competidores. <b>Sabed d&oacute;nde est&aacute;is.</b></p>
            <p>Podemos hacerlo a diversos niveles:</p>
            <p><b>Revisiones sistem&aacute;ticas:</b></p>
            <p><ul>
               <li>estad al d&iacute;a: la investigaci&oacute;n m&eacute;dica crece  exponencialmente, la evidencia se acumula en muchos campos, y estar al d&iacute;a es  una tarea dif&iacute;cil. Os podemos ayudar a resumir la  evidencia publicada en vuestra &aacute;rea de inter&eacute;s y ofreceros una evaluaci&oacute;n  informada de las implicaciones principales para vuestra investigaci&oacute;n en curso.</li>
               <li>sabed d&oacute;nde est&aacute;is: evaluar la evidencia os ayudar&aacute; a saber d&oacute;nde se encuentra  vuestro producto, o d&oacute;nde se encuentra la competencia.  &iquest;Est&aacute;is considerando un nuevo mercado para  vuestro producto? Una revisi&oacute;n sistem&aacute;tica os puede ayudar a definirlo.</li>
               </ul>
            </p>
            <p><b>Metan&aacute;lisis:</b> &iquest;Hab&eacute;is considerado el impacto de una metan&aacute;lisis? Considerad las  posibilidades: una metan&aacute;lisis aporta la m&aacute;s alta evidencia cient&iacute;fica y ofrece  la mayor validez externa. Transmitid vuestro mensaje con una de las mejores  herramientas al alcance de la investigaci&oacute;n  biom&eacute;dica. Pensad:</p>
            <p><ul>
               <li>las metan&aacute;lisis ostentan merecidamente una muy buena consideraci&oacute;n entre los  m&eacute;dicos y son f&aacute;cilmente comprensibles por todos  los profesionales de la salud, dejando un impacto a largo plazo.</li>
               <li>presentar una metan&aacute;lisis a las Autoridades Reguladoras os puede ayudar a  delimitar a favor vuestro los estudios necesarios para posicionar firmemente  vuestro producto en el mercado.</li>
               </ul>
            </p>
			<? break;
			
			case "cat": ?>
            <h2>Avaluaci&oacute; de l&rsquo;evid&egrave;ncia</h2>
            <p>CFR M&egrave;tode us pot ajudar a avaluar l&rsquo;evid&egrave;ncia  disponible, tant del vostre producte com dels vostres competidors. <b>Sapigueu on  sou.</b></p>
            <p>Podem fer-ho a diversos nivells: </p>
            <p><b>Revisions sistem&agrave;tiques:</b></p>
            <p><ul>
               <li>estigueu al dia: la recerca m&egrave;dica creix  exponencialment, l&rsquo;evid&egrave;ncia s&rsquo;acumula en molts camps, i estar al dia esdev&eacute;  una tasca dif&iacute;cil. Us opdem ajudar a resumir l&rsquo;evid&egrave;ncia publicada en la vostra  &agrave;rea d&rsquo;inter&egrave;s i oferir-vos una avaluaci&oacute; informada de les implicacions  principals per la vostra recerca en curs.</li>
               <li>sapigueu on sou: avaluar l&rsquo;evid&egrave;ncia us  ajudar&agrave; a saber on es troba el vostre producte, o on es troba la compet&egrave;ncia. Esteu  considerant un nou mercat pel vostre producte? Una revisi&oacute; sistem&agrave;tica us pot  ajudar a definir-lo.</li>
               </ul>
            </p>   
            <p><b>Metan&agrave;lisis:</b> Heu considerat l&rsquo;impacte d&rsquo;una  metan&agrave;lisi? Considereu les possibilitats: una metan&agrave;lisi aporta la m&eacute;s alta  evid&egrave;ncia cient&iacute;fica i ofereix la m&eacute;s gran validesa externa. Transmeteu el  vostre missatge amb una de les millors eines a l&rsquo;abast de la recerca biom&egrave;dica.  Penseu-hi:</p>
            <p><ul>
               <li>les  metan&agrave;lisis ostenten merescudament una molt bona consideraci&oacute; entre els metges  i s&oacute;n f&agrave;cilment comprensibles per tots els professionals de la salut, deixant  un impacte a llarg termini.</li>
               <li>presentant  una metan&agrave;lisi a les Autoritats Reguladores us pot ajudar a delimitar a favor  vostre els estudis necessaris per a posicionar fermament el vostre producte al  mercat.</li>
               </ul>
            </p>
			<? break;
			
			case "ing":
			default: ?>
            <h2>Evidence assessment</h2>
            <p>CFR M&egrave;tode can  help you assessing the available evidence, both of your product or the  competitor&rsquo;s. <b>Know were you stand</b>.</p>
            <p>We can do this  at various levels:</p>
            <p><b>Systematic reviews: </b></p>
            <p>
                <ul>
                <li><b>Keep up to date</b>: medical research is  exponentially growing; evidence accumulates in many fields and keeping up to  date becomes difficult. We can help you summarize the evidence published in  your area of interest and provide an informed evaluation of the main  implications for your current investigations.</li>
                <li><b>Know were you stand</b>: Assessing the evidence will  help you know where your product stands, or where the competitors stand.<br>Are you considering a new niche  for your product? A systematic review can help sorting it out. </li>
                </ul>
            </p>
            <p><b>Meta-analyses : </b> Have you  considered the impact of a meta-analysis? Envisage the possibilities: a  meta-analysis provides the highest scientific evidence and is able to offer the  best external validity available. Transmit your message with one of the best  tools available for biomedical research. Consider it:</p>
            <p>
                <ul>
                <li>Meta-analyses deservedly enjoy  a high consideration among doctors and are readily understandable by all health  professionals, <b>leaving a lasting impact</b>. </li>
                <li>Presenting your successful  meta-analysis to the Regulatory Agencies can help you outline to your advantage  the required studies to firmly position your product in the market.</li>
                <li>Metanalayses are a  scientifically valuable way of further generating publications that can help  you in your career development.</li>
                <li>Provide high quality  information of your product to your customers: empower your customers with the  best evidence on your products. A metanalysis both summarizes all the available  information on your product and may offer valuable new perspectives.&nbsp; </li>
                </ul>
            </p> 
			<? break;
		} ?>
      
        <p>&nbsp;</p>
        
<?php include("0piecera.php");?>