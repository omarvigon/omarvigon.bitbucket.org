<?php include("0cabecera.php");?>

    <div id="cuerpo">
    	<h1>CFR M&Egrave;TODE</h1>
        <?php switch($_SESSION["lang"])
		{
			case "esp": ?>
            <h2>Clientes</h2>
            <p>Nuestros clientes incluyen relevantes empresas cient&iacute;ficas, p&uacute;blicas y privadas, presentes en los planos nacional e internacional en el &aacute;mbito de la investigaci&oacute;n m&eacute;dica. </p>
			<? break;
			
			case "cat": ?>
            <h2>Clients</h2>
            <p>Els nostres clients inclouen rellevants empreses cient&iacute;fiques, p&uacute;bliques i privades, presents en els pl&agrave;nols nacional i internacional en l'&agrave;mbit de la investigaci&oacute; m&egrave;dica. </p>
			<? break;
			
			case "ing":
			default: ?>
            <h2>Clients</h2>
        	<p>Our clients include public and private scientifically  respected and relevant actors in the national and international field of  medical research.</p>
			<? break;
		} ?>
    	
        <p>
        <img src="img/cliente_uriach.gif" alt="Grupo Uriach" title="Grupo Uriach" align="absmiddle">
        <img src="img/cliente_tfs.gif" alt="TFS" title="TFS" align="absmiddle" hspace="12">
        </p>
        <p>
        <img src="img/cliente_vallhebron.gif" alt="Vall d'Hebron Hospital" title="Vall d'Hebron Hospital" align="absmiddle">
        <img src="img/cliente_eapoblesec.gif" alt="eap Poble-sec" title="eap Poble-sec" align="absmiddle" hspace="12">
        </p>
        
<?php include("0piecera.php");?>
