<?php include("cabecera.php") ?>
	
	<?php 
	switch($_GET['seccion'])
	{
		case "arealaboral":
		echo "<td class='columna1'><img src='img/yuste_laboral.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Servicios</h1>";
			  
		echo "<a href='servicios-asesoria.php?seccion=areafiscal'><h2>&Aacute;rea fiscal i comptable</h2></a>
		<a href='servicios-asesoria.php?seccion=areamercantil'><h2>&Aacute;rea mercantil</h2></a>
		<h2>&Aacute;rea laboral</h2>
		En  el &aacute;rea laboral,&nbsp; les ofrecemos&nbsp; asesoramiento permanente, aportando  soluciones a todas las situaciones y necesidades de car&aacute;cter laboral que surgen  en el d&iacute;a a d&iacute;a de su actividad empresarial.
		<ul>
		  <li>Asesoramiento  continuado o espec&iacute;fico en Derecho Laboral y contrataci&oacute;n de personal. </li>
		  <li>Asesoramiento y negociaci&oacute;n en modificaciones de condiciones de trabajo, movilidad geogr&aacute;fica y funcional. </li>
		  <li>Asesoramiento, planificaci&oacute;n y ejecuci&oacute;n de expedientes de regulaci&oacute;n de empleo y enconflictos laborales. </li>
		  <li>Asesoramiento y dise&ntilde;o de contratos de alta direcci&oacute;n. </li>
		  <li>Asistencia a inspecciones, recursos y reclamaciones administrativas en material laboral. </li>
		  <li>Auditoria laboral. An&aacute;lisis de los procedimientos y pol&iacute;ticas laborales aplicadas por la empresa, evaluaci&oacute;n de posibles pasivos y definici&oacute;n de sistemas de control. </li>
		  <li>Asesoramiento en prevenci&oacute;n de riesgos laborales. </li>
		  <li>Seminarios espec&iacute;ficos en temas de Derecho Laboral </li>
		  <li>Asesoramiento  y negociaci&oacute;n en contrataci&oacute;n de trabajadores. </li>
		  <li>Confecci&oacute;n de hojas de salario, finiquitos y liquidaciones. </li>
		  <li>C&aacute;lculo y estudio de pensiones de jubilaci&oacute;n, invalidez y viudedad. </li>
		  <li>Estudio de costes laborales. </li>
		</ul>";
		break;
		
		case "arealaboralcat":
		echo "<td class='columna1'><img src='img/yuste_laboral.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Serveis</h1>";
			  
		echo "<a href='servicios-asesoria.php?seccion=areafiscalcat&lang=cat'><h2>&Agrave;rea fiscal i comptable</h2></a>
		<a href='servicios-asesoria.php?seccion=areamercantilcat&lang=cat'><h2>&Agrave;rea mercantil</h2></a>
		<h2>&Agrave;rea laboral</h2>
		En l'&agrave;rea laboral, els oferim assessorament permanent, aportant  solucions  a totes les situacions i necessitats de car&agrave;cter  laboral que sorgeixen en el dia a dia  de la seva activitat empresarial.
		<ul>
		  <li>Assessorament continuat o  espec&iacute;fic en Dret Laboral i contractaci&oacute; de  personal.</li>
		  <li>Assessorament i negociaci&oacute; en  modificacions de  condicions de treball, mobilitat geogr&agrave;fica i  funcional.</li>
		  <li>Assessorament, planificaci&oacute; i execuci&oacute;  d'expedients de  regulaci&oacute; d'ocupaci&oacute; i en  conflictes laborals.</li>
		  <li>Assessorament i disseny de  contractes d'alta direcci&oacute;.</li>
		  <li>Assist&egrave;ncia a inspeccions, recursos i  reclamacions administratives en material laboral.</li>
		  <li>Auditoria laboral. An&agrave;lisi dels procediments i pol&iacute;tiques laborals aplicades per l'empresa, avaluaci&oacute; de possibles passius i  definici&oacute; de sistemes de control.</li>
		  <li>Assessorament en prevenci&oacute; de riscos laborals.</li>
		  <li>Seminaris espec&iacute;fics en temes de Dret Laboral.</li>
		  <li>Assessorament i negociaci&oacute; en contractaci&oacute; de  treballadors.</li>
		  <li>Confecci&oacute; de fulles de salari, quitances i liquidacions.</li>
		  <li>C&agrave;lcul i estudi de pensions de jubilaci&oacute;, invalidesa i vidu&iuml;tat.</li>
		  <li>Estudi de costos laborals.</li>
		</ul>";
		break;
		
		case "areafiscal":
		echo "<td class='columna1'><img src='img/yuste_contable.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Servicios</h1>";
			  
		echo "<a href='servicios-asesoria.php?seccion=arealaboral'><h2>&Aacute;rea laboral</h2></a>
		<a href='servicios-asesoria.php?seccion=areamercantil'><h2>&Aacute;rea mercantil</h2></a>
		<h2>&Aacute;rea fiscal y contable</h2>
		Nuestros  profesionales se encargan de asesorar, planificar, dirigir y desarrollar  completamente todos los aspectos relacionados con la contabilidad y la  fiscalidad de su empresa.
		<ul>
		  <li>Elaboraci&oacute;n y asesoramiento de contabilidades. </li>
		  <li>Legalizaci&oacute;n de Libros Contables. </li>
		  <li>Cierre contable y fiscal de sociedades</li>
		  <li>Auditoria  fiscal </li>
		  <li>Grandes  patrimonios y empresa familiar </li>
		  <li>Impuesto  sobre el valor a&ntilde;adido </li>
		  <li>Impuesto  sobre la renta de las Personas F&iacute;sicas </li>
		  <li>Renta  no residentes </li>
		  <li>Procedimiento  tributario </li>
		  <li>Tributaci&oacute;n  de Comunidades Aut&oacute;nomas y Ayuntamientos </li>
		</ul>";
		break;
		
		case "areafiscalcat":
		echo "<td class='columna1'><img src='img/yuste_contable.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Serveis</h1>";
			  
		echo "<a href='servicios-asesoria.php?seccion=arealaboralcat&lang=cat'><h2>&Agrave;rea laboral</h2></a>
		<a href='servicios-asesoria.php?seccion=areamercantilcat&lang=cat'><h2>&Agrave;rea mercantil</h2></a>
		<h2>&Agrave;rea fiscal i comptable</h2>
		Els nostres professionals  s'encarreguen d'assessorar, planificar, dirigir i desenvolupar completament tots els aspectes relacionats amb la comptabilitat i la fiscalitat de la seva empresa.
		<ul>
		  <li>Elaboraci&oacute; i  assessorament de  comptabilitats.</li>
		  <li>Legalitzaci&oacute; de Llibres Comptables.</li>
		  <li>Tancament comptable i fiscal  de societats.</li>
		  <li>Auditoria fiscal.</li>
		  <li>Grans patrimonis i  empresa familiar.</li>
		  <li>Impost sobre el  valor afegit.</li>
		  <li>Impost sobre la  renda de les Persones F&iacute;siques.</li>
		  <li>Renda no residents.</li>
		  <li>Procediment tributari.</li>
		  <li>Tributaci&oacute; de Comunitats Aut&ograve;nomes i Ajuntaments.</li>
		</ul>";
		break;
		
		case "areamercantil":
		echo "<td class='columna1'><img src='img/yuste_mercantil.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Servicios</h1>";
			  
		echo "<a href='servicios-asesoria.php?seccion=arealaboral'><h2>&Aacute;rea laboral</h2></a>
		<a href='servicios-asesoria.php?seccion=areafiscal'><h2>&Aacute;rea fiscal y contable</h2></a>
		<h2>&Aacute;rea mercantil</h2>
		En el &Aacute;rea de derecho Mercantil nos  encargamos de asesorar a la empresa sobre cualquier asunto relacionado con su  actividad.
		<ul>
		  <li>Asesoramiento  continuado o espec&iacute;fico sobre cualquier aspecto relativo al Derecho Mercantil. </li>
		  <li>Constituci&oacute;n  de sociedades u otro tipo de entidades, reestructuraciones societarias,  transformaciones, fusiones, escisiones y disoluciones. </li>
		  <li>Compra  y venta de sociedades, planificaci&oacute;n, &quot;due diligence&quot;, negociaci&oacute;n, y  preparaci&oacute;n de los documentos necesarios que intervienen en cada operaci&oacute;n. </li>
		  <li>Asesoramiento  en todo tipo de acuerdos comerciales: contratos de venta, distribuci&oacute;n,  agencia, franquicia, etc. </li>
		  <li>Empresa  familiar: Determinaci&oacute;n de los &oacute;rganos de gobierno y establecimiento de  instrumentos de control. Protocolo familiar legal. </li>
		  <li>Confecci&oacute;n  y dep&oacute;sito de Cuentas Anuales, Memoria, Informe de gesti&oacute;n, Declaraci&oacute;n  negativa de acciones propias y declaraci&oacute;n medioambiental. </li>
		  <li>Liquidaci&oacute;n  del Impuesto sobre Transmisiones. Patrimoniales y Actos Jur&iacute;dicos Documentados. </li>
		  <li>Inscripci&oacute;n  de documentos en los Registros Mercantiles y/o de la Propiedad correspondientes. </li>
		</ul>";
		break;
		
		case "areamercantilcat":
		echo "<td class='columna1'><img src='img/yuste_mercantil.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Serveis</h1>";
			  
		echo "<a href='servicios-asesoria.php?seccion=arealaboralcat&lang=cat'><h2>&Agrave;rea laboral</h2></a>
		<a href='servicios-asesoria.php?seccion=areafiscalcat&lang=cat'><h2>&Agrave;rea fiscal i comptable</h2></a>
		<h2>&Agrave;rea mercantil</h2>
		En l'&Agrave;rea de dret  Mercantil ens encarreguem d'assessorar a l'empresa sobre qualsevol assumpte relacionat amb la seva activitat.
		<ul>
			<li>Assessorament continuat o  espec&iacute;fic sobre  qualsevol aspecte relatiu al Dret Mercantil.</li>
			<li>Constituci&oacute; de societats o altre  tipus d'entitats,  reestructuracions societ&agrave;ries, transformacions, fusions, escissions i dissolucions.</li>
			<li>Compra i venda de societats, planificaci&oacute;, &quot;due diligence&quot;, negociaci&oacute;, i  preparaci&oacute; dels documents necessaris que intervenen en cada operaci&oacute;.</li>
			<li>Assessorament en tot tipus d'acords comercials: contractes de venda,  distribuci&oacute;, ag&egrave;ncia, franqu&iacute;cia, etc.</li>
			<li>Empresa familiar: Determinaci&oacute; dels &ograve;rgans de govern i establiment d'instruments de  control. Protocol familiar  legal.</li>
			<li>Confecci&oacute; i dip&ograve;sit de Comptes  Anuals, Mem&ograve;ria, Informe de gesti&oacute;, Declaraci&oacute; negativa d'accions pr&ograve;pies i  declaraci&oacute; mediambiental.</li>
			<li>Liquidaci&oacute; de l'Impost sobre  Transmissions.  Patrimonials i Actes  Jur&iacute;dics Documentats.</li>
			<li>Inscripci&oacute; de documents en els Registres  Mercantils i/o de la  Propietat corresponents.</li>
		</ul>";
		break;
		
		default:
		echo "<td class='columna1'><img src='img/yuste3.jpg' alt='Yuste Assesors' vspace='1'></td>
			  <td class='columna2'><h1>Servicios</h1>";
		if($_GET['lang']=="cat")
		{
			echo "
			<a href='servicios-asesoria.php?seccion=arealaboralcat&lang=cat'><h2>&Agrave;rea laboral</h2></a>
			<a href='servicios-asesoria.php?seccion=areafiscalcat&lang=cat'><h2>&Agrave;rea fiscal i comptable</h2></a>
			<a href='servicios-asesoria.php?seccion=areamercantilcat&lang=cat'><h2>&Agrave;rea mercantil</h2></a>";
		}
		else
		{
			echo "
			<a href='servicios-asesoria.php?seccion=arealaboral'><h2>&Aacute;rea laboral</h2></a>
			<a href='servicios-asesoria.php?seccion=areafiscal'><h2>&Aacute;rea fiscal y contable</h2></a>
			<a href='servicios-asesoria.php?seccion=areamercantil'><h2>&Aacute;rea mercantil</h2></a>";
		}
		echo "<br><br><br><br><br><br><br><br><br><br>";
		break;
	}
	?>

<?php include("piecera.php") ?>