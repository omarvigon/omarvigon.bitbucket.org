<html>
<head>
	<meta name="description" content="Yuste Assesors (Barcelona) - Asesoramiento integral a empresas. Servicios en el area laboral, fiscal, contable y mercantil.">
	<meta name="keywords" content="Yuste Assesors, assessors barcelona, asesoria barcelona, asesores barcelona, asesoria laboral, asesoria financiera">
	<meta name="verify-v1" content="FTKtylUfKXKrJy+1TDejEDEAOSaWlOUHwXEyYLRJiPg=" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.9)">
	<title>Asesoramiento integral a empresas en Barcelona. Servicios en el area laboral, fiscal, contable y mercantil.</title>
	<link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="shortcut icon" href="favicon.ico">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>
<body onLoad="iniciar()">

<table cellpadding="0" cellspacing="0" class="central" align="center">
<tr>
	<td class="top0">&nbsp;</td>
	<td class="top1" colspan="2">
		<?
		if($_GET['lang']=="cat")
		{
			echo '
			<a class="bt1" href="index.php?lang=cat">presentaci&oacute;</a>
			<a class="bt5" href="servicios-asesoria.php?lang=cat">serveis</a>
			<a class="bt6" href="situacion.php?lang=cat">situaci&oacute;</a>
			<a class="bt7" href="http://www.yusteassessors.es/noticias/">not&iacute;cies</a>';
		}
		else
		{
			echo '
			<a class="bt1" href="index.php">presentaci&oacute;n</a>
			<a class="bt2" href="servicios-asesoria.php">servicios</a>
			<a class="bt3" href="situacion.php">situaci&oacute;n</a>
			<a class="bt4" href="http://www.yusteassessors.es/noticias/">noticias</a>';
		}
		?>
		<a href="<?php $_SERVER['PHP_SELF'] ?>?lang=esp"><img src="img/icon_espana.gif" border="0" style="margin:0 3 0 60;"></a>
		<a href="<?php $_SERVER['PHP_SELF'] ?>?lang=cat"><img src="img/icon_cataluna.gif" border="0"></a>
	</td>
	<td class="top2">&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
