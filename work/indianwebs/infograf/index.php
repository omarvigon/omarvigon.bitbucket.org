<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)">
    <meta name="revisit" content="7 days">
    <meta name="robots" content="index,follow,all">
    <meta name="lang" content="es,sp,spanish,español,espanol,castellano">
    <meta name="verify-v1" content="qFljCqyDUEkIHa/zJEL5uxqOmmKBsUb41WbhB8kuoeY=">
	<meta name="keywords" content="Aston Idiomas en el mundo">
	<meta name="description" content="Aston Idiomas">
	<title>Aston Idiomas - Idiomas en el mundo</title>
	<link rel="stylesheet" type="text/css" href="estilos_nueva.css">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>

<body>

<div id="central">
	<div class="idiomas">
    	<a href="http://www.astonidiomas.com/index.php">castellano | </a>
        <a href="http://www.astonidiomas.com/index_cat.php">catal&agrave | </a>
        <a href="http://www.astonidiomas.com/index_eng.php">english</a>
    </div>
    
    <div class="logo">
    	<h1>CURSOS EN ESPA&Ntilde;A Y EN EL EXTRANJERO</h1>
    </div>
    
   	<div class="columna1">
    	<img src="imgs/inicio1.jpg">
        <p><b>JUNIORS</b><br>(8- 17 a&ntilde;os)<br>
            <a href="http://www.astonidiomas.com/2cursos.php?modo=25"><em>camps</em></a><br>
            <a href="http://www.astonidiomas.com/2cursos.php?modo=26"><em>extranjero</em></a><br>
            <a href="http://www.astonidiomas.com/3clases.php"><em>clases particulares</em></a><br>
            <a href="http://www.astonidiomas.com/2cursos.php?modo=27"><em>a&ntilde;o acad&eacute;mico</em></a>
        </p>
    </div>
    <div class="columna2">
  		<img src="imgs/inicio2.jpg">
        <p><a href="http://www.astonidiomas.com/2cursos.php?modo=28"><b>UNIVERSITARIOS</b><br>(+17 a&ntilde;os)</a></p>
    </div>
    <div class="columna3">
   		<img src="imgs/inicio3.jpg">
        <p><b>PROFESIONALES</b><br>
        	<a href="http://www.astonidiomas.com/3clases.php"><em>formacion in-company</em></a><br>
        	<a href="http://www.astonidiomas.com/2cursos.php?modo=29"><em>extranjero</em></a>
        </p>
    </div>
    
    <div class="logo_puting">
    	<img src="imgs/aston_putting.gif">
    </div>
    
    <div class="botones1">
    	<a href="http://www.astonidiomas.com/index0.php?aux=login" class="btgris">ZONA DE CLIENTES</a>
        <a href="http://www.astonidiomas.com/index0.php?aux=noticias" class="btgris">NEWS</a>
        <a href="http://www.astonidiomas.com/becas.html" class="btgris" target="_blank">BECAS MEC</a>
        <a href="http://www.astonidiomas.com/index1.php" class="btgris2">&nbsp;</a>
        <a href="http://www.avantespain.com/" class="btgris3" target="_blank">&nbsp;</a>
    </div>
    
    <div class="botones2">
    	<a href="http://www.astonidiomas.com/index0.php?aux=equipo">nuestro equipo | </a>
		<a href="http://www.astonidiomas.com/index0.php?aux=delegacion">delegaciones | </a>
		<a href="http://www.astonidiomas.com/index0.php?aux=contacta">contacta con nosotros | </a>
		<a href="http://www.astonidiomas.com/index0.php?aux=trabaja">trabaja con ASTON | </a>
		<a href="http://www.astonidiomas.com/index0.php?aux=catalogo">solicitud de cat&aacute;logo | </a>
		<a href="http://www.astonidiomas.com/index0.php?aux=inscripcion">hoja de inscripci&oacute;n | </a>
		<a href="http://www.astonidiomas.com/index0.php?aux=enlaces">enlaces de inter&eacute;s</a>
    </div>
    
    <div class="pie">
   	 	Tel. (+34) 902 882 103 - Fax (+34) 93 280 28 84 | Pg. Manuel Girona, 82 - 08034 Barcelona<br>
    	<span>Copyright &copy; astonidiomas.com 2007 | Aviso Legal - <a href="files/politica_privacidad.pdf" target="_blank"> Pol&iacute;tica de Privacidad</a></span>
    </div>
</div>

</body>
</html>
