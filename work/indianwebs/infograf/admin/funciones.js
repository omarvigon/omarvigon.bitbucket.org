function ini()
{
	opacidad("logo");
	var ruta=document.location.pathname;
	var temp;
	
	for(var i=0;i<=ruta.length;i++)
	{
		if(ruta.charAt(i)=="/")
			temp=i;
	}
	ruta=ruta.substr(temp+1,ruta.length);
	fecha();
	
	if(ruta=="index_eng.php")
		fecha_eng();
	if(ruta=="index_cat.php")
		fecha_cat();
	/*else if(ruta=="1traduccion.php")
		opacidad("imagen1");
	else if(ruta=="3clases.php")
		opacidad("imagen3");*/
}
function fecha()
{	
	var dia=new Date();
	var mes;
	switch(dia.getMonth()+1)
	{
		case 1:		mes="enero";		break;
		case 2:		mes="febrero";		break;
		case 3:		mes="marzo";		break;
		case 4:		mes="abril";		break;
		case 5:		mes="mayo";			break;
		case 6:		mes="junio";		break;
		case 7:		mes="julio";		break;
		case 8:		mes="agosto";		break;
		case 9:		mes="septiembre";	break;
		case 10:	mes="octubre";		break;
		case 11:	mes="noviembre";	break;
		case 12:	mes="diciembre";	break;
		default:	break;
	}
	document.getElementById('fecha').innerHTML=dia.getDate()+" de "+mes+" de "+dia.getFullYear();
}
function fecha_eng()
{	
	var dia=new Date();
	var mes;
	switch(dia.getMonth()+1)
	{
		case 1:		mes="january";		break;
		case 2:		mes="february";		break;
		case 3:		mes="march";		break;
		case 4:		mes="april";		break;
		case 5:		mes="may";			break;
		case 6:		mes="june";			break;
		case 7:		mes="july";			break;
		case 8:		mes="august";		break;
		case 9:		mes="september";	break;
		case 10:	mes="october";		break;
		case 11:	mes="november";		break;
		case 12:	mes="december ";	break;
		default:	break;
	}
	document.getElementById('fecha').innerHTML=dia.getDate()+" "+mes+" "+dia.getFullYear();
}
function fecha_cat()
{	
	var dia=new Date();
	var mes;
	switch(dia.getMonth()+1)
	{
		case 1:		mes="gener";		break;
		case 2:		mes="febrer";		break;
		case 3:		mes="mar&ccedil;";	break;
		case 4:		mes="abril";		break;
		case 5:		mes="maig";			break;
		case 6:		mes="juny";			break;
		case 7:		mes="julio";		break;
		case 8:		mes="agost";		break;
		case 9:		mes="setembre";		break;
		case 10:	mes="octubre";		break;
		case 11:	mes="novembre";		break;
		case 12:	mes="desembre";		break;
		default:	break;
	}
	document.getElementById('fecha').innerHTML=dia.getDate()+" de "+mes+" del "+dia.getFullYear();
}

var inicio=0;
function opacidad(nombre)
{
	if(inicio<99) 			
   	{
		inicio=inicio+1;
       	if(inicio<10)
			document.all[nombre].style.opacity=".0"+inicio;
		else
			document.all[nombre].style.opacity="."+inicio;
		document.all[nombre].style.filter="alpha(opacity="+inicio+")";
        setTimeout("opacidad('"+nombre+"')",25);
   }
}
function submenus(num,menu)
{
	if(num==1)
		document.getElementById(menu).style.visibility="visible";
	else if(num==2)
		document.getElementById(menu).style.visibility="hidden";
}
function submenu_ant(num,nombre,menu)
{
	/*if(num==1)
	{
		document.getElementById("sb0").style.background="#0033FF";
		document.getElementById("sb0").style.color="#FFFFFF";
	}
	else if(num==2)
	{
		document.getElementById("sb0").style.color="#0033FF";
		document.getElementById("sb0").style.background="#FFFFFF";
	}*/
}
function submenu2(num,pos)
{
	submenus(num,"submenu2");
	document.getElementById("submenu2").style.top=pos;
}
function submenu_final(num)
{
	submenus(num,"submenu1");
	submenus(num,"submenu2");
}