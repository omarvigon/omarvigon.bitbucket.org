<?php include "c_cabecera2.php";?>

	<p><a href="1traduccion_cat.php"><b>Motius per escollir aston TRADUCCIONS </b></a></p>
	<span class="azul1"><b>Traducci� i correcci� de textos</b></span>
	   <div class="subtext2"><a href="1traduccion_cat.php?aux2=metodo">M�tode de treball</a></div>
	   <div class="subtext2"><a href="1traduccion_cat.php?aux2=correccion">Correcci&oacute; ortogr&aacute;fica, gramatical, sint&aacute;ctica i d'estil</a></div>
	   <div class="subtext2"><a href="1traduccion_cat.php?aux2=areas">�rees de treball i clients</a></div>
	<p><a href="1traduccion_cat.php?aux2=enviarnos"><b>Com ens podeu enviar un text</b></a></p>
	<span class="azul1"><b>Interpretaci&oacute;</b></span>
		<div class="subtext2"><a href="1traduccion_cat.php?aux2=contextos">Contextos</a></div>
	    <div class="subtext2"><a href="1traduccion_cat.php?aux2=modalidad">Modalitats</a></div>
	    <div class="subtext2"><a href="1traduccion_cat.php?aux2=especial">�rees d�especialitat</a></div>
	<p><a href="1traduccion_cat.php?aux2=presupuesto"><b>Sol.licitud de pressupost</b></a></p>
	<p><a href="1traduccion_cat.php?aux2=pedidos"><b>Formulari de Comanda</b></a></p>
	<p><a href="1traduccion_cat.php?aux2=traductores"><b>�rea per a traductors</b></a></p>
	<div class="cont_abajo1">
	<br><br><br><br>
		<a href="http://www.act.es" target="_blank"><img src="imgs/logo_act.jpg" alt="ACT" border="0" hspace="4"></a>
		<a href="http://www.euatc.org" target="_blank"><img src="imgs/logo_euatc.jpg" alt="EUATC" border="0" hspace="4"></a>
	</div>
</div>

<div id="c_traduccion">
	 <div class="correo">
	 <a href="mailto:aston@astontraducciones.com" style="text-decoration:none;color:#FFFFFF;">aston@astontraducciones.com</a>
	 </div>
	
	<?php 
	switch($aux2)
	{
		case "metodo":
		echo "<div id='ttraduccion'>M�tode de treball  </div>";
		echo "<div class='textblanco3'><br>";
		echo "<p>El nostre objectiu �s la QUALITAT FINAL del producte i creiem que, per garantir-la, �s imprescindible comptar amb un equip format per professionals qualificats que apliquin un estricte m�tode de treball.
		<p><b>ASSIGNACI�</b><li>La persona responsable de gestionar les traduccions assigna el text al traductor m�s indicat en cada cas, segons l�idioma de destinaci� (sempre ser� un traductor NATIU) i l�especialitat. En aquest punt �s important la VALORACI� DE L�ORIGINAL, ja que l��mbit i el registre d�un text permetran establir la terminologia i l�estil i garantir la m�xima adequaci� del resultat final respecte de l�original.
		<p><b>TRADUCCI�</b><li>�s la fase en qu� es tradueix el text, estrictament parlant, durant la qual el traductor se serveix dels seus propis coneixements i de tots els recursos i les eines que la TECNOLOGIA ACTUAL posa a la seva disposici�. A aston TRADUCCIONS tenim acc�s a diccionaris electr�nics, bases de dades i centres de terminologia i actualitzem de manera constant la normativa ling��stica que apliquem, els nostres programes de traducci� assistida i les mem�ries associades.
		<p><b>CONTROL</b><li>En aquesta fase, el text que ja s�ha tradu�t se sotmet a una estricta revisi� per garantir que est� complet i que les xifres, els noms i la resta de dades que hi apareixen s�n correctes.
		<li>Un segon traductor-corrector revisa l�adequaci� terminol�gica i ortogr�fica del text i el sotmet a una valoraci� addicional per determinar en quins aspectes cal millorar-lo. En aquesta fase, �s molt important mantenir la COHER�NCIA en tots els documents d�un mateix client que tinguin una certa periodicitat, ja que la IMATGE D�UNA EMPRESA pot dependre d�un detall com aquest. Els nostres traductors elaboren glossaris i els apliquen i actualitzen de manera constant, per a la qual cosa de vegades cal mantenir un di�leg directe amb els clients.
		<p><b>FORMAT</b><li>El format del text s�adapta a l�original, o fins i tot es millora, per aconseguir una PRESENTACI� impecable. En alguns casos, la imatge d�un document pot arribar a comunicar tantes coses com el seu contingut.
		<p><b>PUNTUALITAT EN ELS LLIURAMENTS</b><li>Finalment, el text tradu�t es lliura al client amb la m�xima puntualitat possible. El m�tode de treball que hem descrit no implica retards innecessaris, ja que sabem perfectament fins on ens podem comprometre.
		<p><b>CONFIDENCIALITAT. </b>Exigim al nostre personal (tant intern com extern) el comprom�s de no divulgar a tercers el contingut dels textos treballats.";
		echo "</div>";
		echo "<img src='imgs/f_metodo_trabajo.jpg'>";
		break;
		
		case "correccion":
		echo "<div id='ttraduccion'>Correcci&oacute; ortogr&aacute;fica, gramatical, sint&aacute;ctica i d'estil</div>";
		echo "<div class='textblanco0'><br>
		L��xit d�un negoci o el prestigi d�un conferenciant poden perillar per un defecte de forma. Per aquest motiu, els nostres professionals tamb� es posen a la vostra disposici� i us corregeixen documents originals escrits en qualsevol dels idiomes d��s m�s habitual en el nostre entorn. 
		<p>Si es tracta de textos d��mplia divulgaci� (cat�legs, fullets publicitaris, etc.) �s recomanable, a m�s a m�s, fer-ne una revisi� de GALERADES: despr�s de la maquetaci�, el text se sotmet a una revisi� addicional per descartar tota mena d�errors o fins i tot per millorar-ne algun detall final. D�aquesta manera s�eviten els costos derivats d�haver d�introduir canvis en un text quan ja Se n�han impr�s grans tirades. 
		<p>En aquest tipus de servei, el contacte amb el client �s molt important, ja que en alguns casos el podem assessorar i ajudar-lo a trobar la millor manera de posar les seves idees sobre el paper.";		
		echo "</div><br><img src='imgs/f_traduccion_mio2.gif' border='0'>";
		break;
		
		case "areas":
		echo "<div id='ttraduccion'>�REES DE TREBALL I CLIENTS</div>";
		echo "<div class='textblanco3'><br>";
		echo "<p><b>Sector jur�dic</b><li>Documentaci� jur�dica de tota mena �contractes, actes, etc.� utilitzada di�riament per les empreses en els seus tr�mits legals. Documents espec�fics de bufets d�advocats, consultors, assessories, bancs i caixes d�estalvis, etc.  <br><br>Clients: advocats (Prat y Roca, Salgado Riba and Pardo, Aequo Animo Advocats, VMV 1899 Letrados Asesores, Vendrell and Associats, Audihispana Abogados y Asesores, Folch Advocats, Larrumbe Serrano-Ferran, Buigas, Roig-Ar�n, Labastida y del R�o, Jaus�s, Conde-Escalza, Castilla, etc.); companyies asseguradores (BCH Vida, Groupama, Zurich); departaments jur�dics d�empreses comercials com ara Chupa-Chups, Torraspapel, Tac Consultants, Cirsa, Media Planning, Laboratorios Miret i IBM, entre d�altres.
<p><b>Sector financer i empresarial</b><li>Mem�ries, comptes anuals, informes, estats financers, fullets d�emissi� d�accions, estudis sobre gesti� empresarial, documentaci� banc�ria de tota mena. <br><br>Clients: col�laborem de manera activa amb diferents entitats banc�ries i caixes d�estalvis (vam ser designats des del setembre del 1997 per la Caixa d�Estalvis i Pensions de Barcelona �la Caixa� per traduir els documents dels seus departaments d�Assessoria jur�dica, Assessoria fiscal, Cr�dits, Auditoria, Banca electr�nica i Banc d�Europa (Factoring, Leasing i Renting). Tamb� treballem per a EADA, Seraudit Auditores i Licencing Consultants, i per a entitats banc�ries com ara Barclays Bank i Privat Bank.
<p><b>Cultura i comunicacions</b><li>Cat�legs d�exposicions d�art i tota mena de documentaci� relacionada. <br><br>Clients: Fundaci� la Caixa, Museu Nacional d�Art de Catalunya, Museu d�Art Contemporani de Barcelona, Museo de Arte Contempor�neo de Castilla y Le�n, Fundaci�n Biacs (Bienal de Arte Contempor�neo de Sevilla), Ediciones El Viso, entre d�altres.
<li>Assaig i divulgaci� de tem�tica sociol�gica. <br><br>Clients: Lidia Falc�n.
<p><b>Sector comercial i publicitari</b><li>Programes de m�rqueting i millora de les vendes. Fullets publicitaris sobre diferents productes. Traducci� di�ria de documents de fax i correspond�ncia habitual entre empreses. Traducci� de p�gines web. Presentacions en format Power Point. <br><br>Clients: Snack Ventures, S.A., Institut Futur, Optimus Medina, Grupo Tetrans, Odisea 2000.
<p><b>Sector de la moda i la confecci�</b><li>Publicacions de divulgaci� de nous productes, documentaci� de tipus t�cnic (teixits, fabricaci�...). <br><br>Clients: Ermenegildo Zegna, Burberry, Evita Peroni.
<p><b>Sector de l�alimentaci�</b><li>Informaci� sobre composici� dels aliments, cartes de restaurants, divulgaci� general sobre temes alimentaris, receptes de cuina. <br><br>Clients: SEB Ib�rica, Meli� Barcelona, Girafa Digital, Prats Fatj�, Semadia Marketing.
<p><b>Documents de tipus t�cnic</b><li>Peritatges d�autom�bil. Patents. Informaci� t�cnica sobre diferents productes del sector de les telecomunicacions. Arquitectura i urbanisme. Enginyeria. Medi ambient. Fitxes de seguretat de productes qu�mics. Instruccions d��s de petits electrodom�stics. Normes de seguretat laboral, salut i medi ambient. Comunicats interns de manteniment de maquin�ria. Informaci�-software (manuals d�usuari). A�llament t�cnic i ac�stic. Log�stica. <br><br>Clients: Carburos Met�licos, Rom�n y Asociados, GTH Iberia, NTE, Germ�n Vilalta, Rockwool, Departament de Pol�tica Territorial i Obres P�bliques (Generalitat de Catalunya), Sal� Internacional de la Log�stica, Catalana de Tractament d�Olis Residuals, J. Isern Patentes y Marcas.
<p><b>Medicina</b><li>Informes sobre salut de la poblaci� i incid�ncia de malalties, publicacions sanit�ries divulgadores, documentaci� de suport sobre dispositius m�dics. <br><br>Clients: Centre de Transfusi� i Banc de Teixits, Servei Catal� de la Salut, Asepeyo, Fundaci� Acad�mica de Ci�ncies M�diques, Escola d�Infermeria Santa Madrona.
<p><b>Sector qu�mic i farmac�utic</b><li>Prospectes i publicitat per a empreses del sector quimicofarmac�utic. <br><br>Clients: Biokit, S.A., Esteve Qu�mica, Instrumentation Laboratory, Laboratorios Fardi, Amgen. 
<p><b>Sector de la cosm�tica</b><li>Documentaci� sobre productes cosm�tics (publicit�ria, comercial, t�cnica o relativa a composici� i ingredients). <br><br>Clients: Cosmobelleza, Bruno Vassari, Valmont.
<p><b>Traduccions jurades</b><li>�	Traducci� de documents de tota mena que s�hagin de presentar davant d�organismes oficials espanyols, en judicis, etc. Aquestes traduccions estan certificades per un traductor jurat, habilitat especialment pel Ministeri d�Interior, i s�adjunten al document original, que conserva la seva validesa legal. Alguns exemples de documents oficials que solen requerir traducci� jurada s�n: partides de naixement, inscripcions en el registre civil, certificats de matrimoni, sent�ncies i resolucions judicials, etc.";
		echo "</div>";
		echo "<img src='imgs/f_area_trabajo.jpg'>";
		break;
		
		case "enviarnos":
		echo "<div id='ttraduccion'>Com ens podeu enviar un text</div><br>";
		echo "<div class='textblanco3'>";
		echo "-Per tel�fon <b>[93 280 28 90 </b>o al <b>902 882 103]</b>, poseu-vos en contacte amb el departament de traduccions i especifiqueu qualsevol detall relacionat amb l�enc�rrec de traducci� o correcci�.
<p>-Per correu electr�nic <b><a href='mailto:aston@astontraducciones.com' style='text-decoration:none;color:#FFFFFF'>[aston@astontraducciones.com]</a></b>, tot indicant la combinaci� d�idiomes o qualsevol altra informaci� que considereu necess�ria, a m�s de la data en qu� us cal disposar del text tradu�t o corregit.
<p>-Per fax <b>[93 280 28 84]</b>, tot indicant a la car�tula les vostres dades, els detalls sobre la traducci� o correcci�, el termini de lliurament, etc.
<p>-Personalment, venint a les nostres oficines <b>[Pg. Manuel Girona, 82]</b>, per lliurar-nos el document que voleu traduir i puntualitzar qualsevol detall que considereu important.
<p>Podeu sol�licitar un pressupost previ per con�ixer l�import final aproximat del servei per mitj� d�aquesta  <b><a href='1traduccion.php?aux2=presupuesto' style='text-decoration:none;color:#FFFFFF;'>SOL.LICITUD DE PRESSUPOST.</a></b> 
<p>Per tal de facilitar els tr�mits als nostres Clients, posem a la vostra disposici� un  <b><a href='1traduccion.php?aux2=pedidos' style='text-decoration:none;color:#FFFFFF;'>FORMULARI DE COMANDA</a></b> est�ndard que us permetr� gestionar la traducci� de la manera m�s senzilla i r�pida.";
		echo "</div>";
		break;
		
		case "contextos":
		echo "<div id='ttraduccion'>Contexts</div><br>";
		echo "<div class='textblanco'>";
		echo "El nostre equip est� integrat per professionals qualificats que pertanyen a l�Associaci� Internacional d�Int�rprets de Confer�ncia (AIIC).
		<p>Oferim serveis d�interpretaci� en els idiomes seg�ents: angl�s, franc�s, alemany, itali�, catal�, castell�, rus, �rab, xin�s, japon�s, holand�s i grec.
		<p>Si els locals del client no estan equipats, podem aportar els elements t�cnics necessaris (cabines fixes, receptors, personal t�cnic) o b� un sistema port�til (sense cabina ni personal t�cnic).
		<p><b>Contextos en qu� pot caldre un servei d�interpretaci�:</b> 
		<br>Reunions de negocis, presentacions, debats, entrevistes, confer�ncies, cursos, presentacions a la r�dio i televisi�, rodes de premsa, comit�s d�empresa, confer�ncies internacionals, exposicions, entre d�altres.";
		echo "</div><br><img src='imgs/int_contextos.gif' border='0'>";
		break;
		
		case "modalidad":
		echo "<div id='ttraduccion'>Modalitats d�interpretaci�:</div><br>";
		echo "<div class='textblanco3'>";
		echo "<img src='imgs/int_enlace.gif' align='left' hspace='6' vspace='4'><p><b>Interpretaci� d�enlla�.</b><br>L�int�rpret acompanya una persona o un grup de persones que s�han de comunicar en un idioma diferent del seu. El context �s informal i les interrupcions del discurs s�n constants. �s la modalitat habitual, per exemple, per a reunions comercials en fires o exposicions.
		<img src='imgs/int_consecutiva.gif' align='left' hspace='6' vspace='20'><p><b>Interpretaci� consecutiva.</b><br>Situacions formals en les quals l�orador parla durant 5 o 10 minuts i l�int�rpret pren notes seguint una determinada t�cnica. A continuaci�, l�int�rpret tradueix el que s�ha dit. S�utilitza, per exemple, en negociacions, presentacions de programes o projectes, rodes de premsa, seminaris, etc.
		<img src='imgs/int_simultanea.gif' align='left' hspace='6' vspace='26'><p><b>Interpretaci� simult�nia.</b><br>L�int�rpret treballa en cabines dotades amb el material t�cnic necessari per traduir de manera simult�nia el discurs de l�orador. La interpretaci� �s m�s �gil i r�pida que la modalitat consecutiva i resulta adequada per a congressos, grans confer�ncies, consells d�administraci�, etc.";
		echo "</div>";
		break;
		
		case "especial":
		echo "<div id='ttraduccion'>�rees d�especialitat</div>";
		echo "<img src='imgs/f_traduccion2.jpg' name='imagen1'>";
		echo "<div class='textblanco2'>";
		echo "<br><b>Algunes de les nostres �rees d�especialitat:</b> 
		<p>Sector jur�dic, financer, comercial, farmac�utic, m�dic (cardiologia, ginecologia, dermatologia, neurologia, odontologia, infermeria, logop�dia, etc.), econ�mic, mitjans de comunicaci�, medi ambient, presentacions t�cniques, inform�tica, comit�s d�empresa, art, tecnologies de la informaci�, ling��stica, arquitectura, literatura, museologia.";
		echo "</div>";
		break;
		
		case "presupuesto":
		echo "<div id='ttraduccion'>Sol.licitud de pressupost</div>";
		echo "<div class='textblanco3'><br>";
		echo "<form name='form_presupuesto' method='post' action='4envios.php?tema=presupuesto' target='_blank'><b>DADES DE QUI EL SOL.LICITA</b>
		<br>Nom i cognoms <input type='text' name='nombre' size='40' class='caja_form'>
		<br>Empresa <input type='text' name='empresa' size='40' class='caja_form'>
		<br>Adre�a <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Tel <input type='text' name='telefono' size='20' class='caja_form'>
		<br>Fax &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fax' size='20' class='caja_form'>
		<br>E-Mail <input type='text' name='email' size='40' class='caja_form'>
		<br>Com ens heu conegut? <input type='text' name='conocido' size='70' class='caja_form'>
		<br><br><b>SERVEI QUE SOL.LICITA <hr size='1'>
		TRADUCCIO</b><br><select name='jurada'><option value='jurada'>Jurada</option><option value='nojurada'>No Jurada</option></select>
		<br>N� de paraules del(s) text(os) original(s) <input type='text' name='palabras' size='20' class='caja_form'>
		<br>Idioma(es) d'origen <input type='text' name='iorigen' size='40' class='caja_form'>
		<br>Idioma(es) d'destino <input type='text' name='idestino' size='40' class='caja_form'>
		<br>Descripci� text(os) <input type='text' name='descripcion' size='40' class='caja_form'>
		<br>Format text(os) de destino deseado <input type='text' name='formato' size='40' class='caja_form'>
		<br>Data de lliurament necess�ria <input type='text' name='entrega' size='40' class='caja_form'>
		<br>Altres <input type='text' name='otros' size='40' class='caja_form'>
		<br><br><b>CORRECCIO</b><br><select name='correccion1'><option value='ortografica'>Ortogr�fica, gramatical, sint�ctica y de estilo</option><option value='revision'>Revisi�n de galeradas</option></select>
		<br>N� de paraules del(s) text(os) original(s) <input type='text' name='palabras2' size='20' class='caja_form'>
		<br>Idioma(es) d'origen <input type='text' name='iorigen2' size='40' class='caja_form'>
		<br>Descripci� text(os) <input type='text' name='descripcion2' size='40' class='caja_form'>
		<br>Data de lliurament necess�ria <input type='text' name='entrega2' size='40' class='caja_form'>
		<br>Altres <input type='text' name='otros2' size='40' class='caja_form'>
		<br><br><b>INTERPRETACIO</b><br>	<select name='jurada2'><option value='jurada'>Jurada</option><option value='nojurada'>No Jurada</option></select>
		<br>Tipus d�interpretaci�: <select name='tipoint'><option valuie='simultanea'>simult�nia</option><option value='consecutiva'>consecutiva</option><option value='enlace'>enlla�</option></select>
		<br>Necessiteu l�equip t�cnic? <select name='equipo'><option value='requiere'>Si</option><option value='norequiere'>No</option></select>
		<br>(especifique: cabina, receptores, micr&oacute;fono, auriculares, etc.)
		<br>Idioma(es) de treball <input type='text' name='itrabajo' size='40' class='caja_form'>
		<br>Jornada(es) de treball  <input type='text' name='jornada' size='45' class='caja_form'>
		<br>Altres <input type='text' name='otros3' size='40' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Enviar'></form>";
		echo "</div>";
		break;
		
		case "pedidos":
		echo "<div id='ttraduccion'>Formulari de Comanda</div>";
		echo "<div class='textblanco3'><br>";
		echo "<form name='form_pedido' method='post' action='4envios.php?tema=pedidos' target='_blank'>
		<br>DATA <input type='text' name='fecha' size='20' class='caja_form'>
		<br>N/Ref: <input type='text' name='nref' size='20' class='caja_form'>
		<br><br><b>DADES CLIENT:</b>
		<br>Empresa <input type='text' name='empresa' size='40' class='caja_form'>
		<br>Adre�a <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Ciutat i codi postal <input type='text' name='ciudad' size='40' class='caja_form'>
		<br>NIF <input type='text' name='nif' size='20' class='caja_form'>
		<br>Persona de contacte <input type='text' name='persona' size='40' class='caja_form'>
		<br>Tel&eacute;fon <input type='text' name='telefono' size='20' class='caja_form'>
		<br>S/Ref: <input type='text' name='sref' size='20' class='caja_form'>
		<br><br><b>SERVEI QUE SE SOL.LICITA:</b>
		<br><input type='checkbox' name='cpresupuesto'>Pressupost (sense comprom�s i sense cap cost addicional)
		<br><input type='checkbox' name='csencilla'>Traducci� SIMPLE a l'idioma  <input type='text' name='tsencilla' size='20' class='caja_form'> 
		<br><input type='checkbox' name='cjurada'>Traducci� JURADA a l'idioma <input type='text' name='tjurada' size='20' class='caja_form'>
		<br><input type='checkbox' name='cinterpretacion'>Interpretaci&oacute; de <input type='radio' name='rinterpretacion'>d�ENLLA�  <input type='radio' name='rinterpretacion'>SIMULT&Aacute;NIA <input type='radio' name='rinterpretacion'> CONSECUTIVA al idioma <input type='text' name='tinterpretacion' size='20' class='caja_form'>
		<br><input type='checkbox' name='cestilo'>Correcci� d�ESTIL de l'idioma  <input type='text' name='testilo' size='20' class='caja_form'>
		<br><input type='checkbox' name='cortografica'>Correcci� ORTOGR�FICA de l'idioma  <input type='text' name='tortografica' size='20' class='caja_form'>
		<br><input type='checkbox' name='cgalerada'>Correcci� de GALERADES de l'idioma <input type='text' name='tgalerada' size='20' class='caja_form'>
		<br><br><b>CONDICIONS DE LLIURAMENT:</b>
		<br><input type='checkbox' name='cnormal'>Termini NORMAL   
		<br><input type='checkbox' name='curgente'>Termini URGENT (termini m�xim <input type='text' name='tmaximo1' size='20' class='caja_form'> a les <input type='text' name='tmaximo2' size='20' class='caja_form'> hores) 
		<br>(subjecte a un rec�rrec del 25% al 50% segons la urg�ncia) 
		<br><input type='checkbox' name='cfax'>Tramesa per  FAX 
		<br><input type='checkbox' name='cmail'>Env�o por E-MAIL 
		<br><input type='checkbox' name='cmensajero'>Tramesa per MENSAJERO
		<p>En el sup�sit que calgui fer la factura en nom d�una altra societat, us preguem que ens n�indiqueu les dades corresponents:</p>
		Empresa <input type='text' name='empresa2' size='50' class='caja_form'>
		<br>Adre�a <input type='text' name='direccion2' size='50' class='caja_form'>
		<br>Ciutat i codi postal <input type='text' name='ciudad2' size='50' class='caja_form'>
		<br>NIF <input type='text' name='nif2' size='20' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Enviar'></form>";
		echo "</div>";
		break;
		
		case "traductores":
		echo "<div id='ttraduccion'>�rees de traductors</div>";
		echo "<div class='textblanco3'><br>";
		echo "Si sou traductor professional amb experi�ncia potser us interessa treballar amb nosaltres en r�gim de <i>free-lance</i>. En aquest cas, podeu emplenar el formulari que trobareu a continuaci�.
		<p>Us agra�m el temps que ens heu dedicat i confiem que podrem comptar amb la vostra col�laboraci� en el futur.
		<form name='form_traductor' method='post' action='4envios.php?tema=traductores' target='_blank'><b>DATOS PERSONALES</b>
		<br>Nom <input type='text' name='nombre' size='40' class='caja_form'>
		<br>Primer cognom  <input type='text' name='apellido1' size='40' class='caja_form'>
		<br>Segon cognom  <input type='text' name='apellido2' size='40' class='caja_form'>
		<br>Data de naixement <input type='text' name='nacimiento' size='40' class='caja_form'>
		<br>NIF <input type='text' name='nif' size='40' class='caja_form'>
		<br>Nacionalitat <input type='text' name='nacion' size='40' class='caja_form'>
		<br>Adre�a <input type='text' name='direccion' size='40' class='caja_form'>
		<br>Codi Postal <input type='text' name='cpostal' size='40' class='caja_form'>
		<br>Ciutat <input type='text' name='ciudad' size='40' class='caja_form'>
		<br>Provincia <input type='text' name='provincia' size='40' class='caja_form'>
		<br>Pais <input type='text' name='pais' size='40' class='caja_form'>
		<br>Correi electr&oacute;nic <input type='text' name='email' size='40' class='caja_form'>
		<br>Tel&eacute;fon <input type='text' name='telefono' size='20' class='caja_form'>
		<br>M&oacute;vil <input type='text' name='telefono2' size='20' class='caja_form'>
		<br>Fax &nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='fax' size='20' class='caja_form'>
		<br>Titulaci&oacute;/formaci&oacute; <input type='text' name='titulacion' size='65' class='caja_form'>
		<br>Combinaci&oacute; d'idiomes (idioma d'origen,idioma d'destino  )<input type='text' name='combinacion' size='65' class='caja_form'>
		<br>Anys de experiencia com a traductor <input type='text' name='experiencia' size='20' class='caja_form'>
		<br>&Aacute;rees de especialitzaci&oacute; en que treballa habitualment <input type='text' name='areas' size='65' class='caja_form'>
		<br>Disponibilitat (indiqueu n� de hores /d&iacute;a, dies de la setmana)<input type='text' name='disponibilidad' size='65' class='caja_form'>
		<br>Eines inform&aacute;tiques de que disposeu<input type='text' name='herramientas' size='65' class='caja_form'>
		<br>Estar�eu disposat a fer una petita prova de traducci� no remunerada?<select name='dispuesto'><option value='si'>Si</option><option value='no'>No</option></select>
		<br>Observacions<br><input type='text' name='observaciones' size='65' class='caja_form'>
		<br><br><input type='submit' class='boton_envio' value='Enviar'></form>";
		echo "</div>";
		break;
		
		default:
		echo "<div id='ttraduccion'>Motius per escollir aston TRADUCCIONS</div>";
		echo "<img src='imgs/f_traduccion_mio.gif' name='imagen1'><div class='especial'></div>";
		echo "<div class='textblanco'>";
		echo "<br><p>- <b>Experi�ncia:</b> som una empresa seriosa amb un s�lid bagatge en el camp de la traducci�, un sector en el qual ens movem des de l�any 1988. Ens avala una �mplia cartera de Clients. 
		<p>- <b>Equip hum�:</b> comptem amb una s�lida xarxa de professionals que nom�s tradueixen cap al seu idioma matern i amb un equip de diversos traductors �in-house�, capacitats per garantir el millor resultat final en el context canviant i cada vegada m�s exigent de la comunicaci� actual.
		<p>- <b>Tracte:</b> el contacte diari amb el client �s fonamental per poder mantenir una bona relaci� personal i professional. Estem oberts a qualsevol suggeriment i ens adaptem a les vostres necessitats perqu� volem millorar dia a dia i, per aix�, �s fonamental comptar amb la vostra opini�. 
		<p>- <b>Puntualitat:</b> complim amb els enc�rrecs en el temps previst. Som honestos i sabem en tot moment fins on ens podem comprometre.
		<p>- <b>Especialitzaci�:</b> tradu�m i corregim textos de qualsevol especialitat
		<p>- <b>Fiabilitat:</b> els nostres serveis es caracteritzen per la seva gran professionalitat.
		<p>- <b>Excel.lent relaci� qualitat / preu:</b> tots els nostres treballs se sotmeten a un exhaustiu control de qualitat i correcci�.
		<p>- <b>Tecnologia:</b> disposem de diccionaris electr�nics, bases de dades, acc�s a centres de terminologia, normatives ling��stiques, programes de traducci� assistida, etc.
		<p>- <b>Confidencialitat i privacitat absolutes</b> tant per part del personal de l�empresa com dels <i>free-lance</i>.
		<p>- <b>Personalitzaci�:</b> sabem donar a cada text el to m�s adequat per a l��s que ha de tenir. Cada enc�rrec �s �nic. Elaborem glossaris terminol�gics individualitzats per a cada client.
		<p>Som membres d�ACT (Agrupaci� de Centres Especialitzats en Traducci�), una associaci� espanyola d��mbit nacional fundada el 1990 que centra la seva activitat a regular el sector i recolzar les empreses associades. L�ACT garanteix que tots els seus membres (un total de cinquanta-cinc empreses actualment) compleixen determinats requisits indispensables per oferir uns serveis de traducci� i interpretaci� de la millor qualitat, amb la garantia del rigor i la professionalitat m�s alts. Aquesta associaci�, alhora, �s una de les fundadores d�EUATC (Uni� d�Associacions d�Empreses de Traducci� Europees), una organitzaci� internacional els objectius de la qual s�n la professionalitat, la promoci� de m�todes de qualitat i la cooperaci� entre empreses del sector.";
		echo "</div>";
		break;
	}
	?>
	</div>
	
</div>
<?php include "c_piecera.php";?>