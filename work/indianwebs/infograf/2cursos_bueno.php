<?php include "c_cabecera.php";
	  switch($modo)
	  {
	  	case 1:
	    echo '<a class="bblue" href="2cursos.php">Atr&aacute;s</a>';
		echo '<a class="bblue" href="#">Camps en Espa&ntilde;a</a>';
		$_SESSION['id_ant']=1;
		$_SESSION['nm_ant']="Camps en Espa&ntilde;a";
		conectar($modo);
		break;
	  	
	  	case 2:
		echo '<a class="bblue" href="2cursos.php">Atr&aacute;s</a>';
		echo '<a class="bblue" href="#">Juniors Extranjero</a>';
		$_SESSION['id_ant']=2;
		$_SESSION['nm_ant']="Juniors Extranjero";
		conectar($modo);
		break;
		
		case 3:
		echo '<a class="bblue" href="2cursos.php">Atr&aacute;s</a>';
		echo '<a class="bblue" href="#">J&oacute;venes Adultos</a>';
		$_SESSION['id_ant']=3;
		$_SESSION['nm_ant']="J&oacute;venes Adultos";
		conectar($modo);
		break;
		
		case 4:
		echo '<a class="bblue" href="2cursos.php">Atr&aacute;s</a>';
		echo '<a class="bblue" href="#">Profesionales y Ejecutivos</a>';
		$_SESSION['id_ant']=4;
		$_SESSION['nm_ant']="Profesionales y Ejecutivos";
		conectar($modo);
		break;
		
		default:
		echo '<a class="bblue" href="2cursos.php">Presentaci&oacute;n</a>';
		echo '<a class="bblue" href="2cursos.php?modo=1">Camps en Espa&ntilde;a</a>';
		echo '<a class="bblue" href="files/novedades_camps.pdf" target="_blank">Novedades Camps en Espa&ntilde;a</a>';
		echo '<a class="bblue" href="2cursos.php?modo=2">Juniors Extranjero</a>';
		echo '<a class="bblue" href="2cursos.php?modo=3">J&oacute;venes Adultos</a>';
		echo '<a class="bblue" href="2cursos.php?modo=4">Profesionales y Ejecutivos</a>';
		echo '<a class="bblue" href="2cursos.php?modo=5">A&ntilde;o Acad&eacute;mico convalidable</a>';
		echo '<a class="bblue" href="2cursos.php?modo=6">Idioma a la carta</a>';
		echo '<div class="cont_abajo2">
			<a href="http://www.aseproce.org" target="_blank"><img src="imgs/logo_aseproce.gif" alt="Aseproce" border="0" hspace="8">
			<a href="http://www.trinitycollege.co.uk/" target="_blank"><img src="imgs/logo_trinity.jpg" alt="Trinity" border="0" hspace="8" vspace="6">
			</a></div>';
		break;
	  }
function conectar($modo)
{
	$conexion=mysql_connect("lldb542.servidoresdns.net","qaz095","45t0n");
	mysql_select_db("qaz095");
	$resultado=mysql_query("select * from subfamilia where id_anterior=".$modo." order by id");
	
	echo "<ul>";
	for($i=0;$i<mysql_num_rows($resultado);$i++)
	{
		$fila=mysql_fetch_row($resultado);
		echo "<li>";
		echo "<a class='bblue' href='2cursoscont.php?nom=".$fila[1]."&idfam=".$fila[0]."'>".$fila[1];
		echo "</a></li>";
	}
	echo "</ul>";
	if(empty($fila[0]))
	{
		echo "<b>Cursos todavia no disponibles</b>";
	}
	mysql_close($conexion);
}
echo '</div>';
echo '<div id="c_cursos">';

switch($modo)
{
	case 1:
	echo '<div id="tcursos">Camps en Espa&ntilde;a</div>';
	echo '<div class="textblanco11"><br>
	<p>Les  presentamos nuestros programas educativos en Espa&ntilde;a, dirigidos y organizados  por ASTON. A lo largo de 20 a&ntilde;os muchas familias nos han confiado a sus hijos,  verano tras verano. Ellos son nuestro mejor aval y garant&iacute;a. Aqu&iacute; encontrar&aacute;n  una experiencia, sin duda, inolvidable y enriquecedora para sus hijos.</p>
	<p><i>A estos locos bajitos a los que por su bien<br />
  	hay que domesticar...</p>
	<p>t&uacute;, enciende el sol<br />
 	t&uacute;, ti&ntilde;e el mar<br />
 	t&uacute;, a dibujar el trigo y la flor<br />
  	t&uacute;, haces de viento, dales movimiento<br />
  	nos hace falta tu risa<br />
 	para cocinar el nuevo d&iacute;a...</p>
	<p align="right">(Fragmentos  canciones Joan Manuel Serrat)</i></p>
	<p>LA  PRIORIDAD: LA SEGURIDAD   Y LA FELICIDAD DE LOS <em>CAMPERS</em><br />
	Creamos  un ambiente sano y seguro en un clima de orden y amistad. Nuestros valores  esenciales son: compa&ntilde;erismo, honestidad, respeto y responsabilidad.&nbsp; Toda la organizaci&oacute;n est&aacute; pensada para que  sus hijos se sientan felices y como en casa, mientras aprenden a ser  responsables, aut&oacute;nomos y a saber convivir y compartir. Asumen peque&ntilde;as tareas  diarias (hacerse la cama, tener su habitaci&oacute;n y la ropa ordenada, ducharse y  lavarse los dientes), practican diversos deportes, y adem&aacute;s, juegan, cantan,  bailan, se disfrazan y se r&iacute;en; porque estamos convencidos que la diversi&oacute;n a  estas edades crea &ldquo;ese saco de buenos recuerdos&rdquo; tan preciado cuando se es  adulto.&nbsp;&nbsp;&nbsp;</p>
	<p>EL  OBJETIVO: EL INGL&Eacute;S</p>
	Nuestro  programa de ingl&eacute;s est&aacute; pensado para motivar el aprendizaje del idioma como  lengua activa: consolidar estructuras gramaticales, ampliar vocabulario y  lanzarse a hablar. Desde &ldquo;hacer los primeros pinitos&rdquo; (<em>beginners</em>) hasta  comunicarse en ingl&eacute;s (<em>first certificate</em>). Tres o cuatro horas de clase  (seg&uacute;n el <em>camp</em>) impartidas por profesores nativos, la mayor&iacute;a de los  cuales ya trabaja con nosotros durante el curso escolar, con un ratio de 1:10,  y de acuerdo con los siguientes niveles y estructura.
	</div>
	<img src="imgs/cursos_camps.gif" border="0">';
	break;
	
	case 2:
	echo '<div id="tcursos">Juniors Extranjero</div>';
	echo '<div class="textblanco33"><br>
	<p>UNA MANERA DE EDUCAR</p>
	<p>Todos  los cursos que ofrecemos desde ASTON  &ndash;a los que acuden estudiantes de muchas y diversas nacionalidades&ndash; tienen  limitada la cuota de hispanohablantes. Son programas intensivos o  semi-intensivos que dan una gran importancia al deporte, la cultura y el ocio  como complemento indispensable en el proceso de aprendizaje de una lengua. Para  optimizar los resultados, a muchos de nuestros programas acuden tambi&eacute;n chicos  ingleses, americanos y canadienses. Todos los cursos de ASTON est&aacute;n reconocidos por el British  Council (Reino Unido), por el Institut Fran&ccedil;ais (Francia), por la ACCET  (EE.UU.) o por el Goethe Institut (Alemania), lo que es garant&iacute;a indiscutible  de calidad docente.</p>
	<p>En  cuanto al A&Ntilde;O ACAD&Eacute;MICO CONVALIDABLE, estamos familiarizados con el sistema  educativo americano, el brit&aacute;nico y el irland&eacute;s; disponemos de plazas en  colegios altamente reconocidos.</p>
	<p>En nuestro d&iacute;a a d&iacute;a,  tenemos muy en cuenta que tratamos con ni&ntilde;os, adolescentes y j&oacute;venes. Es aqu&iacute;  donde nuestra &ldquo;manera de educar&rdquo; alcanza el mayor protagonismo. Los programas  que ofrecemos han sido exhaustivamente estudiados y seleccionados dentro de una  industria saturada de oferta. El equipo directivo de ASTON  ha visitado uno por uno los centros, ha revisado programa a programa y conoce  personalmente a cada director acad&eacute;mico. Adem&aacute;s, est&aacute; a su disposici&oacute;n 24 horas  al d&iacute;a durante la estancia de sus hijos fuera de casa.</p>
	<p>NUESTROS MONITORES</p>Les  presentamos a nuestros monitores, muchos de ellos han sido varios a&ntilde;os alumnos  de ASTON, de manera que conocen de primera  mano las atenciones que nuestros estudiantes requieren y nuestra manera de  trabajar. &iexcl;Nos sentimos orgullosos de estos chicos, a los que nos unen momentos  intensos y muchos &eacute;xitos!
	</div>
	<img src="imgs/cursos_juniors.gif" border="0">';
	break;
	
	case 3:
	echo '<div id="tcursos">J&oacute;venes Adultos</div>';
	echo '<div class="textblanco"><br></div>';
	break;
	
	case 4:
	echo '<div id="tcursos">Profesionales y Ejecutivos</div>';
	echo '<div class="textblanco"><br></div>';
	break;
	
	case 5:
	echo '<div id="tcursos">A&ntilde;o acad&eacute;mico convalidable</div>';
	echo '<div class="textblanco3"><br><b>EN COLEGIOS PRIVADOS, CONCERTADOS O P&Uacute;BLICOS. EN FAMILIA, INTERNADO O RESIDENCIA UNIVERSITARIA</b>
	<p>Un a&ntilde;o en el extranjero es m&aacute;s que aprender un idioma. Es conocer otra cultura y valorar la propia. Es hacer nuevos amigos. Es madurar la personalidad.
	<p>Analizamos los intereses y las necesidades del estudiante para poder recomendarle y ofrecerle el programa o colegio que mejor se adapte a sus necesidades.<br><img src="imgs/curso_academico.jpg" border="0" hspace="40" vspace="8"><br>
	<br><b>IRLANDA con oficina propia en DUBLIN</b><br>	
	<br>LOS COLEGIOS Y FAMILIAS en Irlanda tienen una gran tradici&oacute;n de acogida y de ayuda a los estudiantes extranjeros.
	<p>EL SISTEMA ACAD&Eacute;MICO IRLAND&Eacute;S analiza los conocimientos del estudiante para asignarle el nivel adecuado. &Eacute;ste es un programa para estudiantes que deseen vivir una experiencia internacional en un pa&iacute;s de habla inglesa cercano al nuestro. Al tener nuestras propias oficinas en Dubl&iacute;n disponemos de personal cualificado para supervisar la adaptaci&oacute;n y el bienestar de cada alumno.<br>
  	Los estudiantes tienen una coordinadora a la que pueden contactar las 24 horas del d&iacute;a, que les visita regularmente y est&aacute; en contacto con los profesores y familias irlandesas y tambi&eacute;n les organiza clases complementarias de ingl&eacute;s, si las precisan.<br>
  	Los estudiantes viajan en grupo y nos responsabilizamos de los billetes de avi&oacute;n; la oficina de Dubl&iacute;n se hace cargo de los traslados internos. En Irlanda se tienen dos semanas de vacaciones en Navidad y Pascua. En octubre y febrero hay una semana de vacaciones y los estudiantes pueden venir a Espa&ntilde;a o quedarse con sus familias irlandesas. Para los internos disponemos de familias de acogida.</p>
	<p>-Curso en colegio concertado y familia: a partir de 13.000 &euro;<br>
  	-En colegio privado, familia o internado: a partir de 16.000 &euro;<br>
  	-Un trimestre: a partir de 5.000 &euro;</p><br>
	<b>INGLATERRA</b><br>
	En los mejores internados privados de chicos, chicas o mixtos para estudiantes de diferentes niveles acad&eacute;micos y de diferentes conocimientos de ingl&eacute;s.<br>
	-Curso: a partir de 26.000 &euro;<br>
	-Trimestre: a partir de 9.000 &euro;<br>
	<br>
	<b>SUIZA Y AUSTRIA</b><br>
	En Suiza y Austria los internados son mixtos y est&aacute;n situados junto a un lago o en los Alpes. Los estudios se realizan por el sistema americano, brit&aacute;nico o suizo, en ingl&eacute;s, franc&eacute;s o alem&aacute;n. <br>
	En el caso de Austria (Salzburg) los estudios siguen el sistema americano pero los estudiantes deben, adem&aacute;s, elegir alem&aacute;n como segunda lengua.<br>
	-Curso: a partir de 40.000&euro;<br>
	<b>CANADA</b><br>
	<p>&Eacute;ste es un programa muy atractivo, ya que combina el sistema brit&aacute;nico de estudios con la conocida hospitalidad de los canadienses. Disponemos de colegios, familias y coordinadores en la zona de Vancouver y Toronto. En internado gestionamos matr&iacute;culas en distintas zonas del pa&iacute;s.<br>
  	-En familia: a partir de 25.000 &euro;<br>
  	-Trimestre: a partir de 9.000 &euro;<br>
	-Internado: a partir de 30.000 &euro;</p><br>
	<b>CESTADOS UNIDOS</b><br>
	COLEGIOS P&Uacute;BLICOS Y PRIVADOS EN FAMILIA<br>
	Este programa est&aacute; pensado para estudiantes con sinceras ganas de vivir una experiencia americana respetando al cien por cien la forma de vida y normas del pa&iacute;s al que van a vivir.<br>
	La inscripci&oacute;n se gestiona a trav&eacute;s de una Fundaci&oacute;n americana, que es la responsable del programa.<br>
	-P&uacute;blico: a partir de 7.000 &euro; Privado: a partir de 17.000 &euro;
	<p>INTERNADOS<br>
  	La oferta es muy amplia, trabajamos directamente con colegios muy reconocidos en varios estados y, dependiendo de los intereses del estudiante y de sus padres, podemos recomendarle el m&aacute;s apropiado: lugar, tipo de colegio, nivel acad&eacute;mico, ofertas especiales, deportes, etc.<br>
 	 -A partir de 35.000 &euro;</p>
	<p>CAMPUS UNIVERSITARIOS<br>
  	Es una buena opci&oacute;n para estudiantes a partir de 18 a&ntilde;os que quieren cursar el &uacute;ltimo a&ntilde;o de bachillerato con alojamiento en el campus del propio college, y compartiendo experiencia con estudiantes de su edad.<br>
  	-A partir de 24.000 &euro;</p>
	<p><i>Los billetes de avi&oacute;n no est&aacute;n incluidos en ning&uacute;n programa. <br>
  	Disponemos de un seguro de anulaci&oacute;n opcional. <br>
  	En algunos pa&iacute;ses se requiere un seguro m&eacute;dico privado.</i></p>
	</div>';
	break;

	case 6:
	echo '<div id="tcursos">Idioma a la carta</div>';
	echo '<div class="textblanco"><br>Estamos plenamente cualificados para asesorarle y ofrecerle un excelente programa "a medida". En la actualidad, contratar un curso de corta o larga duraci&oacute;n a trav&eacute;s de un consultor fiable garantiza la calidad acad&eacute;mica y la cuota de hispanohablantes. No ignore el nuevo escenario: un buen programa ling�&iacute;stico necesita de un buen consultor educativo y viceversa.</div>';
	break;
	
	default:
	echo "<div id='tcursos'>Presentaci&oacute;n</div>";
		
	echo '<div class="textblanco22"><br><p><b>ASTON IDIOMAS EN EL MUNDO</b> trabaja desde 1987 para ofrecer una educaci&oacute;n de calidad en el aprendizaje de lenguas extranjeras.</p><p>Las dimensiones de nuestra empresa nos permiten considerarnos "big enough to cope but small enough to care", lo que equivale a decir que tenemos acceso a los mejores programas educativos, al mismo tiempo que seguimos ofreciendo a nuestros alumnos y a sus familias un trato muy directo y personal.</p>
	<p><i>El  mejor viaje es la vida, y a menudo nos olvidamos de disfrutarla. El presente  nos ofrece infinitas oportunidades, posibilidades y circunstancias que hace  poco tiempo eran impensables.</p>
	<p>La  realidad actual requiere de una mente abierta, sana, valiente e ilusionada, de  personas h&aacute;biles para relacionarse, que crean en el futuro y en la cultura del  esfuerzo. Nuestros hijos, a veces demasiado inmersos en el consumo y en la  construcci&oacute;n de su yo acad&eacute;mico, deben aprender a valorar un viaje de estudios  como un gran avance en sus habilidades ling&uuml;&iacute;sticas pero tambi&eacute;n, y muy  importante, como la experiencia que les abrir&aacute; la mente y les har&aacute; capaces de  entusiasmarse por valores tan necesarios como la tolerancia, el respeto a la  diversidad, la armon&iacute;a, la no-violencia y el sentido del humor, a la vez que  les puede sensibilizar ante el arte y la cultura. Sin duda, un  viaje en versi&oacute;n original es un reto valiente.</p>
	<p>Todo  ello les ir&aacute; construyendo como personas y les har&aacute; m&aacute;s felices. En ASTON  creemos que la felicidad es una manera de viajar, no el final del viaje.</p>
	<p align="right">&Aacute;ngeles Faus</i></p></div>
	<img src="imgs/f_aston.jpg" name="imagen1">';
	break;
}
echo "</div>"; 
include "c_piecera.php";
?>