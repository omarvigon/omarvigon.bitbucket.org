<?php include "c_cabecera.php";?>
	<br><a href="3clases_cat.php"><b>Per qu&egrave; amb ASTON?</b></a>
		<div class="subtext">Solucions i experi&egrave;ncia</div>
	<span class="azul1"><b>Els nostres professors i els nostres cursos</b></span>
		<div class="subtext">En sintonia amb l'alumne  </div>
		<div class="subtext2"><a href="3clases_cat.php?aux3=profesores">Els professors</a></div>
		<div class="subtext2"><a href="3clases_cat.php?aux3=cursos">Els nostres cursos</a></div>
	<br><a href="3clases_cat.php?aux3=clases"><b>Les nostres classes</b></a>
		<div class="subtext">Portem la classe a l'alumne</div>
		<div class="subtext2"><a href="3clases_cat.php?aux3=chicos">Classes particulars per a nens</a></div>
		<div class="subtext2"><a href="3clases_cat.php?aux3=adultos">Classes particulars per a adults</a></div>
		<div class="subtext2"><a href="3clases_cat.php?aux3=empresas">Classes en empreses</a></div>
		<div class="subtext2"><a href="3clases_cat.php?aux3=especial">Cursos especialitzats</a></div>
	<br><a href="3clases_cat.php?aux3=examenes"><b>Ex&agrave;mens oficials</b></a>
		<div class="subtext">Preparar-se per al futur!</div>
	<a href="3clases_cat.php?aux3=extranjero"><b>Cursos per tot el m&oacute;n</b></a>
		<div class="subtext">dissenyi la seva experi&egrave;ncia!</div>
	<a href="3clases_cat.php?aux3=common"><b>El Marc Europeu Com&uacute;</b></a>
		<div class="subtext">Obtenir el reconeixement merescut!</div>
</div>
<div id="c_cursos">
	<div class="correo">
	<a href="mailto:astoninfo@astonidiomas.com" class="blanco" style="text-decoration:none;color:#FFFFFF;">astoninfo@astonidiomas.com</a>
	</div>

	
	<?php
	switch($aux3)
	{
	case "profesores":
	echo "<div id='tcursos'>Els professors: el nostre capital hum�</div>";
	echo "<div class='textblanco'>";
	echo "<br>Totes les nostres classes estan impartides per llicenciats universitaris i professors homologats en TESOL (ensenyament de l�angl�s per a parlants d�altres idiomes) o CELTA (certificat per a l�ensenyament de l�angl�s a adults), contractats despr�s d�un rigor�s proc�s de selecci�. 
	<p>El nostre equip est� format per professionals motivats i din�mics, entusiasmats amb la seva feina. La majoria s�n persones que han viatjat molt i tenen experi�ncia en l�ensenyament de l�idioma en diferents zones del m�n, de manera que la seva personalitat enriqueix i dimanitza molt les classes. La nostra idea �s que les classes, a m�s de servir per aprendre l�idioma, han de ser una oportunitat per trencar la rutina di�ria i renovar les energies gr�cies al contacte hum�. 
	<p>A Aston som conscients que els professors constitueixen el nostre actiu m�s valu�s i els proporcionem recolzament, assessorament, recursos i formaci� cont�nua per garantir la qualitat d�unes classes tan din�miques com sigui possible.";
	echo "</div>";
	echo "<img src='imgs/f_clases2.jpg' name='imagen3'>";	
	break;
	
	case "cursos":
	echo "<div id='tcursos'>Els nostres cursos: en sintonia amb l'alumne</div>";
	echo "<div class='textblanco4'>";
	echo "<br>Curs acad�mic per a joves o curs financer per a empreses. �s possible que l�alumne necessiti un curs personalitzat, adaptat a determinades necessitats, o b� un curs d�actualitzaci� o de manteniment. Pot escollir entre les nostres propostes de classes o indicar-nos quin horari necessita; de la resta ens n�ocupem nosaltres.
	<p>En tots els casos, la definici� clara dels objectius, la regularitat i l�assist�ncia sistem�tica a les classes permeten assolir els resultats desitjats. 
	<p>Quan s'acaben les classes, tant si s�n d�empresa com particulars, es lliuren informes trimestrals, i a final de curs es fa un examen que pot ser oficial o, de vegades, personalitzat o intern. Aquests es complementen amb una avaluaci� de control de qualitat i amb informes d�assist�ncia de l�alumne.";	
	echo "</div>";
	echo "<img src='imgs/f_clases3.jpg' name='imagen3'>";
	break;
	
	case "clases":
	echo "<div id='tcursos'>Les nostres classes: Portem la classe a l'alumne</div>";	
	echo "<div class='textblanco0'>";
	echo "<br><p>Es pot escollir entre la comoditat de les classes particulars a domicili o la utilitat d�assistir-hi en el lloc de treball.</b>
	<p>Sabem que el ritme de vida actual, tan exigent, mant� tothom ocupat tots els minuts del dia. Per aix�, com que ens desplacem nosaltres a l�hora de fer les classes, ens assegurem que el client aprofitar� al m�xim el seu temps: els nostres professors s�adapten a l�agenda i al lloc de treball o el domicili de l�alumne; des de primera hora del mat� fins a la nit, cinc dies a la setmana.";
	echo "</div>";
	echo "<img src='imgs/f_clases4.jpg' name='imagen3'>";
	break;
	
	case "chicos":
	echo "<div id='tcursos'>Classes particulars per a nens: din�miques i motivadores!</div>";
	echo "<div class='textblanco'>";
	echo "<br>Utilitzem els llibres i els recursos m�s recents, com ara DVD i jocs, per motivar els joves en unes classes entretingudes que estimulen el seu inter�s i l�aprenentatge i fomenten la participaci� i la mem�ria per mitj� del refor� d�est�muls visuals.
	<ul><li>Professors natius amb experi�ncia en l�ensenyament a nens. 
	<li>Classes divertides i entretingudes. 
	<li>Classes a domicili en grups redu�ts o individuals.     
	<li>Recolzament per als deures i classes generals d�angl�s.   
	<li>Ex�mens de Cambridge per a nens, KET & PET.
	<li>Ex�mens orals de Trinity College (Graded Interview).</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases5.jpg' name='imagen3'>";
	break;
	
	case "adultos":
	echo "<div id='tcursos'>Classes particulars per a adults: estil de vida i aprenentatge</div>";
	echo "<div class='textblanco'>";
	echo "<br>A Aston sabem quines dificultats comporta per a un adult el fet d�aprendre un idioma. Per aix�, oferim la m�xima flexibilitat pel que fa a horaris, freq��ncia i lloc de les classes, la qual cosa ens permet concentrar-nos en el nivell, les necessitats i l�aven� de l�alumne.
	<ul><li>Professors natius formals, din�mics i amb una personalitat interessant. 
	<li>Converses en qu� s�inclou correcci� gramatical per mantenir el nivell adquirit.   
	<li>Cursos per a ex�mens oficials. 
	<li>Classes individuals o en grups redu�ts.
	<li>Angl�s per a viatges i oci.
	<li>Material i metodologia d�avantguarda</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases6.jpg' name='imagen3'>";
	break;
	
	case "empresas":
	echo "<div id='tcursos'>Classes en empreses: ajudem a treballar, ajudem a prosperar</div>";	
	echo "<div class='textblanco5'>";
	echo "<br>Per a petites empreses o multinacionals, a Aston sabem la import�ncia que t� la comunicaci�.</b>  
	<ul><li>Els nostres professors es desplacen al lloc concertat per a la classe, tant si �s individual com per a un grup redu�t.  
	<li>Angl�s general o espec�fic, amb la freq��ncia o durant el temps que l�alumne ho necessiti.  
	<li>Cursos amb determinaci� pr�via d�objectius i continguts, adaptats a tots els nivells.  
	<li>Classes individuals de perfeccionament, per a uns objectius espec�fics.  
	<li>Cursos de preparaci� per a ex�mens oficials com a instruments motivadors i d�autoavaluaci�.  
	<li>Cursos d�angl�s per a l�empresa: angl�s comercial, redacci� de faxos, cartes, informes i missatges de correu electr�nic, estructures per a converses telef�niques. Compet�ncia per a la negociaci�, arguments per defensar i conv�ncer, presentacions i confer�ncies.
	<li>Assist�ncia peri�dica i informes sobre l�aven� de l�alumne. 
	<li>Cursos complementaris de capacitaci� per als negocis, altament personalitzats, al Regne Unit.</ul>";
	echo "</div><br>";
	echo "<img src='imgs/f_clases7.jpg' name='imagen3'>";
	break;
	
	case "especial":
	echo "<div id='tcursos'>Cursos especialitzats: busquem la perfecci�!</div>";
	echo "<div class='textblanco'>";
	echo "<br>Sigui quin sigui el nivell de l�alumne, l�ajudem a decidir quin cam� ha de rec�rrer per aconseguir els millors resultats. Descobreixi el nivell d�angl�s que necessita i aconsegueixi�l: posi punt i final als cicles d�aprenentatge estancat i escali els cims m�s reconeguts internacionalment. 
	<p>Li proposem una avaluaci� opcional per ajudar-lo a establir el seu nivell d�angl�s. 
	<ul><li>Perfeccionament i millora personal en determinades �rees de l�angl�s.  
	<li>Preparaci� i presentaci� a ex�mens oficials de tots els nivells, per a nens o adults, amb finalitats acad�miques o laborals.
	<li>Cursos d�angl�s inoblidables en alguns dels centres d�estudi i oci m�s privilegiats del m�n.  
	<li>Els cursos m�s personalitzats del sector de l�ensenyament d�angl�s en les principals institucions del Regne Unit per a professionals. Progressos demostrats en l�aprenentatge de l�idioma en aquests centres.</ul>";
	echo "</div>";
	echo "<img src='imgs/f_clases8.jpg' name='imagen3'>";
	break;
	
	case "examenes":
	echo "<div id='tcursos'>Ex�mens oficials: preparar-se per al futur!</div>";	
	echo "<div class='textblanco3'>";
	echo "<br>Com podem saber si hem avan�at realment o si hem assolit el nivell d�angl�s que ens propos�vem? La resposta passa necess�riament per la presentaci� a ex�mens oficials d�un organisme examinador extern que ens avalu� amb objectivitat.
	<p><b>Prepari�s per al futur</b> i per a les exig�ncies professionals amb els ex�mens de Cambridge o Trinity. ASTON �s un centre examinador oficial del Trinity College, de Londres, i compta amb el reconeixement del departament d�ex�mens ESOL de Cambridge University:
	<p><b>PROVA DE NIVELL: Descobreixi quin lloc ocupa en el marc europeu com�</b>  
	<p>Amb la nostra prova de nivell gratu�ta podrem situar-lo en la classificaci� CEF, a m�s d�aclarir els seus dubtes i aconsellar-li sobre els passos que ha de donar en el futur.
	<br><br><a href='files/cambridge_exams.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Cambridge Exams</a><br>
	<a href='files/trinity_exams.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Trinity Exams</a><br>
	<a href='files/level_test_part1.doc' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_doc.gif' hspace='6' border='0'>Level test part1</a><br>
	<a href='files/level_test_part2.doc' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_doc.gif' hspace='6' border='0'>Level test part2</a>";
	echo "</div>";
	break;
	
	case "extranjero":
	echo "<div id='tcursos'>Cursos d'idiomes a tot el m�n: dissenyi la seva experi�ncia!</div>";	
	echo "<div class='textblanco3'>";
	echo "<br>Viure i aprendre! Aprendre un idioma �s molt m�s que con�ixer-ne la gram�tica i el vocabulari o aprovar ex�mens. Els nostres cursos exclusius en tots els pa�sos de parla anglesa proporcionen un context ling��stic real i l�oportunitat de practicar tot el que s�ha apr�s, oblidar els temors i donar un pas definitiu cap endavant.
	<p>Moltes d�aquestes propostes de vacances combinades amb estudi s�n el complement ideal dels nostres cursos a Barcelona, per tancar el cicle d�aprenentatge de l�idioma. La llarga i estreta col�laboraci� que mantenim amb aquests centres en zones privilegiades de tot el m�n garanteix l�aven� de l�alumne, en tots els sentits.
	<p><b>Executius i c�rrecs d�empresa: Trenqui amb els cicles d�aprenentatge estancats i estalvi� temps!</b> 
	<p>Podria respondre de manera sincera i positiva a aquestes preguntes?
	<ul><li>�s capa� de comprometre�s un any m�s a assistir a classes?   
	<li>Creu que en les classes est�ndard s�aprofiten al m�xim el temps i els recursos?  
	<li>Est� avan�ant de la manera que vol / necessita?  
	<li>Ha assolit el nivell de compet�ncia desitjat? 
	<li>Quan podr� deixar d�assistir a classes regularment?  </ul> 
	<p><b>Amb la nostra f�rmula:</b>
	<li>Desenvolupament personal ASTON + Angl�s comercial Pilgrims + Avaluaci� oficial
	<p>Li garantim que podr� trencar amb els cicles d�aprenentatge estacat i fer un salt definitiu cap endavant. Les nostres classes a mida i el nostre excepcional curs d�angl�s comercial a Canterbury, Anglaterra, l�ajudaran a estalviar diners a llarg termini perqu�: 
	<ul><li>Reduir� espectacularment el seu calendari d�estudi de l�angl�s.   
	<li>Aprofitar� de manera efica� tots els moments que dediqui a aprendre angl�s. 
	<li>Descobrir� quines s�n les necessitats reals i es concentrar� �nicament en aquestes.
	<li>Rebr� ajuda per assolir el nivell que es proposi i per poder demostrar que l�ha assolit.   
	<li>Establir� uns punts d�inici i final clars per a l�aprenentatge de l�angl�s.</ul>
	<p>Creiem que el seu temps �s or i que el preu que paga �s una inversi�, i per aquest motiu els dediquem tot el respecte possible.
	<p>Vegi l'apartat  <a href='2cursos.php' style='color:gray'>'Cursos de Idiomas'</a> a la p�gina d�inici per obtenir informaci� m�s detallada.";
	echo "</div>";
	break;
	
	case "common":
	echo "<div id='tcursos'>El Marc  Europeu Com&uacute</div>";	
	echo "<div class='textblanco'>";
	echo "<br>En quin punt es troba, dins del recorregut de l�aprenentatge de l�angl�s? Li proposem que respongui a la prova de nivell gratu�ta i ens l�envi�. La corregirem i li enviarem una orientaci� fiable sobre el seu lloc de classificaci�, segons la taula seg�ent.
	<br><br><a href='files/marco_europeo_comun.pdf' style='color:#FFFFFF;text-decoration:none;font-weight:bold;' target='_blank'><img src='imgs/icon_pdf.gif' hspace='6' border='0'>Common European Framework</a>";
	echo "</div>";
	break;
		
	default:
	echo "<div id='tcursos'>Per qu&egrave; amb ASTON? -  Solucions i experi&egrave;ncia</div>";	
	echo "<div class='textblanco4'>";
	echo "<br><p><b>Solucions per a l'aprenentatge:</b></p>Oferim solucions reals perqu� entenem els nostres alumnes: sabem les dificultats que comporta aprendre un idioma i dissenyem els nostres cursos altament personalizats per adaptar-los a les necessitats i les compet�ncies reals.
	<p>Combinem la comoditat del servei en el domicili particular o el lloc de treball amb unes classes estimulants per treure el m�xim profit del proc�s d�aprenentatge.  
	<p>Conjuntament amb l�alumne, avaluem i planifiquem el desenvolupament, el contingut i la durada del curs.
	<p><b>Credibilitat i experi�ncia: </b></p>Som un equip de professionals qualificats en EFL (angl�s com a llengua estrangera) amb una provada traject�ria de vint anys com a organitzaci� dedicada a l�ensenyament de l�angl�s. El nostre objectiu �s que l�alumne avanci en l�aprenentatge de l�idioma i aconsegueixi bons resultats, i impartim ensenyament de la millor qualitat a un ventall molt ampli d�alumnes: nens, adolescents i adults, empreses i executius i altres professionals. 
	<p><b>Cursos adaptats a qualsevol necessitat:</b></p> 
	<b>En tots els nostres cursos:</b> Abans de comen�ar, i de manera conjunta amb el client, s�estableixen el nivell, els objectius i la freq��ncia de les classes, a m�s del contingut del curs. Es decideix on es vol arribar i quin cam� cal seguir. 
	<p><b>Reconeixement:</b> A m�s de ser un centre examinador oficial del Trinity College, comptem amb el reconeixement del departament d�ex�mens d�ESOL (angl�s per a parlants d�altres idiomes) de Cambridge University. Tanmateix, possiblement el millor reconeixement de tots �s la fidelitat dels nostres clients.
	<p><b>El cicle complet d'aprenentatge:</b> Impartim classes d�angl�s durant el curs escolar i organitzem estades per aprendre l�idioma a Espanya i als pa�sos de parla anglesa durant l�estiu. Aquestes s�adrecen a totes les edats i els nivells, i s�ofereixen en els centres m�s exclusius i amb el comprom�s d�uns acords �nics per garantir els resultats.";
	echo "</div><br>";
	echo "<img src='imgs/f_clases1.jpg' name='imagen3'>";
	break;
	
	}
	?>
	</div>
</div>

<?php include "c_piecera.php";?>