<?php include("cabecera.php");?>
		
  </td>
  <td class="celda_2">
     	<script type="text/javascript">
		new fadeshow(fadeimages, 560, 157, 0, 6000, 1)
		</script>
        
        <h1>La hand the Sergi : Personal Orders</h1>
        <?php
		switch($_SESSION["idioma"])
		{
			case "cat": ?>
            <p>En aquesta secci�,  et presentem un seguit de propostes personalitzades.L�equip <em>La hand the  Sergi</em> pensem en per exemple, la teva familia, fills , o en persones a les  qui vulguis donar dedicaci� exclusiva i atenci� personalitzada per un moment. O  per dos. Regala dibuixos originals i� si  vols� series o tiratges et preparem les  c�pies manipulades, totes elles diferents, per� tallades sota el mateix patr�.  La serigrafia de segona generaci� Cada exemplar �s diferent als anteriors, com  cada minut. I com cada suspir.</p>
            <p style="margin:15px 0 110px 20px;"><img src="http://www.lahandthesergi.com/img/encargo2.jpg" align="left" style="margin:0 15px 0 0;">Aquest �s l�oli, �nic i idea original.</p>
        	<p style="margin:15px 0 080px 20px;"><img src="http://www.lahandthesergi.com/img/encargo1.jpg" align="left" style="margin:0 15px 0 0;">Aquest �s el dibuis original, en paper.</p>
        	<p style="margin:15px 0 300px 20px;"><img src="http://www.lahandthesergi.com/img/encargo3.jpg" align="left" style="margin:0 15px 0 0;">I aqu� pots veure com queden en aquest cas,  les c�pies manipulades del dibuis original. Serigrafia de segona generaci�.</p>
        	<p style="margin:15px 0 225px 20px;"><img src="http://www.lahandthesergi.com/img/encargo4.jpg" align="left" style="margin:0 15px 0 0;">Si ho prefereixes, tamb� pots decantar-te  per l�oli. Un retrat familiar. Tot un punt.</p>
        	<p style="margin:15px 0 330px 20px;"><img src="http://www.lahandthesergi.com/img/encargo5.jpg" align="left" style="margin:0 15px 0 0;">...i una imatge de la teva m�, el teu  inseparable rellotge? El teu nom�genial, veritat?</p>
        	<p style="margin:15px 0 500px 20px;"><img src="http://www.lahandthesergi.com/img/encargo6.jpg" align="left" style="margin:0 15px 0 0;">I un retrat teu, sota la m�s  hiperealista de las perspectives...</p>
        	<p>Acabem la secci� amb  la famosa poesia "If" de <b>Rudyard Kipling</b></b></p>
			<p>Si pots  mantenir el cap sobre les espatlles<br>quan uns altres el perden i te�n carreguen la seva culpa,<br>
            Si confies en tu mateix encara que tothom dubti de tu,<br>per� tot i aix� tens en compte els dubtes aliens;<br>
            Si pots somiar i no fer-te esclau dels somnis;<br>Si pots pensar sense fer dels teus pensaments una meta;<br>
            Si topes amb Triomf i Derrota en el teu cam�<br>i tractes de la mateixa manera a tots dos impostors, <br>
            Si pots fer  un munt amb totes les teves vict�ries<br>Si pots llan�ar-les al capritx de l'atzar,<br>
            i perdre, i tornar a comen�ar de cap i de nou<br>sense que et surti dels llavis una queixa;<br>
            Si assoleixes que el tremp i el cor et siguin fidel company<br>i resisteixes encara que et minvin les forces<br>
            amb l'�nica ajuda de la voluntat, que diu: �Endavant!� <br>Si pots parlar  amb multituds matenint la virtut,<br>
            i si encara anant al costat dels reis conserves la senzillesa,<br>Si no et poden ferir ni amics ni enemics,<br>
            Si tothom et reclama i ning� et necessita;<br>Si pots omplir un implacable minut<br>
            amb seixanta segons de combat ferotge,<br>teva �s la Terra  i els seus fruits cobejats,<br>
            I, encara m�s: ser�s un Home, fill meu!</p> 
            <? break;
			
			case "ing": ?>
            <p>This section presents our custom-made works. At <em>La hand the Sergi</em> we create gifts for your family, children or for those you wish to lavish with an exclusive  gift. Because as the saying goes, they�re worth it. We will create for you original  drawings, and if you wish to have series or print runs we can prepare  manipulated prints, each one unique. These are not mere screen prints. Each one  is different, much like every minute or every breath is different.</p>
            <p style="margin:15px 0 110px 20px;"><img src="http://www.lahandthesergi.com/img/encargo2.jpg" align="left" style="margin:0 15px 0 0;">This is the  oil painting, unique and original.</p>
        	<p style="margin:15px 0 080px 20px;"><img src="http://www.lahandthesergi.com/img/encargo1.jpg" align="left" style="margin:0 15px 0 0;">This is the original  drawing</p>
        	<p style="margin:15px 0 300px 20px;"><img src="http://www.lahandthesergi.com/img/encargo3.jpg" align="left" style="margin:0 15px 0 0;">And here we show you how  the manipulated prints of the drawing turned out.</p>
        	<p style="margin:15px 0 220px 20px;"><img src="http://www.lahandthesergi.com/img/encargo4.jpg" align="left" style="margin:0 15px 0 0;">If you  prefer, you can opt for an oil painting. An image of your family.</p>
        	<p style="margin:15px 0 330px 20px;"><img src="http://www.lahandthesergi.com/img/encargo5.jpg" align="left" style="margin:0 15px 0 0;">... and one  of your hand, your watch and your name? Isn�t that a stroke of genius?</p>
        	<p style="margin:15px 0 500px 20px;"><img src="http://www.lahandthesergi.com/img/encargo6.jpg" align="left" style="margin:0 15px 0 0;">And your  self-portrait, as hyper-realist as you can get...</p>
        	<p>To end this  section, <b>Rudyard Kipling�s </b>famous poem.</p>
            <p>If you can keep your head  when all about you<br>Are losing theirs and blaming it on you,<br>
            If you can trust yourself when all men doubt you,<br>But make allowance for their doubting too;<br>
            If you can wait and not be tired by waiting,<br>Or being lied about, don't deal in lies,<br>
            Or being hated, don't give way to hating,<br>And yet don't look too good, nor talk too wise:<br>
            If you can dream - and not  make dreams your master,<br>If you can think - and not make thoughts your aim;<br>
            If you can meet with Triumph and Disaster<br>And treat those two impostors just the same;<br>
            If you can bear to hear the truth you've spoken<br>Twisted by knaves to make a trap for fools,<br>
            Or watch the things you gave your life to, broken,<br>And stoop and build 'em up with worn-out tools:<br>
            If you can make one heap of  all your winnings<br>And risk it all on one turn of pitch-and-toss,<br>
            And lose, and start again at your beginnings<br>And never breath a word about your loss;<br>
            If you can force your heart and nerve and sinew<br>To serve your turn long after they are gone,<br>
            And so hold on when there is nothing in you<br>Except the Will which says to them: &quot;Hold on!&quot;<br>
            If you can talk with crowds  and keep your virtue,<br>Or walk with kings - nor lose the common touch,<br>
            If neither foes nor loving friends can hurt you,<br>If all men count with you, but none too much;<br>
            If you can fill the unforgiving minute<br>With sixty seconds' worth of distance run,<br>
			Yours is the Earth and everything that's in it,<br>And - which is more - you'll be a Man, my son!</p>
            <? break;
            
			default: ?>
       		<p>En esta secci�n te  presentamos propuestas personalizadas. En el equipo de <em>La hand the Sergi</em> pensamos en tu familia, hijos, o en personas a las que darles dedicaci�n  exclusiva por un momento. O por otro m�s. O como dicen,� por que ellas lo valen. Te hacemos nemos  dibujos originales y si quieres series o tirajes te preparamos copias  manipuladas, todas ellas diferentes. No es una simple serigraf�a. Cada ejemplar  es distinto a los anteriores, como cada minuto. Cada suspiro.</p>
            <p style="margin:15px 0 110px 20px;"><img src="http://www.lahandthesergi.com/img/encargo2.jpg" align="left" style="margin:0 15px 0 0;">Este �s el �leo, �nico y  original.</p>
        	<p style="margin:15px 0 080px 20px;"><img src="http://www.lahandthesergi.com/img/encargo1.jpg" align="left" style="margin:0 15px 0 0;">Este es el dibujo  original.</p>
        	<p style="margin:15px 0 300px 20px;"><img src="http://www.lahandthesergi.com/img/encargo3.jpg" align="left" style="margin:0 15px 0 0;">Y aqu� te mostramos como  quedaron las copias manipuladas del dibujo.</p>
        	<p style="margin:15px 0 220px 20px;"><img src="http://www.lahandthesergi.com/img/encargo4.jpg" align="left" style="margin:0 15px 0 0;">Si lo prefieres puedes  decantarte por el �leo. Un dibujo familiar.</p>
        	<p style="margin:15px 0 330px 20px;"><img src="http://www.lahandthesergi.com/img/encargo5.jpg" align="left" style="margin:0 15px 0 0;">... y uno de tu mano, tu  reloj y tu nombre? Todo� un punto,  verdad? </p>
        	<p style="margin:15px 0 500px 20px;"><img src="http://www.lahandthesergi.com/img/encargo6.jpg" align="left" style="margin:0 15px 0 0;">Y, tu autoretrato,� desde la m�s hiperealista de las  perspectivas...</p>
        	<p>Termina la secci�n con la famosa poes�a de <b>Rudyard Kipling</b></p>
            <p>Si puedes mantener la cabeza sobre los hombros<br>cuando otros la pierden y te cargan su culpa,<br>
        	Si conf�as en ti mismo a�n cuando todos de ti dudan,<br>pero a�n as� tomas en cuenta sus dudas;<br>
        	Si puedes esperar sin que te canse la espera,<br>o soportar calumnias sin pagar con la misma moneda,<br>
        	o ser odiado sin dar cabida al odio,<br>y ni ensalzas tu juicio ni ostentas tu bondad:<br>
        	Si puedes so�ar y no hacer de tus sue�os tu gu�a;<br>Si puedes pensar sin hacer de tus pensamientos tu meta;<br>
        	Si Triunfo y Derrota se cruzan en tu camino<br>y tratas de igual manera a ambos impostores,<br>
        	Si puedes tolerar que los bribones,<br>tergiversen la verdad que has expresado<br>
        	y que sea trampa de necios en boca de malvados,<br>o ver en ruinas la obra de tu vida,<br>
        	y agacharte a forjarla con �tiles mellados:<br><br>Si puedes hacer un mont�n con todas tus victorias<br>
        	Si puedes arrojarlas al capricho del azar,<br>y perder, y remontarte de nuevo a tus comienzos<br>
        	sin que salga de tus labios una queja;<br>Si logras que tus nervios y el coraz�n sean tu fiel compa�ero<br>
        	y resistir aunque tus fuerzas se vean menguadas<br>con la �nica ayuda de la voluntad que dice: ��Adelante!�<br>
        	Si ante la multitud das a la virtud abrigo,<br>Si a�n marchando con reyes guardas tu sencillez,<br>
        	Si no pueden herirte ni amigos ni enemigos,<br>Si todos te reclaman y ninguno te precisa;<br>
        	Si puedes rellenar un implacable minuto<br>con sesenta segundos de combate brav�o,<br>
       		tuya es la Tierra y sus codiciados frutos,<br>Y, lo que es m�s, �ser�s un Hombre, hijo m�o!</p>
		  	<? break;
		}
		?>
         
<?php include("piecera.php");?>
