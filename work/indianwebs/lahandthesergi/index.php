<?php include("cabecera.php");?>
		<a class="boton_idioma" href="http://www.lahandthesergi.com/index.php?lang=esp">castellano&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://www.lahandthesergi.com/index.php?lang=cat">catal�&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://www.lahandthesergi.com/index.php?lang=ing">english&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://209.85.135.104/translate_c?hl=es&langpair=en%7Cit&u=http://www.lahandthesergi.com/index.php" target="_blank">italiano&nbsp;&nbsp;</a>         	
		<a class="boton_idioma" href="http://209.85.135.104/translate_c?hl=es&langpair=en%7Cpt&u=http://www.lahandthesergi.com/index.php" target="_blank">portugu&ecirc;s&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://209.85.135.104/translate_c?hl=es&langpair=en%7Cfr&u=http://www.lahandthesergi.com/index.php" target="_blank">fran&ccedil;ais&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://www.google.es/translate?u=http%3A%2F%2Fwww.lahandthesergi.com%2Findex.php%3Flang%3Ding&langpair=en%7Cru&hl=es&ie=UTF8" target="_blank">russian&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://64.233.179.104/translate_c?hl=es&langpair=en%7Cde&u=http://www.lahandthesergi.com/index.php%3Flang%3Ding" target="_blank">deutsch&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://www.google.es/translate?u=http%3A%2F%2Fwww.lahandthesergi.com%2Findex.php%3Flang%3Ding&langpair=en%7Car&hl=es&ie=UTF8" target="_blank">arabic&nbsp;&nbsp;</a>
        <a class="boton_idioma" href="http://www.google.es/translate?u=http%3A%2F%2Fwww.lahandthesergi.com%2Findex.php%3Flang%3Ding&langpair=en%7Czh-TW&hl=es&ie=UTF8" target="_blank">chinese&nbsp;&nbsp;</a> 
        <a class="boton_idioma" href="http://www.google.es/translate?u=http%3A%2F%2Fwww.lahandthesergi.com%2Findex.php%3Flang%3Ding&langpair=en%7Cja&hl=es&ie=UTF8 target="_blank"">japanese&nbsp;&nbsp;</a>
        
  </td>
<td class="celda_2">
        <script type="text/javascript">
		new fadeshow(fadeimages, 560, 157, 0, 6000, 1)
		</script>
		
        <h1>La hand the Sergi : The City Art Gallery <i style="color:#CCCCCC;font-style:normal;">for-the-future</i></h1>
		<table cellpadding="0" cellspacing="0">
        <tr><td>
		<?php 
		switch($_SESSION["idioma"])
		{	
			case "cat": ?>
            <p><em>La hand the  Sergi</em> la crea <b>Sergi Pujol</b> al setembre de 2004 al Barri de la Ribera, entre Santa Maria  del Mar i l�antic Mercat del Born, situada en el centre hist�ric de Barcelona.  </p>
			<p>Aquesta Galeria neix amb la intenci� de ser un espai obert a totes les  tend�ncies art�stiques contempor�nies. Per aix�, obre les seves portes a joves  artistes de tot el m�n amatents a establir amb el p�blic un di�leg en el qual  l�obra d�art sigui en tot moment la via de comunicaci�. </p>
			<p><em>L�equip de La hand the Sergi</em> est� obert a col�laborar, tant en l��mbit privat com en el p�blic, amb  clients pertanyents a qualsevol �mbit de la creaci� art�stica: moda,  publicitat, etc., per� necessitem, sobretot, <b>gent com tu</b> que col�labori amb  nosaltres per a fer d�aquest projecte de futur una realitat tangible i  evolutiva. </p>
			<? break;
			
			case "ing": ?>
			<p><em>La hand the Sergi</em> was created by <b>Sergi Pujol</b> in September 2004 in the  Ribera District of Barcelona, between Santa Mar�a del Mar church and the old  Borne Market, in the historic town centre.</p>
			<p>The Gallery was conceived as an open space for all contemporary artistic  trends. It opens its doors to young artists from around the world who wish to  establish with the public a dialogue in which works of art are the means of  communication.</p>
			<p><em>The team of La hand the Sergi</em> is open to collaborating, both privately  and publicly, with clients from any artistic sector: fashion, advertising,  etc., but we need people like you to collaborate with us to make this project a  tangible, evolving reality.</p>
            <? break;
            
			default: ?>
		    <p><em>La hand the Sergi</em> la crea <b>Sergi  Pujol</b> en septiembre de 2004 en el Barrio de la Ribera, entre Santa Mar&iacute;a  del Mar y el antiguo Mercado del Borne, situada en el centro hist&oacute;rico de  Barcelona.</p>
    	    <p>Esta Galer&iacute;a nace con la intenci&oacute;n  de ser un espacio abierto a todas las tendencias art&iacute;sticas contempor&aacute;neas. Por  ello, abre sus puertas a j&oacute;venes artistas de todo el mundo dispuestos a  establecer con el p&uacute;blico un di&aacute;logo en el que la obra de arte sea en todo  momento la v&iacute;a de comunicaci&oacute;n.</p>
	        <p><em>El equipo de La hand the Sergi</em> est&aacute; abierto a colaborar, tanto en el &aacute;mbito privado como en el p&uacute;blico, con  clientes pertenecientes a cualquier &aacute;mbito de la creaci&oacute;n art&iacute;stica: moda,  publicidad, etc., pero necesitamos sobre todo <b>gente como t&uacute;</b> que colabore con  nosotros para hacer de este proyecto de futuro una realidad tangible y  evolutiva.</p>
			<? break;
		}
		?>
        </td>
        <td>
       	 	<h5><a href="http://www.lahandthesergi.com/noticias.php">ART NEWS<br><?=constant("MES_".date("m"))." ".date("Y");?></a></h5>
            <div class="noticia">
            <?php ultimas(false); ?>
            </div>

        </td></tr>
        </table>
        <p><em>&quot;I'm a deeply superficial person&quot;&nbsp;- (Andy Warhol)</em></p>
	
<?php include("piecera.php");?>
