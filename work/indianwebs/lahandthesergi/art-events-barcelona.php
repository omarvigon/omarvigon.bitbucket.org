<?php include("cabecera.php");?>
		<?php
        $mes1["esp"]="Enero";			$mes1["cat"]="Gener";		$mes1["ing"]="January";
		$mes2["esp"]="Febrero";			$mes2["cat"]="Febrer";		$mes2["ing"]="February";
		$mes3["esp"]="Marzo";			$mes3["cat"]="Mar�";		$mes3["ing"]="March";
		$mes4["esp"]="Abril";			$mes4["cat"]="Abril";		$mes4["ing"]="April";
		$mes5["esp"]="Mayo";			$mes5["cat"]="Maig";		$mes5["ing"]="May";
		$mes6["esp"]="Junio";			$mes6["cat"]="Juny";		$mes6["ing"]="June";
		$mes7["esp"]="Julio";			$mes7["cat"]="Juliol";		$mes7["ing"]="July";
		$mes8["esp"]="Agosto";			$mes8["cat"]="Agost";		$mes8["ing"]="August";
		$mes9["esp"]="Septiembre";		$mes9["cat"]="Setembre";	$mes9["ing"]="September";
		$mes10["esp"]="Octubre";		$mes10["cat"]="Octubre";	$mes10["ing"]="October";
		$mes11["esp"]="Noviembre";		$mes11["cat"]="Novembre";	$mes11["ing"]="November";
		$mes12["esp"]="Diciembre";		$mes12["cat"]="Desembre ";	$mes12["ing"]="December";
		$mes13["esp"]="Pa�ses";			$mes13["cat"]="Pa�sos";		$mes13["ing"]="Countries";
		?>
        <a class="boton" href="art-events-barcelona.php?expo=enero"><? echo $mes1[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=febrero"><? echo $mes2[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=marzo"><? echo $mes3[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=abril"><? echo $mes4[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=mayo"><? echo $mes5[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=junio"><? echo $mes6[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="#"><? echo $mes7[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="#"><? echo $mes8[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=septiembre"><? echo $mes9[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=octubre"><? echo $mes10[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=noviembre"><? echo $mes11[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="art-events-barcelona.php?expo=diciembre"><? echo $mes12[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
        <a class="boton" href="http://www.picassomio.es/ferias-de-arte.html" target="_blank"><? echo $mes13[$_SESSION["idioma"]];?>&nbsp;&nbsp;</a>
	</td>
	<td class="celda_2">
		<script type="text/javascript">
		new fadeshow(fadeimages, 560, 157, 0, 6000, 1)
		</script>
        
		<h1>La hand the Sergi : Art Events</h1>
        <?php switch($_GET["expo"])
		{
			case "enero":
			echo "<p><em>".$mes1[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.londonartfair.co.uk/page.cfm">Londres, London Art Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.bolognafiere.it/bof_index.asp?m=73&l=1&ma=108">Bolonia, Arte Fiera Bologna</a>
    		<a class="enlaces" target="_blank" href="http://www.ifae.com/">Palm  Beach, PalmBeach  Art Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.sanfordsmith.com/">Nueva  York, Outsider Art  Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.artfairsinc.com/">Santa Monica, ArtL.A</a>
    		<a class="enlaces" target="_blank" href="http://www.dearte.info/">Madrid, deArte</a>
			</p>';
			break;
			
			case "febrero":
			echo "<p><em>".$mes2[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.ifema.es/ferias/arco/default.html">Madrid, ARCO</a>
    		<a class="enlaces" target="_blank" href="http://www.art-madrid.com/">Madrid, Art-Madrid</a>
    		<a class="enlaces" target="_blank" href="http://www.artdealers.org/artshow/index.html">Nueva York, ADDA</a>
    		<a class="enlaces" target="_blank" href="http://www.20-21intartfair.com/">Londres, 20-21 International Art Fair</a></p>';
			break;
			
			case "marzo":
			echo "<p><em>".$mes3[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.artparis.fr/">Par�s, Art Par�s</a>
		    <a class="enlaces" target="_blank" href="http://www.tefaf.com/">Maastrich, TEFAF</a>
 		    <a class="enlaces" target="_blank" href="http://www.artexpos.com/ME2/Audiences/splash.asp">Ohio, Artexpo</a></p>';
			break;
			
			case "abril":
			echo "<p><em>".$mes4[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.expopark.ru/EN/ART_Moscow.shtml?id=134&larguash=EN">Mosc�, ArtMoscow</a>
    		<a class="enlaces" target="_blank" href="http://www.artexis.com/artbrussels/home.htm">Bruselas, ArtBrussels</a>
    		<a class="enlaces" target="_blank" href="http://www.berlinbiennale.info/">Berl�n, BerlinBiennale</a>
   			<a class="enlaces" target="_blank" href="http://www.mexicoartecontemporaneo.com/">M�jico, MACO</a>
    		<a class="enlaces" target="_blank" href="http://www.miart.it/ita/contatti.asp">Mil�n, MiArt</a></p>';
			break;
			
			case "mayo":
			echo "<p><em>".$mes5[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.swab.es/">Barcelona, Swab Art Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.fineartfairfrankfurt.info/www/">Frankfurt, ArtFrankfurt</a>
    		<a class="enlaces" target="_blank" href="http://www.artchicago.com/">Chicago, ArtChicago</a>
    		<a class="enlaces" target="_blank" href="http://www.kunstrai.nl/">Amsterdam, KunstRAI</a>
    		<a class="enlaces" target="_blank" href="http://www.sp-arte.com/">Sao Paulo, SP Arte</a>
    		<a class="enlaces" target="_blank" href="http://www.circapr.com/spa/index.html">Puerto Rico, CIRCA</a>
    		<a class="enlaces" target="_blank" href="http://www.haughton.com/">Nueva  York, Haughton  International Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.europart.ch/">G�nova, Europ�ART</a>
   		 	<a class="enlaces" target="_blank" href="http://www.arteba.com/">Buenos Aires, arteBA</a></p>';
			break;
			
			case "junio":
			echo "<p><em>".$mes6[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.art.ch/go/id/kh/">Basilea, Art BASEL</a>
	  		<a class="enlaces" target="_blank" href="http://www.kunstart.it/eng/connection.asp">Bolzano, KunStart Art Fair</a>
	  		<a class="enlaces" target="_blank" href="http://www.grosvenor-fair.co.uk/">Londres, The Grosvenor House  Art&amp;Antiques Fair</a>
	  		<a class="enlaces" target="_blank" href="http://www.biennale-de-lyon.org/">Lyon, Bienale d�Art Contemporain</a>
	  		<a class="enlaces" target="_blank" href="http://www.bienaldevalencia.com/">Valencia, Bienal de Valencia</a>
	  		<a class="enlaces" target="_blank" href="http://www.documenta12.de/">Kassel, Documenta Kassel</a></p>';
			break;
			
			case "septiembre":
			echo "<p><em>".$mes9[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.biennial.com/">Liverpool, The Liverpool Biennial of  Contemporary Art</a>
    		<a class="enlaces" target="_blank" href="http://www.artefaim.net/">Madrid, Feria de Arte Independiente en Madrid</a>
    		<a class="enlaces" target="_blank" href="http://www1.messe-berlin.de/vip8_1/website/MesseBerlin/htdocs/art-forum-berlin/index_e.html">Berlin, Art Forum Berlin</a></p>';
			break;
			
			case "octubre":
			echo "<p><em>".$mes10[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.friezeartfair.com/">Londres, Frieze Art Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.artfair.com.au/">Melburne, Melbourne Art Fair</a></p>';
			break;
			
			case "noviembre":
			echo "<p><em>".$mes11[$_SESSION["idioma"]]."</em></p>";
			echo '<p>
			<a class="enlaces" target="_blank" href="http://www.artissima.it/">Torino, Artissima</a>
    		<a class="enlaces" target="_blank" href="http://www.tiafair.com/">Toronto, Toronto International Art Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.kunstzuerich.ch/">Zurich, Kunst</a>
    		<a class="enlaces" target="_blank" href="http://www.artcologne.de/">Colonia, Art Cologne</a>
    		<a class="enlaces" target="_blank" href="http://www.estampa.org/spain/index.html">Madrid, Estampa</a>
    		<a class="enlaces" target="_blank" href="http://www.sanfordsmith.com/art20_info.html">Nueva York, Art20</a>
    		<a class="enlaces" target="_blank" href="http://www.fineartboston.com/">Boston, Boston International Fine Art Show</a>
    		<a class="enlaces" target="_blank" href="http://www.artelisboa.fil.pt/">Lisboa, ArteLisboa</a>
    		<a class="enlaces" target="_blank" href="http://www.cnarts.net/sartfair/">Shangai, Shangai Art Fair</a>
    		<a class="enlaces" target="_blank" href="http://www.st-art.fr/">Estrasburgo, ST-Art</a></p>';
			break;
			
			case "diciembre":
			echo "<p><em>".$mes12[$_SESSION["idioma"]]."</em></p>";
			echo '<p><a class="enlaces" target="_blank" href="http://www.artmiami.com/ME2/Audiences/Default.asp?AudID=851BE093C224409BA852D0240FF17373">Miami, ArtMiami</a></p>';
			break;
			
			default:
			switch($_SESSION["idioma"])
			{
				case "cat":
				echo "<p>L�equip de <em>La hand the Sergi</em> us ofereix una mostra dels diferents cert�mens  i les principals fires d�art contemporani que es celebren per tot el m�n. Si  esteu de viatge, coincideixen les dates i us ve de gust, teniu un altre punt on  clavar-hi el comp�s. El forat 19?. Qui ho sap. Amb un sol clic en el mes  escollit, un apareixeran les visites que proposem.</p><p><em>Ens veiem</em> a alguna d�elles...</p>";
				echo "<p><em>MUSEUS</em></p>";
				echo "<p><em>L�equip de La Hand the Sergi</em> us proposa un viatge temporal I intemporal, amb dicotomies i sense,  amb propostes suggerents i anhelables,  amb proposicions i juxtaposicions, una incursi� que ens allunyi, per un instant  �o per dos-, de la prosa de la vida. Creiem que sense moure�ns podem passejar  per l�art de Barcelona, Madrid, Paris, Londes, Nova York, entre d�altres  ciutats. Comencem...</p>";
				break;
				
				case "ing":
				echo "<p>In this section, the team at <em>La hand the Sergi</em> presents a  sample of the different contemporary art events and main fairs that are  celebrated across the world. If you are travelling, and the dates coincide and  you fancy it, you have another stop in your itinerary. Hole 19? Who knows... Simply <em>click</em> on a month, and several possible visits will appear on your  screen.</p>";
				echo "<p><em>Look forward to seeing you </em>at any one of them</p>";
				echo "<p><em>The team at La Hand the Sergi</em> invites you on a timeless  journey through time, with or without dichotomies, with suggestive and desirable ideas, with propositions and juxtapositions, an incursion that removes us, for  a second�or two � from the prose of life. We believe that we can stroll through  the art of Barcelona, Madrid, Paris, London,   New York and other metropolis,  without moving. Let�s get started...</p>";
				break;
				
				default:
				echo "<p>El equipo de <em>La  hand the Sergi</em> os presenta en esta secci�n una muestra de los diferentes  cert�menes y las principales&nbsp;ferias de arte contempor�neo que se celebran en  todo el mundo. Si est�is de viaje, coinciden las fechas y os apetece, ten�is  otro punto donde clavar el comp�s. El hoyo 19?. Qui�n sabe. Con un s�lo <em>click</em> en el mes elegido, os aparecer�n varias visitas posibles.<br><br><em>Nos vemos</em> en alguna de ellas</p>";
				echo "<p><em>MUSEOS</em></p>";
				echo "<p><em>El equipo de La Hand  the Sergi</em> os propone un viaje temporal y atemporal, con dicotom&iacute;as y sin ellas, con propuestas sugerentes y  anhelables, con proposiciones y yuxtaposiciones, una incursi&oacute;n que nos aleje,  por un instante &ndash;o por dos- de la prosa de la vida. Creemos que sin movernos  podemos pasear por el arte de Barcelona, Madrid, Par&iacute;s, Londres y Nueva York,  entre otras metr&oacute;polis. Empezemos...</p>";
				break;
			}
			echo "<p>
        	<a class='enlaces' href='http://www.museupicasso.bcn.es' target='_blank'>Barcelona, Museu Picasso</a>
        	<a class='enlaces' href='http://www.mnac.es' target='_blank'>Barcelona, Museu Nacional d'Art de Catalunya </a>
        	<a class='enlaces' href='http://www.macba.es' target='_blank'>Barcelona, Museu d'Art Contemporani de Barcelona</a>
        	<a class='enlaces' href='http://www.museoprado.es' target='_blank'>Madrid, Museo nacional del Prado</a>
        	<a class='enlaces' href='http://www.museothyssen.org' target='_blank'>Madrid, Museo de Arte Thyssen-Bornemisza</a>
        	<a class='enlaces' href='http://www.museoreinasofia.es' target='_blank'>Madrid, Museo nacional Reina Sof�a</a>
        	<a class='enlaces' href='http://www.centrepompidou.fr' target='_blank'>Par�s, Centre Pompidou</a>
        	<a class='enlaces' href='http://www.louvre.fr' target='_blank'>Par�s, Muse� du Louvre</a>
        	<a class='enlaces' href='http://www.barbican.org.uk' target='_blank'>Londres, The Barbican</a>
        	<a class='enlaces' href='http://www.nationalgallery.org.uk' target='_blank'>Londres, The National Gallery</a>
        	<a class='enlaces' href='http://www.moma.org' target='_blank'>Nueva York, MoMA-The Museum of Modern Art</a>
       		</p>";
			break;
		}
		?>
        
		
<?php include("piecera.php");?>
