<?php include("cabecera.php");?>
		
	</td>
	<td class="celda_2">
		<script type="text/javascript">
		new fadeshow(fadeimages, 560, 157, 0, 6000, 1)
		</script>
        
		 <?php 
		switch($_SESSION["idioma"])
		{	
			case "cat": ?>
            <h1>La hand the Sergi : Compra les teves obres d�art </h1>
            <p>Tres formes senzilles  de comprar els nostres quadres:</p>
  			<p><em>1. �</em> Trucant al:<br><b>(+34)&nbsp;93.319.14.01 - (+34) 616.18.48.11</b> o contactar a <a href="mailto:sergipujol@lahandthesergi.com"><em>sergipujol@lahandthesergi.com</em></a></p>
		    <p><em>2. �</em> Si ens escriu des de fora del territori nacional i necessita transport  mar�tim, ens pregunti per les tasses portu�ries i pel servei de paqueteria  mar�tima. Nom y direcci� de dest� de l�obra.</p>
		    <p><em>3. �</em> Mitjan�ant transfer�ncia banc�ria.</p>
            <? break;
			
			case "ing": ?>
			<h1>La hand the Sergi : Buy your works of art</h1>
            <p>If you wish to buy these paintings, follow these  simple instructions: <br>
			<p><em>1. �</em> You can call  us on<br><b>(+34)&nbsp;93.319.14.01 - (+34) 616.18.48.11</b> or contact us at <a href="mailto:sergipujol@lahandthesergi.com"><em>sergipujol@lahandthesergi.com</em></a><br>
			<p><em>2. �</em> If you are  writing from outside Spain  and require sea transport, remember to ask us about the port charges and the sea  transport service. Please specify name, address and destination of work of art.</p>
			<p><em>3. �</em> Method of  payment: bank transfer:</p>
            <? break;
			
			default: ?>
            <h1>La hand the Sergi : Compra tus obras de arte</h1>
            <p>Para realizar la compra de estos cuadros, tres pasos sencillos: </p>
            <p><em>1. -</em> Nos puede llamar al<br><b>(+34)�93.319.14.01 - (+34) 616.18.48.11</b> o contactar a <a href="mailto:sergipujol@lahandthesergi.com"><em>sergipujol@lahandthesergi.com</em></a></p>
            <p><em>2. -</em> Si nos escribe desde el fuera del territorio nacional y precisa transporte mar�timo, preg�ntenos por la tasa portuaria y servicio de paqueter�a mar�tima. Nombre y direcci�n, destino de la obra.</p>
   		    <p><em>3. -</em> Datos bancarios, mediante transferencia:</p>
            <? break;
         }
		 ?>
        <p><b>OFFICE ADRESS</b><br><em>La hand the Sergi�SL.</em><br>General Manager: Mr. Sergi Pujol<br>C/Antic de Sant Joan 14-16 08003 BARCELONA</p>
		<p><b>Concept :</b> Your name + artist tittle<br>SWIFT/BIC: CESCESBBXXX - IBAN: ES0420130024460201127483</p>
		<p><b>BANK INSTITUTION :</b><br><em>Caixa Catalunya</em></b><br>Passeig del  Born 7 i 9 08003 Barcelona<br>Tel +(0034) 93.319.57.11 - Fax +(0034)93.310.65.45</p>

<?php include("piecera.php");?>