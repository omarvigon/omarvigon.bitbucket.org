<?php include("0top.php");?>
	<meta name="description" content="Mario Soria Biografia, obras de arte de Mario Soria, cuadros y dibujos del artista">
	<title>Mario Soria Biografia, pinturas y obras del artista.</title>
</head>
<body>

<table cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="celda_0" rowspan="2">
		<a href="index.php"><img src="img/logo200.gif" title="La Hand the Sergi" alt="La Hand the Sergi"></a>
	</td>
	<td class="celda_1x">
		<a class="boton" href="http://www.lahandthesergi.com/exposiciones-de-arte.php" title="Exposiciones de arte"><? echo constant("boton1_".$_SESSION["idioma"]);?>&nbsp;&nbsp;</a>
	    <a class="boton" href="http://www.lahandthesergi.com/artistas-hiperrealistas.php" title="Artistas como Mario Soria"><? echo constant("boton2_".$_SESSION["idioma"]);?>&nbsp;&nbsp;</a>
		<a class="boton" href="http://www.lahandthesergi.com/comprar-obra-de-arte.php" title="Adquiere tu obra de arte"><? echo constant("boton3_".$_SESSION["idioma"]);?>&nbsp;&nbsp;</a>
        <a class="boton" href="http://www.lahandthesergi.com/personal-orders.php" title="Encarga tu obra personalizada"><? echo constant("boton4_".$_SESSION["idioma"]);?>&nbsp;&nbsp;</a>
        <a class="boton" href="http://www.lahandthesergi.com/art-events-barcelona.php" title="Eventos de arte en Barcelona"><? echo constant("boton5");?>&nbsp;&nbsp;</a>
		<a class="boton" href="http://www.lahandthesergi.com/art-gallery-barcelona.php" title="Contacta con nuestra galeria de arte en Barcelona"><? echo constant("boton6_".$_SESSION["idioma"]);?>&nbsp;&nbsp;</a>
        <h1>&nbsp;</h1>
        <h2 align="right">Mario Soria&nbsp;&nbsp;&nbsp;&nbsp;</h2>
	</td>
	<td class="celda_2x">
		<script type="text/javascript">new fadeshow(fadeimages, 560, 157, 0, 6000, 1)</script>    	
        <h1>La hand the Sergi : Mario Soria</h1>
        <h2>
        <a href="mario-soria.php"><? echo $artis1[$_SESSION["idioma"]];?></a> &nbsp;&middot;&nbsp;
        <a href="mario-soria.php?ver=obras"><? echo $artis2[$_SESSION["idioma"]];?></a> &nbsp;&middot;&nbsp;
        <a href="mario-soria.php?ver=prensa"><? echo $artis3[$_SESSION["idioma"]];?> &nbsp;&nbsp;</a>
        </h2>
	</td>
</tr>
<tr>
	<td class="celda3" colspan="2">
    	<?php
		switch($_GET["ver"])
		{
			case "obras":
			echo "<table cellspacing='0' cellpadding='0'>
			<tr>
				<td colspan='6'>&nbsp;</td>
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/angel',2)><img src='mariosoria/angel_mini.jpg' alt='Mario Soria Angel' title='Mario Soria Angel'></a></td>
				<td class='info'><b>Angel �Rafael-</b><br>130x84cm. 2006<br>".$oleo_madera[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/angel2',0)><img src='mariosoria/angel2_mini.jpg' alt='Mario Soria arbol angel' title='Mario Soria angel'></a></td>
				<td class='info'><b>Angel -Rafael-</b><br>92x55cm. 2007<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/arboles',3)><img src='mariosoria/arboles_mini.jpg' alt='Mario Soria arboles' title='Mario Soria arboles'></a></td>
				<td class='info'><b>Bodeg�n Arboles</b><br>90x90cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>	
				<td class='pic'><a href=javascript:popup2('mariosoria/sarkozy',3)><img src='mariosoria/sarkozy_mini.jpg' alt='Mario Soria sarkozy' title='Mario Soria sarkozy'></a></td>
				<td class='info'><b>Sarkozy</b><br>130x50cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/cinemarilyn',2)><img src='mariosoria/cinemarilyn_mini.jpg' alt='Mario Soria cine marilyn' title='Mario Soria cine marilyn'></a></td>
				<td class='info'><b>Bodeg�n cine Marilyn</b><br>67x130cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/colacao',0)><img src='mariosoria/colacao_mini.jpg' alt='Mario Soria colacao' title='Mario Soria colacao'></a></td>
				<td class='info'><b>A la salida de Luna Park</b><br>90x43,7cm. 2004. Cuadro catalogado<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/conseller',2)><img src='mariosoria/conseller_mini.jpg' alt='Mario Soria conseller' title='Mario Soria conseller'></a></td>
				<td class='info'><b>Bodeg�n Conseller</b><br>97x91cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/dali',0)><img src='mariosoria/dali_mini.jpg' alt='Mario Soria dali' title='Mario Soria dali'></a></td>
				<td class='info'><b>Dal�</b><br>80x54,4cm. 2003<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
			</tr>
			<tr>	
				<td class='pic'><a href=javascript:popup2('mariosoria/diarioandorra',3)><img src='mariosoria/diarioandorra_mini.jpg' alt='Mario Soria diario andorra' title='Mario Soria diario andorra'></a></td>
				<td class='info'><b>Diari d�Andorra</b><br>130x50cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/frida',2)><img src='mariosoria/frida_mini.jpg' alt='Mario Soria Frida' title='Mario Soria Frida'></a></td>
				<td class='info'><b>Frida Khalo</b><br>130x73cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>	
				<td class='pic'><a href=javascript:popup2('mariosoria/gioconda',3)><img src='mariosoria/gioconda_mini.jpg' alt='Mario Soria gioconda' title='Mario Soria gioconda'></a></td>
				<td class='info'><b>Gioconda</b><br>R�plica surrealista Leonardo. 1998 al 2006. ".$obra_maestra[$_SESSION["idioma"]]."<br>".$oleo_madera[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/gitanes',1)><img src='mariosoria/gitanes_mini.jpg' alt='Mario Soria gitanes' title='Mario Soria gitanes'></a></td>
				<td class='info'><b>Gitanes</b><br>61x38cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/hombregirasol',6)><img src='mariosoria/hombregirasol_mini.jpg' alt='Mario Soria hombre girasol' title='Mario Soria hombre girasol'></a></td>
				<td class='info'><b>Hombre Girasol</b><br>116x73cm. 2006<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/mariosoria1',0)><img src='mariosoria/mariosoria1_mini.jpg' alt='Mario Soria Vanguardia' title='Mario Soria Vanguardia'></a></td>
				<td class='info'><b>Vanguardia cow parade</b><br>64x48cm. 2005<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/mariosoria2',3)><img src='mariosoria/mariosoria2_mini.jpg' alt='Mario Soria Audrey Hepburn' title='Mario Soria Audrey Hepburn'></a></td>
				<td class='info'><b>Audrey Hepburn</b><br>130x50cm. 2006. Cuadro Catalogado<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/menina',2)><img src='mariosoria/menina_mini.jpg' alt='Mario Soria menina' title='Mario Soria menina'></a></td>
				<td class='info'><b>Infanta Margarita</b><br>162x114cm. 2004<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
			</tr>
			<tr>	
				<td class='pic'><a href=javascript:popup2('mariosoria/pezcaballo',3)><img src='mariosoria/pezcaballo_mini.jpg' alt='Mario Soria pez caballo' title='Mario Soria pez caballo'></a></td>
				<td class='info'><b>Pez Caballo</b><br>110x85cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/pez',0)><img src='mariosoria/pez_mini.jpg' alt='Mario Soria pez' title='Mario Soria pez'></a></td>
				<td class='info'><b>Pez Caballo</b><br>110x85cm. 2007<br>".$dibujo_papel[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/vacas',2)><img src='mariosoria/vacas_mini.jpg' alt='Mario Soria Vacas voladoras' title='Mario Soria Vacas voladoras'></a></td>
				<td class='info'><b>Vacas voladoras</b><br>100x100cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/puente',2)><img src='mariosoria/puente_mini.jpg' alt='Mario Soria puente' title='Mario Soria puente'></a></td>
				<td class='info'><b>Puente</b><br>110x90cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/agua',0)><img src='mariosoria/agua_mini.jpg' alt='Mario Soria agua' title='Mario Soria agua'></a></td>
				<td class='info'><b>Agua</b><br>65x65cm.2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/angel_rojo',2)><img src='mariosoria/angel_rojo_mini.jpg' alt='Mario Soria angel rojo' title='Mario Soria angel rojo'></a></td>
				<td class='info'><b>Angel Rojo</b><br>170x80cm. 2001<br>".$oleo_acrilico[$_SESSION["idioma"]]." sin bastidor. Cuadro Catalogado.<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/aspirina_gallo',0)><img src='mariosoria/aspirina_gallo_mini.jpg' alt='Mario Soria aspirina gallo' title='Mario Soria aspirina gallo'></a></td>
				<td class='info'><b>Aspirina Gallo</b><br>90x90cm. 2006<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/aviador',0)><img src='mariosoria/aviador_mini.jpg' alt='Mario Soria aviador' title='Mario Soria aviador'></a></td>
				<td class='info'><b>Aviador</b><br>65x80cm. 2008 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/bodegon',0)><img src='mariosoria/bodegon_mini.jpg' alt='Mario Soria bodegon' title='Mario Soria bodegon'></a></td>
				<td class='info'><b>Bodegon</b><br>64x50cm. 2005.<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/bodegon_lapices',1)><img src='mariosoria/bodegon_lapices_mini.jpg' alt='Mario Soria Bodegon Lapices' title='Mario Soria Bodegon Lapices'></a></td>
				<td class='info'><b>Bodegon Lapices</b><br>46x38cm. 2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/bruguer',0)><img src='mariosoria/bruguer_mini.jpg' alt='Mario Soria bruguer' title='Mario Soria bruguer'></a></td>
				<td class='info'><b>Bruguer</b><br>40x40cm. 2004�<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/chequetax',0)><img src='mariosoria/chequetax_mini.jpg' alt='Mario Soria Hombre de espaldas' title='Mario Soria Hombre de espaldas'></a></td>
				<td class='info'><b>Hombre de espaldas</b><br>130x97cm. 1998<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/colacao_2',1)><img src='mariosoria/colacao_2_mini.jpg' alt='Mario Soria colacao' title='Mario Soria colacao'></a></td>
				<td class='info'><b>Colacao</b><br>47x55cm. 2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/crucificado',0)><img src='mariosoria/crucificado_mini.jpg' alt='Mario Soria crucificado' title='Mario Soria crucificado'></a></td>
				<td class='info'><b>Crucificado</b><br>90x75cm. 2000<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/danone',1)><img src='mariosoria/danone_mini.jpg' alt='Mario Soria danone' title='Mario Soria danone'></a></td>
				<td class='info'><b>Danone</b><br>55x56cm. 2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/dedo_ala',0)><img src='mariosoria/dedo_ala_mini.jpg' alt='Mario Soria dedo ala' title='Mario Soria el ala'></a></td>
				<td class='info'><b>El Ala</b><br>55x46cm. 2006<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><em>".$vsi[$_SESSION["idioma"]]."</em></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/drogueria',0)><img src='mariosoria/drogueria_mini.jpg' alt='Mario Soria drogueria' title='Mario Soria drogueria'></a></td>
				<td class='info'><b>Drogueria</b><br>100x62. 2002<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/infanta_mariacristina',4)><img src='mariosoria/infanta_mariacristina_mini.jpg' alt='Mario Soria infanta mariacristina' title='Mario Soria infanta mariacristina'></a></td>
				<td class='info'><b>Infanta Maria Cristina</b><br>131x97cm. 2002<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/infantax',0)><img src='mariosoria/infantax_mini.jpg' alt='Mario Soria infanta' title='Mario Soria infanta'></a></td>
				<td class='info'><b>Infantax</b><br>130x90cm. 1999 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/mano_corcho',3)><img src='mariosoria/mano_corcho_mini.jpg' alt='Mario Soria mano corcho' title='Mario Soria mano corcho'></a></td>
				<td class='info'><b>Mano Corcho</b><br>130x19,5cm. 2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/mano_derecha',0)><img src='mariosoria/mano_derecha_mini.jpg' alt='Mario Soria mano derecha' title='Mario Soria mano derecha'></a></td>
				<td class='info'><b>Mano Derecha</b><br>55x33cm. 2000<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><em>".$vsi[$_SESSION["idioma"]]."</em></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/mano_izquierda',1)><img src='mariosoria/mano_izquierda_mini.jpg' alt='Mario Soria mano izquierda' title='Mario Soria mano izquierda'></a></td>
				<td class='info'><b>Mano Izquierda</b><br>55x33cm. 2000<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/maquina_escribir')><img src='mariosoria/maquina_escribir_mini.jpg' alt='Mario Soria maquina escribir' title='Mario Soria maquina escribir'></a></td>
				<td class='info'><b>Maquina Escribir</b><br>100x100cm. 2006.<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/marilyn',2)><img src='mariosoria/marilyn_mini.jpg' alt='Mario Soria marilyn' title='Mario Soria marilyn'></a></td>
				<td class='info'><b>Marilyn</b><br>130x35,5cm. 2006<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/melon',1)><img src='mariosoria/melon_mini.jpg' alt='Mario Soria melon' title='Mario Soria melon'></a></td>
				<td class='info'><b>Melon</b><br>92x74cm.2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/menina_2',0)><img src='mariosoria/menina_2_mini.jpg' alt='Mario Soria menina' title='Mario Soria menina'></a></td>
				<td class='info'><b>Menina</b><br>73x102cm. 1999 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/montserrat',2)><img src='mariosoria/montserrat_mini.jpg' alt='Mario Soria montserrat' title='Mario Soria montserrat'></a></td>
				<td class='info'><b>Montserrat</b><br>180x45cm.2004<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/oleo',0)><img src='mariosoria/oleo_mini.jpg' alt='Mario Soria Titan sota' title='Mario Soria Titan sota'></a></td>
				<td class='info'><b>Titan sota</b><br>46x38cm. 2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/pan_dali',0)><img src='mariosoria/pan_dali_mini.jpg' alt='Mario Soria pan dali' title='Mario Soria pan dali'></a></td>
				<td class='info'><b>Pan Dali</b><br>65x82cm. 2002 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/papeplespegados',0)><img src='mariosoria/papeplespegados_mini.jpg' alt='Mario Soria papeles pegados' title='Mario Soria papeles pegados'></a></td>
				<td class='info'><b>Papeles pegados</b><br>64x48cm.2004<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/pez_besugo',0)><img src='mariosoria/pez_besugo_mini.jpg' alt='Mario Soria pez besugo' title='Mario Soria pez besugo'></a></td>
				<td class='info'><b>Pez Besugo</b><br>55x46cm. 2002 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/pez_caracol',0)><img src='mariosoria/pez_caracol_mini.jpg' alt='Mario Soria pez caracol' title='Mario Soria pez caracol'></a></td>
				<td class='info'><b>Pez Caracol</b><br>46x38cm.2004<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/pez_cocacola',0)><img src='mariosoria/pez_cocacola_mini.jpg' alt='Mario Soriacocacola ' title='Mario Soria cocacola'></a></td>
				<td class='info'><b>Pez Cocacola</b><br>61x38cm. 2004<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/pez_mano',0)><img src='mariosoria/pez_mano_mini.jpg' alt='Mario Soria pez mano' title='Mario Soria pez mano'></a></td>
				<td class='info'><b>Pez Mano</b><br>61x50cm<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br>2004.<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/pez_oreja',0)><img src='mariosoria/pez_oreja_mini.jpg' alt='Mario Soria pez oreja' title='Mario Soria pez oreja'></a></td>
				<td class='info'><b>Pez Oreja</b><br>46x38cm. 2004 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/puleva',2)><img src='mariosoria/puleva_mini.jpg' alt='Mario Soria puleva' title='Mario Soria puleva'></a></td>
				<td class='info'><b>Puleva</b><br>92x73cm. 2003<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>	
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/teclado',4)><img src='mariosoria/teclado_mini.jpg' alt='Mario Soria teclado' title='Mario Soria teclado'></a></td>
				<td class='info'><b>Teclado</b><br>295x90cm. 2001 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/tintin',2)><img src='mariosoria/tintin_mini.jpg' alt='Mario Soria tintin' title='Mario Soria tintin'></a></td>
				<td class='info'><b>Tintin</b><br>125x55cm. 2001 <br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/tintin_mano',0)><img src='mariosoria/tintin_mano_mini.jpg' alt='Mario Soria tintin' title='Mario Soria tintin'></a></td>
				<td class='info'><b>Tintin Mano</b><br>136x20cm. 2001 <br>".$oleo_madera[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/casa',2)><img src='mariosoria/casa_mini.jpg' alt='Mario Soria casa' title='Mario Soria casa'></a></td>
				<td class='info'><b>Casa Andorra</b><br>67x90cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/periodico',0)><img src='mariosoria/periodico_mini.jpg' alt='Mario Soria periodico' title='Mario Soria periodico'></a></td>
				<td class='info'><b>Periodico Solis</b><br>65x65cm. 2005<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/11s2001',4)><img src='mariosoria/11s2001_mini.jpg' alt='Mario Soria 11s' title='Mario Soria 11s'></a></td>
				<td class='info'><b>11 S 2001</b><br>116x73cm.2002.<br>".$oleo_acrilico[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/conseller1',0)><img src='mariosoria/conseller1_mini.jpg' alt='Mario Soria conseller' title='Mario Soria conseller'></a></td>
				<td class='info'><b>Conseller</b><br>109x28cm. 2007<br>".$dibujo_papel[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/conseller2',3)><img src='mariosoria/conseller2_mini.jpg' alt='Mario Soria conseller' title='Mario Soria conseller'></a></td>
				<td class='info'><b>Conseller</b><br>130x50cm. 2007<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/arbolbotella',0)><img src='mariosoria/arbolbotella_mini.jpg' alt='Mario Soria arbol botella' title='Mario Soria arbol botella'></a></td>
				<td class='info'><b>Arbol-botella</b><br>63x51cm. 2007<br>".$dibujo_papel[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/menina_3',2)><img src='mariosoria/menina_3_mini.jpg' alt='Mario Soria Menina' title='Mario Soria Menina'></a></td>
				<td class='info'><b>Menina</b><br>120x90cm. 2007<br>".$oleo_madera[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/audreyhepburn',1)><img src='mariosoria/audreyhepburn_mini.jpg' alt='Mario Soria Audrey Hepburn' title='Mario Soria Audrey Hepburn'></a></td>
				<td class='info'><b>Audrey Hepburn</b><br>2006<br>".$oleo_tela[$_SESSION["idioma"]]."<br><span>".$vno[$_SESSION["idioma"]]."</span></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/marlene',3)><img src='mariosoria/marlene_mini.jpg' alt='Marlene Dietreich Mario Soria' title='Marlene Dietreich Mario Soria'></a></td>
				<td class='info'><b>Marlene Dietrich</b><br>130x35.5cm 2008<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
			</tr>
			<tr>
				<td class='pic'><a href=javascript:popup2('mariosoria/kennedy',0)><img src='mariosoria/kennedy_mini.jpg' alt='JF Kennedy Mario Soria' title='JF Kennedy Mario Soria'></a></td>
				<td class='info'><b>J.F. Kennedy</b><br>143x28cm 2008<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/casa_andorra',0)><img src='mariosoria/casa_andorra_mini.jpg' alt='Casa Andorra Soria' title='Casa Andorra Mario Soria'></a></td>
				<td class='info'><b>Casa Andorra</b><br>81x97cm 2008<br>".$oleo_tela[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><span>".$vno[$_SESSION["idioma"]]."</span></a></td>
				<td class='pic'><a href=javascript:popup2('mariosoria/mickymouse',0)><img src='mariosoria/mickymouse_mini.jpg' alt='Mickey Mouse  Soria' title='Mickey Mouse Mario Soria'></a></td>
				<td class='info'><b>Mickey Mouse Gioconda</b><br>75x120cm 2008<br>".$dibujo_papel[$_SESSION["idioma"]]."<br><a href='comprar_obra_de_arte.php'><em>".$vsi[$_SESSION["idioma"]]."</em></a></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			</table>";
			break;
			
			case "prensa":
			echo "<table cellspacing='0' cellpadding='0'>
			<tr><td class='bordeazul'>&nbsp;</td><td>
			<p><img src='img/pdf.gif' align='absmiddle'> <a href='prensa/andorra_mariosoria.pdf' target='_blank'>Periodic d'Andorra 27-Sept-2007</a></p>
			<p><img src='img/pdf.gif' align='absmiddle'> <a href='prensa/artexpresions_mariosoria.pdf' target='_blank'>Art Expressions Magazine May-1996</a></p>
			<p><img src='img/pdf.gif' align='absmiddle'> <a href='prensa/guiaeuropea_mariosoria.pdf' target='_blank'>Guia Europea de Bellas Artes 1995-1996</a></p>
			<p><img src='img/pdf.gif' align='absmiddle'> <a href='prensa/macuf2004.pdf' target='_blank'>MACUF 2004</a></p>
			<td></tr></table>";
			break;
		
			default:
			if($_SESSION["idioma"]=="ing")
			{
				echo "<table cellspacing='0' cellpadding='0'>
				<tr><td class='bordeazul'>&nbsp;</td><td>
				<p><b>MARIO SORIA</b> - Barcelona, 1968.</p><p>
				<b>1989</b>: Collective Exhibition Sala Par&eacute;s. Barcelona<br>
				<b>1990</b>: Collective Exhibition Sala Gaud&iacute;. Madrid<br>
				<b>1992</b>: Selected artist at the 1st Bienal de Martorell. Barcelona<br>
				<b>1993</b>: Individual Exhibition Sala Villarroel Teatre. Barcelona.<br>&nbsp;Individual Exhibition Sala Conex. Barcelona.<br>
				<b>1994</b>: Individual Exhibition Aula de Cultura Ayuntamiento de  Bullas. Murcia.<br>
				<b>1995</b>: Individual Exhibition Aula de Cultura Ayuntamiento de  Bullas. Murcia.<br>
				Collective Exhibition Centro Cultural la Santa. Barcelona.<br>
				Collective Exhibition Centro de Arte Conxa L&oacute;pez. Tarragona.<br>
				Collective Exhibition Galer&iacute;a de Arte La Aurora. Murcia.<br>
				<b>199</b><b>6</b>: Collective Exhibition Galer&iacute;a de Arte La Aurora. Murcia.<br>
				Collective Exhibition Centro Cultural La Santa. Barcelona.<br>
				Collective Exhibition Homage to art critic Agust&iacute;n Romeo. Galer&iacute;a Xeito. Madrid.<br>
				Collective Exhibition Galer&iacute;a de Arte Villanueva. Seville.<br>
				Collective Exhibition Golla   Art Gallery.  New York.<br>
				Artexpo New York 96. International Art Fair. New York.<br>
				Artexpo 96. International Art Fair. Barcelona.<br>
				Interart 96. International Art Fair. Valencia.<br>
				<b>1997</b>: Artexpo 97. International Art Fair. Barcelona.<br>
				Interart 97. International Art Fair. Valencia.<br>
				NICAF&rsquo;97. International Art Fair. Tokyo.<br>
				Individual Exhibition Centro Cultural La Santa. Barcelona.<br>
				Collective Exhibition Galer&iacute;a Principal / Montcada. Barcelona.<br>
				Collective Exhibition Galer&iacute;a Beaskoa. Barcelona.<br>
				Christmas Collective Exhibition, Spanish Realists. Galer&iacute;a La Aurora. Murcia.<br>
				<b>199</b><b>8</b>: Christmas Collective Exhibition Galer&iacute;a  Beaskoa. Barcelona.<br>
				Collective Exhibition Centro Cultural La Santa. Barcelona. <br>
				<b>1999</b>: &ldquo;Michelin&rdquo; Collective Exhibition. Convento de San Agust&iacute;n. Barcelona.<br>
				Summer Collective Exhibition Galer&iacute;a Rose Selavy. Barcelona.<br>
				<b>2000</b>: Collective Exhibition Galer&iacute;a Beaskoa. Barcelona.<br>
				Artexpo New York  00. Collective Exhibition Galer&iacute;a Beaskoa. <br>
				<b>2001</b>: Collective Exhibition Galer&iacute;a Beaskoa. Barcelona.<br>
				<b>2002/2003</b>: No exhibitions. Years devoted to Painting Study and Research. <br>
				<b>2004</b>: Selected artist VII Mostra Uni&oacute;n Fenosa, MACUF, A Coru&ntilde;a.  Individual&nbsp; Exhibition of Paintings and  Drawings, Galer&iacute;a Beaskoa. Barcelona.<br>
				<b>2005/2006</b>: No exhibitions. Years devoted to Painting Study and Research.<br>
				<b>2007</b>: Collective Exhibition Galer&iacute;a Beaskoa. Barcelona.&nbsp; Individual Exhibition at Galer&iacute;a La hand the  Sergi &amp; Galer&iacute;a Mamma Maria. Andorra&nbsp;la Vella.<br>
				<b>2008</b>: Europ'ART 08 Geneva. Individual exhibition at Galer&iacute;a La hand the Sergi. </p>	
				<td></tr></table>";
			}
			else
			{
				echo "<table cellspacing='0' cellpadding='0'>
				<tr><td class='bordeazul'>&nbsp;</td><td>
				<p><b>MARIO SORIA</b> - Barcelona, 1968.</p><p>
				<b>1989</b>: Colectiva Sala Par�s.Barcelona<br>
				<b>1990</b>: Colectiva Sala Gaud�. Madrid<br>
				<b>1992</b>: Artista seleccionado 1� Bienal de Martorell. Barcelona<br>
				<b>1993</b>: Individual Sala Villarroel  Teatre. Barcelona.<br>
				Individual Sala Conex. Barcelona.<br>
				<b>1994</b>: Individual Aula de Cultura Ayuntamiento de Bullas. Murcia.<br>
				<b>1995</b>: Individual Aula de Cultura Ayuntamiento de Bullas. Murcia.<br>
				Colectiva Centro Cultural la Santa. Barcelona.<br>
				Colectiva Centro de Arte Conxa L�pez. Tarragona.<br>
				Colectiva Galer�a de Arte La Aurora. Murcia.<br>
				<b>199</b><b>6</b>: Colectiva Galer�a de Arte La Aurora. Murcia.<br>
				Colectiva Centro Cultural La Santa. Barcelona.<br>
				Colectiva Homenaje al cr�tico de arte Agust�n Romeo. Galer�a  Xeito. Madrid.<br>
				Colectiva Galer�a de Arte Villanueva. Sevilla.<br>
				Colectiva Galer�a de Arte Golla. New York.<br>
				Artexpo New York 96. Feria de Arte Internacional. New York.<br>
				Artexpo 96. Feria Internacional de Arte. Barcelona.<br>
				Interart 96. Feria Internacional de Arte. Valencia.<br>
				<b>1997</b>: Artexpo 97. Feria Internacional de Arte. Barcelona.<br>
				Interart 97. Feria&nbsp; Internacional de Arte. Valencia.<br>
				NICAF�97. Feria Internacional de Arte. Tokio.<br>
				Individual Centro Cultural La Santa. Barcelona.<br>
				Colectiva Galer�a Principal / Montcada. Barcelona.<br>
				Colectiva Galer�a Beaskoa. Barcelona.<br>
				Colectiva Navidad Realistas Espa�oles Galer�a La Aurora. Murcia.<br>
				<b>199</b><b>8</b>: Colectiva Navidad Galer�a Beaskoa.  Barcelona.<br>
				Colectiva Centro Cultural La Santa. Barcelona. <br>
				<b>1999</b>: Colectiva 'Michelin'. Convento de San Agust�n. Barcelona.<br>
				Colectiva de verano Galer�a Rose Selavy. Barcelona.<br>
				<b>2000</b>: Colectiva Galer�a Beaskoa. Barcelona.<br>
				Artexpo New York 00. Colectiva Galer�a Beaskoa. <br>
				<b>2001</b>: Colectiva Galer�a Beaskoa. Barcelona.<br>
				<b>2002/2003</b>: Sin exposiciones.<br>
				A&ntilde;os dedicados al Estudio e Investigaci�n Pict�rica.<br>
				<b>2004</b>: Artista seleccionado VII Mostra Uni�n Fenosa, MACUF, A Coru�a.  Exposici�n Individual Pinturas y Dibujos Galer�a Beaskoa. Barcelona.<br>
				<b>2005/2006</b>: Sin exposiciones. A&ntilde;os dedicados al Estudio e Investigaci�n  Pict�rica.<br>
				<b>2007</b>: Colectiva Galer�a Beaskoa. Barcelona. Exposici�n Individual  Galer�a La hand the Sergi&amp;Galer�a Mama Maria. Andorra&nbsp; la Vella.<br>
				<b>2008</b>: Europ'ART 08 Ginebra. Individual con galer&iacute;a La hand the Sergi.</p>
				<td></tr></table>";
			}
			break;		
		}
		?>
    </td>
</tr>
</table>

</body>
</html>