<?php include("cabecera.php");?>
		
        <a class="boton" href="exposiciones-de-arte.php?expo=switzerland2008">Switzerland 2008&nbsp;&nbsp;</a>
        <a class="boton" href="exposiciones-de-arte.php?expo=andorra2007">Andorra 2007&nbsp;&nbsp;</a>
	</td>
<td class="celda_2">
		<script type="text/javascript">
		new fadeshow(fadeimages, 560, 157, 0, 6000, 1)
		</script>
        <?php 
		switch($_SESSION["idioma"])
		{	
			case "cat": echo "<h1>La hand the Sergi : Exposicions d'Art</h1>";		break;
			case "ing": echo "<h1>La hand the Sergi : Exhibitions</h1>";			break;
			default:    echo "<h1>La hand the Sergi : Exposiciones de Arte</h1>";	break;
		}
		?>
        <?php 
		switch($_GET["expo"])
		{
			case "andorra2007":
			echo "<p><b>ANDORRA 2007</b></p>
            <p>
            <a href=javascript:popup('img/expo_andorra1')><img title='Exposicion Andorra' src='img/expo_andorra1mini.jpg' alt='Expo Andorra'></a>
            <a href=javascript:popup('img/expo_andorra2')><img title='Exposicion Andorra' src='img/expo_andorra2mini.jpg' alt='Expo Andorra' hspace='2'></a>
            <a href=javascript:popup('img/expo_andorra3')><img title='Exposicion Andorra' src='img/expo_andorra3mini.jpg' alt='Expo Andorra'></a>
            <a href=javascript:popup('img/expo_andorra4')><img title='Exposicion Andorra' src='img/expo_andorra4mini.jpg' alt='Expo Andorra' hspace='2'></a>
            <a href=javascript:popup('img/expo_andorra6')><img title='Exposicion Andorra' src='img/expo_andorra6mini.jpg' alt='Expo Andorra'></a>
            <a href=javascript:popup('img/expo_andorra7')><img title='Exposicion Andorra' src='img/expo_andorra7mini.jpg' alt='Expo Andorra'></a>
            </p>";
            switch($_SESSION["idioma"])
			{	
				case "cat": ?>
				<p><em>La Galeria La hand the Sergi</em>, en col�laboraci� amb la Galeria Mamma Maria, esposa a  Andorra la Vella,  entre el <b>28 de setembre y el 24 de desembre de 2007</b> la col�lecci� &ldquo;Entre la  realitat i la ficci�&ldquo;, obra de l�artista <b>Mario Soria</b>.</p>
				<p>Les quasi vint obres que formen part de la  col�lecci� es caracteritzen per unir en un mateix espai �el llen�- elements que  pertanyen a diferents corrents pict�riques, que abasten des del pop-art, el freak  o el kitsch, passant per l�hiperrealisme i el surrealisme, recurrents a tota  l�obra de l�autor. �s freq�ent, a m�s, als quadres d�aquesta col�lecci� la  pres�ncia de personatges coneguts, alguns d�ells elevats a la categoria de mite,  com �s el cas de l�actriu Marilyn Monroe o la pintora Frida Kahlo, aix� com el  president franc�s Nicolas Sarkozy o una de les �meninas� de Vel�zquez.  L�exposici� posa un especial inter�s en la selecci� de detalls t�picament  andorrans, ja que l�artista ha produ�t la vintena de quadres especialment per  aquesta exposici�.</p>
				<p>El dia de la seva inauguraci� i vernissage, durant la presentaci� de la  col�lecci�, Mario Soria va manifestar que havis estat treballant al llarg de  m�s d�un any en aquesta col�lecci� i que amb ella trenca dos anys de silenci a  les galeries. Soria va explicar que treballa principalment per enc�rrec i que  va necessitar temps i meditaci� per anunciar aquesta obra. En relaci� al pa�s,  apareixen el pont de Par�s, la imatge del conseller, que utilitza una empresa  de caf�s andorrana y la portada del <i>Diari  d�Andorra</i>. Mario es defineix d��observador� y no es descriu a s� mateix en  termes d�artista cr�tic.</p>
				<? break;
				
				case "ing": ?>
				<p><em>La hand the Sergi gallery</em>, in collaboration with Mamma  Mar�a Gallery, exhibits in Andorra  la Vella, from <b>September 28 to December 24 2007</b> the collection &ldquo;Entre la realitat i la ficci�&ldquo;  (Between fiction and reality), the work of artist Mario Soria. </p>
				<p>The collection contains approximately twenty works, all  of which bring together in one same space�the canvas- elements belonging to  different painting styles, from Pop Art to Freak or Kitsch, through  Hyper-realism and Surrealism, which are recurrent styles in the artist�s work. The  collection also includes many well-known characters, some having reached a  mythical status, such as actress Marilyn Monroe or painter Frida Kahlo, and  others such as French president Nicolas Sarkozy and one of Vel�zquez�s <i>Meninas</i>. The exhibition highlights some  typically Andorran details, as the artist has produced these works specifically  for this exhibition.</p>
				<p>On the day of the opening and <i>vernissage</i>, during  the presentation of the collection, Mario Soria said that he had been working  for more than one year on these paintings, and that with it he breaks two years  of silence in the galleries. Soria explained that he mostly works by  commission, and that he needed time and meditation to announce this work. As  references to Andorra,  we see the Paris  bridge, the <i>conseller</i> image,  used by an Andorran coffee company, and the front page of the Diari de Andorra  newspaper. Mario defines himself as an &quot;observer&quot; and does not think  of himself as a critical artist.</p>
				<? break;
				
				default: ?>
				<p><em>La galer&iacute;a La hand  the Sergi</em>, en colaboraci&oacute;n con la Galer&iacute;a Mamma Mar&iacute;a, expone en Andorra la  Vella, entre el <b>28 de septiembre  y el 24 de diciembre de 2007 </b> la colecci&oacute;n  &ldquo;Entre la realitat y la ficci&oacute;&rdquo;, obra del artista <b>Mario Soria</b>. </p>
				<p>Las casi veinte  obras que constituyen la colecci&oacute;n se caracterizan por aunar en un mismo  espacio &ndash;el lienzo- elementos pertenecientes a distintas corrientes pict&oacute;ricas,  que abarcan desde el pop-art, el freak o el kitsch, pasando por el  hiperrealismo y el surrealismo, recurrentes en toda la obra del autor. Es  frecuente tambi&eacute;n en los cuadros de esta colecci&oacute;n la presencia de personajes conocidos,  algunos de ellos elevados a la categor&iacute;a de mito, como es el caso de la actriz  Marilyn Monroe o la pintora Frida Kahlo, o el presidente franc&eacute;s Nicol&aacute;s  Sarkozy o una de las meninas de Vel&aacute;zquez. La exposici&oacute;n se detiene en la  selecci&oacute;n de detalles t&iacute;picamente andorranos, ya que el artista ha producido la  veintena de cuadros espec&iacute;ficamente para la exposici&oacute;n. </p>
				<p>El d&iacute;a de la  inauguraci&oacute;n y vernissage,  durante la presentaci&oacute;n de la colecci&oacute;n, Mario Soria manifest&oacute; que hab&iacute;a estado  trabajando durante m&aacute;s de un a&ntilde;o en la presente colecci&oacute;n y que con ella rompe  dos a&ntilde;os de silencio en las galer&iacute;as. Soria explic&oacute; que trabaja b&aacute;sicamente por  encargo y que necesit&oacute; tiempo y meditaci&oacute;n para anunciar esta obra. En relaci&oacute;n  al pa&iacute;s, aparecen el puente de Par&iacute;s, la imagen del conseller, que utiliza una empresa de caf&eacute;s  andorrana y la misma portada del <i>Diari de Andorra</i>. Mario se define como un  &quot;observador&quot; y no se describe a s&iacute; mismo en t&eacute;rminos de artista  cr&iacute;tico.</p>
				<? break;
         	}
			break;
			
			default: ?>
			<p><b>SWITZERLAND (GEN&Egrave;VE) 2008</b></p>
			<p>
            <a href="javascript:popup('img/expo_suiza1')"><img title='EuropArt' src='img/expo_suiza1mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup_alta('img/expo_suiza2')"><img title='EuropArt' src='img/expo_suiza2mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup_alta('img/expo_suiza3')"><img title='EuropArt' src='img/expo_suiza3mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza4')"><img title='EuropArt' src='img/expo_suiza4mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza5')"><img title='EuropArt' src='img/expo_suiza5mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza6')"><img title='EuropArt' src='img/expo_suiza6mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza7')"><img title='EuropArt' src='img/expo_suiza7mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza8')"><img title='EuropArt' src='img/expo_suiza8mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza9')"><img title='EuropArt' src='img/expo_suiza9mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza10')"><img title='EuropArt' src='img/expo_suiza10mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza11')"><img title='EuropArt' src='img/expo_suiza11mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza12')"><img title='EuropArt' src='img/expo_suiza12mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza13')"><img title='EuropArt' src='img/expo_suiza13mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza14')"><img title='EuropArt' src='img/expo_suiza14mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza15')"><img title='EuropArt' src='img/expo_suiza15mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza16')"><img title='EuropArt' src='img/expo_suiza16mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup_alta('img/expo_suiza17')"><img title='EuropArt' src='img/expo_suiza17mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup_alta('img/expo_suiza18')"><img title='EuropArt' src='img/expo_suiza18mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup('img/expo_suiza19')"><img title='EuropArt' src='img/expo_suiza19mini.jpg' alt='Expo Suiza 2008'></a>
            <a href="javascript:popup_alta('img/expo_suiza20')"><img title='EuropArt' src='img/expo_suiza20mini.jpg' alt='Expo Suiza 2008' hspace="2"></a>            
            <a href="javascript:popup('img/expo_suiza21')"><img title='EuropArt' src='img/expo_suiza21mini.jpg' alt='Expo Suiza 2008'></a>                        
            </p>
            <?php switch($_SESSION["idioma"])
			{
				case "cat": ?>
                <p>El stand de l'exposici&oacute; de Ginebra va ser un &egrave;xit de p&uacute;blic. Ressaltarem personalitats que ens van visitar; el c&ograve;nsol d'Espanya en Ginebra, D. Antonio Garc&iacute;a Abad aix&iacute; com el m&agrave;xim representant de l'Institut Cervantes d'aquesta ciutat. Acad&egrave;mics i personalitats del m&oacute;n cultural i empresarial. El dia 1 de maig es va celebrar una confer&egrave;ncia-col&middot;loqui a c&agrave;rrec de D. Vicente Molina Foix, premi Cervantes de Narrativa 2007, per la seva novel&middot;la &ldquo;El tallapapers&rdquo; a l'acte del qual assistim. M&eacute;s tard els assistents a l'acte van acudir a visitar la nostra mostra d'art contemporani sobre Mario Soria, i van quedar gratament sorpresos per la qualitat de l'obra exposada.</p>
				<? break;
				
				case "ing": ?>
				<p>The booth of the exhibition in Geneva was a success for the public. Resaltar personalities who visited us, the consul of Spain in Geneva, D. Antonio Garcia Abad well as the highest representative of the Cervantes Institute in that city. Academicians and personalities of world culture and business. The day on May 1 was held a conference-conference by Mr. Vincent Molina Foix, Narrative 2007 Cervantes prize, for his novel "The cardboard cutters" whose act witnessed. Later those attending the event came to visit our sample of contemporary art on Mario Soria, and were pleasantly surprised by the quality of the work exposed.</p>
				<? break;
				
				default: ?>
				<p>El stand de la exposici&oacute;n de Ginebra fue un &eacute;xito de  p&uacute;blico. Resaltaremos personalidades que nos visitaron; el c&oacute;nsul de Espa&ntilde;a en  Ginebra, D. Antonio Garc&iacute;a Abad as&iacute; como el m&aacute;ximo representante del Instituto  Cervantes de dicha ciudad. Acad&eacute;micos y personalidades del mundo cultural y  empresarial. El dia 1 de mayo se celebr&oacute; una conferencia-coloquio a cargo de D.  Vicente Molina Foix, premio Cervantes de Narrativa 2007, por su novela &ldquo;El  abrecartas&rdquo; a cuyo acto asistimos. M&aacute;s tarde los asistentes al acto acudieron a  visitar nuestra muestra de arte contempor&aacute;neo sobre Mario&nbsp; Soria, y quedaron gratamente sorprendidos por  la calidad de la obra expuesta. </p>
				<? break;
            }?>
            <p><img src='img/pdf.gif' align='absmiddle'> <a href='prensa/genevart.pdf' target='_blank'>PDF Europ'Art Geneve</a></p>
			<? break;
		}
		?>

<?php include("piecera.php");?>