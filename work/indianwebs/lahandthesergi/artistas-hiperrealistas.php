<?php include("cabecera.php");?>
		
		<a class="boton" href="mario-soria.php">Mario Soria&nbsp;&nbsp;</a>
        <a class="boton" href="artistas-hiperrealistas.php">&nbsp;</a>
        <a class="boton" href="artistas-hiperrealistas.php">&nbsp;</a>
	</td>
<td class="celda_2">
		<script type="text/javascript">
		new fadeshow(fadeimages, 560, 157, 0, 6000, 1)
		</script>
        
        <?php 
		switch($_SESSION["idioma"])
		{	
			case "cat": ?>
            <h1>La hand the Sergi : Artistes</h1>
            <p>En aquesta secci� et presentem als nostres artistes. Creiem en ells i ells  en nosaltres. La nostra missi� �s donar a con�ixer la seva obra i fer-la  extensible a tots. Si en tens inter�s, nom�s has de fer un simple <em>click a sobre dels nostres artistes</em> i transportar-te a un m�n particular i �nic. El m�n que cadascun d�ells deixa  entreveure des de la finestra que l�equip de <em>La hand the Sergi</em> t�obre perqu� ho  miris des de lluny i treguis el cap cada cop que vulguis. </p>
            <? break;
			
			case "ing": ?>
            <h1>La hand the Sergi : Artists</h1>
			<p>In this section we introduce you to our artists. We  believe in them, and they believe in us. Our mission is to make their work  known, and bring it to everyone. If you are interested, just <em>click on our artists</em> and tele-transport  yourself to a unique, special world. A world that each of them lets us catch a  glimpse of through the window opened by the team of <em>La hand the Sergi</em>, for you to look in from outside and come  back to as many times as you wish.</p>
            <? break;
            
			default: ?>
            <h1>La hand the Sergi : Artistas</h1>
            <p>En esta secci&oacute;n te  presentamos a nuestros artistas. Creemos en ellos y ellos en nosotros. Nuestra  misi&oacute;n es dar a conocer su obra y hacerla extensible a todos. Si est&aacute;s  interesado, s&oacute;lo tienes que hacer un simple <em>click  sobre nuestros artistas</em> y teletransportarte a un mundo particular y  singular. El mundo que cada uno de ellos deja entrever desde la ventana que el  equipo de <em>La hand the Sergi</em> te  abre para&nbsp; que mires desde lejos y&nbsp;te asomes cada vez que quieras.</p>
            <? break;
         }
		 ?>
			
<?php include("piecera.php");?>
