<?php 
session_start();
if(!($_SESSION["idioma"]))	$_SESSION["idioma"]="esp";
if(($_GET["lang"]))			$_SESSION["idioma"]=$_GET["lang"];

include("noticias/funciones.php");

define("boton1_esp","exposiciones");	 
define("boton1_cat","exposicions");	  
define("boton1_ing","exhibitions");
define("boton2_esp","artistas");		 
define("boton2_cat","artistes");		  
define("boton2_ing","artists");
define("boton3_esp","compra");		 
define("boton3_cat","compra");		  
define("boton3_ing","purchase");
define("boton4_esp","encargos");       
define("boton4_cat","enc�rrecs");      
define("boton4_ing","personal orders");
define("boton5","art events");	 
define("boton6_esp","contactar");		 
define("boton6_cat","contactar");	  
define("boton6_ing","contact");		

$artis1["esp"]="biograf&iacute;a";		
$artis1["cat"]="biografia";		
$artis1["ing"]="biography";						  		
$artis2["esp"]="obras";		
$artis2["cat"]="obres";		
$artis2["ing"]="works";										 	
$artis3["esp"]="art&iacute;culos &amp; prensa";					  									 								  
$artis3["cat"]="articles i premsa ";							  
$artis3["ing"]="articless &amp; press";	

$vsi["esp"]="DISPONIBLE";	
$vsi["cat"]="DISPONIBLE";	
$vsi["ing"]="AVALAIBLE";
$vno["esp"]="VENDIDO";
$vno["cat"]="VENUT";	
$vno["ing"]="SOLD";	

$oleo_madera["esp"]	="Oleo sobre madera";					
$oleo_madera["ing"]	="Oil on wood";
$oleo_tela["esp"]="Oleo sobre tela";						
$oleo_tela["ing"]="Oil on canvas";
$oleo_acrilico["esp"]="Oleo y acr&iacute;lico sobre lienzo";
$oleo_acrilico["ing"]="Oil and acrilyc on canvas";
$dibujo_madera["esp"]="Dibujo sobre madera";				
$dibujo_madera["ing"]="Draw on wood";
$dibujo_papel["esp"]="Dibujo sobre papel";					
$dibujo_papel["ing"]="Drawing on paper";
$obra_maestra["esp"]="Obra maestra";
$obra_maestra["ing"]="Master Piece";
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)">
	<meta name="keywords" content="La Hand the Sergi - Metropolitan & The City Art Gallery Barcelona,art gallery,gallery art,gallery barcelona,art barcelona,gallery painting,print art,photography art,work art, borne art,born art ">
    <meta name="lang" content="sp,es,espa�ol,espanol,castellano">
    <meta name="revisit" content="7 days" />
    <meta name="robots" content="index,follow,all" />
    <link rel="stylesheet" type="text/css" href="estilos.css">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>