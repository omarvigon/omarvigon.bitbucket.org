<option value="antropologia-salut">Antropologia de la Salut</option>
<option value="antropologia-consum">Antropologia del Consum</option>
<option value="antropologia-genere">Antropologia del G&egrave;nere</option>
<option value="antropologia-genocidi">Antropologia del Genocidi</option>
<option value="antropologia-mite">Antropologia del Mite</option>
<option value="antropologia-treball">Antropologia del Treball</option>
<option value="antropologia-ecologica">Antropologia Ecol&ograve;gica</option>
<option value="antropologia-economica">Antropologia Econ&ograve;mica</option>
<option value="antropologia-desenvolupament">Antropologia i Desenvolupament</option>
<option value="antropologia-historia">Antropologia i Hist&ograve;ria</option>
<option value="antropologia-literatura">Antropologia i Literatura</option>
<option value="antropologia-juridica">Antropologia Jur&iacute;dica</option>
<option value="antropologia-politica">Antropologia Pol&iacute;tica</option>
<option value="antropologia-religiosa">Antropologia Religiosa</option>
<option value="antropologia-simbolicocognitiva">Antropologia Simbolicocognitiva</option>
<option value="antropologia-urbana">Antropologia Urbana</option>
<option value="antropologia-visual">Antropologia Visual</option>
<option value="antropologia-comunicacio-art">Antropologia, Comunicaci&oacute; i Art</option>
<option value="ciencia-tecnica-cultura">Ci&egrave;ncia, T&egrave;cnica i Cultura</option>
<option value="estadistica-aplicada-ciencies-socials">Estad&iacute;stica Aplicada a les Ci&egrave;ncies Socials</option>
<option value="etnologia-afroamericana">Etnologia Afroamericana</option>
<option value="etnologia-andina">Etnologia Andina</option>
<option value="etnologia-magrib">Etnologia del Magrib</option>
<option value="etnologia-regional-peninsula-iberica">Etnologia Regional (Pen&iacute;nsula Ib&egrave;rica)</option>
<option value="geografia-humana-demografia">Geografia Humana i Demografia</option>
<option value="historia-antropologia-1-escoles-classiques">Hist&ograve;ria de l&acute;Antropologia I: Escoles Cl&agrave;ssiques</option>
<option value="historia-antropologia-2-teoria-metode">Hist&ograve;ria de l&acute;Antropologia II: Teoria i M&egrave;tode</option>
<option value="introduccio-antropologia-social">Introducci&oacute; a l&acute;Antropologia Social</option>
<option value="poder-conflicte-cultura">Poder, Conflicte i Cultura</option>
<option value="practiques-camp-antropologia-social">Pr&agrave;ctiques de Camp en Antropologia Social</option>
<option value="tecniques-investigacio-antropologia-social">T&egrave;cniques d&acute;Investigaci&oacute; en Antropologia Social</option>
