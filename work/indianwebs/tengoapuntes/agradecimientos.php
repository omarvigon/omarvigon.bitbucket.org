<?php include("0cabecera.php");?>

  <div class="centro">
        <h2>Agradecimientos</h2>
		<p>Desde el equipo de  <em>tengoapuntes</em> (Marcos L&oacute;pez, Marc Mons&oacute;, Rogeli Puig, Marc Urp&iacute; y Jes&uacute;s de  Molina)&nbsp; queremos agradecer la aportaci&oacute;n a los estudiantes que nos han  facilitado sus apuntes a lo largo de estos dos a&ntilde;os de trabajo.</p>
		<p>En especial a  los alumnos y algunos profesores <b><i>Jose Manuel Cornejo </i></b>y<b><i> Antonio del cerro</i></b> (por lo mucho que me hab&eacute;is ense&ntilde;ado) de la facultad  de psicolog&iacute;a de la UB y del campus Mundet en general, y en particular a <b><i>Jordi  Salamero, Montse Costa, Francesc Segarra, Laura Mui&ntilde;os, Cristina Calvera,  Eduard Pitard, Mireia Masnou, Nachete el crimin&oacute;logo, Sussy Casadevall, Erick  Rufiandis, Xavier, Pedrito Riera, Lluis Alberto Castro, Francisco Villar, Maria  Jos&eacute; Franganillo, Betty Coma, Beatriz Corrales, Vani Llugdar, Claudia y Silvia  (esas rubias!!) y Eli (la morena)!!</i></b> &nbsp;<b><i>Unai Vicente, Cristina  Vidal y Tanit Vila</i></b>.</p>
		<p>Gracias por aguantar mis neuras alg&uacute;n@s y en  general a &nbsp;tod@s porque sin vuestra aportaci&oacute;n y apoyo no habr&iacute;a sido  posible llevar este proyecto adelante. Gracias a <b><i>Josep Lamarca</i></b> por ser la llave que abri&oacute; la puerta del proyecto y a <b><i>Ismael Gil</i></b> por creer en que esto funcionar&aacute;.</p>
   </div> 
   
<?php include("0piecera.php");?>