<?php include("0cabecera.php");?>

  <div class="centro">
        <h2>&iquest;Qui&eacute;nes  somos?</h2>
        <p><em>Tengoapuntes.com</em> vio la luz por primera vez el 20 de mayo de  2007 para cubrir la necesidad de apuntes de los estudiantes de la <b>Facultat de Psicolog&iacute;a</b> de la <b>Universitat de Barcelona</b> ante la ca&iacute;da  inesperada de PSICOUB.TK siempre con la intenci&oacute;n de ayudar a otros estudiantes  y poder colaborar entre todos para llevar una vida m&aacute;s tranquila y plena.</p>
        <p>Con el tiempo lo que comenz&oacute; siendo una modesta labor  altruista se fue convirtiendo en un proyecto que pod&iacute;a ir a m&aacute;s, sin olvidar su  propia esencia, fomentar la cohesi&oacute;n y colaboraci&oacute;n entre estudiantes. <em>Tengoapuntes.com</em>  tuvo el sue&ntilde;o de llevar este modelo a todas las universidades de Catalunya,  realizando una laboriosa investigaci&oacute;n y consulta en internet de todas las  p&aacute;ginas creadas por universitarios que ofreciesen apuntes sin &aacute;nimo de  lucrarse, con la finalidad de unirlos. No todas las p&aacute;ginas encontradas ten&iacute;an  esta visi&oacute;n del mundo universitario y algunas anhelaban su propio beneficio.</p>
        <p>El <b>objeto social</b> de <b>Tengoapuntes.com</b> es el de contribuir a la sociedad de forma creativa,  permitiendo a los estudiantes acceder a los apuntes de otros compa&ntilde;eros y al  mismo tiempo agradecerles su contribuci&oacute;n por enviar nuevos apuntes mediante  donativos que empresas interesadas en el mundo universitario ofrecen a  tengoapuntes.com</p>
        <p>Las <b>finalidades  esenciales</b> de <em>Tengoapuntes.com</em> son las siguientes:</p>
        <ul>
        	<li>Ofrecer a todo estudiante la posibilidad de contribuir en  una importante labor social.</li>
        	<li>Ofrecer a todo estudiante un lugar de encuentro en internet  para aclarar sus dudas acad&eacute;micas a trav&eacute;s de foros gestionados por la  asociaci&oacute;n de estudiantes correspondiente a su centro docente.</li>
        	<li>Ofrecer a todo estudiante la posibilidad de donar sus  apuntes a la comunidad de estudiantes.</li>
        	<li>Ofrecer a todo estudiante la posibilidad de descargarse los  apuntes donados por sus compa&ntilde;eros.</li>
        	<li>Ofrecer a toda empresa que desee acceder a los estudiantes  la posibilidad de publicitarse a cambio de hacer un regalo para los estudiantes  que donen sus apuntes.</li>
        </ul>
        <p>Nuestro <b>compromiso  con la sociedad</b> es total, as&iacute; como con el estudiante, y por ese motivo  tengoapuntes.com nunca tendr&aacute; balances econ&oacute;micos positivos para sus creadores,  y&nbsp; sortear&aacute; en forma de productos o  servicios todo beneficio econ&oacute;mico, as&iacute; como el 7% de esa cantidad deber&aacute;  donarse a las ONG anunciadas en la web por los ganadores de dichos sorteos.</p>
        <p><b>Visi&oacute;n</b><br />La  visi&oacute;n consiste en una proyecci&oacute;n, una imagen de futuro de la web a largo  plazo.<br />
        <i>La finalidad &uacute;ltima de  Tengoapuntes.com es la de humanizar la sociedad desde el mundo estudiantil.</i></p>
        <p><b>Misi&oacute;n</b><br />La  misi&oacute;n de una entidad es el concepto que define su raz&oacute;n de ser, la finalidad y  el prop&oacute;sito fundamental a alcanzar ante los diferentes grupos de inter&eacute;s.<br />
        <i>Comprometer a personas y empresas  a solucionar &nbsp;problemas de nuestra  sociedad y cohesionar el mundo estudiantil desde sus propios participantes. </i></p>
        <p><b>Valores</b></p>
        <ul>
        	<li>Humanidad</li>
        	<li>Compromiso social</li>
        	<li>Compromiso</li>
        </ul>
        <p>Estos  tres valores constituyen nuestra raz&oacute;n de ser y nuestra forma de ver la vida.</p>
        <p>La <b><i>Humanidad</i></b> de las personas es su mayor valor y desde tengoapuntes.com deseamos compartirlo  con los participantes de la web realizando donaciones conjuntamente para  quienes m&aacute;s lo necesitan y colaborando con organizaciones de &iacute;ndole similar.</p>
        <p>El <b><i>Compromiso  social</i> </b>de tengoapuntes.com tiene su reflejo en la constante donaci&oacute;n de  sus beneficios para acciones que ayuden a los que m&aacute;s lo necesitan.</p>
        <p>Finalmente  el <b><i>Compromiso</i></b> de responder a las expectativas que desde ahora se est&aacute;n poniendo en nosotros  satisfaciendo la necesidad de los estudiantes y de las personas que lo  necesitan.</p>
   </div> 
<?php include("0piecera.php");?>