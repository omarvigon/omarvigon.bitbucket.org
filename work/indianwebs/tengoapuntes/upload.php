<?php include("0cabecera.php");?>

  <div class="centro">
  	<?php 
	if($_FILES["archivo"]["name"]) 
    {
		if($_FILES["archivo"]["size"]>10000000) //$_FILES["archivo"]["type"]
			echo "<p>Archivo demasiado grande. Tama&ntilde;o m&aacute;ximo : 10Mb.</p>";
		else	
		    subir_archivo();
	}	
	else
	{ 
		if($_COOKIE["phpbb3_h6fpd_u"]>1) { ?>
		<p>
        <h2>Sube y comparte tus apuntes</h2>
        <form name="form_subir" method="post" action="upload.php" style="margin:16px 0 0 0;" enctype="multipart/form-data">
        <!--&raquo; Comunidad Aut&oacute;noma
        <select name="comunidad">
        	<option value="andalucia">Andaluc&iacute;a</option>
            <option value="aragon">Arag&oacute;n</option>
            <option value="asturias">Asturias</option>
            <option value="baleares">Baleares</option>
            <option value="canarias">Canarias</option>
            <option value="cantabria">Cantabria</option>
            <option value="ceuta">Ceuta</option>
            <option value="castilla-mancha">Castilla-La Mancha</option>
            <option value="castilla-leon">Castilla y Le&oacute;n</option>
            <option value="cataluna">Catalu&ntilde;a</option>
            <option value="valencia">Comunidad Valenciana</option>
            <option value="extremadura">Extremadura</option>
            <option value="galicia">Galicia</option>
			<option value="rioja">La Rioja</option>
            <option value="madrid">Madrid</option>
            <option value="melilla">Melilla</option>
            <option value="murcia">Murcia</option>
            <option value="navarra">Navarra</option>
            <option value="pais-vasco">Pa&iacute;s Vasco</option>
        </select>  -->
        
        &raquo; Universidad/Carrera
        <select name="universidad" onchange="sel_carrera()">
        	<option value='0' selected="selected">(Seleccionar.....)</option>
            <optgroup label="UB Universitat de Barcelona">
            <option value="ub_ade">UB - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="ub_antropologia_social">UB - Antropologia social i cultural</option>
            <option value="ub_arquitectura">UB - Arquitectura</option>
            <option value="ub_bellas_artes">UB - Belles arts</option>
            <option value="ub_biblioteconomia">UB - Biblioteconomia i documentaci&oacute;</option>
            <option value="ub_biologia">UB - Biologia</option>
            <option value="ub_bioquimica">UB - Bioqu&iacute;mica</option>
            <option value="ub_ciencia_tecnologia_alimentos">UB - Ci&egrave;ncia i tecnologia dels aliments</option>
            <option value="ub_ciencias_financieras">UB - Ci&egrave;ncies actuarials i financeres</option>
            <option value="ub_ciencias_ambientales">UB - Ci&egrave;ncies ambientals</option>
            <option value="ub_ciencias_actividad_fisica">UB - Ci&egrave;ncies de l'activitat f&iacute;sica i de l'esport</option>
            <option value="ub_ciencias_trabajo">UB - Ci&egrave;ncies del treball</option>
            <option value="ub_ciencias_empresariales">UB - Ci&egrave;ncies empresarials</option>
            <option value="ub_ciencias_politicas">UB - Ci&egrave;ncies pol&iacute;tiques i de l'administraci&oacute;</option>
            <option value="ub_cine_audiovisuales">UB - Cinema i audiovisuals</option>
            <option value="ub_comunicacion_audiovisual">UB - Comunicaci&oacute;  audiovisual</option>
            <option value="ub_criminologia">UB - Criminologia</option>
            <option value="ub_criminologia_politica_criminal">UB - Criminologia i pol&iacute;tica criminal</option>
            <option value="ub_documentation">UB - Documentaci&oacute;</option>
            <option value="ub_derecho">UB - Dret</option>
            <option value="ub_economia">UB - Economia</option>
            <option value="ub_educacion_social">UB - Educaci&oacute; social</option>
            <option value="ub_ingenieria_geologica">UB - Enginyer ge&ograve;leg</option>
            <option value="ub_ingenieria_materiales">UB - Enginyeria de materials</option>
            <option value="ub_ingenieria_electronica">UB - Enginyeria en electr&ograve;nica</option>
            <option value="ub_ingenieria_quimica">UB - Enginyeria qu&iacute;mica</option>
            <option value="ub_ingenieria_tecnica_informacion_sistemas">UB - Enginyeria t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="ub_estadistica">UB - Estad&iacute;stica</option>
            <option value="ub_estudios_inmobiliarios_construccion">UB - Estudis immobiliaris i de la construcci&oacute;</option>
            <option value="ub_farmacia">UB - Farm&agrave;cia</option>
            <option value="ub_filologia_alemana">UB - Filologia alemanya</option>
            <option value="ub_filologia_inglesa">UB - Filologia anglesa</option>
            <option value="ub_filologia_arabe">UB - Filologia &agrave;rab</option>
            <option value="ub_filologia_catalana">UB - Filologia catalana</option>
            <option value="ub_filologia_clasica">UB - Filologia cl&agrave;ssica</option>
            <option value="ub_filologia_eslava">UB - Filologia eslava</option>
            <option value="ub_filologia_francesa">UB - Filologia francesa</option>
            <option value="ub_filologia_gallega">UB - Filologia gallega</option>
            <option value="ub_filologia_hebrea">UB - Filologia hebrea</option>
            <option value="ub_filologia_hispanica">UB - Filologia hisp&agrave;nica</option>
            <option value="ub_filologia_italiana">UB - Filologia italiana</option>
            <option value="ub_filologia_portuguesa">UB - Filologia portuguesa</option>
            <option value="ub_filologia_romanica">UB - Filologia rom&agrave;nica</option>
            <option value="ub_filosofia">UB - Filosofia</option>
            <option value="ub_fisica">UB - F&iacute;sica</option>
            <option value="ub_fisioterapia">UB - Fisioterapia</option>
            <option value="ub_formacion_organizacion">UB - Formaci&oacute; a les organitzacions</option>
            <option value="ub_geografia">UB - Geografia</option>
            <option value="ub_geologia">UB - Geologia</option>
            <option value="ub_gestion_administracion_publica">UB - Gesti&oacute; i Administraci&oacute; pública</option>
            <option value="ub_historia">UB - Hist&ograve;ria</option>
            <option value="ub_historia_art">UB - Hist&ograve;ria de l'art</option>
            <option value="ub_inef">UB - INEF</option>
            <option value="ub_enfermeria">UB - Infermeria</option>
            <option value="ub_investigacion_tecnicas_mercado">UB - Investigaci&oacute; i t&egrave;cniques de mercat</option>
            <option value="ub_investigacion_privada">UB - Investigaci&oacute; privada</option>
            <option value="ub_linguistica">UB - Ling&uuml;&iacute;stica</option>
            <option value="ub_magisterio_educacion_fisica">UB - Magisterio Ed. Fisica</option>
            <option value="ub_matematicas">UB - Matem&agrave;tiques</option>
            <option value="ub_medicina">UB - Medicina</option>
            <option value="ub_maestro_lengua_extranjera">UB - Mestre, especialitat de llengua estrangera</option>
            <option value="ub_maestro_especialidad_educacion_especial">UB - Mestre, especialitat d'educaci&oacute; especial</option>
            <option value="ub_maestro_especialidad_educacion_fisica">UB - Mestre, especialitat d'educaci&oacute; f&iacute;sica</option>
            <option value="ub_maestro_especialidad_educacion_infantil">UB - Mestre, especialitat d'educaci&oacute; infantil</option>
            <option value="ub_maestro_especialidad_educacion_musical">UB - Mestre, especialitat d'educaci&oacute; musical</option>
            <option value="ub_maestro_especialidad_educacion_primaria">UB - Mestre, especialitat d'educaci&oacute; prim&agrave;ria</option>
            <option value="ub_nutricion_dietetica">UB - Nutrici&oacute; humana i diet&egrave;tica</option>
            <option value="ub_odontologia">UB - Odontologia</option>
            <option value="ub_pedagogia">UB - Pedagogia</option>
            <option value="ub_pedagogia_social">UB - Pedagogia social</option>
            <option value="ub_periodismo">UB - Periodisme</option>
            <option value="ub_podologia">UB - Podologia</option>
            <option value="ub_prevencion_riesgos_laborales">UB - Prevenci&oacute; de  riscos laborals</option>
            <option value="ub_psicologia">UB - Psicologia</option>
            <option value="ub_psicopedagogia">UB - Psicopedagogia</option>
            <option value="ub_publicidad_relaciones_publicas">UB - Publicitat i relacions públiques</option>
            <option value="ub_quimica">UB - Qu&iacute;mica</option>
            <option value="ub_relaciones_laborales">UB - Relacions laborals</option>
            <option value="ub_sociologia">UB - Sociologia</option>
            <option value="ub_teoria_literatura_comparada">UB - Teoria de la literatura i literatura comparada</option>
            <option value="ub_trabajo_social">UB - Treball social</option>
            <option value="ub_tributaria_contable">UB - Tributari i comptable</option>
            <option value="ub_turismo">UB - Turisme</option>
            </optgroup>
            <optgroup label="UAB Universitat Aut&oacute;noma de Barcelona">
            <option value="uab_ade">UAB - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="uab_antropologia_social">UAB - Antropologia social i cultural</option>
            <option value="uab_artes_diseño">UAB - Arts i disseny</option>
            <option value="uab_archivista_gestion_documentos">UAB - Arxiv&iacute;stica i gesti&oacute; de documents</option>
            <option value="uab_biologia">UAB - Biologia</option>
            <option value="uab_bioquimica">UAB - Bioqu&iacute;mica</option>
            <option value="uab_biotecnologia">UAB - Biotecnologia</option>
            <option value="uab_ciencia_tecnologia_alimentos">UAB - Ci&egrave;ncia i tecnologia dels aliments</option>
            <option value="uab_ciencias_ambienrales">UAB - Ci&egrave;ncies ambientals</option>
            <option value="uab_ciencias_trabajo">UAB - Ci&egrave;ncies del treball</option>
            <option value="uab_ciencias_empresariales">UAB - Ci&egrave;ncies empresarials</option>
            <option value="uab_ciencias_politicas_administracion">UAB - Ci&egrave;ncies pol&iacute;tiques i de l'administraci&oacute;</option>
            <option value="uab_comunicacion_audiovisual">UAB - Comunicaci&oacute; audiovisual</option>
            <option value="uab_criminologia">UAB - Criminologia</option>
            <option value="uab_direccion_hotelera">UAB - Direcci&oacute; hotelera</option>
            <option value="uab_diseño">UAB - Disseny (UAB)</option>
            <option value="uab_documentacion">UAB - Documentaci&oacute;</option>
            <option value="uab_derecho">UAB - Dret</option>
            <option value="uab_economia">UAB - Economia</option>
            <option value="uab_educacion_social">UAB - Educaci&oacute; social</option>
            <option value="uab_ingenieria_materiales">UAB - Enginyeria de materials</option>
            <option value="uab_ingenieria_telecomunicacion">UAB - Enginyeria de telecomunicaci&oacute;</option>
            <option value="uab_ingenieria_electronica">UAB - Enginyeria en electr&ograve;nica</option>
            <option value="uab_ingenieria_informatica">UAB - Enginyeria en inform&agrave;tica</option>
            <option value="uab_ingenieria_quimica">UAB - Enginyeria qu&iacute;mica</option>
            <option value="uab_ingenieria_tecnica_telecomunicacion_electronicos">UAB - Enginyeria t&egrave;cnica de telecomunicaci&oacute;,esp. en sistemes electr&ograve;nics</option>
            <option value="uab_ingenieria_tecnica_informatica_gestion">UAB - Enginyeria t&egrave;cnica en inform&agrave;tica de gesti&oacute;</option>
            <option value="uab_ingenieria_tecnica_informatica_sistemas">UAB - Enginyeria t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="uab_ingenieria_tecnica_industrial_electricidad">UAB - Enginyeria t&egrave;cnica industrial, especialitat en electricitat</option>
            <option value="uab_ingenieria_tecnica_industrial_electronica">UAB - Enginyeria t&egrave;cnica industrial, especialitat en electr&ograve;nica industrial</option>
            <option value="uab_ingenieria_tecnica_">UAB - Enginyeria t&egrave;cnica industrial, especialitat en mec&agrave;nica</option>
            <option value="uab_ingenieria_tecnica_">UAB - Enginyeria t&egrave;cnica industrial, especialitat en qu&iacute;mica industrial</option>
            <option value="uab_estadistica">UAB - Estad&iacute;stica</option>
            <option value="uab_estudios_asia_oriental">UAB - Estudis d'&Agrave;sia oriental</option>
            <option value="uab_estudios_internacionales_interculturales">UAB - Estudis internacionals i interculturals</option>
            <option value="uab_filologia_inglesa">UAB - Filologia anglesa</option>
            <option value="uab_filologia_catalana">UAB - Filologia catalana</option>
            <option value="uab_filologia_clasica">UAB - Filologia cl&agrave;ssica</option>
            <option value="uab_filologia_francesa">UAB - Filologia francesa</option>
            <option value="uab_filologia_hispanica">UAB - Filologia hisp&agrave;nica</option>
            <option value="uab_filosofia">UAB - Filosofia</option>
            <option value="uab_fisica">UAB - F&iacute;sica</option>
            <option value="uab_fisioterapia">UAB - Fisioter&agrave;pia</option>
            <option value="uab_geografia">UAB - Geografia</option>
            <option value="uab_geologia">UAB - Geologia</option>
            <option value="uab_gestion_aeronaturica">UAB - Gesti&oacute; aeron&agrave;utica</option>
            <option value="uab_historia">UAB - Hist&ograve;ria</option>
            <option value="uab_historia_arte">UAB - Hist&ograve;ria de l'art</option>
            <option value="uab_historia_ciencias_musica">UAB - Hist&ograve;ria i ci&egrave;ncies de la m&uacute;sica</option>
            <option value="uab_humanidades">UAB - Humanitats</option>
            <option value="uab_enfermeria">UAB - Infermeria</option>
            <option value="uab_investigacion_tecnicas_mercado">UAB - Investigaci&oacute; i t&egrave;cniques de mercat</option>
            <option value="uab_logopedia">UAB - Logop&egrave;dia</option>
            <option value="uab_matematicas">UAB - Matem&agrave;tiques</option>
            <option value="uab_medicina">UAB - Medicina</option>
            <option value="uab_maestro_lengua_extranjera">UAB - Mestre, especialitat de llengua estrangera</option>
            <option value="uab_maestro_educacion_especial">UAB - Mestre, especialitat d'educaci&oacute; especial</option>
            <option value="uab_maestro_educacion_fisica">UAB - Mestre, especialitat d'educaci&oacute; f&iacute;sica</option>
            <option value="uab_maestro_educacion_infantil">UAB - Mestre, especialitat d'educaci&oacute; infantil</option>
            <option value="uab_maestro_esducacion_musical">UAB - Mestre, especialitat d'educaci&oacute; musical</option>
            <option value="uab_maestro_educacion_primaria">UAB - Mestre, especialitat d'educaci&oacute; prim&agrave;ria</option>
            <option value="uab_multimedia">UAB - Multim&egrave;dia </option>
            <option value="uab_pedagogia">UAB - Pedagogia</option>
            <option value="uab_periodismo">UAB - Periodisme</option>
            <option value="uab_podologia">UAB - Podologia</option>
            <option value="uab_prevencion_seguridad_integral">UAB - Prevenci&oacute; i seguretat integral</option>
            <option value="uab_psicologia">UAB - Psicologia</option>
            <option value="uab_psicopedagogia">UAB - Psicopedagogia</option>
            <option value="uab_publicidad_relaciones_publicas">UAB - Publicitat i relacions p&uacute;bliques</option>
            <option value="uab_quimica">UAB - Qu&iacute;mica</option>
            <option value="uab_relaciones_laborales">UAB - Relacions laborals</option>
            <option value="uab_sociologia">UAB - Sociologia</option>
            <option value="uab_teoria_literatura_comparada">UAB - Teoria de la literatura i literatura comparada</option>
            <option value="uab_terapia_ocupacional">UAB - Ter&agrave;pia ocupacional</option>
            <option value="uab_traduccion_interpretacion_aleman">UAB - Traducci&oacute; i interpretaci&oacute; (alemany)</option>
            <option value="uab_traduccion_interpretacion_ingles">UAB - Traducci&oacute; i interpretaci&oacute; (angl&egrave;s)</option>
            <option value="uab_traduccion_interpretacion_frances">UAB - Traducci&oacute; i interpretaci&oacute; (franc&egrave;s)</option>
            <option value="uab_turismo">UAB - Turisme</option>
            <option value="uab_publicidad_comunicacion">UAB - Publicitat i comunicacio</option>
            <option value="uab_veterinaria">UAB - Veterin&agrave;ria</option>
            </optgroup>
            <optgroup label="UPC Universitat Polit&egrave;cnica de Catalunya">
            <option value="upc_ingenieria_minas">UPC - Enginyeria de mines</option>
            <option value="upc_ingenieria_tecnica_minas_explotacion">UPC - Eng. t&egrave;cnica de mines, esp.t en explotaci&oacute; de mines</option>
            <option value="upc_ingenieria_tecnica_telecomunicaciones">UPC - Eng. t&egrave;cnica de telecomunicaci&oacute;, esp. en sistemes electr&ograve;nics</option>
            <option value="upc_ingenieria_tecnica_telecomunicaciones_imagen">UPC - Eng. t&egrave;cnica de telecomunicaci&oacute;, esp. en so i imatge</option>
            <option value="upc_ingenieria_tecnica_industrial_electronica_industrial">UPC - Eng. t&egrave;cnica industrial, especialitat en electr&ograve;nica industrial</option>
            <option value="upc_ingenieria_tecnica_industrial_mecanica">UPC - Eng. t&egrave;cnica industrial, especialitat en mec&agrave;nica</option>
            <option value="upc_ingenieria_tecnica_industrial_quimica_industrial">UPC - Eng. t&egrave;cnica industrial, especialitat en qu&iacute;mica industrial</option>
            <option value="upc_ingenieria_tecnica_industrial_electricidad">UPC - Eng. t&egrave;cnica industrial, especialitat en electricitat</option>
            <option value="upc_ingenieria_telecomunicaciones">UPC - Enginyeria de telecomunicaci&oacute;</option>
            <option value="upc_ingenieria_electronica">UPC - Enginyeria en electr&ograve;nica</option>
            <option value="upc_ingenieria_tecnica_aeronaturica_aeronavegacion">UPC - Eng. t&egrave;cnica aeron&agrave;utica, esp. en aeronavegaci&oacute;</option>
            <option value="upc_ingenieria_tecnica_agricola_agropecuarias">UPC - Eng. t&egrave;cnica agr&iacute;cola, esp. en explotacions agropecu&agrave;ries</option>
            <option value="upc_ingenieria_tecnica_agricola_horticultura_jardineria">UPC - Eng. t&egrave;cnica agr&iacute;cola, esp. en hortofructicultura i jardineria</option>
            <option value="upc_ingenieria_tecnica_agricola_agrarias_alimentarias">UPC - Eng.t&egrave;cnica agr&iacute;cola, esp. en ind&uacute;stries agr&agrave;ries i aliment&agrave;ries</option>
            <option value="upc_ingenieria_tecnica_telecomunicaciones">UPC - Eng. t&egrave;cnica de telecomunicaci&oacute;, esp. en s. de telecomunicaci&oacute;</option>
            <option value="upc_ingenieria_tecnica_telecomunicaciones_telematica">UPC - Eng. t&egrave;cnica de telecomunicaci&oacute;, esp. en telem&agrave;tica</option>
            <option value="upc_diseño">UPC - Disseny</option>
            <option value="upc_ingenieria_materiales">UPC - Enginyeria de materials</option>
            <option value="upc_ingenieria_organizacion_industrial">UPC - Enginyeria en organitzaci&oacute; industrial</option>
            <option value="upc_ingenieria_industrial">UPC - Enginyeria industrial</option>
            <option value="upc_ingenieria_quimica">UPC - Enginyeria qu&iacute;mica</option>
            <option value="upc_arquitectura_tecnica">UPC - Arquitectura t&egrave;cnica</option>
            <option value="upc_ingenieria_tecnica_topografia">UPC - Eng. t&egrave;cnica en topografia</option>
            <option value="upc_medios_audiovisuales">UPC - Mitjans audiovisuals</option>
            <option value="upc_ingenieria_automatica_electronica_industrial">UPC - Enginyeria en autom&agrave;tica i electr&ograve;nica industrial</option>
            <option value="upc_ciencias_empresariales">UPC - Ci&egrave;ncies empresarials</option>
            <option value="upc_prevencion_riesgos_laborales">UPC - Prevenci&oacute; de  riscos laborals</option>
            <option value="upc_fotografia_digital">UPC - Fotografia i creaci&oacute; digital</option>
            <option value="upc_multimedia">UPC - Multim&egrave;dia</option>
            <option value="upc_ciencias_tecnicas_estadistica">UPC - Ci&egrave;ncies i  t&egrave;cniques estad&iacute;stiques</option>
            <option value="upc_estadistica">UPC - Estad&iacute;stica</option>
            <option value="upc_matematicas">UPC - Matem&agrave;tiques</option>
            <option value="upc_arquitectura">UPC - Arquitectura</option>
            <option value="upc_optica_optometria">UPC - &Ograve;ptica i optometria</option>
            <option value="upc_ingenieia_informatica">UPC - Enginyeria en inform&agrave;tica</option>
            <option value="upc_ingenieria_tecnica_informatica_gestion">UPC - Eng. t&egrave;cnica en inform&agrave;tica de gesti&oacute;</option>
            <option value="upc_ingenieria_tecnica_informatica_sistemas">UPC - Eng. t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="upc_ingenieria_geologica">UPC - Enginyer ge&ograve;leg</option>
            <option value="upc_ingenieria_caminos">UPC - Enginyeria de  camins, canals i ports</option>
            <option value="upc_ingenieria_tecnica_obras_publicas_construccion_civil">UPC - Eng. t&egrave;cnica d'obres p&uacute;bliques, esp. en construccions civils</option>
            <option value="upc_ingenieria_tecnica_obras_publicas_hidrologia">UPC - Eng. t&egrave;cnica d'obres p&uacute;bliques, esp.en hidrologia</option>
            <option value="upc_ingenieria_tecnica_obras_publicas_transporte_urbano">UPC - Eng. t&egrave;cnica d'obres p&uacute;bliques, esp. en transports i s.urbans</option>
            <option value="upc_ingenieria_aeronautica">UPC - Enginyeria aeron&agrave;utica</option>
            <option value="upc_ingenieria_tecnica_industrial_textil">UPC - Eng. t&egrave;cnica industrial, especialitat t&egrave;xtil</option>
            <option value="upc_ingenieria_tecnica_naval">UPC - Eng. t&egrave;cnica naval, especialitat en propulsi&oacute; i serveis del vaixell</option>
            <option value="upc_maquina_naval">UPC - M&agrave;quines navals (dipl.)</option>
            <option value="upc_maquina_naval">UPC - M&agrave;quines navals (llic.)</option>
            <option value="upc_nautica_tranporte_maritimo">UPC - N&agrave;utica i transport mar&iacute;tim</option>
            <option value="upc_navegacion_maritima">UPC - Navegaci&oacute; mar&iacute;tima</option>
            </optgroup>
            <optgroup label="UPF - Universitat Pompeu Fabra">
            <option value="upf_ade">UPF - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="upf_arquitectura_tecnica">UPF - Arquitectura t&egrave;cnica</option>
            <option value="upf_biologia">UPF - Biologia</option>
            <option value="upf_ciencias_trabajo">UPF - Ci&egrave;ncies del treball</option>
            <option value="upf_ciencias_empresariales">UPF - Ci&egrave;ncies empresarials</option>
            <option value="upf_ciencias_politicas_administracion">UPF - Ci&egrave;ncies pol&iacute;tiques i de l'administraci&oacute;</option>
            <option value="upf_comercio_internacional">UPF - Comer&ccedil; internacional</option>
            <option value="upf_comunicacion_audiovisual">UPF - Comunicaci&oacute; audiovisual</option>
            <option value="upf_diseño">UPF - Disseny (UPF)</option>
            <option value="upf_derecho">UPF - Dret</option>
            <option value="upf_economia">UPF - Economia</option>
            <option value="upf_ingenieria_telecomunicacion">UPF - Enginyeria de telecomunicaci&oacute;</option>
            <option value="upf_ingenieria_informatica">UPF - Enginyeria en inform&agrave;tica</option>
            <option value="upf_ingenieria_tecnica_telecomunicacion_telematica">UPF - Enginyeria t&egrave;cnica de telecomunicaci&oacute;, esp. en telem&agrave;tica</option>
            <option value="upf_ingenieria_tecnica_diseño_industrial">UPF - Enginyeria t&egrave;cnica en disseny industrial</option>
            <option value="upf_ingenieria_tecnica_informacion_sistemas">UPF - Enginyeria t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="upf_estudios_asia_oriental">UPF - Estudis d'&Agrave;sia oriental</option>
            <option value="upf_humanidades">UPF - Humanitats</option>
            <option value="upf_enfermeria">UPF - Infermeria</option>
            <option value="upf_investigacion_tecnicas_mercado">UPF - Investigaci&oacute; i t&egrave;cniques de mercat</option>
            <option value="upf_linguistica">UPF - Ling&uuml;&iacute;stica</option>
            <option value="upf_periodismo">UPF - Periodisme</option>
            <option value="upf_prevencion_riesgos_laborales">UPF - Prevenci&oacute; de riscos laborals</option>
            <option value="upf_publicidad_relaciones_publicas">UPF - Publicitat i relacions p&uacute;bliques</option>
            <option value="upf_relaciones_laborales">UPF - Relacions laborals</option>
            <option value="upf_traduccion_interpretacion_aleman">UPF - Traducci&oacute; i interpretaci&oacute; (alemany)</option>
            <option value="upf_traduccion_interpretacion_ingles">UPF - Traducci&oacute; i interpretaci&oacute; (angl&egrave;s)</option>
            <option value="upf_traduccion_interpretacion_frances">UPF - Traducci&oacute; i interpretaci&oacute; (franc&egrave;s)</option>
            <option value="upf_turismo">UPF - Turisme</option>
            </optgroup>
            <optgroup label="ULL - Univerdidad de Lleida">
            <option value="ull_ade">ULL - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="ull_arquitectura_tecnica">ULL - Arquitectura t&egrave;cnica</option>
            <option value="ull_biotecnologia">ULL - Biotecnologia</option>
            <option value="ull_ciencia_tecnologia_alimentos">ULL - Ci&egrave;ncia i tecnologia dels aliments</option>
            <option value="ull_ciencias_actividad_fisica_deporte">ULL - Ci&egrave;ncies de l'activitat f&iacute;sica i de l'esport</option>
            <option value="ull_ciencias_trabajo">ULL - Ci&egrave;ncies del treball</option>
            <option value="ull_ciencias_empresariales">ULL - Ci&egrave;ncies empresarials</option>
            <option value="ull_comunicacion_audiovisual">ULL - Comunicaci&oacute; audiovisual</option>
            <option value="ull_derecho">ULL - Dret</option>
            <option value="ull_educacion_social">ULL - Educaci&oacute; social</option>
            <option value="ull_ingenieria_agronomica">ULL - Enginyeria agron&ograve;mica</option>
            <option value="ull_ingenieria_forestal">ULL - Enginyeria de forests</option>
            <option value="ull_ingenieria_informatica">ULL - Enginyeria en inform&agrave;tica</option>
            <option value="ull_ingenieria_agricola_explotaciones_agropecuarias">ULL - Enginyeria t&egrave;cnica agr&iacute;cola, especialitat en explotacions agropecu&agrave;ries</option>
            <option value="ull_ingenieria_agricola_hortofructicultura">ULL - Enginyeria t&egrave;cnica agr&iacute;cola, especialitat en hortofructicultura i jardineria</option>
            <option value="ull_ingenieria_agricola_industria_agraria">ULL - Enginyeria t&egrave;cnica agr&iacute;cola, especialitat en ind&uacute;stries agr&agrave;ries i aliment&agrave;ries</option>
            <option value="ull_ingenieria_agricola_mecanizacion_construccion_rural">ULL - Enginyeria t&egrave;cnica agr&iacute;cola, especialitat en mecanitzaci&oacute; i construccions rurals</option>
            <option value="ull_ingenieria_informatica_gestion">ULL - Enginyeria t&egrave;cnica en inform&agrave;tica de gesti&oacute;</option>
            <option value="ull_ingenieria_informatica_sistemas">ULL - Enginyeria t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="ull_ingenieria_forestal_explotaciones_forestales">ULL - Enginyeria t&egrave;cnica forestal, especialitat en explotacions forestals</option>
            <option value="ull_ingenieria_forestal_industrias_forestales">ULL - Enginyeria t&egrave;cnica forestal, especialitat en ind&uacute;stries forestals</option>
            <option value="ull_ingenieria_">ULL - Enginyeria t&egrave;cnica industrial, especialitat en mec&agrave;nica</option>
            <option value="ull_filologia_inglesa">ULL - Filologia anglesa</option>
            <option value="ull_filologia_catalana">ULL - Filologia catalana</option>
            <option value="ull_filologia_francesa">ULL - Filologia francesa</option>
            <option value="ull_filologia_hispanica">ULL - Filologia hisp&agrave;nica</option>
            <option value="ull_geografia">ULL - Geografia</option>
            <option value="ull_historia">ULL - Hist&ograve;ria</option>
            <option value="ull_historia_arte">ULL - Hist&ograve;ria de l'art</option>
            <option value="ull_enfermeria">ULL - Infermeria</option>
            <option value="ull_medicina">ULL - Medicina</option>
            <option value="ull_maestro_especialidad_lengua_extranjera">ULL - Mestre, especialitat de llengua estrangera</option>
            <option value="ull_maestro_especialidad_educacion_especial">ULL - Mestre, especialitat d'educaci&oacute; especial</option>
            <option value="ull_maestro_especialidad_educacion_fisica">ULL - Mestre, especialitat d'educaci&oacute; f&iacute;sica</option>
            <option value="ull_maestro_especialidad_educacion_infantil">ULL - Mestre, especialitat d'educaci&oacute; infantil</option>
            <option value="ull_maestro_especialidad_educacion_musical">ULL - Mestre, especialitat d'educaci&oacute; musical</option>
            <option value="ull_maestro_especialidad_educacion_primaria">ULL - Mestre, especialitat d'educaci&oacute; prim&agrave;ria</option>
            <option value="ull_nutricion_dietetica">ULL - Nutrici&oacute; humana i diet&egrave;tica</option>
            <option value="ull_psicopedagogia">ULL - Psicopedagogia</option>
            <option value="ull_relaciones_laborales">ULL - Relacions laborals</option>
            <option value="ull_trabajo_social">ULL - Treball social</option>
            <option value="ull_turismo">ULL - Turisme</option>
            </optgroup>
            <optgroup label="UG - Universidad de Girona">
            <option value="ug_ade">UG - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="ug_arquitectura">UG - Arquitectura</option>
            <option value="ug_arquitectura_tecnica">UG - Arquitectura t&egrave;cnica</option>
            <option value="ug_biologia">UG - Biologia</option>
            <option value="ug_ciencia_tecnologia_alimentos">UG - Ci&egrave;ncia i tecnologia dels aliments</option>
            <option value="ug_ciencias_ambientales">UG - Ci&egrave;ncies ambientals</option>
            <option value="ug_ciencias_trabajo">UG - Ci&egrave;ncies del treball</option>
            <option value="ug_ciencias_empresariales">UG - Ci&egrave;ncies empresarials</option>
            <option value="ug_criminologia">UG - Criminologia</option>
            <option value="ug_diseño_industrial">UG - Disseny industrial i desenvolupament del producte</option>
            <option value="ug_derecho">UG - Dret</option>
            <option value="ug_economia">UG - Economia</option>
            <option value="ug_educacion_social">UG - Educaci&oacute; social</option>
            <option value="ug_ingenieria_informatica">UG - Enginyeria en inform&agrave;tica</option>
            <option value="ug_ingenieria_indutrial">UG - Enginyeria industrial</option>
            <option value="ug_ingenieria_tecnica_agricola_explotaciones">UG - Enginyeria t&egrave;cnica agr&iacute;cola, especialitat en explotacions</option>
            <option value="ug_ingenieria_tecnica_agricola_industrias_agrarias">UG - Enginyeria t&egrave;cnica agr&iacute;cola, especialitat ind.agr&agrave;ries/aliment&agrave;ries</option>
            <option value="ug_ingenieria_tecnica_informatica_gestion">UG - Enginyeria t&egrave;cnica en inform&agrave;tica de gesti&oacute;</option>
            <option value="ug_ingenieria_tecnica_informatica_sistemas">UG - Enginyeria t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="ug_ingenieria_tecnica_electronica">UG - Enginyeria t&egrave;cnica industrial, especialitat en electr&ograve;nica</option>
            <option value="ug_ingenieria_tecnica_industrial_mecanica">UG - Enginyeria t&egrave;cnica industrial, especialitat en mec&agrave;nica</option>
            <option value="ug_ingenieria_tecnica_industrial_quimica">UG - Enginyeria t&egrave;cnica industrial, especialitat en qu&iacute;mica&nbsp;</option>
            <option value="ug_filologia_catalana">UG - Filologia catalana</option>
            <option value="ug_filologia_hispanica">UG - Filologia hisp&agrave;nica</option>
            <option value="ug_filologia_romanica">UG - Filologia rom&agrave;nica</option>
            <option value="ug_filosofia">UG - Filosofia</option>
            <option value="ug_fisioterapia">UG - Fisioter&agrave;pia</option>
            <option value="ug_geografia">UG - Geografia</option>
            <option value="ug_gestion_administracion_publica">UG - Gesti&oacute; i Administraci&oacute; p&uacute;blica</option>
            <option value="ug_historia">UG - Hist&ograve;ria</option>
            <option value="ug_historia_arte">UG - Hist&ograve;ria de l'art</option>
            <option value="ug_enfermeria">UG - Infermeria</option>
            <option value="ug_maestro_especialidad_lengua_extranjera">UG - Mestre, especialitat de llengua estrangera</option>
            <option value="ug_maestro_especialidad_educacion_fisica">UG - Mestre, especialitat d'educaci&oacute; f&iacute;sica</option>
            <option value="ug_maestro_especialidad_educacion_infantil">UG - Mestre, especialitat d'educaci&oacute; infantil</option>
            <option value="ug_maestro_especialidad_educacion_musical">UG - Mestre, especialitat d'educaci&oacute; musical</option>
            <option value="ug_maestro_especialidad_educacion_primaria">UG - Mestre, especialitat d'educaci&oacute; prim&agrave;ria</option>
            <option value="ug_pedagofia">UG - Pedagogia</option>
            <option value="ug_psicologia">UG - Psicologia</option>
            <option value="ug_psicopedagogia">UG - Psicopedagogia</option>
            <option value="ug_publicidad_relaciones_publicas">UG - Publicitat i relacions p&uacute;bliques</option>
            <option value="ug_quimica">UG - Qu&iacute;mica</option>
            <option value="ug_realizacion_audiovisual_multimedia">UG - Realitzaci&oacute; audiovisual i multim&egrave;dia</option>
            <option value="ug_turismo">UG - Turisme</option>
            </optgroup>  
            <optgroup label="URV - Universitat Rovira i Virgili">
            <option value="urv_arquitectura">URV - Escola T&egrave;cnica Superior d'Arquitectura</option>
            <option value="urv_quimica">URV - Facultat de Qu&iacute;mica</option>
            <option value="urv_enologia">URV - Facultat d'Enologia</option>
            <option value="urv_ciencias_juridicas">URV - Facultat de Ci&egrave;ncies Jur&iacute;diques</option>
            <option value="urv_doctor_many">URV - Centre Universitari Doctor Many&agrave;</option>
            <option value="urv_economica">URV - Facultat de Ci&egrave;ncies Econ&ograve;miques i Empresarials</option>
            <option value="urv_ingenieria_quimica">URV - Escola T&egrave;cnica Superior d'Enginyeria Qu&iacute;mica</option>
            <option value="urv_aviacion">URV - Centre d'Estudis Superiors de l'Aviaci&oacute; (CESDA)</option>
            <option value="urv_letras">URV - Facultat de Lletres</option>
            <option value="urv_enfermeria">URV - Escola Universit&agrave;ria d'Infermeria</option>
            <option value="urv_enfermeria">URV - Escola Universit&agrave;ria d'Infermeria Verge de la Cinta</option>
            <option value="urv_medicina_ciencias_salud">URV - Facultat de Medicina i Ci&egrave;ncies de la Salut</option>
            <option value="urv_psicologia">URV - Facultat de Ci&egrave;ncies de l'Educaci&oacute; i Psicologia</option>
            <option value="urv_relaciones_laborales">URV - Escola Universit&agrave;ria de Relacions Laborals S. Maria Maris</option>
            <option value="urv_turismo">URV - Escola Universit&agrave;ria de Turisme Bettatur</option>
            <option value="urv_turismo">URV - Escola Universit&agrave;ria de Turisme i Oci</option>
            </optgroup>
            <optgroup label="URL - Universitat Ramon Llull">
            <option value="url_ade">URL - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="url_arquitectura">URL - Arquitectura</option>
            <option value="url_arquitectura_tecnica">URL - Arquitectura t&egrave;cnica</option>
            <option value="url_ciencias_actividad_fisica_deporte">URL - Ci&egrave;ncies de l'activitat f&iacute;sica i de l'esport</option>
            <option value="url_ciencias_empresariales">URL - Ci&egrave;ncies empresarials</option>
            <option value="url_comunicacion_audiovisual">URL - Comunicaci&oacute; audiovisual</option>
            <option value="url_diseño">URL - Disseny (URL)</option>
            <option value="url_derecho">URL - Dret</option>
            <option value="url_educacion_social">URL - Educaci&oacute; social</option>
            <option value="url_ingenieria_telecomunicacion">URL - Enginyeria de telecomunicaci&oacute;</option>
            <option value="url_ingenieria_electronica">URL - Enginyeria en electr&ograve;nica</option>
            <option value="url_ingenieria_informatica">URL - Enginyeria en inform&agrave;tica</option>
            <option value="url_ingenieria_industrial">URL - Enginyeria industrial</option>
            <option value="url_ingenieria_quimica">URL - Enginyeria qu&iacute;mica</option>
            <option value="url_ingenieria_tecnica_telecomunicacion">URL - Enginyeria t&egrave;cnica de telecomunicaci&oacute;, especialitat s. de telecomunicaci&oacute;</option>
            <option value="url_ingenieria_tecnica_telecomunicacion_sistemas_electronicos">URL - Enginyeria t&egrave;cnica de telecomunicaci&oacute;, especialitat en sistemes electr&ograve;nics</option>
            <option value="url_ingenieria_tecnica_telecomunicaciones_imagen">URL - Enginyeria t&egrave;cnica de telecomunicaci&oacute;, especialitat en so i imatge</option>
            <option value="url_ingenieria_tecnica_telecomunicaciones_telematica">URL - Enginyeria t&egrave;cnica de telecomunicaci&oacute;, especialitat en telem&agrave;tica</option>
            <option value="url_ingenieria_tecnica_informatica_gestion">URL - Enginyeria t&egrave;cnica en inform&agrave;tica de gesti&oacute;</option>
            <option value="url_ingenieria_tecnica_informatica_sistemas">URL - Enginyeria t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="url_filosofia">URL - Filosofia</option>
            <option value="url_fisioterapia">URL - Fisioter&agrave;pia</option>
            <option value="url_humanidades">URL - Humanitats</option>
            <option value="url_enfermeria">URL - Infermeria</option>
            <option value="url_logopedia">URL - Logop&egrave;dia</option>
            <option value="url_maestro_especialidad_lengua_extranjera">URL - Mestre, especialitat de llengua estrangera</option>
            <option value="url_maestro_especialidad_educacion_especial">URL - Mestre, especialitat d'educaci&oacute; especial</option>
            <option value="url_maestro_especialidad_educacion_fisica">URL - Mestre, especialitat d'educaci&oacute; f&iacute;sica</option>
            <option value="url_maestro_especialidad_educacion_infantil">URL - Mestre, especialitat d'educaci&oacute; infantil</option>
            <option value="url_maestro_especialidad_educacion_musical">URL - Mestre, especialitat d'educaci&oacute; musical</option>
            <option value="url_maestro_especialidad_educacion_primaria">URL - Mestre, especialitat d'educaci&oacute; prim&agrave;ria</option>
            <option value="url_multimedia">URL - Multim&egrave;dia </option>
            <option value="url_nutricion_ditetetica">URL - Nutrici&oacute; humana i diet&egrave;tica</option>
            <option value="url_pedagogia">URL - Pedagogia</option>
            <option value="url_periodismo">URL - Periodisme</option>
            <option value="url_psicologia">URL - Psicologia</option>
            <option value="url_psicopedagogia">URL - Psicopedagogia</option>
            <option value="url_publicidad_relaciones_publicas">URL - Publicitat i relacions p&uacute;bliques</option>
            <option value="url_quimica">URL - Qu&iacute;mica</option>
            <option value="url_relaciones_laborales">URL - Relacions laborals</option>
            <option value="url_trabajo_social">URL - Treball social</option>
            <option value="url_turismo">URL - Turisme</option>
            </optgroup> 
            <optgroup label="UV - Universitat de Val&eacute;ncia">
            <option value="uv_ade">UV - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="uv_biblioteconomia_documentacion">UV - Biblioteconomia  i documentaci&oacute;</option>
            <option value="uv_biotecnologia">UV - Biotecnologia</option>
            <option value="uv_ciencia_tecnologia_alimentos">UV - Ci&egrave;ncia i tecnologia dels aliments</option>
            <option value="uv_ciencias_ambientales">UV - Ci&egrave;ncies ambientals</option>
            <option value="uv_ciencias_actividad_fisica_deporte">UV - Ci&egrave;ncies de l'activitat f&iacute;sica i de l'esport</option>
            <option value="uv_ciencias_empresariales">UV - Ci&egrave;ncies empresarials</option>
            <option value="uv_comunicacion_audiovisual">UV - Comunicaci&oacute; audiovisual</option>
            <option value="uv_diseño">UV - Disseny (UVic)</option>
            <option value="uv_educacion_social">UV - Educaci&oacute; social</option>
            <option value="uv_ingenieria_organizacion_industrial">UV - Enginyeria en organitzaci&oacute; industrial</option>
            <option value="uv_ingenieria_tecnica_agricola_industrias_agrarias">UV - Eng. t&egrave;cnica agr&iacute;cola, especialitat en ind&uacute;stries agr&agrave;ries i aliment&agrave;ries</option>
            <option value="uv_ingenieria_tecnica_telecomunicacion">UV - Eng. t&egrave;cnica de telecomunicaci&oacute;, especialitat en s. de telecomunicaci&oacute;</option>
            <option value="uv_ingenieria_tecnica_informatica_gestion">UV - Eng. t&egrave;cnica en inform&agrave;tica de gesti&oacute;</option>
            <option value="uv_ingenieria_tecnica_informatica_sistemas">UV - Eng. t&egrave;cnica en inform&agrave;tica de sistemes</option>
            <option value="uv_ingenieria_tecnica_industria_electronica">UV - Eng.t&egrave;cnica industrial, especialitat en electr&ograve;nica industrial</option>
            <option value="uv_fisioterapia">UV - Fisioter&agrave;pia</option>
            <option value="uv_enfermeria">UV - Infermeria</option>
            <option value="uv_maestro_especialidad_lengua_extranjera">UV - Mestre, especialitat de llengua estrangera</option>
            <option value="uv_maestro_especialidad_educacion_especial">UV - Mestre, especialitat d'educaci&oacute; especial</option>
            <option value="uv_maestro_especialidad_educacion_fisica">UV - Mestre, especialitat d'educaci&oacute; f&iacute;sica</option>
            <option value="uv_maestro_especialidad_educacion_infantil">UV - Mestre, especialitat d'educaci&oacute; infantil</option>
            <option value="uv_maestro_especialidad_educacion_primaria">UV - Mestre, especialitat d'educaci&oacute; prim&agrave;ria</option>
            <option value="uv_nutricion_dietetica">UV - Nutrici&oacute; humana i diet&egrave;tica</option>
            <option value="uv_periodismo">UV - Periodisme</option>
            <option value="uv_psicopedagogia">UV - Psicopedagogia</option>
            <option value="uv_publicidad_relaciones_publicas">UV - Publicitat i relacions p&uacute;bliques</option>
            <option value="uv_terapia_ocupacional">UV - Ter&agrave;pia ocupacional</option>
            <option value="uv_traduccion_interpretacion_ingles">UV - Traducci&oacute; i interpretaci&oacute; (angl&egrave;s)</option>
            <option value="uv_turismo">UV - Turisme</option>
            </optgroup>      
            <optgroup label="UIC - Universitat Internacional de Catalunya">
            <option value="uic_ciencias_economicas_sociales">UIC - Facultat de Ci&egrave;ncies Econ&ograve;miques i Socials (UIC - Barcelona)</option>
            <option value="uic_arquitectura">UIC - Escola T&egrave;cnica Superior d'Arquitectura (UIC - Barcelona)</option>
            <option value="uic_ciencias_salud">UIC - Facultat de Ci&egrave;ncies de la Salut (UIC - Sant Cugat del Vall&egrave;s)</option>
            <option value="uic_humanidades">UIC - Facultat d'Humanitats (UIC - Barcelona)</option>
            <option value="uic_ciencias_juridicas_politicas">UIC - Facultat de Ci&egrave;ncies Jur&iacute;diques i Pol&iacute;tiques (UIC - Barcelona)</option>
            <option value="uic_escuela_diseno">UIC - LAi , Escola Superior de Disseny (UIC - Barcelona)</option>
            <option value="uic_tecnologias_informacion">UIC - Escola Universit&agrave;ria de Tecnologies de la Informaci&oacute; i Comunicaci&oacute;&nbsp;</option>
            <option value="uic_ciencias_salud_sant_cugat">UIC - Facultat de Ci&egrave;ncies de la Salut (UIC - Sant Cugat del Vall&egrave;s)</option>
            </optgroup>
            <optgroup label="UAO - Universitat Abat Oliba">
            <option value="uao_ade">UAO - Administraci&oacute; i direcci&oacute; d'empreses</option>
            <option value="uao_ciencias_politicas">UAO - Ci&egrave;ncies pol&iacute;tiques i de l'administraci&oacute;</option>
            <option value="uao_derecho">UAO - Dret</option>
            <option value="uao_economia">UAO - Economia</option>
            <option value="uao_ingenieria_informatica">UAO - Enginyeria en inform&agrave;tica</option>
            <option value="uao_periodismo">UAO - Periodisme</option>
            <option value="uao_psicologia">UAO - Psicologia</option>
            <option value="uao_publicitat_relaciones_publicas">UAO - Publicitat i relacions p&uacute;bliques</option>
            </optgroup>
       	</select>
            
        <p id="zona_asignatura">&nbsp;</p>
        <p id="zona_profesor">&nbsp;</p>
   		<p id="zona_upload">
        	Comentario :<textarea name="comentario" rows="5" cols="98"></textarea><input type="checkbox" name="aceptacion" checked="checked" style="float:left;">Acepto las condiciones para entrar en los sorteos
            <br /><br /><input type="file" name="archivo" style="margin:16px 0 0 0;" size="96" /><b>(Tama&ntilde;o m&aacute;ximo 10Mb.)</b>
        </p>
        <p id="zona_progreso">
            <span id="text_aviso" style="visibility:hidden;">Por favor, espere mientras se sube el archivo. No cancele ni actualice la pagina durante el proceso.</span>
            <table><tr><td>
            <div id="showbar" style="font-size:8pt;padding:2px;border:1px solid #7f9db9;visibility:hidden">
            <span id="progress1">&nbsp; &nbsp;</span> <span id="progress2">&nbsp; &nbsp;</span> <span id="progress3">&nbsp; &nbsp;</span> <span id="progress4">&nbsp; &nbsp;</span>
            <span id="progress5">&nbsp; &nbsp;</span> <span id="progress6">&nbsp; &nbsp;</span> <span id="progress7">&nbsp; &nbsp;</span> <span id="progress8">&nbsp; &nbsp;</span>
            <span id="progress9">&nbsp; &nbsp;</span> <span id="progress10">&nbsp; &nbsp;</span> <span id="progress11">&nbsp; &nbsp;</span> <span id="progress12">&nbsp; &nbsp;</span>
            <span id="progress13">&nbsp; &nbsp;</span> <span id="progress14">&nbsp; &nbsp;</span> <span id="progress15">&nbsp; &nbsp;</span> <span id="progress16">&nbsp; &nbsp;</span>
            <span id="progress17">&nbsp; &nbsp;</span> <span id="progress18">&nbsp; &nbsp;</span> <span id="progress19">&nbsp; &nbsp;</span> <span id="progress20">&nbsp; &nbsp;</span>
            <span id="progress21">&nbsp; &nbsp;</span> <span id="progress22">&nbsp; &nbsp;</span> <span id="progress23">&nbsp; &nbsp;</span> <span id="progress24">&nbsp; &nbsp;</span>
            <span id="progress25">&nbsp; &nbsp;</span> <span id="progress26">&nbsp; &nbsp;</span> <span id="progress27">&nbsp; &nbsp;</span> <span id="progress28">&nbsp; &nbsp;</span>
            <span id="progress29">&nbsp; &nbsp;</span> <span id="progress30">&nbsp; &nbsp;</span> <span id="progress31">&nbsp; &nbsp;</span> <span id="progress32">&nbsp; &nbsp;</span>
            </div>
            </td></tr></table>
            <script language="javascript">
            var progressEnd = 32;
            var progressColor = '#336699';
            var progressInterval = 2000; // set to time between updates (milli-seconds)
            var progressAt = progressEnd;
            var progressTimer;
            function progress_clear() 
            {
                for (var i = 1; i <= progressEnd; i++) 
                    document.getElementById('progress'+i).style.backgroundColor = 'transparent';
                progressAt = 0;
            }
            function progress_update() 
            {
				document.getElementById('text_aviso').style.visibility = 'visible';
                document.getElementById('showbar').style.visibility = 'visible';
                progressAt++;
                if (progressAt > progressEnd) 
                    progress_clear();
                else 
                    document.getElementById('progress'+progressAt).style.backgroundColor = progressColor;
                progressTimer = setTimeout('progress_update()',progressInterval);
            }
            function progress_stop()
            {
                clearTimeout(progressTimer);
                progress_clear();
                document.getElementById('showbar').style.visibility = 'hidden';
            }
            </script>
        </p>
        <p id="zona_ultima">
        	<input type="submit" style="margin:8px 0 0 256px;" onclick="progress_update()" value="    SUBIR    " />
        </p>
	    </form>
        </p>
    
		<? } else {
			echo "<h2>NO estas identificado como un usuario de tengoapuntes.com</h2><p>Si deseas logarte puedes hacerlo <a href='http://www.tengoapuntes.com/foro/ucp.php?mode=login'><em>aqui</em></a>.</p>";	
		}
	}?>
   </div> 
<?php include("0piecera.php");?>