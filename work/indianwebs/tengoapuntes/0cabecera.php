<?php 
include("funciones.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
	<title>Tengo Apuntes Buscar y compartir apuntes universitarios</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)" />
    <meta name="revisit" content="7 days" />
    <meta name="robots" content="index,follow,all" />
	<meta name="description" content="Tengo Apuntes Buscar y compartir apuntes de la universidad. Sube y descarga tu propios apuntes." />
	<meta name="keywords" content="apuntes, buscar apuntes, busca apuntes, busqueda apuntes,apuntes universidad, descargar apuntes,apuntes universitarios,compartir apuntes, subir apuntes" />
    <meta name="lang" content="es,sp,spanish,español,espanol,castellano" />
    <meta name="author" content="www.indianwebs.com" />
	<link rel="stylesheet" type="text/css" href="estilos.css" />
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
    <script language="javascript" type="text/javascript" src="funciones_asignaturas.js"></script>
    <script language="javascript" type="text/javascript" src="funciones_profesores.js"></script>
</head>

<body>
<!-- oncontextmenu="return false" onkeydown="return false" -->
<div class="botonera">
    <a title="TENGO APUNTES Quienes Somos" href="http://www.tengoapuntes.com/quienes-somos.php">&raquo;qui&eacute;nes somos</a>
    <a title="TENGO APUNTES Busca apuntes" href="http://www.tengoapuntes.com/index.php">&raquo;buscar apuntes</a>
    <a title="TENGO APUNTES Subir apuntes" href="http://www.tengoapuntes.com/upload.php">&raquo;subir apuntes</a>
    <a title="TENGO APUNTES Foro" href="http://www.tengoapuntes.com/foro/">&raquo;foro</a>
    <a title="TENGO APUNTES Enlaces" href="http://www.tengoapuntes.com/enlaces.php">&raquo;enlaces</a>
    <a title="TENGO APUNTES Agradecimientos" href="http://www.tengoapuntes.com/agradecimientos.php">&raquo;agradecimientos</a>
    <a title="TENGO APUNTES Contacta" href="http://www.tengoapuntes.com/contacta.php">&raquo;contacta</a>
    <?php if($_COOKIE["phpbb3_h6fpd_u"]>1)
		nombre();
	else
		echo '<a title="TENGO APUNTES Registrate" href="http://www.tengoapuntes.com/foro/ucp.php?mode=login">&raquo;reg&iacute;strate</a>';
	?>
</div>

<? if($_SERVER['PHP_SELF']!="/index.php") { ?>
<div class="arriba">
	<a href="http://www.tengoapuntes.com"><img title="Tengo apuntes" src="img/tengoapuntes.gif" alt="Tengo apuntes"  border="0" align="left" style="margin:0 32px 0 0;"/></a>
    	
    <form name="form_busqueda" method="post" action="apuntes.php" style="margin:16px 0 0 0;">
    <input name="nom_busqueda" type="text" size="64" maxlength="64" />
   	<input type="submit" value="  buscar  " />
    </form>
</div>	
<? } ?>