<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Copia.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="tallas grandes, talla grande, moda tallas grandes, coleccion tallas grandes, lineas tallas grandes, modelos tallas grandes, comprar tallas grandes, tienda tallas grandes." />
	<meta name="description" content="Diviertete Tallas Grandes - Coleccion Diviertete Tallas Grandes y Andrea Bosse. Colecciones y tiendas de tallas grandes. Moda de tallas grandes actual. Compra nuestras de tallas grandes." />
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>DIVIERTETE TALLAS GRANDES - Moda de tallas grandes, tallas Grandes, tienda de tallas grandes...</title>
	<!-- InstanceEndEditable -->
	<link rel="stylesheet" type="text/css" href="estilos.css" />
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
    <!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>
<table id="central" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td>
		<a href="index.htm"><img src="img/cabecera1.jpg" alt="Tallas Grandes Andrea Bosse" border="0"/></a>
	</td>
	<td>
		<a href="index.htm"><img src="img/cabecera2.jpg" alt="Tallas Grandes Diviertete" border="0"/></a>
	</td>
	<td class="top3" valign="top">
		<a href="colecciones.php" class="bt1"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;colecciones</a>
		<a href="novedades.php" class="bt2"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;eventos</a>
		<a href="tiendas.htm" class="bt3"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;tiendas</a>
		<a href="comprar.htm" class="bt4"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;comprar</a>
		<a href="clientes.php" class="bt5"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;area clientes</a>
	</td>
</tr>
<tr>
	<!-- InstanceBeginEditable name="lateral" -->
	<td class="fila1">
		<img src="img/lateral1.jpg" alt="Tallas Grandes" border="0" />
	</td>
	<!-- InstanceEndEditable -->
	
	<td class="fila2">&nbsp;</td>
	
	<!-- InstanceBeginEditable name="cuerpo" -->
   	<td id="fila3">
	<?php
	switch($_GET['seccion'])
	{
		case "andreabosse":
		$texto="<h1><img src='img/cuadro2.gif' border='0'/>&nbsp;COLECCI&Oacute;N ANDREA BOSS&Eacute;</h1>
		<a href='colecciones.php'>Atr&aacute;s...</a><br><br>	
		<cite><a href='#'><img src='img/andreabosse1.gif' alt='Coleccion Andrea Bosse' border='0'></a></cite>
		<cite><a href='#'><img src='img/andreabosse2.gif' alt='Coleccion Andrea Bosse' border='0'></a></cite>
		<cite><a href='#'><img src='img/andreabosse3.gif' alt='Coleccion Andrea Bosse' border='0'></a></cite>
		<p><img src='img/pdf.gif'>&nbsp;";
		if($_SESSION['autentico']==1)
			$texto=$texto."<a href='files/andrea_bosse.pdf'>";
		else
			$texto=$texto."<a href='clientes.php'>";
		$texto=$texto."<b>Colecci&oacute;n Andrea Boss&eacute; Tallas Grandes 2007</b></a><br>Reg&iacute;strate para poder descargarte nuestro cat&aacute;logo.<br></p>";
		echo $texto;
		break;
		
		case "diviertete":
		$texto="<h1><img src='img/cuadro2.gif' border='0'/>&nbsp;COLECCI&Oacute;N DIVIERTETE</h1>
		<p><a href='colecciones.php'>Atr&aacute;s...</a></p>
		<cite><a href='#'><img src='img/diviertete1.gif' alt='Coleccion Tallas Grandes Diviertete' border='0'></a></cite>
		<cite><a href='#'><img src='img/diviertete2.gif' alt='Coleccion Tallas Grandes Diviertete' border='0'></a></cite>
		<cite><a href='#'><img src='img/diviertete3.gif' alt='Coleccion Tallas Grandes Diviertete' border='0'></a></cite> 	 
		<p><img src='img/pdf.gif'>&nbsp;";
		if($_SESSION['autentico']==1)
			$texto=$texto."<a href='files/catalogo_primavera07.pdf'>";
		else
			$texto=$texto."<a href='clientes.php'>";
		$texto=$texto."<b>Colecci&oacute;n Diviertete Tallas Grandes 2007</b></a><br>Reg&iacute;strate para poder descargarte nuestro cat&aacute;logo.</p>";
		echo $texto;
		break;
		
		case "novedades":
		$texto="<h1><img src='img/cuadro2.gif' border='0'/>&nbsp;NOVEDADES DIVIERTETE</h1>
		<p><a href='colecciones.php'>Atr&aacute;s...</a></p>
		<cite><a href='#'><img src='img/novedad1.jpg' alt='Novedades Tallas Grandes Diviertete' border='0'></a></cite>
		<cite><a href='#'><img src='img/novedad2.jpg'' alt='Novedades Tallas Grandes Diviertete' border='0'></a></cite>
		<cite><a href='#'><img src='img/novedad3.jpg'' alt='Novedades Tallas Grandes Diviertete' border='0'></a></cite> 	 
		<p><img src='img/pdf.gif'>&nbsp;";
		if($_SESSION['autentico']==1)
			$texto=$texto."<a href='files/catalogo_novedades07.pdf'>";
		else
			$texto=$texto."<a href='clientes.php'>";
		$texto=$texto."<b>Novedades Diviertete Tallas Grandes 2007</b></a><br>Reg&iacute;strate para poder descargarte nuestro cat&aacute;logo.</p>";
		echo $texto;
		break;
	
		default:
		echo "<h1><img src='img/cuadro2.gif' border='0'>&nbsp;COLECCIONES</h1>
		<h4><img src='img/cuadro3.gif'>&nbsp;Nuestras lines de tallas grandes :</h4>
		<span><a href='colecciones.php?seccion=andreabosse'><img src='img/coleccion1.jpg' border='0' alt='Coleccion Tallas Grandes Andrea Bosse'></a></span>
		<span><a href='colecciones.php?seccion=diviertete'><img src='img/coleccion2.jpg' border='0' hspace='2' alt='Coleccion Tallas Grandes Diviertete'></a></span>
		<span><a href='colecciones.php?seccion=novedades'><img src='img/coleccion3.jpg' border='0' alt='Novedades Tallas Grandes Diviertete'></a></span>";
		break;
	}
		?>
    
    </td>
    <!-- InstanceEndEditable -->
</tr>
<tr>
	<td colspan="3" class="contacto">Contacta con nosotros en <a href="mailto:info@diviertetetallasgrandes.com">info@diviertetetallasgrandes.com</a>, por tel&eacute;fono <em>93.485.60.21</em> o en <em>c/Llull n�51, 5� 3era 08005 Barcelona</em></td>
</tr>
</table>
</body>
<!-- InstanceEnd --></html>
