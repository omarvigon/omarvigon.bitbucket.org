<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Copia.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="keywords" content="tallas grandes, talla grande, moda tallas grandes, coleccion tallas grandes, lineas tallas grandes, modelos tallas grandes, comprar tallas grandes, tienda tallas grandes." />
	<meta name="description" content="Diviertete Tallas Grandes - Coleccion Diviertete Tallas Grandes y Andrea Bosse. Colecciones y tiendas de tallas grandes. Moda de tallas grandes actual. Compra nuestras de tallas grandes." />
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>DIVIERTETE TALLAS GRANDES - Moda de tallas grandes, tallas Grandes, tienda de tallas grandes...</title>
	<!-- InstanceEndEditable -->
	<link rel="stylesheet" type="text/css" href="estilos.css" />
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
    <!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body onload="parpadeo()">
<table id="central" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td>
		<a href="index.htm"><img src="img/cabecera1.jpg" alt="Tallas Grandes Andrea Bosse" border="0"/></a>
	</td>
	<td>
		<a href="index.htm"><img src="img/cabecera2.jpg" alt="Tallas Grandes Diviertete" border="0"/></a>
	</td>
	<td class="top3" valign="top">
		<a href="colecciones.php" class="bt1"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;colecciones</a>
		<a href="novedades.php" class="bt2"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;eventos</a>
		<a href="tiendas.htm" class="bt3"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;tiendas</a>
		<a href="comprar.htm" class="bt4"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;comprar</a>
		<a href="clientes.php" class="bt5"><img src="img/cuadro1.gif" border="0" vspace="4" align="absmiddle">&nbsp;area clientes</a>
	</td>
</tr>
<tr>
	<!-- InstanceBeginEditable name="lateral" -->
	<td class="fila1">
		<img src="img/lateral2.jpg" alt="Moda de Tallas Grandes" border="0" />
	</td>
	<!-- InstanceEndEditable -->
	
	<td class="fila2">&nbsp;</td>
	
	<!-- InstanceBeginEditable name="cuerpo" -->
   	<td id="fila3">
	<h1><img src='img/cuadro2.gif' border="0"/>&nbsp;EVENTOS</h1>
	<?php
	switch($_GET['seccion'])
	{
		case "noticia1":
		echo "<a href='novedades.php'>Atr&aacute;s...</a><br>
   		<h2>Tete Delgado, es la protagonista de la obra de teatro : GORDA!</h2>
		<h4>Teatro Alc&aacute;zar : Gorda<br>Con un reparto de lujo y bajo la  direcci&oacute;n de Tamzin Townsend llega la obra del afamado Neil LaBute</h4>
		<img src='img/noticia1a.jpg' align='right'>
   	 	<p>El Teatro Alc&aacute;zar acoge la segunda temporada de <em>Gorda</em>, de Neil LaBute. Protagonizada por Luis Merlo, Tete Delgado, I&ntilde;aki Miramon y Alicia Mart&iacute;nez y dirigida por Tamzin Townsend, la historia analiza a trav&eacute;s del humor el culto al cuerpo que existe en la sociedad actual.</p>
    	<p><b>LaBute</b> plantea en esta pieza lo que ocurre cuando un chico guapo, sofisticado y lleno de buenas intenciones se sorprende a s&iacute; mismo enamor&aacute;ndose de una mujer con un considerable sobrepreso.</p>
    	<p><b>Tete Delgado</b> es Helena, una chica inteligente, divertida y sincera que usa una talla XXL. Cansada de ser siempre la amiga graciosa y nunca la novia, intenta convencer a Tony, interpretado por <b>Luis Merlo</b>, de que el mundo de los sentimientos est&aacute; por encima de las apariencias. Sin embargo, cuando Tony presenta a Helena a su c&iacute;rculo de amistades, estos empiezan a re&iacute;rse de ella y tambi&eacute;n de &eacute;l. A partir de ese instante empiezan los problemas y las   dudas: permanecer junto a la mujer que quiere o dejarla por convencionalismos sociales.</p>
		<p>En la &eacute;poca de los cuerpos perfectos, de los gimnasios y de los alimentos ligeros, Tony tiene que ir a la contra y tomar una decisi&oacute;n que ser&aacute; trascendental para su propia felicidad.</p>
    	<h4>DATOS DE INTER&Eacute;S</h4>
    	<em><b>Gorda<br></b></em>Segunda temporada a partir del 6 de Septiembre<br>
    	Teatro Alc&aacute;zar<br>Alcal&aacute;, 20</a><br>Tel&eacute;fono. 91 701 02 30<br>Metro:   Sevilla (L2)<br>
    	Horario: mi&eacute;rcoles y jueves a las 20:00 horas, viernes y s&aacute;bados a las 20:00 y a las 22:30 horas y domingos a las 19:00 horas. Lunes y martes, descanso<br>
    	Venta de entradas: en taquilla, en <a href='http://www.elcorteingles.es/entradas' target='_blank'>www.elcorteingles.es/entradas</a> y en los tel&eacute;fonos 902 262 726 y 902 400 222<br>Precio: de 15 &euro; a 24 &euro;
		<p><b>Del Director</b></p>
		<img src='img/noticia1b.jpg' align='left' hspace='4'>
    	<p>El dramaturgo, director y guionista <b>Neil LaBute</b>, considerado uno de los talentos m&aacute;s brillantes, controvertidos y provocadores del cine norteamericano actual, despedaza el comportamiento de la sociedad hacia sus miembros menos &ldquo;perfectos&rdquo;. En <b><em>Gorda</em></b>, el autor hace re&iacute;r al espectador pero tambi&eacute;n hace que tome conciencia de una realidad injusta. Con esta obra, que tras una gira por todo el pa&iacute;s llega a los escenarios madrile&ntilde;os,&nbsp;pretende cuestionar esta b&uacute;squeda de la perfecci&oacute;n y superar todos los tab&uacute;es existentes a trav&eacute;s del humor y la iron&iacute;a, gracias a la cercan&iacute;a de los cuatro personajes de <b><em>Gorda</em></b> con el p&uacute;blico y a&nbsp;la brillantez de sus di&aacute;logos y situaciones.</p>
   	 	<p>Los actores <b>Luis Merlo, I&ntilde;aki Miram&oacute;n, Tet&eacute; Delgado</b> y <b>Alicia Mart&iacute;nez</b> (que ha sustituido en esta temporada a <b>Lidia Ot&oacute;n)</b> forman el reparto de <b><em>Gorda</em></b> bajo las &oacute;rdenes de la directora inglesa <b>Tamzin Townsend</b>, que alcanz&oacute; el  &eacute;xito en nuestro pa&iacute;s con la renombrada obra <b><em>El m&eacute;todo Gr&ouml;nholm.</em></b> La cantante y compositora espa&ntilde;ola <b>Mercedes Ferrer</b> es la encargada de poner el toque musical a esta entretenida comedia que&nbsp;vive su segunda temporada&nbsp;en el Teatro Alc&aacute;zar</p>";
		break;
		
		case "noticia2":
		echo "<a href='novedades.php'>Atr&aacute;s...</a><br>
    	<h2>Tete Delgado muestra los Modelos de la Colecci�n</h2>
		<p>Tete Delgado muestra sus Modelos de la Colecci�n en cada una de sus apariciones en los medios de comunicaci�n. 
		<p>Al igual, que cada lunes en:
		<ul>
		<li>Programa de Chanel 4, en la cadena de televisi�n Quatro</li>
		<li>Programa de Mira qui�n Baila, en la cadena de televisi�n de la Primera</li>
		</ul>";
		break;
		
		case "noticia3":
		echo "<a href='novedades.php'>Atr&aacute;s...</a><br>
		<h2>Entrevista Tete Delgado en Feria SIMM</h2>
		<h4>La simp&aacute;tica actriz presenta sus dise&ntilde;os de la marca 'Diviertete' en la semana internacional de la moda en madrid(SIMM)</h4>	
		<img src='img/noticia3.jpg' align='right' hspace='2' border='0'>
		<p>Qui&eacute;n le iba a decir a Tet&eacute; Delgado que ver&iacute;a su propia l&iacute;nea de ropa en la   Semana Internacional de la Moda de Madrid (SIMM), pero as&iacute; ha sido.&nbsp;&nbsp;Diviertete , la firma de tallas grandes de la que ella es imagen, ten&iacute;a un stand en esta  feria con los mejores dise&ntilde;os de la colecci&oacute;n. Dieferentes tejidos mezclados   entre s&iacute;, colores discretos y atrevidos, cortes favorecedores... y todo con un   &uacute;nico fin: vestir a las&nbsp;&nbsp;grandes de Espa&ntilde;a&nbsp;&nbsp;con la moda m&aacute;s actual. </p>
		<p>La simp&aacute;tica actriz -que actualmente ejerce de jurado en el exitoso concurso de   televisi&oacute;n&nbsp;&nbsp;&iexcl;Mira qui&eacute;n baila! , y que en teatro la hemos podido ver   en&nbsp;&nbsp;Gorda&nbsp;&nbsp;junto a Luis Merlo- nos muestra los mejores dise&ntilde;os de su colecci&oacute;n y   nos comenta c&oacute;mo ha sido la experiencia de crear su propia ropa. </p>	
		<h3>&iquest;C&oacute;mo te sientes en el primer d&iacute;a de presentaci&oacute;n de tu l&iacute;nea de ropa?</h3>
		Estoy bastante nerviosa, porque esto para m&iacute; es aboslutamente novedoso.   El mundo de la moda lo ve&iacute;a desde lejos y dec&iacute;a:&nbsp;&nbsp;Esto no me lo puedo poner,   esto tampoco , entonces ya ni lo ve&iacute;a mucho. Pero ahora tengo la oportunidad de   decir:&nbsp;&nbsp;Mira, esto es lo que me apetece, pues vamos a hacerlo. Estoy como una   ni&ntilde;a con zapatos nuevos: muy contenta y muy ilusionada.	
		<h3>&iquest;Qu&eacute; te ha impulsado ha dar este osado y valiente paso en el mundo   empresarial?</h3>
		S&iacute;, es mucha osad&iacute;a, pero la comparto al cincuenta por   ciento con Xavier Baz&aacute;n, &eacute;l ha sido el osado que me lo ha propuesto. Yo   reconozco que lo ten&iacute;a en la cabeza desde hace tiempo, pero pensaba:&nbsp;&nbsp;A ver   d&oacute;nde voy a encontrar al descerebrado que se arriesgue conmigo de esta manera. Y lo encontr&eacute; en Barcelona. Me apetec&iacute;a much&iacute;simo, porque me encuentro un mont&oacute;n   de gordas por la calle, como yo, que me dicen que no hay ropa. Yo reconozco que   cada vez hay m&aacute;s, y la mayor&iacute;a viene de Catalu&ntilde;a. Por eso, ahora me apetece   decirle a esa gente que la vamos a hacer.	
		<h3>&iquest;Qui&eacute;n dise&ntilde;a la ropa?</h3>
		Hay dos vertientes, una viene del equipo de   Xavier; y luego contamos con la colaboraci&oacute;n de Manu Fern&aacute;ndez, un dise&ntilde;ador   estupendo que nos hace parte de la colecci&oacute;n.	
		<h3>&iquest;C&oacute;mo es&nbsp;&nbsp;Diviertete&nbsp;&nbsp;</h3>
		Es bastante desenfadado, muy ponible y con un   puntito arriesgado. Subimos el largo de las faldas, abrimos los escotes,   entallamos un poco m&aacute;s, siempre dentro del can&oacute;n que nos favorezca a nosotras,   porque es ropa de tallas grandes, para grandes de Espa&ntilde;a. Estamos muy acostumbrados a tapar a la gorda, pues no, &iexcl;favorezcamos a la gorda! (r&iacute;e).	
		<h3>&iquest;Qu&eacute; tallas trabajais?</h3>
		Tenemos desde la 48 hasta la 64. Menos no, las   otras que se busquen la vida en otras tiendas (risas). De momento no hay menos,   pero tengo la intenci&oacute;n de incluir tambi&eacute;n la 46, que est&aacute; muy desprotegida, no   es ni talla grande ni peque&ntilde;a. Si esto va bien abriremos tambi&eacute;n en ba&ntilde;o y lencer&iacute;a.	
		<h3>&iquest;A ti cu&aacute;l es la prenda que m&aacute;s te gusta?</h3>
		Yo tengo especial inter&eacute;s   por un vestido de noche dise&ntilde;ado por Manu. Es maravilloso (muestra un vestido   largo en tonos marrones y con capas hasta el suelo).	
		<h3>&iquest;Qu&eacute; colecci&oacute;n presentais ahora?</h3>
		Tenemos invierno y un peque&ntilde;o avance   de verano. Por ejemplo, tenemos ahora unos vestidos ibicencos blancos, con los   que espero que esas grandes de Espa&ntilde;a que en un momento dado no se arriesgaban a   ponerse seg&uacute;n qu&eacute; cosas porque les daba cierto pudor -como el blanco-, vean que   queda muy bonito, que favorece much&iacute;simo. Ese es un tipo de moda que est&aacute;bamos   condenadas a no poder ponernos. Pues ahora s&iacute;. Y luego tenemos la colecci&oacute;n que   va a salir en esta Primavera-Verano, hay una ropa con estampados setenteros que   me gusta much&iacute;simo. Yop creo que a las grandes nos favorece mucho m&aacute;s la ropa   que entalle un poquito, que no esa que nos hace parecer una mesa camilla.	
		<h3>Si nos decidimos por una prenda, &iquest;a d&oacute;nde tenemos que acudir?</h3>
		Xavier   tiene otra marca que se llama Andrea Boss&eacute; , as&iacute; que se aprovecha toda esa   distribuci&oacute;n que ya hab&iacute;a. Por otro lado, a trav&eacute;s de la p&aacute;gina web <a href='http://www.diviertetetallasgrandes.com/' target='_blank'><em>www.diviertetetallasgrandes.com</em></a> se puede contactar con los puntos de venta, o ver la ropa en la misma p&aacute;gina y comprar desde Internet.	
		<h3>&iquest;C&oacute;mo te sientes ejerciendo de modelo?</h3>
		Me siento muy bien poniendo la   imagen de esta ropa, porque estoy muy contenta con ella. Desde que empec&eacute; una   vez en Cibeles, que ah&iacute; s&iacute; que estaba muerta, en la Semana de la Moda. Me tocaba   abrir el desfile y me acuerdo que fue con el&nbsp;&nbsp;Let's Dance&nbsp;&nbsp;de David Bowie, y como me sab&iacute;a la canci&oacute;n lo abr&iacute; de forma espectacular, y dije:&nbsp;&nbsp;Ahora que sea   lo que Dios quiera . De repente hubo un aplauso y una acogida muy bonita. A   partir de eso dije:&nbsp;&nbsp;Se puede . Yo no soy modelo profesional, pero s&eacute; andar y s&eacute;   tener cierta gracia.
		<h3>&iquest;C&oacute;mo te sientes en las sesiones de fotos?</h3>
		En las fotos fue muy   divertido. Yo preguntaba c&oacute;mo me ten&iacute;a que poner, y Manu, que estaba por all&iacute;,   me dec&iacute;a lo que ten&iacute;a que ir haciendo, o me dec&iacute;a:&nbsp;&nbsp;&iexcl;M&aacute;s glamour! . Cuando me   dejan jugar, me resulta mucho m&aacute;s f&aacute;cil, pero cuando estoy en el medio de la nada y de repente tengo que posar, no s&eacute; qu&eacute; hacer.
		<h3>Con&nbsp;&nbsp;Diviertete&nbsp;&nbsp;habeis hecho un juego de palabras...</h3>
		Ah... &iexcl;Lo has   pillado! (risas). El logotipo viene de un dibujo que me hizo una vez un amigo y   lo guard&eacute; porque sab&iacute;a que en un futuro servir&iacute;a para algo.   Y&nbsp;&nbsp;Diviertete&nbsp;&nbsp;porque es una ropa para pas&aacute;rselo bien, es ponible... Y tambi&eacute;n  porque contiene mi nombre. Lo que no iba a poner es&nbsp;&nbsp;Tet&eacute; Delgado Designer ,   esas cosas no porque yo puedo contribuir al dise&ntilde;o dando alguna opini&oacute;n, pero no   soy dise&ntilde;adora, soy actriz.
		<h3>Me comentabas que barajais la posibilidad de sacar ropa de lencer&iacute;a, de   ba&ntilde;o... &iquest;teneis pensado dise&ntilde;ar accesorios?</h3>
		Eso est&aacute; todo pendiente,   incluso gafas, zapatos con anchos especiales, con refuerzos en el tac&oacute;n, con una   ca&ntilde;a m&aacute;s ancha... Lo que pasa que primero queremos ver c&oacute;mo va esto. &iexcl;Esperemos   que bien! Pero vamos tranquilamente porque el cuento de&nbsp;&nbsp;La lechera&nbsp;&nbsp;es una ense&ntilde;anza.";
		break;
		
		case "noticia4":
		echo "<a href='novedades.php'>Atr&aacute;s...</a><br>
		<h2>Art&iacute;culo de El Peri&oacute;dico de Catalun&ntilde;a 21/Mayo/2007 - Vanguardia en tallas grandes</h2><img src='img/noticia5.jpg' align='right' hspace='8'>
		<p>La moda en tallas grandes ya no est&aacute; re&ntilde;ida con las &uacute;ltimas tendencias. No hace tantos a&ntilde;o que la peque&ntilde;a confecci&oacute;n de gran tallaje ofrec&iacute;a dise&ntilde;os obsoletos y aburridos. Ahora la cosa ha cambiado. Cada vez hay m&aacute;s dise&ntilde;os de vanguardia, con una calidad y diversidad de prendas que la sit&uacute;an en un mercado en plena efervescencia. Este es el caso de Divi&eacute;rtete, una firma de moda de apenas un a&ntilde;o, que en mujeres va de la talla 48 a la 66 y en hombres, de la 54 a la 80.</p>
		<p>La colecci&oacute;n estival para la mujer, cuya imagen promocional representa la actriz Tet&eacute; Delgado, se caracteriza por prendas c&oacute;modas y favorecedoras confeccionadas a base de materias ligeras con gran calidad en los acabados.Por un lado, destacan dise&ntilde;os informales coloristas y atrevidos, as&iacute; como modelos extremados pensados para una mujer sexy. Estas propuestas se pueden adquirir en la tienda Andrea Boss&eacute; &amp; Divi&eacute;rtete (call de Pau Claris, 89 esquina Gran Via. Tel&eacute;fono: 933.027.973).</p>
		<p>MIREYA ROCA</p>";
		break;
		
		case "noticia5":
		echo "<a href='novedades.php'>Atr&aacute;s...</a><br>
		<h2>Art&iacute;culo de El Peri&oacute;dico de Catalun&ntilde;a 27/Mayo/2007 - La ropa de Tet&eacute; Delgado</h2><img src='img/noticia6.jpg' align='right' hspace='8'>
		<p>&laquo;Qu&eacute; dif&iacute;cil es ser humilde, cuando una es... &iexcl;tan grande!&raquo; Esta frase aparece en una de las camisetas XXL de la l&iacute;nea de ropa que lleva la firma de Tet&eacute; Delgado. Divi&eacute;rtete, que as&iacute; se llama la marca, juega con el nombre y la imagen de la protagonista de la obra de teatro Gorda para romper estereotipos y complejos. La colecci&oacute;n se present&oacute; el pasado febrero en el Sal&oacute;n Internacional de la Moda de Madrid y, desde hace dos meses, se exhibe en una tienda propia, ubicada en Pau Claris con Gran Via.</p>
		<p>La idea surgi&oacute; de una empresa catalana que vio la necesidad de un cambio radical del bueno: llevar la moda a las tallas grandes (de la 48 a la 66). Y pensaron en la desenfadada actriz como modelo de sus prendas. &laquo;Tet&eacute; cuenta con esa vitalidad, ese punto sexy y fresco que tambi&eacute;n tienen nuestras creaciones &raquo;, explica Eva Bertomeu, responsable de esta transformaci&oacute;n. Estampados retro, vestidos de colores y con blondas, faldas con serigraf&iacute;as metalizadas, camisetas de rayas, piratas, tejanos... La tienda tambi&eacute;n tiene otra l&iacute;nea de ropa, la Andrea Boss&eacute;, dirigida a mujeres m&aacute;s indiferente a las vanguardias. Tanto entusiasma la propuesta, que se est&aacute;n planteando abrir franquicias.</p>
		<p>ELISABETH FUENTES</p>";
		break;
		
		default:
		echo "
		<h5><a href='files/video_desfilechanneln4.rar'><img src='img/movieicon.png' align='absmiddle' hspace='2' border='0'>��Mira el desfile de Channel N�4!! - Desc�rgalo aqu&iacute;<img src='img/movieicon.png' hspace='2' align='absmiddle' border='0'></a></h5>
		<h4><img src='img/cuadro3.gif'>&nbsp;Mayo 2007</h4>
		<ul>
		<li><a href='novedades.php?seccion=noticia5'><em>Art&iacute;culo de El Peri&oacute;dico de Catalun&ntilde;a 27/Mayo/2007 - La ropa de Tet&eacute; Delgado</em></a><br>
		&laquo;Qu&eacute; dif&iacute;cil es ser humilde, cuando una es... &iexcl;tan grande!&raquo; Esta frase aparece en una de las camisetas XXL de la l&iacute;nea de ropa que lleva la firma de Tet&eacute; Delgado.</li>
		<li><a href='novedades.php?seccion=noticia4'><em>Art&iacute;culo de El Peri&oacute;dico de Catalun&ntilde;a 21/Mayo/2007 - Vanguardia en tallas grandes</em></a><br>
		La moda en tallas grandes ya no est&aacute; re&ntilde;ida con las &uacute;ltimas tendencias.</li>
		<li><em>Tete forma parte del jurado de Eurovisi&oacute;n</em><br />
		El s&aacute;bado 12 de Mayo, Tete form&oacute; parte del jurado en el Festival de Eurovisi&oacute;n.</li>
		</ul>
		
		<h4><img src='img/cuadro3.gif'>&nbsp;Abril 2007</h4>
		<ul>
		<li><em>Tu moda desfila en Channel 4 con Tete y Boris</em><br />
		Lunes 16 de Abril, en Cannel 4 programa de Boris  Irezaguirre, Pase pasarela de la Nueva colecci&oacute;n de Moda Joven Tet&eacute; Delgado DIVIERTETE &amp; ANDREA BOSS&Eacute;.</li>
		<li><em>Por fin lanzamos la marca al p&uacute;blico</em><br />
		Mi&eacute;rcoles 18 Abril. Presentaci&oacute;n a trav&eacute;s de los  Medios de Comunicaci&oacute;n de la Nueva Collecci&oacute;n de Moda Joven Tete Delgado DIVIERTETE &amp; ANDREA BOSS&Eacute; al p&uacute;blico en general. </li>
		</ul>
		
		<h4><img src='img/cuadro3.gif' />&nbsp;Febrero 2007</h4>
		<ul>
		<li><a href='novedades.php?seccion=noticia3'><em>Entrevista Tete Delgado en Feria SIMM</em></a><br>
		Tet&eacute; Delgado muestra, por primera vez, su propia l&iacute;nea de ropa de tallas grandes.</li>
		</ul>
		<h4><img src='img/cuadro3.gif' />&nbsp;Enero 2007</h4>
		<ul>
		<li><a href='novedades.php?seccion=noticia1'><em>Tete Delgado, es la  protagonista de la obra de teatro : GORDA!</em></a><br>Con un reparto de lujo y bajo la direcci&oacute;n de Tamzin Townsend llega la obra del afamado Neil LaBute: GORDA!</li>
		<li><a href='novedades.php?seccion=noticia2'><em>Tete Delgado muestra los Modelos de la Colecci&oacute;n...</em></a><br>Tet&eacute; Delgado muestra sus Modelos de la Colecci&oacute;n en cada una de sus apariciones en los medios de comunicaci&oacute;n.</li>
		</ul>";
		break;
	}
	?>
	</td>
    <!-- InstanceEndEditable -->
</tr>
<tr>
	<td colspan="3" class="contacto">Contacta con nosotros en <a href="mailto:info@diviertetetallasgrandes.com">info@diviertetetallasgrandes.com</a>, por tel&eacute;fono <em>93.485.60.21</em> o en <em>c/Llull n�51, 5� 3era 08005 Barcelona</em></td>
</tr>
</table>
</body>
<!-- InstanceEnd --></html>
