<?php 
session_start();
if($_GET["lang"])	
	$_SESSION["lang"]=$_GET["lang"];
	
$bt1["esp"]="historia";			$bt1["ing"]="history";			$bt1["cat"]="hist&ograve;ria";			
$bt2["esp"]="exteriores";		$bt2["ing"]="external";			$bt2["cat"]="exteriors";	
$bt3["esp"]="interiores";		$bt3["ing"]="internal";			$bt3["cat"]="interiors";	
$bt4["esp"]="servicios";		$bt4["ing"]="services";			$bt4["cat"]="serveis";	
$bt5["esp"]="situacion";		$bt5["ing"]="situation";		$bt5["cat"]="situaci&oacute;";	
$bt6["esp"]="bodas/eventos";	$bt6["ing"]="weddings/events";	$bt6["cat"]="noces/esdeveniments";	
$bt7["esp"]="inicio";			$bt7["ing"]="home";				$bt7["cat"]="inici";	
$bt8["esp"]="contacto";			$bt8["ing"]="contact";			$bt8["cat"]="contacte";	
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)" />
    <meta name="revisit" content="7 days" />
    <meta name="robots" content="index,follow,all" />
	<meta name="description" content="Castell de Sant Mar&ccedil;al en la provincia de Barcelona. Celebracion de bodas y eventos. Ctra. de Sant Cugat a Cerdanyola s/n - 08290 Cerdanyola del Valles" />
	<meta name="keywords" content="Castell de Sant Mar&ccedil;al, castel san marsal, bodas barcelona, eventos barcelona" />
    <meta name="lang" content="es,sp,spanish,espaol,espanol,castellano" />
    <meta name="author" content="www.indianwebs.com" />
	<title>Castell de Sant Mar&ccedil;al - Bodas y Eventos - Ctra. de Sant Cugat a Cerdanyola s/n - 08290 Cerdanyola del Vall&egrave;s</title>
    <link rel="stylesheet" type="text/css" href="estilos.css">
    <link rel="shortcut icon"  href="favicon.ico" />
    <script language="javascript" type="text/javascript" src="funciones.js"></script>
    <script src="img/AC_RunActiveContent.js" type="text/javascript"></script>
</head>
<body>

<div id="central">

    <div class="boton_top">
    	<a href="http://www.castelldesantmarsal.com/index.htm" title="Castell Sant Mar&ccedil;al"><img src="img/icon_home.gif"> <?php echo $bt7[$_SESSION["lang"]] ?></a> 
    	<a href="http://www.castelldesantmarsal.com/contacto.php" title="Contacta Castell Sant Mar&ccedil;al"><img src="img/icon_mail.gif"> <?php echo $bt8[$_SESSION["lang"]] ?></a>
    </div>
    
    <div class="boton_sub">
    	<img src="img/titulo-castell-santmarsal.gif" align="absmiddle">
    	<a href="http://www.castelldesantmarsal.com/historia-castell.php" title="Historia Castell Sant Mar&ccedil;al"><?php echo $bt1[$_SESSION["lang"]] ?></a>
        <a href="http://www.castelldesantmarsal.com/exteriores.php" title="Exteriores Castell Sant Mar&ccedil;al"><?php echo $bt2[$_SESSION["lang"]] ?></a>
        <a href="http://www.castelldesantmarsal.com/interiores.php" title="Interiores Castell Sant Mar&ccedil;al"><?php echo $bt3[$_SESSION["lang"]] ?></a>
        <a href="http://www.castelldesantmarsal.com/servicios.php" title="Servicios Castell Sant Mar&ccedil;al"><?php echo $bt4[$_SESSION["lang"]] ?></a>
        <a href="http://www.castelldesantmarsal.com/mapa-situacion.php" title="Localizacion Castell Sant Mar&ccedil;al"><?php echo $bt5[$_SESSION["lang"]] ?></a>
        <a href="http://www.castelldesantmarsal.com/bodas-eventos.php" title="Bodas Eventos Castell Sant Mar&ccedil;al"><?php echo $bt6[$_SESSION["lang"]] ?></a>
    </div>