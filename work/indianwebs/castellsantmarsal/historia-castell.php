<?php include("cabecera.php"); ?>
    
    <img src="img/castell-sant-marsal-1.jpg" />
    <div class="verde">
    	<?php 
		switch($_SESSION["lang"])
		{
			case "ing":	
			echo "<h1>History of Castell de Sant Mar&ccedil;al ";			break;
			
			case "cat":
			echo "<h1>Hist&ograve;ria del Castell de Sant Mar&ccedil;al ";	break;
			
			default:
			echo "<h1>Historia del Castell de Sant Mar&ccedil;al ";			break;
		}
		echo "<span style='padding:0 0 0 365px'><a href='javascript:cerrar()'>[X]</a></span></h1>";
		?>
       
        <?php
		switch($_GET["pag"])
		{
			case 2:
			echo "<img src='img/castell-historia2.jpg' align='right' style='margin:20px 20px 0 20px;'>"; 
            switch($_SESSION["lang"])
			{
				case "ing": ?>
                <p style="font-size:11px;">In 1311 the church of Tarragona sold its half to the monastery of Sant Cugat, with full lordship, as it is written that Romeu de Marimon paid a tribute to the abbot who enfeoffed him half of the castle. This lordship duality lasted more than two centuries. The Marimon family became very important among the most outstanding families in that time.
				<br><br>In 1546 Bernat Joan de Marimon was performing full lordship of the castle and of the area of Sant Mar&ccedil;al, as on 25th May 1542 he bought for 31,000 pounds all the rights of the monastery of Sant Cugat upon the castle.
				<br><br>The title of marquis of Cerdanyola was granted to Felix de Marimon in 1690. He was a member of the King Council in Italy. During the succession war, they took over the Bourbon party. Later on, the marquisate of Cerdanyolaincorporated the marquisate of Bo&iuml;l and then passed to the ducal house of Castro-Enriquez.
                <br><a href="historia-castell.php" style="color:#000000;font-weight:bold;">[previous...]</a> &middot;&middot;&middot; <a href="historia-castell.php?pag=3" style="color:#000000;font-weight:bold;">[next...]</a></p>
				<? break;
			
				case "cat": ?>
                <p style="font-size:11px;">En 1311 l'esgl&eacute;sia de Tarragona ven la seva meitat al monestir de Sant Cugat amb plena senyoria on consta que Romeu de Marim&oacute;n va prestar homenatge a l'abat que li va enfeudar de la meitat del castell. Aquesta dualitat senyorial es va conservar durant m&eacute;s de dos segles. La fam&iacute;lia Marim&oacute;n va arribar a tenir gran import&agrave;ncia entre les fam&iacute;lies m&eacute;s rellevants de l'&egrave;poca.
				<br><br>Bernat Joan de Marim&oacute;n consta en 1546 exercint la plena senyoria del castell i terme de Sant Mar&ccedil;al ja que el 25 de maig de 1542 va comprar per 31.000 lliures tots els drets del monestir de Sant Cugat sobre el castell.
				<br><br>El t&iacute;tol de marqu&egrave;s de Cerdanyola va ser concedit en 1690 a Felix de Marim&oacute;n membre del consell de S. M. a It&agrave;lia. En la guerra de successi&oacute; van prendre el partit borb&ograve;nic. Posteriorment el marquesat de Cerdanyola incorpora al de Bo&iuml;l i passa despr&eacute;s a la casa ducal de Castro-Enriquez.
                <br><a href="historia-castell.php" style="color:#000000;font-weight:bold;">[anterior...]</a> &middot;&middot;&middot; <a href="historia-castell.php?pag=3" style="color:#000000;font-weight:bold;">[seg&uuml;ent...]</a></p>
				<? break;
			
				default: ?>
            	<p style="font-size:11px;">En 1311 la iglesia de Tarragona vende su mitad al monasterio de Sant Cugat con plena se&ntilde;or&iacute;a pues consta que Romeu de Marim&oacute;n prest&oacute; homenaje al abad que le infeud&oacute; de la mitad del castillo. Esta dualidad se&ntilde;orial se conserv&oacute; durante m&aacute;s de dos siglos. La familia Marim&oacute;n alcanz&oacute; gran importancia entre las familias m&aacute;s relevantes de la &eacute;poca.
				<br><br>Bernat Joan de Marim&oacute;n consta en 1546 ejerciendo la plena se&ntilde;or&iacute;a del castillo y t&eacute;rmino de Sant Mar&ccedil;al puesto que el 25 de mayo de 1542 compr&oacute; por 31.000 libras todos los derechos del monasterio de Sant Cugat sobre el castillo.
				<br><br>El t&iacute;tulo de marqu&eacute;s de Cerdanyola fue concedido en 1690 a Felix de Marim&oacute;n miembro del consejo de S.M. en Italia. En la guerra de sucesi&oacute;n tomaron el partido borb&oacute;nico. Posteriormente el marquesado de Cerdanyola incorpora al de Boil y pasa luego a la casa ducal de Castro-Enriquez.
            	<br><a href="historia-castell.php" style="color:#000000;font-weight:bold;">[anterior...]</a> &middot;&middot;&middot; <a href="historia-castell.php?pag=3" style="color:#000000;font-weight:bold;">[siguiente...]</a></p>
				<? break;
            } 
			break;
			
			case 3:
            echo "<img src='img/castell-historia3.jpg' align='right' style='margin:20px 20px 0 20px;'>";
            switch($_SESSION["lang"])
			{
				case "ing": ?>
                <p style="font-size:11px;">After several modifications, the last one being made by Cayetano Bu&iacute;gas, known for its monument to Christopher Columbus in Barcelona, the castle preserves its XIIIth century ground surface, almost square. The church of Sant Mar&ccedil;al,attached to the East fa&ccedil;ade, is a gothic building which went through a modification in the XVIIth century. The gothic door and the embedded keystones still remain in the fa&ccedil;ade, one of them with the image of a saint. This whole was surrounded by an outside wall whose base is still kept; there still is a ditch which is partly landscaped.
				<br><br>In the last years a lot of work has been done to restore and revalue the building and its surroundings, emphasizing the contributions of its long history.
                <br><a href="historia-castell.php?pag=2" style="color:#000000;font-weight:bold;">[previous...]</a></p>
				<? break;
			
				case "cat": ?>
                <p style="font-size:11px;">Despr&eacute;s de diverses reformes, l'&uacute;ltima d'elles realitzada per Cayetano Buigas tan conegut pel monument a Cristobal Col&oacute;n , el Castell conserva la planta del segle XIII gaireb&eacute; quadrada. L'esgl&eacute;sia de Sant Mar&ccedil;al, adossada a la fa&ccedil;ana Aquest, &eacute;s un edifici g&ograve;tic que va sofrir una reforma en el segle XVII. Es conserva la porta g&ograve;tica i les claus de les voltes encastades en la fa&ccedil;ana, una d'elles amb la imatge d'un sant. Tot aquest conjunt estava envoltat per una muralla exterior de la qual es conserva la base i aquesta a la vegada d'una fossa del que tamb&eacute; existeix enjardinat un bon tram.
				<br><br>En els &uacute;ltims anys s'ha realitzat un gran treball per a restaurar i valorar l'edifici i, el seu entorn ressaltant les aportacions de la llarga hist&ograve;ria que ho caracteritza.
                <br><a href="historia-castell.php?pag=2" style="color:#000000;font-weight:bold;">[anterior...]</a></p>
				<? break;
                
                default: ?>
                <p style="font-size:11px;">Despu&eacute;s de varias reformas, la &uacute;ltima de ellas realizada por Cayetano Buigas tan conocido por el monumento a Cristobal Col&oacute;n , el Castillo conserva la planta del siglo XIII casi cuadrada. La iglesia de Sant Mar&ccedil;al, adosada a la fachada Este, es un edificio g&oacute;tico que sufri&oacute; una reforma en el siglo XVII. Se conserva la puerta g&oacute;tica y las claves de las b&oacute;vedas empotradas en la fachada, una de ellas con la imagen de un santo. Todo este conjunto estaba rodeado por una muralla exterior de la que se conserva la base y esta a su vez de un foso del que tambi&eacute;n existe ajardinado un buen tramo.
				<br><br>En los &uacute;ltimos a&ntilde;os se ha realizado un gran trabajo para restaurar y valorar el edificio y, su entorno resaltando las aportaciones de la larga historia que lo caracteriza.
            	<br><a href="historia-castell.php?pag=2" style="color:#000000;font-weight:bold;">[anterior...]</a></p>
				<? break;
            } 
			break;
			
			default:
			echo "<img src='img/castell-historia.jpg' align='right' style='margin:20px 20px 0 20px;'>";
            switch($_SESSION["lang"])
			{
				case "ing": ?>
				<p style="font-size:11px;">Several historians have stated that the castle of Sant Mar&ccedil;al or the castle of Cerdanyola can have their origins in the first third of the XIth century, when the church of Sant Mar&ccedil;al is already documented, althoug the castle does not appear explicitly until the first half of the XIIth century, when Ramon Berenguer IV includes it among the properties donated to his senechal Ramon Guillen de Montcada, viscount of Bearn. This property, together with other castles, became the Montcada domains. The house of Montcada, in those times, was very attached to the house of the counts of Barcelona, but it was no obstacle for differences and even confrontations which ended up with the cutting off of the city's irrigation channel, leaving Barcelona with no water.
				<br><br>Marimon de Plegamans is the initiator of the Marimon family, of which the current owners are descendants. Romeu de Marimon was the magistrate ofBarcelona in 1275, took part in the construction of the city walls, was entrusted the castle of Montcada, which he also reformed, and took part in the constructionof the shipyards of Barcelona.
                <br><a href="historia-castell.php?pag=2" style="color:#000000;font-weight:bold;">[next...]</a></p>
				<? break;
			
				case "cat": ?>
                <p style="font-size:11px;">Diversos historiadors afirmen que el castell de Sant Mar&ccedil;al  o de Cerdanyola pot tenir el seu origen en el primer ter&ccedil; del segle XI moment que ja es troba documentada l'esgl&eacute;sia de Sant Mar&ccedil;al per&ograve; el castell no apareix clarament esmentat fins a la primera meitat del segle XII en el qual Ram&oacute;n Berenguer IV l'inclou entre els b&eacute;ns donats al seu senescal Ram&oacute;n Guill&eacute;n de Montcada, vescomte de Bearn, que van passar a constituir juntament amb altres castells els dominis de Montcada. La casa de Montcada en aquestes &egrave;poques estava molt vinculada a la casa condal de Barcelona, aix&ograve; no va ser obstacle per a difer&egrave;ncies i fins i tot enfrontaments que van arribar fins a tallar la s&egrave;quia condal i deixar sense aigua a Barcelona.
				<br><br>Marim&oacute;n de Plegamans &eacute;s l'iniciador de la fam&iacute;lia Marim&oacute;n de la qual s&oacute;n descendents els actuals propietaris. Romeu de Marim&oacute;n en 1275 exercia de veguer de Barcelona, va intervenir en la construcci&oacute; de les muralles, li va ser confiat el castell de Montcada que tamb&eacute; va reformar i va intervenir en la construcci&oacute; de les atarazanas barcelonines.
                <br><a href="historia-castell.php?pag=2" style="color:#000000;font-weight:bold;">[seg&uuml;ent...]</a></p>
				<? break;
                
                default: ?>
                <p style="font-size:11px;">Diversos historiadores afirman que el castillo de Sant Mar&ccedil;al o de Cerdanyola puede tener su origen en el primer tercio del siglo XI momento en que ya se encuentra documentada la iglesia de Sant Mar&ccedil;al pero el castillo no aparece claramente mencionado hasta la primera mitad del siglo XII en el que Ram&oacute;n Berenguer IV lo incluye entre los bienes donados a su senescal Ram&oacute;n Guill&eacute;n de Montcada, vizconde de Bearn, que pasaron a constituir junto con otros castillos los dominios de Montcada. La casa de Montcada en estas &eacute;pocas estaba muy vinculada a la casa condal de Barcelona, ello no fue obst&aacute;culo para diferencias e incluso enfrentamientos que llegaron hasta cortar la acequia condal y dejar sin agua a Barcelona.
  	        	<br><br>Marim&oacute;n de Plegamans es el iniciador de la familia Marim&oacute;n de la que son descendientes los actuales propietarios. Romeu de Marim&oacute;n en 1275 ejerc&iacute;a de veguer de Barcelona, intervino en la construcci&oacut;n de las murallas, le fue confiado el castillo de Montcada que tambi&eacute;n reform&oacute; e intervino en la construcci&oacute;n de las atarazanas barcelonesas. 
     	   	 	<br><a href="historia-castell.php?pag=2" style="color:#000000;font-weight:bold;">[siguiente...]</a></p>
                <? break;
             } 
			 break;
		} 
		?>
    </div>
    
<?php include("piecera.php"); ?>