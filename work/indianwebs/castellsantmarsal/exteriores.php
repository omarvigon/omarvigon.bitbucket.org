<?php include("cabecera.php"); ?>
    
    <img src="img/castell-sant-marsal-5.jpg" />
    <div class="verde">
    	<?php 
		switch($_SESSION["lang"])
		{
			case "ing":	
			echo "<h1>External <span style='padding:0 0 0 535px'><a href='javascript:cerrar()'>[X]</a></span></h1>";	
			echo "<p>Its beautiful, romantic park, as well as its polo court, make the Castle of Sant Mar&ccedil;al a unique place for the most special events.</p>";	
			echo "<p style='border-bottom:1px solid #ffffff;'>Photogallery</p>";		
			break;
			
			case "cat":
			echo "<h1>Exteriors <span style='padding:0 0 0 535px'><a href='javascript:cerrar()'>[X]</a></span></h1>";
			echo "<p>El seu bonic i rom&agrave;ntic parc aix&iacute; com la seva pista de pol converteixen al Castell de St. Mar&ccedil;al en un exclusiu lloc per als esdeveniments m&eacute;s especials.</p>";
			echo "<p style='border-bottom:1px solid #ffffff;'>Galeria fotogr&agrave;fica</p>";
			break;
			
			default:
			echo "<h1>Exteriores <span style='padding:0 0 0 535px'><a href='javascript:cerrar()'>[X]</a></span></h1>";	
			echo "<p>Su hermoso y rom&aacute;ntico parque as&iacute; como su cancha de polo convierten al Castell de St. Mar&ccedil;al en un exclusivo lugar para los eventos m&aacute;s especiales.</p>";		
			echo "<p style='border-bottom:1px solid #ffffff;'>Galer&iacute;a fotogr&aacute;fica</p>";
			break;
		}
		?>

        <p>
        <img alt="Exterior del Castell" src="img/img_ext/img1p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img1.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img2p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img2.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img3p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img3.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img4p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img4.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img5p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img5.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img6p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img6.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img7p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img7.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img8p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img8.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img9p.jpg" class="mini"onMouseover="ddrivetip('<img src=img/img_ext/img9.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img10p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img10.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img11p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img11.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img12p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img12.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img13p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img13.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img14p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img14.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img15p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img15.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img16p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img16.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img17p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img17.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img18p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img18.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img19p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img19.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img20p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img20.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img21p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img21.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img22p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img22.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img25p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img25.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img26p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img26.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img27p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img27.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img28p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img28.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img29p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img29.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img30p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img30.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img31p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img31.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img32p.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img32.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img33.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img33.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img34.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img34.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img35.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img35.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img36.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img36.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img37.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img37.jpg>')"; onMouseout="hideddrivetip()">
        <img alt="Exterior del Castell" src="img/img_ext/img38.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_ext/img38.jpg>')"; onMouseout="hideddrivetip()">
        </p>
        
        <p style="margin-bottom:0px;border-bottom:1px solid #ffffff;">Tour Virtual</p>
        <p style="margin-top:5px;">
        
        <a href="javascript:tour('1ext')"><img src="img/tour_ext1.jpg" border="0"></a>
        <a href="javascript:tour('2ext')"><img src="img/tour_ext2.jpg" border="0"></a>
        </p>
    </div>
    
<?php include("piecera.php"); ?>