<?php include("cabecera.php"); ?>
    
    <img src="img/castell-sant-marsal-3.jpg" />
        
    <div class="verde">
    	<?php 
		switch($_SESSION["lang"])
		{
        	case "ing":
			echo "<h1>Internal <span style='padding:0 0 0 535px'><a href='javascript:cerrar()'>[X]</a></span></h1>
			<p style='margin-bottom:0;'>Its incomparable environment makes any event something peculiar. A wide range of new possibilities for celebrating all kind of events. A new concept to define unique acts.</p>
			<p style='float:left;'><b>CELLAR ROOM</b><br>250 m2<br>Cocktail party 300 people<br>Banquet 125 people</p>
			<p style='float:left;'><b>GREENHOUSE HALL</b><br>500 m2<br>Cocktail party 800 people<br>Banquet 450 people</p>";
			break;
			
			case "cat":
			echo "<h1>Interiors <span style='padding:0 0 0 535px'><a href='javascript:cerrar()'>[X]</a></span></h1>
			<p style='margin-bottom:0;'>El seu incomparable entorn oferix un ampli ventall de noves possibilitats per a la realitzaci&oacute; de tot tipus d'esdeveniments. Un nou concepte per a definir el que s&oacute;n actes &uacute;nics.</p>
			<p style='float:left;'><b>SAL&Oacute; CELLER</b><br>250 m2<br>cocktail 300 persones<br>banquet 125 persones</p>
			<p style='float:left;'><b>SAL&Oacute; HIVERNACLE</b><br>500 m2<br>cocktail 800 persones<br>banquet 450 persones</p>";
			break;
			
			default:
			echo "<h1>Interiores <span style='padding:0 0 0 535px'><a href='javascript:cerrar()'>[X]</a></span></h1>
        	<p style='margin-bottom:0;'>Su incomparable entorno ofrece un amplio abanico de nuevas posibilidades para la realizaci&oacute;n de todo tipo de eventos. Un nuevo concepto para definir lo que son actos &uacute;nicos.</p>
        	<p style='float:left;'><b>SAL&Oacute;N BODEGA</b><br>250 m2<br>cocktail 300 personas<br>banquete 125 personas</p>
        	<p style='float:left;'><b>SAL&Oacute;N INVERNADERO</b><br>500 m2<br>cocktail 800 personas<br>banquete 450 personas</p>";
			break;
        } 
		?>
        
        <p style="float:left;"><b>TOUR VIRTUAL</b><br>
        	<a href="javascript:tour('1int')"><img title="Tour Virtual Castel" src="img/tour_int1.jpg" border="0" alt="Tour Virtual Castell"></a>
            <a href="javascript:tour('2int')"><img title="Tour Virtual Castel" src="img/tour_int2.jpg" border="0" alt="Tour Virtual Castel"></a>
        </p>
        
        <p style="clear:left;">
        <img src="img/img_int/img1p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img1.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img2p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img2.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img3p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img3.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img4p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img4.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img5p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img5.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img6p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img6.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img7p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img7.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img8p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img8.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img9p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img9.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img10p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img10.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img11p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img11.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img12p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img12.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img13p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img13.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img14p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img14.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img15p.jpg" style="border:1px solid #ffffff;" onMouseover="ddrivetip('<img src=img/img_int/img15.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img16.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_int/img16.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img17.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_int/img17.jpg>')"; onMouseout="hideddrivetip()">
        <img src="img/img_int/img18.jpg" class="mini" onMouseover="ddrivetip('<img src=img/img_int/img18.jpg>')"; onMouseout="hideddrivetip()">
        </p>
        
    </div>
    
<?php include("piecera.php"); ?>