function mostrar(numero)
{
	document.getElementById("bloque1").style.display="none";
	document.getElementById("bloque2").style.display="none";
	document.getElementById("bloque3").style.display="none";
	document.getElementById("bloque4").style.display="none";
	document.getElementById("bloque5").style.display="none";
	
	document.getElementById("bloque"+numero).style.display="block";
}
function cerrar()
{
	document.getElementsByTagName("div")[3].style.display='none';	
}
function tour(cual)
{
	window.open("tour.php?ntour="+cual,"_blank","width=520,height=220,scrollbars=no,resizable=no,directories=no,location=no,menubar=no,status=no");	
}
function cambiar(nombre)
{
	var texto="";
	document.getElementById("imgservicio").src="img/slogo-"+nombre+".gif";
	
	switch(nombre)
	{
		case "airolo":
		texto="AIROLO CATERING<br>Via Augusta, 125 1er-3a &middot; Barcelona<br>Telf. 93 534 04 44<br>airolo@airolo.es &middot; www.airolo.es";
		break;
		
		case "arola":
		texto="AROLA CATERING<br>c/ Marina 19-21<br>08005 Barcelona<br>(David Martinez)<br>Tel. 93.483.81.02 / Fax. 93.221.30.45<br>david.martinez@ritzcarlton.com";
		break;
		
		case "aspic":
		texto="ASPIC Catering & Services<br>Emancipaci&oacute;n, 25 - 08022 Barcelona<br>Persona de contacto: Eduard Casanovas<br>Telf.: 93 414 40 21<br>Fax.: 93 417 06 61<br>Mail: aspic@aspic.es<br>Web: www.aspic.es";
		break;
		
		case "boix":
		texto="C�TERING BOIX de la Cerdanya<br>Cam&iacute; Ca n'Ametller, s/n<br>08195 Sant Cugat del Vall&egrave;s � BCN<br>Tel: 93 590 03 39 � Fax: 93 590 03 42<br>www.cateringboix.com<br>info@cateringboix.com";
		break;
		
		case "botafum":
		texto="BOTAFUMEIRO CATERING 1975<br>Persona de contacto: SUSANNA S&Aacute;NCHEZ<br>Telf. de contacto: 93.200.12.04/ 607.544.987<br>Mail: catering@botafumeiro.es";
		break;
		
		case "elceller":
		texto="EL CELLER DE MATADEPERA<br>(Abans Armand's Catering)<br>Carrer Gaudi n� 2<br>08230 Matadepera<br>Tel i Fax. 93 787 08 57 &middot; M�bil 669 40 70 80<br>www.elcellerataula.com";
		break;
		
		case "eshterconde":
		texto="ESTHER CONDE<br>Contacto: Esther Conde<br>C/ Benet Mateu 56, entlo 2a<br>08034 Barcelona<br>Tel: 93 280 50 70<br>Web: www.estherconde.com<br>Email: e.conde@estherconde.com";
		break;
		
		case "gourmetparadis":
		texto="GOURMET PARADIS<br>Capitan Arenas, 3 bajos - 08034 Barcelona<br>(Sra. M� Angeles Losantos)<br>Tel. 93 205.04.11";
		break;
		
		case "lechef":
		texto="LE CHEF Catering<br>Laforja 122, bajos 08021 Barcelona<br>Telf. 93.241.21.37  /  Fax. 93 241 31 20<br>Mail: lechefcatering@terra.es<br>Web: www.lechefcatering.com";
		break;
		
		case "merces":
		texto="Diagonal, 539 - 08029 Barcelona<br>(Srta. Merc&egrave; Solernou)<br>Tel. 93 439.44.82<br>Fax: 93.439.70.06<br>Mail: banquetes@merces.es<br>www.merces.es";
		break;
		
		case "pratsfatjo":
		texto="PRATS FATJ&Oacute; CATERING & SERVICES<br>General Mitre, 39<br>(Srta. Carol M&aacute;rquez)<br>Telf. 93 205 41 72<br>Fax 93 205 39 64<br>bodas@pratsfatjo.com";
		break;
		
		case "semon":
		texto="SEMON<br>Ganduxer, 31 - 08021 Barcelona<br>(Sr. Paulino Robles)<br>Tels. 93 201.65.08 / 93 201.57.21 / 93 201.83.66<br>Fax. 93 201.02.92";
		break;
		
		case "sibaris":
		texto="SIBARIS CATERING<br>Pla�a Uni&oacute;, 2; Bajos - 08190 St. Cugat del Vall&egrave;s<br>(Bet Alvarez / Sandra Ramon)<br>Tels. 93 675.40.51 - Fax. 93 589.66.62<br>www.sibariscatering.com";
		break;
		
		case "torres":
		texto="Bertran y Rozpide 7-9 Barcelona 08034<br>Tel&eacute;fonos de contacto:<br>Patty Torres - 667622030<br>Samantha Hellman - 627432595<br>patty@torreshellman.com<br>sam@torreshellman.com";
		break;
		
		case "vilaplana":
		texto="VILAPLANA<br>Tenor Vi&ntilde;as 4-6, 3� 2� 08021 Barcelona<br>Persona de contacto : Meritxell Sir&eacute;<br>comercialbcn@vilaplanacat.com<br>Tel. 93.201.13.00";
		break;
	}
	document.getElementById("txtlogo").innerHTML=texto;
}

////////////////////////toottip con imagen

var offsetxpoint=-300 //Customize x offset of tooltip
var offsetypoint=-320 //Customize y offset of tooltip
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false

function ietruebody()
{
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
function ddrivetip(thetext)
{
	tipobj.innerHTML=thetext
	enabletip=true
	return false
}
function positiontip(e)
{
	if (enabletip)
	{
		var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
		var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
		//Find out how close the mouse is to the corner of the window
		var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20
		var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20
		var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000

		//if the horizontal distance isn't enough to accomodate the width of the context menu
		if (rightedge<tipobj.offsetWidth)
			//move the horizontal position of the menu to the left by it's width
			tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px"
		else if (curX<leftedge)
			tipobj.style.left="5px"
		else
			//position the horizontal position of the menu where the mouse is positioned
			tipobj.style.left=curX+offsetxpoint+"px"
			//same concept with the vertical position
		if (bottomedge<tipobj.offsetHeight)
			tipobj.style.top=ie? ietruebody().scrollTop+event.clientY-tipobj.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-tipobj.offsetHeight-offsetypoint+"px"
		else
			tipobj.style.top=curY+offsetypoint+"px"
		tipobj.style.visibility="visible"
	}
}
function hideddrivetip()
{
	enabletip=false
	tipobj.style.visibility="hidden"
}
document.onmousemove=positiontip