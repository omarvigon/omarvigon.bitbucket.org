<?php include("cabecera.php"); ?>
    
    <img src="img/castell-sant-marsal-2.jpg" />
    <div class="verde">
    	<?php 
		switch($_SESSION["lang"])
		{
			case "ing":
			echo "<h1>Castell de Sant Mar&ccedil;al Services ";
			break;
			
			case "cat":
			echo "<h1>Serveis de Castell de Sant Mar&ccedil;al ";
			break;
			
			default:
			echo "<h1>Servicios de Castell de Sant Mar&ccedil;al ";
			break;
		}
		echo "<span style='padding:0 0 0 365px'><a href='javascript:cerrar()'>[X]</a></span></h1>";
		?>
    
        <table cellpadding="0" cellspacing="0">
        <tr>
        	<td>
            	<a href="javascript:cambiar('merces')">Merc&egrave;s</a><br>
       		 	<a href="javascript:cambiar('eshterconde')">Esther Conde</a><br>
        		<a href="javascript:cambiar('elceller')">El Celler de Matadepera</a><br>
        		<a href="javascript:cambiar('pratsfatjo')">Prats Fatj&oacute; Catering &amp; Services</a><br>
        		<a href="javascript:cambiar('arola')">Arola Catering</a><br>
        		<a href="javascript:cambiar('sibaris')">Sibaris Catering</a><br>
        		<a href="javascript:cambiar('aspic')">ASPIC Catering &amp; Services</a><br>
        		<a href="javascript:cambiar('semon')">Semon</a><br>
        		<a href="javascript:cambiar('torres')">Torres y Hellman</a><br>
        		<a href="javascript:cambiar('gourmetparadis')">Gourmet Paradis</a><br>
        		<a href="javascript:cambiar('airolo')">Airolo Catering</a><br>
        		<a href="javascript:cambiar('vilaplana')">Vilaplana</a><br>
        		<a href="javascript:cambiar('botafum')">Botafumeiro Catering</a><br>
        		<a href="javascript:cambiar('lechef')">Le Chef Catering</a><br>
        		<a href="javascript:cambiar('boix')">C&agrave;teting Boix de la Cerdanya</a>
            </td>
            <td width="80">&nbsp;</td>
            <td class="blanca_logos">
            	<img src="img/vacio.gif" id="imgservicio" vspace="10"><br>
                <span id="txtlogo">
                <?php 
				switch($_SESSION["lang"])
				{
					case "ing":
					echo "<br><br>click on any company title ti get more information...<br>";
					break;
					
					case "cat":
					echo "<br><br>clic en qualsevol empresa per a obtenir m&eacute;s informaci&oacute;...<br>";
					break;
					
					default:
					echo "<br><br>click en cualquier empresa para obtener m&aacute;s informaci&oacute;n...<br>";
					break;
				}
				?>
                 
                </span>
             </td>
        </tr>
        </table>
    </div>
    
<?php include("piecera.php"); ?>