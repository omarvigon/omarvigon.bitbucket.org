<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta name="revisit" content="7 days" >
    <meta name="robots" content="index,follow,all" >
	<meta name="lang" content="es,sp,spanish,español,espanol,castellano" >
	<meta name="description" content="Argostil, S.A. Dise�o en objetos de laton, cobre, rusticos y chapas publicitarias C/ Progreso, s/n Nave 2 08160 Montmelo - Barcelona">
	<meta name="keywords" content="Argostil, laton, objetos laton, piezas laton, dise�o laton">
	<title>Dise�o y artesania en objetos de laton y cobre. Chapas publicitarias</title>
	<link rel="stylesheet" type="text/css" href="estilos.css">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>
<body onload="init()">

<div id="central">
    <div id="top">
    	<img src="img/logo_argostil.gif" alt="Argostil S.A. Dise�o en Laton" title="Argostil S.A. Dise�o en Laton">
    </div>
    <div id="boton">
    	<a href="index.php"><img src="img/boton_inicio.gif" alt="Argostil - Inicio" title="Argostil - Inicio"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<a href="javascript:mostrar()"><img src="img/boton_articulos.gif" alt="Argostil - Articulos" title="Argostil - Articulos"></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="novedades.php"><img src="img/boton_novedades.gif" alt="Argostil - Novedades" title="Argostil - Novedades"></a>&nbsp;
    	<a href="contactar.php"><img src="img/boton_contactar.gif" alt="Argostil - Contactar" title="Argostil - Contactar"></a>
    </div>
    <div id="boton2">
    	<a href="articulos_laton.php"><img src="img/boton_familias1.gif" alt="Articulos de Laton" title="Articulos de Laton"></a>
        <a href="articulos_rustico.php"><img src="img/boton_familias2.gif" alt="Articulos Rusticos" title="Articulos Rusticos" hspace="22"></a>
        <a href="articulos_chapas_publicitarias.php"><img src="img/boton_familias3.gif" alt="Chapas Publicitarias" title="Chapas Publicitarias"></a>
    </div>