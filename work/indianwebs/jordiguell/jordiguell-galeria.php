<?php include("cabecera.php"); ?>

<table cellpadding="0" cellspacing="0" align="center" id="tabla4">

<tr>
	<td class="col_1">
		<h1><a href="jordiguell-biografia.php"><img src="img/tit_biografia<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Biografia de Jordi G�ell" title="Biografia de Jordi G�ell"></a></h1>
		<h1><a href="jordiguell-obras.php"><img src="img/tit_obras<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Obra de Jordi G�ell" title="Obra de Jordi G�ell"></a></h1>
	
		<h4 onmouseover='seccion(1)'><img hspace='3' src='img/titulo_pintura<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Pintura de Jordi Guell' title='Pintura de Jordi Guell'></h4>
		<h4 onmouseover='seccion(2)'><img hspace='3' src='img/titulo_papel<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Jordi Guell en Papel' title='Jordi Guell en Papel'></h4>
		<h4 onmouseover='seccion(3)'><img hspace='3' src='img/titulo_infografia<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Jordi Guell Infografia' title='Jordi Guell Infografia'></h4>
		<h4 onmouseover='seccion(4)'><img hspace='3' src='img/titulo_escritura<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Escritura de Jordi Guell' title='Escritura de Jordi Guell'></h4>
            
		<h1><a href="javascript:popup()"><img src="img/tit_info.gif" border="0"></a></h1>
		<h1><a href="jordiguell-contacta.php"><img src="img/tit_contacta<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Contacta Jordi Guell" title="Contacta Jordi Guell"></a></h1>

	</td>
	<td id="col_2">
		<p align="center" class="cont_gal">
		<img src="img/pixel.gif" name="galeria" border="0" alt="Obra de Jordi G&uuml;ell" title="Obra de Jordi G&uuml;ell">
		</p>
		<div id="informacion">&nbsp;</div>
	</td>
	<td id="col_3">
		<div id="minis">
		<?php
		switch($_GET['tipo'])
		{
			case "pint1990":
			echo '
			<a href=javascript:galeria("pintura1990_26","Sanguina_sobre_papel._1991")><img src="galeria_mini/pintura1990_26.gif" title="Pintura 1991 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_27","Sanguina_sobre_papel._1993.")><img src="galeria_mini/pintura1990_27.gif" title="Pintura 1993 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_24","Sanguina_sobre_papel._1990.")><img src="galeria_mini/pintura1990_24.gif" title="Pintura 1990 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_12","Sanguina_sobre_papel._1993.")><img src="galeria_mini/pintura1990_12.gif" title="Pintura 1993 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_18","Sepia_sobre_papel._1993")><img src="galeria_mini/pintura1990_18.gif" title="Pintura 1993 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_13","Carb�n_sobre_papel._1993")><img src="galeria_mini/pintura1990_13.gif" title="Pintura 1993 - Jordi G�ell"></a>			
			<a href=javascript:galeria("pintura1990_01","Carb�n_sobre_papel._200cm_x_90cm._1995.")><img src="galeria_mini/pintura1990_01.gif" title="Pintura 1995 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_17","Carb�n_sobre_papel._100cm_x_65cm._1994.")><img src="galeria_mini/pintura1990_17.gif" title="Pintura 1994 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_14","Sanguina_sobre_papel._100cm_x_65cm._1994.")><img src="galeria_mini/pintura1990_14.gif" title="Pintura 1994 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_16","Oleo_sobre_tela._116cm_x_81cm._1994.")><img src="galeria_mini/pintura1990_16.gif" title="Pintura 1994 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_19","Oleo_sobre_tela._146cm_x_114cm._1994.")><img src="galeria_mini/pintura1990_19.gif" title="Pintura 1994 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_23","Carb�n,_ceras_y_collage_sobre_papel._65cm_x_50cm._1996.")><img src="galeria_mini/pintura1990_23.gif" title="Pintura 1996 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_20","Carb�n,_ceras,_collage_y_�leo_sobre_papel._65cm_x_50cm._1996.")><img src="galeria_mini/pintura1990_20.gif" title="Pintura 1996 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_25","Carb�n,_ceras,_collage_y_�leo_sobre_papel._65cm_x_50cm._1996.")><img src="galeria_mini/pintura1990_25.gif" title="Pintura 1996 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_03","Carb�n_y_�leo_sobre_tela._54cm_x_65cm._2000.")><img src="galeria_mini/pintura1990_03.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_05","Carb�n_y_�leo_sobre_tela._50cm_x_61cm._2000.")><img src="galeria_mini/pintura1990_05.gif" title="Pintura 2000 - Jordi G�ell"></a>	
			<a href=javascript:galeria("pintura1990_02","Carb�n_y_�leo_sobre_tela._61cm_x_50cm._2000.")><img src="galeria_mini/pintura1990_02.gif" title="Pintura 2000 - Jordi G�ell"></a>	
			<a href=javascript:galeria("pintura1990_04","Carb�n_y_�leo_sobre_tela._46cm_x_27cm._1998.")><img src="galeria_mini/pintura1990_04.gif" title="Pintura 1998 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_07","Carb�n,_esmalte_y_�leo_sobre_tela._65cm_x_54cm._2000.")><img src="galeria_mini/pintura1990_07.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_08","Ceras,_esmalte_y_�leo_sobre_tela._65cm_x_54cm._2000.")><img src="galeria_mini/pintura1990_08.gif" title="Pintura 2000 - Jordi G�ell"></a>					
			<a href=javascript:galeria("pintura1990_06","Ceras_y_�leo_sobre_tela._92cm_x_73cm._2000.")><img src="galeria_mini/pintura1990_06.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_09","Ceras,_esmalte,_collage_fotogr�fico_y_�leo_sobre_tela.-92cm_x_73cm._2000.")><img src="galeria_mini/pintura1990_09.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_10","Ceras,_esmalte,_collage_infogr�fico_y_�leo_sobre_tela.-81cm_x_64cm._2000.")><img src="galeria_mini/pintura1990_10.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura1990_11","Ceras,_esmalte,_collage_infogr�fico_y_�leo_sobre_tela.-130cm_x_100cm._2000.")><img src="galeria_mini/pintura1990_11.gif" title="Pintura 2000 - Jordi G�ell"></a>			
			';
			break;
			
			case "pint2000":
			echo '
			<a href=javascript:galeria("pintura2000_01","�leo_sobre_tela._27cm_x_27cm._2000.")><img src="galeria_mini/pintura2000_01.gif"></a>
			<a href=javascript:galeria("pintura2000_02","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-61cm_x_50cm._2000.")><img src="galeria_mini/pintura2000_02.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_03","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-81cm_x_65cm._2000.")><img src="galeria_mini/pintura2000_03.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_04","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-116cm_x_X81cm._2000.")><img src="galeria_mini/pintura2000_04.gif" title="Pintura 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_05","Resinas,_tierra_y_�leo_sobre_tela._55cm_x_46cm._2001.")><img src="galeria_mini/pintura2000_05.gif" title="Pintura 2001 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_06","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-162cm_x_130cm._2001.")><img src="galeria_mini/pintura2000_06.gif" title="Pintura 2002 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_07","Resinas,_tierra_y_�leo_sobre_tela._65cm_x_54cm._2001.")><img src="galeria_mini/pintura2000_07.gif" title="Pintura 2001 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_08","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-130cm_x_97cm._2001.")><img src="galeria_mini/pintura2000_08.gif" title="Pintura 2001 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_09","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-70cm&Oslash;_2001")><img src="galeria_mini/pintura2000_09.gif" title="Pintura 2001 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_10","Resinas,_tierra,_collage_infogr�fico_y_�leo_sobre_tela.-46cm_x_38cm._2002.")><img src="galeria_mini/pintura2000_10.gif" title="Pintura 2002 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2000_11","Resinas,_tierra_y_�leo_sobre_tela._35cm_x_24cm._2002.")><img src="galeria_mini/pintura2000_11.gif" title="Pintura 2002 - Jordi G�ell"></a>';
			break;
			
			case "pint2002":
			echo '
			<a href=javascript:galeria("pintura2002_01","Resinas,_polvo_de_m�rmol,_limaduras_de_madera,_collage_infogr�fico-y_�leo_sobre_tela._65cmX50cm._2003.")><img src="galeria_mini/pintura2002_01.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_02","Resinas,_polvo_de_m�rmol,_grattage,_collage_infogr�fico-y_�leo_sobre_tela._130cmX89cm._2003.")><img src="galeria_mini/pintura2002_02.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_03","Resinas,_polvo_de_m�rmol,_grattage,_collage_infogr�fico-y_�leo_sobre_tela._58cmX48cm._2003.")><img src="galeria_mini/pintura2002_03.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_04","Resinas,_polvo_de_m�rmol,_collage_infogr�fico-y_�leo_sobre_tela._58cmX48cm._2003.")><img src="galeria_mini/pintura2002_04.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_05","Resinas,_polvo_de_m�rmol,_collage_infogr�fico-y_�leo_sobre_tela._116cmX89cm._2003.")><img src="galeria_mini/pintura2002_05.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_06","Resinas,_polvo_de_m�rmol,_grattage,_collage_infogr�fico-y_�leo_sobre_tela._162cmX130cm._2003.")><img src="galeria_mini/pintura2002_06.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_07","Resinas,_polvo_de_m�rmol,_grattage,_collage_infogr�fico-y_�leo_sobre_tela._146cmX114cm._2003.")><img src="galeria_mini/pintura2002_07.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_08","Resinas,_polvo_de_m�rmol,_grattage,_collage_infogr�fico-y_�leo_sobre_tela._78cmX43cm._2003")><img src="galeria_mini/pintura2002_08.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_09","Resinas,_polvo_de_m�rmol,_carb�n,_limaduras_de_madera,_papel,-collage_infogr�fico_y_�leo_sobre_tela._122cmX110cm._2003.")><img src="galeria_mini/pintura2002_09.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_10","Resinas,_polvo_de_m�rmol,_collage_infogr�fico-y_�leo_sobre_tela._81cmX60cm._2003.")><img src="galeria_mini/pintura2002_10.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_11","Resinas,_polvo_de_m�rmol,_cenizas_de_volc�n,_grattage,-collage_infogr�fico_y_�leo_sobre_tela._81cmX65cm._2003.")><img src="galeria_mini/pintura2002_11.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_12","Resinas,_polvo_de_m�rmol,_grattage,_collage_infogr�fico-y_�leo_sobre_tela._81cmX65cm._2003.")><img src="galeria_mini/pintura2002_12.gif" title="Pintura 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2002_13","Resinas,_cenizas_de_volc�n,_grattage_y-collage_infogr�fico_sobre_tela._81cmX65cm._2003.")><img src="galeria_mini/pintura2002_13.gif" title="Pintura 2003 - Jordi G�ell"></a>';
			break;
			
			case "pint2005":
			echo '
			<a href=javascript:galeria("pintura2005_01","�leo_sobre_tela._162cm_x_130cm._2005.")><img src="galeria_mini/pintura2005_01.gif"></a>
			<a href=javascript:galeria("pintura2005_02","�leo_sobre_tela_y_collage_infogr�fico._130cm_x_97cm._2005.")><img src="galeria_mini/pintura2005_02.gif" title="Pintura 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_03","�leo_sobre_tela_y_collage_infogr�fico._73cm_x_60cm._2006.")><img src="galeria_mini/pintura2005_03.gif" title="Pintura 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_04","�leo_sobre_tela_y_collage_infogr�fico._162cm_x_130cm._2005.")><img src="galeria_mini/pintura2005_04.gif" title="Pintura 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_05","�leo_sobre_tela_y_collage_infogr�fico._162cm_x_130cm._2005.")><img src="galeria_mini/pintura2005_05.gif" title="Pintura 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_06","�leo_sobre_tela_y_collage_infogr�fico._61cm_x_46cm._2005.")><img src="galeria_mini/pintura2005_06.gif" title="Pintura 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_07","�leo_sobre_tela_y_collage_infogr�fico._162cm_x_130cm._2006.")><img src="galeria_mini/pintura2005_07.gif" title="Pintura 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_08","�leo_sobre_tela_y_collage_infogr�fico._130cm_x_81cm._2006.")><img src="galeria_mini/pintura2005_08.gif" title="Pintura 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_09","�leo_sobre_tela_y_collage_infogr�fico._65cm_x_54cm._2006.")><img src="galeria_mini/pintura2005_09.gif" title="Pintura 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_10","�leo_sobre_tela_y_collage_infogr�fico._47cm_x_63cm._2006.")><img src="galeria_mini/pintura2005_10.gif" title="Pintura 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("pintura2005_11","�leo_sobre_tela_y_collage_infogr�fico._81cm_x_65cm._2005.")><img src="galeria_mini/pintura2005_11.gif" title="Pintura 2005 - Jordi G�ell"></a>';
			break;
			
			case "papel2000":
			echo '
			<a href=javascript:galeria("papel2000_01","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._265mm_x_235mm._2000.")><img src="galeria_mini/papel2000_01.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_02","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._265mm_x_220mm._2000.")><img src="galeria_mini/papel2000_02.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_03","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._265mm_x_180mm._2000")><img src="galeria_mini/papel2000_03.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_04","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._245mm_x_170mm._2000.")><img src="galeria_mini/papel2000_04.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_05","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._245mm_x_170mm._2000.")><img src="galeria_mini/papel2000_05.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_06","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._265mm_x_225mm._2000.")><img src="galeria_mini/papel2000_06.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_07","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._272mm_x_210mm._2000.")><img src="galeria_mini/papel2000_07.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_08","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._265mm_x_195mm._2000.")><img src="galeria_mini/papel2000_08.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_09","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._290mm_x_205mm._2000.")><img src="galeria_mini/papel2000_09.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_10","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._290mm_x_230mm._2000.")><img src="galeria_mini/papel2000_10.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_11","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._280mm_x_210mm._2000.")><img src="galeria_mini/papel2000_11.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_12","Resinas,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._290mm_x_210mm._2000.")><img src="galeria_mini/papel2000_12.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_13","Resinas,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._290mm_x_170mm._2000.")><img src="galeria_mini/papel2000_13.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_14","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._480mm_x_370mm._2000.")><img src="galeria_mini/papel2000_14.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_15","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._295mm_x_160mm._2000.")><img src="galeria_mini/papel2000_15.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_16","Resinas,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._268mm_x_210mm._2000.")><img src="galeria_mini/papel2000_16.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_17","Resinas,_tierra,_acuarela_y_�leo_sobre_papel.-265mm_x_215mm._2000.")><img src="galeria_mini/papel2000_17.gif" title="Papel 2000 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2000_18","Resina,_tierra,_collage_infogr�fico,_acuarela-y_�leo_sobre_papel._480mm_x_340mm._2000.")><img src="galeria_mini/papel2000_18.gif" title="Papel 2000 - Jordi G�ell"></a>';
			break;
			
			case "papel2003":
			echo '
			<a href=javascript:galeria("papel2003_01","Resina,_polvo_de_m�rmol_y_collage_infogr�fico_sobre_papel.-330_x_250mm._2003")><img src="galeria_mini/papel2003_01.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_02","Resina,_acuarela,_polvo_de_m�rmol_y-collage_infogr�fico_sobre_papel._310_x_250mm._2003")><img src="galeria_mini/papel2003_02.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_03","Resina,_acuarela,_polvo_de_m�rmol_y-collage_infogr�fico_sobre_papel._305_x_25mm._2003")><img src="galeria_mini/papel2003_03.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_04","Resina,_acuarela,_polvo_de_m�rmol_y-collage_infogr�fico_sobre_papel._280_x_250mm._2003")><img src="galeria_mini/papel2003_04.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_05","Resina,_acuarela,_polvo_de_m�rmol_y-collage_infogr�fico_sobre_papel._290_x_250mm._2003")><img src="galeria_mini/papel2003_05.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_06","Resina,_polvo_de_m�rmol,_grattage_y-collage_infogr�fico_sobre_papel._330_x_250mm._2003")><img src="galeria_mini/papel2003_06.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_07","Resina,_polvo_de_m�rmol,_grattage_y-collage_infogr�fico_sobre_papel._320_x_250mm._2003")><img src="galeria_mini/papel2003_07.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_09","Resina,_polvo_de_m�rmol,_grattage_y-collage_infogr�fico_sobre_papel._380_x_330mm._2003")><img src="galeria_mini/papel2003_09.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_10","Acuarela_y_collage_infogr�fico_sobre_papel._390_x_330mm._2003")><img src="galeria_mini/papel2003_10.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_11","Acuarela_y_collage_infogr�fico_sobre_papel._390_x_330mm._2003")><img src="galeria_mini/papel2003_11.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_12","Acuarela_y_collage_infogr�fico_sobre_papel._390_x_330mm._2003")><img src="galeria_mini/papel2003_12.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_13","Acuarela_y_collage_infogr�fico_sobre_papel._390_x_330mm._2003")><img src="galeria_mini/papel2003_13.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_14","Resinas,_polvo_de_m�rmol,_collage_infogr�fico,-papel_y_�leo_sobre_papel._240_x_220mm._2003")><img src="galeria_mini/papel2003_14.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_15","Resinas,_polvo_de_m�rmol,_collage_infogr�fico,-papel_y_�leo_sobre_papel._305_x_240mm._2003")><img src="galeria_mini/papel2003_15.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_16","Resinas,_polvo_de_m�rmol,_collage_infogr�fico,-papel_y_�leo_sobre_papel._280_x_240mm._2003")><img src="galeria_mini/papel2003_16.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_17","Resinas,_polvo_de_m�rmol,_collage_infogr�fico,-papel_y_�leo_sobre_papel._320_x_330mm._2003")><img src="galeria_mini/papel2003_17.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_18","Resinas,_polvo_de_m�rmol,_collage_infogr�fico,-papel_y_�leo_sobre_papel._360_x_310mm._2003")><img src="galeria_mini/papel2003_18.gif" title="Papel 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2003_19","Resinas,_polvo_de_m�rmol,_collage_infogr�fico,-papel_y_�leo_sobre_papel._460_x_400mm._2003")><img src="galeria_mini/papel2003_19.gif" title="Papel 2003 - Jordi G�ell"></a>';
			break;
			
			case "papel2005":
			echo '
			<a href=javascript:galeria("papel2005_01","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_01.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_02","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_02.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_03","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_03.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_04","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_04.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_05","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_05.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_06","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_06.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_07","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_07.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_08","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_08.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_09","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_09.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_10","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_10.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_11","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_11.gif" title="Papel 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("papel2005_12","�leo_y_ceras_sobre_papel_1250mm_x_180mm._2005")><img src="galeria_mini/papel2005_12.gif" title="Papel 2005 - Jordi G�ell"></a>';
			break;
			
			case "libro1":
			echo '
			<a href=javascript:galeria("libro1_1","-A_imagen_y_semejanza_2006._Pag._56")><img src="galeria_mini/libro1_1.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro1_2","-A_imagen_y_semejanza_2006._Pag._57")><img src="galeria_mini/libro1_2.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro1_3","-A_imagen_y_semejanza_2006._Pag._58")><img src="galeria_mini/libro1_3.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro1_4","-A_imagen_y_semejanza_2006._Pag._59")><img src="galeria_mini/libro1_4.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro1_5","-A_imagen_y_semejanza_2006._Pag._60")><img src="galeria_mini/libro1_5.gif" title="2006 - Jordi G�ell"></a>';
			break;
			
			case "libro2":
			echo '
			<a href=javascript:galeria("libro2_1","-La_paraula_�s_un_gest_2005_Pag._35")><img src="galeria_mini/libro2_1.gif" title="2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro2_2","-La_paraula_�s_un_gest_2005_Pag._36")><img src="galeria_mini/libro2_2.gif" title="2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro2_3","-La_paraula_�s_un_gest_2005_Pag._37")><img src="galeria_mini/libro2_3.gif" title="2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro2_4","-La_paraula_�s_un_gest_2005_Pag._38")><img src="galeria_mini/libro2_4.gif" title="2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro2_5","-La_paraula_�s_un_gest_2005_Pag._39")><img src="galeria_mini/libro2_5.gif" title="2005 - Jordi G�ell"></a>';
			break;
			
			case "libro3":
			echo '
			<a href=javascript:galeria("libro3_1","-Los_ojos_sobre_el_olvido_2003_Pag._27")><img src="galeria_mini/libro3_1.gif" title="2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro3_2","-Los_ojos_sobre_el_olvido_2003_Pag._28")><img src="galeria_mini/libro3_2.gif" title="2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro3_3","-Los_ojos_sobre_el_olvido_2003_Pag._29")><img src="galeria_mini/libro3_3.gif" title="2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro3_4","-Los_ojos_sobre_el_olvido_2003_Pag._30")><img src="galeria_mini/libro3_4.gif" title="2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro3_5","-Los_ojos_sobre_el_olvido_2003_Pag._31")><img src="galeria_mini/libro3_5.gif" title="2003 - Jordi G�ell"></a>';
			break;
			
			case "libro4":
			echo '
			<a href=javascript:galeria("libro4_1","-Manuscrito_de_las_musas._Las_nieblas_de_Tokio_2006_Pag._31")><img src="galeria_mini/libro4_1.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro4_2","-Manuscrito_de_las_musas._Las_nieblas_de_Tokio_2006_Pag._32")><img src="galeria_mini/libro4_2.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro4_3","-Manuscrito_de_las_musas._Las_nieblas_de_Tokio_2006_Pag._33")><img src="galeria_mini/libro4_3.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro4_4","-Manuscrito_de_las_musas._Las_nieblas_de_Tokio_2006_Pag._34")><img src="galeria_mini/libro4_4.gif" title="2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("libro4_5","-Manuscrito_de_las_musas._Las_nieblas_de_Tokio_2006_Pag._35")><img src="galeria_mini/libro4_5.gif" title="2006 - Jordi G�ell"></a>';
			break;
			
			case "digit2003":
			echo '
			<a href=javascript:galeria("digit2003_1","Imagen_digital._168,3M._150cm_x_112,44cm._2003")><img src="galeria_mini/digit2003_1.gif" title="Arte Digital 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2003_2","Imagen_digital._168,3M._150cm_x_112,47cm._2003")><img src="galeria_mini/digit2003_2.gif" title="Arte Digital 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2003_3","Imagen_digital._168,3M._150cm_x_112,47cm._2003")><img src="galeria_mini/digit2003_3.gif" title="Arte Digital 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2003_4","Imagen_digital._168,3M._150cm_x_112,47cm._2003")><img src="galeria_mini/digit2003_4.gif" title="Arte Digital 2003 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2003_5","Imagen_digital._168,3M._150cm_x_112,47cm._2003")><img src="galeria_mini/digit2003_5.gif" title="Arte Digital 2003 - Jordi G�ell"></a>';
			break;
			
			case "digit2004":
			echo '
			<a href=javascript:galeria("digit2004_1","Tr�pticoA1._Imagen_digital._160,9M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_1.gif" title="Arte Digital 2004 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2004_2","Tr�pticoA2._Imagen_digital._160,9M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_2.gif" title="Arte Digital 2004 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2004_3","Tr�pticoA3._Imagen_digital._160,9M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_3.gif" title="Arte Digital 2004 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2004_4","Imagen_digital._231,7M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_4.gif" title="Arte Digital 2004 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2004_5","Tr�pticoB1._Imagen_digital._160,9M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_5.gif" title="Arte Digital 2004 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2004_6","Tr�pticoB2._Imagen_digital._160,9M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_6.gif" title="Arte Digital 2004 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2004_7","Tr�pticoB3._Imagen_digital._160,9M._150cm_x_150cm._2004")><img src="galeria_mini/digit2004_7.gif" title="Arte Digital 2004 - Jordi G�ell"></a>';
			break;
			
			case "digit2005":
			echo '
			<a href=javascript:galeria("digit2005_1","Imagen_digital._286,1M._100cm_x_100cm._2005")><img src="galeria_mini/digit2005_1.gif" title="Arte Digital 2005 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2005_2","Imagen_digital._11,4M._200cm_x_200cm._2006")><img src="galeria_mini/digit2005_2.gif" title="Arte Digital 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2005_3","Imagen_digital._45,8M._200cm_x_200cm._2006")><img src="galeria_mini/digit2005_3.gif" title="Arte Digital 2006 - Jordi G�ell"></a>
			<a href=javascript:galeria("digit2005_4","Imagen_digital._41,3M._187,6cm_x_192,4cm._2006")><img src="galeria_mini/digit2005_4.gif" title="Arte Digital 2006 - Jordi G�ell"></a>';
			break;
			
			default:
			echo '&nbsp;';
			break;
		}
		?>
		</div>
        
<?php include("piecera.php"); ?>