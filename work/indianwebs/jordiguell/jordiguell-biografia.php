<?php include("cabecera.php"); ?>

<table cellpadding="0" cellspacing="0" align="center" class="tabla1">
<tr>
	<td class="col_1">
		<h1><a href="jordiguell-biografia.php"><img src="img/tit_biografia<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Biografia de Jordi G�ell" title="Biografia de Jordi G�ell"></a></h1>
		<h1><a href="jordiguell-obras.php"><img src="img/tit_obras<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Obra de Jordi G�ell" title="Obra de Jordi G�ell"></a></h1>
		<h1><a href="javascript:popup()"><img src="img/tit_info.gif" border="0"></a></h1>
		<h1><a href="jordiguell-contacta.php"><img src="img/tit_contacta<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Contacta Jordi Guell" title="Contacta Jordi Guell"></a></h1>
	</td>
	<td id="col_2">

		<div id="container" style="position:relative;width:400px;height:450px;overflow:hidden;float:left;">
		<div id="content" style="position:absolute;width:390px;left:0;top:0">
			<h3>Jordi G&uuml;ell</h3>
            
            <?php
			switch($_SESSION["idioma"])
			{
				case "cat":
				echo '<ul><li>Molins de  Rei, Espanya, 1973.</li>
				<li>Utilitzant com a refer�ncia el reportatge fotogr�fic realitzat per Sebastiao Salgado el  1985 a Eti�pia, presenta el 1992 la seva primera exposici� a Sant Boi de  Llobregat, amb la tem�tica de la gana en els pa�ssos pobres.</li>
				<li>1990-96, est�dia Procediments Pict�rics i T�cniques Murals a l�Escola Llotja, Barcelona.</li></ul>
				<br>
				<h3>EXPOSICIONS I PUBLICACIONS</h3>
				<h5>2008</h5>
				  Col�lectiva al Museu d�Art Contemporani  Conde Duque. Madrid, Espa&ntilde;a.<br>
				  Cat�leg <i>"Encuentros con el Arte  Actual de Pintura y Escultura"</i>.
				<h5>2007</h5>
				  Individual a l�Ateneu de Sant Cugat, amb col�laboraci�  de Canals-Galeria d�Art. Sant �� Cugat,  Espanya.<br>
				  Recital de poesia. <i>"Homenatge a Yael  Langella"</i>. Galeria d�Art Maragall.
				<h5>2006</h5>
				  Llibre de prosa po�tica, Manuscrito de las  musas.<br>
				  Llibre d�assaig sobre �tica i est�tica,  A imagen y semejanza.
				<h5>2005</h5>  
				  Llibre de poesia, No puc dar-me enteniments.<br>
				  Llibre de poesia, La paraula �s un  gest.<br>
				  Col�lectiva Galer�a ADN. Barcelona,  Espanya.<br>
				  Col�lectiva Darby Louise. Barcelona,  Espanya.<br>
				  Col�lectiva Barcelona Presence.  Water Tower Place. Chicago, EEUU.
				<h5>2004</h5>  
				  Col�lectiva Darby Louise. Barcelona, Espanya.<br>
				  Col�lectiva Barcelona Presence.  Water Tower PLace. Chicago, EEUU.<br>
				  Individual &quot;Las piedras  dormidas&quot;, Darby Louise. Barcelona, Espanya.
				<h5>2003</h5>
				  Llibre de poesia, Los ojos sobre el olvido.<br>
				  Publicaci� de la poesia Almas al  Barro. Lizaso. El s�mbolo y la obra.<br>
				  Col�lectiva &quot;La dona S.  XXI&quot;. Darby Louise. Barcelona, Espanya.<br>
				  Feria Interart. Valencia, Espanya.<br>
				  Col�lectiva &quot;Dal�. Peque&ntilde;o  formato&quot;. Darby Louise. Barcelona, Espanya.<br>
				  Individual Galer�a Akka. Valencia, Espanya.<br>
				  Col�lectiva <i>"El entorno de Jes�s  Lizaso"</i>. Bilbao, Espanya.<br>
				  Col�lectiva Galer�a Akka. Valencia,  Espanya.
				<h5>2002</h5>  
				  Publicaci� del llibre Jordi G�ell. Entre yo  y la monta&ntilde;a.<br>
				  Col�lectiva &quot;El recuerdo de una  mirada&quot;. Darby Louise. Barcelona, Espanya.<br>
				  Individual &quot;Entre yo y la  monta&ntilde;a&quot;, Darby Louise. Barcelona, Espanya.
				<h5>2001</h5>  
				  Publicaci� de la poesia A Jes�s Lizaso.  Lizaso. Arte y sentimiento.<br>
				  Individual &quot;The fertile  seed&quot;, Alliance Fran�aise. Chicago, EEUU.<br>
				  Col�lectiva &quot;Di�legs fi de  segle&quot;. Darby Louise. Barcelona, Espanya. <br>
				  Individual Pane Caldo. Chicago,  EEUU.<br>
				  Individual &quot;509: An American  Experience&quot;. Instituto Cervantes.�  Chicago, EEUU.<br>
				  Individual Galer�a Abad Agu�rre.  Bilbao, Espanya.<br>
				  Individual Darby Louise Cultural  Center. Chicago, EEUU.
				<h5>2000</h5>  
				  &quot;SARTC`2000&quot;. Ateneu igualad�.  Igualada, Espanya. <br>
				  Col�lectiva Premis Eclecxis&quot;,  Galer�a Ol�mpica. Fundaci� Galer�a Olimpica. Barcelona,Espanya. <br>
				  Individual Ateneu lgualad�.  Igualada, Espanya.<br>
				  Individual Casal de Cultura.  Matadapera, Espanya. <br>
				  Individual Sala Salvador Espriu.  Viladrau, Espanya.<br>
				  Individual &quot;Ut poesis tac�ta  pictura est&quot;. Darby Louise COS. Barcelona, Espanya.
				<h5>1997</h5>
				  Mural per la Fundaci� Esclerosis M�ltiple.  Barcelona, Espanya.
				<h5>1996</h5>
				  Mural pel vest�bulo de l�Escola d�Arts  Aplicades i Oficis Art�stics. Barcelona, ��� Espanya.<br>
				  Col�lectiva Casa de Cultura de  Matanzas. Matanzas, Cuba.<br>
				  Col�lectiva Sala Ausart. Vic, Espanya. <br>
				  Col�lectiva Sala Creus. Terrassa,  Espanya.<br>
				  Fira d`Art de Vilanova. Espanya. <br>
				  Individual Galeria Periferiart. Lleida,  Espanya.
				<h5>1995</h5>
				  Col�lectiva Galeria Carme Sala. Molins de  Rei, Espanya.<br>
				  Col�lectiva Galeria Periferiart.  Lleida, Espanya. <br>
				  Col�lectiva Cercle Art�stic de Sant  Lluc. Barcelona, Espanya. <br>
				  Col�leciva Sala de Cultura de  l`Antic Mercat de Sitges. Espanya.
				<h5>1994</h5>
				  Sal� de l`Ensenyament. Fira de Barcelona.  Barcelona, Espanya. <br>
				  Col�lectiva Galeria Manifiesto.  Barcelona, Espanya.
				<h5>1993</h5>
				  Sal� de l`Ensenyament. Fira de Barcelona.  Barcelona, Espanya.<br>
				  Mostra d�Art al Max Center.  Hospitalet de Llobregat, Espanya.
				<h5>1991</h5>
				  Beca para el Cercle Art�stic Sant Lluc.  Barcelona, Espanya.<br>
				  Casal d�Extremadura. St Boi de  Llobregat, Espanya.
				<h5>1988</h5>
				  Seg�n Premi Acad�mia Josso.
				<h5>1986</h5>  
				  Publicaci� d�il�lustracions a la revista TOT  Molins.';
				break;
				
				case "ing":
				echo '<ul><li>Molins  de Rei, Spain, 1973.</li>
				<li>First  exhibit 1992, in Sant Boi de Lobregat (Barcelona), the subject being, hunger in  the underdeveloped countries, uses as referente the 1985 photographic work of  Sebastiao Salgado in Etiopia.</li>
				<li>Studied  Painting Procedures and Mural Techniques at Escuela Llotja in Barcelona. Spain.</li></ul>
				
				<h3>EXHIBITS  AND PUBLICATIONS:</h3>
				<h5>2008</h5>
				  Museum of Contemporary Art Conde Duque.  Madrid, Spain.<br>
				  Catalog <i>"Encuentros con el Arte  Actual de Pintura y Escultura"</i>.
				<h5>2007</h5>
				  One person exhibit, <i>"Pintura de temps"</i>, Ateneu de Sant Cugat and Canals-Galeria d`Art. Sant Cugat, Spain.
				<h5>2006</h5>
				  <i>"Manuscrito de las musas"</i> Book in poetic  prose.<br>
				  <i>"A imagen y semejanza"</i> Essay in  ethics and esthetics.
				<h5>2005</h5>
				  <i>"No puc dar-me enteniments"</i> Book of poems.<br>
				  <i>"La paraula �s un gest"</i> Book of  poems.<br>
				  Group exhibit, Galeria ADN.  Barcelona, Spain.<br>
				  Group exhibit, Darby Louise.  Barcelona, Spain.<br>
				  Group exhibit, <i>"Barcelona Presence"</i>  Water Tower. Chicago, EEUU.
				<h5>2004</h5>
				  One person exhibit, <i>"Las piedras dormidas"</i> Darby Louise. Barcelona, Spain.<br>
				  Group exhibit, Darby Louise.  Barcelona, Spain.<br>
				  Group exhibit, <i>"Barcelona Presence"</i> Water Tower. Chicago, EEUU.<br>
				  2003��� One person exhibit, Galeria Akka. Valencia, Spain.<br>
				  Completes  his poetry book <i>"Los ojos sobre el olvido"</i><br>
				  Publishes  the poem <i>"Alma al barro"</i>, Lizaso. El S�mbolo y la obra<br>
				  Group exhibit, Mujer S.XXI, Darby  Mouise. Barcelona, Spain.<br>
				  Feria  Interart. Valencia, Spain.<br>
				  Group exhibit, <i>"Dal� small format"</i>,  Darby Louise. Barcelona, Spain.<br>
				  Group exhibit, Galeria Akka.  Valencia, Spain.<br>
				  Group exhibit, <i>"The entourage of  Jesus Lizaso"</i>. Bilbao, Spain.
				<h5>2002</h5>
				 One person exhibit <i>"Entre yo y la monta&ntilde;a"</i>, Darby Louise.  Barcelona, Spain.<br>
				  Jordi  G�ell�s book <i>"Entre yo y la monta&ntilde;a"</i> is published.<br>
				  Group  exhibit <i>"El recuerdo de una mirada"</i>, Darby Louise. Barcelona, Spain.
				<h5>2001</h5>
				  One person exhibit, Pane Caldo. Chicago,  EEUU.<br>
				  One person exhibit, <i>"509: An  American Experience"</i>, Instituto Cervantes.�Chicago,  ������������������� EEUU.<br>
				  One person exhibit, Galeria Abad  Aguirre. Bilbao, Spain.<br>
				  One person exhibit, Darby Louise  Cultural Center. Chicago, EEUU.<br>
				  One person exhibit, <i>"The Fertile  Seed"</i>, Allian�e Fran�aise. Chicago, EEUU.<br>
				  The poem <i>"A Jesus Lizaso"</i> is  published. Lizaso Arte y Sentimiento.<br>
				  Group  exhibit, �Di�legs fi de segle�, Darby Louise. Barcelona, Spain.
				<h5>2000</h5>
				  One person exhibit, Ateneu Igualad�. Igualada, Spain.<br>
				  One  person exhibit, Casal de Cultura. Matadepera, Spain.<br>
				  One  person exhibit, Sala Salvador Espriu. Viladrau, Spain.<br>
				  One  person exhibit, <i>"Ut poesis tacita pictura est"</i>, Darby Lousie COS. Barcelona,  Spain.<br>
				  Fair, <i>"SARTC 2000"</i>, Ateneu Igualad�. Igualada, Spain.<br>
				  Group exhibit, <i>"Premis Eclecxis"</i>, Galeria Ol�mpica. Barcelona, Spain.<br>
				<h5>1997</h5>
				  Mural for the Multiple Esclerosis  Foundation. Barcelona, Spain.
				<h5>1996</h5>
				 One person exhibit, Galer�a Periferiart.  Lleida, Spain.<br>
				  Mural in the lobby of the school for  Applied Arts and Artistic Trades. Barcelona, Spain.<br>
				  Group  exhibit, Casa de Cultura de Matanzas. Matanzas, Cuba.<br>
				  Group exhibit, Sala Ausart. Vic,  Spain.<br>
				  Group exhibit, Sala Creus. Terrassa,  Spain.<br>
				  Fira d�Art de Vilanova, Art Fair. Barcelona, Spain.
				<h5>1995</h5>
				  Group exhibit, Galer�a Carme Sala. Molins de Rei, Spain.<br>
				  Group exhibit, Galer�a Periferiart.  Lleida, Spain.<br>
				  Group exhibit, Cercle Art�stic de  Sant Lluc. Barcelona, Spain.<br>
				  Group  exhibit, Sala de Cultura de l�Antic Mercat de Sitges. Sitges, Spain.
				<h5>1994</h5>
				  Feria del Sal� de l�Ensenyament, Fair of Barcelona. Barcelona,  Spain.<br>
				  Group  exhibit, Galer�a Manifiesto. Barcelona, Spain.
				<h5>1993</h5>
				  Feria del Sal� de l�Ensenyament, Fair of Barcelona. Barcelona, Spain.<br>
				  Group exhibit at Mostra d�Art Max  Center. Hospitalet de Llobregat, Spain.
				<h5>1992</h5>
				Scholarship for the Cercle Art�stic de Sant  Lluc. Barcelona, Spain.<br>
				  Group exhibit, Casal d�Extremadura.  St Boi de Llobregat, Spain.
				<h5>1988</h5>
				  Second prize, Academia Josso.
				<h5>1986</h5>
				  Publishes Illustrations, Magazine <i>"TOT  Molins"</i>.';
				break;
				
				default:
				echo '<ul><li>Nacido en Molins de Rei el 15 de mayo de 1973.</li>
				<li>Cursa estudios de Procedimientos Pict&oacute;ricos y  T&eacute;cnicas Murales en la Escuela Llotja, Barcelona.</li>
				<li>Utilizando como referencia el reportage fotogr&aacute;fico realizado por Sebastiao Salgado en el 1985 en Etiop&iacute;a, realiza en  1992 su primera exposici&oacute;n en Sant Boi de Llobregat con la tem&aacute;tica del hambre en los pa&iacute;ses pobres.</li></ul>
				
				<h3>Exposiciones  y publicaciones</h3>
				<h5>2008</h5>
				  Colectiva Museo de Arte Contemporaneo Conde Duque. Madrid, Espa&ntilde;a.<br />
				  Cat&aacute;logo <i>"Encuentros con el Arte  Actual de Pintura y Escultura".</i>
				<h5>2007</h5>
				  Individual, <i>"Pintura de temps"</i>, en el Ateneu de Sant Cugat con colaboraci&oacute;n Canals-Galeria de Arte. Sant Cugat, Espa&ntilde;a.<br>
				  Recital de poesia. <i>"Homenatge a Yael Langella"</i>. Galeria de Arte Maragall.
				<h5>2005</h5>
				  Termina su  libro de poes&iacute;a <i>No puc dar-me enteniments.</i><br />
				  Termina su  libro de poes&iacute;a <i>La paraula &eacute;s un gest.</i><br />
				  Colectiva en  Galer&iacute;a ADN. Barcelona.<br>
				  Colectiva en  Darby Louise. Barcelona.<br>
				  Colectiva en  Barcelona Presence. Water Tower Place. Chicago.
				<h5>2004</h5>
				  Exposici&oacute;n  individual <i>Las piedras dormidas</i>, Darby Louise. Barcelona.<br />
				  Colectiva en  Darby Louise. Barcelona.<br />
				  Colectiva en  Barcelona Presence. Water Tower PLace. Chicago.
				<h5>2003</h5>
				  Exposici&oacute;n  individual en Galer&iacute;a Akka.&nbsp; Valencia.<br />
				  Termina su  libro de poes&iacute;a <i>Los ojos sobre el olvido.</i><br />
				  Publicaci&oacute;n  de la poes&iacute;a <i>Almas al Barro.</i> Lizaso. El s&iacute;mbolo y la obra.<br />
				  Colectiva  Mujer S. XXI. Darby Louise. Barcelona.<br />
				  Feria  Interart. Valencia.<br />
				  Colectiva  Dal&iacute;, peque&ntilde;o formato. Darby Louise. Barcelona.<br />
				  Colectiva  Galer&iacute;a Akka. Valencia.<br />
				  Colectiva el  entorno de Jes&uacute;s Lizaso. Bilbao. Euskadi.
				<h5>2002</h5>
				  Exposici&oacute;n  individual <i>Entre yo y la monta&ntilde;a,</i> Darby Louise. Barcelona.<br />
				  Publicaci&oacute;n  del libro <i>Jordi G&uuml;ell. Entre yo y la monta&ntilde;a.</i><br />
				  Colectiva <i>El recuerdo de una mirada.</i> Darby Louise. Barcelona.
				<h5>2001</h5>
				  Exposici&oacute;n  individual en Pane Caldo. Chicago.<br />
				  Exposici&oacute;n  individual <i>509: An American Experience.</i> Instituto Cervantes.&nbsp; Chicago.<br />
				  Exposici&oacute;n  individual en Galer&iacute;a Abad Agu&iacute;rre. Bilbao.<br />
				  Exposici&oacute;n  individual en Darby Louise Cultural Center. Chicago.<br />
				  Exposici&oacute;n individual <i>The fertile seed.</i> Alliance Fran&ccedil;aise. Chicago.<br />
				  Publicaci&oacute;n  de la poes&iacute;a <i>A Jes&uacute;s Lizaso.</i> Lizaso. Arte y sentimiento.<br />
				  Colectiva <i> Di&aacute;legs fi de segle.</i> Darby Louise. Barcelona.
				<h5>2000</h5>
				  Exposici&oacute;n  individual en Ateneu lgualad&iacute;. Igualada.<br />
				  Exposici&oacute;n  individual en Casal de Cultura. Matadapera.<br />
				  Exposici&oacute;n  individual en Sala Salvador Espriu. Viladrau.<br />
				  Exposici&oacute;n  individual <i>Ut poesis tac&iacute;ta pictura est.</i> Darby Louise COS. Barcelona.<br />
				  Feria SARTC&rsquo;2000. Ateneu igualad&iacute;, Igualada.<br />
				  Colectiva  Premis Eclecxis, Galer&iacute;a Ol&iacute;mpica. Fundaci&oacute; Galer&iacute;a Ol&iacute;mpica. Barcelona.
				<h5>1997</h5>
				  Mural para  la Fundaci&oacute;n Esclerosis M&uacute;ltiple. Barcelona.
				<h5>1996</h5>
				  Exposici&oacute;n  individual en Galer&iacute;a Periferiart. Lleida.<br />
				  Mural para  el vest&iacute;bulo de la Escuela de Artes Aplicadas y Oficios Art&iacute;sticos. Barcelona.<br />
				  Colectiva en  Casa de Cultura de Matanzas. Cuba.<br />
				  Colectiva en  Sala Ausart. Vic. <br />
				  Colectiva en  Sala Creus. Terrassa.<br />
				  Fira d&rsquo;Art  de Vilanova. Barcelona. 
				<h5>1995</h5>
				  Colectiva en  Galer&iacute;a Carme Sala. Molins de Rei. <br />
				  Colectiva en  Galer&iacute;a Periferiart. Lleida. <br />
				  Colectiva en  Cercie Art&iacute;stic de Sant Lluc. Barcelona. <br />
				  Colectiva en  Sala de Cultura de l&rsquo;Anfic Mercat de Sitges. Sitges.
				<h5>1994</h5>
				  Feria del  Sal&oacute; de I&rsquo;Ensenyament. Fira de Barcelona. Barcelona. <br />
				  Colectiva en  Galer&iacute;a Manifiesto. Barcelona.
				<h5>1993</h5>
				  Feria del  Sal&oacute; de I&rsquo;Ensenyament. Fira de Barcelona. Barcelona.<br />
				  Colectiva en  Mostra d&rsquo;Art al Max Center. Hospitalet de Llobregat.
				<h5>1992</h5>
				  Beca para el  Cercle Art&iacute;stic Sant Lluc. Barcelona.<br />
				  Colectiva en  Casal de Extremadura. St Boi de Llobregat.
				<h5>1988</h5>
				  Segundo  Premio Acad&egrave;mia Josso.
				<h5>1986</h5>
				  Publicaci&oacute;n  de ilustraciones en la revista TOT Molins.';
				break;
			}
			?>
		</div>
		</div>
		<script language="javascript">
		function movedown()
		{
			if (parseInt(document.getElementById("content").style.top) >= (document.getElementById("content").offsetHeight*(-1)+100))
				document.getElementById("content").style.top=parseInt(document.getElementById("content").style.top)-5+"px";
			movedownvar=setTimeout("movedown()",20);
		}
		function moveup()
		{
			if (parseInt(document.getElementById("content").style.top) <=0 )
				document.getElementById("content").style.top=parseInt(document.getElementById("content").style.top)+5+"px";
			moveupvar=setTimeout("moveup()",20);
		}
		</script>
		
		<div>
		<img src="img/flecha1.gif" border="0" onMouseover="moveup()" onMouseout="clearTimeout(moveupvar)"> 
		<br><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><em>.</em><br>
		<img src="img/flecha2.gif" border="0" onMouseover="movedown()" onMouseout="clearTimeout(movedownvar)">
		</div>

	</td>
	<td id="col_3">
		<div id="minis">&nbsp;</div>

<?php include("piecera.php"); ?>