<?php include("cabecera.php"); ?>

<table cellpadding="0" cellspacing="0" align="center" class="tabla2">
<tr>
	<td class="col_1">
		<h1><a href="jordiguell-biografia.php"><img src="img/tit_biografia<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Biografia de Jordi G�ell" title="Biografia de Jordi G�ell"></a></h1>
		<h1><a href="jordiguell-obras.php"><img src="img/tit_obras<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Obra de Jordi G�ell" title="Obra de Jordi G�ell"></a></h1>
	
		<h4 onmouseover='seccion(1)'><img hspace='3' src='img/titulo_pintura<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Pintura de Jordi Guell' title='Pintura de Jordi Guell'></h4>
		<h4 onmouseover='seccion(2)'><img hspace='3' src='img/titulo_papel<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Jordi Guell en Papel' title='Jordi Guell en Papel'></h4>
		<h4 onmouseover='seccion(3)'><img hspace='3' src='img/titulo_infografia<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Jordi Guell Infografia' title='Jordi Guell Infografia'></h4>
		<h4 onmouseover='seccion(4)'><img hspace='3' src='img/titulo_escritura<?php echo "_".$_SESSION["idioma"]?>.gif' border='0' alt='Escritura de Jordi Guell' title='Escritura de Jordi Guell'></h4>
            
		<h1><a href="javascript:popup()"><img src="img/tit_info.gif" border="0"></a></h1>
		<h1><a href="jordiguell-contacta.php"><img src="img/tit_contacta<?php echo "_".$_SESSION["idioma"]?>.gif" border="0" alt="Contacta Jordi Guell" title="Contacta Jordi Guell"></a></h1>
	</td>
	<td id="col_2">
    	<?php
		switch($_SESSION["idioma"])
		{
			case "cat":
			echo '<p id="centrado">&quot;Dins l`Art, com oracle, s`hi manifesten passions,  motius, misteris que embriaguen individus i ambients, prenent-los enll� del  temps, i m�s enll� d`un sentit cartesi� de l`espai.<br>Amb l`Art les persones sempre hi veiem, sens dubte. Per� no  amb la mirada del pensament, ni amb l`estructura de la ra�, ni amb la  linealitat de la hist�ria. Les persones hi veiem amb l`Art, i all� vist �s  solament real i per tant encara inexplicable.&quot;.</p>';
			break;
			
			case "ing":
			echo '<p id="centrado">&quot;The work, as an oracle, expresses passions; motifs that inebriate the  individual and the atmosphere beyond the Cartesian conception of time and  space.<br>Trough his work, the artist sees and looks, but not with the look of his  thoughts. Nor with the structure of reason or the linearity of history. The  artist sees something in his work; he traps something without comprehension. It  is simply real, inexplicable&quot;.</p>';
			break;
			
			default:
			echo '<p id="centrado">&quot;La obra, como or&aacute;culo, manifiesta pasiones, motivos que embriagan y toman a individuo y a ambiente, m&aacute;s all&aacute; del tiempo y del divorcio cartesiano del lenguaje.<br><br>En la obra el artista ve, sin lugar a dudas. Pero no con la   mirada del pensamiento. Ni con la estructura de la raz&oacute;n. Ni con la linealidad   de la historia. El artista ve en la obra, atrapa algo a&uacute;n sin comprensi&oacute;n. Y simplemente es real, inexplicable&quot;.</p>';
			break;
		}
		?>
	</td>
	<td id="col_3">
		<div id="minis">&nbsp;</div>
        
<?php include("piecera.php"); ?>
