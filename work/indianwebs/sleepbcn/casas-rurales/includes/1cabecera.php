<?php include($_SERVER['DOCUMENT_ROOT']."/privado/funciones.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Casa Rurales <?=strtoupper($_GET["provincia"])?></title>
    <meta name="title" content="Casa Rurales">
    <meta name="description" content="Casa Rurales">
    <meta name="keywords" content="Casa Rurales">
    <META NAME="REVISIT-AFTER" CONTENT="7 Days">
    <META NAME="REVISIT" CONTENT="7 days">
    <META NAME="ROBOT" CONTENT="Index,Follow">
    <META NAME="ROBOTS" CONTENT="All">
    <META NAME="DISTRIBUTION" CONTENT="Global">
    <meta http-equiv="Pragma" content="no-cache">
    <META NAME="RATING" CONTENT="general">
    <meta name="language" content="">
    <META NAME="ABSTRACT" CONTENT="casas rurales">
    <META NAME="SUBJECT" CONTENT="casas rurales">
    <META NAME="AUTHOR" CONTENT="casas rurales">
    <META NAME="COPYRIGHT" CONTENT="casas rurales">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="includes/estilos.css" />
    <script language="javascript" type="text/javascript" src="http://www.sleepbcn.com/privado/funciones.js"></script>
</head>
<body onload="iniciar()">

<table cellpadding="0" cellspacing="0" align="center" class="tabla1">
<tr>
	<td colspan="2"><h1>Sleep Rural | Alquiler de Casas Rurales</h1></td>
</tr>
<tr>
	<td colspan="2" class="cabecera">
      <div id="idiomas"><img src="http://www.sleepbcn.com/casas-rurales/img/lateral.gif" alt="Castellano - English" usemap="#mapeado1"/></div>
   	  <div>
      <a title="Casas Rurales " href="index.php" class="boton1">casas</a>
      <a title="Hoteles Rurales" href="hoteles.php" class="boton2">hoteles</a>
      <a title="Eventos" href="eventos.php" class="boton3">eventos</a>
      <a title="Condiciones de alquiler" href="condiciones.php" class="boton4">condiciones</a>
      <a title="Contactar Sleep Rural" href="contacto.php" class="boton5">contacto</a>
      </div>
    </td>
</tr>
<tr>
	<td class="izq">
    	<?php
		if(strstr($_SERVER['PHP_SELF'],'/alojamiento.php'))
			echo '<form action="alojamiento.php?id='.$_GET["id"].'" method="post" name="form_bus">';
		else
			echo '<form action="resultados.php" method="post" name="form_bus">';
		?>
        <? echo FECHA_LLEGADA ?>:<br/>
        <select name="dia1" onchange="fecha_ant(1)"><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
		<select name="mes1" onchange="fecha_ant(2)"><option value="01"><? echo MES1 ?></option><option value="02"><? echo MES2 ?></option><option value="03"><? echo MES3 ?></option><option value="04"><? echo MES4 ?></option><option value="05"><? echo MES5 ?></option><option value="06"><? echo MES6 ?></option><option value="07"><? echo MES7 ?></option><option value="08"><? echo MES8 ?></option><option value="09"><? echo MES9 ?></option><option value="10"><? echo MES10 ?></option><option value="11"><? echo MES11 ?></option><option value="12"><? echo MES12 ?></option></select>
		<select name="ano1" onchange="fecha_ant(3)"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option></select>
        <? echo FECHA_SALIDA ?>: <br/>
        <select name="dia2"><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
		<select name="mes2"><option value="01"><? echo MES1 ?></option><option value="02"><? echo MES2 ?></option><option value="03"><? echo MES3 ?></option><option value="04"><? echo MES4 ?></option><option value="05"><? echo MES5 ?></option><option value="06"><? echo MES6 ?></option><option value="07"><? echo MES7 ?></option><option value="08"><? echo MES8 ?></option><option value="09"><? echo MES9 ?></option><option value="10"><? echo MES10 ?></option><option value="11"><? echo MES11 ?></option><option value="12"><? echo MES12 ?></option></select>
		<select name="ano2"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option></select>
        <br />
        Provincia: <span style="padding-left:46px;">Tipo:</span><br />
        <select name="provincia"><option value="0" selected="selected">Todas</option><?php include($_SERVER['DOCUMENT_ROOT']."/casas-rurales/includes/inc.provincias.php");?></select>&nbsp;&nbsp;
        <select name="tipo"><option value="rural">Casa Rural</option><option value="hotel">Hotel Rural</option></select><br/>
        <? echo NUMERO_ADULTOS." : <b>(>10)".ANO."</b>"; ?><br />
        <input type="text" name="adultos" size="5" maxlength="3" value="1" /><br/>
        <? echo NUMERO_NINOS." : <b>(<10)".ANO."</b>"; ?><br />
        <input type="text" name="ninos" size="5" maxlength="3" value="0" /><br/><br />
        <input type="submit" style="margin:-10px 0 0 140px;" value=" <? echo BUSCAR ?> " /><br /><br />
        </form>

		<?php if(!strstr($_SERVER['PHP_SELF'],"alojamiento.php")) rural_destacados();?>