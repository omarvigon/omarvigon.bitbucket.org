<?php $enlace=(strstr($_SERVER['PHP_SELF'],"hoteles.php"))? "hoteles.php":"casas.php"; ?>

<img title="Alojamientos rurales en Espa&ntilde;a" style="position:absolute;" src="http://www.sleepbcn.com/casas-rurales/img/mapa-provincias.gif" alt="Alojamientos rurales en Espa&ntilde;a" border="0" usemap="#Map" />
<map name="Map" id="Map">
	<area shape="poly" coords="345,58,396,65,389,142,342,107" href="<?=$enlace;?>?provincia=huesca" title="Huesca" alt="Huesca" />
    <area shape="poly" coords="307,161,349,145,382,160,385,144,336,109,328,77,312,115,300,121,291,152" href="<?=$enlace;?>?provincia=zaragoza" title="Zaragoza" alt="Zaragoza" />
    <area shape="poly" coords="337,218,377,163,352,150,313,168,309,200" href="<?=$enlace;?>?provincia=teruel" title="Teruel" alt="Teruel" />   
    <area shape="poly" coords="385,173,398,197,377,233,340,222,366,182" href="<?=$enlace;?>?provincia=castellon" title="Castellon" alt="Castellon" />
    <area shape="poly" coords="340,289,385,278,374,237,333,226,324,248" href="<?=$enlace;?>?provincia=valencia" title="Valencia" alt="Valencia" />
    <area shape="poly" coords="342,294,391,282,351,344" href="<?=$enlace;?>?provincia=alicante" title="Alicante" alt="Alicante" />
    <area shape="poly" coords="331,300,349,354,317,366,286,329" href="<?=$enlace;?>?provincia=murcia" title="Murcia" alt="Murcia" />
  	<area shape="poly" coords="393,141,396,186,444,154,431,130" href="<?=$enlace;?>?provincia=tarragona" title="Tarragona" alt="Tarragona" />
    <area shape="poly" coords="191,203,246,216,232,155" href="<?=$enlace;?>?provincia=madrid" title="Madrid" alt="Madrid" />
    <area shape="poly" coords="93,16,102,52,194,35,141,14" href="<?=$enlace;?>?provincia=asturias" title="Asturias" alt="Asturias" />
	<area shape="poly" coords="182,44,215,66,245,32,205,31" href="<?=$enlace;?>?provincia=cantabria" title="Cantabria" alt="Cantabria" />
    
    <area shape="poly" coords="282,75,307,106,339,56,307,37" href="<?=$enlace;?>?provincia=navarra" title="Navarra" alt="Navarra" />
    <area shape="poly" coords="250,73,285,86,302,105,298,113,256,103,248,95" href="<?=$enlace;?>?provincia=rioja" title="Rioja" alt="Rioja" />
    <area shape="poly" coords="88,203,63,242,95,266,165,255,146,206,112,196" href="<?=$enlace;?>?provincia=caceres" title="Caceres" alt="Caceres" />
    <area shape="poly" coords="71,259,107,273,176,255,162,292,115,333,68,309" href="<?=$enlace;?>?provincia=badajoz" title="Badajoz" alt="Badajoz" />
    <area shape="poly" coords="238,155,253,212,303,193,306,167,284,158" href="<?=$enlace;?>?provincia=guadalajara" title="Guadalajara" alt="Guadalajara" />
    <area shape="poly" coords="252,221,304,200,328,222,308,254,260,257" href="<?=$enlace;?>?provincia=cuenca" title="Cuenca" alt="Cuenca" />
    <area shape="poly" coords="264,264,319,259,335,291,279,327" href="<?=$enlace;?>?provincia=albacete" title="Albacete" alt="Albacete" />
    <area shape="poly" coords="160,214,174,251,252,249,240,220,193,206" href="<?=$enlace;?>?provincia=toledo" title="Toledo" alt="Toledo" />
    <area shape="poly" coords="186,261,173,291,195,314,259,304,253,255" href="<?=$enlace;?>?provincia=ciudadreal" title="Ciudad Real" alt="Ciudad Real" />
    
    <area shape="poly" coords="5,64,54,50,66,5,5,13" href="<?=$enlace;?>?provincia=coruna" title="Coru&ntilde;a" alt="Coru&ntilde;a" />
    <area shape="poly" coords="9,105,41,110,52,55,6,66" href="<?=$enlace;?>?provincia=pontevedra" title="Pontevedra" alt="Pontevedra" />
    <area shape="poly" coords="56,64,44,112,89,115,98,79" href="<?=$enlace;?>?provincia=ourense" title="Ourense" alt="Ourense" />
    <area shape="poly" coords="56,57,91,72,96,44,87,11,69,5" href="<?=$enlace;?>?provincia=lugo" title="Lugo" alt="Lugo" />
    <area shape="poly" coords="274,78,248,64,245,48,282,61" href="<?=$enlace;?>?provincia=alava" title="Alava" alt="Alava" />
    <area shape="poly" coords="274,30,267,49,242,43,252,29" href="<?=$enlace;?>?provincia=vizcaya" title="Vizcaya" alt="Vizcaya" />
    <area shape="poly" coords="281,30,270,51,285,57,307,31" href="<?=$enlace;?>?provincia=guipuzcoa" title="Guipuzcoa" alt="Guipuzcoa" />
    
    <area shape="poly" coords="426,269,521,191,555,201,553,279,450,304" href="<?=$enlace;?>?provincia=baleares" title="Baleares" alt="Baleares" />
    <area shape="poly" coords="318,422,318,498,388,497,433,421" href="<?=$enlace;?>?provincia=palmas" title="Las Palmas" alt="Las Palmas" />
    <area shape="poly" coords="423,494,456,416,552,416,540,493" href="<?=$enlace;?>?provincia=santacruz" title="Santa Cruz" alt="Santa Cruz" />
    <area shape="poly" coords="131,450,172,448,173,471,135,474" href="<?=$enlace;?>?provincia=ceuta" title="Ceuta" alt="Ceuta" />
    <area shape="poly" coords="244,461,277,460,279,486,241,485" href="<?=$enlace;?>?provincia=melilla" title="Melilla" alt="Melilla" />
    
    <area shape="poly" coords="71,324,61,377,103,399,111,339" href="<?=$enlace;?>?provincia=huelva" title="Huelva" alt="Huelva" />
    <area shape="poly" coords="116,341,112,394,153,395,179,377,144,332" href="<?=$enlace;?>?provincia=sevilla" title="Sevilla" alt="Sevilla" />
    <area shape="poly" coords="111,403,123,445,165,435,149,399" href="<?=$enlace;?>?provincia=cadiz" title="Cadiz" alt="Cadiz" />
    <area shape="poly" coords="156,399,162,423,214,405,193,377" href="<?=$enlace;?>?provincia=malaga" title="Malaga" alt="Malaga" />
    <area shape="poly" coords="143,319,187,375,206,367,194,319,166,296" href="<?=$enlace;?>?provincia=cordoba" title="Cordoba" alt="Cordoba" />
    <area shape="poly" coords="207,384,225,406,248,406,262,373,284,339,273,335,253,357,206,371" href="<?=$enlace;?>?provincia=granada" title="Granada" alt="Granada" />
    <area shape="poly" coords="286,347,257,407,293,404,316,368" href="<?=$enlace;?>?provincia=almeria" title="Almeria" alt="Almeria" />
    <area shape="poly" coords="219,362,253,349,273,329,268,308,200,318,206,348" href="<?=$enlace;?>?provincia=jaen" title="Jaen" alt="Jaen" />
    
    <area shape="poly" coords="88,195,136,197,158,159,89,144" href="<?=$enlace;?>?provincia=salamanca" title="Salamanca" alt="Salamanca" />
    <area shape="poly" coords="143,198,158,209,187,200,202,184,178,158" href="<?=$enlace;?>?provincia=avila" title="Avila" alt="Avila" />
    <area shape="poly" coords="185,152,203,179,238,147,228,133" href="<?=$enlace;?>?provincia=segovia" title="Segovia" alt="Segovia" />
    <area shape="poly" coords="236,132,286,156,296,119,253,110" href="<?=$enlace;?>?provincia=soria" title="Soria" alt="Soria" />
    <area shape="poly" coords="105,140,158,151,154,107,100,94" href="<?=$enlace;?>?provincia=zamora" title="Zamora" alt="Zamora" />
    <area shape="poly" coords="103,86,101,56,178,46,171,87,156,100" href="<?=$enlace;?>?provincia=leon" title="Leon" alt="Leon" />
    <area shape="poly" coords="160,108,162,155,178,156,209,135,205,121" href="<?=$enlace;?>?provincia=valladolid" title="Valladolid" alt="Valladolid" />
    <area shape="poly" coords="207,113,214,134,230,130,248,108,240,48,231,61,207,75" href="<?=$enlace;?>?provincia=burgos" title="Burgos" alt="Burgos" />
    <area shape="poly" coords="169,104,204,118,202,64,184,53" href="<?=$enlace;?>?provincia=palencia" title="Palencia" alt="Palencia" />
	<area shape="poly" coords="397,132,398,69,440,79,424,128" href="<?=$enlace;?>?provincia=lleida" alt="Lleida" title="Lleida" />
	<area shape="poly" coords="433,125,450,152,485,122,443,88" href="<?=$enlace;?>?provincia=barcelona" alt="Barcelona" title="Barcelona" />
	<area shape="poly" coords="450,85,507,65,522,97,489,123" href="<?=$enlace;?>?provincia=girona" alt="Girona" title="Girona" />
</map>