<?php  include("includes/1cabecera.php");

global $HTTP_POST_VARS;
$url_tpvv='https://sis.sermepa.es/sis/realizarPago';
$clave='clsur9e8wr98werijhes'; 
$code='91739821'; 
$name='Sleep BCN';
$terminal='1';
$order=date('ymdHis');
$currency='978';
$transactionType='0';
$urlMerchant='http://www.sleepbcn.com';
$producto='Alquiler de Alojamiento Rural';

function sha1_str2blks_SHA1($str)
{
   $strlen_str = strlen($str);
   
   $nblk = (($strlen_str + 8) >> 6) + 1;
   
   for ($i=0; $i < $nblk * 16; $i++) $blks[$i] = 0;
   
   for ($i=0; $i < $strlen_str; $i++)
   {
       $blks[$i >> 2] |= ord(substr($str, $i, 1)) << (24 - ($i % 4) * 8);
   }
   
   $blks[$i >> 2] |= 0x80 << (24 - ($i % 4) * 8);
   $blks[$nblk * 16 - 1] = $strlen_str * 8;
   
   return $blks;
}

function sha1_safe_add($x, $y)
{
   $lsw = ($x & 0xFFFF) + ($y & 0xFFFF);
   $msw = ($x >> 16) + ($y >> 16) + ($lsw >> 16);
   
   return ($msw << 16) | ($lsw & 0xFFFF);
}

function sha1_rol($num, $cnt)
{
   return ($num << $cnt) | sha1_zeroFill($num, 32 - $cnt);    
}

function sha1_zeroFill($a, $b)
{
   $bin = decbin($a);
   
   $strlen_bin = strlen($bin);
   
   $bin = $strlen_bin < $b ? 0 : substr($bin, 0, $strlen_bin - $b);
   
   for ($i=0; $i < $b; $i++) $bin = '0'.$bin;
   
   return bindec($bin);
}

function sha1_ft($t, $b, $c, $d)
{
   if ($t < 20) return ($b & $c) | ((~$b) & $d);
   if ($t < 40) return $b ^ $c ^ $d;
   if ($t < 60) return ($b & $c) | ($b & $d) | ($c & $d);
   
   return $b ^ $c ^ $d;
}

function sha1_kt($t)
{
   if ($t < 20) return 1518500249;
   if ($t < 40) return 1859775393;
   if ($t < 60) return -1894007588;
   
   return -899497514;
}

function sha_1($str, $raw_output=FALSE)
{
   if ( $raw_output === TRUE ) return pack('H*', sha_1($str, FALSE));
   
   $x = sha1_str2blks_SHA1($str);
   $a =  1732584193;
   $b = -271733879;
   $c = -1732584194;
   $d =  271733878;
   $e = -1009589776;
   
   $x_count = count($x);
   
   for ($i = 0; $i < $x_count; $i += 16)
   {
       $olda = $a;
       $oldb = $b;
       $oldc = $c;
       $oldd = $d;
       $olde = $e;
       
       for ($j = 0; $j < 80; $j++)
       {
           $w[$j] = ($j < 16) ? $x[$i + $j] : sha1_rol($w[$j - 3] ^ $w[$j - 8] ^ $w[$j - 14] ^ $w[$j - 16], 1);
           
           $t = sha1_safe_add(sha1_safe_add(sha1_rol($a, 5), sha1_ft($j, $b, $c, $d)), sha1_safe_add(sha1_safe_add($e, $w[$j]), sha1_kt($j)));
           $e = $d;
           $d = $c;
           $c = sha1_rol($b, 30);
           $b = $a;
           $a = $t;
       }
       
       $a = sha1_safe_add($a, $olda);
       $b = sha1_safe_add($b, $oldb);
       $c = sha1_safe_add($c, $oldc);
       $d = sha1_safe_add($d, $oldd);
       $e = sha1_safe_add($e, $olde);
   }
   
   return sprintf('%08x%08x%08x%08x%08x', $a, $b, $c, $d, $e);
}?>
         
    </td>
    <td class="der">
        <div class="contacto"> 
		<?php 
		$amount=round(($_POST["TotalPrepago"]*100),2);
		$fecha1=$_SESSION["fecha1ano"]."-".$_SESSION["fecha1mes"]."-".$_SESSION["fecha1dia"];
		$fecha2=$_SESSION["fecha2ano"]."-".$_SESSION["fecha2mes"]."-".$_SESSION["fecha2dia"];
		
		$cuerpo="<table cellspacing=1 cellpadding=1>";
		foreach($_POST as $nombre_campo => $valor)
   			$cuerpo=$cuerpo."<tr><td><b>".$nombre_campo ."</b> : </td><td>".$valor."</td></tr>";
		$cuerpo=$cuerpo."</table>";
		
		if($_GET["tipo"]=="paypal")
		{
			echo "<form name='_xclick' target='paypal' action='https://www.paypal.com/cgi-bin/webscr' method='post'>
			<input type='hidden' name='cmd' value='_cart'>
			<input type='hidden' name='business' value='info@sleepbcn.com'>
			<input type='hidden' name='currency_code' value='EUR'>
			<input type='hidden' name='item_name' value='".$_POST["NombrePiso"]." (".$fecha1.") (".$fecha2.")'>
			<input type='hidden' name='amount' value='".$_POST["TotalPrepago"]."'>
			<p style='margin:25px;'><input type='submit' border='0' name='submit' value='".RESERVAR_2."'></p>
			<input type='hidden' name='add' value='1'>
			</form>";	
		}
		else if($_GET["tipo"]=="tarjeta")
		{
			echo "<form name=compra action=$url_tpvv method=post>
			<input type=hidden name=Ds_Merchant_Amount value='$amount'>
			<input type=hidden name=Ds_Merchant_Currency value='$currency'>
			<input type=hidden name=Ds_Merchant_Order  value='$order'>
			<input type=hidden name=Ds_Merchant_ConsumerLanguage value='2'>
			<input type=hidden name=Ds_SecurePayment value='0'>
			<input type=hidden name=Ds_Merchant_MerchantCode value='$code'>
			<input type=hidden name=Ds_Merchant_Terminal value='$terminal'>
			<input type=hidden name=Ds_Merchant_TransactionType value='$transactionType'>
			<input type=hidden name=Ds_Merchant_MerchantURL value='$urlMerchant'>";
			
			$message = $amount.$order.$code.$currency.$transactionType.$urlMerchant.$clave;
			$signature = strtoupper(sha_1($message));
	
			echo "<input type=hidden name=Ds_Merchant_MerchantSignature value='$signature'>
			</form>";
		}		
		if(ejecutar("insert into rural_reservas (id_casa,correo,pedidohora,fecha1,fecha2,preciototal,personas,persona_nombre,persona_direccion,persona_ciudad,persona_pais,persona_telefono,persona_movil,persona_dni,persona_nacimiento,persona_transporte,persona_hora,persona_comentario) values (".$_POST["IdPiso"].",'".$_POST["EMail"]."','".$order."','".$fecha1."','".$fecha2."',".$_POST["TotalDias"].",'".$_POST["NumAdultos"]."-".$_POST["NumNinos"]."-".$_POST["NumBebe"]."','".$_POST["Nom"]."','".$_POST["Address"]."','".$_POST["City"]."','".$_POST["Country"]."','".$_POST["Tel"]."','".$_POST["Movil"]."','".$_POST["Dnipassport"]."','".$_POST["Birthday"]."','".$_POST["Transport"]."','".$_POST["Time"]."','".$_POST["Comment"]."')"))
		{
			if(mail("info@sleepbcn.com","PRE-Reserva Rural ".$_GET["tipo"],$cuerpo,"MIME-Version:1.0\r\nContent-type:text/html\nFrom:info@sleepbcn.com\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion()."\r\nBcc:omar@indianwebs.com"))					
			{
				if($_GET["tipo"]=="paypal")
					echo "<script type='text/javascript'>document._xclick.submit();</script>";
				else
					echo "<script type='text/javascript'>document.compra.submit();</script>";
			}
		}
		else
			echo "<p>ERROR</p>";

		?>
        </div>
        
<?php include("includes/2piecera.php"); ?>