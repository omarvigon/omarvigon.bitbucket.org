<?php include($_SERVER['DOCUMENT_ROOT']."/privado/funciones.php");		
include("privado/metas.php");
if($_SERVER['SERVER_NAME']=="rural.sleepbcn.com")
{
	Header("HTTP/1.1 301 Moved Permanently");
	Header("Location: http://rural.sleepbcn.com/casas-rurales/");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="verify-v1" content="Tq6yUhSpZOPYfjmU2sPxoJuytXwiDWA8SzrbSQbSZs0=" />
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)" />
    <title>Hotels barcelona | barcelona rent | lodging barcelona | apartments barcelona | barcelona flats | accommodation barcelona</title>
    <meta name="title" content="Hotels barcelona | barcelona rent | lodging barcelona | apartments barcelona | barcelona flats | accommodation barcelona">
    <meta name="description" content="Hotels barcelona | barcelona rent | lodging barcelona | apartments barcelona | barcelona flats | accommodation barcelona">
    <meta name="keywords" content="hotels barcelona rent lodging apartments flats accommodation">
    <META NAME="REVISIT-AFTER" CONTENT="7 Days">
    <META NAME="REVISIT" CONTENT="7 days">
    <META NAME="ROBOT" CONTENT="Index,Follow">
    <META NAME="ROBOTS" CONTENT="All">
    <META NAME="DISTRIBUTION" CONTENT="Global">
    <meta http-equiv="Pragma" content="no-cache">
    <META NAME="RATING" CONTENT="general">
    <meta name="language" content="">
    <META NAME="ABSTRACT" CONTENT="hotels barcelona">
    <META NAME="SUBJECT" CONTENT="barcelona rent">
    <META NAME="AUTHOR" CONTENT="lodging barcelona">
    <META NAME="COPYRIGHT" CONTENT="apartments barcelona">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="http://www.sleepbcn.com/privado/estilos.css" />
    <script language="javascript" type="text/javascript" src="http://www.sleepbcn.com/privado/funciones.js"></script>
</head>
<body onload="iniciar()">

<table cellpadding="0" cellspacing="0" align="center" class="tabla1">
<tr>
	<td colspan="2">
    <?php
	if($_SESSION["lang"]=="ing")
    	echo "<h1>SleepBcn | Apartments for Rent in Barcelona Rooms for rent in Barcelona</h1>";
	else
		echo "<h1>SleepBcn | Alquiler de Apartamentos en Barcelona Habitaciones en Barcelona</h1>";		
	?>
    </td>
</tr>
<tr>
	<td colspan="2" class="cabecera">
      <div id="idiomas"><img title="" src="http://www.sleepbcn.com/img/lateral.gif" alt="" border="0" id="imgidiomas" usemap="#mapeado1"/></div>
   	  <div>
      <?php 
	  if($_SESSION["lang"]=="ing") { ?>
            <a title="Rent Apartments" href="http://www.sleepbcn.com/index.php" class="boton1i">apartments</a>
            <a title="Rent Rooms" href="http://www.sleepbcn.com/barcelona-rooms.php" class="boton2i">rooms</a>
            <a title="Contact with SleepBCN" href="http://www.sleepbcn.com/contactar.php" class="boton3i">contact</a>
            <a title="Rent Conditions" href="http://www.sleepbcn.com/faq.php" class="boton4i">conditions</a>
            <a title="Barcelona Information" href="http://www.sleepbcn.com/info-barcelona.php" class="boton5i">barcelona info</a>
       <? } else { ?> 
       		<a title="Apartamentos Barcelona" href="http://www.sleepbcn.com/index.php" class="boton1">apartamentos</a>
            <a title="Habitaciones Barcelona" href="http://www.sleepbcn.com/barcelona-rooms.php" class="boton2">habitaciones</a>
            <a title="Contacta con SleepBCN" href="http://www.sleepbcn.com/contactar.php" class="boton3">contactar</a>
            <a title="Condiciones de alquiler" href="http://www.sleepbcn.com/faq.php" class="boton4">condiciones</a>
            <a title="Informacion de Barcelona" href="http://www.sleepbcn.com/info-barcelona.php" class="boton5">info barcelona</a>
       <? } ?>
       </div>
    </td>
</tr>
<tr>
	<td class="izq">
    	
        <?php
		if($_SERVER['PHP_SELF']=='/barcelona-apartments.php') 
			echo '<form action="http://www.sleepbcn.com/barcelona-apartments.php?id='.$_GET["id"].'" method="post" name="form_bus">';
		else
			echo '<form action="http://www.sleepbcn.com/barcelona-results.php" method="post" name="form_bus">';
		?>
        
        <? echo FECHA_LLEGADA ?>:<br/>
        <select name="dia1" onchange="fecha_ant(1)"><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
		<select name="mes1" onchange="fecha_ant(2)"><option value="01"><? echo MES1 ?></option><option value="02"><? echo MES2 ?></option><option value="03"><? echo MES3 ?></option><option value="04"><? echo MES4 ?></option><option value="05"><? echo MES5 ?></option><option value="06"><? echo MES6 ?></option><option value="07"><? echo MES7 ?></option><option value="08"><? echo MES8 ?></option><option value="09"><? echo MES9 ?></option><option value="10"><? echo MES10 ?></option><option value="11"><? echo MES11 ?></option><option value="12"><? echo MES12 ?></option></select>
		<select name="ano1" onchange="fecha_ant(3)"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option></select>
        <? echo FECHA_SALIDA ?>: <br/>
        <select name="dia2"><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
		<select name="mes2"><option value="01"><? echo MES1 ?></option><option value="02"><? echo MES2 ?></option><option value="03"><? echo MES3 ?></option><option value="04"><? echo MES4 ?></option><option value="05"><? echo MES5 ?></option><option value="06"><? echo MES6 ?></option><option value="07"><? echo MES7 ?></option><option value="08"><? echo MES8 ?></option><option value="09"><? echo MES9 ?></option><option value="10"><? echo MES10 ?></option><option value="11"><? echo MES11 ?></option><option value="12"><? echo MES12 ?></option></select>
		<select name="ano2"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option></select>
        <br /><br />
        <? echo ELECCION ?>
        <select name="tipo"><option value="apartamento"><? echo APARTAMENTO ?></option><option value="habitacion"><? echo HABITACION ?></option></select><br />
        <? echo NUMERO_ADULTOS." : <b>(>10) ".ANO."</b>"; ?>&nbsp;
        <input type="text" name="adultos" size="5" maxlength="3" value="1" /><br/>
        <? echo NUMERO_NINOS." : <b>(<10)".ANO."</b>"; ?>
        <input type="text" name="ninos" size="5" maxlength="3" value="0" /><br/>
        <br />
        <?php
		if($_SERVER['PHP_SELF']=='/barcelona-apartments.php') 
			echo '<input type="button" style="margin:0 0 -17px 0;font-weight:bold;color:#ff6532;" value=" GO BACK " onclick="window.close()"/>';?>
            
        <input type="submit" style="margin:0 0 0 100px;" value=" <? echo BUSCAR ?> " />
        </form>
        
		<?php if($_SERVER['PHP_SELF']!='/barcelona-apartments.php') { ?>
        <a href="http://www.sleepbcn.com/barcelona-offers.php"><img src="http://www.sleepbcn.com/img/cuadro_ofertas_especiales.gif" class="img_azul" alt="Apartment Barcelona Offers" title="Apartment Barcelona Offers"/></a>

		<? /******************bloque*******************/?>
        <img src="http://www.sleepbcn.com/img/top_azul.gif" alt="" class="bloque"/>
        <p class="azul"><b><? echo TARIFAS_ESPECIALES ?></b><br /><br />
        <a title="Special Rates" href="http://www.sleepbcn.com/special-rates.php"><? echo TARIFAS_ESPECIALES2 ?></a>
        </p>
        <img src="http://www.sleepbcn.com/img/top_azul2.gif" alt="" class="bloque" />
        
        <? /******************bloque*******************/?>
        <img src="http://www.sleepbcn.com/img/top_azul.gif" alt="" class="bloque"/>
   	    <p class="azul"><b>NEWS</b><br />&raquo; April 2009<br />
		<br />
			<?
        	if($_SESSION["lang"]=="ing") 	include("idiomas/news_ing.htm");
			else							include("idiomas/news_esp.htm");
			?>
        <br /><br />
        <a href="http://www.sleepbcn.com/news/saint-george-day.php">Saint George's Day (Barcelona)</a>
        <br /><br /><br />
        <a href="http://www.sleepbcn.com/news/">SEE ALL NEWS</a>
        </p>
        <img src="http://www.sleepbcn.com/img/top_azul2.gif" alt="" class="bloque" />
        
        <? /******************bloque*******************/?>
        <div style="margin:15px 0 0 10px;">
		<div id="TT_XcSkq2Z7AlU3o6IzcT5Z81"><h2><a href="http://www.tutiempo.net/Tiempo-Espana.html">El Tiempo</a></h2><a href="http://www.tutiempo.net/Tiempo-Barcelona-E08001.html">El tiempo en Barcelona</a></div>
		<script type="text/javascript" src="http://www.tutiempo.net/TTapi/XcSkq2Z7AlU3o6IzcT5Z81"></script>
        </div>
        <? } ?>