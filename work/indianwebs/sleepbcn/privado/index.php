<?php include($_SERVER['DOCUMENT_ROOT']."/privado/funciones.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="estilos_privado.css" />
    <?php if($_SESSION['autentificacion']>0) { ?>
	<script language="javascript" type="text/javascript">
    function confirmaborrar(cual)
    {
    	if(window.confirm("¿Esta seguro de eliminar este registro?"))
			window.open("index.php?modo=borrapiso&id="+cual,"_self");
    }
	function confirmaborrar2(cual)
    {
    	if(window.confirm("¿Esta seguro de eliminar este usuario?"))
			window.open("index.php?modo=borrausuario&id="+cual,"_self");
    }
	function reservaborrar(cual)
	{
		if(window.confirm("¿Esta seguro de ELIMINAR DEFINITIVAMENTE esta reserva?"))
			window.open("index.php?modo=borrareserva&id="+cual,"_self");
	}
	function confirmavalidar(cual)
	{
		if(window.confirm("Al VALIDAR esta reserva se MARCARAN los dias en el calendario y se enviara el MAIL de BOOKING CONFIRMATION\n\n¿Desea validarla?"))
			window.open("index.php?modo=validar_reserva&id="+cual,"_self");
	}
	function confirmavalidar_rural(cual)
	{
		if(window.confirm("Al VALIDAR esta reserva se MARCARAN los dias en el calendario y se enviara el MAIL de BOOKING CONFIRMATION\n\n¿Desea validarla?"))
			window.open("index.php?modo=validar_reserva_rural&id="+cual,"_self");
	}
    </script>
    <? } ?>
	<title>AREA PRIVADA : SLEEP BCN</title>
</head>
<body>

<table cellpadding="0" cellspacing="0">
<tr>
	<td class="izq2">
       <?php
	   if($_SESSION['autentificacion']>0)
	   {
	   		if($_SESSION['autentificacion']==2)
	   		{
				echo "
				<a class='bt_privado' href='index.php'>&middot; Inicio</a>
				<hr size='1' style='color:#efeaeb;' />
				<a class='bt_privado' href='index.php?modo=ver_reservas'>&middot; Reservas</a>
				<a class='bt_privado' href='index.php?modo=creareserva'>&middot; Crear Reserva</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado' href='index.php?modo=nuevo_piso'>&middot; Nuevo piso</a>
				<a class='bt_privado' href='index.php?modo=listar_pisos'>&middot; Listar pisos</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado' href='index.php?modo=nuevo_usuario'>&middot; Nuevo usuario</a>
				<a class='bt_privado' href='index.php?modo=listar_usuarios'>&middot; Listar usuarios</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado' href='index.php?modo=textoabre&cual=faq&idioma=esp'>&middot; Condiciones Esp</a>
				<a class='bt_privado' href='index.php?modo=textoabre&cual=faq&idioma=ing'>&middot; Condiciones Ing</a>
				<a class='bt_privado' href='index.php?modo=textoabre&cual=bcn&idioma=esp'>&middot; Info Esp</a>
				<a class='bt_privado' href='index.php?modo=textoabre&cual=bcn&idioma=ing'>&middot; Info Ing</a>
				<a class='bt_privado' href='index.php?modo=textoabre&cual=news&idioma=esp'>&middot; News Esp</a>
				<a class='bt_privado' href='index.php?modo=textoabre&cual=news&idioma=ing'>&middot; News Ing</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado' href='index.php?modo=documentos'>&middot; Documentos</a>
				<a class='bt_privado' href='index.php?modo=informes'>&middot; Informe Mensual</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado2' href='index.php?modo=nueva_casa'>&middot; Nueva Casa</a>
				<a class='bt_privado2' href='index.php?modo=listar_casas'>&middot; Listar Casas</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado2' href='index.php?modo=ver_reservas_rural'>&middot; Rural Reservas</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado2' href='index.php?modo=textoabre&cual=rural_eventos&idioma=esp'>&middot; Rural Eventos</a>
				<a class='bt_privado2' href='index.php?modo=textoabre&cual=rural_condiciones&idioma=esp'>&middot; Rural Condiciones</a>
				<hr size='1' style='color:#efeaeb;'/>
				<a class='bt_privado' href='index.php?tipo=salir'>&middot; Salir</a>";
			}
			else
			{
				echo "<a class='bt_privado' href='index.php'>&middot; Inicio</a>
				<a class='bt_privado' href='index.php?tipo=salir'>&middot; Salir</a>";
			}	
	   }
	   else
	   {	
	   		if($_GET["modo"]=="validar")
	   		{
	   	   		comprobacion();
				echo "<script type='text/javascript'>window.location='http://www.sleepbcn.com/privado/index.php';</script>";
	   		}
			else
			{
				echo "<form action='index.php?modo=validar' method='post' name='form1'>
       			Login : <br /><input type='text' name='nombre' size='18' /><br />
       			Password :<br /><input type='password' name='password' size='18' /><br />
       			<input type='submit' value='   ENTRAR   ' /></form>";
			}
	   }?>
    </td>
    <td class="der2">
    <?php
	if($_SESSION['autentificacion']==2)
	{
		if($_GET["modo"])	call_user_func($_GET["modo"]);
		if($_GET["tipo"])	call_user_func($_GET["tipo"]);
	}
	else if($_SESSION['autentificacion'])
	{
		//el propietario solo puede ver las funciones con parametro tipo=,las modo= no
		if($_GET["tipo"])
			call_user_func($_GET["tipo"]);
		else
			mis_pisos();
	}
	?>
    </td>
</tr>
<tr>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</table>

</body>
</html>