function iniciar()
{
	var fecha=new Date();
	var hoy_dia=fecha.getDate();
	var hoy_mes=fecha.getMonth()+1;
	var hoy_ano=fecha.getYear();
	if (hoy_dia<10)			hoy_dia="0"+hoy_dia;
	if (hoy_mes<10)			hoy_mes="0"+hoy_mes;
	
	document.form_bus.dia1.value=hoy_dia;
	document.form_bus.mes1.value=hoy_mes;
	document.form_bus.ano1.value=hoy_ano;
	document.form_bus.dia2.value=hoy_dia;
	document.form_bus.mes2.value=hoy_mes;
	document.form_bus.ano2.value=hoy_ano;
}
function fecha_ant(numero)
{
	if(numero==1)
		document.form_bus.dia2.value=document.form_bus.dia1.value;
	if(numero==2)	
		document.form_bus.mes2.value=document.form_bus.mes1.value;
	if(numero==3)
		document.form_bus.ano2.value=document.form_bus.ano1.value;
}
function validar(cual)
{
	var x=0;
	if(document.form_reserva2.Nom.value=="")			x++;
	if(document.form_reserva2.Address.value=="")		x++;
	if(document.form_reserva2.City.value=="")			x++;
	if(document.form_reserva2.Country.value=="")		x++;
	if(document.form_reserva2.Tel.value=="")			x++;
	if(document.form_reserva2.Movil.value=="")			x++;
	if(document.form_reserva2.Dnipassport.value=="")	x++;
	if(document.form_reserva2.EMail.value=="")			x++;
	
	if(x==0)
	{
		if(cual==1)			document.form_reserva2.action="pre-pasarela.php?tipo=tarjeta";
		if(cual==2)			document.form_reserva2.action="pre-pasarela.php?tipo=paypal";
		document.form_reserva2.submit();
	}
	else	
		alert("The fields marked * are required\nLos campos marcados con * son obligatorios");
}
function mapa_popup(id)
{
	window.open("mapa.php?id="+id,"_blank","width=640,height=480,scrollbars=no,resizable=no,directories=no,location=no,menubar=no,status=no");	
}
function dias(cualdia,cualmes,cualano)
{
	if(cualmes>0 && cualmes<10)		
		cualmes="0"+cualmes;
	if(cualdia>0 && cualdia<10)		
		cualdia="0"+cualdia;
			
	if( (window.top.document.form1.inicio1.value=="" && window.top.document.form1.final1.value=="") || (window.top.document.form1.inicio1.value && window.top.document.form1.final1.value))
	{
		window.top.document.form1.inicio1.value=cualdia;
		window.top.document.form1.inicio2.value=cualmes;
		window.top.document.form1.inicio3.value=cualano;
		window.top.document.form1.final1.value="";
		window.top.document.form1.final2.value="";
		window.top.document.form1.final3.value="";
	}
	else
	{
		window.top.document.form1.final1.value=cualdia;
		window.top.document.form1.final2.value=cualmes;
		window.top.document.form1.final3.value=cualano;
	}
}