<?php include($_SERVER['DOCUMENT_ROOT']."/privado/funciones.php");

function UltimoDia($a,$m){
  
  //calcular dias de febrero si es bisiesto cada 4 a�os	
  if (((fmod($a,4)==0) and (fmod($a,100)!=0)) or (fmod($a,400)==0)) 
  	{$dias_febrero = 29;}
  else 
  	{$dias_febrero = 28; }
	
  switch($m) {
    case  1: $valor = 31; break;
    case  2: $valor = $dias_febrero; break;
    case  3: $valor = 31; break;
    case  4: $valor = 30; break;
    case  5: $valor = 31; break;
    case  6: $valor = 30; break;
    case  7: $valor = 31; break;
    case  8: $valor = 31; break;
    case  9: $valor = 30; break;
    case 10: $valor = 31; break;
    case 11: $valor = 30; break;
    case 12: $valor = 31; break;
  }
  return $valor;
}

function nombre_mes($m){
  switch($m) {
    
	case  1: $valor = MES1;		break;
    case  2: $valor = MES2;		break;
    case  3: $valor = MES3;		break;
    case  4: $valor = MES4;		break;
    case  5: $valor = MES5;		break;
    case  6: $valor = MES6;		break;
    case  7: $valor = MES7;		break;
    case  8: $valor = MES8;		break;
    case  9: $valor = MES9; 	break;
    case 10: $valor = MES10;	break;
    case 11: $valor = MES11;	break;
    case 12: $valor = MES12;	break;
  }
  return $valor;
}
function numero_dia_semana($d,$m,$a){ 
  $f = getdate(mktime(0,0,0,$m,$d,$a)); 
  $d = $f["wday"];
  if ($d==0) {$d=7;}
  return $d;
} 

$hoy = getdate();
$anhohoy = $hoy["year"];
$meshoy  = $hoy["mon"];
$diahoy  = $hoy["mday"];

$anho = $_REQUEST["anho"];
$mes  = $_REQUEST["mes"];
$dia  = 1;
if (($anho==0)||($mes==0))
{
  $anho=$anhohoy;
  $mes =$meshoy;
}
$dias_mes = UltimoDia($anho,$mes);
$NombreMes = nombre_mes($mes);
$NumeroSemanas = 6;
if ($mes==1) {
  $anhoant = $anho-1;
  $mesant = 12;
  $anhosig = $anho;
  $messig = $mes+1;
} else if ($mes==12) {
  $anhoant = $anho;
  $mesant = $mes-1;
  $anhosig = $anho+1;
  $messig = 1;
} else {
  $anhoant = $anho;
  $mesant = $mes-1;
  $anhosig = $anho;
  $messig = $mes+1;
}
$anhoanterior  = $anho-1;
$anhosiguiente = $anho+1;

?>
<html>
<head>
	<style>
	body		{margin:0;background:#efeaeb;}
	.tabla1		{background:#efeaeb;border:0;margin:0;width:550px;}
	.tabla2		{background:#efeaeb;border:0;margin:0 5px 0 0;float:left;}
	td			{font:11px Arial,Helevetica,san-serif;}
	a			{color:#44485a;text-decoration:none;}
	.tazul		{text-align:center;width:20px;background:#44485a;color:#ffffff;}
	.tgris		{text-align:center;background:#cecece;color:#000000;}
	.tcelda		{text-align:right;background:#ffffff;color:#44485a;font-weight:bold;}
	
	.ocupado	{text-align:right;background:#ff0000;color:#ffffff;font-weight:bold;}
	.ocupado_x1 {text-align:right;background:#ff0000;color:#ffffff;font-weight:bold;}
	.oferta20	{text-align:right;background:#ffff00;color:#000000;font-weight:bold;}
	.oferta15 	{text-align:right;background:#fefe88;color:#000000;font-weight:bold;}
	.oferta25 	{text-align:right;background:#e9e900;color:#000000;font-weight:bold;}
	
	.tcelda_r1  {text-align:right;background:url(../img/leyenda_1a.gif);color:#ffffff;font-weight:bold;}
	</style>
	<script type="text/javascript" src="http://www.sleepbcn.com/privado/funciones.js"></script>
</head>
<body>

<?php
$aux_mes=$mes;
$aux_ano=$anho;

echo "<table cellspacing='0' cellpadding='0' class='tabla1'><tr><td><a href=./calendario.php?id=".$_GET["id"]."&anho=".$anhoant."&mes=".$mesant."><<</a></td><td>";

for ($i=1;$i<=6;$i++)
{
	$dia=1;				//vuelvo a empezar el mes
	echo "<table class='tabla2'>";
	echo "<tr><td colspan='7' class='tgris'>".$NombreMes." ".$aux_ano."</td></tr><tr>";
	echo "<td class='tazul'>L</td>";
	echo "<td class='tazul'>M</td>";
	echo "<td class='tazul'>M</td>";
	echo "<td class='tazul'>J</td>";
	echo "<td class='tazul'>V</td>";
	echo "<td class='tazul'>S</td>";
	echo "<td class='tazul'>D</td>";
	echo "</tr>";

	for ($semana=1;$semana<=$NumeroSemanas;$semana++)
	{
  		echo "<tr>";
  		for ($diasem=1;$diasem<=7;$diasem++)
		{ 
    		$dow = numero_dia_semana($dia,$aux_mes,$aux_ano);
			if (($dow==$diasem) && ($dia<=$dias_mes)) 
			{
	  			$valor = $dia;
	  			$dia++;
			} 
			else 
	  			$valor = "&nbsp;";
			
			//generar variable con formato de dia para comparar con mysql	
			$xdia=$valor;
			$xmes=$aux_mes;
				
			if($xdia>0 && $xdia<10)	$xdia="0".$xdia;
			if($xmes>0 && $xmes<10)	$xmes="0".$xmes;
				
			//devuelve una celda con color
			echo colorear($_GET["id"],$aux_ano."-".$xmes."-".$xdia);
				
			if($_SESSION['autentificacion']>0)
				echo "<a href=javascript:dias('".$valor."','".$aux_mes."','".$aux_ano."')>$valor</a></td>";
			else	
				echo $valor."</td>";
  		} 
  		echo "</tr>";
	}
	echo "</table>";
	
	$aux_mes=$aux_mes+1;
	
	if(($mes==8 && $i==5) || ($mes==9 && $i==4) || ($mes==10 && $i==3) || ($mes==11 && $i==2) || ($mes==12 && $i==1)) 
	{
		$aux_ano=$aux_ano+1;
		$aux_mes=1;
	}
	
	$dias_mes = UltimoDia($aux_ano,$aux_mes);
	$NombreMes = nombre_mes($aux_mes);
}

echo "</td><td><a href=./calendario.php?id=".$_GET["id"]."&anho=".$anhosig."&mes=".$messig.">>></a></td></tr></table>";
?>

</body>
</html>