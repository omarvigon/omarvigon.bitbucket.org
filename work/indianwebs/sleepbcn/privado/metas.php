<?php
define("TITULO-INDEX-ING","Apartments and rooms for rent in Barcelona Sleep BCN");
define("TITULO-INDEX-ESP","Apartamentos y habitaciones de alquiler en Barcelona Sleep BCN");
define("KEYWORDS-INDEX-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-INDEX-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-INDEX-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-INDEX-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-CONTACTAR-ING","Contact with Sleep BCN");
define("TITULO-CONTACTAR-ESP","Contacta con Sleep BCN");
define("KEYWORDS-CONTACTAR-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-CONTACTAR-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-CONTACTAR-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-CONTACTAR-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-INFO-BARCELONA-ING","Info Barcelona Turism Sleep BCN");
define("TITULO-INFO-BARCELONA-ESP","Barcelona Informaci&oacute;n de Turismo Sleep BCN");
define("KEYWORDS-INFO-BARCELONA-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-INFO-BARCELONA-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-INFO-BARCELONA-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-INFO-BARCELONA-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-FAQ-ING","Conditions for rent in Sleep BCN");
define("TITULO-FAQ-ESP","Condiciones de alquiler en Sleep BCN");
define("KEYWORDS-FAQ-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-FAQ-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-FAQ-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-FAQ-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-BARCELONA-ROOMS-ING","Rooms for rent in Barcelona Sleep BCN");
define("TITULO-BARCELONA-ROOMS-ESP","Habitaciones de alquiler en Barcelona Sleep BCN");
define("KEYWORDS-BARCELONA-ROOMS-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-BARCELONA-ROOMS-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-BARCELONA-ROOMS-ING","lo que sea");
define("DESCRIPTION-BARCELONA-ROOMS-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-BARCELONA-APARTMENTS-ING","Apartments for rent in Barcelona Sleep BCN");
define("TITULO-BARCELONA-APARTMENTS-ESP","Apartamentos de alquiler en Barcelona Sleep BCN");
define("KEYWORDS-BARCELONA-APARTMENTS-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-BARCELONA-APARTMENTS-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-BARCELONA-APARTMENTS-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-BARCELONA-APARTMENTS-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-BARCELONA-RESULTS-ING","Search apartments and rooms for rent in Barcelona Sleep BCN");
define("TITULO-BARCELONA-RESULTS-ESP","Busqueda apartamentos y habitaciones de alquiler en Barcelona Sleep BCN");
define("KEYWORDS-BARCELONA-RESULTS-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-BARCELONA-RESULTS-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-BARCELONA-RESULTS-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-BARCELONA-RESULTS-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("TITULO-BARCELONA-OFFERS-ING","Offers in apartments and rooms for rent in Barcelona Sleep BCN");
define("TITULO-BARCELONA-OFFERS-ESP","Ofertas en apartmentos y habitaciones de alquiler en Barcelona Sleep BCN");
define("KEYWORDS-BARCELONA-OFFERS-ING","Apartments for rent in barcelona, Rooms for rent in Barcelona, accommodation in Barcelona, Barcelona apartments, Barcelona Rooms, Barcelona Flat for rent, shared apartments Barcelona");
define("KEYWORDS-BARCELONA-OFFERS-ESP","alquiler apartamentos  barcelona, alquiler habitaciones barcelona, alojamientos barcelona, pisos compartidos en barcelona, apartamentos en barcelona, habitaciones en barcelona");
define("DESCRIPTION-BARCELONA-OFFERS-ING","Barcelona Apartments and Rooms for rent with Sleep BCN, apartments and rooms in the hear of Barcelona, at cheap prices Sleepbcn.com : Apartments and rooms in Barcelona");
define("DESCRIPTION-BARCELONA-OFFERS-ESP","Alquiler apartamentos y habitaciones en Barcelona con Sleepbcn, apartamentos y habitaciones en pleno centro de Barcelona, a precios baratos. Sleepbcn.com : Apartamentos y habitaciones en Barcelona");

define("INFERIOR_ING","<p><a href='index.php' title='Barcelona apartments'>Barcelona apartments</a> - <a href='barcelona-rooms.php' title='Barcelona Rooms'>Barcelona rooms</a></p>");
define("INFERIOR_ESP","<p><a href='index.php' title='Apartamentos Barcelona'>Apartamentos Barcelona</a> - <a href='barcelona-rooms.php' title='Habitaciones Barcelona'>Habitaciones Barcelona</a></p>");
?>