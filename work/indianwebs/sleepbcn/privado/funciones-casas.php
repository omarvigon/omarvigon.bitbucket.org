<?php
/************************************publicas**************************/

function casas_listar($quetipo,$pagina)
{
	if(!$pagina)	
		$pagina=0;
	
	$listado=ejecutar("select id,img1,tipo,nombre,metros,capacidad1,capacidad2,provincia,num_hab,desc_".$_SESSION["lang"].",precio_dia from rural_casas where visible='1' and provincia='".$_GET["provincia"]."' and tipo='".$quetipo."' order by prioridad asc limit ".(POR_PAGINA*$pagina).",".POR_PAGINA." ");
	
	$_SESSION["num_resultados"]=mysql_result(ejecutar("select count(id) from rural_casas where visible=1 and tipo='".$quetipo."'"),0); 
	
	mostrar_casas($listado);
}
function casas_busqueda()
{
	//recibe datos del formulario de busqueda y prepara el query
	
	$fecha1=$_POST['ano1']."-".$_POST['mes1']."-".$_POST['dia1'];
	$fecha2=$_POST['ano2']."-".$_POST['mes2']."-".$_POST['dia2'];
	
	$date1 = mktime(0,0,0,$_POST['mes1'],$_POST['dia1'],$_POST['ano1']);
	$date2 = mktime(0,0,0,$_POST['mes2'],$_POST['dia2'],$_POST['ano2']);
	$numerodias=round(($date2 - $date1) / (60 * 60 * 24));
	
	if($_POST["provincia"]!=0)
		$clausula="and rural_casas.provincia='".$_POST["provincia"]."'";
	
	//busca los pisos sin dias marcados o con dias en oferta (diferentes a ocupado)
	$listado=ejecutar("select id,img1,tipo,nombre,metros,capacidad1,capacidad2,provincia,desc_".$_SESSION["lang"].",precio_dia,estancia,prioridad from rural_casas where rural_casas.visible='1' ".$clausula." and rural_casas.tipo='".$_POST["tipo"]."' and rural_casas.capacidad2 >='".($_POST["adultos"]+$_POST["ninos"])."' and rural_casas.estancia <= ".$numerodias." and rural_casas.id not in (select distinct id_casa from rural_calendario where dia between '".$fecha1."' and '".$fecha2."' and rural_calendario.estado like 'ocupa%' ) order by prioridad asc");
	mostrar_casas($listado);
}
function mostrar_casas($listado)
{
	while($fila = mysql_fetch_assoc($listado))
	{		
		echo "<div class='casas1'><br>
		<a title='Alojamiento rural en ".$fila["provincia"]."' href='alojamiento.php?id=".$fila["id"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img1"]."' alt='Alojamiento rural en ".$fila["provincia"]."' /></a>
        <a title='Alojamiento rural en ".$fila["provincia"]."' href='alojamiento.php?id=".$fila["id"]."' target='_blank'><h3>".$fila["nombre"]." <span style='color:#000000'>&nbsp;&nbsp;".DESDE." ".(round($fila["precio_dia"]/$fila["capacidad1"],2))." &euro;</span></h3>
		<p>&nbsp;</p></a>
		<p><b>".strtoupper($fila["tipo"])." | ".strtoupper($fila["provincia"])." | ".$fila["metros"]."m2 | ".$fila["num_hab"]." room | ".$fila["capacidad1"]."-".$fila["capacidad2"]." ".PERSONAS."</b></p>
		<p>&nbsp;</p>
        <p style='text-align:justify;'>".substr($fila["desc_".$_SESSION["lang"]],0,250)."... <a title='Barcelona ".$texto_tipo." ".$fila["nombre"]."' href='alojamiento.php?id=".$fila["id"]."' target='_blank'>[+info...]</a></p>
		</div>";
	}
}
function rural_destacados()
{
	$listado=ejecutar("select id,nombre,provincia,img1 from rural_casas where prioridad=0");
	while($fila = mysql_fetch_assoc($listado))
	{		
		echo "<p class='bloques'><a title='Alojamiento rural en ".$fila["provincia"]."' href='alojamiento.php?id=".$fila["id"]."'><img src='http://www.sleepbcn.com/casas/".$fila["img1"]."' class='pic' alt='Alojamiento rural en ".$fila["provincia"]."' /></a>
        <br />".$fila["nombre"]." <span>(".$fila["provincia"].")</span><br /></p>";
	}
}
/*************************************************************************/

function listar_casas()
{
	$listado=ejecutar("select rural_casas.id as idcasa,rural_casas.id_usuario,rural_casas.nombre as casanombre,rural_casas.provincia,rural_casas.visible,rural_casas.tipo,rural_casas.prioridad,usuarios.id,usuarios.nombre as usuarionombre from rural_casas,usuarios where rural_casas.id_usuario=usuarios.id order by rural_casas.id desc");
	$texto="<table cellpadding='1' cellspacing='1' class='listadopisos'><tr>
	<th>&nbsp;</th><th>Nombre</th><th>Ubicacion</th><th>Tipo</th><th>Propt.</th><th>Vis.</th><th>Prio.</th></tr>";
		
	while($fila = mysql_fetch_assoc($listado))
	{
		$texto=$texto."<tr'>";
		$texto=$texto."<td nowrap='nowrap'><a href='index.php?modo=modifcasa&id=".$fila["idcasa"]."'><img src='../img/icon_edit.png' border='0' title='Editar casa'></a>
						   <a href='index.php?tipo=calendario_casa&id=".$fila["idcasa"]."&nombre=".$fila["casanombre"]."'><img src='../img/icon_calendario.png' border='0' alt='Ver calendario'></a> 
						   <a href='index.php?modo=modifcar_casa&id=".$fila["idcasa"]."&nombre=".$fila["casanombre"]."'><img src='../img/icon_car.png' border='0' alt='Caracteristicas'></a></td>";
		$texto=$texto."<td>".$fila["casanombre"]."</td>";
		$texto=$texto."<td>".$fila["provincia"]."</td>";
		$texto=$texto."<td>".$fila["tipo"]."</td>";
		$texto=$texto."<td>".$fila["usuarionombre"]."</td>";
		$texto=$texto."<td>".constant("VISIBLE_".$fila["visible"])."</td>";		
		$texto=$texto."<td>".$fila["prioridad"]."</td>";
		$texto=$texto."</tr>";
	}
	echo $texto."</table>";
}
function calendario_casa()
{
	//muestra el calendario privado y el formulario para cambiar el estado de los dias
	
	echo "<h4>Calendario de casa ".$_GET["nombre"]."</h4>";
	echo "<iframe src='http://www.sleepbcn.com/privado/calendario-rural.php?id=".$_GET["id"]."' frameborder='0' scrolling='no'></iframe>
	<p style='margin-top:-60px;'><img src='http://www.sleepbcn.com/img/leyenda_privado.gif' hspace='20'></p>";
	echo "<form method='post' action='index.php?tipo=ejecuta_calendario_casa&nombre=".$_GET["nombre"]."' name='form1'>
	<table cellspacing='0' cellpadding='2' class='nuevo_formulario'><tr>
	<td>
		Fecha inicial del intervalo <input type='text' name='inicio1' size='4' readonly='readonly'>-<input type='text' name='inicio2' size='4' readonly='readonly'>-<input type='text' name='inicio3' size='4' readonly='readonly'><br>
		Fecha final del intervalo &nbsp;&nbsp;<input type='text' name='final1' size='4' readonly='readonly'>-<input type='text' name='final2' size='4' readonly='readonly'>-<input type='text' name='final3' size='4' readonly='readonly'>
	</td>
	<td>
		<input type='radio' name='estado' value='liberar'> Liberar<br>
    	<input type='radio' name='estado' value='ocupado' checked='checked'> Ocupar<br>
    	<input type='radio' name='estado' value='oferta15'> Oferta 15%<br>
    	<input type='radio' name='estado' value='oferta20'> Oferta 20%<br>
    	<input type='radio' name='estado' value='oferta25'> Oferta 25%<br>
	</td>
	</tr></table>
	<p>
	<input type='hidden' name='idenpiso' value='".$_GET["id"]."'>
	<input type='reset' value='      Borrar      '>
    <input type='submit' value=' Confirmar '></p></form>";
}

function ejecuta_calendario_casa()
{
	//calcula dias intermedios, datstart dia de inicio

	$date1 = mktime(0,0,0,$_POST["inicio2"], $_POST["inicio1"], $_POST["inicio3"]);
	$date2 = mktime(0,0,0,$_POST["final2"], $_POST["final1"], $_POST["final3"]);
	
	$numerodias=round(($date2 - $date1) / (60 * 60 * 24));
	
	$comprobar=ejecutar("select * from rural_calendario where dia between '".$_POST["inicio3"]."-".$_POST["inicio2"]."-".$_POST["inicio1"]."' and '".$_POST["final3"]."-".$_POST["final2"]."-".$_POST["final1"]."' and id_casa=".$_POST["idenpiso"]." ");

	while($fila = mysql_fetch_assoc($comprobar))
	{
		if(($fila["autor"]==2 || $fila["autor"]==3) && ($_SESSION['autentificacion']!=2)) //si un propietario intenta cambiar lo de un admin o cliente
			$numerodias=-5;
		if(($fila["estado"]=="ocupado" || $fila["estado"]=="ocupado_x1") && ($_POST["estado"]=="oferta15" || $_POST["estado"]=="oferta20" || $_POST["estado"]=="oferta25"))	//si se intenta cambiar de rojo a
			$numerodias=-5;			
	}	
	if($numerodias<0)
	{
		echo "<script>alert('No se pueden cambiar esas fechas');</script>";
	}
	else
	{
		$datstart = $_POST["inicio3"]."-".$_POST["inicio2"]."-".$_POST["inicio1"]; 
		$rep = $numerodias;
		$dia0=date("Y-m-d", mktime(0,0,0,$_POST["inicio2"],$_POST["inicio1"],$_POST["inicio3"]));
	
		//primero hace un delete de los dias y luego si es ocupar o liberar un insert
		ejecutar("delete from rural_calendario where id_casa='".$_POST["idenpiso"]."' and dia='".$dia0."' ");

		if($_POST["estado"]!="liberar")
		{
			if($_POST["estado"]=="ocupado")
				ejecutar("insert into rural_calendario (id_casa,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dia0."','".$_SESSION['autentificacion']."','ocupado_x1')");
			else
				ejecutar("insert into rural_calendario (id_casa,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dia0."','".$_SESSION['autentificacion']."','".$_POST["estado"]."')");	
		}
		//ejecuta el resto de consultas
	
		//si es ocupar el ultimo dia lo deja libre
		$hasta=1;
		if($_POST["estado"]=="ocupado")
			$hasta=2;
		
		while ($rep >= $hasta) 
		{
			$datyy=substr($datstart,0,4);
			$datmm=substr($datstart,5,2);
			$datdd=substr($datstart,8,2);
			$fda=$datdd + 1;
			$fmo=$datmm + 0;
			$fyr=$datyy + 0;
			$dat1=date("Y-m-d", mktime(0,0,0,$fmo,$fda,$fyr)); 

			ejecutar("delete from rural_calendario where id_casa='".$_POST["idenpiso"]."' and dia='".$dat1."' ");

			if($_POST["estado"]!="liberar")
				ejecutar("insert into rural_calendario (id_casa,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dat1."','".$_SESSION['autentificacion']."','".$_POST["estado"]."')");
		
			$datstart=$dat1; 
			$rep--; 
		}
		//ultimo query poner while rep >=2
		//$dat1=date("Y-m-d", mktime(0,0,0,$fmo,($fda+1),$fyr)); 
		//echo "ultimo <br> insert into calendario (id_piso,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dat1."','".$_SESSION['autentificacion']."','".$_POST["estado"]."') <br>";
	}	
	echo "<script>window.open('index.php?tipo=calendario_casa&id=".$_POST["idenpiso"]."&nombre=".$_GET["nombre"]."','_self')</script>";
}
function colorear_rural($id,$diactual)
{
	//devuelve una celda con color si el diactual esta en la base de datos
	
	$dias=ejecutar("select dia,estado,autor from rural_calendario where id_casa='".$id."' and dia='".$diactual."' ");
	$filadia=mysql_fetch_row($dias);
	
	$autor="";
	if($_SESSION['autentificacion']>0)
	{
		if($filadia[2]=="2")	$autor="*";
		if($filadia[2]=="3")	$autor="-";
	}
	if ($filadia[0]==$diactual)
	{
		if(($filadia[1]=="ocupado_x1")&&($_SESSION['autentificacion']>0))
			return "<td class='tcelda_r1'><span style='color:#000000;'>".$autor."</span>";
		else
			return "<td class='".$filadia[1]."'><span style='color:#000000;'>".$autor."</span>";
	}
	return "<td class='tcelda'>";
}
function nueva_casa()
{
	$usuarios=ejecutar("select id,nombre from usuarios where tipo='rural' order by id desc");	
	
	$texto="<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_nuevopiso' autocomplete='off' method='post' action='index.php?modo=insertar_casa' enctype='multipart/form-data'>
	<tr><td align='right'>Nombre </td><td><input type='text' name='nombre' size='44'></td></tr>
	<tr><td align='right'>Tipo </td><td><select name='tipo'><option value='rural'>Casa Rural</option><option value='hotel'>Hotel</option></select></td></tr>
	<tr><td align='right'>Propietario </td><td><select name='id_usuario'>";
	
	while($fila = mysql_fetch_assoc($usuarios))
	{
		$texto=$texto."<option value='".$fila["id"]."'>".$fila["nombre"]."</option>";
	}
	
	$texto=$texto."</select></td></tr>
	<tr><td align='right'>Direccion </td><td><input type='text' name='direccion' size='44'></td></tr>
	<tr><td align='right'>Poblacion </td><td><input type='text' name='poblacion' size='44'></td></tr>
	<tr><td align='right'>Provincia</td><td><select name='provincia'>";
	
	echo $texto;
	include($_SERVER['DOCUMENT_ROOT']."/casas-rurales/includes/inc.provincias.php");
	
	echo "</select></td></tr>
	<tr><td align='right'>Metros2 </td><td><input type='text' name='metros' size='5' maxlength='3'></td></tr>
	<tr><td align='right'>Capacidad </td><td><input type='text' name='capacidad1' size='2' maxlength='2'>-<input type='text' name='capacidad2' size='2' maxlength='2'> personas</td></tr>	
  	<tr><td align='right'>Estancia Minima </td><td><input type='text' name='estancia' size='4' maxlength='2'> noches</td></tr>
	<tr><td align='right'>Retraso </td><td><input type='text' name='retraso' size='4' maxlength='2'> euros</td></tr>
	<tr><td align='right'>Prioridad </td><td><input type='text' name='prioridad' size='4' maxlength='2'> (0 al 5)</td></tr>
	<tr><td align='right'>Visible </td><td><select name='visible'><option value='1'>Si</option><option value='0'>No</option></select></td></tr>
	<tr><td align='right'>".GESTION."</td><td><input type='text' name='precio_limpieza' size='5' maxlength='3'> euros</td></tr>	
	<tr><td align='right'>Precio por Dia </td><td><input type='text' name='precio_dia' size='4' maxlength='4'> euros - Fin de semana <input type='text' name='precio_dia2' size='4' maxlength='4'> euros</td></tr>
	<tr><td align='right'>Incremento </td><td><input type='text' name='incremento' size='4' maxlength='3'> euros/dia</td></tr>
	<tr><td align='right'>Fianza </td><td><input type='text' name='fianza' size='4' maxlength='4'> euros</td></tr>
	
	<tr><td align='right'>Numero Habitaciones/Ba&ntilde;os/Aseos </td><td><input type='text' name='num_hab' size='4' maxlength='2'>-<input type='text' name='num_bano' size='4' maxlength='2'>-<input type='text' name='num_aseo' size='4' maxlength='2'></td></tr>
	<tr><td align='right'>Numero Camas Indivi/Matrim/Sofa </td><td><input type='text' name='num_cama1' size='4' maxlength='2'>-<input type='text' name='num_cama2' size='4' maxlength='2'>-<input type='text' name='num_camasofa' size='4' maxlength='2'> </td></tr>
	<tr><td align='right'>Persona de contacto </td><td><input type='text' name='contacto_per' size='44'></td></tr>
	<tr><td align='right'>Telefono de contacto </td><td><input type='text' name='contacto_tel' size='44'></td></tr>
	<tr><td align='right'>Correo Electronico de contacto </td><td><input type='text' name='contacto_mail' size='44'></td></tr>
	<tr><td align='right'>Mapa </td><td><textarea name='mapa' cols='65' rows='5'></textarea></td></tr>	
	
	<tr><td align='right'>Descripcion(Esp)</td><td><textarea name='desc_esp' cols='64' rows='5'></textarea></td></tr>
	<tr><td align='right'>Descripcion(Ing)</td><td><textarea name='desc_ing' cols='64' rows='5'></textarea></td></tr>
	<tr><td align='right'>Localizacion(Esp)</td><td><textarea name='loc_esp' cols='65' rows='5'></textarea></td></tr>
	<tr><td align='right'>Localizacion(Ing)</td><td><textarea name='loc_ing' cols='65' rows='5'></textarea></td></tr>
	<tr><td align='right'>Imagen1 :</td><td><input type='file' name='imagen1' size='70'></td></tr>
	<tr><td align='right'>Imagen2 :</td><td><input type='file' name='imagen2' size='70'></td></tr>
	<tr><td align='right'>Imagen3 :</td><td><input type='file' name='imagen3' size='70'></td></tr>
	<tr><td align='right'>Imagen4 :</td><td><input type='file' name='imagen4' size='70'></td></tr>
	<tr><td align='right'>Imagen5 :</td><td><input type='file' name='imagen5' size='70'></td></tr>
	<tr><td align='right'>Imagen6 :</td><td><input type='file' name='imagen6' size='70'></td></tr>
	<tr><td align='right'>Imagen7 :</td><td><input type='file' name='imagen7' size='70'></td></tr>
	<tr><td align='right'>Imagen8 :</td><td><input type='file' name='imagen8' size='70'></td></tr>
	<tr><td align='right'>Imagen9 :</td><td><input type='file' name='imagen9' size='70'></td></tr>
	<tr><td align='right'>Imagen10 :</td><td><input type='file' name='imagen10' size='70'></td></tr>
	<tr><td align='right'>Imagen11 :</td><td><input type='file' name='imagen11' size='70'></td></tr>
	<tr><td align='right'>Imagen12 :</td><td><input type='file' name='imagen12' size='70'></td></tr>
	<tr><td align='right'>Imagen13 :</td><td><input type='file' name='imagen13' size='70'></td></tr>
	<tr><td align='right'>Imagen14 :</td><td><input type='file' name='imagen14' size='70'></td></tr>
	<tr><td align='right'>Imagen15 :</td><td><input type='file' name='imagen15' size='70'></td></tr>
	<tr><td align='right'>Imagen16 :</td><td><input type='file' name='imagen16' size='70'></td></tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td align='right'><input type='submit' value='     CREAR     '></form></td></tr>
	</table>";
}
function insertar_casa()
{
	for($i=0;$i<=16;$i++)
	{
		if($_FILES["imagen".$i]["name"])
		{
			copy($_FILES["imagen".$i]["tmp_name"],"../casas/".$_FILES["imagen".$i]['name']);
			$filename = "../casas/".$_FILES["imagen".$i]['name'];
			list($width, $height) = getimagesize($filename);
			$newwidth = 480;
			$newheight = floor((480 * $height) / $width); //o 320
			$thumb = imagecreatetruecolor($newwidth, $newheight);
			$source = imagecreatefromjpeg($filename);
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			imagejpeg($thumb,"../casas/".$_FILES["imagen".$i]['name']);
			imagedestroy($thumb);
		}
	}

	$consulta="insert into rural_casas set id_usuario='".$_POST["id_usuario"]."',visible='".$_POST["visible"]."',prioridad='".$_POST["prioridad"]."',nombre='".$_POST["nombre"]."',tipo='".$_POST["tipo"]."',direccion='".$_POST["direccion"]." ".$_POST["poblacion"]."',provincia='".$_POST["provincia"]."',metros='".$_POST["metros"]."',capacidad1='".$_POST["capacidad1"]."',capacidad2='".$_POST["capacidad2"]."',precio_limpieza='".$_POST["precio_limpieza"]."',num_hab='".$_POST["num_hab"]."',num_bano='".$_POST["num_bano"]."',num_aseo='".$_POST["num_aseo"]."',num_camasofa='".$_POST["num_camasofa"]."',num_cama1='".$_POST["num_cama1"]."',num_cama2='".$_POST["num_cama2"]."',estancia='".$_POST["estancia"]."',retraso='".$_POST["retraso"]."',contacto_per='".$_POST["contacto_per"]."',contacto_mail='".$_POST["contacto_mail"]."',contacto_tel='".$_POST["contacto_tel"]."',incremento='".$_POST["incremento"]."',fianza='".$_POST["fianza"]."',precio_dia='".$_POST["precio_dia"]."', ";
	$consulta=$consulta." mapa='".$_POST["mapa"]."',desc_esp='".$_POST["desc_esp"]."',desc_ing='".$_POST["desc_ing"]."',loc_esp='".$_POST["loc_esp"]."',loc_ing='".$_POST["loc_ing"]."',img1='".$_FILES["imagen1"]["name"]."',img2='".$_FILES["imagen2"]["name"]."',img3='".$_FILES["imagen3"]["name"]."',img4='".$_FILES["imagen4"]["name"]."',img5='".$_FILES["imagen5"]["name"]."',img6='".$_FILES["imagen6"]["name"]."',img7='".$_FILES["imagen7"]["name"]."',img8='".$_FILES["imagen8"]["name"]."',img9='".$_FILES["imagen9"]["name"]."',img10='".$_FILES["imagen10"]["name"]."',img11='".$_FILES["imagen11"]["name"]."',img12='".$_FILES["imagen12"]["name"]."',img13='".$_FILES["imagen13"]["name"]."',img14='".$_FILES["imagen14"]["name"]."',img15='".$_FILES["imagen15"]["name"]."',img16='".$_FILES["imagen16"]["name"]."' ";

	if(ejecutar($consulta))
	{
		echo "<p>La casa se ha insertado correctamente.</p>";
		if(ejecutar("insert into rural_caracteristicas set id_casa=(select max(id) from rural_casas)"))
			echo "<p>Las caracteristicas de la casa se ha insertado correctamente.</p>";
	}
	else
		echo "<p>ERROR</p>";
}
function modifcasa()
{	
	$fila=mysql_fetch_assoc(ejecutar("select * from rural_casas where id='".$_GET["id"]."'"));
	$usuarios=ejecutar("select id,nombre from usuarios where tipo='rural' order by id desc");
	
	$texto="<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_modcasa' method='post' action='index.php?modo=update_casa&id=".$_GET["id"]."' enctype='multipart/form-data'>
	<tr><td align='right'>Nombre :</td><td><input type='text' name='nombre' size='44' value='".$fila["nombre"]."'></td></tr>
	<tr><td align='right'>Propietario :</td><td><select name='id_usuario'>";
	
	while($lista = mysql_fetch_assoc($usuarios))
	{
		if($fila["id_usuario"]==$lista["id"])
			$texto=$texto."<option value='".$lista["id"]."' selected='selected'>".$lista["nombre"]."</option>";
		else
			$texto=$texto."<option value='".$lista["id"]."'>".$lista["nombre"]."</option>";	
	}
	
	$texto=$texto."</select></td></tr>
	<tr><td align='right'>Direccion :</td><td><input type='text' name='direccion' size='44' value='".$fila["direccion"]."'></td></tr>
	<tr><td align='right'>Provincia</td><td><select name='provincia'>";
	
	echo $texto;
	include($_SERVER['DOCUMENT_ROOT']."/casas-rurales/includes/inc.provincias.php");
	
	echo "</select></td></tr>
	<tr><td align='right'>Metros2 : </td><td><input type='text' name='metros' size='5' maxlength='3' value='".$fila["metros"]."'></td></tr>
	<tr><td align='right'>Capacidad : </td><td><input type='text' name='capacidad1' size='2' maxlength='2' value='".$fila["capacidad1"]."'>-<input type='text' name='capacidad2' size='2' maxlength='2' value='".$fila["capacidad2"]."'> personas</td></tr>
	<tr><td align='right'>Estancia Minima : </td><td><input type='text' name='estancia' value='".$fila["estancia"]."' size='4'  maxlength='2'> noches</td></tr>	
	<tr><td align='right'>Retraso : </td><td><input type='text' name='retraso' value='".$fila["retraso"]."' size='4'  maxlength='2'> euros</td></tr>
	<tr><td align='right'>Prioridad : </td><td><input type='text' name='prioridad' value='".$fila["prioridad"]."' size='4' maxlength='2'> (0 al 5)</td></tr>	
	<tr><td align='right'>Visible : </td><td><select name='visible'>";
	
	if($fila["visible"])
		$texto="<option value='1' selected='selected'>Si</option><option value='0'>No</option>";
	else
		$texto="<option value='1'>Si</option><option value='0' selected='selected'>No</option>";
	
	//en los pisos sin imagen solo un boton cambiar, si tiene imagen boton borrar
		
	$texto=$texto."</select></td></tr>
	<tr><td align='right'>".GESTION." :</td><td><input type='text' name='precio_limpieza' size='5' maxlength='3' value='".$fila["precio_limpieza"]."'> euros</td></tr>
	<tr><td align='right'>Precio por Dia :</td><td><input type='text' name='precio_dia' value='".$fila["precio_dia"]."' size='4' maxlength='4'> euros - Fin de semana <input type='text' name='precio_dia2' size='4' maxlength='4'> euros</td></tr>
	<tr><td align='right'>Incremento :</td><td><input type='text' name='incremento' value='".$fila["incremento"]."' size='4' maxlength='4'> euros por dia</td></tr>
	<tr><td align='right'>Fianza :</td><td><input type='text' name='fianza' value='".$fila["fianza"]."' size='4' maxlength='4'> euros</td></tr>		

	<tr><td align='right'>Numero Habitaciones/Ba&ntilde;os/Aseos :</td><td><input type='text' name='num_hab' value='".$fila["num_hab"]."' size='4'  maxlength='2'>-<input type='text' name='num_bano' value='".$fila["num_bano"]."' size='4' maxlength='2'>-<input type='text' name='num_aseo' value='".$fila["num_aseo"]."' size='4' maxlength='2'></td></tr>
	<tr><td align='right'>Numero Camas Indivi/Matrim/Sofa :</td><td><input type='text' name='num_cama1' value='".$fila["num_cama1"]."' size='4'  maxlength='2'>-<input type='text' name='num_cama2' value='".$fila["num_cama2"]."' size='4' maxlength='2'>-<input type='text' name='num_camasofa' value='".$fila["num_camasofa"]."' size='4' maxlength='2'></td></tr>
	<tr><td align='right'>Persona de contacto :</td><td><input type='text' name='contacto_per' value='".$fila["contacto_per"]."' size='44'></td></tr>
	<tr><td align='right'>Telefono de contacto :</td><td><input type='text' name='contacto_tel' value='".$fila["contacto_tel"]."' size='44'></td></tr>
	<tr><td align='right'>Correo Electronico de contacto :</td><td><input type='text' name='contacto_mail' value='".$fila["contacto_mail"]."' size='44'></td></tr>
	<tr><td align='right'>Mapa </td><td><textarea name='mapa' size='44' cols='45' rows='11'>".$fila["mapa"]."</textarea></td></tr>
	
	<tr><td>Descripcion(Esp) :</td><td>Descripcion(Ing) :</td></tr>
	<tr><td><textarea name='desc_esp' cols='45' rows='11'>".$fila["desc_esp"]."</textarea></td><td><textarea name='desc_ing' cols='45' rows='11'>".$fila["desc_ing"]."</textarea></td></tr>
	<tr><td>Localizacion(Esp) :</td><td>Localizacion(Ing) :</td></tr>
	<tr><td><textarea name='loc_esp' cols='45' rows='11'>".$fila["loc_esp"]."</textarea></td><td><textarea name='loc_ing' cols='45' rows='11'>".$fila["loc_ing"]."</textarea></td></tr>
	
	<tr>
		<td colspan='2' align='center'>
		<table cellspacing='0' cellpadding='8' align='center'>
		<tr>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img1"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img1"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img2"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img2"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img3"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img3"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img4"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img4"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg_casa&num=1&id=".$fila["id"]."&nombre=".$fila["img1"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=1&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=2&id=".$fila["id"]."&nombre=".$fila["img2"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=2&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=3&id=".$fila["id"]."&nombre=".$fila["img3"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=3&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=4&id=".$fila["id"]."&nombre=".$fila["img4"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=4&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		<tr>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img5"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img5"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img6"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img6"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img7"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img7"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img8"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img8"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg_casa&num=5&id=".$fila["id"]."&nombre=".$fila["img5"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=5&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=6&id=".$fila["id"]."&nombre=".$fila["img6"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=6&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=7&id=".$fila["id"]."&nombre=".$fila["img7"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=7&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=8&id=".$fila["id"]."&nombre=".$fila["img8"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=8&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		
		<tr>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img9"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img9"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img10"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img10"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img11"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img11"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img12"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img12"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg_casa&num=9&id=".$fila["id"]."&nombre=".$fila["img9"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=9&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=10&id=".$fila["id"]."&nombre=".$fila["img10"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=10&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=11&id=".$fila["id"]."&nombre=".$fila["img11"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=11&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=12&id=".$fila["id"]."&nombre=".$fila["img12"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=12&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		<tr>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img13"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img13"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img14"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img14"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img15"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img15"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/casas/".$fila["img16"]."' target='_blank'><img src='http://www.sleepbcn.com/casas/".$fila["img16"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg_casa&num=13&id=".$fila["id"]."&nombre=".$fila["img13"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=13&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=14&id=".$fila["id"]."&nombre=".$fila["img14"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=14&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=15&id=".$fila["id"]."&nombre=".$fila["img15"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=15&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg_casa&num=16&id=".$fila["id"]."&nombre=".$fila["img16"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg_casa&num=16&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td align='right'><input type='submit' value='     ACTUALIZAR    '></form></td></tr>
	</table>";
	echo $texto;
	echo "<script type='text/javascript'>document.form_modcasa.provincia.value='".$fila["provincia"]."';</script>";
}
function update_casa()
{	
	$consulta="update rural_casas set nombre='".$_POST["nombre"]."',id_usuario='".$_POST["id_usuario"]."',prioridad='".$_POST["prioridad"]."',visible='".$_POST["visible"]."',direccion='".$_POST["direccion"]."',provincia='".$_POST["provincia"]."',metros='".$_POST["metros"]."',capacidad1='".$_POST["capacidad1"]."',capacidad2='".$_POST["capacidad2"]."',desc_esp='".$_POST["desc_esp"]."',desc_ing='".$_POST["desc_ing"]."',loc_esp='".$_POST["loc_esp"]."',loc_ing='".$_POST["loc_ing"]."',precio_limpieza='".$_POST["precio_limpieza"]."',incremento=".$_POST["incremento"].",";
	$consulta=$consulta."num_hab=".$_POST["num_hab"].",num_bano=".$_POST["num_bano"].",num_aseo=".$_POST["num_aseo"].",num_cama1=".$_POST["num_cama1"].",num_cama2=".$_POST["num_cama2"].",num_camasofa=".$_POST["num_camasofa"].",estancia=".$_POST["estancia"].",retraso=".$_POST["retraso"].",precio_dia='".$_POST["precio_dia"]."',fianza='".$_POST["fianza"]."',contacto_per='".$_POST["contacto_per"]."',contacto_tel='".$_POST["contacto_tel"]."',contacto_mail='".$_POST["contacto_mail"]."',mapa='".$_POST["mapa"]."' where id='".$_GET["id"]."'";
	
	if(ejecutar($consulta))
		echo "<script>window.open('index.php?modo=modifcasa&id=".$_GET["id"]."','_self')</script>";	
	else
		echo "<p>ERROR</p>";	
}
function borraimg_casa()
{
	unlink("../casas/".$_GET["nombre"]);

	if(ejecutar("update rural_casas set img".$_GET["num"]."='' where id='".$_GET["id"]."'"))
		echo "<script>window.open('http://www.sleepbcn.com/privado/index.php?modo=modifcasa&id=".$_GET["id"]."','_self')</script>";	
	else
		echo "<p>ERROR</p>";	
}
function editimg_casa()
{
	echo "<form name='form_newimg' method='post' action='index.php?modo=insertar_img_casa&id=".$_GET["id"]."' enctype='multipart/form-data' class='nuevo_formulario'>
	<input type='file' name='imagen".$_GET["num"]."' size='70'>
	<input type='hidden' name='oculto' value='".$_GET["num"]."'><br>
	<input type='submit' value='   ACEPTAR    '>
	</form>";
}
function insertar_img_casa()
{
	copy($_FILES["imagen".$_POST["oculto"]]["tmp_name"],"../casas/".$_FILES["imagen".$_POST["oculto"]]['name']);
	
	$filename = "../casas/".$_FILES["imagen".$_POST["oculto"]]['name'];
	list($width, $height) = getimagesize($filename);
	$newwidth = 480;
	$newheight = floor((480 * $height) / $width); //o 320
	$thumb = imagecreatetruecolor($newwidth, $newheight);
	$source = imagecreatefromjpeg($filename);
	imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	imagejpeg($thumb,"../casas/".$_FILES["imagen".$_POST["oculto"]]['name']);
	imagedestroy($thumb);
	
	if(ejecutar("update rural_casas set img".$_POST["oculto"]."='".$_FILES["imagen".$_POST["oculto"]]['name']."' where id='".$_GET["id"]."'"))
		echo "<script>window.open('http://www.sleepbcn.com/privado/index.php?modo=modifcasa&id=".$_GET["id"]."','_self')</script>";	
	else
		echo "<p>ERROR</p>";
}
function modifcar_casa()
{
	$fila=mysql_fetch_assoc(ejecutar("select * from rural_caracteristicas where id_casa='".$_GET["id"]."'"));
	
	$texto="<h4>Caracteristicas de casa: ".$_GET["nombre"]."</h4>
	<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_modcar' method='post' action='index.php?modo=update_casa_car&id=".$_GET["id"]."'>
	
	<tr><td>TV</td><td><input type='checkbox' name='tv' ";
	if($fila["tv"])				$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>TV Satelite</td><td><input type='checkbox' name='tv_sat' ";
	if($fila["tv_sat"])			$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Video</td><td><input type='checkbox' name='video' ";
	if($fila["video"])			$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>DVD</td><td><input type='checkbox' name='dvd' ";
	if($fila["dvd"])			$texto=$texto."checked='checked'";

	$texto=$texto."></td></tr><tr><td>Hi-Fi</td><td><input type='checkbox' name='hifi' ";
	if($fila["hifi"])			$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Aire Acondicionado</td><td><input type='checkbox' name='aire' ";
	if($fila["aire"])			$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Calefaccion</td><td><input type='checkbox' name='calefaccion' ";
	if($fila["calefaccion"])	$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>Ventilador</td><td><input type='checkbox' name='ventilador' ";
	if($fila["ventilador"])		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Nevera</td><td><input type='checkbox' name='nevera' ";
	if($fila["nevera"])			$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>Lavadora</td><td><input type='checkbox' name='lavadora' ";
	if($fila["lavadora"])		$texto=$texto."checked='checked'";	
	
	$texto=$texto."></td></tr><tr><td>Secadora</td><td><input type='checkbox' name='secadora' ";
	if($fila["secadora"])		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>Lavavajillas</td><td><input type='checkbox' name='lavavajillas' ";
	if($fila["lavavajillas"])	$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Horno</td><td><input type='checkbox' name='horno' ";
	if($fila["horno"])			$texto=$texto."checked='checked'";			
	
	$texto=$texto."></td><td>Microondas</td><td><input type='checkbox' name='microondas' ";
	if($fila["microondas"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Tostadora</td><td><input type='checkbox' name='tostadora' ";
	if($fila["tostadora"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Cafetera</td><td><input type='checkbox' name='cafetera' ";
	if($fila["cafetera"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Exprimidor</td><td><input type='checkbox' name='exprimidor' ";
	if($fila["exprimidor"])		$texto=$texto."checked='checked'";
	
	$texto=$texto."></td><td>Plancha</td><td><input type='checkbox' name='plancha' ";
	if($fila["plancha"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Secador</td><td><input type='checkbox' name='secador' ";
	if($fila["secador"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Ascensor</td><td><input type='checkbox' name='ascensor' ";
	if($fila["ascensor"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Minusvalidos</td><td><input type='checkbox' name='minusvalidos' ";
	if($fila["minusvalidos"])	$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Terraza</td><td><input type='checkbox' name='terraza' ";
	if($fila["terraza"])		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Balcon</td><td><input type='checkbox' name='balcon' ";
	if($fila["balcon"])			$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Parking</td><td><input type='checkbox' name='parking' ";
	if($fila["parking"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Garaje</td><td><input type='checkbox' name='garaje' ";
	if($fila["garaje"])			$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Jardin</td><td><input type='checkbox' name='jardin' ";
	if($fila["jardin"])			$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Piscina</td><td><input type='checkbox' name='piscina' ";
	if($fila["piscina"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Internet</td><td><input type='checkbox' name='internet' ";
	if($fila["internet"])		$texto=$texto."checked='checked'";
	
	$texto=$texto."></td></tr><tr><td>Fumadores</td><td><input type='checkbox' name='fumadores' ";
	if($fila["fumadores"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Ropa de Cama</td><td><input type='checkbox' name='ropacama' ";
	if($fila["ropacama"])		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Toallas</td><td><input type='checkbox' name='toallas' ";
	if($fila["toallas"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Mascotas</td><td><input type='checkbox' name='mascotas' ";
	if($fila["mascotas"])		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Cuna Bebes</td><td><input type='checkbox' name='cuna' ";
	if($fila["cuna"])			$texto=$texto."checked='checked'";																					
		
	$texto=$texto."></td><td>-</td></tr><tr><td colspan='2' align='right'><input type='submit' value='     ACTUALIZAR     '>
	</form></td></tr></table>";
	
	echo $texto;
}
function update_casa_car()
{
	$tv=($_POST["tv"])? 					1:0;
	$tv_sat=($_POST["tv_sat"])? 			1:0;
	$video=($_POST["video"])?				1:0;
	$dvd=($_POST["dvd"])? 					1:0;
	$hifi=($_POST["hifi"])?					1:0;
	$aire=($_POST["aire"])?					1:0;
	$calefaccion=($_POST["calefaccion"])?	1:0;	
	$ventilador=($_POST["ventilador"])?		1:0;
	$nevera=($_POST["nevera"])?				1:0;
	$lavadora=($_POST["lavadora"])?			1:0;	
	$secadora=($_POST["secadora"])?			1:0;
	$lavavajillas=($_POST["lavavajillas"])?	1:0;
	$horno=($_POST["horno"])?				1:0;	
	$microondas=($_POST["microondas"])?		1:0;
	$tostadora=($_POST["tostadora"])?		1:0;	
	$cafetera=($_POST["cafetera"])?			1:0;	
	$exprimidor=($_POST["exprimidor"])?		1:0;
	$plancha=($_POST["plancha"])?			1:0;	
	$secador=($_POST["secador"])?			1:0;
	$ascensor=($_POST["ascensor"])?			1:0;
	$minusvalidos=($_POST["minusvalidos"])?	1:0;
	$terraza=($_POST["terraza"])?			1:0;	
	$balcon=($_POST["balcon"])?				1:0;
	$parking=($_POST["parking"])?			1:0;
	$garaje=($_POST["garaje"])?				1:0;
	$jardin=($_POST["jardin"])?				1:0;
	$piscina=($_POST["piscina"])?			1:0;
	$internet=($_POST["internet"])?			1:0;
	$fumadores=($_POST["fumadores"])?		1:0;	
	$ropacama=($_POST["ropacama"])?			1:0;
	$toallas=($_POST["toallas"])?			1:0;	
	$mascotas=($_POST["mascotas"])?			1:0;	
	$cuna=($_POST["cuna"])?					1:0;	
		
	if(ejecutar("update rural_caracteristicas set tv='".$tv."',tv_sat='".$tv_sat."',video='".$video."',dvd='".$dvd."',hifi='".$hifi."',aire='".$aire."',calefaccion='".$calefaccion."',ventilador='".$ventilador."',nevera='".$nevera."',lavadora='".$lavadora."',secadora='".$secadora."',lavavajillas='".$lavavajillas."',horno='".$horno."',microondas='".$microondas."',tostadora='".$tostadora."',cafetera='".$cafetera."',exprimidor='".$exprimidor."',plancha='".$plancha."',secador='".$secador."',ascensor='".$ascensor."',minusvalidos='".$minusvalidos."',terraza='".$terraza."',balcon='".$balcon."',parking='".$parking."',garaje='".$garaje."',jardin='".$jardin."',piscina='".$piscina."',internet='".$internet."',fumadores='".$fumadores."',ropacama='".$ropacama."',toallas='".$toallas."',mascotas='".$mascotas."',cuna='".$cuna."' where id_casa=".$_GET["id"]))
	{
		echo "<script>window.alert('Las caracteristicas se ha actualizado correctamente.');
		window.open('index.php?modo=listar_casas','_self');</script>";	
	}			
}
function ver_reservas_rural()
{
	$ano=(!$_POST["ano"])?	"2009":$_POST["ano"];	

	$listado=ejecutar("select rural_reservas.id as reservaid,rural_reservas.id_casa,rural_reservas.pedidohora,rural_reservas.fecha1,rural_reservas.fecha2,rural_reservas.preciototal,rural_reservas.correo,rural_casas.id as casaid,rural_casas.nombre as casanombre from rural_reservas,rural_casas where rural_reservas.id_casa=rural_casas.id and rural_reservas.fecha1 like '".$ano."%' order by reservaid desc");
	$texto="
	<form method='post' action='index.php?modo=ver_reservas_rural' style='margin:10px 0 -10px 20px;'>
	A&ntilde;o : <select name='ano'><option value='2009' selected='selected'>2009</option><option value='2010'>2010</option></select>
	<input type='submit' style='margin:0 0 0 100px;' value=' BUSCAR ' />
	</form>
		
	<table cellpadding='2' cellspacing='1' class='listadopisos'><tr><td>&nbsp;</td>
	<td><b>ID</b></td><td><b>CASA</b></td><td><b>PEDIDOHORA</b></td><td><b>F.ENTRADA</b></td><td><b>F.SALIDA</b></td><td><b>PRECIO</b></td><td><b>EMAIL</b></td></tr>";
		
	while($fila = mysql_fetch_assoc($listado))
	{
		$texto=$texto."<tr>";
		$texto=$texto."<td nowrap='nowrap'><a href='index.php?modo=modifreserva_rural&id=".$fila["reservaid"]."'><img src='../img/icon_edit.png' border='0' title='Ver'></a> <a href='javascript:confirmavalidar_rural(".$fila["reservaid"].")'><img src='../img/icon_ok.png' border='0' title='Validar Reserva'></a></td>";
    	$texto=$texto."<td>".$fila["reservaid"]."</td>";
		$texto=$texto."<td>".$fila["casanombre"]."</td>";
		$texto=$texto."<td>".$fila["pedidohora"]."</td>";
		$texto=$texto."<td>".$fila["fecha1"]."</td>";
		$texto=$texto."<td>".$fila["fecha2"]."</td>";
		$texto=$texto."<td>".$fila["preciototal"]." &euro;</td>";
		$texto=$texto."<td>".$fila["correo"]."</td>";
		$texto=$texto."</tr>";
	}
	echo $texto."</table>";
}
function modifreserva_rural()
{
	$fila=mysql_fetch_assoc(ejecutar("select rural_reservas.id as idreserva,rural_reservas.id_casa as idcasareserva,rural_reservas.correo as correoreserva,rural_reservas.preciototal as preciototal,rural_reservas.fecha1,rural_reservas.fecha2,rural_reservas.personas,rural_reservas.persona_nombre,rural_reservas.persona_direccion,rural_reservas.persona_ciudad,rural_reservas.persona_pais,rural_reservas.persona_telefono,rural_reservas.persona_movil,rural_reservas.persona_dni,rural_reservas.persona_nacimiento,rural_reservas.persona_transporte,rural_reservas.persona_hora,rural_reservas.persona_comentario,rural_casas.id,rural_casas.nombre as nombrecasa,rural_casas.contacto_per,rural_casas.contacto_mail,rural_casas.contacto_tel,rural_casas.precio_limpieza,rural_casas.fianza,rural_casas.direccion as direccionreserva from rural_reservas,rural_casas where rural_reservas.id='".$_GET["id"]."' and rural_reservas.id_casa=rural_casas.id"));
	
	$preciototal=$fila["preciototal"]+$fila["precio_limpieza"];
	$precio20iva=($fila["preciototal"]*0.2)+($fila["preciototal"]*0.2*0.16);
	$precioresto=$preciototal-$precio20iva;
	
	$texto="<p>If you need to contact us do it at <a href='mailto:info@sleepbcn.com'>info@sleepbcn.com</a></p>
	<p>BOOKING CONFIRMATION</p>
	<p>Thank you very much for choosing SleepBCN houses.<br>We have received your payment correctly. This is to confirm your  reservation as follows:</p>
	<p>Total amount : <b>".$preciototal." &euro;</b><br>
	Reservacion TOTAL (20% + IVA) : <b>".$precio20iva." &euro;</b> (all taxes included in this payment)<br>
	Remainder : <b>".$precioresto." &euro;</b> (to be paid cash to the owner on your check-in-day)<br>
	Refundable deposit : <b>".$fila["fianza"]." &euro;</b></p>
	<p>BOOKING REFERENCE: ".$fila["idreserva"]."</p>
	<p>House reference: <b>".$fila["nombrecasa"]."</b><br>
	<a href='http://rural.sleepbcn.com/casas-rurales/alojamiento.php?id=".$fila["idcasareserva"]."' target='_blank'>http://rural.sleepbcn.com/casas-rurales/alojamiento.php?id=".$fila["idcasareserva"]."</a><br>
	Arrival Date : <b>".$fila["fecha1"]."</b><br>
	Departure Date : <b>".$fila["fecha2"]."</b><br>
	Number of adults / children / babys: <b>".$fila["personas"]."</b><br>
	Name : <b>".$fila["persona_nombre"]."</b><br>
	EMail : <b>".$fila["correoreserva"]."</b><br>
	Address : <b>".$fila["persona_direccion"]."</b><br>
	City / Country: <b>".$fila["persona_ciudad"]." - ".$fila["persona_pais"]."</b><br>
	Tel / Movil: <b>".$fila["persona_telefono"]." - ".$fila["persona_movil"]."</b><br>
	DNI / Passport : <b>".$fila["persona_dni"]."</b><br>
	Birthday : <b>".$fila["persona_nacimiento"]."</b><br>
	Transport / Time : <b>".$fila["persona_transporte"]." - ".$fila["persona_hora"]."</b><br>
	Comment : <b>".$fila["persona_comentario"]."</b></p>
	<p>HOUSE ADDRESS</p>
	<p>House address: <b>".$fila["direccionreserva"]."</b><br>
	Check-in time: <b>after 14:00 h.</b><br>
	Check-out time: <b>before 11:00 h.</b></p>
	<p>CONTACT PERSON</p>
	<p>Contact name : <b>".$fila["contacto_per"]."</b><br>Contact e-mail : <b>".$fila["contacto_mail"]."</b><br>Contact Tel: <b>".$fila["contacto_tel"]."</b></p>
	<p><b>IMPORTANT:</b></p>
	<p>Please contact the owners to communicate them your arrival time  details.</p>
	<p>&bull; All the apartments are of private property wich makes a good communication  very important as often the owners will have to go to the apartments themselves to do  check-ins/-outs etc.</p>
	<p>&bull; We would appreciate it very much if you could help the owners a little  bit by contacting them beforahand by e-mail or phone to confirm your exact arrival time. In case of any delay, please call the owner immediately to inform him.</p>
	<p>&bull; The owner or representative will be waiting for you at the apartment to  give, you the keys and to show you everything worth knowing.This is also when  you pay the remaining balance plus a refundable deposit of 150 euros to cover  any possible damage to the property. In case of any doubts or problems  ocurring during your stay, please contact to owner directly. He will try to help  you and to solve the problem right away.</p>
	<p>&bull; The apartments are completely equipped with towels, bed linen and  kitchen implements. All services susch as electricity, gas and water ae included.</p>
	<p>&bull; SleepBCN apartments mediates apartment reservations between owners and customers. As an agency we are only responsible for our service and always  try to help you in this matter with pleasure. For all affairs concerning the apartment, your contact person is the  owner.</p>
	<p>&bull; Neither SleepBCN apartments nor its agent(s) are responsible for any  contracts made between owners and clients, in verbal or written form, any loss of  damage to property or injury to persons caused by any defect, negligence or other  wrong or irresponsible behaviour.</p>
	<p>We wish you a pleasant stay in Barcelona</p>
	<p>****** Please print out this e-mail for your records ******</p>
	<p><a href='http://rural.sleepbcn.com'>http://rural.sleepbcn.com</a></p>";
	echo $texto;
}
function validar_reserva_rural()
{	
	pasarela_fin_rural("rural_reservas.id='".$_GET["id"]."'");
}
function pasarela_fin_rural($clausula)
{
	$ultimo=mysql_fetch_assoc(ejecutar("select rural_reservas.id as idreserva,rural_reservas.id_casa as idcasareserva,rural_reservas.correo as correoreserva,rural_reservas.preciototal as preciototal,rural_reservas.fecha1,rural_reservas.fecha2,rural_reservas.personas,rural_reservas.persona_nombre,rural_reservas.persona_direccion,rural_reservas.persona_ciudad,rural_reservas.persona_pais,rural_reservas.persona_telefono,rural_reservas.persona_movil,rural_reservas.persona_dni,rural_reservas.persona_nacimiento,rural_reservas.persona_transporte,rural_reservas.persona_hora,rural_reservas.persona_comentario,rural_reservas.pedidohora,rural_casas.id,rural_casas.nombre as nombrecasa,rural_casas.contacto_per,rural_casas.contacto_mail,rural_casas.contacto_tel,rural_casas.precio_limpieza,rural_casas.fianza,rural_casas.direccion as direccionreserva from rural_reservas,rural_casas where ".$clausula." and rural_reservas.id_casa=rural_casas.id"));
	
	//ejecutar la ocupacion de dias en rojo automaticamente//autor=3 usuario pasarela
	$fecha_ini=explode("-",$ultimo["fecha1"]);
	$fecha_fin=explode("-",$ultimo["fecha2"]);
	$date1 = mktime(0,0,0,$fecha_ini[1],$fecha_ini[2],$fecha_ini[0]);
	$date2 = mktime(0,0,0,$fecha_fin[1],$fecha_fin[2],$fecha_fin[0]);

	$numerodias=round(($date2 - $date1) / (60 * 60 * 24));
	$datstart = $ultimo["fecha1"];
	
	//primer insert previo borrar si era oferta
	ejecutar("delete from rural_calendario where id_casa='".$ultimo["idcasareserva"]."' and dia='".$ultimo["fecha1"]."'");
	
	if(ejecutar("insert into rural_calendario (id_casa,dia,autor,estado) values('".$ultimo["idcasareserva"]."','".$ultimo["fecha1"]."',3,'ocupado_x1')") )
	{	
		echo "<p>OK</p>";
		while ($numerodias >= 2) 
		{
			$datyy=substr($datstart,0,4);
			$datmm=substr($datstart,5,2);
			$datdd=substr($datstart,8,2);
			$fda=$datdd + 1;
			$fmo=$datmm + 0;
			$fyr=$datyy + 0;
			$dat1=date("Y-m-d", mktime(0,0,0,$fmo,$fda,$fyr)); 
		
			//resto de inserts previos borrar
			ejecutar("delete from rural_calendario where id_casa='".$ultimo["idcasareserva"]."' and dia='".$dat1."'");
			ejecutar("insert into rural_calendario (id_casa,dia,autor,estado) values('".$ultimo["idcasareserva"]."','".$dat1."',3,'ocupado')");
		
			$datstart=$dat1; 
			$numerodias--; 
		}
		$preciototal=$ultimo["preciototal"]+$ultimo["precio_limpieza"]; /*+$incremento*/
		$precio20iva=($ultimo["preciototal"]*0.2)+($ultimo["preciototal"]*0.2*0.16);
		$precioresto=$preciototal-$precio20iva;
		
		$cuerpo_mail="<p>If you need to contact us do it at <a href='mailto:info@sleepbcn.com'>info@sleepbcn.com</a></p>
		<p>BOOKING CONFIRMATION</p>
		<p>Thank you very much for choosing SleepBCN houses.<br>We have received your payment correctly. This is to confirm your reservation as follows:</p>
		Total amount : ".$preciototal." &euro;<br>
		Reservacion TOTAL (20% + IVA) : ".$precio20iva." &euro; (all taxes included in this payment)<br>
		Remainder : ".$precioresto." &euro; (to be paid cash to the owner on your check-in-day)<br>
		Refundable deposit : ".$ultimo["fianza"]." &euro;<br>
		<p>BOOKING REFERENCE: ".$ultimo["idreserva"]."</p>
		House reference: ".$ultimo["nombrecasa"]."<br>
		<a href='http://rural.sleepbcn.com/casas-rurales/alojamiento.php?id=".$ultimo["idcasareserva"]."'>http://rural.sleepbcn.com/casas-rurales/alojamiento.php?id=".$ultimo["idcasareserva"]."</a><br>
		Arrival Date : ".$ultimo["fecha1"]."<br>
		Departure Date : ".$ultimo["fecha2"]." (".round(($date2 - $date1) / (60 * 60 * 24))." night)<br>
		Number of adults / children / babys: ".$ultimo["personas"]."<br>
		Name : ".$ultimo["persona_nombre"]."<br>
		EMail : ".$ultimo["correoreserva"]."<br>
		Address : ".$ultimo["persona_direccion"]."<br>
		City / Country: ".$ultimo["persona_ciudad"]." - ".$ultimo["persona_pais"]."<br>
		Tel / Movil: ".$ultimo["persona_telefono"]." - ".$ultimo["persona_movil"]."<br>
		DNI / Passport : ".$ultimo["persona_dni"]."<br>
		Birthday : ".$ultimo["persona_nacimiento"]."<br>
		Transport / Time : ".$ultimo["persona_transporte"]." - ".$ultimo["persona_hora"]."<br>
		Comment : ".$ultimo["persona_comentario"]."<br>
		<p>HOUSE ADDRESS</p>
		House address: ".$ultimo["direccionreserva"]."<br>
		Check-in time: after 14:00 h.<br>
		Check-out time: before 11:00 h.
		<p>CONTACT PERSON</p>
		Contact name : ".$ultimo["contacto_per"]."<br>Contact e-mail : ".$ultimo["contacto_mail"]."<br>Contact Tel: ".$ultimo["contacto_tel"]." 
		<p>IMPORTANT:</p> 
		Please contact the owners to communicate them your arrival time  details.
		<p>&bull; All the apartments are of private property wich makes a good communication  very important as often the owners will have to go to the apartments themselves to do  check-ins/-outs etc.</p>
		<p>&bull; We would appreciate it very much if you could help the owners a little  bit by contacting them beforahand by e-mail or phone to confirm your exact arrival time. In case of any delay, please call the owner immediately to inform him.</p>
		<p>&bull; The owner or representative will be waiting for you at the apartment to  give, you the keys and to show you everything worth knowing.This is also when  you pay the remaining balance plus a refundable deposit of 150 euros to cover  any possible damage to the property. In case of any doubts or problems  ocurring during your stay, please contact to owner directly. He will try to help  you and to solve the problem right away.</p>
		<p>&bull; The apartments are completely equipped with towels, bed linen and  kitchen implements. All services susch as electricity, gas and water ae included.</p>
		<p>&bull; SleepBCN apartments mediates apartment reservations between owners and customers. As an agency we are only responsible for our service and always  try to help you in this matter with pleasure. For all affairs concerning the apartment, your contact person is the  owner.</p>
		<p>&bull; Neither SleepBCN apartments nor its agent(s) are responsible for any  contracts made between owners and clients, in verbal or written form, any loss of  damage to property or injury to persons caused by any defect, negligence or other  wrong or irresponsible behaviour.</p>
		<p>We wish you a pleasant stay in Barcelona</p>
		<p>****** Please print out this e-mail for your records ******</p>
		<p><a href='http://rural.sleepbcn.com'>http://rural.sleepbcn.com</a></p>";
		
		echo "<p>Transaction DONE CORRECTLY<br><i>Transaccion realizada con EXITO</i></p>";
		if($ultimo["pedidohora"]!="-")
		{
			if(mail($ultimo["correoreserva"],"Booking Confirmation SleepRURAL",$cuerpo_mail,"MIME-Version:1.0\r\nFrom:info@sleepbcn.com\nReply-To:info@sleepbcn.com\nContent-type:text/html\nCc:omar@indianwebs.com\nBcc:".$ultimo["contacto_mail"]."\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
				echo "<p>It has sent an email confirmation<br><i>Se ha enviado un correo de confirmaci&oacute;n a su cuenta.</i></p>";
		}
	}
}
?>