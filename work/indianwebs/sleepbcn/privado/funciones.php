<?php
session_start();
define("POR_PAGINA","8");
define("VISIBLE_0","NO");
define("VISIBLE_1","SI");

if($_GET["lang"])			
	$_SESSION["lang"]=$_GET["lang"];
else	
{
	if(!$_SESSION["lang"])	$_SESSION["lang"]="esp";
}
include($_SERVER['DOCUMENT_ROOT']."/idiomas/".$_SESSION["lang"].".php");

//si se han enviado los datos de busqueda se guardan las variables de sesion
if($_POST["dia1"] && $_POST["dia2"])
{
	$_SESSION["fecha1dia"]=$_POST["dia1"];
	$_SESSION["fecha1mes"]=$_POST["mes1"];
	$_SESSION["fecha1ano"]=$_POST["ano1"];
	$_SESSION["fecha2dia"]=$_POST["dia2"];
	$_SESSION["fecha2mes"]=$_POST["mes2"];
	$_SESSION["fecha2ano"]=$_POST["ano2"];
	$_SESSION["npersonas"]=$_POST["adultos"];
	$_SESSION["nninos"]=$_POST["ninos"];
}

/*****************************************funciones****************************************/
function ejecutar($texto_consulta)
{
	if(mysql_connect("mysql.sleepbcn.com","ADM_bd670856","Barcelona9494"))
	{
		mysql_select_db("bd670856");
		$resultado=mysql_query($texto_consulta) or die(mysql_error());
		mysql_close();	
	}
	return $resultado;	
}	
function salir()
{
	session_unset();
	session_destroy();
	echo "<script>window.open('http://www.sleepbcn.com','_self')</script>";		
}
function comprobacion()
{
	if($_POST["nombre"]==null || $_POST["password"]==null)
		return false;
	$comprobar=ejecutar("select * from usuarios where login='".$_POST["nombre"]."'");
	$registro=mysql_fetch_assoc($comprobar);
	
	if($_POST["password"]==$registro["password"])
	{
		if($registro["tipo"]=="super")	
			$_SESSION["autentificacion"]=2;
		else
		{	
			$_SESSION["autentificacion"]=1;	
			$_SESSION["idnombre"]=$registro["id"];
		}
	}	
}
function redireccionar($pagina)
{
	echo "<script type='text/javascript'>window.open('".$pagina."','_self')</script>";	
}
//////////////////////////////////funciones de la pagina publica///////////////////////////////

function mostrar($listado)
{
	while($fila = @mysql_fetch_assoc($listado))
	{	
		if($fila["tipo"]=="habitacion")								
			$texto_tipo="room";
		else	
			$texto_tipo="apartment";
			
		echo "<div class='pisos1'><br>
		<a title='Barcelona ".$texto_tipo." ".$fila["nombre"]."' href='barcelona-apartments.php?id=".$fila["id"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img1"]."' class='minipic1' alt='".$fila["nombre"]." ".$texto_tipo." barcelona' title='".$fila["nombre"]." ".$fila["tipo"]." barcelona'/></a>
        <a title='Barcelona ".$texto_tipo." ".$fila["nombre"]."' href='barcelona-apartments.php?id=".$fila["id"]."' target='_blank'><h3>".$fila["nombre"]." <span style='color:#44485a'>&nbsp;&nbsp;".DESDE." ".(round($fila["precio_dia"]/$fila["capacidad1"],2))." &euro;</span>";
		
		if($fila["estado"])
		{
			//esto se podria mejorar para hacerlo todo en una consulta y que no haya bucle
			$listado2=ejecutar("select distinct estado from calendario where estado like 'oferta%' and id_piso='".$fila["id"]."'");
			while($fila2 = mysql_fetch_assoc($listado2))
			{
				echo "<img src='img/".$fila2["estado"].".gif' border='0' style='position:absolute;margin:-15px 0 0 5px;'>";
			}
		}
		
		echo "</h3><p>&nbsp;</p></a>
		<p><b>".strtoupper($texto_tipo)." | ".$fila["metros"]."m2 | ".$fila["num_hab"]." room | ".$fila["capacidad1"]."-".$fila["capacidad2"]." ".PERSONAS." | ".$fila["ubicacion"]."</b></p><p>&nbsp;</p>
        <p style='text-align:justify;'>".substr($fila["desc_".$_SESSION["lang"]],0,200)."... <a title='Barcelona ".$texto_tipo." ".$fila["nombre"]."' href='barcelona-apartments.php?id=".$fila["id"]."' target='_blank'>[+info...]</a></p>
		</div>";
	}
}

function pisos_ultimos($quetipo,$pagina)
{
	if(!$pagina)	
		$pagina=0;
	
	$listado=ejecutar("select id,img1,tipo,nombre,metros,capacidad1,capacidad2,ubicacion,num_hab,desc_esp,desc_ing,precio_dia from pisos where visible='1' and tipo='".$quetipo."' order by prioridad asc limit ".(POR_PAGINA*$pagina).",".POR_PAGINA." ");
	
	$_SESSION["num_resultados"]=@mysql_result(ejecutar("select count(id) from pisos where visible=1 and tipo='".$quetipo."'"),0); 
	
	mostrar($listado);
}

function pisos_oferta($pagina)
{
	//selecciona apartamentos y habitaciones en oferta, anteriores a fecha actual no
	
	if(!$pagina)	$pagina=0;
	
	$dia_actual=date("Y-m-d"); //a�o 0000- mes-00 dia-00
	$listado=ejecutar("select id,img1,tipo,nombre,metros,capacidad1,capacidad2,ubicacion,num_hab,desc_esp,desc_ing,precio_dia,estado from pisos,calendario where pisos.visible='1' and pisos.id=calendario.id_piso and calendario.estado like 'oferta%' and calendario.dia >= '".$dia_actual."' group by id order by prioridad asc limit ".(POR_PAGINA*$pagina).",".POR_PAGINA." ");
	
	$x=ejecutar("select pisos.id from pisos,calendario where pisos.visible=1 and pisos.id=calendario.id_piso and calendario.estado like 'oferta%' and calendario.dia >= '".$dia_actual."' group by pisos.id ");
	$_SESSION["num_resultados"]=mysql_num_rows($x);
	
	mostrar($listado);
}

function pisos_busqueda()
{
	//recibe datos del formulario de busqueda y prepara el query
	
	$fecha1=$_POST['ano1']."-".$_POST['mes1']."-".$_POST['dia1'];
	$fecha2=$_POST['ano2']."-".$_POST['mes2']."-".$_POST['dia2'];
	
	$date1 = mktime(0,0,0,$_POST['mes1'],$_POST['dia1'],$_POST['ano1']);
	$date2 = mktime(0,0,0,$_POST['mes2'],$_POST['dia2'],$_POST['ano2']);
	$numerodias=round(($date2 - $date1) / (60 * 60 * 24));
	
	//busca los pisos sin dias marcados o con dias en oferta (diferentes a ocupado)
	$listado=ejecutar("select id,img1,tipo,nombre,metros,capacidad1,capacidad2,ubicacion,desc_esp,desc_ing,precio_dia,estancia,prioridad from pisos where pisos.visible='1' and pisos.tipo='".$_POST["tipo"]."' and pisos.capacidad2 >='".($_POST["adultos"]+$_POST["ninos"])."' and pisos.estancia <= ".$numerodias." and pisos.id not in (select distinct id_piso from calendario where dia between '".$fecha1."' and '".$fecha2."' and calendario.estado like 'ocupa%' ) order by prioridad asc");
	mostrar($listado);
}

function cuantospisos($pagina)
{
	//recibe (index,apartamentos,busqueda-pisos,habitacion) y escribe paginacion abajo
	
	echo "<br /><p style='margin:0 30px 0 0;text-align:right;'>";
	
	if($_GET["pag"]>($_SESSION["num_resultados"]/POR_PAGINA))	
		echo "Next - ";
	else														
		echo "<a href='".$pagina."?pag=".($_GET["pag"]+1)."'>Next</a> - ";	
	
	for($i=0;$i<=($_SESSION["num_resultados"]/POR_PAGINA);$i++)
	{
		if($_GET["pag"]==$i)	
			echo $i." - ";
		else					
			echo "<a href='".$pagina."?ver=".$_GET["ver"]."&pag=".$i."'>".$i."</a> - ";
	}
	if($_GET["pag"]==0)											
		echo "Previous";
	else														
		echo "<a href='".$pagina."?pag=".($_GET["pag"]-1)."'>Previous</a>";
		
	echo "</p>";	
}


//////////////////////////////////funciones de privado///////////////////////////////////////

function creareserva()
{
	if($_POST["idpiso"])
	{
		$fecha1=$_POST["ano1"]."-".$_POST["mes1"]."-".$_POST["dia1"];
		$fecha2=$_POST["ano2"]."-".$_POST["mes2"]."-".$_POST["dia2"];
		
		if(ejecutar("insert into reservas (id_piso,correo,pedidohora,fecha1,fecha2,preciototal,personas,persona_nombre,persona_direccion,persona_ciudad,persona_pais,persona_telefono,persona_movil,persona_dni,persona_nacimiento,persona_transporte,persona_hora,persona_comentario) values (".$_POST["idpiso"].",'".$_POST["EMail"]."','-','".$fecha1."','".$fecha2."',".$_POST["precio"].",'".$_POST["adultos"]."-".$_POST["ninos"]."-0','".$_POST["Nom"]."','".$_POST["Address"]."','".$_POST["City"]."','".$_POST["Country"]."','".$_POST["Tel"]."','".$_POST["Movil"]."','".$_POST["Dnipassport"]."','".$_POST["Birthday"]."','".$_POST["Transport"]."','".$_POST["Time"]."','".$_POST["Comment"]."')"))
			echo "<p>Reserva creada</p>";
	}
	else
	{
		$lista_pisos=ejecutar("select id,nombre from pisos order by nombre asc");
		while($fila = mysql_fetch_assoc($lista_pisos))
		{
			$inputs.="<option value='".$fila["id"]."'>".$fila["nombre"]."</option>";
		}
	
		echo '<form method="post" name="creareserva" action="index.php?modo=creareserva" style="margin:20px 0 0 20px;">
		<table cellspacing="0" cellpadding="2" class="nuevo_formulario">
		<tr>
			<td>Piso:</td>
			<td><select name="idpiso" class="caja200"/>'.$inputs.'</select></td>
		</tr>
		<tr>	
			<td>Precio Total: </td>
			<td><input type="text" name="precio" class="caja200"/> <i>(sin puntos ni comas)</i></td>
		</tr>
		<tr>
			<td>Fecha llegada :</td>
			<td>
			<select name="dia1"><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
			<select name="mes1"><option value="01">'.MES1.'</option><option value="02">'.MES2.'</option><option value="03">'.MES3.'</option><option value="04">'.MES4.'</option><option value="05">'.MES5.'</option><option value="06">'.MES6.'</option><option value="07">'.MES7.'</option><option value="08">'.MES8.'</option><option value="09">'.MES9.'</option><option value="10">'.MES10.'</option><option value="11">'.MES11.'</option><option value="12">'.MES12.'</option></select>
			<select name="ano1"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option></select>
			</td>
        </tr>
		<tr>
			<td>Fecha salida:</td>
        	<td>
			<select name="dia2"><option value="01">1</option><option value="02">2</option><option value="03">3</option><option value="04">4</option><option value="05">5</option><option value="06">6</option><option value="07">7</option><option value="08">8</option><option value="09">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
			<select name="mes2"><option value="01">'.MES1.'</option><option value="02">'.MES2.'</option><option value="03">'.MES3.'</option><option value="04">'.MES4.'</option><option value="05">'.MES5.'</option><option value="06">'.MES6.'</option><option value="07">'.MES7.'</option><option value="08">'.MES8.'</option><option value="09">'.MES9.'</option><option value="10">'.MES10.'</option><option value="11">'.MES11.'</option><option value="12">'.MES12.'</option></select>
			<select name="ano2"><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option></select>
			</td>
		</tr>
		<tr>
			<td>'.NUMERO_ADULTOS.'(>10)'.ANO.'</td>
        	<td><input type="text" name="adultos" size="5" maxlength="3" value="1" /></td>
		</tr>
		<tr>	
        	<td>'.NUMERO_NINOS.':(<10)'.ANO.'</td>
        	<td><input type="text" name="ninos" size="5" maxlength="3" value="0" /></td>
		</tr>
		<tr>
			<td>'.NOMBRE.' :</td>
			<td><input type="text" name="Nom" class="caja200"/></td>
		</tr>
		<tr>	
			<td>'.DIRECCION.' :</td>
			<td><input type="text" name="Address" class="caja200"/></td>
		</tr>
		<tr>
			<td>'.POBLACION.' :</td>
			<td><input type="text" name="City" class="caja200"/></td> 
		</tr>
		<tr>
			<td>'.PAIS.' :</td>
			<td><input type="text" name="Country" class="caja200" /></td>	      	
		</tr>
		<tr>
			<td>'.TELEFONO.' :</td>
			<td><input type="text" name="Tel" maxlength="18" class="caja200" /></td>
		</tr>
		<tr>
			<td>'.MOVIL.' :</td>
			<td><input type="text" name="Movil" maxlength="18" class="caja200" /></td>                   	
		</tr>
		<tr>
			<td>'.DNI.' :</td>
			<td><input type="text" name="Dnipassport" maxlength="18" class=vcaja200" /></td> 	              	
		</tr>
		<tr>
			<td>'.EMAIL.' :</td>
			<td><input type="text" name="EMail" class="caja200" /></td>
		</tr>
		<tr>	
			<td>'.NACIMIENTO.' :</td>
			<td><input type="text" name="Birthday" size="12" maxlength="12" class="caja_m5" /></td>
		</tr>
		<tr>
			<td>'.TRANSPORTE.'</td>
			<td><select name="Transport" class="caja200"><option value="ninguno">'.TRANSPORTE_0.'</option><option value="avion">'.TRANSPORTE_1.'</option><option value="tren">'.TRANSPORTE_2.'</option><option value="barco">'.TRANSPORTE_3.'</option><option value="coche">'.TRANSPORTE_4.'</option><option value="metro">'.TRANSPORTE_5.'</option><option value="autobus">'.TRANSPORTE_6.'</option><option value="taxi">'.TRANSPORTE_7.'</option><option value="otros">'.TRANSPORTE_8.'</option></select></td>
		</tr>
		<tr>	
			<td>'.HORALLEGADA.' :</td>
			<td><input type="text" name="Time" size="12" maxlength="8" class="caja_m5" /></td>
		</tr>
		<tr>	
			<td>'.COMENTARIO.' :</td>
			<td><textarea name="Comment" rows="12" cols="80"></textarea></td>
		</tr>
		<tr>	
			<td>&nbsp;</td>
			<td><input type="submit" value="CREAR" /></td>
		</tr>
		</table>	
		</form>';
	}
}
function listar_pisos()
{
	$listado=ejecutar("select pisos.id as idpiso,pisos.id_usuario,pisos.nombre as pisonombre,pisos.tipo,pisos.ubicacion,pisos.visible,pisos.prioridad,usuarios.id,usuarios.nombre as usuarionombre from pisos,usuarios where pisos.id_usuario=usuarios.id order by pisos.id desc");
	$texto="<table cellpadding='1' cellspacing='1' class='listadopisos'><tr>
	<th>&nbsp;</th><th>Nombre</th><th>Tipo</th><th>Ubicacion</th><th>Propt.</th><th>Vis.</th><th>Prio.</th></tr>";
		
	while($fila = mysql_fetch_assoc($listado))
	{
		$texto=$texto."<tr'>";
		$texto=$texto."<td nowrap='nowrap'><a href='index.php?modo=modifpiso&id=".$fila["idpiso"]."'><img src='../img/icon_edit.png' border='0' title='Editar piso'></a> 
		                   <a href='javascript:confirmaborrar(".$fila["idpiso"].")'><img src='../img/icon_del.png' border='0' title='Borrar piso'></a> 
						   <a href='index.php?tipo=calendario&id=".$fila["idpiso"]."&nombre=".$fila["pisonombre"]."'><img src='../img/icon_calendario.png' border='0' alt='Ver calendario'></a> 
						   <a href='index.php?modo=modifcar&id=".$fila["idpiso"]."&nombre=".$fila["pisonombre"]."'><img src='../img/icon_car.png' border='0' alt='Caracteristicas'></a></td>";
		$texto=$texto."<td>".$fila["pisonombre"]."</td>";
		$texto=$texto."<td>".strtoupper(substr($fila["tipo"],0,1))."</td>";
		$texto=$texto."<td>".$fila["ubicacion"]."</td>";
		$texto=$texto."<td>".$fila["usuarionombre"]."</td>";
		$texto=$texto."<td>".constant("VISIBLE_".$fila["visible"])."</td>";		
		$texto=$texto."<td>".$fila["prioridad"]."</td>";
		$texto=$texto."</tr>";
	}
	echo $texto."</table>";
}

function nuevo_piso()
{
	$usuarios=ejecutar("select id,nombre from usuarios where tipo<>'super' order by id desc");	
	
	$texto="<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_nuevopiso' method='post' action='index.php?modo=insertar_piso' enctype='multipart/form-data'>
	<tr><td align='right'>Nombre </td><td><input type='text' name='nombre' size='44'></td></tr>
	<tr><td align='right'>Tipo </td><td><select name='tipo'><option value='apartamento'>Apartamento</option><option value='habitacion'>Habitacion</option></select></td></tr>
	<tr><td align='right'>Propietario </td><td><select name='id_usuario'>";
	
	while($fila = mysql_fetch_assoc($usuarios))
	{
		$texto=$texto."<option value='".$fila["id"]."'>".$fila["nombre"]."</option>";
	}
	
	$texto=$texto."</select></td></tr>
	<tr><td align='right'>Direccion </td><td><input type='text' name='direccion' size='44'></td></tr>
	<tr><td align='right'>Ubicacion </td><td><input type='text' name='ubicacion' size='44'></td></tr>
	<tr><td align='right'>Metros2 </td><td><input type='text' name='metros' size='5' maxlength='3'></td></tr>
	<tr><td align='right'>Capacidad </td><td><input type='text' name='capacidad1' size='2' maxlength='2'>-<input type='text' name='capacidad2' size='2' maxlength='2'> personas</td></tr>	
  	<tr><td align='right'>Estancia Minima </td><td><input type='text' name='estancia' size='4' maxlength='2'> noches</td></tr>
	<tr><td align='right'>Retraso </td><td><input type='text' name='retraso' size='4' maxlength='2'> euros</td></tr>
	<tr><td align='right'>Prioridad </td><td><input type='text' name='prioridad' size='4' maxlength='2'> (0 al 5)</td></tr>
	<tr><td align='right'>Visible </td><td><select name='visible'><option value='1'>Si</option><option value='0'>No</option></select></td></tr>
	<tr><td align='right'>Precio Limpieza </td><td><input type='text' name='precio_limpieza' size='5' maxlength='3'> euros</td></tr>	
	<tr><td align='right'>Precio por Dia </td><td><input type='text' name='precio_dia' size='4' maxlength='4'> euros</td></tr>
	<tr><td align='right'>Incremento </td><td><input type='text' name='incremento' size='4' maxlength='3'> euros/dia</td></tr>
	<tr><td align='right'>Fianza </td><td><input type='text' name='fianza' size='4' maxlength='4'> euros</td></tr>
	
	<tr><td align='right'>Numero Habitaciones/Ba&ntilde;os/Aseos </td><td><input type='text' name='num_hab' size='4' maxlength='2'>-<input type='text' name='num_bano' size='4' maxlength='2'>-<input type='text' name='num_aseo' size='4' maxlength='2'></td></tr>
	<tr><td align='right'>Numero Camas Indivi/Matrim/Sofa </td><td><input type='text' name='num_cama1' size='4' maxlength='2'>-<input type='text' name='num_cama2' size='4' maxlength='2'>-<input type='text' name='num_camasofa' size='4' maxlength='2'> </td></tr>
	<tr><td align='right'>Persona de contacto </td><td><input type='text' name='contacto_per' size='44'></td></tr>
	<tr><td align='right'>Telefono de contacto </td><td><input type='text' name='contacto_tel' size='44'></td></tr>
	<tr><td align='right'>Correo Electronico de contacto </td><td><input type='text' name='contacto_mail' size='44'></td></tr>
	<tr><td align='right'>Mapa </td><td><textarea name='mapa' cols='45' rows='11'></textarea></td></tr>	
	
	<tr><td>Descripcion(Esp) :</td><td>Descripcion(Ing) :</td></tr>
	<tr><td><textarea name='desc_esp' cols='45' rows='11'></textarea></td><td><textarea name='desc_ing' cols='45' rows='11'></textarea></td></tr>
	<tr><td>Localizacion(Esp) :</td><td>Localizacion(Ing) :</td></tr>
	<tr><td><textarea name='loc_esp' cols='45' rows='11'></textarea></td><td><textarea name='loc_ing' cols='45' rows='11'></textarea></td></tr>
	<tr><td colspan='2'>Imagen1 : <input type='file' name='imagen1' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen2 : <input type='file' name='imagen2' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen3 : <input type='file' name='imagen3' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen4 : <input type='file' name='imagen4' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen5 : <input type='file' name='imagen5' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen6 : <input type='file' name='imagen6' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen7 : <input type='file' name='imagen7' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen8 : <input type='file' name='imagen8' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen9 : <input type='file' name='imagen9' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen10 : <input type='file' name='imagen10' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen11 : <input type='file' name='imagen11' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen12 : <input type='file' name='imagen12' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen13 : <input type='file' name='imagen13' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen14 : <input type='file' name='imagen14' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen15 : <input type='file' name='imagen15' size='70'></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>Imagen16 : <input type='file' name='imagen16' size='70'></td><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td align='right'><input type='submit' value='     CREAR     '></form></td></tr>
	</table>";
	echo $texto;
}

function insertar_piso()
{
	for($i=0;$i<=16;$i++)
	{
		if($_FILES["imagen".$i]["name"])
		{
			copy($_FILES["imagen".$i]["tmp_name"],"../apartments/".$_FILES["imagen".$i]['name']);
			$filename = "../apartments/".$_FILES["imagen".$i]['name'];
			list($width, $height) = getimagesize($filename);
			$newwidth = 480;
			$newheight = floor((480 * $height) / $width); //o 320
			$thumb = imagecreatetruecolor($newwidth, $newheight);
			$source = imagecreatefromjpeg($filename);
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			imagejpeg($thumb,"../apartments/".$_FILES["imagen".$i]['name']);
			imagedestroy($thumb);
		}
	}
	$consulta="insert into pisos(id,nombre,tipo,prioridad,visible,direccion,ubicacion,metros,capacidad1,capacidad2,desc_esp,desc_ing,loc_esp,loc_ing,precio_limpieza,img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12,img13,img14,img15,img16,num_hab,num_bano,num_aseo,num_camasofa,num_cama2,num_cama1,estancia,retraso,contacto_per,contacto_mail,contacto_tel,precio_dia,fianza,incremento,mapa,id_usuario) values(null,'".$_POST["nombre"]."','".$_POST["tipo"]."','".$_POST["prioridad"]."','".$_POST["visible"]."','".$_POST["direccion"]."','".$_POST["ubicacion"]."','".$_POST["metros"]."','".$_POST["capacidad1"]."','".$_POST["capacidad2"]."','".$_POST["desc_esp"]."','".$_POST["desc_ing"]."','".$_POST["loc_esp"]."','".$_POST["loc_ing"]."','".$_POST["precio_limpieza"]."',";
	$consulta=$consulta."'".$_FILES["imagen1"]["name"]."','".$_FILES["imagen2"]["name"]."','".$_FILES["imagen3"]["name"]."','".$_FILES["imagen4"]["name"]."','".$_FILES["imagen5"]["name"]."','".$_FILES["imagen6"]["name"]."','".$_FILES["imagen7"]["name"]."','".$_FILES["imagen8"]["name"]."','".$_FILES["imagen9"]["name"]."','".$_FILES["imagen10"]["name"]."','".$_FILES["imagen11"]["name"]."','".$_FILES["imagen12"]["name"]."','".$_FILES["imagen13"]["name"]."','".$_FILES["imagen14"]["name"]."','".$_FILES["imagen15"]["name"]."','".$_FILES["imagen16"]["name"]."','".$_POST["num_hab"]."','".$_POST["num_bano"]."','".$_POST["num_aseo"]."','".$_POST["num_camasofa"]."','".$_POST["num_cama2"]."','".$_POST["num_cama1"]."','".$_POST["estancia"]."','".$_POST["retraso"]."','".$_POST["contacto_per"]."','".$_POST["contacto_mail"]."','".$_POST["contacto_tel"]."','".$_POST["precio_dia"]."','".$_POST["fianza"]."','".$_POST["incremento"]."','".$_POST["mapa"]."','".$_POST["id_usuario"]."')";
	
	if(ejecutar($consulta))
	{
		echo "<p>El registro se ha insertado correctamente.</p>";
		if(ejecutar("insert into caracteristicas set id_piso=(select max(id) from pisos)"))
			echo "<p>Las caracteristicas se ha insertado correctamente.</p>";
	}
	else
		echo "<p>ERROR</p>";		
}

function modifpiso()
{	
	$fila=mysql_fetch_assoc(ejecutar("select * from pisos where id='".$_GET["id"]."'"));
	$usuarios=ejecutar("select id,nombre from usuarios where tipo<>'super' order by id desc");
	
	$texto="<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_modpiso' method='post' action='index.php?modo=update_piso&id=".$_GET["id"]."' enctype='multipart/form-data'>
	<tr><td align='right'>Nombre :</td><td><input type='text' name='nombre' size='44' value='".$fila["nombre"]."'></td></tr>
	<tr><td align='right'>Propietario :</td><td><select name='id_usuario'>";
	
	while($lista = mysql_fetch_assoc($usuarios))
	{
		if($fila["id_usuario"]==$lista["id"])
			$texto=$texto."<option value='".$lista["id"]."' selected='selected'>".$lista["nombre"]."</option>";
		else
			$texto=$texto."<option value='".$lista["id"]."'>".$lista["nombre"]."</option>";	
	}
	
	$texto=$texto."</select></td></tr>
	<tr><td align='right'>Direccion :</td><td><input type='text' name='direccion' size='44' value='".$fila["direccion"]."'></td></tr>
	<tr><td align='right'>Ubicacion :</td><td><input type='text' name='ubicacion' size='44' value='".$fila["ubicacion"]."'></td></tr>
	<tr><td align='right'>Metros2 : </td><td><input type='text' name='metros' size='5' maxlength='3' value='".$fila["metros"]."'></td></tr>
	<tr><td align='right'>Capacidad : </td><td><input type='text' name='capacidad1' size='2' maxlength='2' value='".$fila["capacidad1"]."'>-<input type='text' name='capacidad2' size='2' maxlength='2' value='".$fila["capacidad2"]."'> personas</td></tr>
	<tr><td align='right'>Estancia Minima : </td><td><input type='text' name='estancia' value='".$fila["estancia"]."' size='4'  maxlength='2'> noches</td></tr>	
	<tr><td align='right'>Retraso : </td><td><input type='text' name='retraso' value='".$fila["retraso"]."' size='4'  maxlength='2'> euros</td></tr>
	<tr><td align='right'>Prioridad : </td><td><input type='text' name='prioridad' value='".$fila["prioridad"]."' size='4' maxlength='2'> (0 al 5)</td></tr>	
	<tr><td align='right'>Visible : </td><td><select name='visible'>";
	
	if($fila["visible"])	
		$texto=$texto."<option value='1' selected='selected'>Si</option><option value='0'>No</option>";
	else
		$texto=$texto."<option value='1'>Si</option><option value='0' selected='selected'>No</option>";
	
	//en los pisos sin imagen solo un boton cambiar, si tiene imagen boton borrar
		
	$texto=$texto."</select></td></tr>
	
	<tr><td align='right'>Precio Limpieza :</td><td><input type='text' name='precio_limpieza' size='5' maxlength='3' value='".$fila["precio_limpieza"]."'> euros</td></tr>
	<tr><td align='right'>Precio por Dia :</td><td><input type='text' name='precio_dia' value='".$fila["precio_dia"]."' size='4' maxlength='4'> euros</td></tr>
	<tr><td align='right'>Incremento :</td><td><input type='text' name='incremento' value='".$fila["incremento"]."' size='4' maxlength='4'> euros por dia</td></tr>
	<tr><td align='right'>Fianza :</td><td><input type='text' name='fianza' value='".$fila["fianza"]."' size='4' maxlength='4'> euros</td></tr>		

	<tr><td align='right'>Numero Habitaciones/Ba&ntilde;os/Aseos :</td><td><input type='text' name='num_hab' value='".$fila["num_hab"]."' size='4'  maxlength='2'>-<input type='text' name='num_bano' value='".$fila["num_bano"]."' size='4' maxlength='2'>-<input type='text' name='num_aseo' value='".$fila["num_aseo"]."' size='4' maxlength='2'></td></tr>
	<tr><td align='right'>Numero Camas Indivi/Matrim/Sofa :</td><td><input type='text' name='num_cama1' value='".$fila["num_cama1"]."' size='4'  maxlength='2'>-<input type='text' name='num_cama2' value='".$fila["num_cama2"]."' size='4' maxlength='2'>-<input type='text' name='num_camasofa' value='".$fila["num_camasofa"]."' size='4' maxlength='2'></td></tr>
	<tr><td align='right'>Persona de contacto :</td><td><input type='text' name='contacto_per' value='".$fila["contacto_per"]."' size='44'></td></tr>
	<tr><td align='right'>Telefono de contacto :</td><td><input type='text' name='contacto_tel' value='".$fila["contacto_tel"]."' size='44'></td></tr>
	<tr><td align='right'>Correo Electronico de contacto :</td><td><input type='text' name='contacto_mail' value='".$fila["contacto_mail"]."' size='44'></td></tr>
	<tr><td align='right'>Mapa </td><td><textarea name='mapa' size='44' cols='45' rows='11'>".$fila["mapa"]."</textarea></td></tr>
	
	<tr><td>Descripcion(Esp) :</td><td>Descripcion(Ing) :</td></tr>
	<tr><td><textarea name='desc_esp' cols='45' rows='11'>".$fila["desc_esp"]."</textarea></td><td><textarea name='desc_ing' cols='45' rows='11'>".$fila["desc_ing"]."</textarea></td></tr>
	<tr><td>Localizacion(Esp) :</td><td>Localizacion(Ing) :</td></tr>
	<tr><td><textarea name='loc_esp' cols='45' rows='11'>".$fila["loc_esp"]."</textarea></td><td><textarea name='loc_ing' cols='45' rows='11'>".$fila["loc_ing"]."</textarea></td></tr>
	
	<tr>
		<td colspan='2' align='center'>
		<table cellspacing='0' cellpadding='8' align='center'>
		<tr>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img1"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img1"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img2"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img2"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img3"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img3"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img4"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img4"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg&num=1&id=".$fila["id"]."&nombre=".$fila["img1"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=1&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=2&id=".$fila["id"]."&nombre=".$fila["img2"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=2&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=3&id=".$fila["id"]."&nombre=".$fila["img3"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=3&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=4&id=".$fila["id"]."&nombre=".$fila["img4"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=4&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		<tr>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img5"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img5"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img6"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img6"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img7"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img7"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img8"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img8"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg&num=5&id=".$fila["id"]."&nombre=".$fila["img5"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=5&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=6&id=".$fila["id"]."&nombre=".$fila["img6"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=6&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=7&id=".$fila["id"]."&nombre=".$fila["img7"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=7&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=8&id=".$fila["id"]."&nombre=".$fila["img8"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=8&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		
		<tr>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img9"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img9"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img10"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img10"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img11"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img11"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img12"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img12"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg&num=9&id=".$fila["id"]."&nombre=".$fila["img9"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=9&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=10&id=".$fila["id"]."&nombre=".$fila["img10"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=10&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=11&id=".$fila["id"]."&nombre=".$fila["img11"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=11&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=12&id=".$fila["id"]."&nombre=".$fila["img12"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=12&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		<tr>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img13"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img13"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img14"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img14"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img15"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img15"]."' width='120' height='120' border='0'></a></td>
			<td><a href='http://www.sleepbcn.com/apartments/".$fila["img16"]."' target='_blank'><img src='http://www.sleepbcn.com/apartments/".$fila["img16"]."' width='120' height='120' border='0'></a></td>
		</tr>
		<tr>
			<td><a href='index.php?modo=borraimg&num=13&id=".$fila["id"]."&nombre=".$fila["img13"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=13&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=14&id=".$fila["id"]."&nombre=".$fila["img14"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=14&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=15&id=".$fila["id"]."&nombre=".$fila["img15"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=15&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
			<td><a href='index.php?modo=borraimg&num=16&id=".$fila["id"]."&nombre=".$fila["img16"]."'><img src='../img/icon_del.png' border='0' title='Borrar Imagen'> Borrar imagen</a><br><a href='index.php?modo=editimg&num=16&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0' title='Cambiar Imagen'> Cambiar imagen</a</td>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td align='right'><input type='submit' value='     ACTUALIZAR    '></form></td></tr>
	</table>";
	echo $texto;
}

function update_piso()
{	
	$consulta="update pisos set nombre='".$_POST["nombre"]."',id_usuario='".$_POST["id_usuario"]."',prioridad='".$_POST["prioridad"]."',visible='".$_POST["visible"]."',direccion='".$_POST["direccion"]."',ubicacion='".$_POST["ubicacion"]."',metros='".$_POST["metros"]."',capacidad1='".$_POST["capacidad1"]."',capacidad2='".$_POST["capacidad2"]."',desc_esp='".$_POST["desc_esp"]."',desc_ing='".$_POST["desc_ing"]."',loc_esp='".$_POST["loc_esp"]."',loc_ing='".$_POST["loc_ing"]."',precio_limpieza='".$_POST["precio_limpieza"]."',incremento=".$_POST["incremento"].",";
	$consulta=$consulta."num_hab=".$_POST["num_hab"].",num_bano=".$_POST["num_bano"].",num_aseo=".$_POST["num_aseo"].",num_cama1=".$_POST["num_cama1"].",num_cama2=".$_POST["num_cama2"].",num_camasofa=".$_POST["num_camasofa"].",estancia=".$_POST["estancia"].",retraso=".$_POST["retraso"].",precio_dia='".$_POST["precio_dia"]."',fianza='".$_POST["fianza"]."',contacto_per='".$_POST["contacto_per"]."',contacto_tel='".$_POST["contacto_tel"]."',contacto_mail='".$_POST["contacto_mail"]."',mapa='".$_POST["mapa"]."' where id='".$_GET["id"]."'";
	
	if(ejecutar($consulta))
		echo "<script>window.open('index.php?modo=modifpiso&id=".$_GET["id"]."','_self')</script>";	
	else
		echo "<p>ERROR</p>";	
}

function borrapiso()
{
	if(ejecutar("delete from pisos where id='".$_GET["id"]."'"))
	{
		echo "<p>El registro se ha eliminado correctamente.</p>";
		if(ejecutar("delete from caracteristicas where id_piso='".$_GET["id"]."'"))
			echo "<p>Las caracteristicas se ha eliminado correctamente.</p>";
	}
	else
		echo "<p>ERROR</p>";	
}

function editimg()
{
	//muestra un input file para cambiar una imagen con el name segun el numero de la imagen
	
	echo "<form name='form_newimg' method='post' action='index.php?modo=insertar_img&id=".$_GET["id"]."' enctype='multipart/form-data' class='nuevo_formulario'>
	<input type='file' name='imagen".$_GET["num"]."' size='70'>
	<input type='hidden' name='oculto' value='".$_GET["num"]."'><br>
	<input type='submit' value='   ACEPTAR    '>
	</form>";
}
function insertar_img()
{
	//sube la nueva imagen y hace update del nombre del archivo en la basededatos
	
	copy($_FILES["imagen".$_POST["oculto"]]["tmp_name"],"../apartments/".$_FILES["imagen".$_POST["oculto"]]['name']);
	
	$filename = "../apartments/".$_FILES["imagen".$_POST["oculto"]]['name'];
	list($width, $height) = getimagesize($filename);
	$newwidth = 480;
	$newheight = floor((480 * $height) / $width); //o 320
	$thumb = imagecreatetruecolor($newwidth, $newheight);
	$source = imagecreatefromjpeg($filename);
	imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	imagejpeg($thumb,"../apartments/".$_FILES["imagen".$_POST["oculto"]]['name']);
	imagedestroy($thumb);
	
	if(ejecutar("update pisos set img".$_POST["oculto"]."='".$_FILES["imagen".$_POST["oculto"]]['name']."' where id='".$_GET["id"]."'"))
		echo "<script>window.open('http://www.sleepbcn.com/privado/index.php?modo=modifpiso&id=".$_GET["id"]."','_self')</script>";	
	else
		echo "<p>ERROR</p>";
}

function borraimg()
{
	unlink("../apartments/".$_GET["nombre"]);

	if(ejecutar("update pisos set img".$_GET["num"]."='' where id='".$_GET["id"]."'"))
		echo "<script>window.open('http://www.sleepbcn.com/privado/index.php?modo=modifpiso&id=".$_GET["id"]."','_self')</script>";	
	else
		echo "<p>ERROR</p>";	
}

function modifcar()
{
	$fila=mysql_fetch_assoc(ejecutar("select * from caracteristicas where id_piso='".$_GET["id"]."'"));
	
	$texto="<h4>Caracteristicas de piso: ".$_GET["nombre"]."</h4>
	<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_modcar' method='post' action='index.php?modo=update_car&id=".$_GET["id"]."'>
	
	<tr><td>TV</td><td><input type='checkbox' name='tv' ";
	if($fila["tv"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>TV Satelite</td><td><input type='checkbox' name='tv_sat' ";
	if($fila["tv_sat"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Video</td><td><input type='checkbox' name='video' ";
	if($fila["video"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>DVD</td><td><input type='checkbox' name='dvd' ";
	if($fila["dvd"])
		$texto=$texto."checked='checked'";

	$texto=$texto."></td></tr><tr><td>Hi-Fi</td><td><input type='checkbox' name='hifi' ";
	if($fila["hifi"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Aire Acondicionado</td><td><input type='checkbox' name='aire' ";
	if($fila["aire"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Calefaccion</td><td><input type='checkbox' name='calefaccion' ";
	if($fila["calefaccion"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>Ventilador</td><td><input type='checkbox' name='ventilador' ";
	if($fila["ventilador"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Nevera</td><td><input type='checkbox' name='nevera' ";
	if($fila["nevera"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>Lavadora</td><td><input type='checkbox' name='lavadora' ";
	if($fila["lavadora"])
		$texto=$texto."checked='checked'";	
	
	$texto=$texto."></td></tr><tr><td>Secadora</td><td><input type='checkbox' name='secadora' ";
	if($fila["secadora"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td><td>Lavavajillas</td><td><input type='checkbox' name='lavavajillas' ";
	if($fila["lavavajillas"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Horno</td><td><input type='checkbox' name='horno' ";
	if($fila["horno"])
		$texto=$texto."checked='checked'";			
	
	$texto=$texto."></td><td>Microondas</td><td><input type='checkbox' name='microondas' ";
	if($fila["microondas"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Tostadora</td><td><input type='checkbox' name='tostadora' ";
	if($fila["tostadora"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Cafetera</td><td><input type='checkbox' name='cafetera' ";
	if($fila["cafetera"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Exprimidor</td><td><input type='checkbox' name='exprimidor' ";
	if($fila["exprimidor"])
		$texto=$texto."checked='checked'";
	
	$texto=$texto."></td><td>Plancha</td><td><input type='checkbox' name='plancha' ";
	if($fila["plancha"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Secador</td><td><input type='checkbox' name='secador' ";
	if($fila["secador"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Ascensor</td><td><input type='checkbox' name='ascensor' ";
	if($fila["ascensor"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Minusvalidos</td><td><input type='checkbox' name='minusvalidos' ";
	if($fila["minusvalidos"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Terraza</td><td><input type='checkbox' name='terraza' ";
	if($fila["terraza"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Balcon</td><td><input type='checkbox' name='balcon' ";
	if($fila["balcon"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Parking</td><td><input type='checkbox' name='parking' ";
	if($fila["parking"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Garaje</td><td><input type='checkbox' name='garaje' ";
	if($fila["garaje"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Jardin</td><td><input type='checkbox' name='jardin' ";
	if($fila["jardin"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Piscina</td><td><input type='checkbox' name='piscina' ";
	if($fila["piscina"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Internet</td><td><input type='checkbox' name='internet' ";
	if($fila["internet"])
		$texto=$texto."checked='checked'";
	
	$texto=$texto."></td></tr><tr><td>Fumadores</td><td><input type='checkbox' name='fumadores' ";
	if($fila["fumadores"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Ropa de Cama</td><td><input type='checkbox' name='ropacama' ";
	if($fila["ropacama"])
		$texto=$texto."checked='checked'";
		
	$texto=$texto."></td></tr><tr><td>Toallas</td><td><input type='checkbox' name='toallas' ";
	if($fila["toallas"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td><td>Mascotas</td><td><input type='checkbox' name='mascotas' ";
	if($fila["mascotas"])
		$texto=$texto."checked='checked'";	
		
	$texto=$texto."></td></tr><tr><td>Cuna Bebes</td><td><input type='checkbox' name='cuna' ";
	if($fila["cuna"])
		$texto=$texto."checked='checked'";																					
		
	$texto=$texto."></td><td>-</td></tr><tr><td colspan='2' align='right'><input type='submit' value='     ACTUALIZAR     '>
	</form></td></tr></table>";
	
	echo $texto;
}
function update_car()
{
	$tv=($_POST["tv"])? 					1:0;
	$tv_sat=($_POST["tv_sat"])? 			1:0;
	$video=($_POST["video"])?				1:0;
	$dvd=($_POST["dvd"])? 					1:0;
	$hifi=($_POST["hifi"])?					1:0;
	$aire=($_POST["aire"])?					1:0;
	$calefaccion=($_POST["calefaccion"])?	1:0;	
	$ventilador=($_POST["ventilador"])?		1:0;
	$nevera=($_POST["nevera"])?				1:0;
	$lavadora=($_POST["lavadora"])?			1:0;	
	$secadora=($_POST["secadora"])?			1:0;
	$lavavajillas=($_POST["lavavajillas"])?	1:0;
	$horno=($_POST["horno"])?				1:0;	
	$microondas=($_POST["microondas"])?		1:0;
	$tostadora=($_POST["tostadora"])?		1:0;	
	$cafetera=($_POST["cafetera"])?			1:0;	
	$exprimidor=($_POST["exprimidor"])?		1:0;
	$plancha=($_POST["plancha"])?			1:0;	
	$secador=($_POST["secador"])?			1:0;
	$ascensor=($_POST["ascensor"])?			1:0;
	$minusvalidos=($_POST["minusvalidos"])?	1:0;
	$terraza=($_POST["terraza"])?			1:0;	
	$balcon=($_POST["balcon"])?				1:0;
	$parking=($_POST["parking"])?			1:0;
	$garaje=($_POST["garaje"])?				1:0;
	$jardin=($_POST["jardin"])?				1:0;
	$piscina=($_POST["piscina"])?			1:0;
	$internet=($_POST["internet"])?			1:0;
	$fumadores=($_POST["fumadores"])?		1:0;	
	$ropacama=($_POST["ropacama"])?			1:0;
	$toallas=($_POST["toallas"])?			1:0;	
	$mascotas=($_POST["mascotas"])?			1:0;	
	$cuna=($_POST["cuna"])?					1:0;	
		
	if(ejecutar("update caracteristicas set tv='".$tv."',tv_sat='".$tv_sat."',video='".$video."',dvd='".$dvd."',hifi='".$hifi."',aire='".$aire."',calefaccion='".$calefaccion."',ventilador='".$ventilador."',nevera='".$nevera."',lavadora='".$lavadora."',secadora='".$secadora."',lavavajillas='".$lavavajillas."',horno='".$horno."',microondas='".$microondas."',tostadora='".$tostadora."',cafetera='".$cafetera."',exprimidor='".$exprimidor."',plancha='".$plancha."',secador='".$secador."',ascensor='".$ascensor."',minusvalidos='".$minusvalidos."',terraza='".$terraza."',balcon='".$balcon."',parking='".$parking."',garaje='".$garaje."',jardin='".$jardin."',piscina='".$piscina."',internet='".$internet."',fumadores='".$fumadores."',ropacama='".$ropacama."',toallas='".$toallas."',mascotas='".$mascotas."',cuna='".$cuna."' where id_piso=".$_GET["id"]))
	{
		echo "<script>window.alert('Las caracteristicas se ha actualizado correctamente.');
		window.open('index.php?modo=listar_pisos','_self');</script>";	
	}		
}

function listar_usuarios()
{
	$listado=ejecutar("select id,nombre,login,email,tipo,telefono from usuarios where tipo<>'super'  order by id desc");
	$texto="<table cellpadding='1' cellspacing='1' class='listadopisos'><tr>
	<th>&nbsp;</th><th>NOMBRE</td><th>LOGIN</th><th>EMAIL</th><th>TELEFONO</th><th>TIPO</th></tr>";
		
	while($fila = mysql_fetch_assoc($listado))
	{
		$texto=$texto."<tr'>";
		$texto=$texto."<td><a href='index.php?modo=modifusuario&id=".$fila["id"]."'><img src='../img/icon_edit.png' border='0'></a> <a href='javascript:confirmaborrar2(".$fila["id"].")'><img src='../img/icon_del.png' border='0'></a></td>";
    	$texto=$texto."<td>".$fila["nombre"]."</td>";
		$texto=$texto."<td>".$fila["login"]."</td>";
		$texto=$texto."<td>".$fila["email"]."</td>";
		$texto=$texto."<td>".$fila["telefono"]."</td>";
		$texto=$texto."<td>".$fila["tipo"]."</td>";
		$texto=$texto."</tr>";
	}
	$texto=$texto."</table>";
	echo $texto;
}

function nuevo_usuario()
{
	echo "<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_nuevousuario' method='post' action='index.php?modo=insertar_usuario'>
	<tr><td>Tipo :</td><td><select name='tipo'><option value='sleepbcn'>Sleep BCN</option><option value='rural'>Rural</option></select></td></tr>
	<tr><td>Login :</td><td><input type='text' name='login' size='44'></td></tr>
	<tr><td>Password :</td><td><input type='text' name='password' size='44'></td></tr>
	<tr><td>Nombre y Apellidos :</td><td><input type='text' name='nombre' size='44'></td></tr>
	<tr><td>D.N.I.:</td><td><input type='text' name='dni' size='44'></td></tr>
	<tr><td>Email :</td><td><input type='text' name='email' size='44'></td></tr>
	<tr><td>Email 2:</td><td><input type='text' name='email2' size='44'></td></tr>
	<tr><td>Telefono :</td><td><input type='text' name='telefono' size='44'></td></tr>
	<tr><td>Telefono 2:</td><td><input type='text' name='telefono2' size='44'></td></tr>
	<tr><td>Cuenta Bancaria:</td><td><input type='text' name='cuenta' size='44'></td></tr>
	<tr><td>Comentario:</td><td><textarea name='comentario' rows='15' cols='75'></textarea></td></tr>
	<tr><td>&nbsp;</td><td align='right'><input type='submit' value='     CREAR     '></form></td></tr>
	</table>";
}
function insertar_usuario()
{
	if(ejecutar("insert into usuarios(id,tipo,login,password,nombre,dni,email,email2,telefono,telefono2,cuenta,comentario) values(null,'".$_POST["tipo"]."','".$_POST["login"]."','".$_POST["password"]."','".$_POST["nombre"]."','".$_POST["dni"]."','".$_POST["email"]."','".$_POST["email2"]."','".$_POST["telefono"]."','".$_POST["telefono2"]."','".$_POST["cuenta"]."','".$_POST["comentario"]."')"))
		echo "<p>El usuario se ha insertado correctamente.</p>";
	else
		echo "<p>ERROR</p>";
}
function modifusuario()
{
	$fila=mysql_fetch_assoc(ejecutar("select * from usuarios where id='".$_GET["id"]."'"));
	
	$texto="<table cellspacing='0' cellpadding='2' class='nuevo_formulario'>
	<form name='form_modusuario' method='post' action='index.php?modo=update_usuario&id=".$_GET["id"]."'>
	<tr><td>Login :</td><td><input type='text' name='login' size='44' value='".$fila["login"]."'></td></tr>
	<tr><td>Password :</td><td><input type='text' name='password' size='44' value='".$fila["password"]."'></td></tr>
	<tr><td>Nombre y Apellidos :</td><td><input type='text' name='nombre' size='44' value='".$fila["nombre"]."'></td></tr>
	<tr><td>D.N.I.:</td><td><input type='text' name='dni' size='44' value='".$fila["dni"]."'></td></tr>
	<tr><td>Email :</td><td><input type='text' name='email' size='44' value='".$fila["email"]."'></td></tr>
	<tr><td>Email 2:</td><td><input type='text' name='email2' size='44' value='".$fila["email2"]."'></td></tr>
	<tr><td>Telefono :</td><td><input type='text' name='telefono' size='44' value='".$fila["telefono"]."'></td></tr>
	<tr><td>Telefono 2:</td><td><input type='text' name='telefono2' size='44' value='".$fila["telefono2"]."'></td></tr>
	<tr><td>Cuenta Bancaria:</td><td><input type='text' name='cuenta' size='44' value='".$fila["cuenta"]."'></td></tr>
	<tr><td>Comentario:</td><td><textarea name='comentario' rows='15' cols='75'>".$fila["comentario"]."</textarea></td></tr>
	<tr><td>&nbsp;</td><td align='right'><input type='submit' value='     ACTUALIZAR     '></form></td></tr>
	</table>";	
	echo $texto;
}
function update_usuario()
{
	if(ejecutar("update usuarios set login='".$_POST["login"]."',password='".$_POST["password"]."',nombre='".$_POST["nombre"]."',dni='".$_POST["dni"]."',email='".$_POST["email"]."',email2='".$_POST["email2"]."',telefono='".$_POST["telefono"]."',telefono2='".$_POST["telefono2"]."',cuenta='".$_POST["cuenta"]."',comentario='".$_POST["comentario"]."' where id='".$_GET["id"]."'"))
		echo "<p>El registro se ha actualizado correctamente.</p>";
	else
		echo "<p>ERROR</p>";	
}
function borrausuario()
{
	if(ejecutar("delete from usuarios where id='".$_GET["id"]."'"))
		echo "<p>El registro se ha eliminado correctamente.</p>";
	else
		echo "<p>ERROR</p>";		
}

function textoabre()
{
	//abre el archivo faq/bcn espa�ol/ingles y lo muestra en un textarea

	$abierto=fopen("../idiomas/".$_GET["cual"]."_".$_GET["idioma"].".htm","r");
	$archivo=fread($abierto,filesize("../idiomas/".$_GET["cual"]."_".$_GET["idioma"].".htm"));
	fclose($abierto);
	
	$texto="<p>Texto ".strtoupper($_GET["cual"])." ".strtoupper($_GET["idioma"])."</p>
	<form name='form_txt' method='post' action='index.php?modo=textomod&cual=".$_GET["cual"]."&idioma=".$_GET["idioma"]."' class='nuevo_formulario'>
	<textarea name='textofile' rows='35' cols='100'>".strip_tags($archivo)."</textarea><br>
	<input type='submit' value='   ACTUALIZAR   '></form>";
	echo $texto;
}
function textomod()
{
	$abierto=fopen("../idiomas/".$_GET["cual"]."_".$_GET["idioma"].".htm","w");
	if(fwrite($abierto,str_replace("\r","<br>",$_POST["textofile"])))
		echo "<p>El texto se ha modificado correctamente</p>";
	else
		echo "<p>ERROR</p>";
	fclose($abierto);
}
function documentos()
{
	echo "<iframe frameborder='0' width='800' height='500' src='http://www.sleepbcn.com/privado/ftp/index.php'></iframe>";
}
function informes()
{
	if($_GET["dia"])
	{
		$temp_dia = ($_GET["dia"]<10)? "0".$_GET["dia"]:$_GET["dia"];
	
		$quereserva=mysql_result(ejecutar("select id from reservas where id_piso='".$_GET["idpiso"]."' and fecha1 <= '".$_GET["ano"]."-".$_GET["mes"]."-".$temp_dia."' and fecha2 >= '".$_GET["ano"]."-".$_GET["mes"]."-".$temp_dia."'"),0);
		
		echo "<script>window.open('index.php?modo=modifreserva&id=".$quereserva."','_self')</script>";
	}
	else
	{
		echo "<form method='get' action='index.php' style='margin:10px 0 -10px 20px;'>
		<input type='hidden' name='modo' value='informes' />
		<select name='mes'><option value='01'>Enero</option><option value='02'>Febrero</option><option value='03'>Marzo</option><option value='04'>Abril</option><option value='05'>Mayo</option><option value='06'>Junio</option><option value='07'>Julio</option><option value='08'>Agosto</option><option value='09'>Septiembre</option><option value='10'>Octubre</option><option value='11'>Noviembre</option><option value='12'>Diciembre</option></select>
		<select name='ano'><option value='2008'>2008</option><option value='2009'>2009</option><option value='2010'>2010</option></select>
		<input type='submit' style='margin:0 0 0 100px;' value=' BUSCAR ' />
		</form>";
		
		if($_GET["mes"] && $_GET["ano"])
		{
			$listado=ejecutar("select id,nombre from pisos order by nombre asc");
		
			$texto="<h2>&nbsp;&nbsp;&nbsp;INFORME Mes: ".$_GET["mes"]."&nbsp;&nbsp;&nbsp; A&ntilde;o: ".$_GET["ano"]."</h2>
			<table cellpadding='1' cellspacing='1' style='margin:20px 0 20px 20px;'>
			<tr><th>ID</th><th width='400'>NOMBRE</th><th colspan='31'>DIA</th></tr>";
			
			while($fila = mysql_fetch_assoc($listado))
			{
				$texto=$texto."<tr>";
				$texto=$texto."<td>".$fila["id"]."</td>";
				$texto=$texto."<td>".$fila["nombre"]."</td>";
				
				for($i=1;$i<=31;$i++)
				{
					if($i<10)
						$dias=ejecutar("select estado,autor from calendario where id_piso='".$fila["id"]."' and dia='".$_GET["ano"]."-".$_GET["mes"]."-0".$i."' ");
					else
						$dias=ejecutar("select estado,autor from calendario where id_piso='".$fila["id"]."' and dia='".$_GET["ano"]."-".$_GET["mes"]."-".$i."' ");
					
					$fila2 = mysql_fetch_assoc($dias);
					
					if($fila2["estado"]=="ocupado" && $fila2["autor"]>0)
						$texto=$texto."<td class='dia_ocupado'><a href='index.php?modo=informes&idpiso=".$fila["id"]."&ano=".$_GET["ano"]."&mes=".$_GET["mes"]."&dia=".$i."' target='_blank'>".$i."</a></td>";
					else
						$texto=$texto."<td class='dia_".$fila2["estado"]."'>".$i."</td>";
				}
				$texto=$texto."</tr>";
			}
			echo $texto."</table>";
		}
	}
}
////////////////////////////////////funciones de propietario///////////////////////////////////////////

function mis_pisos()
{	
	$listado=ejecutar("select * from pisos where id_usuario='".$_SESSION["idnombre"]."' order by id desc");
	$texto="<table cellpadding='1' cellspacing='1' class='listadopisos'><tr>
	<td>&nbsp;</td><td>NOMBRE</td><td>UBICACION</td><td>METROS</td></tr>";
		
	while($fila = mysql_fetch_assoc($listado))
	{
		$texto=$texto."<tr'>";
		$texto=$texto."<td><a href='index.php?tipo=calendario&id=".$fila["id"]."&nombre=".$fila["nombre"]."'><img src='../img/icon_calendario.png' border='0' alt='Ver calendario'></a></td>";
		$texto=$texto."<td>".$fila["nombre"]."</td>";
		$texto=$texto."<td>".$fila["ubicacion"]."</td>";
		$texto=$texto."<td>".$fila["metros"]."</td>";
		$texto=$texto."</tr>";
	}
	$texto=$texto."</table>";
	echo $texto;
}

function colorear($id,$diactual)
{
	//devuelve una celda con color si el diactual esta en la base de datos
	
	$dias=ejecutar("select dia,estado,autor from calendario where id_piso='".$id."' and dia='".$diactual."' ");
	$filadia=mysql_fetch_row($dias);
	
	$autor="";
	if($_SESSION['autentificacion']>0)
	{
		if($filadia[2]=="2")	$autor="*";
		if($filadia[2]=="3")	$autor="-";
	}
	if ($filadia[0]==$diactual)
	{
		if(($filadia[1]=="ocupado_x1")&&($_SESSION['autentificacion']>0))
			return "<td class='tcelda_r1'><span style='color:#000000;'>".$autor."</span>";
		else
			return "<td class='".$filadia[1]."'><span style='color:#000000;'>".$autor."</span>";
	}
	return "<td class='tcelda'>";
}
function calendario()
{
	//muestra el calendario privado y el formulario para cambiar el estado de los dias
	
	echo "<h4>Calendario de piso: ".$_GET["nombre"]."</h4>";
	echo "<iframe src='http://www.sleepbcn.com/privado/calendario.php?id=".$_GET["id"]."' frameborder='0' scrolling='no'></iframe>
	<p style='margin-top:-60px;'><img src='http://www.sleepbcn.com/img/leyenda_privado.gif' hspace='20'></p>";
	echo "<form method='post' action='index.php?tipo=ejecuta_calendario&nombre=".$_GET["nombre"]."' name='form1'>
	<table cellspacing='0' cellpadding='2' class='nuevo_formulario'><tr>
	<td>
		Fecha inicial del intervalo <input type='text' name='inicio1' size='4' readonly='readonly'>-<input type='text' name='inicio2' size='4' readonly='readonly'>-<input type='text' name='inicio3' size='4' readonly='readonly'><br>
		Fecha final del intervalo &nbsp;&nbsp;<input type='text' name='final1' size='4' readonly='readonly'>-<input type='text' name='final2' size='4' readonly='readonly'>-<input type='text' name='final3' size='4' readonly='readonly'>
	</td>
	<td>
		<input type='radio' name='estado' value='liberar'> Liberar<br>
    	<input type='radio' name='estado' value='ocupado' checked='checked'> Ocupar<br>
    	<input type='radio' name='estado' value='oferta15'> Oferta 15%<br>
    	<input type='radio' name='estado' value='oferta20'> Oferta 20%<br>
    	<input type='radio' name='estado' value='oferta25'> Oferta 25%<br>
	</td>
	</tr></table>
	<p>
	<input type='hidden' name='idenpiso' value='".$_GET["id"]."'>
	<input type='reset' value='      Borrar      '>
    <input type='submit' value=' Confirmar '></p></form>";
}

function ejecuta_calendario()
{
	//calcula dias intermedios, datstart dia de inicio

	$date1 = mktime(0,0,0,$_POST["inicio2"], $_POST["inicio1"], $_POST["inicio3"]);
	$date2 = mktime(0,0,0,$_POST["final2"], $_POST["final1"], $_POST["final3"]);
	
	$numerodias=round(($date2 - $date1) / (60 * 60 * 24));
	
	$comprobar=ejecutar("select * from calendario where dia between '".$_POST["inicio3"]."-".$_POST["inicio2"]."-".$_POST["inicio1"]."' and '".$_POST["final3"]."-".$_POST["final2"]."-".$_POST["final1"]."' and id_piso=".$_POST["idenpiso"]." ");

	while($fila = mysql_fetch_assoc($comprobar))
	{
		if(($fila["autor"]==2 || $fila["autor"]==3) && ($_SESSION['autentificacion']!=2)) //si un propietario intenta cambiar lo de un admin o cliente
			$numerodias=-5;
		if(($fila["estado"]=="ocupado" || $fila["estado"]=="ocupado_x1") && ($_POST["estado"]=="oferta15" || $_POST["estado"]=="oferta20" || $_POST["estado"]=="oferta25"))	//si se intenta cambiar de rojo a
			$numerodias=-5;			
	}	
	if($numerodias<0)
	{
		echo "<script>alert('No se pueden cambiar esas fechas');</script>";
	}
	else
	{
		$datstart = $_POST["inicio3"]."-".$_POST["inicio2"]."-".$_POST["inicio1"]; 
		$rep = $numerodias;
		$dia0=date("Y-m-d", mktime(0,0,0,$_POST["inicio2"],$_POST["inicio1"],$_POST["inicio3"]));
	
	
		//primero hace un delete de los dias y luego si es ocupar o liberar un insert
		ejecutar("delete from calendario where id_piso='".$_POST["idenpiso"]."' and dia='".$dia0."' ");

		if($_POST["estado"]!="liberar")
		{
			if($_POST["estado"]=="ocupado")
				ejecutar("insert into calendario (id_piso,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dia0."','".$_SESSION['autentificacion']."','ocupado_x1')");
			else
				ejecutar("insert into calendario (id_piso,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dia0."','".$_SESSION['autentificacion']."','".$_POST["estado"]."')");	
		}
		//ejecuta el resto de consultas
	
		//si es ocupar el ultimo dia lo deja libre
		$hasta=1;
		if($_POST["estado"]=="ocupado")
			$hasta=2;
		
		while ($rep >= $hasta) 
		{
			$datyy=substr($datstart,0,4);
			$datmm=substr($datstart,5,2);
			$datdd=substr($datstart,8,2);
			$fda=$datdd + 1;
			$fmo=$datmm + 0;
			$fyr=$datyy + 0;
			$dat1=date("Y-m-d", mktime(0,0,0,$fmo,$fda,$fyr)); 

			ejecutar("delete from calendario where id_piso='".$_POST["idenpiso"]."' and dia='".$dat1."' ");

			if($_POST["estado"]!="liberar")
				ejecutar("insert into calendario (id_piso,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dat1."','".$_SESSION['autentificacion']."','".$_POST["estado"]."')");
		
			$datstart=$dat1; 
			$rep--; 
		}
		//ultimo query poner while rep >=2
		//$dat1=date("Y-m-d", mktime(0,0,0,$fmo,($fda+1),$fyr)); 
		//echo "ultimo <br> insert into calendario (id_piso,dia,autor,estado) values('".$_POST["idenpiso"]."','".$dat1."','".$_SESSION['autentificacion']."','".$_POST["estado"]."') <br>"; 
	}	
	echo "<script>window.open('index.php?tipo=calendario&id=".$_POST["idenpiso"]."&nombre=".$_GET["nombre"]."','_self')</script>";
}

function ver_reservas()
{
	$ano=(!$_POST["ano"])?	"2009":$_POST["ano"];	

	$listado=ejecutar("select reservas.id as reservaid,reservas.id_piso,reservas.pedidohora,reservas.fecha1,reservas.fecha2,reservas.preciototal,reservas.correo,pisos.id as pisoid,pisos.nombre as pisonombre from reservas,pisos where reservas.id_piso=pisos.id and reservas.fecha1 like '".$ano."%' order by reservaid desc");
	$texto="
	<form method='post' action='index.php?modo=ver_reservas' style='margin:10px 0 -10px 20px;'>
	A&ntilde;o : <select name='ano'><option value='2008'>2008</option><option value='2009' selected='selected'>2009</option><option value='2010'>2010</option></select>
	<input type='submit' style='margin:0 0 0 100px;' value=' BUSCAR ' />
	</form>
		
	<table cellpadding='2' cellspacing='1' class='listadopisos'><tr><td>&nbsp;</td>
	<td><b>ID</b></td><td><b>PISO</b></td><td><b>PEDIDOHORA</b></td><td><b>F.ENTRADA</b></td><td><b>F.SALIDA</b></td><td><b>PRECIO</b></td><td><b>EMAIL</b></td></tr>";
		
	while($fila = mysql_fetch_assoc($listado))
	{
		$texto=$texto."<tr>";
		$texto=$texto."<td nowrap='nowrap'><a href='index.php?modo=modifreserva&id=".$fila["reservaid"]."'><img src='../img/icon_edit.png' border='0' title='Ver'></a> <a href='javascript:reservaborrar(".$fila["reservaid"].")'><img src='../img/icon_del.png' border='0' title='Borrar Reserva'></a> <a href='javascript:confirmavalidar(".$fila["reservaid"].")'><img src='../img/icon_ok.png' border='0' title='Validar Reserva'></a></td>";
    	$texto=$texto."<td>".$fila["reservaid"]."</td>";
		$texto=$texto."<td>".$fila["pisonombre"]."</td>";
		$texto=$texto."<td>".$fila["pedidohora"]."</td>";
		$texto=$texto."<td>".$fila["fecha1"]."</td>";
		$texto=$texto."<td>".$fila["fecha2"]."</td>";
		$texto=$texto."<td>".$fila["preciototal"]." &euro;</td>";
		$texto=$texto."<td>".$fila["correo"]."</td>";
		$texto=$texto."</tr>";
	}
	echo $texto."</table>";
}
function modifreserva()
{
	$fila=mysql_fetch_assoc(ejecutar("select reservas.id as idreserva,reservas.id_piso as idpisoreserva,reservas.correo as correoreserva,reservas.preciototal as preciototal,reservas.fecha1,reservas.fecha2,reservas.personas,reservas.persona_nombre,reservas.persona_direccion,reservas.persona_ciudad,reservas.persona_pais,reservas.persona_telefono,reservas.persona_movil,reservas.persona_dni,reservas.persona_nacimiento,reservas.persona_transporte,reservas.persona_hora,reservas.persona_comentario,pisos.id,pisos.nombre as nombrepiso,pisos.contacto_per,pisos.contacto_mail,pisos.contacto_tel,pisos.precio_limpieza,pisos.fianza,pisos.direccion as direccionreserva from reservas,pisos where reservas.id='".$_GET["id"]."' and reservas.id_piso=pisos.id"));
	
	$preciototal=$fila["preciototal"]+$fila["precio_limpieza"];
	$precio20iva=($fila["preciototal"]*0.2)+($fila["preciototal"]*0.2*0.16);
	$precioresto=$preciototal-$precio20iva;
	
	$texto="<p>If you need to contact us do it at <a href='mailto:info@sleepbcn.com'>info@sleepbcn.com</a></p>
	<p>BOOKING CONFIRMATION</p>
	<p>Thank you very much for choosing SleepBCN apartments for your travel  planning in Barcelona.<br>We have received your payment correctly. This is to confirm your  reservation as follows:</p>
	<p>Total amount : <b>".$preciototal." &euro;</b><br>
	Reservacion TOTAL (20% + IVA) : <b>".$precio20iva." &euro;</b> (all taxes included in this payment)<br>
	Remainder : <b>".$precioresto." &euro;</b> (to be paid cash to the owner on your check-in-day)<br>
	Refundable deposit : <b>".$fila["fianza"]." &euro;</b></p>
	<p>BOOKING REFERENCE: ".$fila["idreserva"]."</p>
	<p>Apartment reference: <b>".$fila["nombrepiso"]."</b><br>
	<a href='http://www.sleepbcn.com/barcelona-apartments.php?id=".$fila["idpisoreserva"]."' target='_blank'>http://www.sleepbcn.com/barcelona-apartments.php?id=".$fila["idpisoreserva"]."</a><br>
	Arrival Date : <b>".$fila["fecha1"]."</b><br>
	Departure Date : <b>".$fila["fecha2"]."</b><br>
	Number of adults / children / babys: <b>".$fila["personas"]."</b><br>
	Name : <b>".$fila["persona_nombre"]."</b><br>
	EMail : <b>".$fila["correoreserva"]."</b><br>
	Address : <b>".$fila["persona_direccion"]."</b><br>
	City / Country: <b>".$fila["persona_ciudad"]." - ".$fila["persona_pais"]."</b><br>
	Tel / Movil: <b>".$fila["persona_telefono"]." - ".$fila["persona_movil"]."</b><br>
	DNI / Passport : <b>".$fila["persona_dni"]."</b><br>
	Birthday : <b>".$fila["persona_nacimiento"]."</b><br>
	Transport / Time : <b>".$fila["persona_transporte"]." - ".$fila["persona_hora"]."</b><br>
	Comment : <b>".$fila["persona_comentario"]."</b></p>
	<p>APARTMENT ADDRESS</p>
	<p>Apartment address: <b>".$fila["direccionreserva"]."</b><br>
	Check-in time: <b>after 14:00 h.</b><br>
	Check-out time: <b>before 11:00 h.</b></p>
	<p>CONTACT PERSON</p>
	<p>Contact name : <b>".$fila["contacto_per"]."</b><br>
	Contact e-mail : <b>".$fila["contacto_mail"]."</b><br>Contact Tel: <b>".$fila["contacto_tel"]."</b></p>
	<p><b>IMPORTANT:</b></p> 
	<p>Please contact the owners to communicate them your arrival time  details.</p>
	<p>&bull; All the apartments are of private property wich makes a good communication  very important as often the owners will have to go to the apartments themselves to do  check-ins/-outs etc.</p>
	<p>&bull; We would appreciate it very much if you could help the owners a little  bit by contacting them beforahand by e-mail or phone to confirm your exact arrival time. In case of any delay, please call the owner immediately to inform him.</p>
	<p>&bull; The owner or representative will be waiting for you at the apartment to  give, you the keys and to show you everything worth knowing.This is also when  you pay the remaining balance plus a refundable deposit of 150 euros to cover  any possible damage to the property. In case of any doubts or problems  ocurring during your stay, please contact to owner directly. He will try to help  you and to solve the problem right away.</p>
	<p>&bull; The apartments are completely equipped with towels, bed linen and  kitchen implements. All services susch as electricity, gas and water ae included.</p>
	<p>&bull; SleepBCN apartments mediates apartment reservations between owners and customers. As an agency we are only responsible for our service and always  try to help you in this matter with pleasure. For all affairs concerning the apartment, your contact person is the  owner.</p>
	<p>&bull; Neither SleepBCN apartments nor its agent(s) are responsible for any  contracts made between owners and clients, in verbal or written form, any loss of  damage to property or injury to persons caused by any defect, negligence or other  wrong or irresponsible behaviour.</p>
	<p>We wish you a pleasant stay in Barcelona</p>
	<p>****** Please print out this e-mail for your records ******</p>
	<p><a href='http://www.sleepbcn.com'>www.sleepbcn.com</a></p>";
	echo $texto;
}
function borrareserva()
{
	if(ejecutar("delete from reservas where id='".$_GET["id"]."'"))
		echo "<script>window.open('index.php?modo=ver_reservas','_self')</script>";	
	else
		echo "<p>ERROR</p>";
}
function pasarela_fin($clausula)
{
	$ultimo=mysql_fetch_assoc(ejecutar("select reservas.id as idreserva,reservas.id_piso as idpisoreserva,reservas.correo as correoreserva,reservas.preciototal as preciototal,reservas.fecha1,reservas.fecha2,reservas.personas,reservas.persona_nombre,reservas.persona_direccion,reservas.persona_ciudad,reservas.persona_pais,reservas.persona_telefono,reservas.persona_movil,reservas.persona_dni,reservas.persona_nacimiento,reservas.persona_transporte,reservas.persona_hora,reservas.persona_comentario,reservas.pedidohora,pisos.id,pisos.nombre as nombrepiso,pisos.contacto_per,pisos.contacto_mail,pisos.contacto_tel,pisos.precio_limpieza,pisos.fianza,pisos.direccion as direccionreserva from reservas,pisos where ".$clausula." and reservas.id_piso=pisos.id"));
	
	//ejecutar la ocupacion de dias en rojo automaticamente//autor=3 usuario pasarela
	$fecha_ini=explode("-",$ultimo["fecha1"]);
	$fecha_fin=explode("-",$ultimo["fecha2"]);

	$date1 = mktime(0,0,0,$fecha_ini[1],$fecha_ini[2],$fecha_ini[0]);
	$date2 = mktime(0,0,0,$fecha_fin[1],$fecha_fin[2],$fecha_fin[0]);

	$numerodias=round(($date2 - $date1) / (60 * 60 * 24));
	$datstart = $ultimo["fecha1"];
	
	//primer insert previo borrar si era oferta
	ejecutar("delete from calendario where id_piso='".$ultimo["idpisoreserva"]."' and dia='".$ultimo["fecha1"]."'");
	
	if(ejecutar("insert into calendario (id_piso,dia,autor,estado) values('".$ultimo["idpisoreserva"]."','".$ultimo["fecha1"]."',3,'ocupado_x1')") )
	{	
		echo "<p>OK</p>";
	
		while ($numerodias >= 2) 
		{
			$datyy=substr($datstart,0,4);
			$datmm=substr($datstart,5,2);
			$datdd=substr($datstart,8,2);
			$fda=$datdd + 1;
			$fmo=$datmm + 0;
			$fyr=$datyy + 0;
			$dat1=date("Y-m-d", mktime(0,0,0,$fmo,$fda,$fyr)); 
		
			//resto de inserts previos borrar
			ejecutar("delete from calendario where id_piso='".$ultimo["idpisoreserva"]."' and dia='".$dat1."'");
			ejecutar("insert into calendario (id_piso,dia,autor,estado) values('".$ultimo["idpisoreserva"]."','".$dat1."',3,'ocupado')");
		
			$datstart=$dat1; 
			$numerodias--; 
		}
		$preciototal=$ultimo["preciototal"]+$ultimo["precio_limpieza"]; /*+$incremento*/
		$precio20iva=($ultimo["preciototal"]*0.2)+($ultimo["preciototal"]*0.2*0.16);
		$precioresto=$preciototal-$precio20iva;
		
		$cuerpo_mail="<p>If you need to contact us do it at <a href='mailto:info@sleepbcn.com'>info@sleepbcn.com</a></p>
		<p>BOOKING CONFIRMATION</p>
		<p>Thank you very much for choosing SleepBCN apartments for your travel  planning in Barcelona.<br>We have received your payment correctly. This is to confirm your  reservation as follows:</p>
		Total amount : ".$preciototal." &euro;<br>
		Reservacion TOTAL (20% + IVA) : ".$precio20iva." &euro; (all taxes included in this payment)<br>
		Remainder : ".$precioresto." &euro; (to be paid cash to the owner on your check-in-day)<br>
		Refundable deposit : ".$ultimo["fianza"]." &euro;<br>
		<p>BOOKING REFERENCE: ".$ultimo["idreserva"]."</p>
		Apartment reference: ".$ultimo["nombrepiso"]."<br>
		<a href='http://www.sleepbcn.com/barcelona-apartments.php?id=".$ultimo["idpisoreserva"]."'>http://www.sleepbcn.com/barcelona-apartments.php?id=".$ultimo["idpisoreserva"]."</a><br>
		Arrival Date : ".$ultimo["fecha1"]."<br>
		Departure Date : ".$ultimo["fecha2"]." (".round(($date2 - $date1) / (60 * 60 * 24))." night)<br>
		Number of adults / children / babys: ".$ultimo["personas"]."<br>
		Name : ".$ultimo["persona_nombre"]."<br>
		EMail : ".$ultimo["correoreserva"]."<br>
		Address : ".$ultimo["persona_direccion"]."<br>
		City / Country: ".$ultimo["persona_ciudad"]." - ".$ultimo["persona_pais"]."<br>
		Tel / Movil: ".$ultimo["persona_telefono"]." - ".$ultimo["persona_movil"]."<br>
		DNI / Passport : ".$ultimo["persona_dni"]."<br>
		Birthday : ".$ultimo["persona_nacimiento"]."<br>
		Transport / Time : ".$ultimo["persona_transporte"]." - ".$ultimo["persona_hora"]."<br>
		Comment : ".$ultimo["persona_comentario"]."<br>
		<p>APARTMENT ADDRESS</p>
		Apartment address: ".$ultimo["direccionreserva"]."<br>
		Check-in time: after 14:00 h.<br>
		Check-out time: before 11:00 h.
		<p>CONTACT PERSON</p>
		Contact name : ".$ultimo["contacto_per"]."<br>
		Contact e-mail : ".$ultimo["contacto_mail"]."<br>Contact Tel: ".$ultimo["contacto_tel"]." 
		<p>IMPORTANT:</p> 
		Please contact the owners to communicate them your arrival time  details.
		<p>&bull; All the apartments are of private property wich makes a good communication  very important as often the owners will have to go to the apartments themselves to do  check-ins/-outs etc.</p>
		<p>&bull; We would appreciate it very much if you could help the owners a little  bit by contacting them beforahand by e-mail or phone to confirm your exact arrival time. In case of any delay, please call the owner immediately to inform him.</p>
		<p>&bull; The owner or representative will be waiting for you at the apartment to  give, you the keys and to show you everything worth knowing.This is also when  you pay the remaining balance plus a refundable deposit of 150 euros to cover  any possible damage to the property. In case of any doubts or problems  ocurring during your stay, please contact to owner directly. He will try to help  you and to solve the problem right away.</p>
		<p>&bull; The apartments are completely equipped with towels, bed linen and  kitchen implements. All services susch as electricity, gas and water ae included.</p>
		<p>&bull; SleepBCN apartments mediates apartment reservations between owners and customers. As an agency we are only responsible for our service and always  try to help you in this matter with pleasure. For all affairs concerning the apartment, your contact person is the  owner.</p>
		<p>&bull; Neither SleepBCN apartments nor its agent(s) are responsible for any  contracts made between owners and clients, in verbal or written form, any loss of  damage to property or injury to persons caused by any defect, negligence or other  wrong or irresponsible behaviour.</p>
		<p>We wish you a pleasant stay in Barcelona</p>
		<p>****** Please print out this e-mail for your records ******</p>
		<p><a href='http://www.sleepbcn.com'>www.sleepbcn.com</a></p>";
		
		echo "<p>Transaction DONE CORRECTLY<br><i>Transaccion realizada con EXITO</i></p>";
		if($ultimo["pedidohora"]!="-")
		{
			if(mail($ultimo["correoreserva"],"Booking Confirmation SleepBCN",$cuerpo_mail,"MIME-Version:1.0\r\nFrom:info@sleepbcn.com\nReply-To:info@sleepbcn.com\nContent-type:text/html\nCc:omar@indianwebs.com\nBcc:".$ultimo["contacto_mail"]."\r\nX-Priority:3\r\nX-MSMail-Priority:High\r\nX-Mailer:".phpversion().""))
				echo "<p>It has sent an email confirmation<br><i>Se ha enviado un correo de confirmaci&oacute;n a su cuenta.</i></p>";
		}
	}
}
function validar_reserva()
{	
	pasarela_fin("reservas.id='".$_GET["id"]."'");
}
include("funciones-casas.php");
?>