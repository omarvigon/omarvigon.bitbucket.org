function cambio(color,pos)
{
	document.getElementById(pos).style.background=color;
}
function comollegar(num)
{
	document.getElementById("llegar1").innerHTML="";
	document.getElementById("llegar2").innerHTML="";
	document.getElementById("llegar3").innerHTML="";
	document.getElementById("llegar4").innerHTML="";
	document.getElementById("llegar5").innerHTML="";
	document.getElementById("llegar6").innerHTML="";
	switch(num)
	{
		case 1:
		document.getElementById("llegar1").innerHTML="<ul><li>Toma la linea 10 (Cercanias) hasta la Estaci&oacute;n de Sants (aprox. 30 min.).</li><li> Cambia al metro linea 3 (verde) en direcci&oacute;n a 'Caneyelles' y baja en Paral-lel.</li><li> Desde alli, est&aacute;s a solo dos minutos caminando del hostal (mira explicaci&oacute;n arriba).</li><li><b>Horario</b>: Desde el aeropuerto: desde las 6:00 hasta las 23:44; desde la estaci&oacute;n de Fran&ccedil;a: desde 5:12 a las 22:42. Cada 30 minutos. </li><li><b>Consejo:</b> Puedes comprar un ticket 'T10' (para 10 viajes) que puede ser usado en el bus o en el metro.</li></ul>";
		break;
		
		case 2:
		document.getElementById("llegar2").innerHTML="<ul><li>Toma el autobus n&uacute;mero 20 en el World Trade Centre en el moll de Barcelona, direcci&oacute;n 'Pla del congres'.</li><li> Son solo dos paradas hasta Paral.lel (estar atento al Teatro Apolo).</li><li>Desde all&iacute;, est&aacute;s a solo dos minutos caminando del hostal (mira explicaci&oacute;n arriba).</li></ul>";
		break;
		
		case 3:
		document.getElementById("llegar3").innerHTML="<ul><li>Toma la l�nea 3 (verde), direcci&oacute;n 'Canyelles' y baja en Paral.lel.</li><li> Sal del metro siguiendo las indicaciones de 'Paral.lel'.</li><li> Camina hasta detr&aacute;s del Teatro Apolo, a lo largo de la calle Nou de la Rambla (una manzana), hasta llegar a la calle Vila i Vila.</li><li> Girar a la izquierda e inmediatamente despu&eacute;s a la derecha, entrando en la calle LaFont. </li><li>Nos encontrar&aacute;s en el n&uacute;mero 8-10.</li></ul>";
		break;
		
		case 4:
		document.getElementById("llegar4").innerHTML="<ul><li>Toma el autobus n&uacute;mero 64 en 'Pla de Palau', direcci&oacute;n 'Pedralbes'.</li><li> Baja en Paral.lel (estar atento al Teatro Apolo).</li><li> Desde all&iacute;, est&aacute;s a solo dos minutos caminando del hostal (mira explicaci&oacute;n arriba).</li></ul>";
		break;
		
		case 5:
		document.getElementById("llegar5").innerHTML="<ul><li>Toma el metro, linea 1 (roja) a Catalunya. </li><li>Cambia a la linea verde, direcci&oacute;n Zona Universitaria y baja en Paral.lel.</li><li> Desde all&iacute;, est&aacute;s a solo dos minutos caminando del hostal (mira explicaci&oacute;n arriba).</li><li><b>Consejo:</b> Los autobuses de Ryanair generalmente llegan hasta la Estaci&oacute;n del Norte (principal estaci&oacute;n de autobuses). Mira arriba las indicaciones desde la estaci&oacute;n de autobuses.</li></ul>";
		break;
		
		case 6:
		document.getElementById("llegar6").innerHTML="<ul><li>Sal del metro siguiendo las indicaciones de 'Paral.lel'.</li><li>Camina hasta detr&aacute;s del Teatro Apolo, a lo largo de la calle Nou de la Rambla (una manzana), hasta llegar a la calle Vila i Vila. </li><li>Girar a la izquierda e inmediatamente despu&eacute;s a la derecha, entrando en la calle LaFont. </li><li>Nos encontrar&aacute;s en el numero 8-10.</li></ul>";
		break;
		
		default:
		break;
	}
}
function llegaringles(numi)
{
	document.getElementById("llegar1i").innerHTML="";
	document.getElementById("llegar2i").innerHTML="";
	document.getElementById("llegar3i").innerHTML="";
	document.getElementById("llegar4i").innerHTML="";
	document.getElementById("llegar5i").innerHTML="";
	document.getElementById("llegar6i").innerHTML="";

	switch(numi)
	{
		case 1:
		document.getElementById("llegar1i").innerHTML="<ul><li>Take line 10 (Cercanias) to 'Sants' station (Approximately 30 mins).</li><li>Change to the metro � line 3 (green) in the direction 'Caneyelles' and get off in Paral.lel.</li><li>From there you�re just 2 minutes walk away from the hostel (see above).</li><li><b>Timetable:</b> From the airport: from 6.00 to 23,44; From Fran&ccedil;a: station: from 5.12 to 22,42. Every 30 minutes.</li><li><b>TIP:</b> You can buy a 'T10' (10 trip pass) which can be used on the bus and metro.</li></ul>";
		break;
		
		case 2:
		document.getElementById("llegar2i").innerHTML="<ul><li>Take bus number 20 at the World Trade Centre in the 'moll of Barcelona', direction 'Pla del congres'.</li><li>It�s just 2 stops to Paral.lel (look out for the Apolo Theatre).</li><li> From there you�re just 2 minutes walk away from the hostel (see above).</li></ul>";
		break;
		
		case 3:
		document.getElementById("llegar3i").innerHTML="<ul><li>Take line 3 (green), direction 'Canyelles' and get off in Paral.lel.</li><li>Exit the metro following the signs for 'Paral.lel'.</li><li>Walk behind the Apolo Theatre along the street Nou de la Rambla for one block until you reach the street Vila I Vila.</li><li>Turn left and immediately right again into Lafont street.</li><li>We are at number 8 � 10.</li></ul>";
		break;
		
		case 4:
		document.getElementById("llegar4i").innerHTML="<ul><li>Take bus number 64 in 'Pla de Palau', direction 'Pedralbes'.</li><li>Get off at Paral.lel (Look out for the Apolo Theatre).</li><li>From there you are just 2 minutes walk to the hostel (see above).</li></ul>";
		break;
		
		case 5:
		document.getElementById("llegar5i").innerHTML="<ul><li>Take the metro, line 1 (red), to Catalunya.</li><li>Change to the green line, direction 'Zona Universiteria' and get off in Paral.lel.</li><li>From there you are just 2 minutes walk to the hostel (see above).</li><li><b>Tip:</b> Ryanair buses generally come into Nord Station (the main bus station). See above for directions from the bus station.</li></ul>";
		break;
		
		case 6:
		document.getElementById("llegar6i").innerHTML="<ul><li>Exit the metro following the signs for 'Paral.lel'.</li><li>Walk behind the Apolo Theatre along the street Nou de la Rambla for one block until you reach the street Vila I Vila.</li><li>Turn left and immediately right again into Lafont street.</li><li>We are at number 8 � 10.</li></ul>";
		break;
		
		default:
		break;
	}
}
var punt=1;
function imgalerias(aux)
{
	var numimgs=29;
	if(aux==0)
	{
		if(punt==1)	punt=numimgs;
		else		punt--;
	}
	else if(aux==1)
	{
		if(punt==numimgs)	punt=1;
		else				punt++;
	}
	document.getElementById('numeros').innerHTML=punt+"/"+numimgs;
	galeria.src="./imgs/galeria"+punt+".jpg";
	
}
var opaco=0;
function opacidad(nombre)
{
	if(opaco<99) 			
   	 {
		opaco=opaco+1;
       	if(opaco<10)
			document.all[nombre].style.opacity=".0"+opaco;
		else
			document.all[nombre].style.opacity="."+opaco;
		document.all[nombre].style.filter="alpha(opacity="+opaco+")";
        setTimeout("opacidad('"+nombre+"')");
    }
}
var rotar=1;
function cambiarimagenes()
{
	imgi1.src="imgs/inicio"+rotar+".jpg";
	if(rotar==10)rotar=1;
	else		rotar++;
	imgi2.src="imgs/inicio"+rotar+".jpg";
    setTimeout("cambiarimagenes()",3000);
}
var capa_alto=75;
function movcapa()
{
	if(capa_alto<100)
	{
		capa_alto=capa_alto+1;
		document.getElementById('cptit1').style.top=capa_alto;
		setTimeout("movcapa()",30);
	}	
}
function init()
{
	cambiarimagenes();
	movcapa();
	opacidad("sitio1");
	opacidad("sitio2");	
	/*
	var ruta=document.location.pathname;
	var temp;
	for(var i=0;i<=ruta.length;i++)
	{
		if((ruta.charAt(i)=="/") || (ruta.charAt(i)=="\\"))
			temp=i;
	}
	ruta=ruta.substr(temp+1,ruta.length);
	if(ruta=="index.php")*/	
}
function imgmapa(num)
{
	if(num==1)
		document.getElementById('cp1').style.visibility="visible";
	else if(num==0)
		document.getElementById('cp1').style.visibility="hidden";
}
function imginst(num)
{	
	switch(num)
	{
		case 1:
		document.getElementById('txtinst').innerHTML="<b>HABITACIONES</b><br>Privadas o compartidas en el caso de que viajes con amigos. Contamos con habitaciones de hasta capacidad m&aacute;xima de 8 personas. Tambi&eacute;n disponemos de una habitacion totalmente adaptada para minusv&aacute;lidos.";
		break;
		
		case 2:
		document.getElementById('txtinst').innerHTML="<b>BAR CON TV SAT&Eacute;LITE</b><br>El mejor lugar para conocer y relacionarte con otros viajeros tomando las cervezas m&aacute;s baratas de la zona. Abierto de 19:00 a 1:00 de la noche. Ademas de ofrecemos TV sat&eacute;lite para que puedas disfrutar de los mejroes eventos deportivos.";
		break;
		
		case 3:
		document.getElementById('txtinst').innerHTML="<b>INTERNET</b><br>Internet gratuito para estar conectado las 24 hrs. del d&iacute;a.";
		break;
		
		case 4:
		document.getElementById('txtinst').innerHTML="<b>GIMNASIO</b><br>Despu&eacute;s de todo el d&iacute;a andando por Barcelona, estira los m&uacute;sculos en nuestro gimnasio."
		break;
		
		case 5:
		document.getElementById('txtinst').innerHTML="<b>LAVABOS</b><br>Somos el albergue de Barcelona con mayor n&uacute;mero de ba&ntilde;os por hu&eacute;sped. Siempre limpios para que se sientas c&Oacute;modo durante tu estancia.";
		break;
		
		default:
		break;
	}
}
function imginst2(num)
{	
	switch(num)
	{
		case 1:
		document.getElementById('txtinst2').innerHTML="<b>Rooms</b><br>We have private (double and twin) rooms and dormitories of up to 8 beds. We also have a room completely equipped for the disabled.";
		break;
		
		case 2:
		document.getElementById('txtinst2').innerHTML="<b>Bar with satellite TV</b><br>The best place to get to know fellow travellers is having a beer in our bar, which serves the cheapest beer in town. The bar is open from 7pm to 1am. Our bar also has satellite TV so you don�t have to miss those important games!";
		break;
		
		case 3:
		document.getElementById('txtinst2').innerHTML="<b>Internet</b><br>We provide computers with FREE access to the internet. And for those travelling with their laptop, we�ve also got wireless internet access (WIFI).";
		break;
		
		case 4:
		document.getElementById('txtinst2').innerHTML="<b>Gym</b><br>After a long day walking all over Barcelona, why not stretch your muscles in our gym! Free of charge!"
		break;
		
		case 5:
		document.getElementById('txtinst2').innerHTML="<b>Bathrooms</b><br>We have more showers per guest than any other hostel in Barcelona! Our bathrooms are always clean so you can feel comfortable in them.";
		break;
		
		default:
		break;
	}
}
function lugar(num)
{
document.getElementById('lg1').innerHTML="<a href='javascript:lugar(1)'><b>LAS RAMBLAS</b></a>";
document.getElementById('lg2').innerHTML="<a href='javascript:lugar(2)'><b>MONUMENTO A COL&Oacute;N</b></a>";
document.getElementById('lg3').innerHTML="<a href='javascript:lugar(3)'><b>PUERTO PORT VELL</b></a>";
document.getElementById('lg4').innerHTML="<a href='javascript:lugar(4)'><b>MAREMAGNUM</b></a>";
document.getElementById('lg5').innerHTML="<a href='javascript:lugar(5)'><b>BARRIO G&Oacute;TICO</b></a>";
document.getElementById('lg6').innerHTML="<a href='javascript:lugar(6)'><b>EL LICEO</b></a>";
document.getElementById('lg7').innerHTML="<a href='javascript:lugar(7)'><b>EL BUS TUR&Iacute;STIC</b></a>";
document.getElementById('lg8').innerHTML="<a href='javascript:lugar(8)'><b>PLAZA REAL</b></a>";
	
	switch (num)
	{
		case 1:
		document.getElementById('lg1').innerHTML=document.getElementById('lg1').innerHTML+"<br>La calle m&aacute;s emblem&aacute;tica de Barcelona y un lugar para el encuentro, el paseo o la inmovilidad de los mimos. Llena de puestos coloristas de pintores, quioscos o vendedores de p&aacute;jaros.";
		lugares.src="imgs/lugar_rambla.jpg";
		break;
		
		case 2:
		document.getElementById('lg2').innerHTML=document.getElementById('lg2').innerHTML+"<br>Monumento de 51,30 m. de altura construido en 1886. Col&oacute;n indica con su dedo la direcci&oacute;n de las Am&eacute;ricas. Un ascensor comunica la base del monumento con el mirador superior, donde hay unas excelentes vistas.";
		lugares.src="imgs/lugar_colon.jpg"
		break;
		
		case 3:
		document.getElementById('lg3').innerHTML=document.getElementById('lg3').innerHTML+"<br>El puerto de Barcelona puede recorrerse en unas peque&ntilde;as embarcaciones llamadas Golondrinas que atracan al final del puerto de las Ramblas. Actualmente se utiliza como puerto deportivo.";
		lugares.src="imgs/lugar_port.jpg"
		break;
		
		case 4:
		document.getElementById('lg4').innerHTML=document.getElementById('lg4').innerHTML+"<br>Al final del paseo mar&iacute;timo se encuentra el complejo Maremagnum, centro comercial y de ocio, y donde se ha construido el ACUARIUM de Barcelona. Un lugar donde disfrutar del tiempo libre e ir de compras, ubicado en el Port Vell de Barcelona. Por la noche se convierte en un punto de encuentro de ocio nocturno.";
		lugares.src="imgs/lugar_mare.jpg"
		break;
		
		case 5:
		document.getElementById('lg5').innerHTML=document.getElementById('lg5').innerHTML+"<br>Es el n&uacute;cleo de Barcelona, donde naci&oacute; la ciudad, donde se encuentran los principales edificios p&uacute;blicos: el Palau de la Generalitar, el Ayuntamiento, la Catedral, el Palacio Real...";
		lugares.src="imgs/lugar_gotico.jpg"
		break;
		
		case 6:
		document.getElementById('lg6').innerHTML=document.getElementById('lg6').innerHTML+"<br>Situado en el centro de la Rambla, fue construido en 1845. Las obras acabaron en 1847. En 1861 un incendio destruy&oacute; la sala y el escenario. La fachada original se reform&oacute; en 1875. En 1944 sufri&oacute; un gran incencio quedando s&oaute;lo en pie el vest&iacute;bulo y el arco de herradura. Fue construido dejando lo que no se hab&iacute;a quemado y recreando la sala como en la construcci&oacute;n original.";
		lugares.src="imgs/lugar_liceo.jpg"
		break;
		
		case 7:
		document.getElementById('lg7').innerHTML=document.getElementById('lg7').innerHTML+"<br>El Bus Tur&iacute;stico de Barcelona os ense&ntilde;ara de la forma m&aacute;s c&oacute;moda, los itios m&aacute; atractivos e interesantes de la ciudad. Podr&eacute;is subir y bajar del Bus tantas veces como quer&aacute;is en las 42 paradas. Combinad las 3 rutas con el mismo billete en los 5 puntos de conexi&oacute;n.";
		lugares.src="imgs/lugar_bus.jpg"
		break;
		
		case 8:
		document.getElementById('lg8').innerHTML=document.getElementById('lg8').innerHTML+"<br>Plaza p&uacute;blica de formato cuadrangular que colinda con La Rambla y est&aacute; situada en el Barrio G&oacute;tico de la ciudad. El sitio es frecuentado de noche porque en sus alrededores existen bares y sitios para bailar y escuchar m&uacute;sica. En la plaza existe una fuente y el per&iacute;metro de la misma est&aacute; adornado con palmeras reales.";
		lugares.src="imgs/lugar_plaza.jpg"
		break;
		
		default:
		break;
	}
}
function lugar2(num)
{
document.getElementById('lg1i').innerHTML="<a href='javascript:lugar2(1)'><b>Las Ramblas</b></a>";
document.getElementById('lg2i').innerHTML="<a href='javascript:lugar2(2)'><b>The Colon Monument</b></a>";
document.getElementById('lg3i').innerHTML="<a href='javascript:lugar2(3)'><b>Port Vell (The old port)</b></a>";
document.getElementById('lg4i').innerHTML="<a href='javascript:lugar2(4)'><b>Maremagnum</b></a>";
document.getElementById('lg5i').innerHTML="<a href='javascript:lugar2(5)'><b>Barrio G&oacute;tico</b></a>";
document.getElementById('lg6i').innerHTML="<a href='javascript:lugar2(6)'><b>El Liceo</b></a>";
document.getElementById('lg7i').innerHTML="<a href='javascript:lugar2(7)'><b>El Bus Turistic</b></a>";
document.getElementById('lg8i').innerHTML="<a href='javascript:lugar2(8)'><b>Plaza Real</b></a>";
	
	switch(num)
	{
		case 1:
		document.getElementById('lg1i').innerHTML=document.getElementById('lg1i').innerHTML+"<br>This is the most symbolic street in Barcelona and a place to meet, to walk along or to just hang out. It is full of painters, paper shops and pet stalls.";
		lugares.src="imgs/lugar_rambla.jpg";
		break;
		
		case 2:
		document.getElementById('lg2i').innerHTML=document.getElementById('lg2i').innerHTML+"<br>This monument, constructed in 1886, is 51.30 metres high. Supposedly Christopher Columbus is pointing to America (however, he is actually pointing out to sea).There is a lift to take you to the look out at the top, where there are great views of Barcelona.";
		lugares.src="imgs/lugar_colon.jpg"
		break;
		
		case 3:
		document.getElementById('lg3i').innerHTML=document.getElementById('lg3i').innerHTML+"<br>The port of Barcelona is a combination of numerous little ports called 'Golondrinas', that all meet at the end of 'Las Ramblas'. It is currently used as a recreational port.";
		lugares.src="imgs/lugar_port.jpg"
		break;
		
		case 4:
		document.getElementById('lg4i').innerHTML=document.getElementById('lg4i').innerHTML+"<br>At the end of the 'paseo maritimo'(maritime walk) you will find Maremagnum, a shopping centre complete with restaurants, bars, cinemas and even an Aquarium. During the daytime it is a place to shop and by night, it becomes a meeting place for many young visitors to Barcelona.";
		lugares.src="imgs/lugar_mare.jpg"
		break;
		
		case 5:
		document.getElementById('lg5i').innerHTML=document.getElementById('lg5i').innerHTML+"<br>This is the nucleus of Barcelona, where it all began! Here you will find the first public buildings, the Palaces of the Generalitat and the Ayuntamiento, the Cathedral, and the Royal Palace...";
		lugares.src="imgs/lugar_gotico.jpg"
		break;
		
		case 6:
		document.getElementById('lg6i').innerHTML=document.getElementById('lg6i').innerHTML+"<br>This theatre, situated in La Rambla, was constructed in 1845. The building was completed in 1847 and in 1861 a fire destroyed the auditorium and the stage. The original facade was reformed in 1875. In 1994 another fire left standing only the foyer and the horseshoe arch. It was reconstructed maintaining what remained of the old structure and recreating the auditorium based on its original design.";
		lugares.src="imgs/lugar_liceo.jpg"
		break;
		
		case 7:
		document.getElementById('lg7i').innerHTML=document.getElementById('lg7i').innerHTML+"<br>This bus tour will take you to the most popular tourist attractions in Barcelona. You can hop-on and hop-off the bus as much as you want in any of the 42 stops. There are 3 routes to choose from (included in the one ticket) to keep you busy all day.";
		lugares.src="imgs/lugar_bus.jpg"
		break;
		
		case 8:
		document.getElementById('lg8i').innerHTML=document.getElementById('lg8i').innerHTML+"<br>This plaza, situated just off Las Ramblas, is situated in the heart of the 'Barrio Gotico'.It is a surrounded by bars and restaurants making it a popular spot both day and night.";
		lugares.src="imgs/lugar_plaza.jpg"
		break;
		
		default:
		break;
	}
}