<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<META NAME="keywords" CONTENT="Barcelona's newest youth hostel,Nuevo albergue en barcelona,Budget accommodation,alojamiento economico,clean,limpio,cheap,barato,spacious rooms,habitaciones espaciosas,fun,divertido,bar,bar,close to bus, metro, bars, restaurants, nightclubs, tourist sites, Mont,Juic, Las Ramblas, Barrio Gotico, beach, el raval,cerca del metro, bus,bares, restaurantes, bares de noche, sitios turisticos, montjuich, las,ramblas, barrio gotico, playa, el raval,Catch metro to Park Guell, Casa Batllo, Casa Milla, Sagrada Familia,Free breakfast,desayuno gratis,Free internet,internet gratuito,Hostel for young travellers,albergue para jovenes viajeros,Meet backpackers from different countries,conoce a mochileros de,diferentes paises,Dormitories/ dormitorios,Double rooms,habitaciones dobles,air conditioned,aire acondicionado,funky design,dise�o moderno,friendly staff,personal amigable,tourist information,informaci�n turistica,location - central,situado en el centro,in the centre,en el centro,in the heart of the city,en el corazon de la ciudad,large common room,gran sala de estar,Kitchen,cocina,sleep well,dormir bien,party,fiesta,gaudi,beer,cerveza,sangria,youth hostel,albergue,bcn">
<META NAME="description" CONTENT="Barcelona's newest youth hostel,Nuevo albergue en barcelona,Budget accommodation,alojamiento economico,clean,limpio,cheap,barato,spacious rooms,habitaciones espaciosas,fun,divertido,bar,bar,close to bus, metro, bars, restaurants, nightclubs, tourist sites, Mont,Juic, Las Ramblas, Barrio Gotico, beach, el raval,cerca del metro, bus,bares, restaurantes, bares de noche, sitios turisticos, montjuich, las,ramblas, barrio gotico, playa, el raval,Catch metro to Park Guell, Casa Batllo, Casa Milla, Sagrada Familia,Free breakfast,desayuno gratis,Free internet,internet gratuito,Hostel for young travellers,albergue para jovenes viajeros,Meet backpackers from different countries,conoce a mochileros de,diferentes paises,Dormitories/ dormitorios,Double rooms,habitaciones dobles,air conditioned,aire acondicionado,funky design,dise�o moderno,friendly staff,personal amigable,tourist information,informaci�n turistica,location - central,situado en el centro,in the centre,en el centro,in the heart of the city,en el corazon de la ciudad,large common room,gran sala de estar,Kitchen,cocina,sleep well,dormir bien,party,fiesta,gaudi,beer,cerveza,sangria,youth hostel,albergue,bcn">

<title>HELLOBCN HOSTEL</title>
	<link rel="stylesheet" href="estilos.css" type="text/css">
	<script language="javascript" src="funciones.js" type="text/javascript"></script>
</head>

<body onLoad="init()">
<table cellspacing="2" cellpadding="0" width="100%" height="100%">
<tr>
	<td width="75%" height="80" nowrap>
		<img src="imgs/top_logo.gif">&nbsp;&nbsp;&nbsp;&nbsp;
		<img src="imgs/inicio1.jpg" name="imgi1" hspace="4">
		<img src="imgs/inicio2.jpg" name="imgi2"></td>
	<td width="25%">
		<ul>
		<li><a href="index.php" class="tabla2"><b>Spanish</b></a>
		<li><a href="indexi.php" class="tabla2"><b>English</b></a>
		</ul>
	</td>
</tr>
<tr>
	<td valign="top">
	<?php 
	switch($_GET['seccion'])
	{
		case "comollegari":
		echo'<table id="centro2" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Where are we located?</td></tr>
		<tr><td class="txtcentro" valign="top" width="360">
		<p><b>How to get to HelloBCN</b></p>
		<p>We are located in Lafont Street, number 8 � 10; just behind the Apolo Theatre.</p>
		<div><a href="javascript:llegaringles(5)"><b>From the main bus stop (Estacion del Nord):</b></a></div>
		<br><div id="llegar5i"></div>
		<div><a href="javascript:llegaringles(6)"><b>From the metro stop Paral.lel(2 minutes walk):</b></a></div>
		<br><div id="llegar6i"></div><div><a href="javascript:llegaringles(1)"><b>From the airport:</b></a></div>
		<br><div id="llegar1i"></div><div><a href="javascript:llegaringles(2)"><b>From the wharf:</b></a></div>
		<br><div id="llegar2i"></div><div><a href="javascript:llegaringles(3)"><b>From the main train station (Estacion de Sants):</b></a></div>
		<br><div id="llegar3i"></div><div><a href="javascript:llegaringles(4)"><b>From France Station (Estacion de Fran&ccedil;a):</b></a></div>
		<br><div id="llegar4i"></div></td>
		<td valign="top">
		<div id="cp1" class="capa1" onMouseOver="imgmapa(1)" onMouseOut="imgmapa(0)">
		<img src="imgs/mapa2.jpg" border="1"></div>
		<img src="imgs/mapa1.jpg" border="1"onMouseOver="imgmapa(1)" onMouseOut="imgmapa(0)">
		<div onMouseOver="imgmapa(1)" onMouseOut="imgmapa(0)"><b>Zoom</b></div>
		</td></tr></table>';
		break;
		
		case "instalacionesi":
		echo'<table id="centro3" cellpadding="4" cellspacing="0">
    	<tr><td colspan="2" class="titulo1"><div id="cptit3">Facilities</div></td></tr>
		<tr><td height="140"><br>
		<a href="#"><img src="imgs/instalaciones1.jpg" border="1" onMouseOver="imginst2(1)"></a></td>
		<td><br><a href="#"><img src="imgs/instalaciones2.jpg" border="1" onMouseOver="imginst2(2)"></a></td>
		<td><br><a href="#"><img src="imgs/instalaciones3.jpg" border="1" onMouseOver="imginst2(3)"></a></td>	
		</tr><tr valign="top">
		<td><a href="#"><img src="imgs/instalaciones4.jpg" border="1" onMouseOver="imginst2(4)"></a></td>
		<td><a href="#"><img src="imgs/instalaciones5.jpg" border="1" onMouseOver="imginst2(5)"></a></td>
		<td class="txtcentro" id="txtinst2"><b>Rooms</b><br>We have private (double and twin) rooms and dormitories of up to 8 beds.  We also have a room completely equipped for the disabled.</td>
		</tr></table>';
		break;
		
		case "serviciosi":
		echo'<table id="centro1" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Services</td></tr>
		<tr><td colspan="2" class="txtcentro" valign="top">
		<p><b>At HelloBCN we offer you a million and one services to make your stay here as comfortable as possible:</b></p></td></tr>
		<tr><td valign="top"><br><ul>
		<li>24 hour reception
		<li>WIFI 
		<li>Breakfast included
		<li>Private rooms with bathrooms
		<li>Dormitories
		<li>Sheets  and blankets included
		<li>Towels available
		<li>Hot water showers
		<li>Air conditioning and heating
		<li>Security lockers in your room
		<li>Free internet
		<li>Gym
		<li>Vending machines
		<li>Public telephone
		<li>Security - Magnetic access keys
		<li>Kitchen with fridges and a microwave
		<li>Large common room with platinum TV screens and sofas
		<li>Chill out zone with sofas
		<li>Laundry � washer and dryer
		<li>Lift
		<li>Rooms cleaned daily
		<li>Bar open from 7pm to 1 am 
		<li>Luggage storage (day of departure)
		<li>Credit cards accepted.</ul></td>
		<td valign="top">
		<img src="imgs/servicios1.jpg" border="1" hspace="4" vspace="8" name="ser1"><br>
		<img src="imgs/servicios2.jpg" border="1" hspace="4" vspace="8" name="ser2">
		</td></tr></table>';
		break;
		
		case "lugarinteresi":
		echo'<table id="centro2" cellpadding="0" cellspacing="0">
	 	<tr> <td class="titulo1" colspan="2">Places of Interest</td></tr>
    	<tr><td valign="top"><br>
  		<div id="lg1i" class="txtcentro"><a href="javascript:lugar2(1)">
		<b>Las Ramblas</b></a><br>This is the most symbolic street in Barcelona and a place to meet, to walk along or to just hang out.  It is full of painters, paper shops and pet stalls.</div>
	  	<div id="lg2i" class="txtcentro"><a href="javascript:lugar2(2)"><b>The Colon Monument</b></a></div>
	  	<div id="lg3i" class="txtcentro"><a href="javascript:lugar2(3)"><b>Port Vell (The old port)</b></a></div>
		<div id="lg4i" class="txtcentro"><a href="javascript:lugar2(4)"><b>Maremagnum</b></a></div>
		<div id="lg5i" class="txtcentro"><a href="javascript:lugar2(5)"><b>Barrio G&oacute;tico</b></a></div>
	  	<div id="lg6i" class="txtcentro"><a href="javascript:lugar2(6)"><b>El Liceo</b></a></div>
		<div id="lg7i" class="txtcentro"><a href="javascript:lugar2(7)"><b>El Bus Turistic</b></a></div>
	 	<div id="lg8i" class="txtcentro"><a href="javascript:lugar2(8)"><b>Plaza Real</b></a></div>
		</td><td valign="top"><img src="imgs/lugar_rambla.jpg" name="lugares" hspace="16" vspace="12"></td></tr></table>';
		break;
		
		case "tarifasreservasi":
		echo'<table id="centro3" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Rates</td></tr>
        <tr><td class="txtcentro2"><br><ul><b>LOW SEASON:</b><br>(prices per person per night)
		<li><b>Beds</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from 16&#8364;</li>
		<li><b>Rooms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> from 25&#8364;</li></ul>
		<br><img src="imgs/tarifa1.jpg" name="tar1" border="1"></td>
		<td><br><ul><b>HIGH SEASON:</b><br>(prices per person per night)
		<li><b>Beds</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from 20&#8364;</li>
		<li><b>Rooms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> from 30&#8364;</li></ul>
		<img src="imgs/tarifa2.jpg" name="tar2" border="1"></td></tr>
		<tr><td class="titulo1" colspan="2">Reservations</td></tr>
		<tr><td colspan="2" class="txtcentro">
		<p><b>You can book a room or a bed in our hostel by:</b></p>
		<b>1-</b>Make a confirmed booking using our on-line booking system.<a href="http://www.reservations.bookhostels.com/hellobcnhostel.com/" target="_blank">CLICK HERE</a>
		<br><b>2-</b>Calling us on +34 93 442 83 92
		<br><b>1-</b>Sending an email to <b><a href="mailto:info@hellobcnhostel.com">info@hellobcnhostel.com</a></b>(credit card details are required)
		<p><b>Group Bookings:(minimum 10 people)</b> Send an email <b><a href="mailto:groups@hellobcnhostel.com">groups@hellobcnhostel.com</a></b> quoting the following information:
		<ul><li>Number of people
		<li>Date of arrival
		<li>Date of departure
		<li>Number of nights</ul>
		We will respond to you promptly with prices and availability.<br><br></td></tr></table>';
		break;
	
		case "galeriafotosi":
		echo'<table id="centro1" cellpadding="0" cellspacing="0">
		<tr><td class="titulo1" colspan="2">Photo Gallery</td></tr>
		<tr><td align="center" valign="top"><br><br><img src="imgs/galeria1.jpg" name="galeria" border="1">
		<div><a href="javascript:imgalerias(0)"><b><< Previous</b></a> 
		<span id="numeros">1/29</span><a href="javascript:imgalerias(1)"><b>Next >></b></a></div>
		<p>Send your photos to <b><a href="mailto:info@hellobcnhostel.com�">info@hellobcnhostel.com</a></b></p>
		</td></tr></table>';
		break;
		
		case "enlacesi":
		echo'<table id="centro2" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Links</td></tr>
		<tr><td valign="top"><br>&nbsp;&nbsp;<b>Other Hostels:</b><ul>
		<li><a href="http://www.hostelsalessandro.com/" target="_blank">Roma - Alessandros</a>
		<li><a href="http://www.hostelnapoli.com/" target="_blank">Napoles - Hostel of the Sun</a>
		<li><a href="http://www.nesthostelsvalencia.com/" target="_blank">Valencia - Nest</a>
		<li><a href="http://www.catshostel.com/" target="_blank">Madrid - Cat`s</a>
		<li><a href="http://www.madhostel.com" target="_blank">Madrid - Mad</a>
		<li><a href="http://www.vsaint.com/?l=en&p=splash" target="_blank">Nice - Villa Saint Exup&eacute;ry</a>
		<li><a href="http://flyingpig.nl/" target="_blank">Amsterdan - The Flying Pig</a>
		<li><a href="http://www.flamingo-hostel.com/index.htm" target="_blank">Krakov</a>
		</ul>&nbsp;&nbsp;<b>Embassies and Consulates</b><ul>
		<li><a href="http://www.spain.embassy.gov.au/" target="_blank">Australia</a>
		<li><a href="http://www.nzembassy.com/home.cfm?c=27" target="_blank">New Zealand</a>
		<li><a href="http://madrid.usembassy.gov/barcelonaes.html" target="_blank">America</a>
		<li><a href="http://www.dfait-maeci.gc.ca/canada-europa/spain/consuladobar-es.asp" target="_blank">Canada</a>
		<li><a href="http://www.barcelona.diplo.de/Vertretung/barcelona/es/Deutsche__Institutionen__in__Gastland.html" target="_blank">Germany</a>
		<li><a href="http://www.consulfrance-barcelone.org/" target="_blank">France</a>
		<li><a href="http://www.britishembassy.gov.uk/servlet/Front?pagename=OpenMarket/Xcelerate/ShowPage&c=Page&cid=1144249363460" target="_blank">Great Britain</a>
		<li><a href="http://www.embajadapaisesbajos.es/" target="_blank">Holland</a>
		<li><a href="http://www.swedenabroad.com/pages/general.asp?id=14016&expand=9632" target="_blank">Sweden</a>
		<li><a href="http://www.italconsulbcn.org/" target="_blank">Italy</a>
		<li><a href="http://www.sre.gob.mx/barcelona/" target="_blank">Mexico</a>
		<li><a href="http://www.consuladoargentinobarcelona.com/Index.swf" target="_blank">Argentina</a>	
		</ul></td><td valign="top"><br><b>Barcelona:</b><ul>
		<li><a href="http://www.barcelona.es" target="_blank">Barcelona</a>
		<li><a href="http://www.fcbarcelona.com" target="_blank">Barcelona Football Club</a>
		<li><a href="http://www.sonar.es" target="_blank">Barcelona Sonar Festival</a>
		<li><a href="http://www.rabbies.eu.com" target="_blank">Discover the secrets of Catalunya</a>
		<li><a href="http://www.bcn.es/museus/welcomeCast.htm" target="_blank">Museums</a>
		<li><a href="http://www.guiadelocio.com/barcelona/" target="_blank">Entertainment</a></ul>
		<b>Train:</b><ul><li><a href="http://www.renfe.es/" target="_blank">www.renfe.es</a></ul>
		<b>Bus:</b><ul><li><a href="http://www.barcelonanord.com/" target="_blank">www.barcelonanord.com</a></ul>	
		<b>Ferry:</b><ul><li><a href="http://www.trasmediterranea.es" target="_blank">www.trasmediterranea.es</a></ul>
		<b>Plane:</b><ul>
		<li><a href="http://www.whichbudget.com" target="_blank"> www.whichbudget.com</a>
		<li><a href="http://www.easyjet.com/" target="_blank">www.easyjet.com</a>
		<li><a href="http://www.bmibaby.com" target="_blank">www.bmibaby.com</a>
		<li><a href="http://www.alpieagles.com" target="_blank">www.alpieagles.com</a>
		<li><a href="http://www.ryanair.com" target="_blank">www.ryanair.com</a></ul></td></tr></table>';
		break;
		
		case "tuopinioni":
		echo'<table id="centro1" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Your opinion</td></tr><tr><td valign="top" class="txtcentro">
		<p><b>Your opinion is important to us.  Send us your comments using this feedback form:</b></p>
		<form name="formulario3" method="post" action="indexi.php?seccion=maili">Name:<br>
		<input type="text" name="nombre" size="36" class="form2"><br>Nationality:<br>
		<input type="text" name="nacion" size="36" class="form2"><br>Email address:<br>
		<input type="text" name="correo" size="36" class="form2"><br>Message<br>
		<textarea name="comentario" cols="60" rows="10" class="form2"></textarea><br>
		<input type="submit" name="enviar" value="Send" class="form2"></form></td></tr></table>';
		break;
		
		case "maili":
		echo '<table id="centro1" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Your Opinion</td></tr>
		<tr><td valign="top" class="txtcentro">';

		if(mail("info@hellobcnhostel.com", "Comentarios de la pagina de HelloBCNHostel", "Nombre: \n".$_POST['nombre']."\nNacionalidad: \n".$_POST['nacion']."\nCorreo: \n".$_POST['correo']."\n\n\n".$_POST['comentario']))
			echo "Thank you for your opinion.";
		else
			echo "ERROR";
		echo '</td></tr></table>';
		break;
		
		default:
		echo'<table id="centro1" cellpadding="0" cellspacing="0">
        <tr><td class="titulo1" colspan="2">Who we are?</td></tr>
        <tr><td valign="top" class="txtcentro">
		<p><b>You�re always welcome in HelloBCN!</b></p>
		<p>We are located in the heart of the city, and only 10 minutes walk from �Las Ramblas� and the port!</p><p>In HelloBCN you will find all the comforts to make your visit to Barcelona an unforgettable one � new and modern facilities and friendly service!</p><p>Come and see for yourself!</p><p>We�ll be waiting for you!</p>
		C/ Lafont 8 � 10,<br> 08004  Barcelona<br>+34 93 442 83 92<br>
		<a href="mailto:info@hellobcnhostel.com">info@hellobcnhostel.com</a><br>
		<p><b><a href="indexi.php?seccion=comollegari">>>Check out the map to see where we�re located!</a></b></p></td>
    	<td valign="top"><img src="imgs/sitio1.jpg" border="1" hspace="16" name="sitio1"><br>
        <img src="imgs/sitio2.jpg" border="1" hspace="16" vspace="12" name="sitio2"> </td>
    	</tr></table>';
		break;
	}
	?>
	</td>
	
	<td valign="top">
	 <table class="tabla2" cellspacing="0" cellpadding="0" width="100%">
	 	 <tr>
	 	 <td id="bt1" onMouseOver="cambio('#F2CACF',this.id)" onMouseOut="cambio('#F4B5BD',this.id)">
	 	 <a href="indexi.php"><b>Who we are?</b></a></td></tr><tr><td height="2"></td></tr>
	 	 <tr>
		 <td id="bt2" onMouseOver="cambio('#E87180',this.id)" onMouseOut="cambio('#E34559',this.id)">
		 <a href="indexi.php?seccion=comollegari"><b>Where are we located?</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt3" onMouseOver="cambio('#E3F269',this.id)" onMouseOut="cambio('#C4DB0D',this.id)">
		 <a href="indexi.php?seccion=instalacionesi"><b>Facilities</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt4" onMouseOver="cambio('#F2CACF',this.id)" onMouseOut="cambio('#F4B5BD',this.id)">
		 <a href="indexi.php?seccion=serviciosi"><b>Services</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt5" onMouseOver="cambio('#E87180',this.id)" onMouseOut="cambio('#E34559',this.id)">
	     <a href="indexi.php?seccion=lugarinteresi"><b>Places of Interest</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt6" onMouseOver="cambio('#E3F269',this.id)" onMouseOut="cambio('#C4DB0D',this.id)">
		 <a href="indexi.php?seccion=tarifasreservasi"><b>Rates and Reservations</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt7" onMouseOver="cambio('#F2CACF',this.id)" onMouseOut="cambio('#F4B5BD',this.id)">
		<a href="indexi.php?seccion=galeriafotosi"><b>Photo Gallery</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt8" onMouseOver="cambio('#E87180',this.id)" onMouseOut="cambio('#E34559',this.id)">
		 <a href="indexi.php?seccion=enlacesi"><b>Links</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt9" onMouseOver="cambio('#E3F269',this.id)" onMouseOut="cambio('#C4DB0D',this.id)">
		 <a href="foro"><b>Stay in touch(Forum)</b></a></td></tr><tr><td height="2"></td></tr>
		 <tr>
		 <td id="bt10">
		 <a href="indexi.php?seccion=tuopinioni"><b>Your opinion</b></a></td></tr><td height="2"></td></tr>
		 <tr><td id="bt11">
		 <b>LAST POSTS IN THE FORUM</b><br>
		 <?php 
$conexion=mysql_connect("lldb162.servidoresdns.net","qbt918","hello");
mysql_select_db("qbt918");
$resultado=mysql_query("select * from hello_posts_text order by post_id desc limit 0,3");

for($i=0;$i<mysql_num_rows($resultado);$i++)
{
	$fila=mysql_fetch_row($resultado);
	$resultado2=mysql_query("select topic_id from hello_posts where post_id=".$fila[0]."");
	$fila2=mysql_fetch_row($resultado2);
	
	$temp=substr($fila[3],0,64);
	echo "<li><a href='http://www.hellobcnhostel.com/foro/viewtopic.php?t=".$fila2[0]."'><u><i><font color='#555555'>".$temp."...</font></u></i></a></li>";
}
mysql_close($conexion);
		?>
		 </td></tr>
	 
	 <tr><td valign="top">
	 	<p><b><a href="http://www.reservations.bookhostels.com/hellobcnhostel.com/" target="_blank"><span class="titulo2">BOOKING HERE</span></a></b></p>
	 	<b>Adress:</b><br>c/ Lafont 8-10, 08004 Barcelona
		<br><b>Telephone:</b><br>+34 93.442.83.92<br>
		<b><a href="mailto:info@hellobcnhostel.com">info@hellobcnhostel.com</a></b></td>
	 </tr>
	</table>
	</td>
</tr>
</table>
</body>
</html>
