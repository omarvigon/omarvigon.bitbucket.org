<?php 
$titulo="Llibres de poesia de Jordi Guardans. Web del poeta catal&agrave; Jordi Guardans.";
include("cabecera.php"); ?>

<tr>
	<td class="celda_libro1"><img title="Jordi Guardans Contra Venena" src="img/libro_contra_venena.jpg" alt="Jordi Guardans Contra Venena"></td>
    <td class="celda_libro1c"><img src="img/libro_balalaika.gif" title="Jordi Guardans Balalaika" alt="Jordi Guardans Balalaika"></td>
    <td class="celda_libro1"><img src="img/libro_montserrat_2.jpg" title="Jordi Guardans Montserrat" alt="Jordi Guardans Montserrat"></td>
</tr>
<tr>
	<td class="celda_libro1"><em>Jordi Guardans: <i>Contra Venena et Animalia Venenosa.</i></em><br />Ed. Emboscall. Prima Materia, 72. Vic, 2008.<p><a href="http://issuu.com/diviertete/docs/jordiguardans/1?mode=a_p" target="_blank"><img src="img/pdf.gif" align="absmiddle"> Pr&ograve;leg d'Enric Bou ( p.p 5-25).</a></p></td>
    <td class="celda_libro1c"><em>Jordi Guardans: <i>Planeta Balalaika.</i></em><br>Ed. Emboscall. Prima Materia, 63.Vic, 2007.<p><a href="img/anex_balalaika.gif" target="_blank"><img src="img/img.gif" align="absmiddle">&nbsp;Il.lustraci&oacute;: Santiago Ram&oacute;n y Cajal.</a></p><p><a href="files/planeta_balalaika.pdf" target="_blank"><img src="img/pdf.gif" align="absmiddle">&nbsp;Carles Guill&eacute;n i Selva : "Planeta Balalaika. L'enlairament el&egrave;ctric cap a casa"</a></p></td>
    <td class="celda_libro1"><em>Jordi Guardans: <i>Montserrat.</i></em><br>Ed. Emboscall. El taller de poesia, 77. <br>Vic, 2007. 2&ordf; Edici&oacute;.<p><a href="img/anex_montserrat_2.jpg" target="_blank"><img src="img/img.gif" align="left">&nbsp;Il.lustraci&oacute;: Seraf&iacute;. Absis de Santa Eul&agrave;lia d'Estaon.  S.XIII.&nbsp;</a></p></td>
</tr>
<tr>
    <td class="celda_libro1"><img src="img/libro_angels_el_perill.jpg" title="Jordi Guardans Els angels i el perill" alt="Jordi Guardans Els angels i el perill"></td>
    <td class="celda_libro1c"><img src="img/libro_abba.jpg" title="Jordi Guardans Abba" alt="Jordi Guardans Abba"></td>
    <td class="celda_libro1"><img src="img/libro_montserrat_1.jpg" title="Jordi Guardans Montserrat" alt="Jordi Guardans Montserrat"></td>
</tr>
<tr>
    <td class="celda_libro1"><em>Jordi Guardans: <i>Els &agrave;ngels i el perill.</i></em><br>Antologia. Tria i introducci&oacute; de Carles Guill&eacute;n i Selva. Ed. Emboscall. Nova Carpeta, 1. Vic, 2006.<p><a href="files/prologo_angels.pdf" target="_blank"><img src="img/pdf.gif" align="absmiddle">&nbsp;Introducci&oacute; de Carles Guill&eacute;n i Selva.</a></p><a href="img/anex_angels.jpg" target="_blank"><img src="img/img.gif" align="left"> Il.lustraci&oacute;: Gustave Dor&eacute; illlustration to Milton's  Paradise Lost.</a></td>
    <td class="celda_libro1c"><em>Jordi Guardans: <i>Abb&agrave;.</i></em><br>Ed. Emboscall. Prima Materia, 50. <br>Vic, 2006.<p><a href="img/anex_abba.jpg" target="_blank"><img src="img/img.gif" align="left">&nbsp;Fotografia</a>.</p></td>
    <td class="celda_libro1"><em>Jordi Guardans: <i>Montserrat.</i></em><br>Ed. Emboscall. El taller de poesia, 77. <br>Vic, 2003.<p><a href="img/anex_montserrat_1.jpg" target="_blank"><img src="img/img.gif" align="left">&nbsp;Il.lustraci&oacute;: William Blake.<br>Elohim creating Adam. 1795/1805.<br>Tate  Gallery.</a></p></td>
</tr>
<tr>
    <td class="celda_libro1"><img src="img/libro_requiem_blau.jpg" title="Jordi Guardans El Requiem Blau" alt="Jordi Guardans El Requiem Blau"></td>
    <td class="celda_libro1c"><img src="img/libro_tant_de_bo_que_siguin.jpg" title="Jordi Guardans Tant de bo que siguin les penultimes penes" alt="Jordi Guardans Tant de bo que siguin les penultimes penes"></td>
    <td class="celda_libro1"><img src="img/libro_ciutat_illuminada.jpg" title="Jordi Guardans La ciutat il.luminada" alt="Jordi Guardans La ciutat il.luminada"></td>
</tr>
<tr>
    <td class="celda_libro1"><em>Jordi Guardans: <i>El R&egrave;quiem blau.</i></em><br>Ed. 62. Els llibres de l'Escorp&iacute;. Poesia, 195. Barcelona, 2002. <br>Pr&ograve;leg de Pere Gimferrer.<p><a href="files/prologo_requiem.pdf" target="_blank"><img src="img/pdf.gif" align="absmiddle">&nbsp;Pr&ograve;leg de Pere Gimferrer (p.p 5-6).</a></p><a href="img/anex_requiem_blau.jpg" target="_blank"><img src="img/img.gif" align="left"> Il.lustraci&oacute;:&nbsp; Gustave Dor&eacute; illustration to Milton's Paradise  Lost.</a></td>
    <td class="celda_libro1c"><em>Jordi Guardans: <i>Tant de bo que siguin les pen&uacute;ltimes penes.</i></em><br>Ed. Columna, 126. Barcelona, 1994. Premi Mari&agrave; Manent de poesia, 1993. Premi&agrave; de Dalt.<p><a href="img/anex_tant.jpg" target="_blank"><img src="img/img.gif" align="left">&nbsp;Il.lustraci&oacute;: T&egrave;cnica mixta de Carles Guill&eacute;n i  selva, 1979.</a></p></td>
    <td class="celda_libro1"><em>Jordi Guardans: <i>La ciutat il.luminada.</i></em><br>Institut del teatre de la Diputaci&oacute; de Barcelona. Biblioteca Teatral, 68. Barcelona, 1989. Pr&ograve;leg d'Anna M. Moix. <br>L'any 1981 guany&agrave; la medalla d'or de teatre, que atorg&agrave; el Reial Cercle Art&iacute;stic de Barcelona en motiu del seu Centenari. Malgrat les bases del concurs, mai no ha estat estrenada.<p><a href="files/prologo_ciutat.pdf" target="_blank"><img src="img/pdf.gif" align="absmiddle"> Pr&ograve;leg d'Anna M. Moix ( p.p 5-13).</a></p><a href="img/anex_ciutat.jpg" target="_blank"><img src="img/img.gif" align="absmiddle"> Fotografia: Carles Guill&eacute;n i Selva, 1977.</a></td>
</tr>
</table>

<p align="center"><a href="http://www.indianwebs.com" target="_blank" title="Dise&ntilde;o Web Barcelona" style="font:10px Arial, Helvetica, sans-serif;color:#cccccc;text-decoration:none;">Dise&ntilde;o Web Barcelona</a></p>

</body>
</html>