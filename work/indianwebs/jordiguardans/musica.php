<?php 
$titulo="M&uacute;sica y can&ccedil;ons de Jordi Guardans. Web del poeta catal&agrave; Jordi Guardans.";
include("cabecera.php"); ?>

<tr>
	<td colspan="3">
	  <h4 style="border-bottom:30px solid #ffffff;">
	  <img title="Jordi Guardans Turnez Sese" src="img/disco_turnez_sese.jpg" alt="Jordi Guardans Turnez Sese" align="left">
	  <p><em><b>CD T&uacute;rnez &amp; Ses&eacute; "Roman&ccedil;os i estampes del 21"</b></em><br />Ed. Picap 2008<br />&quot;Rondalla del transvestit&quot; Lletra: Jordi Guardans. M&uacute;sica: Daniel Ses&eacute;. Arranjament: Jordi Ruiz.<br />
      &quot;Rondalla del ca&ccedil;ador il&middot;luminat&quot; Lletra: Jordi Guardans. M&uacute;sica: Daniel Ses&eacute;. Arranjament: Laia Rius.</p>
      <p><a target="_blank" href="popup.php?cancion=09-romanc_del_transvestit_minuto_1"><img src="img/mp3.gif" border="0" align="absmiddle"><em>9. Rondalla del transvestit</em></a><br />
      <a target="_blank" href="popup.php?cancion=10-rondalla_del_cacador_illuminat_1"><img src="img/mp3.gif" border="0" align="absmiddle"><em>10. Rondalla del ca&ccedil;ador il&middot;luminat</em></a></p>
	  </h4>
      
      <h4>
	  <img src="img/disco_terra_secreta.jpg" title="Jordi Guardans Terra secreta" alt="Jordi Guardans Terra secreta" align="left">
	  <p><em><b>CD Maria del Mar Bonet. &quot;Terra secreta&quot;.</b><br>
	  </em>Ed. Picap, 2007.  &quot;L'ex&egrave;rcit&quot;. Lletra i m&uacute;sica: Jordi Guardans. Arranjament: Dani  Espasa. Dani Espasa: piano. Lautaro Rosas: guitarr&oacute;. Olvido Lanza: viol&iacute;. Jordi  Coll: viol&iacute;. Pere Bardag&iacute;: viola. Manuel Mart&iacute;nez del Fresno: violoncel. Jordi  Gaspar: contrabaix. Javier Mas: guitarra 12 cordes. Antonio S&aacute;nchez:  percussions, efectes. Llu&iacute;s Ribalta: bateria.</p>
	  <p>L'ex&egrave;rcit  de Jordi Guardans: &quot; El record de les mobilitzacions de Barcelona la  primavera del 2003 contra la guerra de l'Iraq. Com la banda sonora d'un  documental mut, assimilable tamb&eacute; a d'altres tipus de lluita per les llibertats  col.lectives&quot; Maria del Mar Bonet.</p>
	  <p><a target="_blank" href="popup.php?cancion=un_exercit"><img src="img/mp3.gif" border="0" align="absmiddle"><em>5. L'ex&egrave;rcit.&nbsp;</em></a></p>
	  </h4>
	  
	  <h4>
	  <img src="img/disco_jordi_guardans.jpg" title="Jordi Guardans Ester Formosa canta Jordi Guardans" alt="Jordi Guardans Ester Formosa canta Jordi Guardans" align="left">
	  <p><em><b>CD &quot;Ester Formosa canta Jordi Guardans. Sola com el poeta&quot;</b>.</em>
	  <br>Ed. K-Ind&uacute;stria. 2007. Lletra i m&uacute;sica: Jordi Guardans.<br> 
	  Interpretaci&oacute;: Ester Formosa. Direcci&oacute; musical i piano: Maurici Villavecchia. Obra de la coberta: &quot;Busques&quot; del Perejaume. 1981. Col.lecci&oacute; particular de l'autor.</p>
	  <a target="_blank" href="popup.php?cancion=carrussel"><img src="img/mp3.gif" border="0" align="absmiddle"><em>1. Carrussel</em></a>
	  <br><a target="_blank" href="popup.php?cancion=el_poeta"><img src="img/mp3.gif" border="0" align="absmiddle"><em>2. El poeta</em></a>
	  <br><a target="_blank" href="popup.php?cancion=mencanta_cantar"><img src="img/mp3.gif" border="0" align="absmiddle"><em>3. M'encanta cantar</em></a>
	  <br><a target="_blank" href="popup.php?cancion=vaig_disfressat"><img src="img/mp3.gif" border="0" align="absmiddle"><em>4. Vaig disfressat</em></a>
	  <br><a target="_blank" href="popup.php?cancion=lany_nou"><img src="img/mp3.gif" border="0" align="absmiddle"><em>5. L'any nou</em></a>
	  <br><a target="_blank" href="popup.php?cancion=el_messies"><img src="img/mp3.gif" border="0" align="absmiddle"><em>6. El messies</em></a>
	  <br><a target="_blank" href="popup.php?cancion=desig"><img src="img/mp3.gif" border="0" align="absmiddle"><em>7. Desig</em></a>
	  <br><a target="_blank" href="popup.php?cancion=el_princep_llum"><img src="img/mp3.gif" border="0" align="absmiddle"><em>8. El Pr&iacute;ncep llum</em></a>
	  <br><a target="_blank" href="popup.php?cancion=vaixell_de_combat"><img src="img/mp3.gif" border="0" align="absmiddle"><em>9. Vaixell de combat</em></a>
	  <br><em>10. Com plou sobre Berl&iacute;n</em>
	  <br><a target="_blank" href="popup.php?cancion=mana_que_mana"><img src="img/mp3.gif" border="0" align="absmiddle"><em>11. Mana que mana</em></a>
	  <br><a target="_blank" href="popup.php?cancion=havanera_de_laurora"><img src="img/mp3.gif" border="0" align="absmiddle"><em>12. Havanera de l`aurora</em></a>
	  <br><a target="_blank" href="popup.php?cancion=pobrament"><img src="img/mp3.gif" border="0" align="absmiddle"><em>13. Pobrament</em></a>
	  <br><a target="_blank" href="popup.php?cancion=un_ocell_important"><img src="img/mp3.gif" border="0" align="absmiddle"><em>14. Un ocell important</em></a>
	  <br><a target="_blank" href="popup.php?cancion=prou"><img src="img/mp3.gif" border="0" align="absmiddle"><em>15. Prou</em></a>
      <br>
      <a href="img/anex_canta_jordiguardans.jpg" target="_blank"><img src="img/img.gif" align="absmiddle">Text introductori de Feliu Formosa. Fotografia: &Agrave;lex  Llopis. 2006.</a>
      </h4>
	  
	  <h4>
	  <img src="img/disco_per_un_tros_de_cel.jpg" title="Jordi Guardans Per un tros de cel" alt="Jordi Guardans Per un tros de cel" align="left">
	  <p><b><em>DVD + CD: Gerard Quintana &quot;Per un tros de cel&quot;.</em></b>
	  <br>
	  Ed. M&uacute;sica Global. 2005. <br />Concert de presentaci&oacute; a Barcelona del CD <em>&quot;Les claus de sal&quot;</em>, enregistrat el 2 de febrer del 2005 al Barcelona Teatre Musical en el marc del festival BARNASANTS.<br />
	  <em>&quot; He perdut el mar&quot;.</em> Poema: Jordi Guardans. Recita: Jordi Guardans.<br /><em>&quot; He perdut el mar&quot;.</em> Poema: Jordi Guardans. M&uacute;sica: Quintana/ Najas. </p>	
	  <p><em>11. He perdut el mar.<br>12. He perdut el mar.</em></p>
	  </h4>
	  
	  <h4>
	  <img src="img/disco_claus_de_sal.jpg" title="Jordi Guardans Les claus de sal" alt="Jordi Guardans Les claus de sal" align="left">
	  <b><em>CD Gerard Quintana &quot;Les claus de sal&quot;.</em></b>
	  <br>
	  Ed. M&uacute;sica Global. 2004.<br /><em>&quot;El dinar dels muts&quot;.</em> <br>
	  Poema: Jordi Guardans. M&uacute;sica: Quintana/ Najas. Del llibre <i>El R&egrave;quiem blau</i>. Ed. 62. Els llibres de l'Escorp&iacute;. Poesia, 195. Barcelona, 2002. Pr&ograve;leg de Pere Gimferrer.<br>
 	  <em>&quot;He perdut el mar&quot;.</em>
	  <br>
	  Poema: Jordi Guardans. M&uacute;sica: Quintana/ Najas. Del llibre <i>El R&egrave;quiem blau</i>. Ed. 62. Els llibres de l'Escorp&iacute;. Poesia, 195. Barcelona, 2002.<br>
	  Arranjaments: Bertran, Busquets, Llosas i Quintana. Francesc Bertran: guitarres. Jordi Busquets: guitarres, veus, baix. Xavi Llosas: Piano, teclats, rodhes, mini moog, veus, baix. Gerard Quintana: veu i veus.</p>
	  <p>
      <a target="_blank" href="popup.php?cancion=el_dinar_dels_muts"><img src="img/mp3.gif" border="0" align="absmiddle"><em>12. El dinar dels muts.</em></a><br>
      <a target="_blank" href="popup.php?cancion=he_perdut_el_mar"><img src="img/mp3.gif" border="0" align="absmiddle"><em>13. He perdut el mar.</em></a>
      </p>
      <a href="img/anex_les_claus.jpg" target="_blank"><img src="img/img.gif" align="absmiddle">Imatge</a>
	  </h4>
	  
	  <h4>
	  <img src="img/disco_epoca.jpg" title="Jordi Guardans Epoca" alt="Jordi Guardans Epoca" align="left">
	  <p><b><em>CD Ester Formosa. &quot;&Egrave;poca&quot;.</em></b>
	  <br>
	  Ed. Ventilador music. 2003.<br />
	  &quot;<em>El Pr&iacute;ncep llum&quot;.</em> Lletra i m&uacute;sica: Jordi Guardans.<em>&quot;El Messies&quot;.</em> <br>
	  Lletra i m&uacute;sica: Jordi Guardans.<br />
	  Direcci&oacute; musical i arranjaments: Maurici Villavecchia. Maurici Villavecchia: Piano i acordi&oacute;. &Agrave;ngel Pereira: Bateria i percussi&oacute;. Guillermo Prats: Contrabaix. Bernat Roman&iacute;: Guitarra.</p>
	  <p>
      <em>2. El Pr&iacute;ncep llum</em></a><br>
	  <em>13. El messies</em></a></p>
	  </h4>
	  
	  <h4>
	  <img src="img/disco_lunapau.jpg" title="Jordi Guardans Lluna de pau" alt="Jordi Guardans Lluna de pau" align="left">
	  <p><b><em>CD Maria del Mar Bonet. &quot;Lluna de pau&quot;.</em></b>
	  <br>
	  Enregistrament en directe al TNC. Ed. Picap. 2003. <br>
	  Recaptaci&oacute; &iacute;ntegra destinada a la campanya unit&agrave;ria Catalunya per Iraq. Suport humanitari.<br />
	  <em>&quot;L'ex&egrave;rcit&quot;.</em> Lletra i m&uacute;sica: Jordi Guardans. Arranjaments: Dani Espasa. Javier Mas: Guitarra de 12 cordes. Dani Espasa: Piano. Roger Blavia: Bateria i percussi&oacute;. Jordi Gaspar: Contrabaix. Lautaro Rosas: Guitarr&oacute;.</p>
	  <p><em>1. L'ex&egrave;rcit.&nbsp;</em></p>
	  </h4>
	  
	  <h4>
	  <img src="img/disco_casa_solitaria.jpg" title="Jordi Guardans La casa solitaria" alt="Jordi Guardans La casa solitaria" align="left">
	  <p><b><em>CD Ester Formosa. &quot; La casa solit&agrave;ria&quot;.</em></b>
	  <br>Ed. Zanfonia. 2000.<br /><em>&quot;Com plou sobre Berl&iacute;n&quot;.</em> Lletra i m&uacute;sica: Jordi Guardans. Arranjaments: S&iacute;lvia Comes. Roger Blavia: bateria. Jorge Sarraute: Contrabaix. Liba Villavecchia: Saxo sopr&agrave;. Mathew Simon: Trompeta. Maurici Villavecchia: Piano i teclats.</p>
	  <a target="_blank" href="popup.php?cancion=com_plou_a_berlin"><img src="img/mp3.gif" border="0" align="absmiddle"><em>8. Com plou sobre Berl&iacute;n</em></a></h4>
	  
	  <h4>
	  <img src="img/disco_estrella.jpg" title="Jordi Guardans Port Estrella" alt="Jordi Guardans Port Estrella" align="left">
	  <p><b><em>LP &quot;Port estrella&quot;.</em></b>
	  <br>
	  Ed. Pdi. 1988. Veu, lletra i m&uacute;sica: Jordi Guardans. Arranjaments: Jaime Stinus, Xavier Iba&ntilde;ez.
	  <em><p>Cara A.</p>
	  1. Port estrella.
	  <br>2. El secret.
	  <br>3. Vaig disfressat.
	  <br>4. Llum blanca.
	  <p>Cara B.</p>
	  1. Carroussel del cel.
	  <br>2. La mare de la nit.
	  <br>3. L'any nou.
	  <br>4. M'agraden les faules.</em>
	  </h4>
    </td>
</tr>
</table>

<p align="center"><br><br><a href="http://www.indianwebs.com" target="_blank" title="Dise&ntilde;o Web Barcelona" style="font:10px Arial, Helvetica, sans-serif;color:#cccccc;text-decoration:none;">Dise&ntilde;o Web Barcelona</a></p>
        
</body>
</html>