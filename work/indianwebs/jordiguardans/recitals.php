<?php 
$titulo="Recitals de Jordi Guardans. Web del poeta catal&agrave; Jordi Guardans.";
include("cabecera.php"); ?>

<tr>
	<td colspan="3">
      	<table cellspacing="0" cellpadding="6">
        <tr>
	  		<td><img title="Jordi Guardans" src="img/recitals_presentacio.gif" style="border:1px solid #333333;" alt="Contra venena et animalia venenosa"></td>
	  		<td><b><em>Presentaci&oacute; del llibre Contra venena et animalia venenosa.</em></b><br />A c&agrave;rrec d'Enric Bou. Lectura de poemes: Ester  Formosa i Jordi Guardans. Llibreria Ona, 17 de mar&ccedil; del 2009.</td>
	  	</tr>
        <tr>
	  		<td><img title="Jordi Guardans Recitals Turnez Sese" src="img/recitals_turnez_sese.jpg" alt="Jordi Guardans Recitals Turnez Sese"></td>
	  		<td><b><em>Roman&ccedil;os i estampes del 21.</em></b><br />Presentaci&oacute; del nou espectacle dels T&uacute;rnez &amp; Ses&eacute;.<br />Fira de la Mediterr&agrave;nia. Manresa, 30 i 31 d'octubre de 2008.</td>
	  	</tr>
        <tr>
	  		<td><img title="Jordi Guardans Recitals Silencia" src="img/recitals_silencia.jpg" alt="Jordi Guardans Recitals Silencia"></td>
	  		<td><b><em>SILENCIA. Concert per a la llibertat d'expressi&oacute; al m&oacute;n. Homenatge als defensors dels drets humans. POETES I CANTAUTORS.</em></b><br />Organitzat per Amnistia Internacional amb la col&middot;laboraci&oacute; de Reporters sense fronteres i el PEN catal&agrave;. Teatre Romea: Barcelona, 21 de juliol del 2008.</td>
	  	</tr>
      	<tr>
	  		<td><img src="img/recitals_paranoia.jpg" title="Jordi Guardans Recitals Paranoia" alt="Jordi Guardans Recitals Paranoia"></td>
	  		<td><b><em>Recital de poemes. <i>Paranoia Accions</i>.</em></b><br>Igualada, 24 d'abril de 2008, a L'ou com balla.</td>
      	</tr>
		<tr>	
	  		<td><img src="img/recitals_balalaika.jpg" title="Jordi Guardans Recitals Hospitalet" style="border:1px solid #cccccc;" alt="Jordi Guardans Recitals Hospitalet"></td>
	  		<td><b><em>Presentació del llibre <i>Planeta Balalaika</i>.</em></b><br>A c&agrave;rrec d'en Feliu Formosa i d'en Gerard Quintana.<br>Lectura de poemes: Núria  Martínez i Vernis i Carles Rebassa.<br>Llibreria Ona, 30 de gener  de 2008.</td>
		</tr>
		<tr>
	  		<td><img src="img/recitals_marquet_roques.jpg" title="Jordi Guardans Recitals Marquet Roques" alt="Jordi Guardans Recitals Marquet Roques"></td>
	  		<td><b><em>III Cicle d'Estiu del Marquet de les Roques.</em></b><br>
	  		Sant Lloren&ccedil; Savall. 21 de juliol  del 2007. <br>Recital po&egrave;tic i can&ccedil;&oacute; (poemes i can&ccedil;ons -lletra i m&uacute;sica- de Jordi  Guardans): &quot;Els &Agrave;ngels i el perill&quot;. <br>Jordi Guardans: veu. Ester  Formosa: veu i can&ccedil;&oacute;. Maurici Villavecchia: piano.</td>
		</tr>
		<tr>
	  		<td><img title="Jordi Guardans Recitals Ateneu" src="img/recitals_ateneu.jpg" alt="Jordi Guardans Recitals Ateneu"></td>
	  		<td><b><em>Poesia a cau d'orella.</em></b><br>5 de Maig del 2007<br>A l'Ateneu Barcelon&egrave;s. Recital po&egrave;tic col.lectiu.</td>
		</tr>
		<tr>
	 		<td><img src="img/recitals_hospitalet.jpg" title="Jordi Guardans Recitals Hospitalet" alt="Jordi Guardans Recitals Hospitalet"/></td>
	  		<td><b><em>Festival BarnaSants. Centre Barradas. L'Hospitalet de Llobregat.</em></b><br>18 de febrer del  2007.<br>Recital po&egrave;tic i de can&ccedil;&oacute; (poemes i can&ccedil;ons -lletra i m&uacute;sica- de Jordi  Guardans):&nbsp; &quot;Els &Agrave;ngels i el perill&quot;.<br>Jordi Guardans: veu. Ester  Formosa: veu i can&ccedil;&oacute;. Maurici Villavecchia: piano.</td>
		</tr>
		<tr>
	  		<td><img src="img/recitals_kinzena_poetica.gif" title="Jordi Guardans Recitals Marquet Roques" style="border:1px solid #cccccc;" alt="Jordi Guardans Recitals Marquet Roques"></td>
	  		<td><b><em>KINZENA POETIKA.</em></b><br>Vilafranca del Pened&egrave;s.<br>20 d'octubre del  2006.<br>Recital po&egrave;tic sobre <em><i>Els &Agrave;ngels i el perill</i></em>.</td>
		</tr>
		<tr>
	  		<td><img src="img/recitals_luz.jpg" title="Jordi Guardans Luz Casal" alt="Jordi Guardans Luz Casal"></td>
	  		<td><b><em>Presentaci&oacute; de la biografia de Luz Casal <i>Mi memoria es agua. Luz</i> de Magda Bonet.</em></b><br>Presentaci&oacute; a la F&agrave;brica Moritz, el 24 d'abril de 2006.</td>
		</tr>
      	</table>
    </td>
</tr>
</table>

<p align="center"><br><br><a href="http://www.indianwebs.com" target="_blank" title="Dise&ntilde;o Web Barcelona" style="font:10px Arial, Helvetica, sans-serif;color:#cccccc;text-decoration:none;">Dise&ntilde;o Web Barcelona</a></p>
        
</body>
</html>