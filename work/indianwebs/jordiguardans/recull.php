<?php 
$titulo="Jordi Guardans Recull. Web del poeta catal&agrave; Jordi Guardans.";
include("cabecera.php"); ?>

<tr>
	<td colspan="3" width="100%" style="background:#000000;" align="right" title="Recull Foto. Mart&iacute; Fradera 2008"><img src="img/jordi_guardans_recull.jpg" alt="Recull Foto. Mart&iacute; Fradera 2008" /></td>
</tr>
<tr>
	<td colspan="3" align="center"><div class="txtblanco">Contra venena et  animalia venenosa.(2008)</div></td>
</tr>
<tr>
	<td class="celda_recull">
         	
        <p><b>PRIMERA  ANUNCIACI&Oacute;</b></p>
        <p>&ldquo;Potser que hi hagi falses anunciacions,<br />gr&agrave;cies d&rsquo;un dia, aut&egrave;ntics trencacolls de<br />
          l&rsquo;&agrave;nima, abisme, abisme on ha anat a <br />abocar-se l&rsquo;ocell espl&egrave;ndidament trist de la <br />divinaci&oacute;&rdquo;.</p>
        <p>Andr&eacute; Breton. <b>Nadja</b>.</p>
        
        <p>&ldquo;Rec&oacute;rrer al somni no vol dir marxar de <br />casa; significa escorcollar el graner, on la <br />nostra inf&agrave;ncia prenia contacte amb la <br />poesia&rdquo;.</p>
        <p>Jean Cocteau. <b>Carta a  Jacques Maritain</b>.</p>
        
        <p>A la cambra de jugar de casa meva<br />a la nit sentir un senyal de  llumeneta, <br />
          plens de cel els papers blancs  quan jo hi anava<br />amb el llapis impossible de  guspires,<br />
          molt content d&rsquo;aquell contacte,  de la crida.<br />Paper blanc, de blanc escrit,  blanca lectura, <br />
          carta molt familiar, bonica,  clara,<br />amb el nom que jo tenia, diferent<br />
          al que em deien al voltant tota  l&rsquo;estona.<br />El bateig d&rsquo;aquesta vida era  fictici, <br />
          tot plegat un simulacre:  aleshores<br />era un nen i el que sentia m&eacute;s de  veres<br />
          era el nom que encap&ccedil;alava  aquella carta.<br />Dir-me el nom a mitjanit la  meravella,<br />
          recordant una bellesa  inolbidable,<br />ja no era un estranger en el viatge,<br />
          llum intensa de vaixell Stella  Solaris<br />navegant per tots els mars  mediterranis<br />
          dibuixats al cel content, amb un  llenguatge<br />invisible com el cor de criatura<br />
          que no parla, que no parla, que  no parla.<br />Vaig callar com vaig poder durant  tres dies,<br />
          o tres mesos, o tres anys o  tres-cents contes,<br />vaig quedar-me mut del tot per  defensar-me<br />
          per no ser mai tra&iuml;dor d&rsquo;una  paraula.<br />La fam&iacute;lia de gent semblava estranya,<br />
          tots havien oblidat la seva casa,<br />el seu nom, el seu vestit i el  seu silenci.<br />
          Per&ograve; clar, vaig ser parit com  tots nosaltres<br />i sabia que m&eacute;s tard, potser m&eacute;s  d&rsquo;hora,<br />
          dominat pel rar perfum que  m&rsquo;envoltava<br />m&rsquo;aniria oblidant sense adonar-me<br />
          del que avui nom&eacute;s recordo sense  dia.<br />He crescut, serveix de poc el  nom, les cartes,<br />
          les que no puc explicar perqu&egrave; eren blanques.<br />Nom&eacute;s s&eacute; que em donen for&ccedil;a a la  gran lluita<br />
          quan em veig que no s&oacute;c home i ho  s&oacute;c massa,<br />ja he menjat i vomitat moltes  paraules,<br />
          per&ograve; tinc com una estranya  malaltia,<br />mig de de boig, d&rsquo;enamorat i de  poeta,<br />
          a la cambra de jugar de casa meva<br />a la nit sentir un senyal de  llumeneta. <br />
          Sempre m&eacute;s un nom secret i de  fortuna,  <br />no s&eacute; quan potser entendr&eacute; com  vaig entendre<br />
          de trencar el meu silenci  agosarant-me<br />a dir una paraula trista,  compromesa,<br />
          amb l&rsquo;esfor&ccedil; de ser prec&iacute;s amb la  m&eacute;s clara,<br />com lliurat profundament al  sacrifici.<br />
          De cam&iacute; per carretera enmig dels  boscos,<br />amb els pares i germans, mentre  xerraven,<br />
          va ser un gran esfor&ccedil; dir <b>casa </b>assenyalant-la<br />enmig d&rsquo;una gran muntanya  enverdissada.<br />
          I despr&eacute;s va ser el descans i la  vinguda<br />de la serp m&eacute;s verinosa,  compartida<br />
          amb el riure de tothom, perqu&egrave;  parlava. <br />Aquest cop ja vaig fer meu el nom  de guerra,  <br />
          nom real, realitat que s&rsquo;imposava<br />com si fos un musical nou i  dif&iacute;cil  <br />
          i que hauria d&rsquo;esperar una  temporada.<br />No va ser fins als nou anys a la  pantalla,<br />
          un paraigua i mainadera que  volaven<br />amb senyals codificats per  criatures,<br />
          que els seus &agrave;vids enemics no  imaginaven,<br />en un cel irresistible: blavejava<br />
          amb un blau tan poder&oacute;s que va  ensenyar-me<br />que hi havia una preg&agrave;ria dels  homes,<br />
          dels nens grans que compartien la  meva &agrave;nima.<br />Recordava el nom callat quan ja  parlava,<br />
          i el recordo avui prec&iacute;s i sense  entendre,<br />com tampoc s&rsquo;ent&egrave;n la m&uacute;sica que  estimes.<br />
          Als cinc anys ja responia al meu  nom d&rsquo;ara,<br />sense cap reserva forta o veu en  contra,<br />
          aprenent de mica en mica quan  jugava<br />com un nen, qualsevol nen, a fer  viatge.<br />
          Al moment just la trobada amb un  piano<br />que de sobte va tenir la meva al&ccedil;ada,<br />
          em dictava no s&eacute; qui la melodia,<br />per tocar-la infantilment per&ograve;  amb fermesa.<br />
          &Eacute;s l&rsquo;anunci d&rsquo;un secret, potser  no ens deixa?<br />Vaig pensar el que ara avui em  sembla entendre:<br />
          que vivia de les meves cartes blanques,<br />que la vida &eacute;s una guerra  necess&agrave;ria,<br />
          i  que el nom original ha d&rsquo;oblidar-se,<br />estrat&egrave;gia d&rsquo;amor per  protegir-nos,<br />
          perqu&egrave; es viu fins al final  acompanyada<br />d&rsquo;unes ales, d&rsquo;unes ales, d&rsquo;unes  ales.<br />
          El meu cas: musicalment em protegeixo<br />de la serp, que ja he dit, tan  seductora<br />
          que vol veure&rsquo;ns caminant caiguts  per terra<br />per no ser, segur, mai m&eacute;s nens  perillosos,<br />
          adormits i enverinats de boira  adulta. <br />Algun dia vaig nevar-me a la  infantesa<br />
          amb la neu que &eacute;s dins del nen  que no es controla,<br />era el cim que jo seria, poca  cosa,<br />
          fins al punt que estic mirant les  serralades,<br />la not&iacute;cia de dins de ser  muntanya<br />
          la recordo lleu i blanca i  voladora,<br />com el conte preci&oacute;s,  extraterrestre,<br />
          que defenso ben callat perqu&egrave; no  riguin<br />els que es van atrevir a riure&rsquo;s  d&rsquo;un messies<br />
          que era el cim m&eacute;s alt, segur, de  molts planetes,<br />conservant nom&eacute;s ell sol el nom  d&rsquo;origen<br />
          fins la sang, el sacrifici i la  trag&egrave;dia.<br />Recordava m&eacute;s que tots, veu  infinita,<br />
          no va haver de morir mai ni quan  moria<br />ple d&rsquo;enorme patiment de gran  poeta.<br />
          Si pateixo jo nom&eacute;s pateixo  engrunes,<br />si defenso s&oacute;c covard en la  defensa,<br />
          que la b&egrave;stia fa trampes  poderoses<br />i s&rsquo;espanta el cap quan veu el  que s&rsquo;acosta<br />
          tot caient dins de l&rsquo;infern dels  que ens imposen<br />una mort definitiva, sempre  morta.<br />
          Com si fos una condemna  esperan&ccedil;ada <br />per la mort del que la mort ens  significa, <br />
          que l&rsquo;inici de la festa que  celebra<br />la utopia, noms secrets amb que  els nens neixen,<br />
          porta encara programada una  matan&ccedil;a. <br />Hem de ser Reis d&rsquo;Orient  interminables, <br />
          ben humils, sense esperar grans  meravelles.<br />Tots plegats en un desert.  Seguint estrelles. </p>
	</td>
    <td class="celda_recull" colspan="2">

    <p><b>NOVENA  ANUNCIACI&Oacute;</b></p>
    <p>&lt;&lt; Nom&eacute;s hi ha dues maneres  de guanyar la <br />
      partida: jugar cors o fer  trampes. Fer trampes &eacute;s <br />
      dif&iacute;cil. Un tramp&oacute;s atrapat perd.  La gran ra&ccedil;a dels <br />
      bandarres, no se l&rsquo;atrapa mai.  (&hellip;) Jugar cors &eacute;s <br />
      senzill. Cal tenir-ne, vet-ho  aqu&iacute;. Us creieu sense <br />
      cor. Mireu malament les vostres  cartes. El vostre <br />
      cor s&rsquo;amaga per por de fer el  rid&iacute;cul i per <br />
      obedi&egrave;ncia a un antic codi  criminal: &ldquo;Ja tenim aqu&iacute; <br />
      el temps dels assassins&rdquo;. Mostreu  el vostre cor i <br />
      guanyareu. Ja tenim aqu&iacute; el temps  de l&rsquo;amor. La <br />
      dolenteria s&rsquo;ha fet servir massa,  es gasta. La <br />
      bondat, nova de trinca, permet  combinacions <br />
      sorprenents &gt;&gt;.</p>
    <p>Jean Cocteau. <b>Carta a Jacques Maritain.</b></p>
            <p>Ens regalen molts rellotges  verinosos<br />
      que ens despisten, marquen hores  perilloses,<br />
      no suporten les campanes ni les  mares<br />
      perqu&egrave; els nens van aprenent  totes les hores<br />
      en sentit de jugar fort. Hi ha  l&rsquo;amena&ccedil;a<br />
      d&rsquo;un futur de replicants fets  patrimoni<br />
      dels que maten l&rsquo;esperit, el del  principi<br />
      de la hist&ograve;ria d&rsquo;un amor per a  portar-nos<br />
      a fer cel m&eacute;s del pensat. Tenim  planetes<br />
      ben a prop. Per qu&egrave; sentim exili  d&rsquo;&agrave;nima?<br />
      Potser aqu&iacute; hi ha algun problema  de disfressa<br />
      d&rsquo;animal molt perill&oacute;s amb pell  humana.<br />
      Per qu&egrave; som signes de mort entre  nosaltres?<br />
      Nom&eacute;s goso de pensar que ser  poeta<br />
      es fa forma d&rsquo;intuir la gran  trag&egrave;dia.<br />
      A les fosques sento amor, em d&oacute;na  pistes<br />
      de l&rsquo;enorme laberint, li veig  sortida<br />
      en met&agrave;fora solemne i delicada<br />
      d&rsquo;una veu d&rsquo;humanitat que tot ho  plora<br />
      enyorant un no s&eacute; qu&egrave;, sempre a  l&rsquo;espera.<br />
      I el ver&iacute; d&rsquo;alguna estrella es  configura<br />
      com el m&agrave;xim responsable, no  descansa<br />
      d&rsquo;alletar-nos letalment de vida  estranya<br />
      per a fer-nos esperits sense  sortida.<br />
      Potser &eacute;s l&rsquo;hora dels rellotges  m&eacute;s fiables<br />
      dels que perden, dels m&eacute;s pobres  de la vida,<br />
      sense veu es manifesten un  ant&iacute;dot<br />
      als efectes verinosos dels qui  manen.<br />
      I si f&oacute;ssim convidats a  compartir-nos<br />
      amb la seva perspectiva de  l&rsquo;origen?<br />
      A la NASA cal amor entre les  m&agrave;quines,<br />
      si l&rsquo;amor va amb la mateixa  rapidesa<br />
      que la llum, doncs per qu&egrave; mai no  l&rsquo;utilitzen?<br />
      Hi ha el ver&iacute; a tots els contes  que ara em v&eacute;nen,<br />
      el dolor es fa col&middot;lectiu i no  se&rsquo;n parla,<br />
      no oblido que mil creus o m&eacute;s  encara<br />
      fan la creu, la creu de veres,  companyia.<br />
      Sempre ploro quan la sento  fracassada<br />
      plenament, nom&eacute;s la trobo ben  suada<br />
      en els bars foscos i buits besant  ap&ograve;stols,<br />
      que no ho saben ni que ho s&oacute;n,  plens de ferides.<br />
      Ara es tracta de callar en  estimar-se,<br />
      un lligar moltes persones fent  planeta<br />
      amb l&rsquo;amor dignificant els que  m&eacute;s callen.<br />
      Compartint entre nosaltres els  enigmes,<br />
      demostrant-nos que som vides  connectades<br />
      amb la sang, plena de nits  inexplicables<br />
      on s&rsquo;encenen just als v&egrave;rtex les  pir&agrave;mides<br />
      i alg&uacute; troba un avi&oacute; mort a la  sorra.<br />
      La preg&agrave;ria m&rsquo;arrossega per  farm&agrave;cies,<br />
      per salvar-me del ver&iacute; que  s&rsquo;inocula,<br />
      no vull mai que sigui meva la  desgr&agrave;cia<br />
      de ser sord a la pres&egrave;ncia  delicada,<br />
      me&rsquo;n recordo del senyal de  llumeneta<br />
      quan d&rsquo;infant em va ensenyar  l&rsquo;abecedari<br />
      ja oblidat, nom&eacute;s recordo una A  clara,<br />
      la d&rsquo;amor, la d&rsquo;aventura i la  d&rsquo;&agrave;nsia,<br />
      m&rsquo;acompanya, m&rsquo;acompanya,  m&rsquo;acompanya.<br />
      Ara tinc alguna edat que ja  m&rsquo;indica<br />
      matrimoni d&rsquo;infantesa i de  vellesa,<br />
      un senyal de maduresa descartada.<br />
      La bandera del bressol despr&eacute;s de  n&eacute;ixer<br />
      fa que aprengui d&rsquo;una p&agrave;tria  amena&ccedil;ada,<br />
      amena&ccedil;a general de vida humana.<br />
      No serem pas replicants, tenim  mem&ograve;ria,<br />
      del Graal em ve desig, encara el  sento<br />
      despullat a dins del llit fent  mil car&iacute;cies<br />
      amb l&rsquo;amant amb qui em faig vell.  Besant m&rsquo;inspira<br />
      una copa de ver&iacute; que ha de  trencar-se.<br />
      Veniu tots els que m&rsquo;estimo  aquesta tarda,<br />
      tots els morts i tots els vius  per retrobar-me,<br />
      per parlar de l&rsquo;estrat&egrave;gia que  perdura<br />
      d&rsquo;amistat i de bellesa, ning&uacute;  perdi<br />
      ni una pista pel viatge que ens  espera<br />
      ajudant-nos com puguem entre  nosaltres.<br />
      El ver&iacute; em debilita quan m&rsquo;hi  enfronto,<br />
      vaig sentint una tristesa, tot em  cansa,<br />
      nom&eacute;s tinc ganes de llit per  adormir-me,<br />
      perqu&egrave; em senti ben malalt i no  segueixi<br />
      insistint en el poema que s&oacute;c  ara.<br />
      Se m&rsquo;acusa d&rsquo;assaltar la nit  humana<br />
      com nadons cridant la gana a  totes hores,<br />
      potser s&iacute;, el crit d&rsquo;infant  sempre m&rsquo;atrapa<br />
      i supera que deserti del poema.<br />
      S&oacute;c atac quan veig instants que  s&rsquo;abandonen<br />
      a llen&ccedil;ar menjar d&rsquo;amor a les  deixalles<br />
      perqu&egrave; &eacute;s mengi b&eacute; a la festa  fastigosa<br />
      dels que ronden o festegen massa  b&egrave;sties<br />
      verinoses, perfumades de  vict&ograve;ria,<br />
      que han pactat que l&rsquo;esperit es  posi en venda.<br />
      Ens vendrem a les rebaixes que  ens pertoquen?<br />
      Som molt cars, ho som avui i ho  serem sempre,<br />
      formem part d&rsquo;una gran festa  embarassada,<br />
      cadasc&uacute; en forma part com ho  endevina.<br />
      S&oacute;c petit, vomito mar, no s&eacute;  estar a soles<br />
      tinc un nen i &eacute;s del cristall  protagonista<br />
      que no s&eacute; pas ni d&rsquo;on ve ni cap  on porta.<br />
      Me l&rsquo;estimo, per ell visc i per  ell lluito,<br />
      per ell prenc for&ccedil;a ant&iacute;dots  farmac&egrave;utics,<br />
      vull ser fort fins on permeti la  tendresa<br />
      d&rsquo;aquest m&oacute;n, la que a vegades no  practico,<br />
      la que costa, la que costa, la  que costa.<br />
      Imagino les campanes de l&rsquo;ermita<br />
      ben fidels a l&rsquo;anar fent d&rsquo;aquell  que resa<br />
      fent-se bosc que va creixent en  el silenci<br />
      insistent, malgrat el crit quan  cau un arbre<br />
      massa gran i massa vell com  perqu&egrave; caigui.<br />
      Cop de vent que porta a dins tot  la r&agrave;bia.<br />
      A la lluita hi ha un su&iuml;cidi rera  l&rsquo;altre,<br />
      en el metro viu la dona que &eacute;s  molt bona<br />
      llan&ccedil;ant flors a la sang seca de  les vies,<br />
      i aqu&iacute; tinc un gran amic que va  llan&ccedil;ar-s&rsquo;hi<br />
      esperant una gran dama a l&rsquo;altra  banda<br />
      que cregu&eacute;s en el palau d&rsquo;on ell  venia.<br />
      Em resulta molt dif&iacute;cil  l&rsquo;alegria,<br />
      ens trepitgen, ens trepitgen, ens  trepitgen.<br />
      Fan fa&ccedil;anes oblidant-se les  finestres,<br />
      els va b&eacute; que la pudor mai es  ventili,<br />
      beneint oracions d&rsquo;orgull  sinistre<br />
      amb molts segles de mem&ograve;ria  corrompuda<br />
      perqu&egrave; es perdi l&rsquo;argument de  l&rsquo;aventura<br />
      de l&rsquo;amor. L&rsquo;esfor&ccedil; molt gran  d&rsquo;inflar les veles<br />
      a favor de la pres&egrave;ncia callada<br />
      o no tant. Hi ha la gran m&uacute;sica  sonora<br />
      de la llum, la que projecta  aquest planeta<br />
      fins a l&iacute;mits impensables de  gal&agrave;xies.<br />
      Ple de sants, no estic parlant de  cap esgl&eacute;sia<br />
      ni de molts dels manaments per a  manar-nos,<br />
      ensenyant com basta el cor per  fer contacte<br />
      amb la flama d&rsquo;enyoran&ccedil;a bene&iuml;da.<br />
      Aquest m&oacute;n, <b>contra venena et animalia</b><br />
      <b>venenosa</b>, &eacute;s el meu, el que  m&rsquo;ataca<br />
      aquell punt m&eacute;s vulnerable,  consci&egrave;ncia <br />
      que alg&uacute; em vol inconscient, foll  sense mida.<br />
      Verinosa soledat fent-se amena&ccedil;a,<br />
      que el cristall del nen que  estimo tingui tra&ccedil;a.<br />
      Corre al bosc un c&eacute;rvol blanc i  ning&uacute; el ca&ccedil;a!</p>
  	</td>
</tr>
<tr>
	<td colspan="3" align="center"><div class="txtblanco">Planeta Balalaika (2007)</div></td>
</tr>
<tr>
    <td class="celda_recull">
       <p>Enyorar la veu d'Orfeu<br>&egrave;s un privilegi<br>i una condemna,<br>fins al punt que no hi ha <br>
          veu enlloc<br>que la recordi:<br>fins la m&eacute;s voladora <br>de les veus<br>
          cau sobtadament<br>als tres minuts<br>a l'abisme<br>de la meravella falsa.<br>
          La veu d'Orfeu<br>no tothom la plora,<br>faria diamant<br>el planeta per&ograve;<br>
          una Mort no ho vol.<br>Aix&iacute; doncs<br>aplaudim<br>les veus humanes <br>
          molt agra&iuml;ts:<br>les estrelles<br>encara no s&oacute;n nostres<br>i ara per ara<br>
          trobar la veu d'Orfeu<br>fa de la poesia<br>malaltia terminal.</p>
     </td>
     <td class="celda_recull" colspan="2">    
          <p>Gla&ccedil;at &eacute;s el vals de  matinada<br>del Sant Petesburg que  m'acompanya,<br>ballant amb desig vull  abra&ccedil;ar-lo <br>i rebre el dolor de les  car&iacute;cies<br>cansades, potents i  putejades,<br>bragueta de R&uacute;ssia  inconsolable<br>
          que guarda l'estrella  trepitjada<br>que va fer de proa de les  &agrave;nimes.<br>Tots dos compartint  cristall en vena,<br>&eacute;s droga que ve de neu  donada,<br>
          no cal que no em pugui  regalar<br>la icona trencada d'ell  mateix:<br>n'hi ha prou amb la veu i  com em mira,<br>tot ell &eacute;s la for&ccedil;a  desitjada<br>
          de llum, de bellesa i de  nit blanca<br>per fer-me tornar de nou  a casa.<br>Tots dos hem ballat  aquesta nit<br>i el fred no pot ser tan  miserable,<br>
          s'estengui amb el vals,  de matinada,<br>aquesta vegada ple de  sort,<br>l'al&egrave; del planeta  Balalaika.</p>
	</td>
</tr>
<tr>    
    <td class="celda_recull">
        <p>Aquesta setmana &eacute;s Santa:<br>hi ha hagut nova ferida  mortal<br>
          per la voluntat dels  morts<br>que volen matar vida.<br>
          Laberint de tra&iuml;cions,<br>crims i riallades<br>
          que s'han menjat l'&agrave;nima<br>i el planeta. La tristesa<br>
          l'acompanya a cuinar<br>el millor que sap: vol  fer,<br>
          de la carn sacrificada,<br>de les faves i del brou<br>
          guspires de celebraci&oacute;<br>de la bona sang  terrestre, <br>
          esperan&ccedil;a extraterrestre  i<br>festa de tots els sants.<br>
          T&eacute;, procura, la cuina  neta:<br>i per les rajoles i l'extractor<br>
          regalimen gotes de suor i<br>de ll&agrave;grimes.</p>
      </td>
      <td class="celda_recull" colspan="2">   
        <p>En aquest m&oacute;n de totes  les inc&ograve;gnites<br>no farem grans sermons,  nom&eacute;s un misto<br>
          ens encendr&agrave; la flama de  la cuina<br>del menjar vell que ens  guia en el viatge.<br>
          Ensumarem la Terra a les  safates,<br>i ens omplirem els plats amb l'esperan&ccedil;a<br>
          que aquest viatge t&eacute; una  perspectiva:<br>som des de Mart planeta  diminut<br>
          i tot seguit un punt, la  nostra lluna;<br>som electrons, petons i  rovellons<br>
          entre la molsa. Amor i  fortuna<br>amb all i julivert, el  cor de brasa,<br>
          &eacute;s el retrat, la mida del  poeta<br>que com tothom l'acaba  vessant sempre.<br>
          Per&ograve; el que visc ho dono  al telescopi<br>t&iacute;midament, s&oacute;c volts que  van de festa:<br>
          ple de desig dels peus  quan es despullen,<br>faig comuni&oacute;, rampell i  bona cosa,<br>
          descal&ccedil;a fam de pluja  bene&iuml;da:<br>aigua i cristall, cometes  amb muntanyes:<br>
          imaginant l'exili que s'acaba.</p>
	</td>
</tr>


<tr>
	<td colspan="3" align="center"><div class="txtblanco">Abb&agrave; (2006)</div></td>
</tr>
<tr>
   	 <td class="celda_recull">
     <p>Des de fa segles<br>que es bateja<br>majorit&agrave;riament<br>amb aigua corrupta<br>de les clavegueres,<br>i aquest Papa ho sap<br>
     quan reguen amb cucs<br>jardins frondosos;<br>per&ograve; els boscos s'imposen <br>plens de secrets de pluja<br>
     a les fulles, i de bolets<br>que il.luminen<br>els que es descalcen<br>per ficar-se als rierols<br>
     que encara queden,<br>i alguns s&oacute;n Baptistes<br>al.lucinats que veuen<br>en l'aigua un cristall<br>
     per batejar tot el  planeta.<br>I van de rierol en rierol<br>esquivant l'aigua f&egrave;tida,<br>a voltes disfressada de  neu<br>pels miserables del fang<br>i de la sang, que es  barregen <br>
     per fer de la vida <br>una pel.l&iacute;cula de por<br>amb aigua enverinada<br>plena de v&iacute;ctimes <br>
     amb cor de c&eacute;rvols.<br />Als set anys <br>se'm va escapar <br>
     el barquet que tenia;<br>em vaig ficar a l'estany <br>a cercar-lo,<br>amarat fins al coll,<br>
     amb aquella alegria <br>de l'aigua quan ens  estima.<br>Tothom em cridava,<br>per&ograve; el barquet i jo  quiets,<br>jugant amb Abba.</p>
     </td>
     <td class="celda_recull" colspan="2">  
      <p>Monedes falses per pagar  desgr&agrave;cies,<br>aquell amor que amagava  la trampa,<br>que t'ha robat el sol i  la teva ombra,<br> i has de comprar de  pressa una ambul&agrave;ncia,<br>perqu&egrave; has caigut m&eacute;s alt  del que es pot caure,<br>malalt i greu plorant qualsevol planta.</p>
      <p>Monedes falses per pagar  desgr&agrave;cies,<br>que t'han venut l'infern  inevitable,<br>i et veus ficat al bosc  que mata els arbres,<br>per si de cas et tornes  miserable.<br>Tu pagar&agrave;s moneda sempre  falsa,<br>Si no tens canvi paga m&eacute;s  i corre.</p>
      <p>Monedes falses per pagar  desgr&agrave;cies,<br>per&ograve; la sang &eacute;s sempre  verdadera,<br>or i dolor &eacute;s la teva  mirada,<br>i per aix&ograve; tu tens amb  qu&egrave; pagar-te;<br>que el diner fals et  salvi d'un martiri,<br>tra&iuml;ci&oacute; de Judes  fracassada.</p>
     </td>
</tr>
<tr>        
	<td class="celda_recull">
     <p>S&oacute;n aqu&iacute; quan s'acosten  com moixons<br>o quan ploren un plugim  perqu&egrave; et consolis,<br>t'han anat ensenyant  l'infern de mica en mica,<br>perqu&egrave; t'anessis  protegint de les cremades,<br>
     salvant el teu nom de  naixement que no saps.<br>Segueix el llampec de la  bona mort:<br>un far vulnerable que &eacute;s  un nou aeroport <br>per les seves vides a la  Terra.</p>
    </td>
    <td class="celda_recull" colspan="2">
      <p>Passeja amb mi <br>sota la Lluna del perd&oacute;,<br>no em deixis sol:<br>em segueixen els  assassins <br>de la infantesa que em  queda,<br>sentenciada a cadena  perp&egrave;tua.</p>
      <p>Si arribo a vell<br>morint-me de fred,<br>voldria que m'acab&eacute;s de  matar<br>all&ograve; que fa la neu m&eacute;s  pura.</p>	

    </td>
</tr>


<tr>
 	<td colspan="3" align="center"><div class="txtblanco">El R&egrave;quiem blau (2002)</div></td>
</tr>
<tr>  
    <td class="celda_recull">
            <p>Hi haur&agrave; nens a la corona  dels pirates<br>com sempre des que els  jocs rondinen<br>i fan plorar les nenes  que jugant amb la filosa<br>es punxen mentre aprenen  a cosir flors blaves.</p>
        <p>Per&ograve; tamb&eacute; hi ha nens que  miren solitaris<br>el jard&iacute; de flors blaves  de l'exili,<br>i nenes que es vesteixen  de corsari<br>cansades de tanta i tanta  lluna.</p>
        <p>En el pati de jocs de les  escoles<br>fins i tot hi ha un ta&uuml;t  de fusta blanca<br>per aquell mort menut que  encara juga<br>a no cr&eacute;ixer mai m&eacute;s  entre nosaltres.</p>
        <p>Criatures amb jocs de  tota mena <br>a les cinc totes s&oacute;n ben  llamineres,<br>i acostant-se al ta&uml;t  cobert de dol&ccedil;os<br>els agafen i es fan un  fart de pena.</p>
        <p>A les set de la tarda de  la vida,<br>quan retornin a casa vora  els pares,<br>entrepans de cristall i  melangia<br>i algun conte de l'hora  que els estima.</p>
     </td>
     <td class="celda_recull" colspan="2"> 
		<p>Vellut vermell de  cardenals de sofre que somriuen<br>mentre es masturben folls  trencant espelmes,<br>perqu&egrave; anhelen conspirar  ben a les fosques<br>bevent amb calzes de  tra&iuml;ci&oacute; la sang dels m&agrave;rtirs<br>per tal de fer de Crist  un r&egrave;ptil tr&agrave;gic.</p>
        <p>Sorra usurpadora de  maragdes,<br>senyals d'alerta que ja  no alerten,<br>fr&agrave;gil cristall que espera  el terratr&egrave;mol,<br>cavall desbocat per la  tempesta<br>de llunes mortes de  l'assass&iacute; de llunes:<br>l'home de l'entranya  verinosa,<br>sempre culpable de  qualsevol ll&agrave;grima vessada,<br>fins i tot de la del nen  que plora a les ant&iacute;podes <br>del seu llit de preg&agrave;ries <br>al vellut vermell dels  cardenals sinistres.</p>
        <p>Aquest home va a  l'esgl&eacute;sia i resa i mata,<br>i menja els escarabats  quan va i combrega,<br>t&eacute; fills i fills vestits de negre;<br>algun d'entre ells que  vol salvar-se<br>va i es despulla i  despullat comen&ccedil;a <br>pelegrinatges de cadires  velles<br>buscant miracles  d'ereccions d'estrelles <br>i fonts de mel per  netejar les venes<br>amb la suor del seu front  tr&agrave;gic.</p>
        <p>L'home influent de les  medalles d'ossos<br>es pot trobar a massa  cantonades,<br>fins i tot surt a les not&iacute;cies<br>ambaixador de l'aigua clara<br>inaugurant fanals i  llunes<br>on els amants puguin  robar-se <br>la sort del m&oacute;n m&eacute;s  delicada.</p>
        <p>Mor un cavall d'un llamp  de r&agrave;bia.</p>
    </td>
</tr>
<tr>    
    <td class="celda_recull">
    	<p>El senyor de les rates <br>m'ha fet una visita<br>quan jo no l'esperava,<br>m&eacute;s ben dit quan dinava<br>a poc a poc i amb gana.</p>
        <p>Jo que li he obert la  porta<br>i el senyor de les rates<br>m'ha fet una ganyota<br>mentre se'm presentava:<br>'Vinc a veure si plora'.</p>
        <p>He callat amb prud&egrave;ncia<br>malgrat veure amb molt f&agrave;stic<br>com llen&ccedil;ava pel terra<br>del repl&agrave; de l'escala<br>un munt de rates mortes.</p>
        <p>Ha fet una gran burla:<br>'S&oacute;n les rates que neixen <br>i moren per vost&egrave;,<br>no fos cas que cant&eacute;ssiu<br>massa b&eacute;, com els &agrave;ngels.'</p>    
		
  	</td>
    <td class="celda_recull" colspan="2">
		<p>&nbsp;</p>
    </td>
</tr> 


<tr>
 	<td colspan="3" align="center"><div class="txtblanco">Tant de bo que siguin les pen&uacute;ltimes penes (1994)</div></td>
</tr>     
<tr>
    <td class="celda_recull">
        <p>C&agrave;lid sol de l'alba,<br>llum que el blat  desperta,<br>ja ve el m&oacute;n dels &agrave;ngels,<br>al foll de la plana,<br>que escriu a les fulles<br>tots els noms que estima.</p>
        <p>El foll veu els &agrave;ngels<br>fent les guerres santes,<br>quan de nit es mengen <br>els insectes negres<br>que ha enviat la lluna<br>en nits sense estrelles.</p>
        <p>El foll no t&eacute; cura,<br>diu que el blat li parla<br>d'un planeta m&agrave;gic<br>on pot arribar-se<br>menjant pa tothora.<br>Pa de molla blanca.</p>
    </td>
    <td class="celda_recull" colspan="2">   
        <p>La teva veu ja tan  cansada<br>de defensar l'home que  portes<br>s'ha fet m&eacute;s lleu, tamb&eacute;  m&eacute;s fr&agrave;gil,<br>i f&agrave;cilment, ben suau,  s'apaga<br>com una espelma sense  aire.</p>
        <p>Per&ograve; sovint de cop  s'anima<br>i ja no sap com  aturar-se,<br>i mentre explica coses  vanes<br>vol conquerir el silenci <br>on s'inauguren noves  formes.</p>
        <p>Surten del cor les ones  blaves<br>com un lament sense  frontera<br>ni d'assassins ni de  cad&agrave;vers.<br>La teva veu atabalada<br>amb tanta mort s'ha fet  molt clara. </p>
    </td>
</tr>

</table>
</body>
</html>