<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="Jordi Guardans Pàgina personal del poeta català. Biografia, poesia, música, recitals.">
	<meta name="keywords" content="Jordi Guardans, Pagina personal del poeta catalan, guardans, guardan, jordi guarda, jordi guardan, poeta catala, poeta catalá, poeta catalá, poesia catalana, poesía catalana, biografia, poesia, musica catalan, musica catala, recitals, recitals catalan, recitals catala.">
	<title><?php echo $titulo; ?></title>
	<link rel="stylesheet" type="text/css" href="estilos.css">
	<script language="javascript" type="text/javascript" src="funciones.js"></script>
</head>
<body>

<table cellpadding="0" cellspacing="0" align="center" class="tabla1">
<tr>
	<td colspan="3" style="border-bottom:5px solid #ffffff;">
		<a href="index.php" class="n1">jordi guardans</a>
    	<span style="padding:0 0 0 95px;">
		<a href="biografia.php" class="n2">biograf&iacute;a&nbsp;&nbsp;</a>
		<a href="comentaris.php" class="n2">comentaris&nbsp;&nbsp;</a>
        <a href="poesia.php" class="n2">poesia&nbsp;&nbsp;</a>
		<a href="musica.php" class="n2">m&uacute;sica&nbsp;&nbsp;</a>
        <a href="recitals.php" class="n2">recitals&nbsp;&nbsp;</a>
        <a href="recull.php" class="n2">recull&nbsp;&nbsp;</a>
		<a href="mailto:jordi@jordiguardans.cat" class="n2">contacte</a>
        </span>
	</td>
</tr>