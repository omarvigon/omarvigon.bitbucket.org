<?php 
$titulo="Biografia de Jordi Guardans. Web del poeta catal&agrave; Jordi Guardans.";
include("cabecera.php"); ?>

<tr>
	<td class="celda_azul">
		<img title="Jordi Guardans Biografia - Foto Juan Miguel Morales. 2005" src="img/jordi_guardans.jpg" alt="Jordi Guardans Biografia - Foto Juan Miguel Morales. 2005" border="0">
    </td>
    <td colspan="2" class="celda_bio">    
		<h1>JORDI  GUARDANS (Barcelona 1955).</h1>
		<p>Ha  escrit els llibres de poesia <em>Tsares</em> (publicat  a Barcelona per l'Escola Eina en edici&oacute; restringida el 1979. Tradu&iuml;t  posteriorment a l'angl&egrave;s i editat als Estats Units a la revista liter&agrave;ria de  l'&quot;American Catalan Society&quot;); <em>Tant de bo que siguin les pen&uacute;ltimes penes</em> (Premi  Mari&agrave; Manent 1993. Ed. Columna, exhaurit); <em>El R&egrave;quiem blau</em> (Edicions 62, 2002) amb pr&ograve;leg de Pere Gimferrer; <em>Montserrat</em> (Emboscall, 2006. 2a. Edici&oacute;, 2007); <em>Abb&agrave;</em> (Emboscall, 2006); i l'antologia <em>Els &agrave;ngels i el perill</em>. Antologia. Tria i  introducci&oacute; de Carles Guill&eacute;n i Selva (Emboscall, 2006) - a partir d'aquesta  antologia i amb el mateix t&iacute;tol ha fet recitals amb la col&middot;laboraci&oacute; d'Ester  Formosa i el pianista Maurici Villavecchia. 
        El 2007 es va publicar <em>Planeta Balalaika </em>(Emboscall) i el 2008 <em>Contra venena et animalia venenosa</em> (Emboscall) amb  pr&ograve;leg de l'Enric Bou.</p>
		<p>L'any  1981 guanya la medalla d'or del Reial Cercle Art&iacute;stic de Barcelona en ocasi&oacute;  del seu centenari, amb l'obra de teatre <em>La ciutat  il&middot;luminada</em>, publicada el 1989 per l'Institut de Teatre de Barcelona amb  pr&ograve;leg d'Anna Maria Moix.&nbsp;</p>
		<p>Pel  que fa a la m&uacute;sica i a les can&ccedil;ons, que considera part de la producci&oacute; po&egrave;tica,  el 1988 s'edita el disc <em>Port Estrella</em>, en  qu&egrave; cantava les seves composicions. L'Ester Formosa li grava can&ccedil;ons en els  CD's <em>La casa solit&agrave;ria</em> (2000) i <em>&Egrave;poca</em> (2003). La Maria del Mar Bonet als CD's <em>Lluna de pau</em> (2003), enregistrament en directe al  TNC (Recaptaci&oacute; &iacute;ntegra destinada a la Campanya unit&agrave;ria Catalunya per l'Iraq.  Suport humanitari) i <em>Terra secreta</em> (2007).  El Gerard Quintana li &nbsp;canta i posa m&uacute;sica a poemes als CD's <em>Les claus de sal</em> (2004) i <em>Per  un tros de cel</em> (2005), experi&egrave;ncia nova que li sembla del tot reeixida i  enriquidora. 
		El 2007 l'Ester Formosa li dedica l'album  <em>Ester Formosa canta Jordi Guardans. Sola com el poeta.</em> amb quinze  can&ccedil;ons, lletra i m&uacute;sica d'ell mateix. El 2008 T&uacute;rnez &amp; Ses&eacute;, al CD <em>Roman&ccedil;os i estampes del 21</em> li musiquen i  interpreten un poema i una can&ccedil;&oacute;.</p>
		
        <p align="right"><a href="http://www.indianwebs.com" target="_blank" title="Dise&ntilde;o Web Barcelona" style="font:10px Arial, Helvetica, sans-serif;color:#cccccc;text-decoration:none;">Dise&ntilde;o Web Barcelona</a></p>
	</td>
</tr>

</table>
</body>
</html>