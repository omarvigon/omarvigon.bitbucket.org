<?php include ("cabecera.php");?>

	<td class="celda1">
		<p class="ofertas">OFERTAS</p>
		<h3 id="fscroller"></h3>
	</td>
	<td class="celda2">
      	<p><b>PRECIOS</b></p>
        <p>
        <img src="img/logo_lesseps.jpg" alt="Alquiler apartamento Barcelona" title="Alquiler apartamento Barcelona">
        </p>
        <p>
        <em>Temporada baja</em>.......03/01 al 31/03 - 01/10 al 22/12<br />
  		<em>Temporada media</em>...01/04 al 30/06 - 01/09 al 30/09<br />
  		<em>Temporada alta</em>........01/07 al 31/08 - 23/12 al 02/01
        </p>
    </td>
	<td class="celda3">	
        <em style="margin:0 0 0 20px;">Tarifas para 2 personas</em>
        <p><table cellspacing="1" cellpadding="2">
        <tr><td>&nbsp;</td>
            <td><b>TEMP. BAJA</b></td>
            <td><b>TEMP. MEDIA</b></td>
            <td><b>TEMP. ALTA</b></td></tr>
        <tr><td><b>d�a</b></td>
            <td>85 &euro;</td>
            <td>100 &euro;</td>
            <td>120 &euro;</td></tr>
        <tr><td><b>semana&nbsp;&nbsp;</b></td>
            <td>536 &euro;</td>
            <td>630 &euro;</td>
            <td>756 &euro;</td></tr>
        <tr><td><b>mes</b></td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td></tr></table></p>
        <em style="margin:0 0 0 20px;">Tarifas para 3 personas</em>
        <p><table cellspacing="1" cellpadding="2">
        <tr><td><b>d�a</b></td>
            <td>95 &euro;</td>
            <td>110 &euro;</td>
            <td>130 &euro;</td>
        </tr>
        <tr><td><b>semana&nbsp;&nbsp;</b></td>
            <td>599 &euro;</td>
            <td>693 &euro;</td>
            <td>819 &euro;</td> 
        </tr>
        <tr><td><b>mes</b></td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td></tr></table></p>
        <em style="margin:0 0 0 20px;">Tarifas para 4 personas</em>
        <p><table cellspacing="1" cellpadding="2">
        <tr><td><b>d�a</b></td>
            <td>105 &euro;</td>
            <td>120 &euro;</td>
            <td>140 &euro;</td>
        </tr>
        <tr><td><b>semana&nbsp;&nbsp;</b></td>
            <td>662 &euro;</td>
          <td>756 &euro;</td>
            <td>882 &euro;</td>
        </tr>
        <tr><td><b>mes</b></td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td></tr></table></p>
        <em style="margin:0 0 0 20px;">Tarifas para 5 personas</em>
        <p><table cellspacing="1" cellpadding="2">
        <tr><td><b>d�a</b></td>
            <td>115 &euro;</td>
            <td>130 &euro;</td>
            <td>150 &euro;</td> 
        </tr>
        <tr><td><b>semana&nbsp;&nbsp;</b></td>
            <td>725 &euro;</td>
            <td>819 &euro;</td>
          <td>945 &euro;</td>
        </tr>
        <tr><td><b>mes</b></td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td></tr></table></p>
        <em style="margin:0 0 0 20px;">Tarifas para 6 personas</em>
        <p><table cellspacing="1" cellpadding="2">
        <tr><td><b>d�a</b></td>
            <td>125 &euro;</td>
            <td>140 &euro;</td>
            <td>160 &euro;</td> 
        </tr>
        <tr><td><b>semana&nbsp;&nbsp;</b></td>
            <td>788 &euro;</td>
          <td>882 &euro;</td>
            <td>1008 &euro;</td>
        </tr>
        <tr><td><b>mes</b></td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td>
            <td>Consulte/ Ask</td></tr></table></p>
            
<?php include ("piecera.php");?>