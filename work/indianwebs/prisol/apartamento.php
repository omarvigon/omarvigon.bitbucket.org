<?php include ("cabecera.php");?>

	<td class="celda1">
		<p class="pizq"><em>Lesseps</em><br><img src="img/foto_lesseps.jpg" /></p>
        <div id="dhtmltooltip"></div>
        <script>var tipobj=document.getElementById("dhtmltooltip")</script>
        <p class="pics">
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-1.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-1.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-2.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-2.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-3.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-3.jpg>')"; onMouseout="hideddrivetip()">
        <br>
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-4.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-4.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-5.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-5.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-6.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-6.jpg>')"; onMouseout="hideddrivetip()">
        <br>
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-7.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-7.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-8.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-8.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-9.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-9.jpg>')"; onMouseout="hideddrivetip()">
        <br>
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-10.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-10.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        <img src="http://www.desigbarcelona.com/images/fotos/p-les-11.jpg" onMouseover="ddrivetip('<img src=http://www.desigbarcelona.com/images/fotos/p-les-11.jpg>')"; onMouseout="hideddrivetip()">&nbsp;
        </p>
	</td>
	<td class="celda2">
    	<p><b>DESCRIPCI&Oacute;N</b></p>
		<p>Precioso apartamento de 52 metros cuadrados ubicado en la zona alta  de Barcelona, a escasas calles del popular barrio de Gracia. A 20  minutos andando encontrar&aacute;n el impresionante Parque G&uuml;ell, obra del  famoso arquitecto Antoni Gaud&iacute;.</p>
        <p>El barrio es una de las zonas  altas de Barcelona, muy tranquila y segura. A cinco minutos andando  desde el apartamento se encontrar&aacute;n con la animada zona del barrio de  Gracia, donde encontrar&aacute;n gran variedad de bares, restaurantes y  locales nocturnos as&iacute; como un agradable barrio para pasear por sus  estrechas calles y peque&ntilde;as plazas.</p>
		<p>El apartamento est&aacute;  preparado para alojar a 6 personas. Cuenta con dos habitaciones con  cama de matrimonio ubicadas en altillos y dos salas de estar con un  sof&aacute; cama individual en cada uno. La cocina americana est&aacute; totalmente  equipada con todos los enseres para cocinar, vitrocer&aacute;mica,  lavavajillas, microondas, nevera, tostadora y cafetera. En un peque&ntilde;o  patio se sit&uacute;a la lavadora. El apartamento cuenta con dos ba&ntilde;os, uno de  ellos con torre hidromasaje y el otro con ducha. Tiene acceso a  Internet v&iacute;a WI-FI, aire acondicionado y calefacci&oacute;n</p>
    </td>
	<td class="celda3">
		<p><b>APARTAMENTO</b></p>
 		<p><em>Sumario</em><br />- ubicaci&oacute;n: Sant Gervasi <br />- capacidad: 6 Personas <br />- habitaciones: 2 <br />- sof&aacute; cama simple: 2 <br />- ba&ntilde;os: 2</p>
		<p><em>Caracter&iacute;sticas</em><br />- Aire acondicionado<br />- Ba&ntilde;o con ducha<br />- Cafetera<br />- Calefacci&oacute;n<br />- Cuberter&iacute;a, ollas<br />- Enseres de cocina, vajillas<br />- Horno<br />- Lavadora<br />
		- Lavavajillas<br />- Microondas<br />- Nevera/ Congelador<br />- Plancha<br />- Reproductor de CD&acute;s<br />- Secador de pelo<br />- Sof&aacute;<br />- Tel&eacute;fono con llamadas locales<br />- Toallas, s&aacute;banas y mantas<br />- Tostadora<br />- TV/ DVD<br />- Vitrocer&aacute;mica<br />- WI-FI</p>
        
<?php include ("piecera.php");?>

