/**************script de novedades izquierda*********************************/
var fcontent=new Array();
fcontent[0]="<a href='apartamento.php'><em>Lesseps<br>01/11/2007</em><p>Apartamento Exclusivo y de dise&ntilde;o Barcelona, sarria san gervasi</p><p>Exclusivo apartamento de dise&ntilde;o en la zona alta de Barcelona, muy cerca  del popular barrio de Gracia y del famoso Parque G&uuml;ell de Antoni Gaudi.  Metro L3 (Lesseps) y L6/L7 (Plaza Molina).</p></a>";
fcontent[1]="<a href='apartamento.php'><p>Apartamento Exclusivo y de dise&ntilde;o Barcelona, sarria san gervasi</p><p>Exclusivo apartamento de dise&ntilde;o en la zona alta de Barcelona, muy cerca  del popular barrio de Gracia y del famoso Parque G&uuml;ell de Antoni Gaudi.  Metro L3 (Lesseps) y L6/L7 (Plaza Molina).</p></a>";

var delay = 4000; //set delay between message change (in miliseconds)
var maxsteps=30; // number of steps to take to change from start color to endcolor
var stepdelay=40; // time in miliseconds of a single step
var startcolor= new Array(255,255,255); // start color (red, green, blue)
var endcolor=new Array(0,0,0); // end color (red, green, blue)
var fadelinks=1;  //should links inside scroller content also fade like text? 0 for no, 1 for yes.
var ie4=document.all&&!document.getElementById;
var DOM2=document.getElementById;
var faderdelay=0;
var index=0;

if (window.addEventListener)
	window.addEventListener("load", changecontent, false)
else if (window.attachEvent)
	window.attachEvent("onload", changecontent)
else if (document.getElementById)
	window.onload=changecontent

function getstepcolor(step)
{
  var diff
  var newcolor=new Array(3);
  for(var i=0;i<3;i++) 
  {
    diff = (startcolor[i]-endcolor[i]);
    if(diff > 0) 
      newcolor[i] = startcolor[i]-(Math.round((diff/maxsteps))*step);
	else 
      newcolor[i] = startcolor[i]+(Math.round((Math.abs(diff)/maxsteps))*step);
  }
  return ("rgb(" + newcolor[0] + ", " + newcolor[1] + ", " + newcolor[2] + ")");
}
var fadecounter;

function colorfade(step) 
{
  if(step<=maxsteps) 
  {	
    document.getElementById("fscroller").style.color=getstepcolor(step);
    if (fadelinks)
      linkcolorchange(step);
    step++;
    fadecounter=setTimeout("colorfade("+step+")",stepdelay);
  }
  else
  {
    clearTimeout(fadecounter);
    document.getElementById("fscroller").style.color="rgb("+endcolor[0]+", "+endcolor[1]+", "+endcolor[2]+")";
    setTimeout("changecontent()", delay);
  }   
}
function changecontent()
{
  if (index>=fcontent.length)
    index=0
  if (DOM2)
  {
    document.getElementById("fscroller").style.color="rgb("+startcolor[0]+", "+startcolor[1]+", "+startcolor[2]+")"
    document.getElementById("fscroller").innerHTML="<div>"+fcontent[index]+"</div>"
    if (fadelinks)
      linkcolorchange(1);
    colorfade(1, 15);
  }
  else if (ie4)
    document.all.fscroller.innerHTML="<div>"+fcontent[index]+"</div>";
  index++
}
function linkcolorchange(step)
{
  var obj=document.getElementById("fscroller").getElementsByTagName("A");
  if (obj.length>0)
  {
    for (i=0;i<obj.length;i++)
      obj[i].style.color=getstepcolor(step);
  }
}
/*************************calendario************************************************/
var ie=document.all
var ns6=document.getElementById&&!document.all
var n=document.layers
fShow="visible";
fHide="hidden";

var MonthNames = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
var nCurrentYear = 0;
var nCurrentMonth = 0;
var nWidth  = 30;
var nHeight = 20;
var leftX;
var rightX
var topY;
var bottomY;
function Calendar()
{
	var HTMLstr = "<table id='tablacalendario' width='260px' cellspacing='0' cellpadding='0' align='center' style='border-top:2px solid #D8E9D6;'>\n";
	HTMLstr += "<tr><td bgcolor='#999999'>\n";
	HTMLstr += "\n";
	HTMLstr += "<table width='100%'>\n";
	HTMLstr += "<tr>\n";
	HTMLstr += "<td><b>A&ntilde;o:</b></td>\n\n";
	HTMLstr += "<td align='right' width='80'>\n";
	HTMLstr += "<a href=\"javascript:prevYear();\" style='color:#ffffff;font-weight:bold;'> << </a>\n";
	HTMLstr += "</td>\n";
	HTMLstr += "\n";
	HTMLstr += "<td align='center'>";
	HTMLstr += "<div id='main' style='position: relative'><b>1999</b></div>\n";
	HTMLstr += "</td>\n";
	HTMLstr +="<td><a href=\"javascript:nextYear();\" style='color:#ffffff;font-weight:bold;'> >> </a></td>";
	HTMLstr += "</tr>\n";
	HTMLstr += "<tr>\n";
	HTMLstr += "<td><b>Mes:</b></td>\n\n";
	HTMLstr += "<td align='right'>\n";
	HTMLstr += "<a href=\"javascript:prevMonth();\" style='color:#ffffff;font-weight:bold;'> << </a>\n";
	HTMLstr += "</td>\n\n";
	HTMLstr += "<td align='center' width='100'>\n";
	HTMLstr += "<div id='main2' style='position=relative;'><b>December</b><div>\n";
	HTMLstr += "</td>\n\n";
	HTMLstr +="<td><a href=\"javascript:nextMonth();\" style='color:#ffffff;font-weight:bold;'> >> </a></td>";
	HTMLstr += "</tr>\n";
	HTMLstr += "</table>\n";
	HTMLstr += "\n";
	HTMLstr += "</td></tr>\n";
	HTMLstr += "\n";
	HTMLstr += "<tr height='120px'><td valign=\"top\">\n";
	HTMLstr += "\n";
	HTMLstr += "<table cellpadding='0' cellspacing='0' bgcolor='#cccccc'>\n";
	HTMLstr += "<tr>\n";
	HTMLstr += "<td width='40'><b>&nbsp;LUN</b></td>\n";
	HTMLstr += "<td width='36'><b>MAR</b></td>\n";
	HTMLstr += "<td width='30'><b>MIE</b></td>\n";
	HTMLstr += "<td width='38'><b>JUE</b></td>\n";
	HTMLstr += "<td width='38'><b>VIE</b></td>\n";
	HTMLstr += "<td width='38'><b>SAB</b></td>\n";
	HTMLstr += "<td width='46'><b>DOM</b></td>\n";
	HTMLstr += "</tr>\n";
	HTMLstr += "<tr>\n";
	HTMLstr += "<td colspan=7>\n";
	HTMLstr += "<div style='position: relative;'>";
	
	for (var date=1;date<=31;date++)
	{
		HTMLstr += "  <div id=\"idDate"+date+"\" val="+date+" style=\"position: absolute; visibility: hidden\">\n";
		HTMLstr += "    <b>"+date+"</b>\n";
		HTMLstr += "  </div>\n";
	}
	HTMLstr += "</div>";
	HTMLstr += "</td></tr>\n";
	HTMLstr += "</table>\n";
	HTMLstr += "\n";
	HTMLstr += "</td></tr>\n";
	HTMLstr += "</table>\n";
	
	document.writeln(HTMLstr);
}
function setCurrentMonth()
{
  date = new Date();
  currentyear=date.getYear()
  if (currentyear < 1000)
	currentyear+=1900
  setYearMonth(currentyear, date.getMonth()+1);
}
function setMonth(nMonth)
{
	setYearMonth(nCurrentYear, nMonth);
}
function setYearMonth(nYear, nMonth)
{
  nCurrentYear = nYear;
  nCurrentMonth = nMonth;
  var cross_obj=ns6? document.getElementById("main"): document.all["main"]
  var cross_obj2=ns6? document.getElementById("main2"): document.all["main2"]
  
  cross_obj.innerHTML  = "<font color=\"#E6443B\"><b>"+nCurrentYear+"</b></font>";
  cross_obj2.innerHTML = "<font color=\"#E6443B\"><b>"+MonthNames[nCurrentMonth-1]+"</b></font>\n";

  var date   = new Date(nCurrentYear, nCurrentMonth-1, 1);
  var nWeek  = 1;
  var nDate;

  while (date.getMonth() == nCurrentMonth-1)
  {
	nDate = date.getDate();
	nLastDate = nDate;

	var posDay = date.getDay()-1;
	if (posDay == -1) posDay=6;
	var posLeft = posDay*(nWidth+5)+5;
	var posTop  = (nWeek-1)*nHeight;
		var cross_obj3=ns6? document.getElementById("idDate"+nDate).style : document.all["idDate"+nDate].style
	cross_obj3.left = posLeft;
	cross_obj3.top  = posTop;
	if (date.getDay() == 0 || date.getDay() == 6)
		cross_obj3.color  = "red";
	else
		cross_obj3.color  = "black";
	cross_obj3.visibility = "visible";
	date = new Date(nCurrentYear, date.getMonth(), date.getDate()+1);
	
	if (posDay == 6) 
		nWeek++;
  }
  for (++nDate; nDate <= 31; nDate++)
  {
	cross_obj3=ns6? document.getElementById("idDate"+nDate).style : document.all["idDate"+nDate].style
	cross_obj3.visibility = "hidden";
  }
}
function nextMonth()
{
  nCurrentMonth++;
  if (nCurrentMonth > 12)
  {
	nCurrentMonth -= 12;
	nextYear();
  }
  setYearMonth(nCurrentYear, nCurrentMonth);
}
function prevMonth()
{
  nCurrentMonth--;
  if (nCurrentMonth < 1)
  {
	nCurrentMonth += 12;
	prevYear();
  }
  setYearMonth(nCurrentYear, nCurrentMonth);
}
function prevYear()
{
  nCurrentYear--;
  setYearMonth(nCurrentYear, nCurrentMonth);
}
function nextYear()
{
  nCurrentYear++;
  setYearMonth(nCurrentYear, nCurrentMonth);
}
/************************agrandar img tooltip**********************************************/

var offsetxpoint=5 //Customize x offset of tooltip
var offsetypoint=-150 //Customize y offset of tooltip
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false

function ietruebody()
{
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
function ddrivetip(thetext)
{
	tipobj.innerHTML=thetext
	enabletip=true
	return false
}
function positiontip(e)
{
	if (enabletip)
	{
		var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
		var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
		//Find out how close the mouse is to the corner of the window
		var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20
		var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20
		var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000

		//if the horizontal distance isn't enough to accomodate the width of the context menu
		if (rightedge<tipobj.offsetWidth)
			//move the horizontal position of the menu to the left by it's width
			tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px"
		else if (curX<leftedge)
			tipobj.style.left="5px"
		else
			//position the horizontal position of the menu where the mouse is positioned
			tipobj.style.left=curX+offsetxpoint+"px"
			//same concept with the vertical position
		if (bottomedge<tipobj.offsetHeight)
			tipobj.style.top=ie? ietruebody().scrollTop+event.clientY-tipobj.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-tipobj.offsetHeight-offsetypoint+"px"
		else
			tipobj.style.top=curY+offsetypoint+"px"
		tipobj.style.visibility="visible"
	}
}
function hideddrivetip()
{
	enabletip=false
	tipobj.style.visibility="hidden"
}
document.onmousemove=positiontip