<?php 
define("BBDD_HOST","mysql.persianasamedida.com");
define("BBDD_USER","ADM_bd698412");
define("BBDD_PASSWORD","Barcelona92");
define("BBDD_NAME","bd698412");

if(mysql_connect(BBDD_HOST,BBDD_USER,BBDD_PASSWORD))
{
	mysql_select_db(BBDD_NAME);
	$resultado=mysql_query("select distinct tipo_producto,precio_m2 from persianas_enrollables") or die(mysql_error());
	mysql_close();
	
	$desplegable="<optgroup label='PERSIANAS ENROLLABLES'>";
	while($fila=mysql_fetch_assoc($resultado))
	{
		$desplegable.="<option value='".$fila["precio_m2"]."'>".$fila["tipo_producto"]."</option>";
	}
	$desplegable.="</optgroup>";
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Comprar persianas</title>
	<meta name="description" content="Comprar persianas" />
	<meta name="keywords" content="Comprar persianas" />
	<script language="javascript" type="text/javascript" src="_funciones.js"></script>
	<style type="text/css">	
	body		{margin:0;}
	th			{font:11px Arial, Helvetica, sans-serif;background:#cccccc;text-align:left;}
	td 			{font:11px Arial, Helvetica, sans-serif;}
	input,textarea	{font:11px Arial, Helvetica, sans-serif;border:1px solid #333333;}
	select		{font-size:11px;}
	.lista1		{width:190px;}
	table		{width:100%;}
	p			{margin:4px;}
	.lectura	{background:#eeeeee;}
	</style>
    <script type="text/javascript">
	function calcular(fila)
	{
		var articulo=fila.parentNode.getElementsByTagName("select")[0].value
		var unidades=fila.parentNode.getElementsByTagName("input")[1].value;
		var ancho=fila.parentNode.getElementsByTagName("input")[2].value;
		var alto=fila.parentNode.getElementsByTagName("input")[3].value;
		
		if(!isNaN(ancho) && !isNaN(alto) && !isNaN(unidades) && (articulo>0))
		{	
			var metros=((ancho*alto)/1000000);
			metros=Math.round(metros * 100) / 100;
			
			var indice=fila.parentNode.getElementsByTagName("select")[0].selectedIndex;
			
			fila.parentNode.getElementsByTagName("input")[0].value=fila.parentNode.getElementsByTagName("select")[0].options[indice].text;
			fila.parentNode.getElementsByTagName("input")[6].value=metros;
			fila.parentNode.getElementsByTagName("input")[7].value=articulo;
			fila.parentNode.getElementsByTagName("input")[8].value=metros=Math.round((articulo)*(metros)*(unidades) * 100) / 100;
			
			calcular_totales();
		}
		else
			window.alert("Debe seleccionar el articulo, unidades, ancho y alto correctamente");
	}
	function calcular_totales()
	{
		var suma=0;
		for(var i=1;i<=((document.getElementById("tabla1").rows.length)-1);i++)
		{
			suma=suma+parseInt(document.getElementById("tabla1").rows[i].getElementsByTagName("input")[8].value);
		}	
		var gastos=(suma<100)? 15:0;
		var iva=Math.round((suma*0.16) * 100) / 100;
			
		document.form1.total_suma.value=suma;	
		document.form1.total_gastos_envio.value=gastos;
		document.form1.total_iva.value=iva;
		document.form1.total_importe.value=Math.round((suma+iva+gastos) * 100) / 100;
	}
	function borrar(fila)
	{
		if(document.getElementById("tabla1").rows.length>2)
		{
			var num_fila=fila.parentNode.rowIndex;
			document.getElementById("tabla1").deleteRow(num_fila);
			document.form1.total_unidades.value=(document.getElementById("tabla1").rows.length)-1;
			calcular_totales();
		}
	}
	function agregar()
	{
		var fila=document.getElementById("tabla1").rows.length;
		
   		var row = document.createElement("TR");
    	var td1 = document.createElement("TD");var td2 = document.createElement("TD");
		var td3 = document.createElement("TD");var td4 = document.createElement("TD");
		var td5 = document.createElement("TD");var td6 = document.createElement("TD");
		var td7 = document.createElement("TD");var td8 = document.createElement("TD");
		
   		td1.innerHTML="<select name='articulo"+fila+"' class='lista1'><option value='0'>Seleccionar...</option><?=$desplegable;?></select><input type='hidden' name='articulo"+fila+"nombre' value='' />";
   		td2.innerHTML="<input name='art"+fila+"unidades' onchange='calcular(this.parentNode)' type='text' size='4' maxlength='3' value='1' />";
		td3.innerHTML="<input name='art"+fila+"ancho' onchange='calcular(this.parentNode)' type='text' size='4' maxlength='5' />";
		td4.innerHTML="<input name='art"+fila+"alto' onchange='calcular(this.parentNode)' type='text' size='4' maxlength='5'/>";
		td5.innerHTML="<input type='button' onclick='calcular(this.parentNode)' value='CALCULAR' /> <input type='button' onclick='borrar(this.parentNode)' value='BORRAR' />";
		td6.innerHTML="<input name='art"+fila+"m2' type='text' size='5' readonly='readonly' class='lectura'/>";
		td7.innerHTML="<input name='art"+fila+"precio' type='text' size='6' readonly='readonly' class='lectura'/>&euro;";
		td8.innerHTML="<input name='art"+fila+"importe' type='text' size='6' readonly='readonly' class='lectura'/>&euro;";

    	row.appendChild(td1);row.appendChild(td2);
		row.appendChild(td3);row.appendChild(td4);
		row.appendChild(td5);row.appendChild(td6);
		row.appendChild(td7);row.appendChild(td8);
    	document.getElementsByTagName("TBODY")[0].appendChild(row);
		document.form1.total_unidades.value=(document.getElementById("tabla1").rows.length)-1;
	}
	/* IE
	var myRow = document.getElementById("tabla1").insertRow();
   	myRow.insertCell().innerText = "xyz";
	myRow.insertCell().innerText = "xyz22";
	*/
	function finalizar()
	{
		if(!document.form1.condiciones.checked)
			alert("Debe aceptar las condiciones legales");
		else	
			document.form1.submit();
	}
	</script>
</head>

<body>

<form action="config-pasarela.php" method="post" name="form1">
<table cellpadding="2" cellspacing="1" id="tabla1">
<tbody>
<tr>
	<th>Articulo</th>
    <th>Unidades</th>
    <th>Ancho (mm.)</th>
    <th>Alto (mm.)</th>
    <th>Accion</th>
    <th>M&sup2;</th>
    <th>Precio M&sup2;</th>
    <th>Importe</th>
</tr>
<tr>
	<td><select name="articulo1" class="lista1"><option value="0">Seleccionar...</option><?=$desplegable;?></select>
    	<input type="hidden" name="articulo1nombre" /></td>
    <td><input name="art1unidades" onchange="calcular(this.parentNode)" type="text" size="4" maxlength="3" value="1" /></td>
    <td><input name="art1ancho" onchange="calcular(this.parentNode)" type="text" value="0" size="4" maxlength="5" /></td>
    <td><input name="art1alto" onchange="calcular(this.parentNode)" type="text" value="0" size="4" maxlength="5"/></td>
    <td><input type="button" onclick="calcular(this.parentNode)" value="CALCULAR" /> 
    	<input type="button" onclick="borrar(this.parentNode)" value="BORRAR" /></td>
    <td><input name="art1m2" type="text" size="5" readonly="readonly" class="lectura" /></td>
    <td><input name="art1precio" type="text" size="6" readonly="readonly" class="lectura"/>&euro;</td>
    <td><input name="art1importe" type="text" size="6" readonly="readonly" class="lectura"/>&euro;</td>
</tr>
</tbody>
</table>

<table cellpadding="2" cellspacing="1">
<tr>
	<td width="240">&nbsp;</td>
    <td width="70">min.250mm<br />max.4000mm</td>
    <td width="70">min.300mm<br />max.3600mm</td>
    <td align="right"><input type="button" onclick="agregar()" value="A&Ntilde;ADIR OTRO" />&nbsp;</td>
</tr>
</tbody>
</table>

<table cellpadding="2" cellspacing="1" style="margin:16px 0 0 0;">
<tr>
	<th>TOTAL Articulos</th>
    <th>TOTAL Suma</th>
    <th>I.V.A.(16%)</th>
    <th>Gastos de Envio</th>
    <th><b>TOTAL</b></th>
</tr>
<tr>
    <td><input name="total_unidades" type="text" value="1" size="16" readonly="readonly" class="lectura"/></td>
    <td><input name="total_suma" type="text" size="16" readonly="readonly" class="lectura"/>&euro;</td>
    <td><input name="total_iva" type="text" size="16" readonly="readonly" class="lectura"/>&euro;</td>
    <td><input name="total_gastos_envio" type="text" size="16" readonly="readonly" class="lectura"/>&euro;</td>
    <td><input name="total_importe" type="text" size="16" readonly="readonly" class="lectura"/>&euro;</td>
</tr>
</table>

<table cellpadding="2" cellspacing="1" style="margin:32px 0 0 0;">
<tr>
	<th colspan="4">Datos del cliente</th>
</tr>
<tr>
    <td>Nombre:</td>
    <td><input name="nombre" type="text" size="42"  /></td>
    <td>D.N.I./Nif</td>
    <td><input name="dni" type="text" size="42"  /></td>
</tr>
<tr>
    <td>Direccion:</td>
    <td><input name="direccion" type="text" size="42" /></td>
    <td>Poblacion:</td>
    <td><input name="poblacion" type="text" size="42" /></td>
</tr>
<tr>
    <td>C.P.:</td>
    <td><input name="cp" type="text" size="42"/></td>
    <td>Provincia:</td>
    <td><input name="provincia" type="text" size="42"/></td>
</tr>
<tr>
    <td>Telefono:</td>
    <td><input name="telefono" type="text" size="42"/></td>
    <td>E-Mail:</td>
    <td><input name="email" type="text" size="42"/></td>
</tr>
<tr>
    <td>Observaciones:</td>
    <td><textarea name="observaciones" rows="6" cols="42"></textarea></td>
    <td>Paga y se&ntilde;al:</td>
    <td><select name="porcentaje"><option value="25">25%</option><option value="50" selected="selected">50%</option><option value="75">75%</option><option value="100">100%</option></select> del importe total</td>
</tr>
<tr>
	<td>&nbsp;</td>
    <td><input type="checkbox" name="condiciones" style="border:0;" />Acepto las <a href="http://www.persianasamedida.com/condiciones_generales.html" target="_blank" style="color:#003399;">condiciones legales</a></td>
    <td><input type="button" onclick="finalizar()" value="  COMPRAR  " /></td>
    <td>&nbsp;</td>
</tr>
</table>
</form>


</body>
</html>