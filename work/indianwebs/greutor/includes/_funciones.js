function comprobar()
{
	var errores=false;
	if(document.form_ficha.email.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
		errores=true;
		
	if(document.form_ficha.nombre.value=="")		errores=true;		
	if(document.form_ficha.direccion.value=="")		errores=true;
	if(document.form_ficha.poblacion.value=="")		errores=true;
	if(document.form_ficha.provincia.value=="")		errores=true;
	if(document.form_ficha.telefonos.value=="")		errores=true;
	
	if(!errores)
		document.form_ficha.submit();
	else
		window.alert("Debe rellenar los campos obligatorios (*)");
}
/***********************************************************************************/
var fadeimages=new Array()
fadeimages[0]=["img/greutor-productos1.jpg"]
fadeimages[1]=["img/greutor-productos2.jpg"]
fadeimages[2]=["img/greutor-productos3.jpg"]
fadeimages[3]=["img/greutor-productos4.jpg"]
fadeimages[4]=["img/greutor-productos5.jpg"]
fadeimages[5]=["img/greutor-productos6.jpg"]
fadeimages[6]=["img/greutor-productos7.jpg"]

var fadebgcolor="white"
var fadearray=new Array()
var fadeclear=new Array() 
var dom=(document.getElementById)
var iebrowser=document.all
 
function fadeshow(theimages, fadewidth, fadeheight, borderwidth, delay, pause, displayorder)
{
	this.pausecheck=pause
	this.mouseovercheck=0
	this.delay=delay
	this.degree=10
	this.curimageindex=0
	this.nextimageindex=1
	fadearray[fadearray.length]=this
	this.slideshowid=fadearray.length-1
	this.canvasbase="canvas"+this.slideshowid
	this.curcanvas=this.canvasbase+"_0"
	if (typeof displayorder!="undefined")
		theimages.sort(function() {return 0.5 - Math.random();})
	this.theimages=theimages
	this.postimages=new Array()
	for (p=0;p<theimages.length;p++)
	{
		this.postimages[p]=new Image()
		this.postimages[p].src=theimages[p][0]
	}
	 
	var fadewidth=fadewidth
	var fadeheight=fadeheight
	document.write('<div id="master'+this.slideshowid+'" style="position:relative;margin:0 0 0 620px;width:'+fadewidth+'px;height:'+fadeheight+'px;overflow:hidden;"><div id="'+this.canvasbase+'_0" style="position:absolute;width:'+fadewidth+'px;height:'+fadeheight+'px;top:0;left:0;filter:progid:DXImageTransform.Microsoft.alpha(opacity=10);-moz-opacity:10;-khtml-opacity:10;background-color:'+fadebgcolor+'"></div><div id="'+this.canvasbase+'_1" style="position:absolute;width:'+fadewidth+'px;height:'+fadeheight+'px;top:0;left:0;filter:progid:DXImageTransform.Microsoft.alpha(opacity=10);-moz-opacity:10;background-color:'+fadebgcolor+'"></div></div>')
	this.startit()
}
function fadepic(obj)
{
	if (obj.degree<100)
	{
		obj.degree+=10
		if (obj.tempobj.filters&&obj.tempobj.filters[0])
			obj.tempobj.filters[0].opacity=obj.degree
		else if (obj.tempobj.style.MozOpacity)
			obj.tempobj.style.MozOpacity=obj.degree/101
		else if (obj.tempobj.style.KhtmlOpacity)
			obj.tempobj.style.KhtmlOpacity=obj.degree/100
	}
	else
	{
		clearInterval(fadeclear[obj.slideshowid])
		obj.nextcanvas=(obj.curcanvas==obj.canvasbase+"_0")? obj.canvasbase+"_0" : obj.canvasbase+"_1"
		obj.tempobj=iebrowser? iebrowser[obj.nextcanvas] : document.getElementById(obj.nextcanvas)
		obj.populateslide(obj.tempobj, obj.nextimageindex)
		obj.nextimageindex=(obj.nextimageindex<obj.postimages.length-1)? obj.nextimageindex+1 : 0
		setTimeout("fadearray["+obj.slideshowid+"].rotateimage()", obj.delay)
	}
}
fadeshow.prototype.populateslide=function(picobj, picindex)
{
	picobj.innerHTML='<img src="'+this.postimages[picindex].src+'" class="cabecera">'
}
fadeshow.prototype.rotateimage=function()
{
	if (this.pausecheck==1) //if pause onMouseover enabled, cache object
		var cacheobj=this
	if (this.mouseovercheck==1)
		setTimeout(function(){cacheobj.rotateimage()}, 100)
	else if (iebrowser&&dom||dom)
	{
		this.resetit()
		var crossobj=this.tempobj=iebrowser? iebrowser[this.curcanvas] : document.getElementById(this.curcanvas)
		crossobj.style.zIndex++
		fadeclear[this.slideshowid]=setInterval("fadepic(fadearray["+this.slideshowid+"])",90)
		this.curcanvas=(this.curcanvas==this.canvasbase+"_0")? this.canvasbase+"_1" : this.canvasbase+"_0"
	}
	else
	{
		var ns4imgobj=document.images['defaultslide'+this.slideshowid]
		ns4imgobj.src=this.postimages[this.curimageindex].src
	}
	this.curimageindex=(this.curimageindex<this.postimages.length-1)? this.curimageindex+1 : 0
}
fadeshow.prototype.resetit=function()
{
	this.degree=10
	var crossobj=iebrowser? iebrowser[this.curcanvas] : document.getElementById(this.curcanvas)
	if (crossobj.filters&&crossobj.filters[0])
		crossobj.filters(0).opacity=this.degree
	else if (crossobj.style.MozOpacity)
		crossobj.style.MozOpacity=this.degree/101
	else if (crossobj.style.KhtmlOpacity)
		crossobj.style.KhtmlOpacity=obj.degree/100
}
fadeshow.prototype.startit=function()
{
	var crossobj=iebrowser? iebrowser[this.curcanvas] : document.getElementById(this.curcanvas)
	this.populateslide(crossobj, this.curimageindex)
	if (this.pausecheck==1)
	{ 	
		var cacheobj=this
		var crossobjcontainer=iebrowser? iebrowser["master"+this.slideshowid] : document.getElementById("master"+this.slideshowid)
		crossobjcontainer.onmouseover=function(){cacheobj.mouseovercheck=1}
		crossobjcontainer.onmouseout=function(){cacheobj.mouseovercheck=0}
	}
	this.rotateimage()
}