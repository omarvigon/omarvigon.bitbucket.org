<?php include("includes/1cabecera.php");?>
    <title>Derecho Concursal RC ASOCIADOS</title>
    <meta name="title" content="Derecho Concursal RC ASOCIADOS" />
    <meta name="description" content="Derecho Concursal RC ASOCIADOS" />
    <meta name="keywords" content="derecho concursal,RC ASOCIADOS,abogado online" />
<?php include("includes/2cabecera.php");?>
    
    <h2>Concursal</h2>
	<ul>
    <li>Estudio de la problem&aacute;tica de crisis de la empresa. Formas alternativas a la  del concurso, si es posible.</li>
    <li>Asesoramiento previo y preparaci&oacute;n para afrontar un pr&oacute;ximo procedimiento  concursal.</li>
    <li>Instar un procedimiento concursal, caso de que sea necesario.</li>
    <li>Defensa jur&iacute;dica en el mismo.</li>
    <li>Preparaci&oacute;n de plan de viabilidad.</li>
    <li>Negociaci&oacute;n con los acreedores.</li>
    <li>Preparaci&oacute;n del plan de liquidaci&oacute;n.</li>
    </ul>
    <p>Asimismo,  prestamos el asesoramiento necesario y el seguimiento oportuno&nbsp; en caso de que Ud. o su empresa sea acreedor  en un proceso concursal. A este respecto, nuestra labor consiste en:</p>
    <ul>
    <li>La comparecencia en el procedimiento.</li>
    <li>Seguimiento de la labor de la administraci&oacute;n y del concurso.</li>
    <li>Impugnaci&oacute;n de los acuerdos lesivos.</li>
    <li>An&aacute;lisis del estado de la empresa concursada para decidir si&nbsp; aceptar o no el convenio.</li>
    <li>Seguimiento de la pieza de calificaci&oacute;n y b&uacute;squeda de responsabilidades.</li>
    <li>Seguimiento del plan de liquidaci&oacute;n cuando lo haya.</li>
    </ul>
    
<?php include("includes/3piecera.php");?>
