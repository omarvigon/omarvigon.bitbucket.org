<?php include("includes/1cabecera.php");?>
    <title>Derecho Mercantil RC ASOCIADOS</title>
    <meta name="title" content="Derecho Mercantil RC ASOCIADOS" />
    <meta name="description" content="Derecho Mercantil RC ASOCIADOS" />
    <meta name="keywords" content="derecho mercantil,RC ASOCIADOS,abogado online" />
<?php include("includes/2cabecera.php");?>
    
    <h2>Mercantil</h2>
	<ul>
    <li>Revisi&oacute;n de contratos con proveedores.</li>
    <li>Estudio de condiciones generales de  contrataci&oacute;n.</li>
    <li>Preparaci&oacute;n de Juntas Generales.</li>
    <li>Convocatorias de Juntas Generales.</li>
    <li>En colaboraci&oacute;n con el fiscalista:  estudio de las cuentas anuales.</li>
    <li>Confecci&oacute;n de actas.</li>
    <li>Constituci&oacute;n de todo tipo de sociedades.</li>
    <li>Cambios de domicilio, ampliaciones y  reducciones de capital.</li>
    <li>Planificaci&oacute;n de las actuaciones a tomar  para evitar las acciones de responsabilidad civil de los miembros del &oacute;rgano  de&nbsp;&nbsp;&nbsp;&nbsp; administraci&oacute;n.</li>
    <li>Revisi&oacute;n contrataci&oacute;n p&oacute;lizas de cr&eacute;dito  y l&iacute;neas de descuento.</li>
    <li>Interposici&oacute;n de demandas de tipo  mercantil, impugnaci&oacute;n de acuerdos societarios.</li>
    </ul>
    <p><img title="" src="img/bufete-2.jpg" alt=""/></p>
    
<?php include("includes/3piecera.php");?>
