    <meta name="language" content="es" />
    <link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
    <script language="javascript" type="text/javascript" src="includes/_funciones.js"></script>
</head>

<body>
<div id="arriba">
	<h1><a title="Rc Asociados Abogados" href="index.php"><img src="img/logo-rc.gif" alt=""> RC Asociados</a></h1>
    <h3>
    	<a title="Inicio" href="index.php">Inicio</a> | 
        <a title="Socios" href="socios.php">Socios</a> | 
    	<a title="Contacta" href="contacta.php">Contacta</a>
    </h3>
</div>
<div id="centro">
	<div id="boton">
    	<img title="" src="img/abogado.jpg" alt="" />
    	<h4>Especialidades</h4>
    	<a title="Derecho Civil" href="derecho-civil.php">&raquo; Civil</a>
        <a title="Derecho Mercantil" href="derecho-mercantil.php">&raquo; Mercantil</a>
        <a title="Derecho Concursal" href="derecho-concursal.php">&raquo; Concursal</a>
        <a title="Derecho Extranjeria" href="derecho-extranjeria.php">&raquo; Extranjer&iacute;a</a>
    	<a title="Derecho Fiscal" href="derecho-fiscal.php">&raquo; Fiscal</a>
    	<a title="Derecho Penal" href="derecho-penal.php">&raquo; Penal</a>
    	<a title="Derecho Inmobiliario" href="derecho-inmobiliario.php">&raquo; Inmobiliario</a>
    	<a title="Derecho Administrativo" href="derecho-administrativo.php">&raquo; Administrarivo</a>
    	<a title="Derecho Laboral" href="derecho-laboral.php">&raquo; Laboral</a>
    	<a title="Derecho Procesal" href="derecho-procesal.php">&raquo; Procesal</a>
    </div>