<?php include("includes/1cabecera.php");?>
    <title>RC ASOCIADOS es un gabinete con 25 a&ntilde;os de experiencia  en el mercado</title>
    <meta name="title" content="RC ASOCIADOS" />
    <meta name="description" content="RC ASOCIADOS Abogados Barcelona" />
    <meta name="keywords" content="RC ASOCIADOS Barcelona" />
<?php include("includes/2cabecera.php");?>
    
    <h2>RC ASOCIADOS</h2>
	<p><b>RC ASOCIADOS</b> es un gabinete con 25 a&ntilde;os de experiencia  en el mercado.</p>
	<p>Nuestra solvencia, seriedad, honestidad y  profesionalidad ha hecho que nuestro equipo sea considerado uno de los m&aacute;s  competentes del mercado.</p>
	<p>Nuestros miembros est&aacute;n altamente  cualificados y especializados en diferentes &aacute;reas, alcanzando un gran nivel de  eficacia en su forma de trabajar.</p>
	<p>Nuestros clientes: Grandes, medianas y peque&ntilde;as empresas de todo tipo y particulares.</p>
	<p>Entre nuestros especialistas se encuentran  abogados y economistas j&oacute;venes y din&aacute;micos que ofrecen la totalidad de  servicios que nuestros clientes puedan necesitar.</p>
	<p>Nuestras premisas:</p>
	<p>a) <b>Ser pr&aacute;cticos</b>.<br />No sirve de nada  que demos a nuestros clientes clases magistrales si no arreglamos su problema.</p>
	<p>b) <b>Ser &aacute;giles</b>.<br />Nosotros no estamos  siempre reunidos, como la mayor&iacute;a de asesores, y, en caso de estarlo,  intentaremos devolverle la llamada el mismo d&iacute;a.</p>
	<p>c) <b>Ser competentes</b>.<br />No esperamos a que  nos llamen para preguntar como esta lo nuestro. Procuramos llamar nosotros e  informar.</p>
	<p>d) <b>Ser honestos</b>.<br />La honestidad es  defender los intereses de nuestros clientes. No los nuestros.</p>
    
<?php include("includes/3piecera.php");?>
