<?php include("includes/1cabecera.php");?>
    <title>Socios de RC ASOCIADOS es un gabinete de abogados</title>
    <meta name="title" content="Socios de RC ASOCIADOS" />
    <meta name="description" content="Socios de RC ASOCIADOS Abogados Barcelona" />
    <meta name="keywords" content="Socios de RC ASOCIADOS Barcelona" />
<?php include("includes/2cabecera.php");?>
    
    <h2>Socios</h2>
    <dl>
    	<dt>DAVID RUA CONTE</dt>
        <dd>email: <a href="mailto:darua@icab.es">darua@icab.es</a></dd>
        
        <dt>JOSE ANTONIO VALLEJO GARCIA</dt>
        <dd>email: <a href="mailto:javg@icab.es">javg@icab.es</a></dd>
        
        <dt>JAVIER MIRANDA GONZALEZ</dt>
        <dd>email: <a href="mailto:javiermiranda@icab.es">javiermiranda@icab.es</a></dd>
    </dl>

    
<?php include("includes/3piecera.php");?>
