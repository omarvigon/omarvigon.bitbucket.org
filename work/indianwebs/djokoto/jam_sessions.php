<?php include("cabecera.php"); ?>

    <h1 class="b3">JAM SESSIONS</h1>
    <p>�Jam Sessions� � son encuentros de m�sicos,  percusionistas y bailarines de diferentes estilos de m�sica reunidos para  tocar, cantar y bailar y por supuesto disfrutar durante unas horas.La interacci�n con el publico es lo primero, porque  queremos �hacer el publico participar en  este encuentro y a unirlo a la fiesta. </p>
    <p>Todos que desean puedan tener el contacto con la m�sica y  instrumentos musicales en un ambiente informal. Ven a  tocar o ven a escuchar o las dos cosas. En los Jam Sessions puedes encontrar otros percusionistas� profesionales y otros m�sicos.</p>
    <p>No es necesario tener un tambor, porque encontraras en el escenario  Djembes, Dun Duns, Congas, Bongos� etc. Pero si tienes tu propio instrumento �traelo!!!!!!!!!!!!! Porque queremos  disfrutar con tu m�sica juntos.</p>
    <p>
    <img src="img/jam1.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam2.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam3.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam4.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam5.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam6.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam7.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam8.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam9.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam10.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    </p>
    <p><u>Los  Jam sessions a Destacar:</u></p>
    <ul>
      <li>Jam Session en Boca Norte 2003�organizado por Ayao�con participaci�n de alumnos y profesores del mismo centro.</li>
      <li>Enero 2004 -Jam Session con LLU�S MOLAS &amp; AYAO LOGOSS� (cuentos y percusi�n) en Can Butjosa en Parets del Valles (Barcelona), donde Ayao Logosse explicaba� los cuentos� de su tierra,� y Lluis Molas dirig�a equipo de percusi�n quienes dejaban sorprendidos al publico con sus �improvisaciones.</li>
      <li>Jam Session en Cenro Civico El Surtidor dirigido por Ayao con colaboraci�n de artistas de  DJOKOTO en Diciembre 2005, Mayo 2006, Diciembre 2006, Mayo 2007. </li>
    </ul>
    <p><b>PROXIMO Jam Session</b><br>21 Diciembre a las 22.15h en Centro C�vico el Surtidor<br>Direccion: Pla�a del Surtidor, 12 � Barcelona</p>
    <p>
    <img src="img/jam11.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam12.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam13.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam14.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam15.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam16.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam17.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam18.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam19.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    <img src="img/jam20.jpg" width="55" alt="Djokoto Jam Sessions" title="Djokoto Jam Sessions" onMouseover="ddrivetip(this.src)" onMouseout="hideddrivetip()">
    </p>     
    <div id="dhtmltooltip"></div>
    <script>var tipobj=document.getElementById("dhtmltooltip")</script>    
             
<?php include("piecera.php"); ?>