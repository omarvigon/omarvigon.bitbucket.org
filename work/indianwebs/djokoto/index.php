<?php include("cabecera.php"); ?>
	
    <h1 class="b1">GRUPO DJOKOTO - PERCUSION Y DANZA AFRICANA</h1>
    <img src="img/djokoto_inicio.jpg" align="right" style="margin:0 0 0 25px;">
    <p style="color:#a30306;font-weight:bold;">Queremos que  el arte sea un factor de gu�a, un factor de privilegios educando los  trabajadores en sus trabajos respectivos y creativos.</p>
    <p style="color:#a30306;font-weight:bold;">Queremos que  el arte sea un arma contra la xenofobia, la intolerancia, el racismo, la guerra  que favorezca la comprensi�n del mestizaje, el amor y la paz.</p>
    <p style="color:#a30306;font-weight:bold;">Queremos que este embri�n de la paz se forme en el esp�ritu  de todo el mundo y que despierte en nosotros poco a poco todo aquello que  estamos olvidando.</p>
    <p style="color:#a30306;font-weight:bold;">Queremos que todos los encuentros e intercambios culturales y  art�sticos de todos pueblos del mundo sean un mensaje a la humanidad.</p>
    <p>El Maestro  de Percusion Africana, Ayao Logosse, naci� en una familia de Griots y� Percusionistas famosos de� Lom�, Republica de Togo (�frica  Occidental)<br>
      Su carera  profesional de percusionista empez� en a�o 1978 en la Escuela Oficial de M�sica  Tradicional.<br>
      Despu�s de  ganar en 1981 el concurso nacional del &quot;Percusionista Solista&quot; ha  empezado su carrera en el Equipo Nacional que dur� hasta 1996.<br>
      Domina  perfectamente los siguientes instrumentos de Percusi�n, como: </p>
    <ul>
      <li><b>Djembe, </b></li>
      <li><b>Tama, </b></li>
      <li><b>Dundum, </b></li>
      <li><b>Toba, </b></li>
      <li><b>Atopani, </b></li>
      <li><b>Sogo,</b></li>
      <li><b>Congas y Bongo.</b> </li>
    </ul>
    <p>En Africa trabajaba con  diferentes grupos profesionales de Togo y pa�ses vecinos, como Ghana, Costa de  Marfil y Benin.&nbsp;<br>Con estos grupos he  participado en varios festivales, fiestas, carnavales, conferencias culturales  en Africa y en el resto del mundo, como en las giras� organizados por :</p>
    <ul>
      <li><b>CIOFF (Comisi�n  Internacional de Festival Folkl�rico) </b></li>
      <li><b>CIATP (Comisi�n  Internacional de Artes y Tradiciones Populares) por Francia, B�lgica,  Inglaterra, Italia, Alemania, Brasil, Espa�a y Rusia. </b></li>
    </ul>
    
    <p>Como profesor de  Percusi�n Africana ha impartido clases de percusi�n en <u>Europa</u> � </p>
    <ul>
      <li><b>Munchen, Bad�n-Bad�n  (Alemania) </b></li>
      <li><b>Lile, Lens, Chatearoux  (Francia) </b></li>
      <li><b>Barcelona, Zaragoza, Logro�o, Lleida, Las Palmas de Gran Canarias  (Espa�a)</b></li>
    </ul>
    
    <p>Ha participado en el  Festival de Cine de Francia en Zaragoza, 2001-08-19<br>
       Ha sido solista  principal del grupo �MAKUMBA� de Zaragoza, desde 1997 hasta 2002.<br>
       Desde a�o 2000 toca con  el grupo &quot;CAE MA DELA&quot; (Barcelona)� de Toni Espa�ol.<br>
       Actualmente  dirije el grupo �DJOKOTO � Ballet Africano de BCNa��compuesto por percusionistas, m�sicos,  cantantes y bailarinas de Togo, Costa de Marfil, Ghana y Burkina Faso.<br>
       Tambi�n  escribe y cuenta cuentos y los acompa�a con cora, balafon y cantos. �Al mismo tiempo colabora con el Cantautor,  Animador y Cuenta Cuentos Rah-Mon Roma en los espectaculos infantiles.<br>
       Actualmente  esta creando nuevas formas de la expresi�n musical con las que quiere:</p>
    <ul>
      <li><b>Difundir un mensaje a favor de la colaboraci�n y el  mestizaje cultural</b></li>
      <li><b>Ofrecer una imagen positiva de �frica, su gente, sus  culturas, sus m�sicas</b></li>
      <li><b>Ense�ar el ejemplo de las artistas que est�n en contra de  la intolerancia, el fanatismo, la xenofobia, y a favor de la libertad, la  solidaridad y la comprensi�n. </b></li>
    </ul>
             
<?php include("piecera.php"); ?>