<?php include("cabecera.php"); ?>

    <h1 class="b1">DISCOS DE DJOKOTO</h1>
    <p>En Febrero 2005 despues de todo un a�o de muchos esfuerzos, trabajo y ensayos intensos, el grupo  Djokoto ha grabado su primer disco denominado 'La  tierra y el ritmo'.</p>    
    <img src="img/disco_djokoto.gif" align="right" alt="Djokoto - La tierra y el ritmo" title="Djokoto - La tierra y el ritmo">
    <p>Aqu� os dejamos con unos canciones a escuchar...</p>
    <p><b>Djokoto - La tierra y el ritmo</b><br>Temas en "La tierra y el ritmo":</p>
    <ol>
      <li>DAGBENEVA (Ritmo de bienvenida)<br><a href="javascript:popup(1)"><img src="img/audio.gif" border="0" align="absmiddle"> Escuchar pista</a></li>
      <li>SENKUTE (Canci�n sobre la vida y el amor no correspondido de una joven)<br><a href="javascript:popup(2)"><img src="img/audio.gif" border="0" align="absmiddle"> Escuchar pista</a></li>
      <li>ATOPANI (El tambor habla)<br><a href="javascript:popup(3)"><img src="img/audio.gif" border="0" align="absmiddle"> Escuchar pista</a></li>
      <li>INITY (Calor humano, unidad) </li>
      <li>ATODJELIMATO (Sobre la vida cotidiana en pueblos de Africa) </li>
      <li>FULANU (El poder de la mujer) </li>
      <li>BOREMBO (Saludo a los musicos y bailarines) </li>
      <li>EYA EYA (Homenaje a Africa)</li>
      <li>TIKAYA (Ritmo del espiritu del bosque) </li>
      <li>DAGADO (El Destino) </li>
      <li>DJOKOTO (Video Track) </li>
    </ol>
    <p>En estos momentos el Grupo DJOKOTO esta trabajando para sacar a la luz su segundo disco.</p>
             
<?php include("piecera.php"); ?>