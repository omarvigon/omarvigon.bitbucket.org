<?php include("cabecera.php"); ?>

	<h1 class="b2">PRENSA</h1>
    <table cellspacing="2" cellpadding="2">
      <tr>
        <td><b>Festival Mangrana 2007 a Cambrils</b><br>10� festival de m�sica �tnica i popular. </td>
        <td width="150"><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle">Ver <a href="http://www.djokoto.com/pdf/prensa001.pdf" target="_blank">art�culo1</a> | <a href="http://www.djokoto.com/pdf/prensa002.pdf" target="_blank">art�culo2</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en Jaca - 1 de Agosto 2007</b></td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa003.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en Getafe - 30 de Junio 2007</b><br>Fiesta intercultural.</td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa004.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en Logro�o - 6 de Julio 2007</b><br>Diversons. M�sica para la integraci�n.</td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa005.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en San Miguel de Salinas - Agosto 2005</b><br>"La cultura siempre une"</td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa006.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en Isla Cristina - 16 deAgosto 2007</b><br>La multiculturalidad llega a Isla Cristina a trav�s de la m�sica. </td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa007.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>3� edici� del CPam Centelles Percussi�</b><br>"Estiu de Centelles 2007" </td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa008.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>La figura del griot: el guardi� de la mem�ria dels homes</b><br>a c�rrec de Ayao Djimedo - Grupo Djokoto </td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa009.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en Canet de Mar</b><br>"El grup de dansa i percussi� africana Djokoto, que  en el dialecte Afric� ewe significa..." </td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa011.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
      <tr>
        <td><b>Djokoto en Melilla 5 de Mayo 2007</b><br>"Los ritmos de �frica volaron sobre la Plaza de las Culturas de Melilla"</td>
        <td><img src="img/pdf.gif" alt="Djokoto prensa" align="absmiddle"><a href="http://www.djokoto.com/pdf/prensa012.pdf" target="_blank">Ver noticia completa</a></td>
      </tr>
    </table>
             
<?php include("piecera.php"); ?>