<?php include("cabecera.php"); ?>

    <h1 class="b1">DJOKOTO TRAYECTORIA</h1>
    <p>�Djokoto� cuyo significado es �calor de hermanos� en el dialecto ewe, de Togo esta compuesto por artistas, m�sicos, bailarines, bailarinas y cantantes profesionales de varios pa�ses de �frica Occidental que tras varias giras por Europa con los Ballets Nacionales de sus respectivos pa�ses (Togo, Benin, Senegal, Burkina Faso y Ghana) decidieron establecerse en Barcelona.</p>
    <p>Djokoto ofrece un maravilloso espect�culo basado en m�sica y danza tradicional africana, aderezado con escenas cotidianas de la vida africana. La percusi�n de piel (djembe, dun dun...) va acompa�ada por balaf�n e instrumentos de cuerda (cora) consiguiendo ambientes muy variados. La danza alterna pasajes r�tmicos con momentos de potente descarga de energ�a f�sica.</p>
    <p>A trav�s de cuentos, mitos,  leyendas y historias africanas; construimos nuestros espect�culos mezclando  danzas, mimos, teatro y m�sica para mostrar el pasaje� de la tradici�n folkl�rica a la modernidad. Los cuerpos y voz son los instrumentos primarios que permiten a  transmitir� la alegr�a, la uni�n y la paz  entre los pueblos. El repertorio del grupo DJOKOTO est� basado en los  ritmos y danzas de TOGO.</p>
    <p>El grupo se form� en Abril de  2004, pero ya ha hecho varios� conciertos  y espect�culos en Barcelona , Catalu�a y en toda Espa�a.</p>
    <p><u>Los conciertos a destacar son:</u></p>
    <ul>
      <li>Concierto en plaza Catalu�a el 25 de Septiembre durante  la fiesta de Merc�</li>
      <li>Concierto en Sant Boi el  3 de Octubre en �Barrejant 2004�, dedicado a �frica</li>
      <li>Espect�culo en Partes de Valles (Can Butjosa) organizado por Ayuntamiento de Parets de Valles en Noviembre 2004</li>
      <li>Concierto en Fira de Pur�sima en Sant Boi, durante la fiesta de Pur�sima en Diciembre 2005</li>
      <li>Concierto en �La Farinera� organizado por Ayuntamiento de  Barcelona</li>
      <li>Concierto en� Canet  de Mar organizado por �Ass. Re-percusi�n per L�ODEON�</li>
      <li>Concierto en Andorra organizado por Ayuntamiento</li>
      <li>Concierto y Talleres de Percusi�n y Danza Africana en  �Festizaje 2005� en Villafranca del Bierzo, Le�n</li>
      <li>Conciertos y Animaci�n en Regatas de Copa America en  Valencia en Mayo-Junio 2006, para acompa�ar el equipo sudafricano �Shosholoza�</li>
      <li>Conciertos organizados  por �La Caixa�  y �Diversons� en ciclos de M�sica del Mundo, en giras en toda Espana durante  ciclo de 2006-2007.</li>
      <li>Concierto en Mercat de M�sica Viva del Vic en Septiembre  2007</li>
    </ul>
    <p>DJOKOTO ofrece un abanico de espectaculos, conciertos,  actuaciones y pasacalles con una variedad de repertorios desde tradicional a  afrocontemporanea.�Tambien ofrecen una session de cuenta cuentos africanos  acompa�ado con percusion,� cora o  balafon.</p>
    <p>Con sus espectaculos DJOKOTO muestra lo que hace y  difunde la cultura Aficana aqui en Europa.</p>
             
<?php include("piecera.php"); ?>