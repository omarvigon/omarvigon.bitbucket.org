<?php include("cabecera.php"); ?>

    <h1 class="b2">ACTUACIONES</h1>
    <table cellspacing="1" cellpadding="1">
      <tr>
        <td><b>Fecha</b></td>
        <td><b>Lugar</b></td>
        <td><b>Actuacion</b></td>
      </tr>
      <tr>
        <td>15/11/2007</td>
        <td>Huelva</td>
        <td>Concierto de Djokoto</td>
      </tr>
      <tr>
        <td>26/10/2007</td>
        <td>Alicante</td>
        <td>Concierto Djokoto</td>
      </tr>
      <tr>
        <td>06/10/2007</td>
        <td>Barcelona - Parc de Cuitadela</td>
        <td>Actuacion de Djokoto en la Festa de la Cooperaci� Catalana</td>
      </tr>
      <tr>
        <td>13/09/2007</td>
        <td>Vic</td>
        <td>Concierto en Mercat de la Musica Viva del Vic</td>
      </tr>
      <tr>
        <td>02/09/2007</td>
        <td>Cambrils</td>
        <td>Concierto Djokoto organizado por �La Fundaci�n La Caixa�</td>
      </tr>
      <tr>
        <td>25/08/2007</td>
        <td>Calafel</td>
        <td>Conciet en el Castillo de Calafel</td>
      </tr>
      <tr>
        <td>09/08/2007</td>
        <td>Isla Cristina</td>
        <td>Actuaci�n en la  Isla Cristina</td>
      </tr>
      <tr>
        <td>02/08/2007</td>
        <td>Moraleja</td>
        <td>Concierto de Djokoto en Ciclo de Diversons</td>
      </tr>
      <tr>
        <td>01/08/2007</td>
        <td>Jaca</td>
        <td>Concierto de Djokoto en ciclo de Diversons</td>
      </tr>
      <tr>
        <td>26/07/2007</td>
        <td>Mallorca</td>
        <td>Conciertod de Djokoto en Palma de Mallorca</td>
      </tr>
      <tr>
        <td>14/07/2007</td>
        <td>Fuenlabrad/Madrid</td>
        <td>Actuaci�n de Djokoto en Festival Folclorico</td>
      </tr>
      <tr>
        <td>29/06/2007</td>
        <td>Getafe</td>
        <td>Actuaci�n de Djokoto en ciclo de Diversons</td>
      </tr>
      <tr>
        <td>27/06/2007</td>
        <td>Almeria/ Ejido</td>
        <td>Pasacalle de Percusi�n y Danza protagonizado por  Djokoto</td>
      </tr>
      <tr>
        <td>01/06/2007</td>
        <td>Jerez de la Frontera</td>
        <td>Conciert</td>
      </tr>
      <tr>
        <td>07/07/2007</td>
        <td>Centelles</td>
        <td>Pasacalle con Percusi�n y Danza</td>
      </tr>
      <tr>
        <td>12/05/2007</td>
        <td>Barcelona</td>
        <td>Concierto de Djokoto en Forum de la Fundacion �LaCaixa�</td>
      </tr>
      <tr>
        <td>05/05/2007</td>
        <td>Melilla</td>
        <td>Concierto de Djokoto en Ciclo �Diversons�</td>
      </tr>
      <tr>
        <td>07/02/2007</td>
        <td>Esplugas</td>
        <td>Passacalle en el Carnaval de Esplugas</td>
      </tr>
      <tr>
        <td>06/01/2007</td>
        <td>Puigcerd�</td>
        <td>Pasacalle-animacion en la Cavalcada de Reyes</td>
      </tr>
      <tr>
        <td>11/11/2006</td>
        <td>Massanet de la Selva</td>
        <td>Concierto de Djokoto </td>
      </tr>
      <tr>
        <td>08/10/2006</td>
        <td>Gava</td>
        <td>Concieto de� Djokoto en Fiestas de Gava</td>
      </tr>
      <tr>
        <td>15/09/2006</td>
        <td>Barcelona </td>
        <td>Batukada en la Discoteca �Pacha� � en la Fiest de �Bacardi�</td>
      </tr>
      <tr>
        <td>17/06/2006</td>
        <td>Mollet del Valles</td>
        <td>Actuacion en Centro Cultural �La Marinera�</td>
      </tr>
      <tr>
        <td>07/06/2006</td>
        <td>Lerida</td>
        <td>Concierto de Djokoto en ciclo de Diversons</td>
      </tr>
      <tr>
        <td>12-19/05/2006</td>
        <td>Valencia</td>
        <td>Actuacion en �Copa America� para apoyar el equipo de    Sud Africa � �Shosholoza�</td>
      </tr>
      <tr>
        <td>12/12/2005</td>
        <td>Sant  Boi</td>
        <td>Animacion en fiesta de Purissima</td>
      </tr>
      <tr>
        <td>17/12/2005</td>
        <td>Barcelona</td>
        <td>Batukada de Percusion en Pavillon Vall d�Hebron</td>
      </tr>
      <tr>
        <td>19/08/2005</td>
        <td>Alicante/San Miguel de Salinas</td>
        <td>Concieto de Djokoto en Festival de Folklore</td>
      </tr>
      <tr>
        <td>22-23/07/2005</td>
        <td>Leon</td>
        <td>Concieto de Djokoto y Talleres de Danza y Percucion</td>
      </tr>
      <tr>
        <td>10/06/2005</td>
        <td>Canet de Mar</td>
        <td>Concierto de Djokoto</td>
      </tr>
      <tr>
        <td>01/06/2005</td>
        <td>Tarragona</td>
        <td>Concierto de percussion en �Port Aventura�</td>
      </tr>
      <tr>
        <td>27/03/2005</td>
        <td>Barcelon </td>
        <td>Concierto de Djokot en �La Farinera�</td>
      </tr>
      <tr>
        <td>19/11/2004</td>
        <td>Paret del Valles</td>
        <td>Concierto de Djokoto en Can Butjosa</td>
      </tr>
      <tr>
        <td>01/10/2004</td>
        <td>Sant Boi de LLbt</td>
        <td>Animacion en Barrejant 2004</td>
      </tr>
      <tr>
        <td>25/09/2004</td>
        <td>Barcelona</td>
        <td>Concierto en la Fiesta de Merce</td>
      </tr>
    </table>
             
<?php include("piecera.php"); ?>