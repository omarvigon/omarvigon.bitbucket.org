<?php include("cabecera.php"); ?>

    <h1 class="b2">CLASSES DE PERCUSION AFRICANA</h1>
    <p>Desde a�o 1997 Ayao Logosse  esta impartiendo clases y talleres de Percusi�n Africana en Espa�a.</p>
    <ul>
    <li>De 1997 hasta 2001 dirig�a clases de percusi�n en  Zaragoza, en varios Casas de la   Juventud, y tambi�n en la �Escuela Municipal de Zaragoza�</li>
    <li>Durante 2001 y 2002 ha sido el profesor de  Percusi�n Africana en Centro Insular de Danza y de Ritmo &quot;MARIA EULATE en �Las Palmas de Gran Canaria� </li>
    <li>Durante 2003 y 2004 ha realizado talleres y clases  de en el Espai Jove de Boca Nord, en el distrito de Horta en Barcelona.</li>
    <li>Actualmente�  esta complementando su trabajo como Profesor en el Centro C�vico El  Sortidor en el Poble Sec de Barcelona</li>
    </ul>
    <p>La realizaci�n de Talleres de Percusi�n Africana conlleva una  propuesta de la educaci�n multicultural.</p>
    <p>El instrumento principal que se utiliza en los talleres es el DJEMBE.</p>
    <p>El tambor  denominado �Djemb� es un instrumento musical de percusi�n Africana muy popular  y ampliamente conocido por el mundo occidental.</p>
    <p>Su origen  est� en el �frica Occidental, se utiliza para acompa�ar ciertos rituales, y  �stos a su vez tienen asignados diferentes tipos de ritmos.</p>
    <p>La propuesta did�ctica consiste en clases pr�cticas en  las que se pretende que el alumno aprenda t�cnicas - toques y sonidos- que le  permitir�n crear ritmos b�sicos, estas clases se acompa�ar�n de exposiciones  te�ricas del profesor abiertas a un debate con los alumnos.</p>
    <p>Objetivo inicial es ense�ar  a los alumnos a expresarse a trav�s de los ritmos.</p>
    <p><u>Las  unidades Did�cticas a utilizar son:</u></p>
    <ul>
    <li>La  percusi�n y el djemb� en la cultura africana. </li>
    <li>El  djemb� y los ritmos� en los rituales.</li>
    <li>Fabricaci�n  del djemb�. </li>
    <li>T�cnicas  b�sicas: toques y sonidos; y ritmos b�sicos</li>
    <li>Perfeccionamiento  de ritmos.</li>
    <li>Canciones  que acompa�an algunos ritmos</li>
    </ul>
    <p>El Objetivo  final del Profesor es transmitir a los alumnos sus conocimientos para que al  finalizar el curso los alumnos est�n preparados para realizar un peque�o  concierto dirigido por el Profesor.</p>
    <p>A parte de las clases de  duraci�n continua, Ayao ha organizado varios Talleres Intensivos� de Fin de Semana.</p>
    <p><u>Algunos talleres a  destacar son:</u></p>
    <ul>
    <li>1997 en Miraverte en Teruel</li>
    <li>1998 en Canfranc de Pirineos</li>
    <li>2000 en MACBA en Barcelona</li>
    <li>2004 en Logro�o</li>
    <li>2004 en Casal de Verano en L�rida</li>
    <li>2005en la Masia de Tipiwacan en Massanet de la Selva (Barcelona)</li>
    <li>2006 en Centro C�vico el Surtidor (Barcelona)</li>
    </ul>
             
<?php include("piecera.php"); ?>