<?php include("1cabecera.php");?>
    <title>GENERAL DESAG&Uuml;ES Servicios Reparacion de desagues Barcelona</title>
	<meta name="title" content="GENERAL DESAG&Uuml;ES Servicios Reparacion de desagues Barcelona" />
	<meta name="description" content="GENERAL DESAG&Uuml;ES BARCELONA Reparacion de desagues, reparacion de albañales Limpieza y anulacion de fosas septicas y desagues en general" />
	<meta name="keywords" content="GENERAL DESAG&Uuml;ES BARCELONA, desagues, desague barcelona, reparacion desagues ,reparacion desague, desagues barcelona, reparacion albañales, reparacion albañal barcelona," />
<?php include("2cabecera.php");?>

    <h2>SERVICIOS</h2>
	<p>Desde siempre el objetivo de General Desag&uuml;es es seguir creciendo, avanzando y  construyendo para responder a las necesidades del mercado en la reparaci&oacute;n, sustituci&oacute;n y construcci&oacute;n de desag&uuml;es:</p>
	<ul>
	  <li>Reparaci&oacute;n  de alba&ntilde;ales, tanto exteriores en v&iacute;a p&uacute;blica como interiores en edificios. </li>
	  <li>Construcci&oacute;n  y reparaci&oacute;n de redes de desag&uuml;es colectores enterrados o a&eacute;reos.</li>
	  <li>Sustituci&oacute;n  de desag&uuml;es y bajantes verticales en edificios.</li>
	  <li>Anulaci&oacute;n  de pozos de agua fecal.</li>
	  <li>Reposici&oacute;n  de catas en asfalto caliente.</li>
	</ul>
	<p>Disponemos de los servicios de inspecci&oacute;n con  equipo TV y detecci&oacute;n por radar para redes de colectores enterrados.</p>
	<p>Nuestra area principal de servicio de reparaci&oacute;n de desag&uuml;es es la provincia de Barcelona.</p>
    
<?php include("3piecera.php");?>