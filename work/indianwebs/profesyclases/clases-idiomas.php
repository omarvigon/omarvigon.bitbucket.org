<?php include("includes/1cabecera.php");?>
    <title>CLASES DE IDIOMAS PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="CLASES DE IDIOMAS PROFES Y CLASES BARCELONA" />
    <meta name="description" content="Clases particulares de ingles y otros idiomas. Clases de idiomas personalizadas a cualquier nivel. PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases idiomas, clases ingles, clases frances, clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2>CLASES DE IDIOMAS</h2>
			<?php switch($_GET['pag'])
			{
				default:
				case "ingles": ?>  
            <img title="" src="img/clases_idiomas2.jpg" class="img_der" alt="" />
            <p><b>PROFES&amp;CLASES</b>, especialistas en clases particulares a  domicilio, ofrece a sus alumnos, ni&ntilde;os, adolescentes y adultos un amplio abanico de idiomas con un formato tan flexible como son las clases  particulares a domicilio. En el lugar, el d&iacute;a y la hora que le vaya bien al  alumno.</p>
            <p>Cada alumno es &uacute;nico, por lo tanto nuestros profesores de  idiomas, la mayor&iacute;a nativos, dise&ntilde;ar&aacute;n con nuestras pedagogas un plan de  trabajo en funci&oacute;n del perfil de cada alumno, de su nivel inicial, del tiempo  que pueda dedicar a las clases particulares de idiomas y sobre todo al objetivo  que marque el alumno.</p>
            <h3>CLASES PARTICULARES DE INGL&Eacute;S</h3>
            <p><em>&raquo; Clases particulares  de ingl&eacute;s para ni&ntilde;os de 4 a  10 a&ntilde;os</em></p>
            <p>Las clases particulares de ingl&eacute;s para alumnos de estas  edades, est&aacute;n pensadas para que el primer contacto del ni&ntilde;o con un nuevo idioma  sea l&uacute;dico y divertido. Nuestros profesores organizar&aacute;n clases con materiales  adaptados a alumnos que en algunos casos aun no leen. El objetivo para estos  alumnos es desarrollar el gusto por el idioma y las ganas por aprenderlo.</p>
            <p><em>&raquo; Clases particulares  de ingl&eacute;s para j&oacute;venes de 11 a  18 a&ntilde;os</em></p>
            <p>La mayor&iacute;a de los alumnos estudian ingl&eacute;s en el colegio,  para reforzar un programa escolar de ingl&eacute;s, que no siempre garantiza un nivel  de conocimiento del idioma exigido en un entorno acad&eacute;mico y laboral cada vez m&aacute;s competitivo.</p>
            <p><em>&raquo; Clases particulares  de ingl&eacute;s para adultos</em></p>
            <p>Para cualquier persona que quiera mejorar su nivel de ingl&eacute;s,  tenga la edad que tenga. Para un adulto compartir la clase en una academia con  alumnos mucho m&aacute;s j&oacute;venes no resulta nada atractivo, ni eficiente.<br>
              Con un profesor particular de ingl&eacute;s, el alumno adulto podr&aacute;  aprender a su ritmo. Adem&aacute;s, la libre elecci&oacute;n de horario es una de las  ventajas m&aacute;s apreciadas de las clases particulares a domicilio.</p>
                <? break;
				
				case "otros": ?>
            <h3>CLASES PARTICULARES DE CUALQUIER IDIOMA Y NIVEL</h3>
            <img title="" src="img/clases_frances.jpg" class="img_der" alt="" />
            <p>Clases particulares de franc&eacute;s,  clases particulares de alem&aacute;n, clases particulares de chino, clases  particulares de ruso, clases particulares de italiano, clases particulares de  &aacute;rabe, etc...</p>
            <p>Clases particulares de catal&aacute;n  y clases de espa&ntilde;ol para extranjeros.<br>Sea el idioma que sea, le  podemos ofrecer nuestro servicio de clases particulares a domicilio, con  profesores rigurosamente seleccionados, experimentados y con titulaci&oacute;n para  poder ense&ntilde;ar.</p>
            <p><em>&raquo; Nuestros  profesores particulares son:</em></p>
            <ul>
            <li>Profesores nativos, con  titulaci&oacute;n y experiencia para ense&ntilde;ar el idioma como lengua extranjera.</li>
            <li>Licenciados en filolog&iacute;a  (inglesa, francesa, alemana, catalana&hellip;), titulares del CAP (certificado de  aptitud pedag&oacute;gica).</li>
            <li>Diplomados en magisterio,  especialidad lengua extranjera (para los m&aacute;s peque&ntilde;os)</li>
            </ul>
            <p><em>&raquo; Niveles  establecidos</em></p>
            <p><b>Nivel Principiante</b></p>
            <ul>
            <li>Comprender frases y vocabulario m&aacute;s frecuente</li>
            <li>Comunicarse con informaci&oacute;n sencilla</li>
            <li>Comprender textos f&aacute;ciles</li>
            </ul>
            <p><b>Nivel intermedio</b></p>
            <ul>
            <li>Comprender las ideas principales de un discurso o de un  programa de televisi&oacute;n</li>
            <li>Mantener una conversaci&oacute;n de manera correcta</li>
            <li>Escribir textos sencillos (e-mails, correspondencia&hellip;)</li>
            </ul>
            <p><b>Nivel avanzado</b></p>
            <ul>
            <li>Comprender discursos complejos</li>
            <li>Expresar ideas con precisi&oacute;n</li>
            <li>Escribir textos detallados con un vocabulario elaborado</li>
            </ul>
                <? break;
            } ?> 
            <p>Para definir con nuestras pedagogas el ritmo &oacute;ptimo de  clases particulares <a href="contacta.php">contacte con nosotros</a>.</p>
        </div>
<?php include("includes/3piecera.php");?>