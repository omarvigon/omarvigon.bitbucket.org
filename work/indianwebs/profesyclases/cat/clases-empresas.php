<?php include("../includes/1cabecera.php");?>
    <title>FORMACION A EMPRESAS PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="FORMACION A EMPRESAS PROFES Y CLASES BARCELONA" />
    <meta name="description" content="PROFES y CLASES ofrece formacion dirigida a empresas para  mejorar la competitividad de sus colaboradores. Clases y cursos personalizados para empresas" />
    <meta name="keywords" content="formacion empresas,cursos empresas,clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
            <h2><strong>FORMACI&Oacute; A EMPRESES</strong></h2>
            <img title="" src="../img/clases_empresas2.jpg" class="img_der" alt="" />
        <p><strong>PROFES&amp;CLASES</strong> ofereix formaci&oacute; dirigida a empreses per a millorar la competitivitat  dels seus col&middot;laboradors. En una primera entrevista, els nostres formadors  configuraran un <strong>pla d&rsquo;estudis adaptat</strong>.</p>
        <p>Els nostres docents, tots professionals de la  formaci&oacute; han estat rigorosament seleccionats per la seva titulaci&oacute;, experi&egrave;ncia  i les seves dots pedag&ograve;giques.</p>
        <p>El nostre professorat actualitza constantment els  seus coneixements i la seva metodologia per a adaptar-se a les necessitats de  professionals tant en <strong>classes particulars</strong> com en grups.</p>
        <p>Escolliu els <strong>horaris segons la seva  conveni&egrave;ncia</strong> per a interferir el m&iacute;nim possible en l&rsquo;activitat de la seva  empresa.</p>
<h3><strong>LES NOSTRES &Agrave;REES DE FORMACI&Oacute; A EMPRESES:</strong></h3>
            <ul>
            
              <li>Idiomes (Classes d&rsquo;Angl&egrave;s, Classes de Franc&egrave;s, Classes d&rsquo;Alemany,       Classes de Xin&egrave;s, Classes d&rsquo;Itali&agrave;, Classes de Rus, Classes de Japon&egrave;s,       ...)</li>
              <li>Actualitzaci&oacute; de coneixements (Classes de Comptabilitat, Classes       d&rsquo;Administraci&oacute;, Classes de Fiscalitat, Classes de Marketing i Vendes...)</li>
              <li>Ofim&agrave;tica (Word, Excel, Internet, Access, Power Point) </li>
            </ul> 
        <p>Per a necessitats espec&iacute;fiques, truqui&rsquo;ns <em>93 254 00 48</em>, en <strong>PROFES&amp;CLASES</strong> dissenyarem un pla a  mida i calcularem un pressupost ajustat.</p>
        <h3><strong>LES NOSTRES F&Ograve;RMULES DE FORMACI&Oacute; A EMPRESES:</strong></h3>
            <ul>
              <li>Classes particulars</li>
              <li>Classes en grups a les seves oficines</li>
            </ul>
            <p>Per a definir amb les nostres pedagogues el ritme &ograve;ptim de classes  particulars <a href="contacta.php">contacti amb nosaltres</a></p>
        </div>
<?php include("../includes/3piecera.php");?>