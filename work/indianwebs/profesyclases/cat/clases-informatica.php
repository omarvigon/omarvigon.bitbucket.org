<?php include("../includes/1cabecera.php");?>
    <title>CLASES DE INFORMATICA PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="CLASES DE INFORMATICA PROFES Y CLASES BARCELONA" />
    <meta name="description" content="Clases de informatica. Clases particulares de informatica. Clases de ofimatica, clases de diseño por ordenador. PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases informatica, clases ofimatica,clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2><strong>CLASSES D&rsquo;INFORM&Agrave;TICA</strong></h2>  
            <img title="" src="../img/clases_informatica.jpg" class="img_der" alt="" />  
	    <p>Dominar les eines inform&agrave;tiques s&rsquo;ha convertit avui en dia en una  necessitat b&agrave;sica, tant important com saber llegir i escriure.</p>
        <p>Qualsevol que siguin les seves necessitats, l&rsquo;ajudarem a comprendre i  dominar des dels programes inform&agrave;tics m&eacute;s b&agrave;sics fins als m&eacute;s complexes i  sofisticats.</p>
        <p>En <strong>PROFES&amp;CLASES</strong>, tenim un  equip d&rsquo;especialistes inform&agrave;tics amb habilitats pedag&ograve;giques capa&ccedil;os de posar  a l&rsquo;abast de tothom la majoria de les aplicacions inform&agrave;tiques disponibles en  el mercat.</p>
<h3><strong>CLASSES PARTICULARS D&rsquo;INFORM&Agrave;TICA PER A PRINCIPIANTS</strong></h3>
		  <p>Classes particulars d&rsquo;iniciaci&oacute; a la inform&agrave;tica, per als que no van tenir  ocasi&oacute; d&rsquo;accedir a la inform&agrave;tica abans i que no volen renunciar a totes les  seves avantatges: poder navegar per Internet, enviar correus electr&ograve;nics, crear  els seus propis discos de m&uacute;sica, gestionar les seves fotografies, portar els  comptes dom&egrave;stics, etc...</p>
	
    		<img title="" src="../img/clases_empresas.jpg" class="img_der" alt="" />
	    <h3><strong>CLASSES PARTICULARS D&rsquo;OFIM&Agrave;TICA</strong></h3>
		  <p>Per a ser m&eacute;s eficient a l&rsquo;oficina, avui en dia &eacute;s imprescindible dominar  els programes d&rsquo;ofim&agrave;tica m&eacute;s comuns. Per a millorar el seu nivell, li proposem  classes d&rsquo;Excel, Word, PowerPoint, Access etc&hellip;</p>

		  <h3><strong>CLASSES PARTICULARS D&rsquo;INFORM&Agrave;TICA DE DISENY</strong></h3>
			<ul>
              <li>Classes particulars per a programes com Photoshop, Freehand,       Dreamweaver,&nbsp; etc&hellip; </li>
              <li>Classes particulars per a programes de dibuix assistit per ordenador,       com Autocad 2D y 3D </li>
			</ul>
	    <h3><strong>CLASSES PARTICULARS D&rsquo;INFORM&Agrave;TICA PER ALS M&Eacute;S AVAN&Ccedil;ATS</strong></h3>
			<p>Necessita per motius professionals o d&rsquo;estudis avan&ccedil;ats, dominar eines  inform&agrave;tiques m&eacute;s sofisticades? Contacti amb nosaltres, tenim professors  especialistes en llenguatges de programaci&oacute; (Java, C, C++, Visual Basic, .Net,  etc&hellip;)</p>
        </div>
<?php include("../includes/3piecera.php");?>