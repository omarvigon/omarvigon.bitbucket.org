<?php include("../includes/1cabecera.php");?>
    <title>Clases particulares y profesores a domicilio en Barcelona</title>
    <meta name="title" content="PROFES Y CLASES BARCELONA clases particulares y profesores a domicilio" />
    <meta name="description" content="PROFES Y CLASES BARCELONA Para cualquier necesidad educativa a medida en la provincia de Barcelona, le proponemos un servicio dedicado en exclusiva al exito. Clases Particulares y profesores a domicilio. Clases de idiomas, clases de informatica, clases para empresas" />
    <meta name="keywords" content="clases barcelona,profesores barcelona,cursos barcelona,clases idiomas,clases informatica,clases para empresas" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2><strong>L&rsquo;Ensenyament a mida sempre es la soluci&oacute; adequada</strong></h2>
			<img title="" src="../img/profes_clases.jpg" style="float:right;margin:0 0 5px 5px;" alt="" />
        <h3><strong>Som especialistes en classes particulars a domicili per a  refor&ccedil; escolar, idiomes, inform&agrave;tica, oci i formaci&oacute; per adults.</strong></h3>
        <h3><strong>Per a qualsevol necessitat educativa a mida en la prov&iacute;ncia  de Barcelona, li proposarem un servei dedicat en exclusiva a l&rsquo;&egrave;xit.</strong></h3>
	    <p>Classes particulars dissenyades per a adaptar-se a les necessitats de  l&rsquo;alumnat i a la seva disponibilitat hor&agrave;ria.<br />
Els nostres compromisos s&oacute;n:</p>
        <ul type="disc">
          <li>Posar a       disposici&oacute; dels nostres alumnes un equip docent qualificat y experimentat</li>
          <li>Garantir un       servei de seguiment pedag&ograve;gic &ograve;ptim</li>
          <li>Oferir les       tarifes m&eacute;s competitives</li>
          <li>Adaptar-nos       a les necessitats i gustos del nostre alumnat</li>
          <li>Lliure       elecci&oacute; d&rsquo;horaris</li>
          <li>Possibilitat       de canviar de professor si vost&egrave; ho vol</li>
        </ul>
        <p>Sol&middot;licitar els serveis de <strong>PROFES  &amp; CLASES</strong> a domicili, &eacute;s la garantia d&rsquo;estalviar temps i diners  assegurant l&rsquo;&egrave;xit de cada alumne.</p>
        </div>
<?php include("../includes/3piecera.php");?>