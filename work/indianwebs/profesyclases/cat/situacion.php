<?php include("../includes/1cabecera.php");?>
    <title>PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="PROFES Y CLASES BARCELONA" />
    <meta name="description" content="PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2>DONDE ESTAMOS</h2>
            <h3><em>Agencia Sarri&aacute; - Les Corts</em><br />Ronda General Mitre 139  08022 BARCELONA<br />Tel&eacute;fono (+34) 93.254.00.48</h3>
            <p><iframe width="540" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?f=q&amp;hl=es&amp;geocode=&amp;q=Ronda+General+Mitre+139+08022+BARCELONA&amp;sll=40.396764,-3.713379&amp;sspn=8.280213,14.150391&amp;ie=UTF8&amp;ll=41.411707,2.141733&amp;spn=0.007967,0.013819&amp;z=14&amp;iwloc=addr&amp;output=embed&amp;s=AARTsJr09aLbMGr7f-ioL-6zx5h8kCxOJw"></iframe></p>
            
            <h3><em>Agencia Eixample - Centro</em><br />Avenida de Roma, 113, 08011 BARCELONA<br />Tel&eacute;fono (+34) 93.323.90.73</h3>
            <p><iframe width="540" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?f=q&amp;hl=es&amp;geocode=&amp;q=Avenida+de+Roma,+113,+08011+BARCELONA&amp;sll=41.404545,2.138128&amp;sspn=0.007967,0.013819&amp;ie=UTF8&amp;ll=41.394968,2.160101&amp;spn=0.007969,0.013819&amp;z=14&amp;g=Avenida+de+Roma,+113,+08011+BARCELONA&amp;iwloc=cent&amp;output=embed&amp;s=AARTsJqKq0Jr4mLQ0MufKmqtFGlh2w2plw"></iframe></p>
        </div>
<?php include("../includes/3piecera.php");?>