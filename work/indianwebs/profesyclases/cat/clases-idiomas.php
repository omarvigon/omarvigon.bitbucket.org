<?php include("../includes/1cabecera.php");?>
    <title>CLASES DE IDIOMAS PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="CLASES DE IDIOMAS PROFES Y CLASES BARCELONA" />
    <meta name="description" content="Clases particulares de ingles y otros idiomas. Clases de idiomas personalizadas a cualquier nivel. PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases idiomas, clases ingles, clases frances, clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2><strong>CLASSES D&rsquo;IDIOMES</strong></h2>
			<?php switch($_GET['pag'])
			{
				default:
				case "ingles": ?>  
            <img title="" src="../img/clases_idiomas2.jpg" class="img_der" alt="" />
        <p><strong>PROFES&amp;CLASES</strong>, especialistes en classes particulars a domicili, ofereix als seus  alumnes, nens, adolescents i adults un gran i divers ventall d&rsquo;idiomes amb un  format tant flexible com s&oacute;n les classes particulars a domicili. En el lloc,  dia i hora que sigui millor per a l&rsquo;alumne.</p>
        <p>Cada alumne &eacute;s &uacute;nic, per tant, els nostres professors  d&rsquo;idiomes, la majoria nadius, dissenyaran juntament amb les nostres pedagogues  un pla de treball en funci&oacute; del perfil de cada alumne, del seu nivell inicial,  del temps que pugui dedicar-li a les classes d&rsquo;idiomes i sobretot a l&rsquo;objectiu  que marqui el propi alumne.</p>
<h3><strong>CLASSES PARTICULARS D&rsquo;ANGL&Egrave;S</strong></h3>
          <p><em>&raquo; Classes particulars d&rsquo;angl&egrave;s per a nens de 4 a 10 anys</em></p>
          <p>Les classes particulars d&rsquo;angl&egrave;s per a alumnes d&rsquo;aquestes edats, estan  pensades per a que el primer contacte del nen amb un nou idioma sigui l&uacute;dic i  divertit. El professor particular organitzar&agrave; la classe amb materials adaptats  al alumne, que en alguns casos ni tan sols llegeixen encara. L&rsquo;objectiu per a  aquests alumnes es desenvolupar el gust per l&rsquo;idioma i les ganes per  aprendre&rsquo;l.</p>
          <p><em>&raquo; Classes particulars d&rsquo;angl&egrave;s per a joves de 11 a 18 anys</em></p>
          <p>La majoria dels alumnes estudia angl&egrave;s a l&rsquo;escola, per refor&ccedil;ar un programa  escolar d&rsquo;angl&egrave;s, que no sempre garanteix un nivell de coneixement de l&rsquo;idioma  exigit en un entorn acad&egrave;mic i laboral cada cop m&eacute;s competitiu.</p>
            <p><em>&raquo; Classes particulars d&rsquo;angl&egrave;s per a adults</em></p>
            <p>Per a qualsevol persona que vulgui millorar el seu nivell d&rsquo;Angl&egrave;s, tingui  l&rsquo;edat que tingui. Per a un adult compartir la classe en una acad&egrave;mia amb  alumnes molt m&eacute;s joves no resulta gens atractiu, ni eficient.<br />
Amb un professor particular d&rsquo;angl&egrave;s, l&rsquo;alumne adult podr&agrave; aprendre al seu  ritme. A mes, la lliure elecci&oacute; d&rsquo;horari &eacute;s un dels avantatges m&eacute;s apreciats de  les classes particulars a domicili.</p>
                <? break;
				
				case "otros": ?>
        <h3><strong>CLASSES PARTICULARS DE QUALSEVOL IDIOMA I NIVELL</strong></h3>
            <img title="" src="../img/clases_frances.jpg" class="img_der" alt="" />
        <p>Classes particulars de franc&egrave;s, classes particulars d&rsquo;alemany, classes  particulars de xin&egrave;s, classes particulars de rus, classes particulars d&rsquo;itali&agrave;,  classes particulars d&rsquo;&agrave;rab, etc.</p>
        <p>Classes particulars de catal&agrave; i classes d&rsquo;espanyols per a estrangers.<br />
          Sigui l&rsquo;idioma que sigui li podem oferir els nostres serveis de classes  particulars a domicili, amb professors rigorosament seleccionats, experimentats  i amb una titulaci&oacute; per a poder ensenyar.</p>
        <p><em>&raquo; Els nostres professors particulars s&oacute;n:</em></p>
            <ul>
            
              <li>Professors nadius, amb titulaci&oacute; i experi&egrave;ncia per a ensenyar l&rsquo;idioma       com a llengua estrangera</li>
              <li>Llicenciats en filologia (anglesa, francesa, alemanya, catalana...),       titulars del CAP (Certificat d&rsquo;Aptitud Pedag&ograve;gica)</li>
              <li>Diplomats en Magisteri, especialitat Llengua Estrangera (per als mes       petits)</li>
            </ul>
        <p><em>&raquo; Nivells establerts</em></p>
          <p><strong>Nivell Principiant</strong></p>
            <ul>
              <li>Comprendre frases i vocabulari m&eacute;s freq&uuml;ent</li>
              <li>Comunicar-se amb informaci&oacute; senzilla</li>
              <li>Comprendre textos f&agrave;cils</li>
            </ul>
            <p><strong>Nivell intermig</strong></p>
            <ul>
              <li>Comprendre les idees principals d&rsquo;un discurs o d&rsquo;un programa de       televisi&oacute;</li>
              <li>Mantenir una conversa de manera correcta</li>
              <li>Escriure textos senzills (emails, correspond&egrave;ncia...)</li>
            </ul>
            <p><strong>Nivell avan&ccedil;at</strong></p>
            <ul>
              <li>Comprendre discursos complexes</li>
              <li>Expressar idees amb precisi&oacute;</li>
              <li>Escriure textos detallats amb un vocabulari elaborat</li>
            </ul>
                <? break;
            } ?> 
            <p>Per a definir amb les nostres pedagogues el ritme &ograve;ptim de les seves  classes particulars: <a href="contacta.php">contacti amb nosaltres</a>.</p>
        </div>
<?php include("../includes/3piecera.php");?>