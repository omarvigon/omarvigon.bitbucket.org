<?php include("../includes/1cabecera.php");?>
    <title>PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="PROFES Y CLASES BARCELONA" />
    <meta name="description" content="PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2><strong>REFOR&Ccedil; ESCOLAR</strong></h2>
            
            <?php switch($_GET['pag'])
			{
				default:
				case "primaria": ?>            
        <h3><strong>PROFES&amp;CLASES PRIM&Agrave;RIA</strong></h3>
            <p><em>&raquo;Ajuda amb els deures</em></p>
          <img title="" src="../img/clases_primaria.jpg" class="img_der" alt="" />
          <p>Per tal que els seus fills aprenguin les seves lli&ccedil;ons de manera adequada i  adquireixin bons h&agrave;bits d&rsquo;estudi, &eacute;s important que l&rsquo;alumne aconsegueixi unes  bases s&ograve;lides. Un professor particular competent i pluri-disciplinar ajudar&agrave;  als seus fills a millora la lectura, l&rsquo;ortografia i la gram&agrave;tica sense oblidar  la geometria i el c&agrave;lcul.</p>
          <p>Les classes particulars d&rsquo;ajuda amb els deures s&oacute;n:</p>
          
          <ul>
            <li>Per fer els       deures de l&rsquo;escola</li>
            <li>Per repassar       les lli&ccedil;ons</li>
            <li>Per aprendre       a millorar les seves t&egrave;cniques d&rsquo;estudi</li>
          </ul>
            
        <p>&nbsp;</p>
<p><em>&raquo; Classes particulars de refor&ccedil;</em></p>
          <p>Per a que els seus fills se sentin c&ograve;modes amb la geometria i el c&agrave;lcul,  les classes particulars de matem&agrave;tiques s&oacute;n la soluci&oacute; adaptada. Per a que  tinguin unes bases s&ograve;lides en gram&agrave;tica, ortografia i lectura opti per les  classes particulars de llengua.<br />
Amb un professor particular de refor&ccedil;, els seus fills:</p>
          <ul type="disc">
            <li>Dominaran i       assimilaran els conceptes fonamentals</li>
            <li>Estaran       millor preparats per a la Secund&agrave;ria</li>
            <li>Milloraran       la seva metodologia d&rsquo;estudi</li>
          </ul>
          <p>El nostre professorat &eacute;s:</p>
          <ul type="disc">
            <li>Titulat</li>
            <li>Generalistes       o especialistes amb qualitats pedag&ograve;giques</li>
            <li>Experimentats       per a entendre les necessitats de l&rsquo;alumnat </li>
          </ul>
                <? break;
				
				case "secundaria": ?>
        <h3><strong>PROFES&amp;CLASES SECUND&Agrave;RIA</strong></h3>
            <img title="" src="../img/clases_idiomas.jpg" class="img_der" alt="" />
            
        <p><em>&raquo; Classes particulars a mida</em></p>
          <p>A <strong>PROFES &amp; CLASES</strong>, creiem  que la personalitzaci&oacute; de l&rsquo;ensenyament garanteix l&rsquo;&egrave;xit escolar de l&rsquo;alumne.  Amb un pla de treball dissenyat en funci&oacute; de les dificultats de l&rsquo;alumne i de la  seva personalitat definirem uns objectius que el professor desenvolupar&agrave; durant  el per&iacute;ode de refor&ccedil;.</p>
          <p>M&eacute;s del 90% dels nostres alumnes aconsegueixen millorar les seves notes  entre 2 i 3 punts per mat&egrave;ria. Proposem classes particulars en les assignatures  cient&iacute;fiques: classes de matem&agrave;tiques, classes particulars de f&iacute;sica i qu&iacute;mica,  classes de ci&egrave;ncies naturals...</p>
          <p>Tamb&eacute; per a les mat&egrave;ries d&rsquo;humanitats, classes de llengua i literatura  catalana, classes particulars de llengua i literatura castellana, classes  d&rsquo;hist&ograve;ria i geografia, classes particulars d&rsquo;angl&egrave;s.</p>

            <img title="" src="../img/clases_secundaria.jpg" class="img_der" alt="" /> 
        <p><em>&raquo; Tutoria, Ajuda amb els deures</em></p>
          <p>El seguiment tutoritzat dels deures &eacute;s un element imprescindible per a  l&rsquo;&egrave;xit de l&rsquo;alumnat en la seva fase d&rsquo;adaptaci&oacute; a la ESO, donat que el  professorat exigeix m&eacute;s autonomia per part de l&rsquo;alumnat que en la Prim&agrave;ria.</p>
          <p>El professor particular l&rsquo;ajudar&agrave;:</p>
          <ul>
            <li>a repassar i       comprendre les seves lli&ccedil;ons</li>
            <li>a organitzar       la seva feina i les seves tasques</li>
            <li>a tenir       bones t&egrave;cniques d&rsquo;estudi</li>
          </ul>
          <p>El nostre professorat &eacute;s:</p>
          <ul>
            <li>titulat i       qualificat</li>
            <li>experimentat       amb alumant de nivell similar</li>
            <li>seleccionat       per les seves capacitats pedag&ograve;giques</li>
          </ul>
<p>Per &nbsp;a definir amb les nostres  pedagogues el ritme &ograve;ptim de les seves classes particulars <a href="contacta.php"><strong><u>contacti amb nosaltres</u></strong></a>.</p>
            
          <p><em>&raquo; Classes particulars de refor&ccedil;</em></p>
            <p>Quan un alumne t&eacute; dificultats per a comprendre i assimilar una mat&egrave;ria, la  millor soluci&oacute; s&oacute;n les classes particulars, la regularitat assegura unes  millors notes de forma habitual.<br />
Amb un ritme de 2 a  3 hores de classes particulars per setmana, els seus fills aprendran a  treballar de forma m&eacute;s regular.</p>
            <p>Durant les classes, el professor particular:</p>
          <ul>
              <li>ajudar&agrave; a       l&rsquo;alumnat en el seu proc&eacute;s d&rsquo;aprenentatge de la mat&egrave;ria</li>
              <li>insistir&agrave; en       els conceptes que impedeixen a l&rsquo;alumne avan&ccedil;ar al ritme de la classe</li>
              <li>millorar&agrave; la motivaci&oacute; de l&rsquo;alumne que dubta de les seves capacitats<strong> </strong></li>
            </ul>
                <? break;	
				
				case "bachillerato": ?>
	    <h3><strong>BATXILLERAT</strong></h3>
            <img title="" src="../img/clases_bachillerato.jpg" class="img_der" alt="" />
        <p>Per a l&rsquo;alumne el pas de la ESO al Batxillerat &eacute;s un moment delicat. Les  nostres classes particulars li facilitaran l&rsquo;adaptaci&oacute; per a aquelles mat&egrave;ries  en les que el salt de nivell l&rsquo;impedeixi seguir &nbsp;el ritme de la classe.</p>
          <p><em>&raquo; Classes particulars adaptades a las necessitats de l&rsquo;alumne.</em></p>
          <p>A <strong>PROFES &amp; CLASES</strong>, el nostres  professors particulars adaptar&agrave;n el ritme de les seves classes a les  dificultats del seu alumnat.</p>
          <p><em>&raquo; Classes particulars de refor&ccedil; d&rsquo;una o diverses assignatures</em></p>
          <p>El professor particular realitzar&agrave; un diagn&ograve;stic de la situaci&oacute; inicial de  l&rsquo;alumne, de les seves car&egrave;ncies i tamb&eacute; analitzar&agrave; les notes obtingudes i els  comentaris dels seus professors.</p>
          <p>Despr&eacute;s de la primera classe, i un cop realitzada l&rsquo;avaluaci&oacute;, dissenyarem  un pla de suport, amb objectius clars i determinant la duraci&oacute; i el ritme  setmanal de classes.</p>
          <p>El perfil del professor sempre ser&agrave; adequat a les necessitats de l&rsquo;alumne,  la titulaci&oacute; ser&agrave; segons la mat&egrave;ria a treballar.<br />
            Llicenciats en matem&agrave;tiques, en f&iacute;sica, en qu&iacute;mica, enginyers... per a  classes particulars de ci&egrave;ncies. Llicenciats en filologia catalana o hisp&agrave;nica,  en filosofia, en hist&ograve;ria... per a classes particulars de lletres.</p>
<p><em>&raquo; Classes particulars de metodologia d&rsquo;estudi</em></p>
          <p>Al Batxillerat, tenir una t&egrave;cnica d&rsquo;estudi adequada &eacute;s fonamental per a que  l&rsquo;alumne aconsegueixi assimilar tot el que els seus professors li exigeixen.  Les classes particulars li serviran per a dominar les t&egrave;cniques d&rsquo;aprenentatge  i de memoritzaci&oacute;: anticipar el treball de rep&agrave;s, fer esquemes i planificar  cada hora d&rsquo;estudi per a optimitzar-la al m&agrave;xim.</p>
          <p><em>&raquo; Preparaci&oacute; de ex&agrave;mens</em></p>
          <p>&Eacute;s convenient que des de 1r de Batxillerat l&rsquo;alumne aprengui a establir una  estrat&egrave;gia apropiada per a preparar els seus ex&agrave;mens. Amb un professor  particular qualificat, aconseguirem que l&rsquo;alumne s&agrave;piga preparar els seus  ex&agrave;mens i, a m&eacute;s, el dia de l&rsquo;examen s&agrave;piga controlar el seu estr&egrave;s, organitzar  el temps d&rsquo;examen i, si es tracta d&rsquo;un examen oral, conegui les pautes a seguir.</p>
          <p>El professor particular far&agrave; treballar a l&rsquo;alumne amb exercicis tipus en  les mateixes condicions de l&rsquo;examen. La correcci&oacute; de l&rsquo;examen permetr&agrave; destacar  els punts forts i les car&egrave;ncies de l&rsquo;alumne.</p>
          <p>El ritme recomanat &eacute;s de 2 a  4 hores de refor&ccedil; per setmana durant un trimestre com a m&iacute;nim.</p>
<p><em>&raquo; Preparaci&oacute; de la selectivitat</em></p>
          <p>La selectivitat, &eacute;s un moment crucial en l&rsquo;escolaritat d&rsquo;un alumne: part  del seu futur es decideix en pocs dies. No es tracta d&rsquo;aprovar l&rsquo;examen, sin&oacute;  d&rsquo;obtenir la nota mitja necess&agrave;ria per a accedir a la carrera universit&agrave;ria  escullida.</p>
          <p>Recomanem un treball continu durant tot el segon curs de Batxillerat. Per a  alumnes que vulguin assegurar millors resultats, podem dissenyar un curs  espec&iacute;fic de preparaci&oacute; de la selectivitat. Amb un o diversos professors, prepararem  les mat&egrave;ries m&eacute;s importants per a l&rsquo;alumne segons la modalitat de Batxillerat  que estigui cursant (Art&iacute;stic, Social, Cient&iacute;fic, Sanitari).</p>
          <p>El nostre professorat, preparador de selectivitat &eacute;s:</p>
          <ul>
            <li>titulat i       qualificat en cada mat&egrave;ria</li>
            <li>experimentats       en preparaci&oacute; de selectivitat en cursos anteriors</li>
          </ul>
                <? break;
				
				case "universitario": ?>
        <h3><strong>UNIVERSITARIS</strong></h3>
            <img title="" src="../img/clases_universitario.jpg" class="img_der" alt="" />
        <p>El primer any a la Universitat pot generar confusi&oacute; entre els estudiants. Els  resultats en els primers ex&agrave;mens parcials indiquen les dificultats amb les que  es troben els estudiants a l&rsquo;inici de la seva carrera.</p>
        <p>Les classes particulars realitzades per un professor particular de nivell  superior permetran a l&rsquo;estudiant assimilar molts m&eacute;s coneixements que la que  s&rsquo;exigeixen a l&rsquo;institut.</p>            
        <p><em>&raquo; Classes particulars de refor&ccedil; d&rsquo;una mat&egrave;ria</em></p>
          <p>Tenim professors qualificats per a impartir:</p>
          <ul>
            <li>Classes particulars de matem&agrave;tiques </li>
            <li>Classes particulars d&rsquo;estad&iacute;stica </li>
            <li>Classes particulars d&rsquo;economia, econometria, comptabilitat </li>
            <li>Classes particulars de dret </li>
            <li>Classes particulars de mec&agrave;nica, electr&ograve;nica, electrot&egrave;cnia </li>
            <li>Classes particulars de filosofia, l&ograve;gica </li>
          </ul>
          <p>Per a altres mat&egrave;ries ens pots fer arribar el temari i t&rsquo;assignarem un  professor experimentat en impartir classes d&rsquo;aquesta mat&egrave;ria a nivell  universitari.</p>
          <p>Per a nivells des de 1r de carrera fins a postgraus.</p>
<p><em>&raquo; Cursos intensius de preparaci&oacute; de ex&agrave;mens universitaris</em></p>
            <p>Per a ajudar a l&rsquo;alumne a preparar els seus ex&agrave;mens, proposem cursos dissenyats  espec&iacute;ficament per cada mat&egrave;ria, segons el nivell de l&rsquo;alumne. Amb un ritme de  3 o 4 classes de 2 hores per setmana durant 2 o 3 setmanes consecutives abans  de l&rsquo;examen.</p>                
                <? break;	
			}?>
            <p>Per &nbsp;a definir amb les nostres  pedagogues el ritme &ograve;ptim de les seves classes particulars <a href="contacta.php"><strong><u>contacti amb nosaltres</u></strong></a>.</p> 
        </div>
<?php include("../includes/3piecera.php");?>