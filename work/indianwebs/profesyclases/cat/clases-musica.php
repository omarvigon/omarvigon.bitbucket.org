<?php include("../includes/1cabecera.php");?>
    <title>CLASES DE MUSICA PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="CLASES DE MUSICA PROFES Y CLASES BARCELONA" />
    <meta name="description" content="Clases de musica. Clases particulares de musica a domicilio. PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases de musica, clases particulares de musica,clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("../includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2><strong>CLASSES DE M&Uacute;SICA</strong></h2>
            <img title="" src="../img/clases_musica.jpg" class="img_der" alt="" />
        <h3><strong>&iquest;Necessita un professor particular de m&uacute;sica a domicili?</strong></h3>
		  <p>Tenim un equip de professors amb formaci&oacute; en conservatori i m&uacute;sics  professionals desitjosos de compartir i transmetre la seva passi&oacute; per la  m&uacute;sica.</p>
          <ul>
            <li>Classes de solfeig i llenguatge musical</li>
            <li>Classes particulars de guitarra cl&agrave;ssica i el&egrave;ctrica</li>
            <li>Classes particulars de piano</li>
            <li>Classes particulars de viol&iacute;, violoncel, sax&ograve;fon</li>
          </ul>
          <p>&nbsp;</p>
            <img title="" src="../img/clases_musica2.jpg" class="img_izq" alt="" />
        <p>&nbsp;</p>
            <p>Per oci o per complementar una formaci&oacute; musical reglada, les nostres  classes particulars de m&uacute;sica, ofereixen la flexibilitat i personalitzaci&oacute; que  garanteix a cada alumne aconseguir els seus objectius.</p>
        </div>
<?php include("../includes/3piecera.php");?>