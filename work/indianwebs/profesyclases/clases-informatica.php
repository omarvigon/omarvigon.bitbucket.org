<?php include("includes/1cabecera.php");?>
    <title>CLASES DE INFORMATICA PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="CLASES DE INFORMATICA PROFES Y CLASES BARCELONA" />
    <meta name="description" content="Clases de informatica. Clases particulares de informatica. Clases de ofimatica, clases de diseño por ordenador. PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases informatica, clases ofimatica,clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2>CLASES DE INFORM&Aacute;TICA</h2>  
            <img title="" src="img/clases_informatica.jpg" class="img_der" alt="" />  
			<p>Dominar las herramientas  inform&aacute;ticas se ha convertido hoy en una necesidad b&aacute;sica, tan importante como  saber leer y escribir.</p>
 	 		<p>Cualesquiera que sean sus  necesidades, le ayudaremos a comprender y dominar los programas inform&aacute;ticos, desde los m&aacute;s b&aacute;sicos, hasta los m&aacute;s complejos y sofisticados.</p>
  			<p>En <b>PROFES&amp;CLASES</b>, tenemos un equipo de especialistas inform&aacute;ticos con dotes pedag&oacute;gicos capaces  de poner al alcance de cualquier persona la mayor&iacute;a de las aplicaciones  inform&aacute;ticas disponibles en el mercado.</p>
			
            <h3>CLASES PARTICULARES DE INFORM&Aacute;TICA PARA  PRINCIPIANTES</h3>
			<p>Clases  particulares de iniciaci&oacute;n a la inform&aacute;tica, para los que no tuvieron ocasi&oacute;n  de acceder a la inform&aacute;tica antes, y que no quieren renunciar a todas sus  ventajas: poder navegar por Internet, enviar correos electr&oacute;nicos, crear sus  discos de m&uacute;sica, gestionar sus fotograf&iacute;as, llevar las cuentas domesticas,  etc&hellip;</p>
	
    		<img title="" src="img/clases_empresas.jpg" class="img_der" alt="" />
			<h3>CLASES PARTICULARES DE OFIM&Aacute;TICA</h3>
			<p>Para ser  m&aacute;s eficiente en la oficina, hoy es imprescindible dominar los programas de ofim&aacute;tica m&aacute;s comunes. Para mejorar su nivel, le proponemos clases de Excel, Word, PowerPoint, Access etc&hellip;</p>

			<h3>CLASES PARTICULARES DE INFORM&Aacute;TICA DE DISE&Ntilde;O </h3>
			<ul>
			<li>Clases  particulares para programas como Photoshop, Freehand, Dreamweaver,  etc&hellip;</li>
			<li>Clases particulares para programas  de dibujo asistido por ordenador, como Autocad 2D y 3D</li>
			</ul>
			<h3>CLASES PARTICULARES DE  INFORM&Aacute;TICA PARA LOS M&Aacute;S AVANZADOS</h3>
			<p>Necesita por motivos  profesionales o de estudios avanzados, dominar herramientas inform&aacute;ticas m&aacute;s  sofisticadas, contacte con nosotros, tenemos profesores especialistas en&nbsp;lenguajes de programaci&oacute;n (Java, C, C++,  Visual Basic, .Net, etc&hellip;)</p>
        </div>
<?php include("includes/3piecera.php");?>