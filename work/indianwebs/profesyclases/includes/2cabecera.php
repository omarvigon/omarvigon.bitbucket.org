    <meta name="revisit-after" content="7 Days" />
    <meta name="revisit" content="7 days" />
    <meta name="robot" content="Index,Follow" />
    <meta name="robots" content="All" />
    <meta name="distribution" content="Global" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="ratinh" content="general" />
    <meta name="language" content="ES" />
    <meta name="abstract" content="PROFESORES Y CLASES BARCELONA" />
    <meta name="subject" content="PROFESORES Y CLASES BARCELONA" />
    <meta name="author" content="PROFESORES Y CLASES BARCELONA" />
    <meta name="copyright" content="PROFESORES Y CLASES BARCELONA" />
    <link rel="stylesheet" type="text/css" href="http://www.profesyclases.com/includes/_estilos.css" />
    <script language="javascript" type="text/javascript" src="http://www.profesyclases.com/includes/_funciones.js"></script>
    <script> function utmx_section(){}function utmx(){} (function(){var k='2046366591',d=document,l=d.location,c=d.cookie;function f(n){ if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return c.substring(i+n. length+1,j<0?c.length:j)}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash; d.write('<sc'+'ript src="'+ 'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com' +'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime=' +new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+ '" type="text/javascript" charset="utf-8"></sc'+'ript>')})(); </script>
	  <script>utmx("url",'A/B');</script>
</head>

<body>
<div id="central">
	<div id="top">
    	  <a title="Profes y Clases" href="index.php"><img title="Profes y Clases" src="http://www.profesyclases.com/img/logo_profes_clases.gif" class="img1" alt="Profes y Clases" /></a>
        <a title="Informacion sobre cursos" href="contacta.php"><img title="Cursos Barcelona" src="http://www.profesyclases.com/img/cabecera2_<?=$idioma;?>.jpg" class="img2" alt="Cursos Barcelona" /></a>
        <div class="top1"><a title="Inicio" href="index.php"><?=$titulo[0];?></a> | <a title="Contacta" href="contacta.php"><?=$titulo[1];?></a> | <a title="Situacion" href="situacion.php"><?=$titulo[2];?></a></div>
        <div class="top2"><a title="Castellano" href="http://www.profesyclases.com"><img title="Castellano" src="http://www.profesyclases.com/img/bandera_es.gif" align="absmiddle" alt="Castellano" /> Castellano</a> <a title="Catala" href="http://www.profesyclases.com/cat/"><img title="Catala" src="http://www.profesyclases.com/img/bandera_cat.gif" align="absmiddle" alt="Catala" /> Catal&agrave;</a></div>
    </div>
    <div id="medio">
    	<div id="botones">
        	<div style="height:110px;background:url(http://www.profesyclases.com/img/boton1-largo.gif) left no-repeat;border-top:1px solid #eeeeee;border-bottom:1px solid #cccccc;">
            	<a title="Escolar" class="bot0" href="refuerzo-escolar.php">ESCOLAR</a>               
            	<a href="refuerzo-escolar.php?pag=primaria" class="sub1">&bull; <?=$titulo[3];?></a>
            	<a href="refuerzo-escolar.php?pag=secundaria" class="sub1">&bull; <?=$titulo[4];?></a>
            	<a href="refuerzo-escolar.php?pag=bachillerato" class="sub1">&bull; <?=$titulo[5];?></a>
            	<a href="refuerzo-escolar.php?pag=universitario" class="sub1">&bull; <?=$titulo[6];?></a>    
            </div>    
            
            <div style="height:75px;background:url(http://www.profesyclases.com/img/boton1-medio.gif) left no-repeat;border-top:1px solid #eeeeee;border-bottom:1px solid #cccccc;">
             	  <a title="Idiomas" class="bot0" href="clases-idiomas.php"><?=$titulo[7];?></a>              
                <a href="clases-idiomas.php?pag=ingles" class="sub1">&bull; <?=$titulo[8];?></a>
		        <a href="clases-idiomas.php?pag=otros" class="sub1">&bull; <?=$titulo[9];?></a>
			</div>
                 
            <a title="Informatica" class="bot1" href="clases-informatica.php"><?=$titulo[10];?></a>
            <a title="Empresas" class="bot1" href="clases-empresas.php"><?=$titulo[11];?></a>
            <a title="Musica" class="bot1" href="clases-musica.php">M&Uacute;SICA</a>
            <br />
            <a title="Profesores" class="bot2" href="empleo.php"><?=$titulo[12];?></a>
            
            <h3 align="center"><em>Agencia Sarri&aacute; - Les Corts</em><br />Tel&eacute;fono (+34) 93.254.00.48</h3>
            <h3 align="center"><em>Agencia Eixample - Centro</em><br />Tel&eacute;fono (+34) 93.323.90.73</h3>    
        </div>