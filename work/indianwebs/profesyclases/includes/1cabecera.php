<?php 
if(strstr($_SERVER['PATH_INFO'],"/cat"))
{
	$idioma="cat";
	$titulo[0]="Inici";
	$titulo[1]="Contacti";
	$titulo[2]="On Som";
	$titulo[3]="Prim&agrave;ria";
	$titulo[4]="Secund&agrave;ria";
	$titulo[5]="Batxillerat";
	$titulo[6]="Universitari";
	$titulo[7]="IDIOMES";
	$titulo[8]="Classes particulars d'angl&egrave;s";
	$titulo[9]="Classes particulars altres idiomes";
	$titulo[10]="INFORM&Agrave;TICA";
	$titulo[11]="EMPRESES";
	$titulo[12]="TREBALLA - PROFESSORS";
}
else
{
	$idioma="es";
	$titulo[0]="Inicio";
	$titulo[1]="Contacto";
	$titulo[2]="Situaci&oacute;n";
	$titulo[3]="Primaria";
	$titulo[4]="Secundaria";
	$titulo[5]="Bachillerato";
	$titulo[6]="Universitario";
	$titulo[7]="IDIOMAS";
	$titulo[8]="Clases particulares de ingl&eacute;s";
	$titulo[9]="Clases particulares otros idiomas";
	$titulo[10]="INFORM&Aacute;TICA";
	$titulo[11]="EMPRESAS";
	$titulo[12]="EMPLEO - PROFESORES";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=$idioma;?>">
<head>
	<meta name="verify-v1" content="HEv0wIRZJbs2++5HMaIOb3c4UD95dhjO0XiCPIfvIiM=" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />