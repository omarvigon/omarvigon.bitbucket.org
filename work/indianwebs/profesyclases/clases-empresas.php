<?php include("includes/1cabecera.php");?>
    <title>FORMACION A EMPRESAS PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="FORMACION A EMPRESAS PROFES Y CLASES BARCELONA" />
    <meta name="description" content="PROFES y CLASES ofrece formacion dirigida a empresas para  mejorar la competitividad de sus colaboradores. Clases y cursos personalizados para empresas" />
    <meta name="keywords" content="formacion empresas,cursos empresas,clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("includes/2cabecera.php");?>
        <div id="cuerpo">
            <h2>FORMACI&Oacute;N A EMPRESAS</h2>
            <img title="" src="img/clases_empresas2.jpg" class="img_der" alt="" />
            <p><b>PROFES&amp;CLASES</b> ofrece formaci&oacute;n dirigida a empresas para  mejorar la competitividad de sus colaboradores. En una primera entrevista,  nuestros formadores configurar&aacute;n un <b>plan de estudios adaptado</b>.</p>
            <p>Nuestros docentes, todos profesionales de la formaci&oacute;n son cuidadosamente  seleccionados por su titulaci&oacute;n, experiencia y sus dotes pedag&oacute;gicos.</p>
            <p>Nuestros profesores actualizan constantemente sus conocimientos y su  metodolog&iacute;a para adaptarse a las necesidades de profesionales tanto en <b>clases particulares</b> como en grupos.</p>
            <p>Elija los <b>horarios a su conveniencia</b> para interferir lo menos posible en la actividad de su empresa.</p>
            <h3>NUESTRAS &Aacute;REAS DE FORMACI&Oacute;N EN EMPRESAS</h3>
            <ul>
            <li>Idiomas (Clases de Ingl&eacute;s, Clases de Franc&eacute;s, Clases de  Alem&aacute;n, Clases de Chino, Clases de Italiano, Clases de Ruso,&nbsp; Clases de Japon&eacute;s,...)</li>
            <li>Actualizaci&oacute;n de conocimientos (Clases de Contabilidad, Clases de  Administraci&oacute;n, Clases de Fiscalidad, Clases de Marketing y Ventas ...)</li>
            <li>Ofim&aacute;tica (Word, Excel, Internet, Access, Power Point)</li>
            </ul> 
            <p>Para necesidades espec&iacute;ficas ll&aacute;menos <em>93 254 00 48</em>, en <b>PROFES&amp;CLASES</b> dise&ntilde;aremos un plan a medida y calcularemos un presupuesto ajustado.</p>
            <h3>NUESTRAS F&Oacute;RMULAS DE FORMACI&Oacute;N A EMPRESAS:</h3>
            <ul>
            <li>Clases particulares</li>
            <li>Clases en grupos en sus oficinas</li>
            </ul>
            <p>Para definir con nuestras pedagogas el ritmo &oacute;ptimo de  clases particulares <a href="contacta.php">contacte con nosotros</a></p>
        </div>
<?php include("includes/3piecera.php");?>