<?php include("includes/1cabecera.php");?>
    <title>PROFES Y CLASES BARCELONA</title>
    <meta name="title" content="PROFES Y CLASES BARCELONA" />
    <meta name="description" content="PROFES Y CLASES BARCELONA" />
    <meta name="keywords" content="clases barcelona,profesores barcelona,cursos barcelona" />
<?php include("includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2>REFUERZO ESCOLAR</h2>
            
            <?php switch($_GET['pag'])
			{
				default:
				case "primaria": ?>            
            <h3>PROFES&amp;CLASES PRIMARIA</h3>
            <p><em>&raquo; Ayuda en los deberes - Tutor&iacute;a</em></p>
            <p>Para que sus hijos aprendan sus lecciones de manera adecuada y adquieran buenos  h&aacute;bitos de estudio. Es importante que el alumno consiga unas s&oacute;lidas bases. Con un profesor particular competente y pluri-disciplinar  ayudar&aacute; a sus hijos a mejorar la lectura, la ortograf&iacute;a y la gram&aacute;tica sin  olvidar la geometr&iacute;a y el c&aacute;lculo.</p>
            <img title="" src="img/clases_primaria.jpg" class="img_der" alt="" />
            <p>Las clases particulares de ayuda en los deberes son:</p>            
            <ul>
            <li>Para hacer  los deberes del colegio</li>
            <li>Para repasar las lecciones</li>
            <li>Para aprender a mejorar sus t&eacute;cnicas de estudio</li>
            </ul>
            <p><em>&raquo; Clases particulares de refuerzo</em></p>
            <p>Para que sus hijos se  sientan c&oacute;modos con la geometr&iacute;a y el c&aacute;lculo las clases particulares de  matem&aacute;ticas son la soluci&oacute;n adaptada. Para que tengan unas s&oacute;lidas bases en gram&aacute;tica, ortograf&iacute;a  y lectura elija las clases particulares de lengua. </p>
            <p>Con un profesor particular de refuerzo, sus hijos:</p>
            <ul>
            <li>dominar&aacute;n  y asimilar&aacute;n los conceptos fundamentales</li>
            <li>estar&aacute;n  mejor preparados para la secundaria</li>
            <li>mejorar&aacute;n  su metodolog&iacute;a de estudio</li>
            </ul>
            <p>Nuestros profesores son:</p>
            <ul>
            <li>Titulados</li>
            <li>Generalistas o especialistas con  calidades pedag&oacute;gicas</li>
            <li>Experimentados para entender las  necesidades del alumno</li>
            </ul>
                <? break;
				
				case "secundaria": ?>
            <h3>PROFES&amp;CLASES SECUNDARIA</h3>
            <img title="" src="img/clases_idiomas.jpg" class="img_der" alt="" />
            
            <p><em>&raquo; Clases particulares a medida</em></p>
            <p>En <b>PROFES&amp;CLASES</b>, creemos que la personalizaci&oacute;n de la ense&ntilde;anza, garantiza el &eacute;xito escolar del  alumno. Con un plan de trabajo dise&ntilde;ado en funci&oacute;n de las dificultades del  alumno y de su personalidad. Definiremos unos objetivos que el profesor se encargar&aacute; de desarrollar mientras dure el refuerzo.</p>
            <p>M&aacute;s del 90% de nuestros alumnos consiguen mejorar sus notas  entre 2 y 3 puntos por materia.Proponemos clases particulares en las asignaturas  cient&iacute;ficas: clases de matem&aacute;ticas, clases particulares de f&iacute;sica y qu&iacute;mica,  clases de ciencias naturales&hellip; </p>
            <p>Tambi&eacute;n para las materias de humanidades, clases de lengua y  literatura catalana, clases particulares de lengua y literatura castellana,  clases de historia y geograf&iacute;a, clases particulares de ingl&eacute;s </p>

            <img title="" src="img/clases_secundaria.jpg" class="img_der" alt="" /> 
            <p><em>&raquo; Tutor&iacute;a, Ayuda en los deberes</em></p>
            <p>El seguimiento tutorizado de los deberes es un elemento imprescindible  para el &eacute;xito del alumno en su fase de adaptaci&oacute;n a la E.S.O . Los profesores  exigen m&aacute;s autonom&iacute;a por parte del alumno que en primaria.</p>            
            <p>El profesor particular  le ayudar&aacute;:</p>
            <ul>
            <li>a repasar y comprender sus  lecciones</li>
            <li>a organizar su trabajo</li>
            <li>a tener buenas t&eacute;cnicas de  estudio</li>
            </ul>
            <p>Nuestros docentes son:</p>
            <ul>
            <li>titulados  y cualificados</li>
            <li>experimentados  con alumnos de nivel similar</li>
            <li>seleccionados  por sus dotes pedag&oacute;gicos</li>
            </ul>
            <p>Para definir con nuestras pedagogas el ritmo &oacute;ptimo de clases particulares contacte con nosotros.</p>
            
            <p><em>&raquo; Clases  particulares de refuerzo</em></p>
            <p>Cuando un alumno encuentra dificultades para comprender y  asimilar una materia, la mejor soluci&oacute;n son las clases particulares, la  regularidad asegura unas mejores notas de forma habitual. <br>
            Con un ritmo de 2h a 3h de clases particulares por semana  sus hijos aprender&aacute;n a trabajar de manera m&aacute;s regular.</p>
            <p>Durante las clases, el profesor particular:</p>
            <ul>
            <li>ayudar&aacute; al  alumno en su proceso de aprendizaje de la materia</li>
            <li>insistir&aacute; en los conceptos que impiden al alumno avanzar al ritmo de la clase</li>
            <li>mejorar&aacute; la motivaci&oacute;n del alumno que duda de sus capacidades</li>
            </ul>
                <? break;	
				
				case "bachillerato": ?>
			<h3>BACHILLERATO</h3>
            <img title="" src="img/clases_bachillerato.jpg" class="img_der" alt="" />
            <p>Para el alumno el paso de la E.S.O. al bachillerato es un momento delicado,  nuestras clases particulares facilitar&aacute;n la adaptaci&oacute;n del alumno para aquellas  materias en las que el salto de nivel impida al alumno seguir el ritmo de la  clase.</p>
            <p><em>&raquo; Clases particulares adaptadas a las  necesidades del alumno.</em></p>
            <p>En <b>PROFES&amp;CLASES</b>, nuestros profesores particulares  adaptar&aacute;n el ritmo de sus clases a las dificultades del alumno.</p>
            <p><em>-Clases  particulares de refuerzo de una o varias asignaturas</em></p>
            <p>El profesor  particular realizar&aacute; un diagn&oacute;stico de la situaci&oacute;n del alumno, de sus lagunas  y tambi&eacute;n analizar&aacute; las notas obtenidas y los comentarios de sus profesores.</p>
            <p>Despu&eacute;s de la primera clase, y una vez realizada la  evaluaci&oacute;n, dise&ntilde;aremos un plan de apoyo, con objetivos claros, la duraci&oacute;n y  el ritmo semanal de clases.</p>
            <p>El perfil del profesor siempre ser&aacute; adecuado a las  necesidades del alumno, la titulaci&oacute;n ser&aacute; acorde con la materia a trabajar.  Licenciados en matem&aacute;ticas, en f&iacute;sica, en qu&iacute;mica, ingenieros&hellip; para clases  particulares de ciencias. Licenciados en filolog&iacute;a catalana o castellana, en  filosof&iacute;a, en historia&hellip; para clases particulares de letras.</p>
            <p><em>&raquo; Clases  particulares de metodolog&iacute;a de estudio</em></p>
            <p>En bachillerato, poseer una t&eacute;cnica de estudio adecuada es  fundamental para que el alumno consiga asimilar todo lo que sus profesores le  exigen. Las clases particulares le servir&aacute;n para dominar las  t&eacute;cnicas de aprendizaje y de memorizaci&oacute;n: anticipar el trabajo de repaso,  hacer esquemas planificar cada hora de estudio para sacarle el mejor partido.</p>
            <p><em>&raquo; Preparaci&oacute;n de  ex&aacute;menes</em></p>
            <p>Es conveniente que desde 1&ordm; de Bachillerato el alumno  aprenda a establecer una estrategia apropiada para preparar sus ex&aacute;menes. Con  un profesor particular cualificado, conseguimos que el alumno sepa preparar sus  ex&aacute;menes, y adem&aacute;s llegado el d&iacute;a del examen sepa gestionar su estr&eacute;s,  organizar el tiempo de examen, y si se trata de examen oral conozca las pautas  a seguir.</p>
            <p>El profesor particular har&aacute; trabajar el alumno con  ejercicios tipo en las mismas condiciones del examen. La correcci&oacute;n del examen  permitir&aacute; destacar los puntos fuertes y las carencias del alumno.</p>
            <p>El ritmo recomendado es de 2 a 4 horas de refuerzo por  semana durante un trimestre m&iacute;nimo.</p>
            <p><em>&raquo; Preparaci&oacute;n de la selectividad</em></p>
            <p>La selectividad, es un momento crucial en la escolaridad de  un alumno, parte de su futuro se decide en pocos d&iacute;as. No se trata de aprobar  el examen, sino de obtener la nota media necesaria para acceder a la carrera  universitaria que elija.</p>
            <p>Recomendamos un trabajo continuo durante todo el segundo  curso de bachillerato. Para los alumnos que quieran asegurar mejores resultados,  podemos dise&ntilde;ar un curso espec&iacute;fico de preparaci&oacute;n de la selectividad. Con uno  o varios profesores, prepararemos las materias m&aacute;s importantes para el alumno  seg&uacute;n el tipo de bachillerato cursado (Art&iacute;stico, Social, Cient&iacute;fico). </p>
            <p>Nuestros profesores, preparadores de selectividad son:</p>
            <ul>
            <li>titulados  y cualificados en cada materia</li>
            <li>experimentados en preparaci&oacute;n de  selectividad en cursos anteriores</li>
            </ul>
                <? break;
				
				case "universitario": ?>
            <h3>UNIVERSITARIOS</h3>
            <img title="" src="img/clases_universitario.jpg" class="img_der" alt="" />
            <p>El primer a&ntilde;o de universidad suele generar desconcierto  entre los estudiantes. Los resultados en los primeros ex&aacute;menes parciales indican  las dificultades con las que se encuentran los estudiantes en el inicio de su  carrera.</p>
            <p>Las clases particulares realizadas por un profesor  particular de nivel superior permitir&aacute;n al estudiante asimilar una cantidad de  conocimientos mucho mayor que la que se  exige en el instituto.</p>            
            <p><em>&raquo; Clases  particulares de refuerzo de una materia</em></p>
            <p>Tenemos profesores cualificados para impartir:</p>
            <ul>
            <li>Clases  particulares de matem&aacute;ticas</li>
            <li>Clases  particulares de estad&iacute;stica</li>
            <li>Clases  particulares de econom&iacute;a, econometria, contabilidad</li>
            <li>Clases  particulares de derecho</li>
            <li>Clases  particulares de mec&aacute;nica, electr&oacute;nica, electrotecnia</li>
            <li>Clases  particulares de filosof&iacute;a, l&oacute;gica</li>
            </ul>
            <p>Para otras asignaturas tr&aacute;enos el temario y asignaremos un  profesor experimentado en impartir clases en la materia a nivel universitario.</p>
            <p>Para niveles desde 1&ordm; de carrera hasta postgrados.</p>
            <p><em>&raquo; Cursos intensivos de preparaci&oacute;n de ex&aacute;menes universitarios</em></p>
            <p>Para ayudar al alumno a preparar sus ex&aacute;menes, proponemos  cursos dise&ntilde;ados espec&iacute;ficamente para cada materia, seg&uacute;n el nivel del alumno. Con un ritmo de 3 o 4 clases de 2h por semanas durante 2 o 3  semanas consecutivas antes del examen.</p>                
                <? break;	
			}?>
            <p>Para definir con nuestras pedagogas el ritmo &oacute;ptimo de  clases particulares <a href="contacta.php">contacte con nosotros</a>.</p> 
        </div>
<?php include("includes/3piecera.php");?>