<?php include("includes/1cabecera.php");?>
    <title>Clases particulares y profesores a domicilio en Barcelona</title>
    <meta name="title" content="PROFES Y CLASES BARCELONA clases particulares y profesores a domicilio" />
    <meta name="description" content="PROFES Y CLASES BARCELONA Para cualquier necesidad educativa a medida en la provincia de Barcelona, le proponemos un servicio dedicado en exclusiva al exito. Clases Particulares y profesores a domicilio. Clases de idiomas, clases de informatica, clases para empresas" />
    <meta name="keywords" content="clases barcelona,profesores barcelona,cursos barcelona,clases idiomas,clases informatica,clases para empresas" />
<?php include("includes/2cabecera.php");?>
        <div id="cuerpo">
        	<h2>La  ense&ntilde;anza a medida siempre es la soluci&oacute;n adecuada</h2>
			<img title="" src="img/profes_clases.jpg" style="float:right;margin:0 0 5px 5px;" alt="" />
            <h3>Somos  especialistas en clases particulares a domicilio para refuerzo escolar,  idiomas, inform&aacute;tica, ocio y formaci&oacute;n para adultos.</h3>
			<h3>Para  cualquier necesidad educativa a medida en la provincia de Barcelona, le proponemos  un servicio dedicado en exclusiva al &eacute;xito.</h3>
  			<p>Clases particulares dise&ntilde;adas para adaptarse a las  necesidades del alumno y a su disponibilidad horaria.</p>
  			<p>Nuestros compromisos:</p>
            <ul>
            <li>Poner a disposici&oacute;n de nuestros alumnos un equipo  docente cualificado y experimentado</li>
            <li>Garantizar un servicio de seguimiento pedag&oacute;gico &oacute;ptimo</li>
            <li>Ofrecer las tarifas m&aacute;s competitivas</li>
            <li>Adaptarnos a las necesidades y gustos de nuestros  alumnos</li>
            <li>Libre elecci&oacute;n de horarios</li>
            <li>Posibilidad de cambiar de profesor si lo desea</li>
            </ul>
            <p>Solicitar  los servicios de <b>PROFES &amp; CLASES</b> a  domicilio, es la garant&iacute;a de ahorrar tiempo y dinero asegurando el &eacute;xito de  cada alumno.</p>
        </div>
<?php include("includes/3piecera.php");?>