<?php include("includes/0cabecera.php");?>
	<title>Mapa del sitio Rincones de Europa</title>
	<meta name="description" content="Mapa del sitio de Rincones de Europa" />
	<meta name="keywords" content="Rincones de Europa, rincon europa, viajes europa, rutas europa, destinos europa, viaje europa" />
<?php include("includes/1cabecera.php");?>

        <div class="cuerpo">
        	<h1>Mapa del sitio</h1>
            <h2>Rincones de Europa</h2>
            <p><img title="Viajar por Europa" align="right" hspace="16" src="http://www.rinconesdeeuropa.com/img/postales_europa4.jpg" alt="Viajar por Europa"/></p>
            <dl>
                <dt><a title="Quienes somos" href="http://www.rinconesdeeuropa.com/quienes-somos.php">quienes somos</a></dt>
                <dt>Viajes</dt>
                <?php 
				$listap=str_replace("<a","<dd> - <a",$textop);
				$listap=str_replace("class='pais'","",$listap);
				$listap=str_replace("'>","'>Viajes a ",$listap);
				$listap=str_replace("</a>","</a></dd>",$listap);
				echo $listap;
				?>
            	<dt><a title="Rutas" href="http://www.rinconesdeeuropa.com/rutas.php">rutas</a></dt>
                <?php
				$rutas=ejecutar("select id,titulo from rutas where visible='1' order by titulo asc");
				while($fila=mysql_fetch_assoc($rutas))
				{
					$textor.="<dd>  - <a title='Rutas por ".$fila["titulo"]."' href='http://www.rinconesdeeuropa.com/rutas.php?id=".$fila["id"]."'>&raquo; ".$fila["titulo"]."</a></dd>";
				}
				echo $textor;	
				?>
        		<dt><a title="Condiciones" href="http://www.rinconesdeeuropa.com/condiciones.php">condiciones</a></dt>
      			<dt><a title="Mapa del sitio" href="http://www.rinconesdeeuropa.com/mapa-sitio.php">mapa del sitio</a></dt>
            	<dt><a title="Contacto" href="http://www.rinconesdeeuropa.com/contacto.php">contacto</a></dt>
                	<dd> - <a href="http://www.rinconesdeeuropa.com/reservas.php?tipo=coche">Reserva de Coches</a></dd>
                    <dd> - <a href="http://www.rinconesdeeuropa.com/reservas.php?tipo=vuelo">Reserva de Vuelos</a></dd>
                    <dd> - <a href="http://www.rinconesdeeuropa.com/reservas.php?tipo=hotel">Reserva de Hoteles</a></dd>
            </dl>
        </div>

<?php include("includes/2piecera.php");?>