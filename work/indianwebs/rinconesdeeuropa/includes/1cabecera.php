	<meta name="language" content="es" />
	<link rel="stylesheet" type="text/css" href="http://www.rinconesdeeuropa.com/includes/_estilos.css" />
	<script language="javascript" type="text/javascript" src="http://www.rinconesdeeuropa.com/includes/_funciones.js"></script>
</head>

<body>
<div id="central">
	<div class="izq">
    	<a title="Rincones de Europa" href="http://www.rinconesdeeuropa.com/"><img src="http://www.rinconesdeeuropa.com/img/rincones_europa.gif" alt="Rincones de Europa" /></a>
        <div><?=$textop;?></div>
        
        <img class="imgborde" src="img/fondo_caja.gif" alt="" />
        <div align="center">
        	<a title="Rutas por Europa" href="http://www.rinconesdeeuropa.com/rutas.php"><img src="http://www.rinconesdeeuropa.com/img/boton_rutas.gif" class="imgreserva1" alt="Rutas por Europa" /></a>
            <a title="Reserva tu vuelo" href="http://www.rinconesdeeuropa.com/reservas.php?tipo=vuelo"><img src="http://www.rinconesdeeuropa.com/img/boton_reserva_avion.gif" class="imgreserva1" alt="Reserva tu vuelo" /></a>
            <a title="Reserva tu hotel" href="http://www.rinconesdeeuropa.com/reservas.php?tipo=hotel"><img src="http://www.rinconesdeeuropa.com/img/boton_reserva_hotel.gif" class="imgreserva1" alt="Reserva tu hotel" /></a>
            <a title="Reserva tu coche" href="http://www.rinconesdeeuropa.com/reservas.php?tipo=coche"><img src="http://www.rinconesdeeuropa.com/img/boton_reserva_coche.gif" class="imgreserva1" alt="Reserva tu coche" /></a>
            <br />
        </div>
        
        <p align="center"><br />
        <a title="Dise&ntilde;o Web" href="http://www.indianwebs.com/" target="_blank">Dise&ntilde;o web Barcelona</a></p>
    </div>
    <div class="der">
    	<div class="baner">
        	<a title="Rutas por Europa" href="http://www.rinconesdeeuropa.com/rutas.php"><img src="http://www.rinconesdeeuropa.com/img/boton_rutas2.gif" alt="Rutas por Europa" /></a>
            <a title="Reservar vuelo" href="http://www.rinconesdeeuropa.com/reservas.php?tipo=vuelo"><img src="img/boton_reserva_avion2.gif" alt="Reservar vuelo" /></a>
            <a title="Reservar hotel" href="http://www.rinconesdeeuropa.com/reservas.php?tipo=hotel"><img src="img/boton_reserva_hotel2.gif" alt="Reservar hotel" /></a>
        	<a title="Reservar coche" href="http://www.rinconesdeeuropa.com/reservas.php?tipo=coche"><img src="img/boton_reserva_coche2.gif" alt="Reservar coche" /></a>            
        </div>
    	<div class="boton">
        	<a title="Quienes somos" href="http://www.rinconesdeeuropa.com/quienes-somos.php">quienes somos</a>
            <a title="Rutas por Europa" href="http://www.rinconesdeeuropa.com/rutas.php">rutas</a>
        	<a title="Condiciones" href="http://www.rinconesdeeuropa.com/condiciones.php">condiciones</a>
      		<a title="Mapa del Sitio" href="http://www.rinconesdeeuropa.com/mapa-sitio.php">mapa del sitio</a>
            <a title="Contacto" href="http://www.rinconesdeeuropa.com/contacto.php">contacto</a>
        </div>