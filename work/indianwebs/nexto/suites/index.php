<?php include("../includes/1cabecera.php");?>
	<title>Suites NextTo Barcelona </title>
	<meta name="description" content="Hotel Barcelona Suites Barcelona NextTo" />
	<meta name="keywords" content="Hotel Barcelona Suites Barcelona NextTo" />
<?php include("../includes/2cabecera.php");?>

	<div class="cuerpo_suites">
    	<div id="columna1">
        <h3>Nextto Picasso</h3>
        <!--
        <p><b>Addres:</b> c\calle 123 08001 Barcelona<br /><b>Telephone</b>: (+34) 933.111.333<br /><b>Capacity:</b> 1-2</p>
        -->
        <p><img title="Next to Suites" src="<?=$ruta?>img/suites1.jpg" alt="Suites Nextto" /></p>
		<ul>
		<li>Double Room with double bed and room with bunk beds for 2 people.</li>
		<li>Main Hall-furnished dining area and resting area with sofa, table and  TV. Area is transformed into sleeping at night thanks to two single  beds available. With large windows, has natural light all day dining  for 6 people.</li>
		<li>Additional sofa bed -2 are in the entry.</li>
		<li>Fully equipped kitchen with refrigerator, microwave, coffee maker,  juice squeezer, toaster. You also have at your disposal cutlery,  crockery and cooking utensils.</li>
		<li>Room bathroom with shower hairdryer is provided.</li>
		<li>4a. 2nd floor. No elevator.</li>
		</ul>
        <p><iframe width="250" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=C%2F+Tuset+32+barcelona&amp;sll=41.398043,2.152612&amp;sspn=0.003984,0.006909&amp;ie=UTF8&amp;ll=41.397479,2.150917&amp;spn=0.009336,0.012445&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe></p>
        </div>
        
        <div id="columna2">
		<h3>Nextto Pedrera</h3>
        <!--
        <p><b>Addres:</b> c\calle 123 08001 Barcelona<br /><b>Telephone</b>: (+34) 933.111.333<br /><b>Capacity:</b> 1-2</p>
        -->
        <p><img title="Next to Suites" src="<?=$ruta?>img/suites2.jpg" alt="Suites Nextto" /></p>
		<ul>
		<li>In this study, the sleeping area includes a double bed, along the side  of the closet in this same space encontramo two sofa beds.</li>
		<li>In the study we found the dining area with plasma screen TV and stereo</li>
		<li>The newly-renovated kitchen are a breakfast bar, refrigerator, kitchen utensils and microwave oven.</li>
		<li>The bathroom is large with shower and toilet</li>
		<li>At 20m from the terrace you can enjoy a formidable sight.</li>
		<li>Other services</li>
		<li>Air conditioning / heating.</li>
		<li>Bedding and bath.</li>
		</ul>
        <p><iframe width="250" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?&amp;hl=es&amp;geocode=&amp;q=C%2F+Tuset+32+barcelona&amp;sll=41.398043,2.152612&amp;sspn=0.003984,0.006909&amp;ie=UTF8&amp;ll=41.397479,2.150917&amp;spn=0.009336,0.012445&amp;z=15&amp;output=embed"></iframe></p>
        </div>
        
        <div id="columna3">
		<h3>Nextto Ramblas</h3>        
        <!--
        <p><b>Addres:</b> c\calle 123 08001 Barcelona<br /><b>Telephone</b>: (+34) 933.111.333<br /><b>Capacity:</b> 1-2</p>
        -->
        <p><img title="Next to Suites" src="<?=$ruta?>img/suites3.jpg" alt="Suites Nextto" /></p>
		<ul>
		<li>1 Bedroom with 2 single beds</li>
		<li>2 In the living room sofa bed</li>
		<li>The sa&ocirc;ne consists of 2 single sofa bed, a TV, CD player and a dining room for 4 people.</li>
		<li>The American-style kitchen is fully equipped: fridge, ceramic hob, utensils, microwave, toaster, kettle, coffee maker.</li>
		<li>This apartment has two bathrooms each with shower and toilet, a sink / toilet separated.</li>
		<li>This loclaizado in a 1st floor in a building with no elevator.</li>
		<li>Other services</li>
		<li>Air conditioning and heating.</li>
		<li>Bedding and bath.</li>
		</ul>
        <p><iframe width="250" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=C%2F+Tuset+32+barcelona&amp;sll=41.398043,2.152612&amp;sspn=0.003984,0.006909&amp;ie=UTF8&amp;ll=41.397479,2.150917&amp;spn=0.009336,0.012445&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe></p>
        </div>
	</div>
    
<?php include("../includes/3piecera.php");?>