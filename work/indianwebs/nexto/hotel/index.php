<?php include("../includes/1cabecera.php");?>
	<title>Hotel NextTo Barcelona</title>
	<meta name="description" content="Hotel Barcelona Suites Barcelona NextTo" />
	<meta name="keywords" content="Hotel Barcelona Suites Barcelona NextTo" />
<?php include("../includes/2cabecera.php");?>

    <div class="cuerpo_hotel" style="min-height:560px;">
    	<div class="col_1">
        	<h3>Location</h3>
            <p>At the heart of <b>Barcelona</b>, in the modernist Eixample district and  business center, we find <b>HOTEL</b> <em>NEXT TO</em>, between the streets of Paseo de Gracia and Pau Claris. A few meters from the Avenida Diagonal. In terms of communications, has all the potential of a big city.</p>
            <p><iframe width="370" height="330" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?source=s_q&amp;f=q&amp;hl=es&amp;geocode=&amp;q=C%2F+Rossell%C3%B3+242+Barcelona&amp;sll=41.395999,2.154436&amp;sspn=0.015936,0.027637&amp;ie=UTF8&amp;ll=41.398896,2.161217&amp;spn=0.011267,0.01502&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe></p>
        </div>
        <div class="col_2">
        	<h3>Description</h3>
            <p>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona1.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona1pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona2.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona2pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona3.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona3pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona4.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona4pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona5.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona5pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona6.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona6pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona7.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona7pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona8.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona8pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona9.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona9pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona10.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona10pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona11.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona11pic.jpg" alt="Hotel Next to Barcelona" /></a>
            <a title="Hotel Nextto" rel="shadowbox[Hotel]" href="<?=$ruta;?>img/hotel-nextto-barcelona12.jpg"><img src="<?=$ruta;?>img/hotel-nextto-barcelona12pic.jpg" alt="Hotel Next to Barcelona" /></a>
            </p>          
			<p><b>HOTEL</b> <em>NEXT TO</em>, born as a new generation hotel in the most prestigious area of  Barcelona where mixed functionality, location, distinction and comfort  inaugurated on July 1, 2008. Just a few meters from the Paseo de  Gracia, with a view of Gaudi's famous building <i>&quot;La Pedrera&quot;</i>.</p>
            <p>The hotel offers among others, two special elements: a unique  garden terrace with a view to <i>&quot;La Pedrera&quot;</i> on the first floor and a  fully equipped solarium on the top floor of the building. Surrounded by restaurants that meet your category most exquisite  tastes, our beloved <em>NEXT TO</em> is a great alternative to offer the  opportunity to feel well taken care of well at home.</p>
			<p><b>Description of rooms:</b><br />All rooms are well cared for and selected designs. We offer very  comfortable beds, plasma TV 37 &quot;, WIFI internet, mini bar, terrace,  possibility, LED lighting, solar hot water, designer bathrooms with  high quality finishes.</p>
        </div>
    </div>
    
<?php include("../includes/3piecera.php");?>