	<meta name="language" content="es" />
	<link rel="stylesheet" type="text/css" href="<?=$ruta;?>includes/_estilos.css" />
	<script type="text/javascript" src="<?=$ruta;?>includes/_funciones.js"></script>
    <script type="text/javascript" src="<?=$ruta;?>includes/_func_calendar.js"></script>
    <script type="text/javascript" src="<?=$ruta;?>includes/shadowbox/shadowbox-prototype.js"></script>
	<script type="text/javascript" src="<?=$ruta;?>includes/shadowbox/shadowbox-base.js"></script>
    <script type="text/javascript" src="<?=$ruta;?>includes/shadowbox/shadowbox.js"></script>
    <script type="text/javascript">
	Shadowbox.loadSkin('classic', '<?=$ruta;?>includes/shadowbox/');Shadowbox.loadLanguage('es', '<?=$ruta;?>includes/shadowbox/');
	Shadowbox.loadPlayer(['img'], '<?=$ruta;?>includes/shadowbox/');window.onload = Shadowbox.init;
	</script>
</head>

<body>
<div id="central">
	<div class="top">
		<a href="<?=$ruta;?>"><img title="Hotel Next To" src="<?=$ruta;?>img/logo-nextto.gif" alt="Hotel Nextto" /></a>
        <a title="Nextto" href="<?=$ruta;?>">&raquo;home</a>
        <a title="Hotel" href="<?=$ruta;?>hotel/">&raquo;hotel</a>
        <a title="Suites" href="#">&raquo;suites</a>
        <? /*<a title="Suites" href="<?=$ruta;?>suites/">&raquo;suites</a> */?>
        <a title="Reservations" href="<?=$ruta;?>reservations.php">&raquo;reservations</a>
        <a title="Conditions" href="<?=$ruta;?>conditions.php">&raquo;conditions</a>
        <a title="Contact" href="<?=$ruta;?>contact.php">&raquo;contact</a>
	</div>