<?php include("includes/1cabecera.php");?>
	<title>Conditions NextTo Barcelona </title>
	<meta name="description" content="Conditions Hotel Barcelona Suites Barcelona NextTo" />
	<meta name="keywords" content="Conditions Hotel Barcelona Suites Barcelona NextTo" />
<?php include("includes/2cabecera.php");?>

	<div class="cuerpo_hotel" style="height:590px;">
    	<div class="col_1">
        	<h3>CONDITIONS</h3>
            <dl>
            <dt>Check in time</dt>
        	<dd>After 13:00 hrs, if the arrival is after 22:00 hrs, there is an extra charge of 30&euro; to pay directly to the person who will do the check in.</dd>
        	<dt>Check out time</dt>
        	<dd>Before 11:00 hrs.</dd>
        	<dt>Payment</dt>
        	<dd>30% when the booking is done and the balance due to pay in cash in your arrival.</dd>
        	<dt>How to pick up the key</dt>
        	<dd>In the apartment, please confirm your arrival time at least 1 week before your arrival by email: info@rentperdaysbarcelona.com</dd>
            <dt>Cancellations</dt>
        	<dd>In case of cancellation no refund will be made on the pre-payment. However, you will be able to change the dates of your rental period subject to availability on the same apartment, and 20 days in advance.</dd>
        	<dt>Cleaning service</dt>
        	<dd>At your arrival the apartment is cleaned with nice and fresh towels and bedlinen. Our cleaning team has verified that everything is in place. This cleaning will be performed prior to your arrival. The cost of the cleaning is obligatory, and its price is 30&euro;.</dd>
        	<dt>How to get to the apartmet</dt>
        	<dd><em>From the airport:</em><br />Taxi: 20/25 minutes depending on traffic. aprox. 25&euro;<br />AeroBus: 35 minutes. The aerobus stops in front of every airport terminal. Departures every 10 minutes. To reach the apartment, step off the aerobus at Pla&ccedil;a Catalunya and take a taxi to the apartment, the ride is aprox. 6&euro;.</dd>
            <dd><em>From Sans station:</em><br />Metro: Take the line 3 and stop in Drassanes.<br />AeroBus: Step off the aerobus at Pla&ccedil;a Catalunya and take a taxi to the apartment, the ride is aprox. 6&euro;.</dd>
            </dl>
        </div>
        <div class="col_2">
        	<h3>LEGAL</h3>
        	<dl>        
			<dt>User Agreement and privacy policy</dt>
            <dd>In compliance with the Organic Law 15/1999 of 13 December on Protection of Personal Data, we wish to inform you that your personal contact details have been incorporated in computerized files belonging to Nextto, who will be the only recipient of said data, the purpose of which is to deal with clients and handle actions related to commercial communications.<br /><br />At the same time, we would like to remind you that you have the right to access, rectify, amend and/or delete such data, as provided for by law, via a written request addressed to info@nextto.biz.</dd>
            <dt>Disclosure of your personal details</dt>
            <dd>Nextto agrees never to sell, rent or reveal your personal details given on registering, except in the following two indicated cases: in an agreement signed by yourself personally, or a request from legal authorities.<br /><br />The parties agree that the conditions are subject to Spanish legislation. In the event of any dispute regarding the interpretation or execution of these conditions, and in the event of failure in any attempt to reach an amicable solution, authority will be exclusively attributed to the Spanish Courts.</dd>
            <dt>Intellectual and Industrial Property</dt>
            <dd>In compliance with the international agreements on Intellectual and Industrial Property, Nextto is the sole and exclusive owner of the Intellectual and Industrial Property rights of the website and its contents. Total or partial reproduction of the website is entirely prohibited, under penalty of instigation of legal proceedings.</dd>
			</dl>            
        </div>
	</div>
    
<?php include("includes/3piecera.php");?>