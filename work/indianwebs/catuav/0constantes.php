<?php
define("BOT1_esp","INICIO");
define("BOT2_esp","EMPRESA");									
define("BOT3_esp","SERVICIOS");									
define("BOT4_esp","TECNOLOG&Iacute;A");	
define("BOT5_esp","GALER&Iacute;A");									

define("BOT1_cat","INICI");
define("BOT2_cat","EMPRESA");
define("BOT3_cat","SERVEIS");
define("BOT4_cat","TECNOLOGIA");
define("BOT5_cat","GALERIA");

define("BOT1_ing","HOME");
define("BOT2_ing","COMPANY");
define("BOT3_ing","SERVICES");
define("BOT4_ing","TECHNOLOGY");
define("BOT5_ing","GALLERY");

define("SEC_NEWS_esp","NOTICIAS");
define("SEC_NEWS_cat","NOT&Iacute;CIES");
define("SEC_NEWS_ing","NEWS");
define("SEC_NEWS2_esp","Ver todas las noticias");
define("SEC_NEWS2_cat","Veure totes les not&iacute;cies");
define("SEC_NEWS2_ing","See all news");
define("SEC_LOC_esp","LOCALIZACI&Oacute;N");
define("SEC_LOC_cat","LOCALITZACI&Oacute;");
define("SEC_LOC_ing","LOCATION");
define("SEC_LOC2_esp","C&oacute;mo llegar a CATUAV...");
define("SEC_LOC2_cat","Com arribar a CATUAV...");
define("SEC_LOC2_ing","How to arrive at CATUAV...");
define("SEC_GAL_esp","GALERIA FOTOGR&Agrave;FICA");
define("SEC_GAL_cat","GALER&Iacute;A FOTOGR&Agrave;FICA");
define("SEC_GAL_ing","PHOTO GALLERY");
            
define("G_ORTO_esp","Ortofotos Georeferenciadas");
define("G_ORTO_cat","Ortofotos georeferenciades");
define("G_ORTO_ing","Geocoded Images");
define("G_OPER_esp","Fotos de la operativa");
define("G_OPER_cat","Fotos de l'operativa");
define("G_OPER_ing","Operational System Pictures");
define("G_ESTEREO_esp","Vertical estereosc&oacute;pica");
define("G_ESTEREO_cat","Vertical estereosc&ograve;pica");
define("G_ESTEREO_ing","Stereoscopic Images");
define("G_NDVI_esp","Infraroja con tratamiento NDVI");
define("G_NDVI_cat","Infraroja amb tractament NDVI");
define("G_NDVI_ing","Vegetation Index Images (NDVI)");
define("G_NIR_esp","Infraroja con tratamiento NIR");
define("G_NIR_cat","Infraroja amb tractament NIR");
define("G_NIR_ing","Near Infrared Images (NIR)");
define("G_VISIBLE_esp","Vertical en espectro visible");
define("G_VISIBLE_cat","Vertical en espectre visible");
define("G_VISIBLE_ing","Visible Images");

define("VIDEO_DEMO_esp","Demostraci&oacute;n del sistema CATUAV");
define("VIDEO_DEMO_cat","Demostraci&oacute; del sistema CATUAV");
define("VIDEO_DEMO_ing","Demonstration of the CATUAV system");
define("VIDEO_ATMOS_esp","Vuelo nocturno Atmos-3");
define("VIDEO_ATMOS_cat","Vol nocturn<br>Atmos-3");
define("VIDEO_ATMOS_ing","Night flying Atmos-3");

define("DOC_ESTUDIO_esp","Estudio");
define("DOC_ESTUDIO_cat","Estudi");
define("DOC_ESTUDIO_ing","Study");
define("DOC_DATOS_esp","Datos t&eacute;cnicos");
define("DOC_DATOS_cat","Dades t&agrave;cniques");
define("DOC_DATOS_ing","Technical data");

define("EMPRESA_0_esp","CATUAV ha invertido en los &uacute;ltimos a&ntilde;os muchos recursos en el desarrollo de una tecnolog&iacute;a innovadora propia. Como empresa nos proponemos  amortizar la inversi&oacute;n realizada prestando servicios y colaborando en la  difusi&oacute;n de las aplicaciones a los veh&iacute;culos a&eacute;reos no tripulados. La cultura  organizativa de la empresa prioriza la identificaci&oacute;n y el despliegue de nuevas  aplicacions adaptadas a los UAV. Con esta finalidad CATUAV facilitar&aacute; convenios  y colaboraciones con universidades, proyectos experimentales, centros de  innovaci&oacute;n y empresas en las que los objetivos a conseguir no se cuantifiquen  s&oacute;lo en beneficions econ&oacute;micos inmediatos.");
define("EMPRESA_1_esp","Ingeniero electr&oacute;nico con m&aacute;s de 20 a&ntilde;os de experiencia, es responsable de los enlaces, el software y la integraci&oacute;n de los sistemas.");
define("EMPRESA_2_esp","Gestor de proyectos, coordinador del equipo y responsable del &aacute;rea comercial.");
define("EMPRESA_3_esp","F&iacute;sico con amplia experiencia en diferentes campos de la industria espacial. Es responsable del desarrollo de sensores de imagen para los UAVs.");
define("EMPRESA_4_esp","Dise&ntilde;ador de las plataformas UAV, instructor de vuelo y responsable de operaciones. Es gerente y fundador de la empresa.");

define("EMPRESA_0_cat","CATUAV ha invertit ens els darrers anys molts recursos en desenvolupar una tecnologia innovadora pr&ograve;pia. Com a empresa ens proposem  amortitzar aquestes inversions donant serveis i col.laborant en la difusi&oacute; de  les aplicacions als vehicles aeris no tripulats. La cultura organitzativa de  CATUAV prioritza la identificaci&oacute; i el desplegament de noves aplicacions adaptades als UAV. Amb aquesta finalitat CATUAV facilitar&agrave; convenis i col&middot;laboracions amb universitats, projectes experimentals, centres de recerca i empreses en els que els objectius a assolir no es quantifiquin nom&eacute;s en  beneficis econ&ograve;mics inmediats.");
define("EMPRESA_1_cat","Enginyer electr&oacute;nic amb m&eacute;s de 20 anys d'experi&egrave;ncia, &eacute;s responsable dels enlla&ccedil;os, el software i la integraci&oacute; dels sistemes.");
define("EMPRESA_2_cat","Gestor de projectes, coordinador de l'equip i cap de l'&agrave;rea comercial.");
define("EMPRESA_3_cat","F&iacute;sic amb m&eacute;s de quinze anys d'experi&egrave;ncia en diferents camps de la ind&uacute;stria espaial. &Eacute;s responsable del desenvolupament de sensors d`imatge per als UAVs.");
define("EMPRESA_4_cat","Dissenyador de les plataformes UAV, instructor de vol i responsable d'operacions. &Eacute;s gerent i fundador de l'empresa.");

define("EMPRESA_0_ing","CATUAV has invested a lot of resources over the last few years in the development of an own innovating technology. As a company we set out to recover the investment by providing services and collaborating in the diffusion of the applications of unmanned aircraft. The organizational culture of the company prioritises the identification and deployment of new applications adapted to the UAV. With this purpose CATUAV will facilitate agreements and collaborations with universities, experimental projects, centres of innovation and companies in which the objectives to be obtained are not quantified solely in immediate economic benefits.");
define("EMPRESA_1_ing","An electronics engineer with more than 20 years of experience, he is responsible for the connections, software and systems integration.");
define("EMPRESA_2_ing","The project manager, team coordinator and head of the commercial area.");
define("EMPRESA_3_ing","Physicist with a wide and strong background in different fields of the spatial industry applications. He is responsible of the development of imaging sensors for the UAVs.");
define("EMPRESA_4_ing","A designer of UAV platforms, flying instructor and head of operations. He is manager and founder of the company.");
?>