<p><img title="Uav Frio" src="http://www.catuav.com/img/noticia_tvmadrid.jpg" alt="Uav frio" align="left" style="margin:0 10px 0 0;"/></p>
<? switch($_SESSION["lang"])
{
	case "cat": ?>
    <p><b>12/01/09<br />CATUAV  en directe a Telemadrid</b></p>
	<p>En aplicaci&oacute; de l&rsquo;acord estrat&egrave;gic amb  Aurensis i en el marc d&rsquo;una jornada t&egrave;cnica de demostraci&oacute; d&rsquo;UAV&rsquo;s a Madrid,  CATUAV va ser present, en directe, en el programa <em>Buenos d&iacute;as, Madrid</em> un reportatge de 12&rsquo; que va descriure els UAV&rsquo;s i  les seves aplicacions com un dels sector m&eacute;s innovadors a l&rsquo;Estat espanyol.</p>
	<? break;
	
	case "ing": ?>
	<p><b>12/01/09<br />CATUAV on live in Telemadrid TV</b></p>
	<p>In the context of their strategic agreement framework, CATUAV and Aurensis participated in a technical UAV demonstration workshop in Madrid. As part of it, CATUAV performed an on live exhibition in the �Buenos dias Madrid� magazine of Telemadrid showing their UAV application to the territory monitoring as the most innovative Earth observation activity in Spain.</p>
	<? break;

	case "esp":
	default: ?>
	<p><b>12/01/09<br />CATUAV  en directo en Telemadrid</b></p>
	<p>En aplicaci&oacute;n del acuerdo estrat&eacute;gico con Aurensis  y en el marco de una jornada t&eacute;cnica de demostraci&oacute;n de UAV&rsquo;s en Madrid, CATUAV  estuvo presente, en directo, en el programa <em>Buenos  d&iacute;as, Madrid</em> un reportaje de 12&rsquo;  que describi&oacute; los UAV&rsquo;s y sus aplicaciones como uno de los sectores m&aacute;s innovadores  en el Estado espa&ntilde;ol.</p>
	<? break;
} ?>