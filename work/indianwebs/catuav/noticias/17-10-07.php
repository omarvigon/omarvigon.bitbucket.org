<?
switch($_SESSION["lang"])
{
	case "cat":
	echo "<p><b>17/10/07</b> Es crea l’empresa CATUAV S.L. per donar vida pròpia i independent a  les activitats relacionades amb els UAV que desde l’any 2003 s’havien fet dins  d’Aeroplans Blaus S.L. El motiu principal és separar dues activitat diferents  com són la importació i comercialització d’equips i sistemes per aeronaus  esportives del desenvolupament i operació d’UAVs. Les dues empreses  comparteixen a partir d’ara les mateixes instal.lacions a l’aeròdrom de Moi&agrave;.</p>";
	break;
	
	case "ing":
	echo "<p><b>17/10/07</b> CATUAV S.L. is created to give the activities related to UAVs, which have taken place within Aeroplans Blaus S.L. since 2003, their own independent existence. The main reason is to separate two different activities: the import and selling of equipment and systems for sport aircraft, and the development and operation of UAVs. From now on the two companies share the same facilities in the Moià airfield.</p>";
	break;
	
	case "esp":
	default:
	echo "<p><b>17/10/07</b> Se crea la empresa CATUAV   S.L. para dar vida propia e independiente a las actividades  relacionadas con los UAV que desde el año 2003 se efectuaban dentro de  Aeroplans Blaus S.L. El motivo principal es separar dos actividades diferentes:  la importación y comercialización de equipos y sistemas para aeronaves  deportivas y el desarrollo y operaci&oacute;n de UAVs. Las dos empresas comparten a  partir de ahora las mismas instalaciones en el aer&oacute;dromo de Moi&agrave;.</p>";
	break;
}
?>