<p><img title="CATUAV" src="http://www.catuav.com/img/noticia-globalgeo.jpg" alt="CATUAV" align="left" style="margin:0 10px 0 0;"/></p>
<? switch($_SESSION["lang"])
{
	case "ing": ?>
	<p><b>04/03/09<br />CATUAV  attend the international fair GLOBALGEO</b></p>
    <p>The ATMOS-3  UAV platform was exposed at the Fair GLOBALGEO in a prominent location of the  Aurensis stand, located at the main entrance of the Fair. </p>
	<p>The presence of  CATUAV aircraft at the Fair is part of the strategic agreement signed between  CATUAV and AURENSIS to boost the services offered by the new UAV systems.</p>
	<? break;
	
	case "cat": ?>
    <p><b>04/03/09<br />CATUAV  &eacute;s present a la Fira internacional GLOBALGEO</b></p>
    <p>L&rsquo;aeronau  ATMOS-3 ha estat exposada a la Fira GLOBALGEO, en un lloc destacat de l&rsquo;estand  d&rsquo;Aurensis, ubicat a l&rsquo;entrada principal de la Fira.</p>
	<p>La  pres&egrave;ncia de l&rsquo;aeronau de CATUAV a la Fira &eacute;s una actuaci&oacute; de l&rsquo;acord  estrat&egrave;gic signat entre&nbsp; AURENSIS i CATUAV per donar a con&egrave;ixer els  serveis que ofereixen els nous sistemes UAV.</p>
	<? break;
	
	default:
	case "esp": ?>
    <p><b>04/03/09<br />Presencia  de CATUAV en la Feria internacional GLOBALGEO</b></p>
    <p>La  platadorma UAV ATMOS-3 estuvo expuesta en la Feria GLOBALGEO, en una ubicaci&oacute;n  destacada del stand de Aurensis, situado en la entrada principal de la Feria.</p>
	<p>La  presencia de la aeronave de CATUAV en la Feria se inscribe en el acuerdo  estrat&eacute;gico firmado entre&nbsp; AURENSIS y CATUAV para&nbsp; impulsar los  servicios que ofrecen los nuevos sistemas UAV. </p>
	<? break;
	
} ?>