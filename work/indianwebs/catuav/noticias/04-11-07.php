<?
switch($_SESSION["lang"])
{
	case "cat":
	echo "<p><b>4/11/07</b> CATUAV finalitza amb &egrave;xit un programa de vols nocturns que ha perm&egrave;s posar a prova el nou sistema d’aterratge sense visibilitat i diferents millores  aplicades a la plataforma UAV ATOM-3. En un total de 8 vols s’han acumulat 12 hores de vol nocturn.</p>";
	break;
	
	case "ing":
	echo "<p><b>4/11/07</b> CATUAV has successfully completed a night flight program that allowed the testing of a new landing system without visibility and the application of different improvements to the ATMOS-3 UAV platform. In a total of 8 flights, 12 h of nocturnal flight were accumulated.</p>";
	break;
	
	case "esp":
	default:
	echo "<p><b>4/11/07</b> CATUAV finaliza con &eacute;xito un programa de vuelos nocturnos que ha permitido  poner a prueba el nuevo sistema de aterrizaje sin visibilidad y diferentes  mejoras aplicadas a la plataforma UAV ATOM-3. En un total de 8 vuelos se han  acumulado 12 h de vuelo nocturno.</p>";
	break;
}
?>