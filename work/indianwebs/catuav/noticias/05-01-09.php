<p><img title="Uav Frio" src="http://www.catuav.com/img/noticia_atmos4.jpg" alt="Uav frio" align="left" style="margin:0 10px 0 0;"/></p>
<? switch($_SESSION["lang"])
{
	case "cat": ?>
    <p><b>05/01/09<br />El vol  inaugural de la plataforma <em>Atmos-4</em>, el  millor regal de Reis</b></p>
	<p>El primer vol d&rsquo;una nova plataforma &eacute;s una  viv&egrave;ncia &uacute;nica, el moment en qu&egrave; els mesos de c&agrave;lculs, assajos i modelitzacions  s&rsquo;han de contrastar, en un instant, amb la dimensi&oacute; de la realitat.</p>
	<p><em>Atmos&ndash;4</em> &eacute;s un avi&oacute; de  propulsi&oacute; el&egrave;ctrica, com el seu predecessor Atmos&ndash;3, per&ograve; amb una bodega que  triplica el volum de c&agrave;rrega. Aquestes innovacions faciliten embarcar c&agrave;meres i  sensors m&eacute;s sofisticats, que permeten gravar imatge m&ograve;bil en alta definici&oacute; o  introduir sistemes inercials de vol.</p>
	<p><em>Atmos&ndash;4</em> &eacute;s en plena fase  operativa. En paral&middot;lel, s&rsquo;estan avaluant les seves prestacions i perfeccionant  els sistemes de calibraci&oacute;. Aquesta plataforma s&rsquo;ha pogut desenvolupar en aplicaci&oacute;  del projecte de I+D ITUMA, cofinan&ccedil;at per ACC1&Oacute; del CIDEM. </p>
	<p>Pot visionar-se el vol inaugural a <a href="http://es.youtube.com/watch?v=k80uc63kd98" target="_blank"><b>http://es.youtube.com/watch?v=k80uc63kd98</b></a></p>
	<? break;
	
	case "ing": ?>
	<p><b>05/01/09<br />Maiden flight of the new Atmos-4 platform</b></p>
	<p>The first flight of a new platform is a unique experience. The moment when months of calculations, tests and hard work must be contrasted with reality.</p>
    <p>The <em>Atmos-4</em>is an electric UAV, similar in more aspects to its predecessor Atmos-3, but with three times more cargo volume. This innovation facilitates the use of bigger and more sophisticated sensors and cameras able to record HD moving images and the introduction of inertial sensors.</p>
	<p>After its inaugural flight, <em>Atmos-4</em> is now in operational phase. In parallel, CATUAV is evaluating its performances and enhancing its calibration systems. The development of this platform has been co-financed in part by the Goverment of Catalonia in application of the ITUMA R&D Project.</p>
	<p>The maiden flight can be watched in <a href="http://es.youtube.com/watch?v=k80uc63kd98" target="_blank"><b>http://es.youtube.com/watch?v=k80uc63kd98</b></a></p>
	<? break;
	
	case "esp":
	default: ?>
	<p><b>05/01/09<br />Vuelo inaugural de la plataforma Atmos-4, el mejor regalo de Reyes</b></p>
	<p>El primer vuelo de una nueva plataforma es  una experiencia &uacute;nica, el instante en que meses de c&aacute;lculos, ensayos y modelizaciones deben contrastarse con  la realidad.</p>
	<p><em>Atmos&ndash;4</em> es un avi&oacute;n de  propulsi&oacute;n el&eacute;ctrica, como su predecesor Atmos&ndash;3, pero con una bodega que  triplica el volumen de carga. Estas innovaciones facilitan el uso de c&aacute;maras y  sensores m&aacute;s sofisticados, que permiten grabar imagen m&oacute;vil en alta definici&oacute;n  o introducir sistemas inerciales de vuelo.</p>
	<p><em>Atmos&ndash;4</em> est&aacute; ya en fase  operativa. En paralelo, se est&aacute;n evaluando sus prestaciones y perfeccionando los  sistemas de calibraci&oacute;n. Esta plataforma se ha podido desarrollar en aplicaci&oacute;n  del proyecto de I+D ITUMA, cofinanciado por el CIDEM del Gobierno de Catalu&ntilde;a. </p>
	<p>Puede  visionarse el vuelo inaugural en <a href="http://es.youtube.com/watch?v=k80uc63kd98" target="_blank"><b>http://es.youtube.com/watch?v=k80uc63kd98</b></a></p>
	<? break;
} ?>