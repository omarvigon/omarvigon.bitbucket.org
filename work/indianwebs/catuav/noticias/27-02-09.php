<p><img title="Generalitat de Catalunya visita CATUAV" src="http://www.catuav.com/img/noticia-int2.jpg" alt="Generalitat de Catalunya visita CATUAV" align="left" style="margin:0 10px 0 0;"/></p>
<? switch($_SESSION["lang"])
{
	default:
	case "cat": ?>
    <p><b>25/02/09<br />El  Departament d&rsquo;Interior de la Generalitat de Catalunya visita CATUAV</b></p>
	<p>El  Secretari general del Departament d&rsquo;Interior, Relacions Institucionals i  Participaci&oacute;, Sr. Joan Boada, va encap&ccedil;alar la visita per con&egrave;ixer els serveis  de CATUAV a Moi&agrave;.</p>
	<p>L&rsquo;acompanyaven  el Sr. Carles Lluss&agrave;, Director dels Serveis Territorials de la Catalunya  Central, el Sr. Llu&iacute;s Torrens, Director de Serveis i una delegaci&oacute; de 10  persones m&eacute;s en representaci&oacute; dels Mossos d&rsquo;Esquadra, els Bombers i el Servei  Catal&agrave; de Tr&agrave;nsit.</p>
	<p>L&rsquo;alcalde  de Moi&agrave;, Sr. Josep Montr&agrave;s, va donar la benvinguda al Secretari General i a la  resta d&rsquo;assistents, que durant dues hores van estar observant els vols de  l&rsquo;Atmos 3 i Atmos 4 i valorant la seva adequaci&oacute; per les funcions que porta a  terme el Departament d&rsquo;Interior.</p>
	<? break;
	
	case "esp": ?>
    <p><b>25/02/09<br />El  Departamento de Interior de la Generalitat de Catalu&ntilde;a visita&nbsp;CATUAV</b></p>
	<p>El  Secretario General del Departamento de Interior de la Generalitat, Sr. Joan  Boada, encabez&oacute; una delegaci&oacute;n del Gobierno para conocer los servicios de  CATUAV. Acompa&ntilde;aban al Secretario General los responsables de los Servicios de  Prevenci&oacute;n y Extinci&oacute;n de Incendios, de la Polic&iacute;a y del Servicio de Tr&aacute;fico. </p>
	<p>En el  transcurso de la visita se realizaron vuelos de demostraci&oacute;n con las aeronaves  Atmos 3 y Atmos 4. Se valor&oacute;, tambi&eacute;n, la adecuaci&oacute;n de los sensores CATUAV  para las funciones encomendadas al Departamento de Interior en temas de  seguridad, vigilancia, rescate, coordinaci&oacute;n de incendios forestales y  movilidad. </p>
	<? break;
	
} ?>