<?php include("_cabecera.php"); ?>
        
    <div class="contenedor">
    	<script type="text/javascript">new fadeshow(fadeimages, 325, 244, 0, 4000, 1)</script>
        
    	<div class="cuerpo">
        	<h2>CATUAV</h2>
            <?php switch($_SESSION["lang"])
			{
				case "cat":
				echo "<p>CATUAV &eacute;s una empresa privada dedicada al desenvolupament i operaci&oacute; d'aeronaus no tripulades (UAV). Disposem de tecnologia pr&ograve;pia i plataformes plenament operatives que ja han demostrat la seva efic&agrave;cia i fiabilitat en aplicacions reals. Oferim als nostres clients solucions flexibles que els permetran obtenir el m&agrave;xim rendiment de la nostra flota UAV.";
				break;
				
				case "ing":
				echo "<p>CATUAV is a private company dedicated to the development and operation of unmanned aircraft (UAV). We have our own technology and totally operative platforms that have already demonstrated their effectiveness and reliability in real applications. We offer flexible solutions to our clients that will allow them to obtain the maximum performance from our UAV fleet.</p>";
				break;
				
				case "esp":
				default:
				echo "<p>CATUAV es una empresa privada dedicada al desarrollo y a la operaci&oacute;n de aeronaves no tripuladas  (UAV). Disponemos de tecnolog&iacute;a propia y plataformas plenamente operativas que ya han demostrado su eficacia y fiabilidad en aplicaciones reales. Ofrecemos a  nuestros clientes soluciones flexibles que les permitir&aacute;n obtener el m&aacute;ximo rendimiento de nuestra flota UAV.</p>";
				break;			
			}?>

        </div>
        <div class="derecha">
			<?php include("noticias-caja.php");?>
   		 </div>
    </div>
    <!-- <p align="center" style="margin:500px 0 0 0;"><a href="http://www.indianwebs.com" target="_blank" title="Dise&ntilde;o Web Barcelona" style="font:10px Arial, Helvetica, sans-serif;color:#999999;text-decoration:none;">Dise&ntilde;o Web Barcelona</a></p>-->

<?php include("_piecera.php"); ?>
