<?php include("_cabecera.php"); ?>
        
    <div class="contenedor">
    	<img src="img/catuav-1.jpg" alt="Informacion Catuav" title="Informacion Catuav" style="margin:15px 0 0 0;border:1px solid #dddddd;"/>
    	<div class="cuerpo">
        	<h2>FAQ</h2>
            <?php switch($_SESSION["lang"])
			{
			//<p><a href="javascript:mostrar(1)">Què és un  UAV? </a></p>
			//<p id="oculto1">El terme Uauie de missions. </p>
			
			case "cat": ////////////////////// catalan ?>
			<p><a href="info-tecnica.php?pag=1">Què és un  UAV? </a></p>
			<? if($_GET["pag"]==1) 	
                echo "<p>El terme UAV és l’abreviació de  la denominació anglesa Unmanned Aerial Vehicle, és a dir, vehicle aeri no  tripulat. Es tracta d’aeronaus no tripulades amb capacitat per portar a terme  missions de forma autònoma. Aquestes missions poden ser de tipus científic,  militar, d’observació, filmació, fotografia, CATUAV disposa de 4 models d’UAV (Atmos-3, Atmos-5, Furos i Argos) que s’ajusten a diferents tipus de missions.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=2">Quins  avantatges tenen els UAV respecte les aeronaus convencionals? </a></p>
			<? if($_GET["pag"]==2) 
            	echo "<p>Tenen grans avantatges, i de  naturalesa ben diferent. Per una banda són una alternativa molt més econòmica i  ecològica als avions i helicòpters. El fet de no portar tripulació també els  permet portar a terme missions perilloses sense posar en risc el pilot. El  reduït pes i tamany d’alguns UAV com l’Atmos-3 també els permet sobrevolar  persones o propietats sense posar-los en risc ni molestar-los, i fins i tot  sense ser detectats, cosa que els fa ideals per a tasques policials o  d’observació de la   natura. Cada dia es descobreixen noves aplicacions per als  UAV, es tracta d’una nova tecnologia que tot just comença a trobar les seves  aplicacions pràctiques.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=3">Quina és l’autonomia de vol dels UAV de CATUAV?</a></p>
			<? if($_GET["pag"]==3) 
				echo "<p>L’UAV de propulsió elèctrica  Atmos-3 pot volar un màxim de 2 hores, les plataformes de motor alternatiu  Furos i Atmos-5 tenen una autonomia de 6 i 10 h respectivament. Amb la darrera s&egrave;rie Argos s'aconsegueix una autonomia de m&eacute;s de 14 hores en vol programat.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=4">Com és el sistema CATUAV?</a></p>
			<? if($_GET["pag"]==4) 
            	echo "<p>El sistema CATUAV consta d’un  centre de control terrestre comú a tots els UAV i dels propis UAV, que són  desmuntables i es poden portar dins de qualsevol vehicle. El centre de control  s’assembla molt a una cabina d’avió, pot allotjar fins a tres persones en el  seu interior i disposa de dues pantalles que mostren tota la informació  relativa al vol. Totes les imatges i dades obtingudes es graven en format  digital. Gràcies a dues bateries de gran capacitat i a un grup electrògen el  centre de control té una gran autonomia. Un sol pilot pot operar tot el sistema  CATUAV, tot i que per determinades missions és aconsellable disposar d’un  copilot.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=5">Com es pilota un UAV?</a></p>
			<? if($_GET["pag"]==5) 
            	echo "<p>El pilotatge és molt similar al  d’una aeronau convencional, amb la diferència que el pilot és a terra dins la  cabina de control. El pilot disposa de tota la instrumentació i dels sistemes  de navegació i comunicacions d’una aeronau convencional. L’UAV també pot volar  de forma autònoma controlat per un pilot automàtic que es pot activar i  desactivar des del centre de control.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=6">Quina és la distància màxima de treball?</a></p>
			<? if($_GET["pag"]==6) 
            	echo "<p>El radi d’acció del sistema  CATUAV està limitat a uns 15   km al voltant del centre de control per a l’Atmos-3 i a  uns 20 km  per al Furos i Atmos-5. En cap cas es poden interposar obtacles entre el centre  de control i la plataforma, cal tenir sempre present l’orografia del terreny a  l’hora de programar els vols. Els vols pre-programats al pilot automàtic poden  fer-se més enllà del radi d’acció de la cabina de control i depenen només de  l’autonomia de la plataforma.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=7">Quina &eacute;s la regulaci&oacute; normativa dels UAV's?</a></p>
			<? if($_GET["pag"]==7) 
            	echo "<p>Ara per ara no disposem d’una  reglamentació que reguli l’ús dels UAV. Des del punt de vista de l’ocupació de  l’espai aeri la seva operació és legal sempre que no superi els 300 m sobre el terreny ni  entri en cap espai aeri controlat. Respectant aquestes limitacions són  infinites les aplicacions que podem donar al sistema CATUAV avui mateix.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=8">Es pot volar de nit?</a></p>
			<? if($_GET["pag"]==8) 
            	echo "<p>Sí, totes les plataformes de  CATUAV poden volar de nit. El sistema de navegació GPS i unes balisses  lluminoses portàtils permeten l’aterratge per instruments fins i tot de nit o  amb molta boira.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=9">Quines són les condicions meteorològiques necessàries?</a></p>
			<? if($_GET["pag"]==9) 
            	echo "<p>Les plataformes de CATUAV poden  volar fins i tot en condicions de vent, turbulència i reducció de visibilitat  que impedirien el vol d’una aeronau lleugera convencional. No volem amb vents  superiors a 35 km/h  ni visibilitats horitzontals inferiors a 200 m. Evidentment per als treballs  d’observació, fotografia i video esperem a obtenir condicions meteorològiques  favorables.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=10">Com ha de ser la zona d’enlairament i  aterratge?</a></p>
			<? if($_GET["pag"]==10) 
            	echo "<p>Per a operar còmodament es  necessita una zona lliure d’obstacles d’uns 200 m de radi, preferiblement  lluny d’edificis i línies d’alta tensió.  </p>"; ?>
			
            <p><a href="info-tecnica.php?pag=11">Hi ha perill de col.lisió amb altres  aeronaus?</a></p>
			<? if($_GET["pag"]==11) 
            	echo "<p>Segons un estudi del MIT (Massachusetts Institute of Technology) que podeu consultar en la nostra secció  d’informació tècnica, les probabilitats estadístiques de col.lisió d’un UAV com l’Atmos-3 en un espai aeri relativament congestionat són de l’ordre d’una entre 100.000.000 per hora de vol, és a dir, extremadament baixes. Si tenim en compte que durant les operacions evitem sempre els espais aeris controlats i fem servir comunicacions en banda aeronàutica seguint els protocols habituals, les probabilitats s’acosten molt a zero. Recordeu que desde la dècada dels 40 a Europa es llencen dotzenes de sondes meteorològiques cada dia que arriben fins als 15.000 m d’alçada i que  seguint els vents dominants no eviten aerovies ni espais aeris controlats, tot  i això i després de llençar-ne centenars de milers, no s’ha produït mai cap  col.lisió. Cal recordar també que totes les aeronaus certificades estan preparades per als impactes amb aus de pes i mida superiors als nostres AV.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=12">Quin és el cost econòmic dels sistemes UAV de CATUAV?</a></p>
			<? if($_GET["pag"]==12) 
            	echo "<p>CATUAV no comercialitza cap dels  seus sistemes, només ofereix als clients la possibilitat de llogar els seus  serveis. Les tarifes depenen de la plataforma, del tipus de treball i de les  hores de vol necessàries per portar-lo a terme. CATUAV pot preparar-li un  pressupost que s’ajusti a les seves necessitats.</p>"; ?>
			<? break;
			
			case "ing": ////////////////////// ingles ?>
            <p><a href="info-tecnica.php?pag=1">What is a UAV?</a></p>
			<? if($_GET["pag"]==1) 	
                echo "<p>The acronym UAV is the abbreviation of the English term Unmanned Aerial Vehicle. They are unmanned aircraft with the capacity to carry out missions independently. The types of missions could be scientific, military, observational, filming, photographic… CATUAV has 4 models of UAV (Atmos-3, Atmos-5, Furos and Argos) that are adapted to different types of missions.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=2">What advantages do UAVs have over conventional aircraft?</a></p>
			<? if($_GET["pag"]==2) 
            	echo "<p>They have great advantages, and of very different types. On the one hand they are a much more economical and ecological alternative to planes and helicopters in certain tasks. When not carrying crew it allows dangerous missions to be undertaken without putting the pilot at risk. The reduced weight and size of some UAVs, such as the Atmos-3, also allows flying over people or properties without putting them at risk or creating annoyances, and they can even pass unnoticed, which makes them ideal for police tasks or for the observation of nature. New applications are discovered for UAVs every day, it is a new technology that is beginning to find practical applications.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=3">What are the flying times of the CATUAV UAVs?</a></p>
			<? if($_GET["pag"]==3) 
				echo "<p>The Atmos-3 UAV functions with electrical propulsion and can fly a maximum of 2 hours; the alternative motor platforms of Furos and Atmos-5 have a range of 6 and 10 h respectively.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=4">What is the CATUAV system?</a></p>
			<? if($_GET["pag"]==4) 
            	echo "<p>The CATUAV system consists of a ground control centre common to all the different UAV models. It can be dismantled and transported in any vehicle. The control centre is very similar to an aeroplane cabin, can accommodate up to three people in its interior and has two screens that show the information relative to the flight in real time. All the images and data obtained are recorded in digital format. Thanks to two large capacity batteries and a generating group the control centre has a great autonomy. A single pilot can operate the CATUAV system, although for certain missions it is advisable to have a co-pilot.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=5">How is a UAV piloted?</a></p>
			<? if($_GET["pag"]==5) 
            	echo "<p>The piloting is very similar to that of a conventional aircraft, except that the pilot is on the ground. The pilot has the same instrumentation and navigation systems used in conventional aircraft. The UAV can also fly independently controlled by an automatic pilot which can be activated and deactivated from the control centre.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=6">What is the maximum working distance?</a></p>
			<? if($_GET["pag"]==6) 
            	echo "<p>The operational range of the CATUAV system is limited to about 15 km around the control centre for the Atmos-3 and about 20 km for the Furos and Atmos-5. In no circumstance can obstacles be interposed between the control centre and the UAV, the orography of the terrain must be taken into account when programming the flights. The flights pre-programmed in the automatic pilot can extend the operational range of the control cabin and they only depend on the flying times of the UAV.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=7">Is the use of the CATUAV system legal?</a></p>
			<? if($_GET["pag"]==7) 
            	echo "<p>At the moment there is no legislation that regulates the use of UAVs. From the point of view of the use of the airspace, their operation is legal provided they do not exceed 1.000 feet above ground level or enter controlled airspace. Respecting these limitations, the applications that can be given to the CATUAV system today are infinite</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=8">Can they be flown at night?</a></p>
			<? if($_GET["pag"]==8) 
            	echo "<p>Yes, all the CATUAV platforms can fly at night. The GPS positioning system and portable beacon lights allow flying by instrument.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=9">What are the necessary meteorological conditions?</a></p>
			<? if($_GET["pag"]==9) 
            	echo "<p>CATUAV UAV platforms can fly up to conditions of wind, turbulence and reduced visibility that would prevent the flight of a conventional light aircraft. Operations are suspended with winds greater than 35 km/h or visibility of less than 200 m. For observational, photographic or video missions favourable meteorological conditions are required for obvious reasons.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=10">What must the takeoff and landing area be like?</a></p>
			<? if($_GET["pag"]==10) 
            	echo "<p>In order to operate comfortably a clearway with a radius of about 200 m is needed, preferably far from buildings and power lines.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=11">Is there a danger of collision with other aircraft?</a></p>
			<? if($_GET["pag"]==11) 
            	echo "<p>According to an MIT study (Massachusetts Institute of Technology) that can be consulted in our technical information section, the statistical probabilities of collision of a UAV such as the Atmos-3 in a relatively congested airspace is of the order of one in 100,000,000 per flight hour, extremely low. If we consider that in all our flights we always avoided controlled air space and use aeronautical band communications following the usual protocols, the probabilities become much closer to zero. From the decade of the 40’s in Europe dozens of meteorological probes have been launched every day that reach 15,000 m and that follow prevailing winds without avoiding air routes or controlled air space. In spite of having launched several hundreds of thousands already, there has never been a collision. It is also important to remember that all aircraft are prepared to resist the impact of birds that exceed the weight and size of Atmo-3.</p>"; ?>
			
            <p><a href="info-tecnica.php?pag=12">What is the economic cost of the CATUAV UAV systems?</a></p>
			<? if($_GET["pag"]==12) 
            	echo "<p>CATUAV does not sell any of its systems, it only offers its clients the possibility of renting their services. The tariffs depend on the UAV platform to be used, the type of work and the flight hours necessary to carry it out. CATUAV can prepare a budget according to need.</p>"; ?>
			<? break;
			
			case "esp":
			default: ////////////////////// castellano  ?>
			<p><a href="info-tecnica.php?pag=1">&iquest;Qu&eacute; es un UAV? </a></p>
            <? if($_GET["pag"]==1) 
				echo "<p>El t&eacute;rmino UAV es la abreviaci&oacute;n de la denominaci&oacute;n inglesa Unmanned Aerial  Vehicle, o veh&iacute;culo a&eacute;reo no tripulado. Se trata de aeronaves no tripuladas con  capacidad de llevar a cabo misiones de forma aut&oacute;noma. Estas misiones pueden  ser de tipo cient&iacute;fico, militar, de observaci&oacute;n, filmaci&oacute;n, fotograf&iacute;a … CATUAV  dispone de 4 modelos de UAV (Atmos-3, Atmos-5, Furos y Argos) que se ajustan a  distintos tipos de misiones.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=2">&iquest;Qu&eacute;  ventajas tienen los UAV sobre las aeronaves convencionales?</a></p>
			<? if($_GET["pag"]==2)
            	echo "<p>Tienen grandes  ventajas, y de tipo muy distinto. Por un lado son una alternativa mucho m&aacute;s  econ&oacute;mica y ecol&oacute;gica a los aviones y helic&oacute;pteros en determinadas  tareas.&nbsp; Al no llevar tripulaci&oacute;n les permite llevar a cabo misiones  peligrosas sin poner en riesgo al piloto. El reducido peso y tamaño de algunos  UAV como el Atmos-3 tambi&eacute;n &nbsp;permite sobrevolar personas o propiedades sin  ponerlos en riesgo ni crear molestias, y pueden incluso pasar desapercibidos,  lo que los hace ideales para tareas policiales o de observaci&oacute;n de la naturaleza. Cada  d&iacute;a se descubren nuevas aplicaciones para los UAV, se trata de una nueva  tecnolog&iacute;a que est&aacute; empezando a encontrar aplicaciones pr&aacute;cticas.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=3">&iquest;Cu&aacute;l es la autonom&iacute;a de los UAV de CATUAV?</a></p>
            <? if($_GET["pag"]==3)
            	echo "<p>El UAV de  propulsi&oacute;n el&eacute;ctrica Atmos-3 puede volar un m&aacute;ximo de 2 horas, las plataformas  de motor alternativo Furos y Atmos-5 tienen una autonom&iacute;a de 6 y 10 h  respectivamente.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=4">&iquest;C&oacute;mo es el sistema CATUAV?</a></p>
            <? if($_GET["pag"]==4) 
            	echo "<p>El sistema  CATUAV consta de un centro de control terrestre com&uacute;n a todos los UAV y de los  distintos modelos de UAV, que son desmontables y pueden transportarse en  cualquier veh&iacute;culo. El centro de control es muy similar a una cabina de avi&oacute;n,  puede albergar hasta tres personas en su interior y dispone de dos pantallas  que muestran en tiempo real la informaci&oacute;n relativa al vuelo. Todas las  im&aacute;genes y datos obtenidos se registran en formato digital. Gracias a dos  bater&iacute;as de gran capacidad y a un grupo generador el centro de control tiene  una gran autonom&iacute;a. Un solo piloto puede operar el sistema CATUAV, aunque para  determinadas misiones es aconsejable disponer de un copiloto.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=5">&iquest;C&oacute;mo se pilota un UAV?</a></p>
            <? if($_GET["pag"]==5)
            	echo "<p>El pilotaje es  muy similar al de una aeronave convencional, salvo que el piloto se encuentra  en tierra. El piloto dispone de la misma instrumentaci&oacute;n y de los mismos  sistemas de navegaci&oacute;n empleados en aeronaves convencionales. El UAV tambi&eacute;n  puede volar de forma aut&oacute;noma controlado por un piloto autom&aacute;tico que se puede  activar y desactivar desde el centro de control.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=6">&iquest;Cu&aacute;l es la distancia m&aacute;xima de trabajo?</a></p>
            <? if($_GET["pag"]==6)
            	echo "<p>El radio de  acci&oacute;n del sistema CATUAV est&aacute; limitado a unos 15 km alrededor del centro de  control para el Atmos-3 y a unos 20   km para el Furos y Atmos-5. En ning&uacute;n caso se pueden  interponer obst&aacute;culos entre el centro de control y el UAV, hay que tener en  cuenta la orograf&iacute;a del terreno al programar los vuelos. Los vuelos  pre-programados en el piloto autom&aacute;tico pueden superar el radio de acci&oacute;n de la  cabina de control y s&oacute;lo dependen de la autonom&iacute;a del UAV.</p>"; ?>
           
            <p><a href="info-tecnica.php?pag=7">&iquest;Es legal el uso del sistema CATUAV?</a></p>
            <? if($_GET["pag"]==7)
            	echo "<p>Por ahora no  disponemos de una reglamentaci&oacute;n que regule el uso de los UAV. Desde el punto  de vista del uso del espacio a&eacute;reo, su operaci&oacute;n es legal siempre que no supere  los 300 m  sobre el terreno ni entre en espacio a&eacute;reo controlado. Respetando estas  limitaciones son infinitas las aplicaciones que podemos dar al sistema CATUAV  hoy mismo.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=8">&iquest;Se puede volar de noche?</a></p> 
            <? if($_GET["pag"]==8)
            	echo "<p>S&iacute;, todas las  plataformas de CATUAV pueden volar de noche. El sistema de navegaci&oacute;n GPS y  unas balizas luminosas port&aacute;tiles permiten el vuelo por instrumentos.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=9">&iquest;Cu&aacute;les son las condiciones meteorol&oacute;gicas necesarias?</a></p>
            <? if($_GET["pag"]==9)
            	echo "<p>Las  plataformas UAV de CATUAV pueden volar hasta en condiciones de viento,  turbulencia y visibilidad reducida que impedir&iacute;an el vuelo de una aeronave  ligera convencional. Las operaciones se suspenden con vientos superiores a 35 km/h o visibilidad  inferior a 200 m.  Por razones evidentes, para las misiones de observaci&oacute;n, fotograf&iacute;a o video se  requieren condiciones meteorol&oacute;gicas favorables.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=10">&iquest;C&oacute;mo tiene que ser la zona de despegue y aterrizaje?</a></p>
            <? if($_GET["pag"]==10)
            	echo "<p>Para operar  c&oacute;modamente se necesita una zona libre de obst&aacute;culos de unos 200 m de radio,  preferiblemente lejos de edificios y l&iacute;neas de alta tensi&oacute;n.</p>"; ?>
            
            <p><a href="info-tecnica.php?pag=11">&iquest;Hay peligro de colisi&oacute;n con otras aeronaves?</a></p>
            <? if($_GET["pag"]==11)
            	echo "<p>Seg&uacute;n un  estudio del MIT (Massachusetts Institute of Technology) que se puede consultar  en nuestra secci&oacute;n de informaci&oacute;n t&eacute;cnica, las probabilidades estad&iacute;sticas de  colisi&oacute;n de un UAV como el Atmos-3 en un espacio a&eacute;reo relativamente  congestionado son del orden de una entre 100.000.000 por hora de vuelo,  extremadamente bajas. Si consideramos que en todos los vuelos evitamos siempre  los espacios a&eacute;reos controlados y utilizamos comunicaciones en banda  aeron&aacute;utica siguiendo los protocolos habituales, las probabilidades se acercan  mucho a cero. Desde la d&eacute;cada de los 40 en Europa se lanzan docenas de sondas  meteorol&oacute;gicas cada d&iacute;a que alcanzan los 15.000 m y que siguen los  vientos dominantes sin evitar aerov&iacute;as ni espacios a&eacute;reos controlados. A pesar  de haberse lanzado ya varios centenares de miles, no se ha producido nunca  ninguna colisi&oacute;n. 
            	Tambi&eacute;n es importante recordar que todas las aeronaves est&aacute;n  preparadas para resistir el impacto de aves que superan en peso y tamaño al  Atmos-3</p>"; ?>   
           
            <p><a href="info-tecnica.php?pag=12)">&iquest;Cu&aacute;l es el coste econ&oacute;mico de los sistemas UAV de CATUAV?</a></p>
            <? if($_GET["pag"]==12)
            	echo "<p>CATUAV no  comercializa ninguno de sus sistemas, s&oacute;lo ofrece a sus clientes la posibilidad  de alquilar sus servicios. Las tarifas dependen de la plataforma UAV a  emplear, del tipo de trabajo y de las horas de vuelo necesarias para llevarlo a  cabo. CATUAV puede prepararle un presupuesto que se ajuste a sus necesidades.</p>"; ?>
			
			<? break;  } ?>
        </div>
        <div class="derecha">
            <h3>DOCUMENTS</h3>
			<p><a href='files/estudi_mit.pdf' target='_blank'><img src='img/pdf.gif' border='0' align='absmiddle'/>&nbsp;<?=constant("DOC_ESTUDIO_".$_SESSION["lang"])?> MIT</a></p>
			<p><a href='files/atmos-3esp.pdf' target='_blank'><img src='img/pdf.gif' border='0' align='absmiddle'/>&nbsp;<?=constant("DOC_DATOS_".$_SESSION["lang"])?> ATMOS-3</a></p>
			<p><a href='files/furos-esp.pdf' target='_blank'><img src='img/pdf.gif' border='0' align='absmiddle'/>&nbsp;<?=constant("DOC_DATOS_".$_SESSION["lang"])?> FUROS</a></p>
			<p><a href='files/argos.pdf' target='_blank'><img src='img/pdf.gif' border='0' align='absmiddle'/>&nbsp;<?=constant("DOC_DATOS_".$_SESSION["lang"])?> ARGOS</a></p>
			<p><a href='files/vegcam-1.pdf' target='_blank'><img src='img/pdf.gif' border='0' align='absmiddle'/>&nbsp;<?=constant("DOC_DATOS_".$_SESSION["lang"])?> VEGCAM-1</a></p>
            <p><a href='files/panopte-1.pdf' target='_blank'><img src='img/pdf.gif' border='0' align='absmiddle'/>&nbsp;<?=constant("DOC_DATOS_".$_SESSION["lang"])?> PANOPTE-1</a></p>
   		 </div>
    </div>   
   
<?php include("_piecera.php"); ?>