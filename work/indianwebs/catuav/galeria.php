<?php include("_cabecera.php"); ?>
        
    <div class="contenedor">
    	<div class="cuerpo_galeria">
		<h2><?=constant("SEC_GAL_".$_SESSION["lang"]);?></h2>

        <?php switch($_GET["seccion"])
		{ 
			case "espectro_visible": ?>			
            <div align="center">
            	<a href="javascript:galeria('espectro_vis1')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis1_mini.jpg" alt="UAV Espectro visible" /></a>
                <a href="javascript:galeria('espectro_vis2')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis2_mini.jpg" alt="UAV Espectro visible" /></a>
                <a href="javascript:galeria('espectro_vis3')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis3_mini.jpg" alt="UAV Espectro visible" /></a>
                <a href="javascript:galeria('espectro_vis4')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis4_mini.jpg" alt="UAV Espectro visible" /></a>
                <a href="javascript:galeria('espectro_vis5')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis5_mini.jpg" alt="UAV Espectro visible" /></a>
                <a href="javascript:galeria('espectro_vis6')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis6_mini.jpg" alt="UAV Espectro visible" /></a>
                <a href="javascript:galeria('espectro_vis7')"><img title="UAV Espectro visible" src="img/galeria/espectro_vis7_mini.jpg" alt="UAV Espectro visible" /></a>
            </div>
            <img title="UAV Espectro visible" id="caja_galeria" src="img/galeria/espectro_vis1.jpg" class="imggaleria" alt="UAV Espectro visible"/>
			<? break; 
			
			case "infrarrojo_nir":?>
			<div align="center">
            	<a href="javascript:galeria('infrarroja_nir1')"><img title="UAV Infrarroja NIR" src="img/galeria/infrarroja_nir1_mini.jpg" alt="UAV Infrarroja NIR" /></a>
                <a href="javascript:galeria('infrarroja_nir2')"><img title="UAV Infrarroja NIR" src="img/galeria/infrarroja_nir2_mini.jpg" alt="UAV Infrarroja NIR" /></a>
                <a href="javascript:galeria('infrarroja_nir3')"><img title="UAV Infrarroja NIR" src="img/galeria/infrarroja_nir3_mini.jpg" alt="UAV Infrarroja NIR" /></a>
                <a href="javascript:galeria('infrarroja_nir4')"><img title="UAV Infrarroja NIR" src="img/galeria/infrarroja_nir4_mini.jpg" alt="UAV Infrarroja NIR" /></a>
            </div>
            <img title="UAV Infrarroja NIR" id="caja_galeria" src="img/galeria/infrarroja_nir1.jpg" class="imggaleria" alt="UAV Infrarroja NIR"/>
			<? break; 
			
			case "infrarroja_ndvi":?>
			<div align="center">
            	<a href="javascript:galeria('infrarroja_dvi1')"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi1_mini.jpg" alt="UAV Infrarroja DVI" /></a>
                <a href="javascript:galeria('infrarroja_dvi2')"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi2_mini.jpg" alt="UAV Infrarroja DVI" /></a>
                <a href="javascript:galeria('infrarroja_dvi3')"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi3_mini.jpg" alt="UAV Infrarroja DVI" /></a>
                <a href="javascript:galeria('infrarroja_dvi4')"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi4_mini.jpg" alt="UAV Infrarroja DVI" /></a>
                <a href="javascript:galeria('infrarroja_dvi5')"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi5_mini.jpg" alt="UAV Infrarroja DVI" /></a>
                <a href="javascript:galeria('infrarroja_dvi6')"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi6_mini.jpg" alt="UAV Infrarroja DVI" /></a>
            </div>
            <img title="UAV Infrarroja DVI" id="caja_galeria" src="img/galeria/infrarroja_dvi1.jpg" class="imggaleria" alt="UAV Infrarroja DVI"/>
			<? break; 
			
			case "estereoscopica":?>
			<div align="center">
            	<img title="Gafas" src="img/gafas.gif" alt="Glasses" />
            	<a href="javascript:galeria('estereoscopica1')"><img title="UAV estereoscopica" src="img/galeria/estereoscopica1_mini.jpg" alt="UAV estereoscopica" /></a>
                <a href="javascript:galeria('estereoscopica2')"><img title="UAV estereoscopica" src="img/galeria/estereoscopica2_mini.jpg" alt="UAV estereoscopica" /></a>
                <a href="javascript:galeria('estereoscopica3')"><img title="UAV estereoscopica" src="img/galeria/estereoscopica3_mini.jpg" alt="UAV estereoscopica" /></a>
                <a href="javascript:galeria('estereoscopica4')"><img title="UAV estereoscopica" src="img/galeria/estereoscopica4_mini.jpg" alt="UAV estereoscopica" /></a>
                <a href="javascript:galeria('estereoscopica5')"><img title="UAV estereoscopica" src="img/galeria/estereoscopica5_mini.jpg" alt="UAV estereoscopica" /></a>
            </div>
            <img title="UAV estereoscopica" id="caja_galeria" src="img/galeria/estereoscopica1.jpg" class="imggaleria" alt="UAV estereoscopica"/>
			<? break; 
			
			case "ortofoto":?>
			<div align="center">
            	<a href="javascript:galeria('ortofoto1')"><img title="UAV" src="img/galeria/ortofoto1_mini.jpg" alt="UAV" /></a>
                <a href="javascript:galeria('ortofoto2')"><img title="UAV" src="img/galeria/ortofoto2_mini.jpg" alt="UAV" /></a>
                <a href="javascript:galeria('ortofoto3')"><img title="UAV" src="img/galeria/ortofoto3_mini.jpg" alt="UAV" /></a></div>
            <img title="UAV" id="caja_galeria" src="img/galeria/ortofoto1.jpg" class="imggaleria" alt="UAV"/>			
			<? break; 
			
			case "operativa":?>
			<div align="center">
            	<a href="javascript:galeria('operativa1')"><img title="UAV" src="img/galeria/operativa1_mini.jpg" alt="UAV" /></a>
                <a href="javascript:galeria('operativa2')"><img title="UAV" src="img/galeria/operativa2_mini.jpg" alt="UAV" /></a>
                <a href="javascript:galeria('operativa3')"><img title="UAV" src="img/galeria/operativa3_mini.jpg" alt="UAV" /></a>
                <a href="javascript:galeria('operativa4')"><img title="UAV" src="img/galeria/operativa4_mini.jpg" alt="UAV" /></a>
                <a href="javascript:galeria('operativa5')"><img title="UAV" src="img/galeria/operativa5_mini.jpg" alt="UAV" /></a>            </div>
            <img title="UAV" id="caja_galeria" src="img/galeria/operativa1.jpg" class="imggaleria" alt="UAV"/>
			<? break; 

			default:
			?>
			<table cellspacing="1" cellpadding="12">
        	<tr>
            	<td><a href="galeria.php?seccion=espectro_visible"><img title="UAV Espectro visible" src="img/galeria/espectro_vis1_mini.jpg" alt="UAV Espectro visible" align="absmiddle" hspace="4" /><?=constant("G_VISIBLE_".$_SESSION["lang"])?></a></td>
           	  <td><a href="galeria.php?seccion=infrarrojo_nir"><img title="UAV Infrarroja NIR" src="img/galeria/infrarroja_nir1_mini.jpg" alt="UAV Infrarroja NIR" align="absmiddle" hspace="4" /><?=constant("G_NIR_".$_SESSION["lang"])?></a></td>
        	</tr>
            <tr>
            	<td><a href="galeria.php?seccion=infrarroja_ndvi"><img title="UAV Infrarroja DVI" src="img/galeria/infrarroja_dvi1_mini.jpg" alt="UAV Infrarroja DVI" align="absmiddle" hspace="4" /><?=constant("G_NDVI_".$_SESSION["lang"])?></a></td>
              <td><a href="galeria.php?seccion=estereoscopica"><img title="UAV estereoscopica" src="img/galeria/estereoscopica3_mini.jpg" alt="UAV estereoscopica" align="left" hspace="4" /><?=constant("G_ESTEREO_".$_SESSION["lang"])?><br />
              <img title="Gafas" src="img/gafas.gif" alt="Glasses" width="64" border="0" /></a></td>
            </tr>
        	<tr>
            	<td><a href="galeria.php?seccion=ortofoto"><img title="UAV" src="img/galeria/ortofoto1_mini.jpg" alt="UAV" align="absmiddle" hspace="4" /><?=constant("G_ORTO_".$_SESSION["lang"])?></a></td>
           	  <td><a href="galeria.php?seccion=operativa"><img title="UAV" src="img/galeria/operativa1_mini.jpg" alt="UAV" align="absmiddle" hspace="4" /><?=constant("G_OPER_".$_SESSION["lang"])?></a></td>
        	</tr>
        	</table>
			<? break;
		} ?>
        </div>
        <?php if(!$_GET["seccion"]) { ?>
        <div class="derecha_video">
        	<h3>VIDEOS</h3>
            <p><a href='videos.php?video=aerial'><img title='CATUAV aerial boundaries' src='img/video1.gif' align='left' border='0' alt='CATUAV aerial boundaries' style='margin:0 10px 0 0;'/><br />CATUAV aerial boundaries<br /></a></p>
            <p><a href='videos.php?video=highmountain'><img title='CATUAV high mountain' src='img/video1.gif' align='left' border='0' alt='CATUAV high mountain' style='margin:0 10px 0 0;'/><br />CATUAV high mountain<br /></a></p>
            <p><a href='videos.php?video=earth'><img title='CATUAV earth observation UAVs' src='img/video1.gif' align='left' border='0' alt='CATUAV earth observation UAVs' style='margin:0 10px 0 0;'/>CATUAV earth observation UAVs</a></p>
            
            <p><a href='videos.php?video=demostracion'><img title='<?=constant("VIDEO_DEMO_".$_SESSION["lang"]);?>' src='img/video1.gif' align='left' border='0' alt='<?=constant("VIDEO_DEMO_".$_SESSION["lang"]);?>' style='margin:0 10px 0 0;'/><?=constant("VIDEO_DEMO_".$_SESSION["lang"]);?></a></p>
			<p><a href='videos.php?video=nocturno'><img title='<?=constant("VIDEO_ATMOS_".$_SESSION["lang"]);?>' src='img/video1.gif' align='left' border='0' alt='<?=constant("VIDEO_ATMOS_".$_SESSION["lang"]);?>' style='margin:0 10px 0 0;'/><?=constant("VIDEO_ATMOS_".$_SESSION["lang"]);?></a></p>
   		 </div>
         <? } ?>
</div>

<?php include("_piecera.php"); ?>