<h3><a title='<?=constant("SEC_NEWS_".$_SESSION["lang"]);?>' href='noticias.php'><?=constant("SEC_NEWS_".$_SESSION["lang"]);?></a></h3>
<marquee direction='up' scrolldelay='20' scrollamount='1' style='height:250px;'>
<?php 
switch($_SESSION["lang"])
{
    case "cat": ?>  
    <p><a href='noticias.php?noticia=04-03-09'><b>04/03/09</b><br />CATUAV  &eacute;s present a la Fira internacional GLOBALGEO</a></p>
    <p><a href='noticias.php?noticia=27-02-09'><b>25/02/09</b><br />El  Departament d&rsquo;Interior de la Generalitat de Catalunya visita CATUAV</a></p>
    <p><a href='noticias.php?noticia=30-01-09'><b>30/01/09</b><br />Peritatge  de la devastaci&oacute; del vent a Sant Boi de Llobregat</a></p>
    <p><a href='noticias.php?noticia=12-01-09'><b>12/01/09</b><br />CATUAV  en directe a Telemadrid</a></p>
    <p><a href='noticias.php?noticia=05-01-09'><b>05/01/09</b><br />El vol  inaugural de la plataforma <em>Atmos-4</em>, el  millor regal de Reis</a></p>
	<? break;
		
	case "ing": ?>
    <p><a href='noticias.php?noticia=04-03-09'><b>04/03/09</b><br />CATUAV  attend the international fair GLOBALGEO</a></p>
    <p><a href='noticias.php?noticia=30-01-09'><b>30/01/09</b><br />Catastrophic wind damage evaluation in Sant Boi de Llobregat</a></p>
    <p><a href='noticias.php?noticia=12-01-09'><b>12/01/09</b><br />CATUAV on live in Telemadrid TV</a></p>
    <p><a href='noticias.php?noticia=05-01-09'><b>05/01/09</b><br />Maiden flight of the new Atmos-4 platform</a></p>            
	<? break;
	
	default: ?>
    <p><a href='noticias.php?noticia=04-03-09'><b>04/03/09</b><br />Presencia  de CATUAV en la Feria internacional GLOBALGEO</a></p>
    <p><a href='noticias.php?noticia=27-02-09'><b>25/02/09</b><br />El  Departamento de Interior de la Generalitat de Catalu&ntilde;a visita&nbsp;CATUAV</a></p>
    <p><a href='noticias.php?noticia=30-01-09'><b>30/01/09</b><br />Peritaje  de la devastaci&oacute;n del viento en Sant Boi de Llobregat</a></p>
    <p><a href='noticias.php?noticia=12-01-09'><b>12/01/09</b><br />CATUAV  en directo en Telemadrid</a></p>
    <p><a href='noticias.php?noticia=05-01-09'><b>05/01/09</b><br />Vuelo inaugural de la plataforma Atmos-4, el mejor regalo de Reyes</a></p>
	<? break;
} ?>
</marquee>