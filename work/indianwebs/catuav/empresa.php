<?php include("_cabecera.php"); ?>
        
    <div class="contenedor">
    	<div class="cuerpo_empresa">
        <h2><?=constant("BOT2_".$_SESSION["lang"])?></h2>
		<p><?=constant("EMPRESA_0_".$_SESSION["lang"])?></p>
            <table cellspacing='0' cellpadding='0'>
            <tr>
                <td><img class='imgequipo' src='img/equipo_josep.jpg' alt='Josep Santacana' title='Josep Santacana'></td>
                <td><b>Josep Santacana</b><br><?=constant("EMPRESA_1_".$_SESSION["lang"])?> <a href="mailto:josepsantacana@catuav.com">josepsantacana@catuav.com</a></td>
                <td><img class='imgequipo' src='img/equipo_josepfont.jpg' alt='Josep Font' title='Josep Font'></td>
                <td><b>Josep Font</b><br><?=constant("EMPRESA_2_".$_SESSION["lang"])?> <a href="mailto:josepfont@catuav.com">josepfont@catuav.com</a></td>
            </tr>
            <tr>
                <td><img class='imgequipo' src='img/equipo_manuel.jpg' alt='David Sunyol' title='David Sunyol'></td>
                <td><b>Manuel Castillo</b><br><?=constant("EMPRESA_3_".$_SESSION["lang"])?> <a href="mailto:manuelc@catuav.com">manuelc@catuav.com</a></td>
                <td><img class='imgequipo' src='img/equipo_jordi.jpg' alt='Jordi Santacana' title='Jordi Santacana'></td>
                <td><b>Jordi Santacana</b><br><?=constant("EMPRESA_4_".$_SESSION["lang"])?> <a href="mailto:jordisantacana@catuav.com">jordisantacana@catuav.com</a></td>
            </tr>
            </table>
        </div>
        
        <div class="derecha">
        	<h3><?=constant("SEC_LOC_".$_SESSION["lang"]);?></h3>
			<p><a href="localizacion.php"><img src="img/localizacion.jpg" alt="Localizacion de Catuav" title="Localizacion de Catuav" border="0"/><br /><br />&nbsp;<?=constant("SEC_LOC2_".$_SESSION["lang"]);?></a></p>
   		 </div>
    </div>

<?php include("_piecera.php"); ?>