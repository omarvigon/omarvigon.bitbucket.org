<?php
session_start();
if($_GET["lang"])			
	$_SESSION["lang"]=$_GET["lang"];
else	
{
	if(!$_SESSION["lang"])	
		$_SESSION["lang"]="esp";
}	
include("0constantes.php");

$nombre=$_SERVER['PHP_SELF'];
$nombre=substr($nombre,1,strlen($nombre)-5);
$nombre=strtoupper($nombre);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)" />
    <meta name="verify-v1" content="27kP2kKIAlHgNfsbb5zEhSIkfF/iZiNUbpVRoYu0mxk=" />
    <meta name="revisit" content="7 days" />
    <meta name="robots" content="index,follow,all" />
	<meta name="description" content="Catuav Avion no tripulado UAV - AEROPLANS BLAUS, SL. UAV Barcelona CATUAV Unmanned Plane Drone en Moia Barcelona Spain" />
	<meta name="keywords" content="Catuav Avion no tripulado UAV - AEROPLANS BLAUS, SL. UAV Barcelona CATUAV Unmanned Plane Drone Moia Barcelona Spain" />
    <meta name="title" content="Catuav Avion no tripulado UAV <?php echo $nombre;?> AEROPLANS BLAUS, SL" />
    <meta name="lang" content="es,sp,spanish,español,espanol,castellano" />
    <meta name="author" content="www.indianwebs.com" />
    <title>Catuav Avion no tripulado UAV <?php echo $nombre;?> AEROPLANS BLAUS, SL</title>
	<link rel="stylesheet" type="text/css" href="0estilos.css" />
    <link rel="icon" href="favicon.ico" />
    <link rel="shortcut icon"  href="favicon.ico" />
	<script language="javascript" type="text/javascript" src="0funciones.js"></script>
    <script type="text/javascript" src="http://www.catuav.com/includes/accordian.pack.js"></script>
</head>

<body onload="new Accordian('acordeon',5,'header_highlight');">
  <div class="logo">
  		<div class="idiomas">
		<a href='http://www.catuav.com<?=$_SERVER['PHP_SELF']?>?lang=ing'> | ENGLISH</a>
		<a href='http://www.catuav.com<?=$_SERVER['PHP_SELF']?>?lang=esp'> | ESPA&Ntilde;OL</a>
 		<a href='http://www.catuav.com<?=$_SERVER['PHP_SELF']?>?lang=cat'> | CATAL&Agrave;</a>
        </div>
    	<a href="http://www.catuav.com/index.php"><img src="img/logo_catuav.gif" alt="Catuav" title="Catuav" border="0"/></a>
        <a href="http://www.catuav.com/index.php" class="bt1"><?=constant("BOT1_".$_SESSION["lang"])?></a>
        <a href="http://www.catuav.com/empresa.php" class="bt2"><?=constant("BOT2_".$_SESSION["lang"])?></a>
        <a href="http://www.catuav.com/servicios.php" class="bt3"><?=constant("BOT3_".$_SESSION["lang"])?></a>
        <a href="http://www.catuav.com/info-tecnica.php" class="bt4"><?=constant("BOT4_".$_SESSION["lang"])?></a>
        <a href="http://www.catuav.com/galeria.php" class="bt5"><?=constant("BOT5_".$_SESSION["lang"])?></a>   
	</div>
    <div class="direccion">Camp de vol El Prat 08180 Moi&agrave; (Spain) &nbsp;Tel. (+34) 93 830 05 30  &middot; <a href="mailto:info@catuav.com">info@catuav.com</a></div>
    
<div class="linea_azul">&nbsp;</div>
<div class="linea_gris">&nbsp;</div>
<div class="linea1">&nbsp;</div>
<div class="linea2">&nbsp;</div>
<div class="linea3">&nbsp;</div>
<div class="linea4">&nbsp;</div>
<div class="linea5">&nbsp;</div>