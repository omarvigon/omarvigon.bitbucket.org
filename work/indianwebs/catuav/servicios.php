<?php include("_cabecera.php"); ?>
       
    <div class="contenedor">
    	<div class="cuerpo" style="width:800px;left:95px">
		<!--<h2><?=//constant("BOT3_".$_SESSION["lang"])?></h2>-->
		
        <div id="acordeon">
        <br />
        
        <?php switch($_SESSION["lang"])
		{
			case "cat": ?>
            <div id="test-header" class="accordion_headings header_highlight">Cartografia</div>
  		<div id="test-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Ortofoto</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image1.jpg" alt="" /></td>
                <td>Realitzaci&oacute; d'ortofotomapes de termes municipals, nuclis urbans, parcs naturals i superf&iacute;cies agr&iacute;coles i forestals.</td>
            </tr>
            <tr>
                <td>Altimetria</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image2.jpg" alt="" /></td>
                <td>Realitzaci&oacute; de models d'elevacions del terreny d'alta resoluci&oacute;. C&agrave;lcul d'al&ccedil;ades d'elements del terreny. </td>
            </tr>
            </table>
  		</div>

  		<div id="test1-header" class="accordion_headings" >Agricultura</div>  
  		<div id="test1-content">  
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Gesti&oacute; de conreus, vivers i plantacions</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image3.jpg" alt="" /></td>
                <td>Control i monitoritzaci&oacute; de l'estat dels conreus mitjan&ccedil;ant imatges multiespectrals.</td>
            </tr>
            <tr>
                <td>Avaluaci&oacute; i comparativa de par&agrave;metres biof&iacute;sics</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image4.jpg" alt="" /></td>
                <td>Derivaci&oacute; de par&agrave;metres biof&iacute;sics utilitzant imatges multiespectrals i la seva variabilitat temporal en funci&oacute; de l'evoluci&oacute; fenol&ograve;gica dels conreus.</td>
            </tr>
            <tr>
                <td>Control de l'efici&egrave;ncia de regadius</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image5.jpg" alt="" /></td>
                <td>L'efici&egrave;ncia del reg s'expressa en correlaci&oacute; positiva amb l'&iacute;ndex normalitzat de vegetaci&oacute; definit amb imatges multiespectrals des de UAVs. Les observacions poden programar-se i periodificar-se en funci&oacute; de les pol&iacute;tiques de reg implementades.</td>
            </tr>
            </table> 
  		</div>

  		<div id="test2-header" class="accordion_headings" >Forestal</div>
  		<div id="test2-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Evoluci&oacute; i seguiment de boscos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image6.jpg" alt="" /></td>
                <td>Monitoritzaci&oacute; de l'estat d'&agrave;rees boscoses mitjan&ccedil;ant imatges multiespectrals.</td>
            </tr>
            <tr>
                <td>Mesura i evoluci&oacute; de densitat i d'al&ccedil;ada de l'arbrat</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image7.jpg" alt="" /></td>
                <td>Quantificaci&oacute; de la densitat, distribuci&oacute; i volumetria de differents especies forestals mitjan&ccedil;ant s&egrave;ries d'imatges multiespectrals i t&egrave;cniques estereosc&ograve;piques.</td>
            </tr>
            <tr>
                <td>Avaluaci&oacute; i comparativa de par&agrave;metres biof&iacute;sics</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image8.jpg" alt="" /></td>
                <td>Derivaci&oacute; de par&agrave;metres biof&iacute;sics utilitzant imatges multiespectrals i la seva variabilitat en funci&oacute; de la evoluci&oacute; fenol&ograve;gica de cada especie.</td>
            </tr>
            <tr>
                <td>Identificaci&oacute; d'esp&egrave;cies en grans extensions d'arbrat</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image9.jpg" alt="" /></td>
                <td>Clasificaci&oacute; del bosc utilitzant imatges multiespectrals i la seva variabilitat temporal en funci&oacute; de la evoluci&oacute; estacional de cada especie.</td>
            </tr>
            <tr>
                <td>Seguiment i avaluaci&oacute; de plagues</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image10.jpg" alt="" /></td>
                <td>Identificaci&oacute; de plagues i estudi de la seva extensi&oacute; i evoluci&oacute; mitjancant imatges multiespectrals i la seva variabilitat temporal</td>
            </tr>
            <tr>
                <td>Incendis</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image11.jpg" alt="" /></td>
                <td>Seguiment de l'estat de sequera dels boscos Suport a la coordinaci&oacute; de les tasques d'extinci&oacute;. Control de l'evoluci&oacute; d'incendis en temps real des de terra. C&agrave;lcul eficient del per&iacute;metre i de l'extensi&oacute; i del grau de calcinaci&oacute; del bosc cremat</td>
            </tr>
            </table>
 		</div>
        
        <div id="test3-header" class="accordion_headings" >Vitivinicultura</div>
  		<div id="test3-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Monitoritzaci&oacute; de vinyes</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image12.jpg" alt="" /></td>
                <td>Seguiment pormenoritzat de les vinyes, cep a cep, utilitzant imatges multiespectrals i la seva variabilitat al llarg de tot el cicle fenol&ograve;gic</td>
            </tr>
            <tr>
                <td>Suport a la viticultura de precisio</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image13.jpg" alt="" /></td>
                <td>Mitjan&ccedil;ant s&egrave;ries d'imatges multiespectrals: Quantificaci&oacute; de l'efecte, cep a cep, de la aplicaci&oacute; de reg, productes fitosanitaris, podes, etc., i optimitzaci&oacute; d'aquests recursos. Determinaci&oacute; precisa del moment de recolecci&oacute; en funci&oacute; de l'estat de maduraci&oacute; del raïm.</td>
            </tr>
            <tr>
                <td>Quantificaci&oacute; de danys</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image14.jpg" alt="" /></td>
                <td>Quantificaci&oacute; precisa de vinyes afectades, per incid&egrave;ncies meteorol&ograve;giques i clim&agrave;tiques extremes (calamarsa, gla&ccedil;ades, ventades, sequera, etc) o per plagues i contaminacio.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test4-header" class="accordion_headings" >Geologia</div>
  		<div id="test4-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Cartograf&iacute;a</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image15.jpg" alt="" /></td>
                <td>Captura d'imatges multiespectrals per a la realitzaci&oacute; a resoluci&oacute; centim&egrave;trica de mapes geol&ograve;gics sedimentol&ograve;gics, mineral&ograve;gics i geof&iacute;sics.</td>
            </tr>
            <tr>
                <td>Explotaci&oacute; minera</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image16.jpg" alt="" /></td>
                <td>Control i monitoritzaci&oacute; de explotacions mineres i el seu impacte ambiental: moviments de terres, producci&oacute; d'&agrave;rids, residus met&agrave;lics, balses de decantaci&oacute;, etc.</td>
            </tr>
            <tr>
                <td>Riscos Geol&ograve;gics: esllevissades, inundacions, etc</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image17.jpg" alt="" /></td>
                <td>Mitjan&ccedil;ant imatges multiespectrals, determinaci&oacute; i control a escala centim&egrave;trica d'&agrave;rees amb riscos geol&ograve;gics associats : esllevissades, inundacions, etc.</td>
            </tr>
            <tr>
                <td>Riscos Geol&ograve;gics: allaus</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image18.jpg" alt="" /></td>
                <td>Caracteritzaci&oacute; de zones amb risc d'allaus utilitzant imatges multiespectrals per a determinar la humitat de la neu, c&agrave;mares t&egrave;rmiques per a determinar la seva temperatura i t&egrave;cniques esteorosc&ograve;piques per a determinar gruixos.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test5-header" class="accordion_headings" >Hidrologia</div>
  		<div id="test5-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Control de conques</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image19.jpg" alt="" /></td>
                <td>An&agrave;lisi de l'estat d'ocupaci&oacute; de les conques, risc d'inundaci&oacute; i estimaci&oacute; de cabdals utilitzant s&egrave;ries d''imatges multiespectrals i t&egrave;cniques estereosc&ograve;piques.</td>
            </tr>
            <tr>
                <td>Quantificaci&oacute; de l'estat d´embassaments</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image20.jpg" alt="" /></td>
                <td>Quantificaci&oacute; i seguiment de conques i reserves d´embassaments utilitzant imatges visibles i multiespectrals amb t&egrave;cniques estereosc&ograve;piques.</td>
            </tr>
            <tr>
                <td>Quantificaci&oacute; de reserves hidrol&ograve;giques</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image21.jpg" alt="" /></td>
                <td>An&agrave;lisi de reserves d'aigua en alta muntanya i estimaci&oacute; de cabdals de desgla&ccedil; utilitzant s&egrave;ries d'imatges multiespectrals.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test6-header" class="accordion_headings" >Medi Ambient</div>
  		<div id="test6-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Contaminaci&oacute; lum&iacute;nica</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image22.jpg" alt="" /></td>
                <td>Parametritzaci&oacute; de l'&iacute;ndex de contaminaci&oacute; lum&iacute;nica per elaborar mapes de poluci&oacute; lum&iacute;nica i monitoritzar l'efici&egrave;ncia de mesures de ecoenerg&egrave;tiques. Per aquest prop&ograve;sit s'utilitza video digital d'alta definicio. L'analisi multiespectral d'aquestes imatges permet la clasificaci&oacute; i estad&iacute;stica de les diferents fonts de llum.</td>
            </tr>
            <tr>
                <td>Estat de l'atmosfera</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image23.jpg" alt="" /></td>
                <td>Determinaci&oacute; de perfils troposf&egrave;rics de temperatura i presi&oacute; i quantificaci&oacute; de la concentraci&oacute; d'aerosols i gasos mitjan&ccedil;ant detectors espec&iacute;fics: oz&oacute;, vapor d'aigua, CO2, etc. Mesures d'irradiaci&oacute; solar i albedo. Detecci&oacute; de fugues de gasos t&ograve;xics i fums</td>
            </tr>
            <tr>
                <td>Poluci&oacute; i vessaments t&ograve;xics en medi aqu&agrave;tic i terrestre</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image24.jpg" alt="" /></td>
                <td>Mitjancant imatges multiespectrals, control i seguiment d'accidents industrials amb vessaments t&ograve;xics en medi aqu&agrave;tic i terrestre. Amb l'&uacute;s de diferents bandes espectrals &eacute;s possible registrar la pres&egrave;ncia de determinats contaminants en superf&iacute;cie.</td>
            </tr>
            <tr>
                <td>Gesti&oacute;n de Residuos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image25.jpg" alt="" /></td>
                <td>Control independiente de &aacute;reas de dep&oacute;sito y almacenaje de residuos industriales y de su tratamiento. Les im&aacute;genes multiespectrales permiten el control cuantitativo del impacto ambiental de materiales peligrosos y de dif&iacute;cil reciclaje. </td>
            </tr>
            </table>
 		</div>
        
        <div id="test7-header" class="accordion_headings" >Gesti&oacute; Espais Naturals</div>
  		<div id="test7-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Registre i evoluci&oacute; del paisatge</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image26.jpg" alt="" /></td>
                <td>Creaci&oacute; d'arxius del paisatge visible en&oacute;Parcs Naturals, PEIN's i altres espais protegits. A m&eacute;s de constituir documents de mem&ograve;ria pel futur, permeten observar l'evoluci&oacute; dels espais: al llarg del cicle de l'any i en el temps (evoluci&oacute; conreus, boscos, modificacions antropog&egrave;niques, etc.)</td>
            </tr>
            <tr>
                <td>Planificaci&oacute; i seguiment de repoblacions</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image27.jpg" alt="" /></td>
                <td>Monitoritzaci&oacute; de la repoblaci&oacute; i recuperaci&oacute; de boscos cremats o degradats utiltzant imatges multiespectrals i la seva evoluci&oacute; temporal.</td>
            </tr>
            <tr>
                <td>Observaci&oacute; diurna i nocturna de fauna en espais oberts</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image28.jpg" alt="" /></td>
                <td>Els silenciosos motors el&egrave;ctrics i la capacitat de c&agrave;rrega dels UAV's permeten efectuar observacions diurnes i nocturnes de fauna en espais oberts sense interferir en la seva vida.</td>
            </tr>
            <tr>
                <td>Avaluacions d'impacte</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image29.jpg" alt="" /></td>
                <td>Estudis d'impacte ambiental a d'obertura de camins o d'altres infraestructures en espais naturals mitjancant imatges multiespectrals des de UAV.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test8-header" class="accordion_headings" >Obra p&uacute;blica</div>
  		<div id="test8-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Avaluaci&oacute; d'impacte</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image30.jpg" alt="" /></td>
                <td>An&aacute;lisis del impacto visual de infraestructuras en grandes extensiones de terreno mediante im&aacute;genes en espectro visible capturadas desde UAVs a baja altura.</td>
            </tr>
            <tr>
                <td>Control obres civils</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image31.jpg" alt="" /></td>
                <td>Seguiment de la implementaci&oacute; d'obres civils que afecten grans extensions (carreteres, canals, embassaments, l&iacute;nies alta tensi&oacute;, parcs e&ograve;lics, etc.) utilitzant imatges a&egrave;ries a escales centimetriques.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test9-header" class="accordion_headings" >Urbanisme</div>
  		<div id="test9-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Gesti&oacute; i planejament urban&iacute;stics</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image32.jpg" alt="" /></td>
                <td>Observaci&oacute; i seguiment de construccions mitjan&ccedil;ant sobrevol. Registre en fotografia d'alta resoluci&oacute; des de diferents perspectives o en v&iacute;deo de zones urbanes vinculades a projectes de planejament</td>
            </tr>
            <tr>
                <td>Control de desenvolupament urban&iacute;stic ilegal</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image33.jpg" alt="" /></td>
                <td>Mitjan&ccedil;ant imatges multiespectrals i visibles embarcades en UAVs, &eacute;s possible la detecci&oacute; de construccions ilegals en zones rurals, boscoses i de dif&iacute;cil acc&eacute;s.</td>
            </tr>
            <tr>
                <td>Gesti&oacute; d'&agrave;rees verdes</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image34.jpg" alt="" /></td>
                <td>Seguiment de l'estat de zones verdes i caracteritzaci&oacute; de la seva interacci&oacute; amb les &agrave;rees urbanes anexes mitjan&ccedil;ant s&egrave;ries d'imatges multiespectrals i visibles.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test10-header" class="accordion_headings" >Mobilitat-Tr&agrave;nsit</div>
  		<div id="test10-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Monitoritzaci&oacute; de la mobilitat per carretera</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image35.jpg" alt="" /></td>
                <td>Seguiment de l'estat del tr&agrave;nsit. &eacute;s una alternativa eficient i amb valor afegit respecte a l'helic&ograve;pter, al suprimir riscos per al personal que retransmet l'estat del tr&agrave;nsit i permet gravar digitalment la situaci&oacute; di&agrave;ria del tr&agrave;nsit, a m&eacute;s de ser molt m&eacute;s econ&ograve;mica i sostenible (no contamina)</td>
            </tr>
            </table>
 		</div>
        
        <div id="test11-header" class="accordion_headings" >Protecci&oacute; Civil</div>
  		<div id="test11-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Integraci&oacute; i centralitzaci&oacute; de funcions</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image36.jpg" alt="" /></td>
                <td>La coordinaci&oacute; de funcions que s'extenen en l'espai i es concentren en el temps pot constituir una funci&oacute; cr&iacute;tica en grans esdeveniments. El control i la saturaci&oacute; d'accessos a peu i amb vehicle, el seguiment de fluxos de persones, la situaci&oacute; dels p&agrave;rquings i qualsevol incid&egrave;ncia pot coordinar-se i ser transmesa als usuaris amb facilitat amb l'ajut dels UAV's </td>
            </tr>
            <tr>
                <td>Manifestacions culturals en espais oberts i nuclis urbans</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image37.jpg" alt="" /></td>
                <td>Monitoritzaci&oacute; en temps real de fluxos de persones. Control d'incid&egrave;ncies en med&iacute; urba.</td>
            </tr>
            <tr>
                <td>Suport a equips de rescat</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image38.jpg" alt="" /></td>
                <td>Detecci&oacute; de v&iacute;ctimes en zones de risc geol&ograve;gic: allaus, inundacions, esllevisades. Desenvolupament de sistemes de detecci&oacute; nocturna de persones com a support al seu rescat diürn. Detecci&oacute; de persones i/o elements amb radiobalises.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test12-header" class="accordion_headings" >Cultura</div>
  		<div id="test12-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Gesti&oacute; del Patrimoni</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image39.jpg" alt="" /></td>
                <td>Caracteritzaci&oacute; de l'estat de conservaci&oacute; d'elements del patrimoni cultural i control de les seves alteracions mitjancant el registre de fotografia en alta resoluci&oacute; des de UAVs,</td>
            </tr>
            <tr>
                <td>Arqueologia</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image40.jpg" alt="" /></td>
                <td>Suport a excavacions i prospeccions arqueol&ograve;giques. La fotografia en alta resoluci&oacute; i multiespectral de jaciments millora la documentaci&oacute; i pot aportar informaci&oacute; a l'interpretar respostes de la vegetaci&oacute; a estructures existents sota cota zero</td>
            </tr>
            </table>
 		</div>
        
        <div id="test13-header" class="accordion_headings" >Esports</div>
  		<div id="test13-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Seguiment i enregistrament d'esdeveniments esportius en espais oberts</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image41.jpg" alt="" /></td>
                <td>Seguiment dels esdeveniments esportius. Monitoritzaci&oacute; en temps real de fluxos de persones en esdeveniments esportius. Control d'incid&egrave;ncies.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test14-header" class="accordion_headings" >Seguretat</div>
  		<div id="test14-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servei</th>
                <th>Imatge</th>
                <th>Descripci&oacute;</th>
            </tr>
            <tr>
                <td>Vigilancia alta resoluci&oacute;</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image42.jpg" alt="" /></td>
                <td>En serveis diürns i nocturns, detecci&oacute; de persones i objectes al mar, seguiment d'objectes m&ograve;bils sense possibilitat de detecci&oacute; i sobrev&ograve;l i enregistrament d'infraestructures i situacions ben diverses. CATUAV garanteix la total confidencialitat per a les organitzacions p&uacute;bliques clients, aix&iacute; com pels enc&agrave;rrecs i serveis desenvolupats.</td>
            </tr>
            </table>
 		</div>
			<? break;
			
			case "ing": ?>
            <div id="test-header" class="accordion_headings header_highlight">Cartography</div>
  		<div id="test-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Ortho-Maps</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image1.jpg" alt="" /></td>
                <td>Ortho-Map production for city districts, urban areas, natural parks and agriculture and forestry surfaces.</td>
            </tr>
            <tr>
                <td>Altimetry</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image2.jpg" alt="" /></td>
                <td>Production of High Resolution Terrain Elevation Models. Calculation of the elevation of terrain elements.</td>
            </tr>
            </table>
  		</div>

  		<div id="test1-header" class="accordion_headings" >Agriculture</div>  
  		<div id="test1-content">  
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Management of crops, greenhouses and plantations</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image3.jpg" alt="" /></td>
                <td>Control and monitoring of the state of crops using multispectral images.</td>
            </tr>
            <tr>
                <td>Evaluation and comparison of biophysical parameters</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image4.jpg" alt="" /></td>
                <td>Estimation of biophysical parameters using multispectral images and their temporal variability following the phenological evolution of crops.</td>
            </tr>
            <tr>
                <td>Irrigation Efficiency Control </td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image5.jpg" alt="" /></td>
                <td>The irrigation efficiency is correlated with the normalized vegetation index defined with multispectral images from UAVs. The observations can be programmed following the implemented irrigation policies.</td>
            </tr>
            </table>   
  		</div>

  		<div id="test2-header" class="accordion_headings" >Forestry</div>
  		<div id="test2-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Evolution and monitoring of forests</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image6.jpg" alt="" /></td>
                <td>Monitoring of the state of forestry areas using multispectral images.</td>
            </tr>
            <tr>
                <td>Measurement and evolution of the density and height of the canopy</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image7.jpg" alt="" /></td>
                <td>Quantification of the density, distribution and volume of different forest species using series of mutispectral images together with stereoscopic techniques.</td>
            </tr>
            <tr>
                <td>Evaluation and comparison of biophysical parameters</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image8.jpg" alt="" /></td>
                <td>Estimation of biophysical parameters using multispectral images and their temporal variability following the phenological evolution of each forest species.</td>
            </tr>
            <tr>
                <td>Identification of species in large forest areas</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image9.jpg" alt="" /></td>
                <td>Forest classification using multispectral images and their temporal variability following the seasonal evolution of each species.</td>
            </tr>
            <tr>
                <td>Monitoring of pests and diseases</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image10.jpg" alt="" /></td>
                <td>Identification of plagues and diseases and study of their affectation and evolution using multispectral images and their temporal variability.</td>
            </tr>
            <tr>
                <td>Forest Fires</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image11.jpg" alt="" /></td>
                <td>Forest drought monitoring. Support to the coordination of the extinction tasks. Real Time ground control of the fire evolution. Efficient estimation of perimeter and affectation of burned areas. </td>
            </tr>
            </table>
 		</div>
        
        <div id="test3-header" class="accordion_headings" >Viticulture</div>
  		<div id="test3-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Vineyard Monitoring</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image12.jpg" alt="" /></td>
                <td>Plant by plant monitoring of the vineyard using multispectral images and their temporal variability following the complete phenological cycle.</td>
            </tr>
            <tr>
                <td>Support to precision viticulture practices</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image13.jpg" alt="" /></td>
                <td>By means of series of multispectral images: Plant by plant quantification of the effect of irrigation, application of fertilizers or pesticides, trimmer and cropping, and optimization of these resources. Precise determination of vintage in function of the grape maturity degree.</td>
            </tr>
            <tr>
                <td>Damage Evaluation</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image14.jpg" alt="" /></td>
                <td>Precise quantification (plant by plant) of vineyard damage caused by extreme meteorological or climatic events (hail, frost, winds, etc) or by pests or contamination.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test4-header" class="accordion_headings" >Geology</div>
  		<div id="test4-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Cartography</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image15.jpg" alt="" /></td>
                <td>Multispectral images capture for the production of centimeter resolution geologic maps showing sedimentologic, mineralogic of geophysical information.</td>
            </tr>
            <tr>
                <td>Mining Engineering</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image16.jpg" alt="" /></td>
                <td>Control and monitoring of mineral exploitations and their environmental impact: terrain movements, arid production, metallic waste, decantation dumps, etc.</td>
            </tr>
            <tr>
                <td>Geological Risks: terrain slides, floods, etc.</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image17.jpg" alt="" /></td>
                <td>By means of multispectral images, determination and monitoring at centimeter scales of areas with associated geological risks: terrain slides, floods, etc.</td>
            </tr>
            <tr>
                <td>Geological Risks: snow avalanches </td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image18.jpg" alt="" /></td>
                <td>Characterization of areas presenting snow avalanche risks using multispectral images to determine snow humidity, thermal imagery to measure temperatures and stereoscopic techniques to calculate snow thickness.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test5-header" class="accordion_headings" >Hydrology</div>
  		<div id="test5-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Basin Monitoring</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image19.jpg" alt="" /></td>
                <td>Analysis of the state of river beds and courses, flood risk and estimation of waterflow using multispectral images and stereoscopic techniques.</td>
            </tr>
            <tr>
                <td>Quantification of the capacity of dumps and basins</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image20.jpg" alt="" /></td>
                <td>Quantification and monitoring of dump basins and reservoirs using visible and multiespectral images and stereoscopic techniques.</td>
            </tr>
            <tr>
                <td>Quantification of hydrologic resources</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image21.jpg" alt="" /></td>
                <td>Analysis of water resources in high mountain and estimation of snow melting flows using series of multispectral images.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test6-header" class="accordion_headings" >Environment</div>
  		<div id="test6-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Light Pollution</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image22.jpg" alt="" /></td>
                <td>Parameterization of the light pollution index to elaborate impact maps and to monitoring the efficiency of eco-energetic measures. For this purpose HD video is used. The spectral analysis of the different frames allows the classification and statistics of the different light sources.</td>
            </tr>
            <tr>
                <td>Atmosphere State</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image23.jpg" alt="" /></td>
                <td>Measurement of temperature and pressure tropospheric profiles and aerosol and gases altimetric concentrations using specific sensors: ozone, water vapor, carbon dioxide, etc. Detection of toxic gas and smoke leaks.</td>
            </tr>
            <tr>
                <td>Pollution and toxic waste in aquatic and terrain environments</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image24.jpg" alt="" /></td>
                <td>Control and monitoring of industrial accidents with associated toxic waste in aquatic and terrestrial environments. With the use of different specific spectral bans it is possible to detect the surface presence of certain pollutants.</td>
            </tr>
            <tr>
                <td>Waste Management</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image25.jpg" alt="" /></td>
                <td>Independent monitoring of dumps and deposits of industrial residues and their treatment. The multispestral images allow the quantitative control of the environmental impact of dangerous materials with difficult recycling.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test7-header" class="accordion_headings" >Protected Areas Management</div>
  		<div id="test7-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Landscape Evolution</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image26.jpg" alt="" /></td>
                <td>Elaboration Landscape inventories in natural parks and other protected natural areas. These inventories permit to observe the landscape temporal evolution both in monthly and yearly basis: crop and forest evolution, anthropogenic changes, etc.</td>
            </tr>
            <tr>
                <td>Planning and monitoring of reforestation</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image27.jpg" alt="" /></td>
                <td>Support to the planning and monitoring of reforestation activities and recovery of burned or degraded forests using multispectral images and their temporal</td>
            </tr>
            <tr>
                <td>Day and Night Observation of wild species in open landscapes</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image28.jpg" alt="" /></td>
                <td>The UAVs silent electric propulsion and the onboard charge capacity permits to perform day and night observations of wild species in open landscapes without any interference.</td>
            </tr>
            <tr>
                <td>Impact Evaluations</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image29.jpg" alt="" /></td>
                <td>Studies of environmental impact trails and roads or other infrastructures in protected natural areas using multispectral images from UAVs.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test8-header" class="accordion_headings" >Infrastructures</div>
  		<div id="test8-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Civil work impact evaluation</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image30.jpg" alt="" /></td>
                <td>Analysis of the environmental impact of infrastructures in large areas using visible spectra images captured from low altitude UAV flights.</td>
            </tr>
            <tr>
                <td>Control de obras civiles</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image31.jpg" alt="" /></td>
                <td>Monitoring of the implementation and performance of civil works affecting large areas (roads, channels, dumps, high tension electrical lines, aeolian plants, etc.) using aerial images at centimeter level.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test9-header" class="accordion_headings" >Urbanism</div>
  		<div id="test9-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Urban Planning and Monitoring</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image32.jpg" alt="" /></td>
                <td>Observation and monitoring of urban development. Registration in high resolution images or HD video of the evolution of urban areas under planning.</td>
            </tr>
            <tr>
                <td>Illegal Urban Development Monitoring</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image33.jpg" alt="" /></td>
                <td>Using multispectral and visible images onboard UAVs, it is possible to detect illegal buildings in country and forested areas with difficult access.</td>
            </tr>
            <tr>
                <td>Urban Green Areas Management</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image34.jpg" alt="" /></td>
                <td>Monitoring of urban green areas and characterization of its interaction with near built urban areas using series of multispectral and visible images.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test10-header" class="accordion_headings" >Mobility -Traffic</div>
  		<div id="test10-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Road Mobility Monitoring </td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image35.jpg" alt="" /></td>
                <td>Traffic Monitoring. UAVs are efficient alternatives to helicopters with a high added value due to the suppression of personal risks allowing the digital record of the traffic state. Additionally UAVs are eco-friendly and economic.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test11-header" class="accordion_headings" >Civil Protection</div>
  		<div id="test11-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Centralization and Integration of Control Tasks</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image36.jpg" alt="" /></td>
                <td>The coordination of monitoring and control tasks extended at spatial level but concentrated in time is a critical issue in large mass events. The control and saturation of vehicles, monitoring of people flows, parking status, and any other situation can be easily coordinated and transmitted to the users with the help of UAVs. </td>
            </tr>
            <tr>
                <td>Cultural mass events in open and urban areas </td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image37.jpg" alt="" /></td>
                <td>Monitoring in real time of people mass flows. Control of incidences in urban environments.</td>
            </tr>
            <tr>
                <td>Support to rescue teams</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image38.jpg" alt="" /></td>
                <td>Detection of victims in geologic risk areas: avalanches, earthquakes, land slides, etc. Development of detection system for persons or elements with radio beacons.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test12-header" class="accordion_headings" >Culture</div>
  		<div id="test12-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Cultural Heritage Management</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image39.jpg" alt="" /></td>
                <td>Characterization of the conservation state of cultural heritage elements and control of their alterations using high resolution imagery form UAVs.</td>
            </tr>
            <tr>
                <td>Archeology</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image40.jpg" alt="" /></td>
                <td>Support to archeological prospecting and excavation. High resolution multispectral imagery of archeological sites improves documentation and provides ancillary information useful to interpret the vegetation response to underground ancient structures.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test13-header" class="accordion_headings" >Sports</div>
  		<div id="test13-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Monitoring and registration of sports mass events in open areas</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image41.jpg" alt="" /></td>
                <td>Monitoring of sports mass events. Monitoring in real time of people mass flows. Control of incidences in open areas. </td>
            </tr>
            </table>
 		</div>
        
        <div id="test14-header" class="accordion_headings" >Security</div>
  		<div id="test14-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Service</th>
                <th>Image</th>
                <th>Description/th>
            </tr>
            <tr>
                <td>Surveillance</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image42.jpg" alt="" /></td>
                <td>Detection of persons and vehicles in land and sea during day and night. Undetectable surveillance of mobile targets. Surveillance of infrastructures in different situations. CATUAV assures the highest confidentiality about clients and services.</td>
            </tr>
            </table>
 		</div>
			<? break;
			
			case "esp":
			default: ?>
            <div id="test-header" class="accordion_headings header_highlight">Cartograf&iacute;a</div>
  		<div id="test-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Ortofoto</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image1.jpg" alt="" /></td>
                <td>Realizaci&oacute;n de ortofotomapas de t&eacute;rminos municipales, n&uacute;cleos urbanos, parques naturales y superficies agr&iacute;colas y forestales.</td>
            </tr>
            <tr>
                <td>Altimetr&iacute;a</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image2.jpg" alt="" /></td>
                <td>Realizaci&oacute;n de modelos de elevaciones del terreno de alta resoluci&oacute;n. C&aacute;lculo de alturas de elementos del terreno.</td>
            </tr>
            </table>
  		</div>

  		<div id="test1-header" class="accordion_headings" >Agricultura</div>  
  		<div id="test1-content">  
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Gesti&oacute;n de cultivos, viveros y plantaciones</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image3.jpg" alt="" /></td>
                <td>Control y monitorizaci&oacute;n del estado de los cultivos mediante im&aacute;genes multiespectrales.</td>
            </tr>
            <tr>
                <td>Evaluaci&oacute;n y comparativa de par&aacute;metros biof&iacute;sicos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image4.jpg" alt="" /></td>
                <td>Derivaci&oacute;n de par&aacute;metros biof&iacute;sicos utilizando im&aacute;genes multiespectrales y su variabilidad temporal en funci&oacute;n de la evoluci&oacute;n fenol&oacute;gica de los cultivos.</td>
            </tr>
            <tr>
                <td>Control de la eficiencia de regad&iacute;os</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image5.jpg" alt="" /></td>
                <td>La eficiencia del riego se manifiesta en correlaci&oacute;n positiva con el &iacute;ndice normalizado de vegetaci&oacute;n definido con im&aacute;genes multiespectrales desde UAVs. Les observaciones pueden programarse en funci&oacute;n de les pol&iacute;ticas de riego implementadas.</td>
            </tr>
            </table>   
  		</div>

  		<div id="test2-header" class="accordion_headings" >Forestal</div>
  		<div id="test2-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Evoluci&oacute;n y seguimiento de bosques</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image6.jpg" alt="" /></td>
                <td>Monitorizaci&oacute;n del estado de &aacute;reas boscosas mediante im&aacute;genes multiespectrales.</td>
            </tr>
            <tr>
                <td>Medida y evoluci&oacute;n de densidad y altura del arbolado</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image7.jpg" alt="" /></td>
                <td>Cuantificaci&oacute;n de la densidad, distribuci&oacute;n y volumetr&iacute;a de diferentes especies forestales mediante series de im&aacute;genes multiespectrales y t&eacute;cnicas estereosc&oacute;picas.</td>
            </tr>
            <tr>
                <td>Evaluaci&oacute;n y comparativa de par&aacute;metros biof&iacute;sicos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image8.jpg" alt="" /></td>
                <td>Derivaci&oacute;n de par&aacute;metros biof&iacute;sicos utilizando im&aacute;genes multiespectrales y su variaci&oacute;n en funci&oacute;n de la evoluci&oacute;n fenol&oacute;gica de cada especie.</td>
            </tr>
            <tr>
                <td>Identificaci&oacute;n de especies en grandes extensiones de arbolado</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image9.jpg" alt="" /></td>
                <td>Clasificaci&oacute;n del bosque utilizando im&aacute;genes multiespectrales y su variaci&oacute;n temporal en funci&oacute;n de la evoluci&oacute;n estacional de cada especie.</td>
            </tr>
            <tr>
                <td>Seguimiento y evaluaci&oacute;n de plagas</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image10.jpg" alt="" /></td>
                <td>Identificaci&oacute;n de plagas y estudio de su extensi&oacute;n y evoluci&oacute;n mediante im&aacute;genes multiespectrales y su variaci&oacute;n temporal</td>
            </tr>
            <tr>
                <td>Incendios</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image11.jpg" alt="" /></td>
                <td>Seguimiento del estado de sequ&iacute;a de los bosques. Soporte a la coordinaci&oacute;n de las tareas de extinci&oacute;n. Control de la evoluci&oacute;n de incendios en tiempo real desde tierra. C&aacute;lculo eficiente del per&iacute;metro y de la extensi&oacute;n y del grado de calcinaci&oacute;n del bosque quemado.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test3-header" class="accordion_headings" >Vitivinicultura</div>
  		<div id="test3-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Monitorizaci&oacute;n de vi&ntilde;as</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image12.jpg" alt="" /></td>
                <td>Seguimiento pormenorizado de las vi&ntilde;as, cepa a cepa, utilizando im&aacute;genes multiespectrales y su variaci&oacute;n a lo largo de todo el ciclo fenol&oacute;gico.</td>
            </tr>
            <tr>
                <td>Soporte a la viticultura de precisi&oacute;n</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image13.jpg" alt="" /></td>
                <td>Mediante series de im&aacute;genes multiespectrales: Cuantificaci&oacute;n del efecto, cepa a cepa, de la aplicaci&oacute;n de riego, productos fitosanitarios, podas, etc., y optimizaci&oacute;n de estos recursos. Determinaci&oacute;n precisa del momento de recolecci&oacute;n en funci&oacute;n del estado de maduraci&oacute;n del racimo.</td>
            </tr>
            <tr>
                <td>Cuantificaci&oacute;n de da&ntilde;os</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image14.jpg" alt="" /></td>
                <td>Cuantificaci&oacute;n precisa de vi&ntilde;as afectadas, por incidencias meteorol&oacute;gicas y clim&aacute;ticas extremas (granizo, heladas, viento, sequ&iacute;a, etc) o por plagas y contaminaci&oacute;n.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test4-header" class="accordion_headings" >Geolog&iacute;a</div>
  		<div id="test4-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Cartograf&iacute;a</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image15.jpg" alt="" /></td>
                <td>Captura de im&aacute;genes multiespectrales para la realizaci&oacute;n a resoluci&oacute;n centim&eacute;trica de mapas geol&oacute;gicos sedimentol&oacute;gicos, mineral&oacute;gicos y geof&iacute;sicos.</td>
            </tr>
            <tr>
                <td>Explotaci&oacute;n minera</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image16.jpg" alt="" /></td>
                <td>Control y monitorizaci&oacute;n de explotaciones mineras y su impacto ambiental: movimientos de tierras, producci&oacute;n de &aacute;ridos, residuos met&aacute;licos, balsas de decantaci&oacute;n, etc.</td>
            </tr>
            <tr>
                <td>Riesgos geol&oacute;gicos: corrimientos de tierra, inundaciones, etc</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image17.jpg" alt="" /></td>
                <td>Mediante im&aacute;genes multiespectrales, determinaci&oacute;n y control a escala centim&eacute;trica de &aacute;reas con riesgos geol&oacute;gicos asociados: corrimientos de tierra, inundaciones, etc.</td>
            </tr>
            <tr>
                <td>Riesgos geol&oacute;gicos: aludes</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image18.jpg" alt="" /></td>
                <td>Caracterizaci&oacute;n de zonas con riesgo de aludes utilizando im&aacute;genes multiespectrales para determinar la humedad de la nieve, c&aacute;maras t&eacute;rmicas para determinar su temperatura y t&eacute;cnicas estereosc&oacute;picas para determinar grosores.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test5-header" class="accordion_headings" >Hidrolog&iacute;a</div>
  		<div id="test5-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Control de cuencas</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image19.jpg" alt="" /></td>
                <td>An&aacute;lisis del estado de ocupaci&oacute;n de cauces, riesgo de inundaci&oacute;n y estimaci&oacute;n de caudales utilizando series de im&aacute;genes multiespectrales y t&eacute;cnicas estereosc&oacute;picas. </td>
            </tr>
            <tr>
                <td>Cuantificaci&oacute;n del estado de embalses</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image20.jpg" alt="" /></td>
                <td>Cuantificaci&oacute;n y seguimiento de cuencas y reservas de embalses utilizando im&aacute;genes visibles y multiespectrales con t&eacute;cnicas estereosc&oacute;picas.</td>
            </tr>
            <tr>
                <td>Cuantificaci&oacute;n de reservas hidrol&oacute;gicas</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image21.jpg" alt="" /></td>
                <td>An&aacute;lisis de reservas de agua en alta monta&ntilde;a y estimaci&oacute;n de caudales de deshielo utilizando series de im&aacute;genes multiespectrales.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test6-header" class="accordion_headings" >Medio Ambiente</div>
  		<div id="test6-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Contaminaci&oacute;n lum&iacute;nica</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image22.jpg" alt="" /></td>
                <td>Parametrizaci&oacute;n del &iacute;ndice de contaminaci&oacute;n lum&iacute;nica para elaborar mapas de poluci&oacute;n lum&iacute;nica y monitorizar la eficiencia de medidas ecoenerg&eacute;ticas. Para ello se utiliza video digital de alta definici&oacute;n. El an&aacute;lisis multiespectral de estas im&aacute;genes permite la clasificaci&oacute;n y estad&iacute;stica de les diferentes fuentes de luz.</td>
            </tr>
            <tr>
                <td>Estado de la atm&oacute;sfera</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image23.jpg" alt="" /></td>
                <td>Determinaci&oacute;n de perfiles troposf&eacute;ricos de temperatura y presi&oacute;n y cuantificaci&oacute;n de la concentraci&oacute;n de aerosoles y gases mediante detectores espec&iacute;ficos: ozono, vapor de agua, CO2, etc. Medidas de irradiaci&oacute;n solar y albedo. Detecci&oacute;n de fugues de gases t&oacute;xicos y humos.</td>
            </tr>
            <tr>
                <td>Poluci&oacute;n y vertidos t&oacute;xicos en medio acu&aacute;tico y terrestre</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image24.jpg" alt="" /></td>
                <td>Mediante im&aacute;genes multiespectrales, control y seguimiento de accidentes industriales con vertidos t&oacute;xicos en medios acu&aacute;ticos y terrestres. Con el uso de diferentes bandas espectrales es posible registrar la presencia de determinados contaminantes en superficie.</td>
            </tr>
            <tr>
                <td>Gesti&oacute;n de Residuos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image25.jpg" alt="" /></td>
                <td>Control independiente de &aacute;reas de dep&oacute;sito y almacenaje de residuos industriales y de su tratamiento. Les im&aacute;genes multiespectrales permiten el control cuantitativo del impacto ambiental de materiales peligrosos y de dif&iacute;cil reciclaje. </td>
            </tr>
            </table>
 		</div>
        
        <div id="test7-header" class="accordion_headings" >Gesti&oacute;n Espacios Naturales</div>
  		<div id="test7-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Registro y evoluci&oacute;n del paisaje</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image26.jpg" alt="" /></td>
                <td>Creaci&oacute;n de archivos del paisaje visible en Parques Naturales, PEIN's y otros espacios protegidos. Adem&aacute;s de constituir documentos de registro para el futuro, permiten observar su evoluci&oacute;n a lo largo del ciclo anual y en el tiempo (evoluci&oacute;n cultivos, bosques, modificaciones antropog&eacute;nicas, etc.)</td>
            </tr>
            <tr>
                <td>Planificaci&oacute;n y seguimiento de repoblaciones</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image27.jpg" alt="" /></td>
                <td>Monitorizaci&oacute;n de la repoblaci&oacute;n y recuperaci&oacute;n de bosques quemados o degradados utilizando im&aacute;genes multiespectrales y la su evoluci&oacute;n temporal.</td>
            </tr>
            <tr>
                <td>Observaci&oacute;n diurna y nocturna de fauna en espacios abiertos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image28.jpg" alt="" /></td>
                <td>Los silenciosos motores el&eacute;ctricos y la capacidad de carga de los UAV's permiten efectuar observaciones diurnas y nocturnas de fauna en espacios abiertos sin interferir en su vida.</td>
            </tr>
            <tr>
                <td>Evaluaciones de impacto</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image29.jpg" alt="" /></td>
                <td>Estudios de impacto ambiental de apertura de caminos o de otras infraestructuras en espacios naturales mediante im&aacute;genes multiespectrales desde UAV.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test8-header" class="accordion_headings" >Obra p&uacute;blica</div>
  		<div id="test8-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Evaluaci&oacute;n de impacto</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image30.jpg" alt="" /></td>
                <td>An&aacute;lisis del impacto visual de infraestructuras en grandes extensiones de terreno mediante im&aacute;genes en espectro visible capturadas desde UAVs a baja altura.</td>
            </tr>
            <tr>
                <td>Control de obras civiles</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image31.jpg" alt="" /></td>
                <td>Seguimiento de la implementaci&oacute;n de obres civiles que afectan a grandes extensiones (carreteras, canales, embalses, l&iacute;neas de alta tensi&oacute;n, parques e&oacute;licos, etc.) utilizando im&aacute;genes a&eacute;reas a escales centim&eacute;tricas</td>
            </tr>
            </table>
 		</div>
        
        <div id="test9-header" class="accordion_headings" >Urbanismo</div>
  		<div id="test9-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Gesti&oacute;n y planificaci&oacute;n urban&iacute;stica</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image32.jpg" alt="" /></td>
                <td>Observaci&oacute;n y seguimiento de construcciones. Registro en fotograf&iacute;a de alta resoluci&oacute;n desde diferentes perspectivas o en v&iacute;deo de zonas urbanas vinculadas a proyectos de planificaci&oacute;n.</td>
            </tr>
            <tr>
                <td>Control del desarrollo urban&iacute;stico ilegal</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image33.jpg" alt="" /></td>
                <td>Mediante im&aacute;genes multiespectrales y visibles embarcadas en UAVs, es posible la detecci&oacute;n de construcciones ilegales en zonas rurales, boscosas y de dif&iacute;cil acceso.</td>
            </tr>
            <tr>
                <td>Gesti&oacute;n de &aacute;reas verdes</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image34.jpg" alt="" /></td>
                <td>Seguimiento del estado de zonas verdes y caracterizaci&oacute;n de su interacci&oacute;n con las &aacute;reas urbanas anexas mediante series de im&aacute;genes multiespectrales y visibles.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test10-header" class="accordion_headings" >Movilidad-Tr&aacute;fico</div>
  		<div id="test10-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Monitorizaci&oacute;n de la movilidad por carretera</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image35.jpg" alt="" /></td>
                <td>Seguimiento del estado del tr&aacute;nsito. Es una alternativa eficiente y con valor a&ntilde;adido respecto al helic&oacute;ptero, al suprimir riesgos para el personal que retransmite el estado del tr&aacute;nsito y permite grabar digitalmente la situaci&oacute;n diaria del tr&aacute;fico, adem&aacute;s de ser mucho m&aacute;s econ&oacute;mica y sostenible (no contamina)</td>
            </tr>
            </table>
 		</div>
        
        <div id="test11-header" class="accordion_headings" >Protecci&oacute;n Civil</div>
  		<div id="test11-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Integraci&oacute;n y centralizaci&oacute;n de funciones</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image36.jpg" alt="" /></td>
                <td>La coordinaci&oacute;n de funciones que se extienden en el espacio y se concentran en el tiempo puede constituir una funci&oacute;n cr&iacute;tica en grandes eventos. El control y la saturaci&oacute;n de accesos a pie y con veh&iacute;culo, el seguimiento de flujos de personas, la situaci&oacute;n de aparcamientos y cualquier incidencia puede coordinarse y transmitirse a los usuarios con facilidad con la ayuda de los UAV's</td>
            </tr>
            <tr>
                <td>Manifestaciones culturales en espacios abiertos y n&uacute;cleos urbanos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image37.jpg" alt="" /></td>
                <td>Monitorizaci&oacute;n en tiempo real de flujos de personas. Control de incidencias en medio urbano.</td>
            </tr>
            <tr>
                <td>Soporte a equipos de rescate</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image38.jpg" alt="" /></td>
                <td>Detecci&oacute;n de v&iacute;ctimas en zonas de riesgo geol&oacute;gico: aludes, terremotos, inundaciones, corrimientos de tierra. Desarrollo de sistemas de detecci&oacute;n nocturna de persones como a soporte a su rescate diurno. Detecci&oacute;n de persones y elementos con radiobalizas.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test12-header" class="accordion_headings" >Cultura</div>
  		<div id="test12-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Gesti&oacute;n del Patrimonio</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image39.jpg" alt="" /></td>
                <td>Caracterizaci&oacute;n del estado de conservaci&oacute;n de elementos del patrimonio cultural y control de sus alteraciones mediante el registre de fotograf&iacute;a en alta resoluci&oacute;n desde UAVs,</td>
            </tr>
            <tr>
                <td>Arqueolog&iacute;a</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image40.jpg" alt="" /></td>
                <td>Soporte a excavaciones y prospecciones arqueol&oacute;gicas. La fotograf&iacute;a en alta resoluci&oacute;n y multiespectral de yacimientos mejora la documentaci&oacute;n y puede aportar informaci&oacute;n para interpretar respuestas de la vegetaci&oacute;n a estructuras existentes bajo la superficie.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test13-header" class="accordion_headings" >Deportes</div>
  		<div id="test13-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
               <th>Servicio</th>
               <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Seguimiento y registro de eventos deportivos en espacios abiertos</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image41.jpg" alt="" /></td>
                <td>Seguimiento de eventos deportivos. Monitorizaci&oacute;n en tiempo real de flujos de personas en accesos y en los eventos. Control de incidencias.</td>
            </tr>
            </table>
 		</div>
        
        <div id="test14-header" class="accordion_headings" >Seguridad</div>
  		<div id="test14-content">
    		<table cellpadding="0" cellspacing="1" class="tabla_servicios">
            <tr>
                <th>Servicio</th>
                <th>Imagen</th>
                <th>Descripci&oacute;n</th>
            </tr>
            <tr>
                <td>Vigilancia alta resoluci&oacute;n</td>
                <td><img title="" src="http://www.catuav.com/img/servicios/Image42.jpg" alt="" /></td>
                <td>En servicios diurnos y nocturnos, detecci&oacute;n de persones y objetos en tierra y mar, seguimiento de objetos m&oacute;viles sin posibilidad de detecci&oacute;n y sobrevuelo y registro de infraestructuras y diversas situaciones.<br /><br />CATUAV garantiza la total confidencialidad para las organizaciones p&uacute;blicas clientes, as&iacute; como para los encargos y servicios desarrollados.</td>
            </tr>
            </table>
 		</div>
			<? break;
		} ?>
        </div>
</div>

<?php include("_piecera.php"); ?>