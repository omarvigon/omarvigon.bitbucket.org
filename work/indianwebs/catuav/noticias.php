<?php include("_cabecera.php"); ?>
       
    <div class="contenedor">
    	<div class="cuerpo" style="width:800px;left:95px">
		<h2><?=constant("SEC_NEWS_".$_SESSION["lang"])?></h2>

		<?php
		if($_GET["noticia"])
		{
			include("noticias/".$_GET["noticia"].".php");
			echo "<p><a href='http://www.catuav.com/noticias.php'>".constant("SEC_NEWS2_".$_SESSION["lang"])."</a></p>";
		}	
		else
		{
			switch($_SESSION["lang"])
			{
			case "cat":?>
            <p><a href='noticias.php?noticia=04-03-09'><b>04/03/09</b><br />CATUAV  &eacute;s present a la Fira internacional GLOBALGEO</a></p>
            <p><a href='noticias.php?noticia=27-02-09'><b>25/02/09</b><br />El  Departament d&rsquo;Interior de la Generalitat de Catalunya visita CATUAV</a></p>
            <p><a href='noticias.php?noticia=30-01-09'><b>30/01/09</b><br />Peritatge  de la devastaci&oacute; del vent a Sant Boi de Llobregat</a></p>
            <p><a href='noticias.php?noticia=12-01-09'><b>12/01/09</b><br />CATUAV  en directe a Telemadrid</a></p>
            <p><a href='noticias.php?noticia=05-01-09'><b>05/01/09</b><br />El vol  inaugural de la plataforma <em>Atmos-4</em>, el  millor regal de Reis</a></p>
            <p><a href='noticias.php?noticia=15-12-08'><b>15/12/08</b><br />CATUAV sotmet el seu sistema  UAV al rigor del fred</a></p>
            <p><a href='noticias.php?noticia=25-11-08'><b>25/11/08</b><br />Acord estrat&egrave;gic a 3 anys entre CATUAV i AURENSIS</a></p>
			<p><a href='noticias.php?noticia=18-11-08'><b>18/11/08</b><br />Reportatge de CATUAV a TV3</a></p>
			<p><a href='noticias.php?noticia=24-10-08'><b>24/10/08</b><br />Primer  vol de la c&agrave;mera multiespectral PANOPTE</a></p>
			<p><a href='noticias.php?noticia=26-09-08'><b>26/09/08</b><br />Missi&oacute; a l&rsquo;Espai d&rsquo;Inter&egrave;s Natural Carbass&iacute; per avaluar una plaga en el bosc de Pinus Nigra de 62Ha., en col&middot;laboraci&oacute; amb la Generalitat de Catalunya</a></p>
			<p><a href='noticias.php?noticia=12-09-08'><b>12/09/08</b><br />CATUAV desenvolupa una c&agrave;mera multiespectral</a></p>
			<p><a href='noticias.php?noticia=31-07-08'><b>31/07/08</b><br />ARGOS plus ultra!</a></p>
			<p><a href='noticias.php?noticia=25-07-08'><b>25/07/08</b><br />CATUAV participa en una sessió de treball al Parc Aeroespacial de Viladecans</a></p>
			<p><a href='noticias.php?noticia=27-06-08'><b>27/06/08</b><br />CATUAV es d&oacute;na a con&egrave;ixer en una sessi&oacute; de  treball al Centre Tecnol&ograve;gic Forestal de Catalunya (CTFC)</a></p>
			<p><a href='noticias.php?noticia=23-06-08'><b>20/06/08</b><br />La nova plataforma ARGOS fa el seu primer vol</a></p>
			<p><a href='noticias.php?noticia=23-03-08'><b>23/03/08</b><br />La nova c&agrave;mera VEGCAM-1 ja ha estat provada en  vol</a></p>
			<p><a href='noticias.php?noticia=03-12-07'><b>03/12/07</b><br />La nova plataforma ATOM-4 està llesta per</a></p>
			<p><a href='noticias.php?noticia=04-11-07'><b>04/11/07</b><br />CATUAV finalitza amb &egrave;xit un programa de vols nocturns que ha</a></p>
			<p><a href='noticias.php?noticia=17-10-07'><b>17/10/07</b><br />Es crea l’empresa CATUAV S.L per donar vida pr&ograve;pia</a></p>
			<? break;
			
			case "ing": ?>
            <p><a href='noticias.php?noticia=04-03-09'><b>04/03/09</b><br />CATUAV  attend the international fair GLOBALGEO</a></p>
            <p><a href='noticias.php?noticia=30-01-09'><b>30/01/09</b><br />Catastrophic wind damage evaluation in Sant Boi de Llobregat</a></p>
            <p><a href='noticias.php?noticia=12-01-09'><b>12/01/09</b><br />CATUAV on live in Telemadrid TV</a></p>
            <p><a href='noticias.php?noticia=05-01-09'><b>05/01/09</b><br />Maiden flight of the new Atmos-4 platform</a></p>
            <p><a href='noticias.php?noticia=15-12-08'><b>15/12/08</b><br />CATUAV submits its UAV system to harsh cold</a></p>
            <p><a href='noticias.php?noticia=25-11-08'><b>28/11/08</b><br />Strategic agreement has been signed by CATUAV and AURENSIS</a></p>
			<!--<p><a href='noticias.php?noticia=18-11-08'><b>18/11/08</b><br />Reportaje de CATUAV en TV3</a></p>-->
			<p><a href='noticias.php?noticia=24-10-08'><b>24/10/08</b><br />First  flight of the PANOPTE mutispectral camera</a></p>
			<!--<p><a href='noticias.php?noticia=26-09-08'><b>26/09/08</b><br />Missi&oacute; a l&rsquo;Espai d&rsquo;Inter&egrave;s Natural Carbass&iacute; per avaluar una plaga en el bosc de Pinus Nigra de 62Ha., en col&middot;laboraci&oacute; amb la Generalitat de Catalunya</a></p>-->
			<p><a href='noticias.php?noticia=12-09-08'><b>12/09/08</b><br />CATUAV has completed the development of the PANOPTE multispectral camera</a></p>
			<p><a href='noticias.php?noticia=04-07-08'><b>04/07/08</b><br />CATUAV explores the effects of a hailstorm over the Art&eacute;s vineyards (Pla de Bages)</a></p>
			<p><a href='noticias.php?noticia=27-06-08'><b>27/06/08</b><br />CATUAV was officially presented in a workshop at the Centre Tecnol&ograve;gic Forestal de Catalunya (CTFC)</a></p>
			<p><a href='noticias.php?noticia=20-06-08'><b>20/06/08</b><br />First Argos Platform Flight</a></p>
			<p><a href='noticias.php?noticia=23-03-08'><b>23/03/08</b><br />The new VEGCAM-1 camera has been flight tested</a></p>
			<p><a href='noticias.php?noticia=03-12-07'><b>03/12/07</b><br />The new ATOM-4 platform is ready to begin the flight</a></p>				
			<p><a href='noticias.php?noticia=04-11-07'><b>04/11/07</b><br />CATUAV has successfully completed a night flight program</a></p>
			<p><a href='noticias.php?noticia=17-10-07'><b>17/10/07</b><br />CATUAV S.L. is created to give the activities related to UAVs, which have taken place within</a></p>
			<? break;
		
			default: ?>
            <p><a href='noticias.php?noticia=04-03-09'><b>04/03/09</b><br />Presencia  de CATUAV en la Feria internacional GLOBALGEO</a></p>
            <p><a href='noticias.php?noticia=27-02-09'><b>25/02/09</b><br />El  Departamento de Interior de la Generalitat de Catalu&ntilde;a visita&nbsp;CATUAV</a></p>
            <p><a href='noticias.php?noticia=30-01-09'><b>30/01/09</b><br />Peritaje  de la devastaci&oacute;n del viento en Sant Boi de Llobregat</a></p>
            <p><a href='noticias.php?noticia=12-01-09'><b>12/01/09</b><br />CATUAV  en directo en Telemadrid</a></p>
            <p><a href='noticias.php?noticia=05-01-09'><b>05/01/09</b><br />Vuelo inaugural de la plataforma Atmos-4, el mejor regalo de Reyes</a></p>
            <p><a href='noticias.php?noticia=15-12-08'><b>15/12/08</b><br />CATUAV somete su sistema UAV al  rigor del fr&iacute;o.</a></p>
            <p><a href='noticias.php?noticia=25-11-08'><b>28/11/08</b><br />Acuerdo estrat&eacute;gico entre CATUAV y AURENSIS</a></p>
			<p><a href='noticias.php?noticia=18-11-08'><b>18/11/08</b><br />Reportaje de CATUAV en TV3</a></p>
			<p><a href='noticias.php?noticia=24-10-08'><b>24/10/08</b><br />Primer  vuelo de la c&aacute;mara multiespectral PANOPTE</a></p>
			<p><a href='noticias.php?noticia=26-09-08'><b>26/09/08</b><br />Misi&oacute;n al Espacio de Inter&eacute;s Natural Carbass&iacute; para evaluar una plaga en el  bosque de Pinus Nigra de 62Ha., en colaboraci&oacute;n conla Generalitat de  Catalu&ntilde;a</a></p>
			<p><a href='noticias.php?noticia=12-09-08'><b>12/09/08</b><br />CATUAV desarrolla una c&aacute;mara multiespectral</a></p>
			<p><a href='noticias.php?noticia=31-07-08'><b>31/07/08</b><br />ARGOS plus ultra !</a></p>
			<p><a href='noticias.php?noticia=25-07-08'><b>25/07/08</b><br />CATUAV participa en una sesión de trabajo en el Parque Aeroespacial de Viladecans</a></p>
			<p><a href='noticias.php?noticia=04-07-08'><b>04/07/08</b><br />CATUAV explora los efectos de una granizada sobre los viñedos de Artés (Pla de Bages)</a></p>
			<p><a href='noticias.php?noticia=27-06-08'><b>27/06/08</b><br />CATUAV se presenta en una sesi&oacute;n de trabajo en el Centre  Tecnol&ograve;gic Forestal de Catalunya (CTFC)</a></p>
			<p><a href='noticias.php?noticia=20-06-08'><b>20/06/08</b><br />Primer vuelo de la nueva plataforma ARGOS</a></p>
			<p><a href='noticias.php?noticia=23-03-08'><b>23/03/08</b><br />La nueva c&aacute;mara VEGCAM-1 ya se ha probado en vuelo</a></p>
			<p><a href='noticias.php?noticia=03-12-07'><b>03/12/07</b><br />La nueva plataforma ATOM-4 est&aacute; lista para empezar</a></p>
			<p><a href='noticias.php?noticia=04-11-07'><b>04/11/07</b><br />CATUAV finaliza con &eacute;xito un programa de vuelos nocturnos</a></p>
			<p><a href='noticias.php?noticia=17-10-07'><b>17/10/07</b><br />Se crea la empresa CATUAV S.L. para dar vida propia</a></p>
			<? break;
			}
		} ?>
        </div>
</div>

<?php include("_piecera.php"); ?>