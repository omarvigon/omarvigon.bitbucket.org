<?php include("includes/1cabecera.php");?>

    <title>EVENTOS DEL CAFE DE L'OPERA BARCELONA. Eventos culturales, artisticos, musicales, degustaciones, etc.</title>
    <meta name="description" content="Eventos CAFE DE L'OPERA BARCELONA. Eventos culturales, artisticos, musicales, degustaciones, etc" />
    <meta name="keywords" content="cafe opera barcelona, cafe opera bcn" />
    
<?php include("includes/2cabecera.php");?>

    <div id="cuerpo">
		<h1>EVENTOS DEL CAF&Egrave; DE L'&Ograve;PERA</h1>
        <h4>CULTURALES / POL&Iacute;TICOS</h4>
        <p><b>Ponencias</b><br />Organiza  &quot;Joventuts d&acute;Unio&quot; una vez al mes (car&aacute;cter privado).Para ello, se invitan a los mejores  especialistas en el &aacute;mbito local de cada tema. Traen a nuestro  Caf&egrave; algun pol&iacute;tico de &aacute;mbito nacional.</p>
  		<p><b>Conferencias</b><br />Organiza una  conocida Radio de ambito local &quot;T&eacute; de las 5&quot;, una o  dos&nbsp; veces al mes (abierta al p&uacute;blico). Vienen personajes de &aacute;mbito nacional o internacional que dan una  conferencia sobre alg&uacute;n tema de inter&eacute;s cultural. Temas muy variables.  Literatura, viajes, astrolog&iacute;a, arte, etc.</p>
        
        <h4>ART&Iacute;STICOS</h4>
        <img title="Cafe Opera Barcelona" src="http://www.cafeoperabcn.com/pics/foto04.jpg" alt="Cafe Opera Barcelona" class="imgp" />
		<p><b>Escultura-pintura -Otras expresiones  art&iacute;sticas</b><br /><u>Discusiones y charlas sobre arte </u>
		<br />Organizado por  diversos colectivos de alumnos y ex-alumnos de escuelas de arte. (Car&aacute;cter  privado). En algunos casos  invitan alg&uacute;n artista relevante.</p>
		<p><b>Arquitectura-dise&ntilde;o</b><br /><u>Debates de arquitectura </u><br />Alumnos y grupos de jovenes arquitectos&quot;Nouvell Arquitects&quot; organizan  debates sobre alg&uacute;n arquitecto o tendenicia de arquitectura.  Diapositivas, videos, maquetas ayudan a la exposici&oacute;n de los temas.  Car&aacute;cter privado.</p>
		<p>En algunos casos  se permite la entrada de personas ajenas al grupo (requisito m&iacute;nimo ser  Arquitecto o estudiante de  arquitectura).En otros casos traen  arquitectos de renombre que dan conferencias. (Suele ser una vez al mes). Especialmente  emotiva fue la presencia de Enric Miralles poco antes de su muerte. </p>

		<h4>LITERARIOS</h4>
        <p><b>Tertulias literarias</b><br />Grupo de intelectuales y lectores avezados  debaten sobre temas literarios, libros, etc. Car&aacute;cter privado.</p>
        <p>Peque&ntilde;as presentaciones de libros, as&iacute; como, actos de publicidad y  propaganda de libros. Exclusivo para prensa,&nbsp; medios comunicaci&oacute;n y  invitados. Car&aacute;cter privado</p>
        
        <h4>MUSICALES</h4>
        <img title="Cafe Opera Barcelona" src="http://www.cafeoperabcn.com/pics/foto05.jpg" alt="Cafe Opera Barcelona" class="imgp" />
        <p><b>Opera- Amics del Liceo</b><br />Se reunen de  forma privada para hablar y escuchar Temas de &quot;Opera&quot;. Cantantes,  fragmentos de obras, etc.</p>
        <p><b>M&uacute;sica espa&ntilde;ola</b><br />&quot;Flamenco&quot;  Exhibici&oacute;n de danza&quot;. Conciertos  &quot;Guitarra espa&ntilde;ola&quot;. Acceso libre</p>
        <p><b>M&uacute;sica Cl&aacute;sica. </b><br />Peque&ntilde;os conciertos por estudiantes avanzados  del Liceo.Acceso libre</p>
        <p><b>M&uacute;sica folcl&oacute;rica del pa&iacute;s. En todas sus vertientes</b><br />Can&ccedil;o Catalana.  Habaneras.  Acceso libre</p>
        <p><b>Jazz.</b><br />Diversos grupos  contratados por el Caf&egrave; de l&acute;&Ograve;pera. Acceso libre. Organiza Focus.  Car&aacute;cter privado. Exclusivo para prensa y medios comunicaci&oacute;n. Presentaci&oacute;n para  Catalunya de grupos de Jazz de todo el mundo y peque&ntilde;os conciertos de jazz.</p>
        <p><b>Tango. </b><br />Canto y baile  Exhibiciones. Acceso libre</p>
        <p><b>M&uacute;sica&nbsp;  popular</b><br />Repertorio de  canciones en ingl&eacute;s, franc&eacute;s, italiano, espa&ntilde;ol, etc ampliamente conocidas para amenizar la  estancia a nuestros clientes. Acceso libre.</p>
        <p align="center">
		<img title="Evento musical" src="http://www.cafeoperabcn.com/fotos/musicals1.jpg" alt="Evento musical" />
		<img title="Evento musical" src="http://www.cafeoperabcn.com/fotos/musicals2.jpg" alt="Evento musical" />
		<img title="Evento musical" src="http://www.cafeoperabcn.com/fotos/musicals3.jpg" alt="Evento musical" /></p>
        <p>Nota: En todos  los casos de acceso privado se requiere targeta de invitaci&oacute;n para acceder al  acto.</p>
        
        <h4>CATAS Y DEGUSTACIONES</h4>
		<p><b>1.- </b>El Caf&egrave; de  l&acute;&Ograve;pera de Barcelona dedica cada m&eacute;s a un producto determinado Caf&eacute;, t&eacute;, etc<br />
		Durante ese m&eacute;s organiza una serie de charlas, ponencias,  degustaciones, demostraciones,&nbsp; que nos ayudaran a conocer mejor un  producto alimentario determinado. Para ello se invitan  a los mejores especialistas en el ambito local de cada tema. Acceso libre</p>
        <p align="center">
		<img title="Degustacion en Cafe Opera" src="http://www.cafeoperabcn.com/pics/catas1.jpg" alt="Cata en Cafe Opera Barcelona" />
		<img title="Degustacion en Cafe Opera" src="http://www.cafeoperabcn.com/pics/catas2.jpg" alt="Cata en Cafe Opera Barcelona" /></p>

		<p><b>2.- </b>Adem&aacute;s  durante todo el a&ntilde;o se organizan diversas catas guiadas de bebidas como  el Whisky, el vino, el t&eacute;, etc. Acceso limitado</p>
		<p><b>3.- </b>Participamos  en fiestas locales. Donde se pueden degustar diversos productos propios  &quot;Caf&egrave; &Ograve;pera Gourmet&quot; con car&aacute;cer gratuito.<br />Caf&eacute;s-t&eacute;s-Chocolates-Cervezas-Cocktails-  Vinos-Generosos- Cavas y Champagnes - licores y destilados (  Whisky-Vodka-Ron-Tequila-Brandy-Cognac-Armagnac-Calvados-etc)</p>
		<p>Para m&aacute;s  informaci&oacute;n pongase en contacto con <a href="mailto:info@cafeoperabcn.com">info@cafeoperabcn.com</a></p>
    </div>
    
<?php include("includes/3piecera.php");?>