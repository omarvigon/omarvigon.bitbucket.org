<?php include("includes/1cabecera.php");?>

    <title>CAFE DE L'OPERA BARCELONA La Rambla, 74 08002 BARCELONA Tel: (+34)933 177 585</title>
    <meta name="description" content="CAFE DE L'OPERA BARCELONA La Rambla, 74 08002 BARCELONA Tel (+34) 933 177 585" />
    <meta name="keywords" content="cafe opera barcelona, cafe opera bcn" />
    
<?php include("includes/2cabecera.php");?>

    <div id="cuerpo" class="fondo_home">
		<div>
        <h1>CAF&Egrave; DE L'&Ograve;PERA</h1>
        <h2>La Rambla, 74<br />08002 BARCELONA<br /><span>Horario:</span> 08:30h - 02:30h<br /><span>(Lunes a Domingo)</span><br /><span>Tel&eacute;fono:</span> (+34) 933 177 585</h2>
        <h6>"A su servicio desde 1929"</h6>
        <h5><a title="Localizacion" href="localizacion.php" class="gris">Ver nuestra localizaci&oacute;n en el mapa</a></h5>
        </div>
    </div>
    
<?php include("includes/3piecera.php");?>