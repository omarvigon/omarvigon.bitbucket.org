<?php include("includes/1cabecera.php");?>

    <title>FOTOGRAFIAS Y PINTURAS CAFE DE L'OPERA BARCELONA</title>
    <meta name="description" content="FOTOGRAFIAS Y PINTURAS CAFE DE L'OPERA BARCELONA, pinturas cafe opera" />
    <meta name="keywords" content="cafe opera barcelona, cafe opera bcn" />
    
<?php include("includes/2cabecera.php");?>

    <div id="cuerpo">
		<h1>FOTOGRAFIAS CAF&Egrave; DE L'&Ograve;PERA</h1>

        <h4>Planta Primera (Balconet Opera) - Planta Segunda (Salones Privados)</h4>
        <p align="center">
  		<a href="http://www.cafeoperabcn.com/fotos/act_balconetopera.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_balconetopera_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera"  /></a> 
  		<a href="http://www.cafeoperabcn.com/fotos/act_balconetopera2.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_balconetopera2_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="http://www.cafeoperabcn.com/fotos/act_mirallsalons.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_mirallsalons_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/act_salonsprivats1.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_salonsprivats1_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/act_salonsprivats2.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_salonsprivats2_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/act_vidreressalons.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_vidreressalons_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a></p>
        
        <h4>Planta Baja</h4>
        <p align="center">
  		<a href="http://www.cafeoperabcn.com/fotos/act_vitrinaescala.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_vitrinaescala_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
 		<a href="http://www.cafeoperabcn.com/fotos/act_pbaja1.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_pbaja1_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
  		<a href="http://www.cafeoperabcn.com/fotos/act_pbaja2.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_pbaja2_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/act_pbaja3.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/act_pbaja3_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
  		<a href="http://www.cafeoperabcn.com/fotos/ant_80salonatras.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_80salonatras_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a></p>
  
        <h4>Fotos Antig&uuml;as</h4>
        <p align="center">
        <a href="http://www.cafeoperabcn.com/fotos/ant_30terraza.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_30terraza_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_40mesaterraza.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_40mesaterraza_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_60facana.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_60facana_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_60salonatras.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_60salonatras_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera"/></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_60salonentrada.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_60salonentrada_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_olimpiades.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_olimpiades_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera"/></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_sala1.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_sala1_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/ant_sala2.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ant_sala2_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a>
        <a href="http://www.cafeoperabcn.com/fotos/antigua1.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/antigua1mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/antigua2.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/antigua2mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/antigua3.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/antigua3mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/antigua4.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/antigua4mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/antigua5.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/antigua5mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
        <a href="http://www.cafeoperabcn.com/fotos/antigua6.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/antigua6mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a></p>
        
        <h4>Fotos Ramblas</h4>
        <p align="center">
		<a href="http://www.cafeoperabcn.com/fotos/ramb_liceo40.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ramb_liceo40_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera"/></a> 
		<a href="http://www.cafeoperabcn.com/fotos/ramb_liceo60.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ramb_liceo60_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/ramb_liceo.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ramb_liceo_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/ramb_ocellsprinc.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/ramb_ocellsprinc_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a></p>

        <h4>Pinturas</h4>
        <p align="center">
		<a href="http://www.cafeoperabcn.com/fotos/pint_acuarela_fachada.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_acuarela_fachada_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/pint_acuarella.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_acuarella_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a>
		<a href="http://www.cafeoperabcn.com/fotos/pint_balconettinta.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_balconettinta_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/pint_collagetela.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_collagetela_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/pint_dibujolinea.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_dibujolinea_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera"/></a> 
		<a href="http://www.cafeoperabcn.com/fotos/pint_oleosalo.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_oleosalo_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a> 
		<a href="http://www.cafeoperabcn.com/fotos/pint_salo.jpg" target="_blank"><img src="http://www.cafeoperabcn.com/fotos/pint_salo_mini.jpg" align="absmiddle" alt="Foto Cafe de la Opera" /></a></p>
        
        <h4>Fotografias para descargar</h4>
        <ul> 
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/132-fotocolor-salon-atras-anos-80.zip" target="_blank">Fotocolor salon atras anos80 <i>4.20MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/S8000051.zip" target="_blank">Cafe Opera Bcn <i>1.60MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/entrada-cafe.zip" target="_blank">Entrada cafe  <i>4.95MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/45980008.zip" target="_blank">Cafe Opera Bcn  <i>600KB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/45980002.zip" target="_blank">Cafe Opera Bcn <i>522KB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/231-salons-privats-darrera-2.zip" target="_blank">Salons privats darrera <i>7.78MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/229-balconet-opera-davant.zip" target="_blank">Balconet opera davant <i>2.41MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/196-pintures-acuarella-facana-90th.zip" target="_blank">Pintures acuarella facana <i>2.96MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/194-pintures-balconet-tinta-bueno.zip" target="_blank">Pintures balconet tinta <i>4.54MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/193-pintures-salo-davant-bueno.zip" target="_blank">Pintures salo davant  <i>7.04MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/168-foto-facana-anys-60-avi.zip" target="_blank">Foto facana anys60 <i>6.50MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/165-pintures-facana-acuarella-palou.zip" target="_blank">Pintures acuarella  <i>9.10MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/161-foto-pintura-terraca.zip" target="_blank">Foto pintura terraza  <i>2.06MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/159-foto-pintura-interior.zip" target="_blank">Foto pintura interior <i>4.20MB</i></a></li>
        <li><a href="http://www.cafeoperabcn.com/pics/descargar/152-foto-pintura-interior.zip" target="_blank">Foto pintura interior <i>1.78MB</i></a></li>
        </ul>
    </div>
    
<?php include("includes/3piecera.php");?>