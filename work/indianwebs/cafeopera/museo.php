<?php include("includes/1cabecera.php");?>

    <title>MUSEO CAFE DE L'OPERA BARCELONA</title>
    <meta name="description" content="MUSEO CAFE DE L'OPERA BARCELONA, Vinos, Licores, Brandys, Rones, Tequilas, Whiskys, Ginebras" />
    <meta name="keywords" content="cafe opera barcelona, cafe opera bcn" />
    
<?php include("includes/2cabecera.php");?>

    <div id="cuerpo">
		<h1>MUSEO CAF&Egrave; DE L'&Ograve;PERA</h1>

        <h4>Productos adquiridos antes de 1990</h4>
        <div align="center">
        <img src="http://www.cafeoperabcn.com/pics/1990_licors.jpg" alt="Licores" /><br />
        <img src="http://www.cafeoperabcn.com/pics/1985_licors1.jpg" alt="Licores" /><br />
        <img src="http://www.cafeoperabcn.com/pics/1985_licors2.jpg" alt="Licores" />
        </div>
                
        <h4>Productos adquiridos desde 1990 hasta 2005</h4>
        <p>Brandy-Cognac-Armagnac-Calvados<br />
        <img src="http://www.cafeoperabcn.com/fotos/brandy1.jpg" alt="Brandy" />
        <img src="http://www.cafeoperabcn.com/fotos/brandy2.jpg" alt="Brandy" />
        <img src="http://www.cafeoperabcn.com/fotos/brandy3.jpg" alt="" /></p>
        <p>Jerez-Port-Madeira-Tokaji <span style="padding:0 0 0 200px;">Canadiense</span><br />
        <img src="http://www.cafeoperabcn.com/fotos/jerez.jpg" alt="Jerez" />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_canadiense.jpg" alt="Whisky Canadiense" /></p>
        <p>Licors Altres<br />
        <img src="http://www.cafeoperabcn.com/fotos/licores.jpg" alt="Licores" /></p>
        <p>Marc-Pisco-Bagaceira-Orujo-Grappa<br />
        <img src="http://www.cafeoperabcn.com/fotos/marc1.jpg" alt="Orujo" />
        <img src="http://www.cafeoperabcn.com/fotos/marc2.jpg" alt="Orujo" />
        <img src="http://www.cafeoperabcn.com/fotos/marc3.jpg" alt="Orujo" /></p>
        <p>Ron(Rum)<br />
        <img src="http://www.cafeoperabcn.com/fotos/rones1.jpg" alt="Ron" /><br />
        <img src="http://www.cafeoperabcn.com/fotos/rones2.jpg"alt="Ron"  /></p>
        <p>Vins(Wines)-Caves-Champagnes<br />
        <img src="http://www.cafeoperabcn.com/fotos/vinos.jpg" alt="Vinos" /></p>
        
        <table cellspacing="2" align="center">
        <tr>
        	<td valign="bottom"><img src="http://www.cafeoperabcn.com/pics/2000ginebra.jpg" alt="Ginebra" /></td>
            <td valign="bottom"><img src="http://www.cafeoperabcn.com/pics/2000vodka.jpg" alt="Vodka" /></td>
            <td valign="bottom"><img src="http://www.cafeoperabcn.com/pics/2000tequilasauza.jpg" alt="Tequila" /></td>
            <td valign="bottom"><img src="http://www.cafeoperabcn.com/pics/2000tequilacuervo.jpg" alt="Tequila" /></td>
            <td valign="bottom"><img src="http://www.cafeoperabcn.com/pics/2000tequilaherradura.jpg" alt="Tequila" /></td>
        </tr>
        <tr>
        	<td align="center">Vodka-Gin-Tequila-etc</td>
            <td align="center">Vodka</td>
            <td align="center">Tequila Sauza</td>
            <td align="center">Tequila Cuervo</td>
            <td align="center">Tequila Herradura</td>
        </tr>
        </table>
        
        <p>Whisky<br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_blend1.jpg" alt="Whisky" /><br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_blend2.jpg" alt="Whisky" /><br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_blend3.jpg" alt="Whisky" /><br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_blend4.jpg" alt="Whisky" /><br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_bourbon1.jpg" alt="Whisky" /></p>
        <p>Bourbon<br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_bourbon2.jpg" alt="Bourbon" /></p>
        <p>Irlandes<br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_irlandes.jpg" alt="Whisky" /></p>
        <p>Japon&eacute;s<br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_japones.jpg" alt="Whisky" /></p>
        <p>Malta<br />
        <img src="http://www.cafeoperabcn.com/fotos/whisky_malta.jpg" alt="Whisky" /></p>
        
        <h4>Productos adquiridos despues del 2005</h4>
        <p>Vajilla y Mobiliario<br /><img src="http://www.cafeoperabcn.com/pics/mobiliario.jpg" alt="Vajilla y mobiliario" /></p>
    </div>
    
<?php include("includes/3piecera.php");?>