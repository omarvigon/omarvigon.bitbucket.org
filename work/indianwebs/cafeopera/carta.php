<?php include("includes/1cabecera.php");?>

    <title>CARTA DEL CAFE DE L'OPERA BARCELONA. Especialidades, vinos y cavas, infusiones, aguardientes, etc.</title>
    <meta name="description" content="Carta del CAFE DE L'OPERA BARCELONA. Cafes, Granizados, Sangria, Helados, Vinos, Infusiones, Tes, etc." />
    <meta name="keywords" content="cafe opera barcelona, cafe opera bcn, carta del cafe opera" />
    
<?php include("includes/2cabecera.php");?>

    <div id="cuerpo" class="fondo1">
		<h1>CARTA DEL CAF&Egrave; DE L'&Ograve;PERA</h1>
        <h4>Carta 1</h4>
        <ul>
          <li> Caf&eacute; y combinados con caf&eacute;</li>
          <li> Brandy</li>
          <li> Selecci&oacute;n  de whiskies</li>
          <li> Selecci&oacute;n de t&eacute;s</li>
          <li> Chocolates y vainillas</li>
          <li> Tartas </li>
        </ul>
        <h4>Carta 2</h4>
        <ul>
          <li>Sangria</li>
          <li>Seleccion de vinos, champagnes y cavas, aguardientes</li>
          <li>Granizados </li>
          <li>Helados y batidos </li>
          <li>Cervezas </li>
          <li>Tapas y bocadillos</li>
        </ul>
        <h4>Carta 3 : Vinos y cavas</h4>
        <ul>
          <li>Vinos jovenes</li>
          <li>Cavas y champagnes</li>
          <li>Generosos </li>
          <li>Port  y Madeira</li>
          <li>Vinos &rdquo;Gran Reserva&rdquo; nacionales y de importaci&oacute;n. </li>
        </ul>
        <h4>Carta 4 : Infusiones</h4>
        <ul>
          <li>T&eacute;s aromatizados de importaci&oacute;n exclusiva</li>
          <li>T&eacute;s del mundo (m&aacute;s de 300) </li>
          <li>Pastas de t&eacute; (de venta s&oacute;lo en&rdquo; Balconet de l&acute;&Ograve;pera&rdquo;) </li>
        </ul>
        <h4>Carta  5 - 5A - 5B : Aguardientes</h4>
        <ul>
          <li> Uva (Brandy, Cognac, Armagnac, Orujo, Grappa, Marc, Pisco, etc...)</li>
          <li> Bayas (Gin) </li>
          <li> Manzana (Calvados, etc...) </li>
          <li> Maguey (Tequila)</li>
          <li> Naranja</li>
          <li> Ca&ntilde;a (Ron, Cachaza) </li>
          <li> Arroz (Sake, Mao Tai, etc...)</li>
          <li> Anisados </li>
          <li> Cereales</li>
          <li> Vodka y Whisky (m&aacute;s de 1300 botellas diferentes) </li>
          <li> Licores, etc... </li>
        </ul>
        <h4>Carta 6 : Balconet de l'&Ograve;pera</h4>
        <ul>
          <li> Carta 1 + Carta 2 + Productos de venta exclusiva en salones Adic.</li>
          <li>Chocolates (Amazonico-Trinitario-Forastero-Criollo-Aromatizados)</li>
          <li>Caf&eacute;s (Selecci&oacute;n de las mejores plantaciones del mundo)</li>
          <li>Cr&ecirc;pes</li>
        </ul>
        <h4>Cartas  Especiales</h4>
        <ul>
          <li>Mes de la cerveza (m&aacute;s de 200 botellas diferentes, algunas de importaci&oacute;n exclusiva)</li>
          <li>Mes del whisky (4.500 botellas exposici&oacute;n /   2.900 botellas degustaci&oacute;n) </li>
        </ul>
        
        <h4>Productos &Oacute;pera Gourmet</h4>
		<p align="center"><img src="http://www.cafeoperabcn.com/pics/opera_bombons2.jpg" alt="Bombones" />
		<img src="http://www.cafeoperabcn.com/pics/opera_te2.jpg" alt="Te" />
		<img src="http://www.cafeoperabcn.com/fotos/cartas1.jpg" alt="Carta" />
		<img src="http://www.cafeoperabcn.com/fotos/cartas2.jpg" alt="Carta" /></p>
    </div>
    
<?php include("includes/3piecera.php");?>