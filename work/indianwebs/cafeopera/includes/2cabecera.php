    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="includes/_estilos.css" />
    <script language="javascript" type="text/javascript" src="includes/_funciones.js"></script>
</head>

<body>
<div id="arriba">
    <a title="Castellano" href="#"><img src="img/bandera_es.gif" alt="Castellano" /></a>
    <a title="Catala" href="#"><img src="img/bandera_cat.gif" alt="Catala" /></a>
    <a title="English" href="#"><img src="img/bandera_uk.gif" alt="English" /></a>
</div>
<div id="centro">
	<div id="boton">
    	<a title="Inicio" href="http://www.cafeoperabcn.com/"><img src="img/cafeopera-logo.gif" alt="Cafe Opera Bcn" /></a>
    	<a title="Historia" href="http://www.cafeoperabcn.com/historia.php">&raquo; Historia</a>
        <a title="Carta" href="http://www.cafeoperabcn.com/carta.php">&raquo; Carta</a>
        <a title="Reservas" href="http://www.cafeoperabcn.com/reservas.php">&raquo; Reservas</a>
        <a title="Especialidades" href="http://www.cafeoperabcn.com/especialidades.php">&raquo; Especialidades</a>
        <a title="Eventos" href="http://www.cafeoperabcn.com/eventos.php">&raquo; Eventos</a>
        <a title="Fotos y pinturas" href="http://www.cafeoperabcn.com/fotografias.php">&raquo; Fotograf&iacute;as y pinturas</a>
        <a title="Museo de botellas" href="http://www.cafeoperabcn.com/museo.php">&raquo; Museo de botellas</a>
        <a title="Loalizacion" href="http://www.cafeoperabcn.com/localizacion.php">&raquo; Localizaci&oacute;n</a>
        <a title="Contactar" href="http://www.cafeoperabcn.com/contactar.php">&raquo; Contactar</a>
        <br />
    </div>