<?php include("includes/1cabecera.php");?>

    <title>RESERVAS CAFE DE L'OPERA BARCELONA. Haz tu reserva en el Cafe de la Opera Bcn</title>
    <meta name="description" content="Haz tu reserva en el CAFE DE L'OPERA BARCELONA" />
    <meta name="keywords" content="cafe opera barcelona, cafe opera bcn" />
    
<?php include("includes/2cabecera.php");?>

    <div id="cuerpo">
		<h1>RESERVAS CAF&Egrave; DE L'&Ograve;PERA</h1>
        <h2>Salones para  eventos, fiestas privadas, aniversarios, etc... con bebidas y servicio de camareros inclu&iacute;do</h2>
        <ul>
        <li>Servicio de Desayunos</li>
        <li>Aperitivos</li>
        <li>Pousse Caf&eacute;</li>
        <li>Royal tea</li>
        <li>Meriendas</li>
        <li>Lunch</li>
        <li>etc...</li>
        </ul>    
        <p>Contactar con: Sr. Jose Angel<br />Tel.: (0034) 933 177 585 (08:30 h - 16:30 h) - Fax: (0034) 933 024 032 <br /><a href="mailto:info@cafeoperabcn.com">info@cafeoperabcn.com</a></p>
        <p>En la medida de lo posible, procure concretar la  reserva con un m&iacute;nimo de 15 dias de antelaci&oacute;n.</p>
        <p align="center"><img title="Cafe Opera" src="http://www.cafeoperabcn.com/pics/reservas3.jpg" alt="Reserva en Cafe Opera"/></p>
    </div>
    
<?php include("includes/3piecera.php");?>