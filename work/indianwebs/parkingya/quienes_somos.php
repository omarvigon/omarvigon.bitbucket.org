<?php include("1cabecera.php");?>
  
  </div><!-- fin #cabecera -->
  
  <div id="contenido">
    
      <div id="texto">
        <h1 class="negro">Quienes somos</h1>
        <br  />
        <h2>Parking Ya!, tus parkings en Barcelona</h2>
        <br />
        <h3>La compra-venta de parkings en Barcelona centra la actividad de Parking Ya!, un equipo de profesionales que trabaja para ofrecer al p&uacute;blico las ofertas m&aacute;s atractivas en plazas de estacionamiento. </h3>
        <p>Parking Ya! es la empresa l&iacute;der en la gesti&oacute;n de parkings en Barcelona, un servicio especializado que nos ha permitido asesorar al cliente de forma experta y con plena dedicaci&oacute;n, poniendo sobre la mesa las alternativas que se adaptan a aquello que desea. <br />
          <br />
          Tras este innovador proyecto de compra y venta de parkings en Barcelona, se halla un grupo de cualificados profesionales a los que nos avalan m&aacute;s de diez a&ntilde;os de experiencia en el &aacute;mbito inmobiliario, viviencias que han sido de gran ayuda para enfrentarnos a esta novedosa iniciativa. <br />
  <br />
          A esta alta especializaci&oacute;n en la gesti&oacute;n de parkings en Barcelona, se une un equipo joven y emprendedor que pone todo su conocimiento en beneficio de sus clientes, y el respaldo de importantes grupos inversores, una combinaci&oacute;n de factores clave para situarnos hoy como referente en el sector del parkings en Barcelona. <br />
  <br />
          En Parking Ya!, l&iacute;deres de gesti&oacute;n de parkings en Barcelona, nuestros clientes van desde los particulares que desean vender una plaza de parking hasta grupos inversores que quieren desprenderse de lotes enteros de plazas. <br />
  <br />
          El equipo de Parking Ya! trata de presentar parkings en Barcelona, Santa Coloma, Badalona, Sant Adri&agrave; y L'Hospitalet, una vez realizado un an&aacute;lisis en el que se examinan las caracter&iacute;sticas de cada parking, bas&aacute;ndonos en los requerimientos y deseos del cliente. <br />
  <br />
          Este trabajo es clave para dar con la mejor oferta de parkings en Barcelona que ayude a que el cliente obtenga la m&aacute;xima satisfacci&oacute;n con el servicio y el asesoramiento brindado. <br />
  <br />
          Parking Ya!, tu gestor en la b&uacute;squeda de parkings en Barcelona, te espera en:<br />
  <br />
          Pau Claris 97, ppal. 1&ordf;. 08009 Barcelona. T: 93 318 02 64. </p>
        <h3 class="negro">&nbsp;</h3>
      </div>
      <!-- fin #texto -->
      <div id="columna_derecha">
      	<h2 align="center">Encuentra tu parking en Barcelona mediante el mapa</h2>
        <p align="center"><a href="ver_parkings.php"><img src="images/mini-mapa.gif" alt="Encuentra tu parking en Barcelona mediante el mapa" width="198" height="156" border="0" /></a></p></div>
      <!-- fin #columna_derecha -->
<?php include("2piecera.php");?>