<?php include("includes/0cabecera.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Parking</title>
<style type="text/css">
td		{vertical-align:top;font-family:Arial, Helvetica, sans-serif;color:#000000;font-weight:bold;}
th		{vertical-align:top;font-family:Arial, Helvetica, sans-serif;color:#ffffff;background:#9797d7;text-align:left;}

a		{color:#cccccc;}
label	{padding:8px 64px;background:#9797d7;color:#ffffff;}
</style>
</head>

<body>
<? if($_GET["id"])
{
	$resultado=ejecutar("select parkings.*,comerciales.nombre as nombrecomercial from parkings,comerciales where parkings.id='".$_GET["id"]."' and parkings.id_comercial=comerciales.id");
	if($resultado)
	{
		$fila=mysql_fetch_assoc($resultado);
		$resultado2=ejecutar("select nombre,telefono from clientes where id='".$fila["id_cliente"]."' ");
		$fila2=mysql_fetch_assoc($resultado2);
	} ?>

        <table cellpadding="5" cellspacing="3" width="1000">
        <tr>
			<td><img src="img/logo.gif" alt="ParkingYa" /></td>
            <td><a href="ficha.php?id=<?=(($_GET["id"])-1)?>"><< Anterior</a></td>
            <td align="center">Referencia <label><?=$fila["id"]?></label></th>
            <td><a href="ficha.php?id=<?=(($_GET["id"])+1)?>">Siguiente >></a></td>
        </tr>
        </table>
        
        <table cellpadding="5" cellspacing="3" width="1000">
        <tr>
        	<th style="width:25%;">Direcci&oacute;n:</th>
            <th style="width:25%;">Cruce Calles:</th>
        </tr>  
        <tr>  
			<td style="border:1px solid #0066CC;"><?=$fila["direccion"]?></td>
            <td style="border:1px solid #0066CC;"><?=$fila["direccion2"]?></td>
        </tr>
        <tr>
        	<th>Comercial:</th>
            <th>Codigo Postal:</th>
        </tr>  
        <tr>  
			<td style="border:1px solid #0066CC;"><?=$fila["nombrecomercial"]?></td>
            <td style="border:1px solid #0066CC;"><?=$fila["cp"]?></td>
        </tr>
        <tr>
        	<th>Estado:</th>
            <th>Fecha Alta:</th>
        </tr>  
        <tr>  
			<td style="border:1px solid #0066CC;"><?=constant("ESTADO_".$fila["estado"])?></td>
            <td style="border:1px solid #0066CC;"><?=$fila["fecha_alta"]?></td>
        </tr>
        </table>
        
        <br /><br />
        
        <table cellpadding="5" cellspacing="3" width="1000">
        <tr>
        	<th>Propietario:</th>
            <td style="width:35%;background:#cccccc;"><?=$fila["propietario_nombre"]?></td>
            <th>Direcci&oacute;n:</th>
            <td style="width:35%;background:#cccccc;"><?=$fila["propietario_direccion"]?></td>
        </tr>
        <tr>
        	<th>Telefonos:</th>
            <td style="background:#cccccc;"><?=$fila["propietario_telefono"]?></td>
            <th>Email:</th>
            <td style="background:#cccccc;"><?=$fila["propietario_email"]?></td>
        </tr>
        </tr>
        </table>
        
        <br /><br />
        
        <table cellpadding="5" cellspacing="3" style="border:1px solid #000000;" width="1000">
        <tr>
        	<th>Medida</th>
            <th>Planta</th>
            <th>Plaza</th>
            <th colspan="3">Tipo Coche</th>
        </tr>
        <tr>
        	<td><?=$fila["medida"]?></td>
            <td><?=$fila["planta"]?></td>
            <td><?=$fila["plaza"]?></td>
            <td colspan="3"><?=$fila["tipocoche"]?></td>
        </tr>
        <tr>
        	<th>Comunidad</th>
            <th>I.B.I.</th>
            <th>Ascensor</th>
            <th>Llaves</th>
            <th>Captacion</th>
            <th>Captador</th>
        </tr>
        <tr>
        	<td><?=$fila["gastos_comunidad"]?></td>
            <td><?=$fila["gastos_ibi"]?></td>
            <td><?=constant("SI_NO_".$fila["ascensor"])?></td>
            <td><?=constant("LLAVES_".$fila["llaves"])?></td>
            <td><?=$fila["captacion"]?></td>
            <td><?=$fila["captador"]?></td>
        </tr>
        </table>
        
        <br /><br />
        
        <table cellpadding="5" cellspacing="3" width="1000">
        <tr>
        	<td style="background:#cccccc;" align="right"><?=$fila["precio_propietario"]?> &euro;</td>
        	<th style="width:25%;">Precio Propietario</th>
            <th style="width:25%;">Precio ParkingYa!</th>
            <td style="background:#cccccc;" align="left"><?=$fila["precio_parkingya"]?>  &euro;</td>
        </tr>
        </table>
        
        <br /><br />
        
        <table cellpadding="5" cellspacing="3" width="1000">
        <tr>
        	<td style="height:250px;border:1px solid #0066CC;"><label>Notas publicas</label><br /><br /> <?=$fila["notas_publicas"]?></td>
        </tr>
        <tr>
        	<td style="height:250px;border:1px solid #0066CC;"><label>Notas privadas</label><br /><br /> <?=$fila["notas_privadas"]?></td>
        </tr>
        </table>
<? }?>
</body>
</html>
