<? include("panel/includes/0cabecera.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Garaje en Barcelona Parkings en Barcelona Compra de garajes en Barcelona | Parking Ya!</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="css/funciones.js"></script>
</head>

<body>
<div id="contenedor">
<div id="contenedor_fondo">

  <div id="cabecera">
  
    <div id="logotipo"><a href="http://www.parkingya.es"><img src="images/logotipo_parkingya.jpg" alt="Parking Ya !" width="219" height="100" border="0" /></a></div><!-- fin #logotipo -->
    <div id="copyright">&copy; ParkingYa.es 2006-2009. Parkings en Barcelona</div><!-- fin #telefono_email -->
    <br class="clearfloat" />
    
    <div id="menu_principal">
      <div class="boton_menu_principal"><a title="Ver parkings" href="ver_parkings.php">Ver parkings</a></div>
      <div class="boton_menu_principal"><a title="Qui&eacute;nes somos" href="quienes_somos.php">Qui&eacute;nes somos</a></div>
      <div class="boton_menu_principal"><a title="Trabaja con nosotros" href="trabaja_con_nosotros.php">Trabaja con nosotros</a></div>
      <div class="boton_menu_principal"><a title="Compra de garajes en Barcelona" href="compra_de_garajes_en_barcelona.php">Compra de garajes</a></div>
      <div class="boton_menu_principal"><a title="Visita nuestro blog" href="http://parkingya.blogspot.com" target="_blank">Visita nuestro blog</a></div>
      <div class="boton_menu_principal"><a title="Grupo Big-House" href="http://www.big-house.net" target="_blank">Grupo Big-House</a></div>
    </div><!-- fin #menu_principal -->
