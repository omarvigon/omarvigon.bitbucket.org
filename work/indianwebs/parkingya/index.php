<?php include("1cabecera.php");?>
  
  <div id="banner">
  
  	<div id="mensajes">
    	<br />
  	  	<h1 class="blanco">Compramos tu plaza de parking</h1><br />
  	  	<h2 class="azul_claro">La soluci&oacute;n r&aacute;pida y eficaz <br />para vender tu plaza de parking</h2><br />
      	<h2 class="azul_claro">Tambi&eacute;n lotes y garajes enteros</h2><br />
        <p><i>Llama ahora al <span class="telefono_banner">93 318 02 64</span> o env&iacute;anos tus datos</i></p>
         <a title="Enviar datos" href="javascript:popup2();"><img src="images/boton_enviar_datos.gif" alt="Enviar datos" width="150" height="38" border="0" title="Enviar datos" /></a>
  	</div><!-- fin #mensajes -->
    <div id="foto_banner"><img src="images/foto_banner.jpg" width="536" height="252" alt="Imagen Publicidad Parking Ya !" /></div>
  
  </div><!-- fin #banner -->
  
  </div><!-- fin #cabecera -->
  
  <div id="contenido">
    
      <div id="columna_izquierda">
        <h3 class="negro">Parking Ya!, tus parkings en Barcelona</h3>
        <p>La <strong>compra-venta</strong> de <strong>parkings en Barcelona</strong> centra la actividad de <strong>Parking Ya!</strong>, un equipo de profesionales que trabaja para ofrecer al público las ofertas más atractivas en <strong>plazas</strong> de <strong>estacionamiento</strong>. </p>
        <p><strong>Parking Ya!</strong> es la empresa líder en la <strong>gestión</strong> de <strong>parkings</strong> en <strong>Barcelona</strong>, un servicio especializado que nos ha permitido asesorar al cliente de forma experta y con plena dedicación, poniendo sobre la mesa las alternativas que se adaptan a aquello que desea. </p>
        <p>Tras este innovador proyecto de <strong>compra y venta</strong> de <strong>parkings en Barcelona</strong>, se halla un grupo de cualificados profesionales a los que nos avalan más de diez años de experiencia en el ámbito inmobiliario, viviencias que han sido de gran ayuda para enfrentarnos a esta novedosa iniciativa.
      </p>
        
        <br class="clearfloat" />
        <h3 class="negro">Parkings / Garajes destacados:</h3>
        <br />
        <table cellpadding="2" class="listado">
        <tr>
            <th>Ref.</th>
            <th>CP</th>
            <th>Parkings / Garajes en Barcelona</th>
            <th>Planta</th>
            <th>Dimensiones</th>
            <th>Precio</th>
            <th width="220">Detalles</th>
            <th>&nbsp;</th>
        </tr>
            
        <? 
        $resultado=ejecutar("select id,cp,direccion2,medida,planta,precio_parkingya,notas_publicas from parkings where estado='2' ");
        if(mysql_num_rows($resultado)>0)
		{
            while($fila=mysql_fetch_assoc($resultado))
            { 
                $texto.='<tr>
                  <td><strong>'.$fila["id"].'</strong></td>
				  <td>'.$fila["cp"].'</td>
				  <td><strong>'.$fila["direccion2"].'</strong></td>
                  <td>'.$fila["planta"].'</td>
                  <td>'.$fila["medida"].'</td>
                  <td>'.$fila["precio_parkingya"].' &euro;</td>
				  <td>'.$fila["notas_publicas"].'</td>
                  <td><a title="Informacion" href="javascript:popup('.$fila["id"].')">[+] Info</a></td>
                </tr>';
			 }
			 echo $texto; 
		}	
		?>
        </table>
      
      </div><!-- fin #columna_izquierda -->
      
      <div id="columna_derecha">
			<h2 align="center">Encuentra tu parking en Barcelona mediante el mapa</h2>
        	<p align="center"><a href="ver_parkings.php"><img src="images/mini-mapa.gif" alt="Encuentra tu parking en Barcelona mediante el mapa" width="198" height="156" border="0" /></a></p>
      
      </div>
    
<?php include("2piecera.php");?>
