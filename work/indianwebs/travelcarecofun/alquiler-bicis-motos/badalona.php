<?php 
$titulo="Alquiler de motos y bicicletas el&eacute;ctricas en Badalona";
include("../cabecera.php")?>
		
<tr>
      <td class="fila_izq">
		<?php include("00bt_tiendas.php"); ?>
      </td>
      <td class="fila_der">	
      	<h1>Alquiler de motos y bicis el&eacute;ctricas en Badalona</h1>
        <p><b>YA ES  POSIBLE EN BADALONA <em>Tfn 933.992.581</em></b></p>
		<p>
        <a href="javascript:anuncio(1)" title="Travelcar Ecofun en la radio"><img src="http://www.travelcarecofun.com/img/icon_audio.gif" border="0" align="absmiddle"> <b>Escucha el anuncio1 de Radio Pomar (101.2FM) de Travel Car Eco Fun!</b></a><span id="cajaudio1">&nbsp;</span><br>
        <a href="javascript:anuncio(2)" title="Travelcar Ecofun en la radio"><img src="http://www.travelcarecofun.com/img/icon_audio.gif" border="0" align="absmiddle"> <b>Escucha el anuncio2 de Radio Pomar (101.2FM) de Travel Car Eco Fun!</b></a><span id="cajaudio2">&nbsp;</span><br>
        <a href="javascript:anuncio(3)" title="Travelcar Ecofun en la radio"><img src="http://www.travelcarecofun.com/img/icon_audio.gif" border="0" align="absmiddle"> <b>Escucha el anuncio3 de Radio Pomar (101.2FM) de Travel Car Eco Fun!</b></a><span id="cajaudio3">&nbsp;</span><br>
        </p>
        <p>Travelcar ECOFUN inicia su actividad en BADALONA por su importante nivel tur&iacute;stico, con un servicio de alquiler de bicicletas y motos el&eacute;ctricas no contaminantes en la playa y la costa.</p>        
        <ul>
        <li>Alquiler de bicicletas el&eacute;ctricas y tradicionales por horas&nbsp; a partir de&nbsp; 1,50 &euro;  la hora</li>
        <li>Alquiler de motos el&eacute;ctricas por horas  utiliz&aacute;ndolas solo lo necesario&nbsp; y a un  precio inmejorable 8,00 &euro; la hora</li>
        <li>Alquiler de sillas de ruedas de hasta 2 plazas para personas con dificultad de movilidad por horas, d&iacute;as, o semanas</li>
        <li>Alquiler de Mini Bus el&eacute;ctrico de 9  plazas para fiestas, despedidas de soltero, bodas, bautizos, cumplea&ntilde;os, etc</li>
        </ul>
        <p><img src="http://www.travelcarecofun.com/img/badalona-ecofun.jpg" alt="Ecofun en Badalona" title="Ecofun en Badalona"></p>
        <p><b>RESPETO  AL MEDIO AMBIENTE</b></p>
        <p>Hazte  socio ECO-FUN&nbsp; y disfruta de media hora  gratis cada d&iacute;a al alquilar tu bici.<br>Disfruta  de todas las ventajas de ser socio ECO-FUN por tan solo 30 &euro;  anuales.<br>Venta  de motos el&eacute;ctricas a precio incre&iacute;ble , mas baratas que las tradicionales.</p>
        <ul>
        <li>Consumo inferior de 30 c&eacute;ntimos por  recarga ( como tu m&oacute;vil)</li>
        <li>Autonom&iacute;a de 60 Km</li>
        <li>Velocidad 40 km/h</li>
        <li>Gasto de 3 &euro; cada 400 Km.</li>
        </ul>
        <p><img src="http://www.travelcarecofun.com/img/bicicletas_badalona.jpg" class="bordeverde" alt="Alquilar bicicletas y motos electicas en Badalona" title="Alquilar bicicletas y motos electicas en Badalona"></p>
    </td>    
		
<?php include("../piecera.php")?>