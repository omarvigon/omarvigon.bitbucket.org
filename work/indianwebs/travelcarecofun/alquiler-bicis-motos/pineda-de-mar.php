<?php 
$titulo="Alquiler de motos y bicicletas el&eacute;ctricas en Pineda de Mar - Travel Car Eco Fun";
include("../cabecera.php")?>
		
<tr>
      <td class="fila_izq">
		<?php include("00bt_tiendas.php"); ?>
      </td>
      <td class="fila_der">	
      	<h1>Alquiler de motos y bicis el&eacute;ctricas en Pineda de Mar</h1>
		<p>Travelcar ECO FUN tiene el placer de invitarles a la inauguraci&oacute;n de la primera tienda dedicada a alquiler y venta de <em>veh&iacute;culos el&eacute;ctricos</em>, bicicletas y motos en Pineda de Marque tendr&aacute; lugar el pr&oacute;ximo <b>7 de Mayo a las 17:00h</b> en la <b>Avda. Montserrat,59 Tel. 933.992.581.</b></p>
        <p>Para m&aacute;s informaci&oacute;n contactar en los tel&eacute;fonos 937.670.130 / 679.754.232</p>
        <p>Travelcar ECO FUN inicia su actividad en PINEDA DE MAR por su importante nivel tur&iacute;stico, con un servicio de alquiler de bicicletas y motos el&eacute;ctricas no contaminantes en la playa y la costa.</p>
        <p><img title="Alquilar bicicletas y motos electicas en Pineda de Mar" src="http://www.travelcarecofun.com/img/ecofun_pineda_de_mar.jpg" alt="Alquilar bicicletas y motos electicas en Pineda de Mar"></p>
        <!--<p><img src="http://www.travelcarecofun.com/img/bicicletas_pinedademar.jpg" class="bordeverde" alt="Alquilar bicicletas y motos electicas en Pineda de Mar" title="Alquilar bicicletas y motos electicas en Pineda de Mar"></p>-->
    </td>    
		
<?php include("../piecera.php")?>