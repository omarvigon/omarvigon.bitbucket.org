<?php 
$titulo="Bicicletas electricas alquiler y venta en Barcelona";
include("../cabecera.php")?>

<tr>
    <td class="fila_izq">
		<?php include("00bt_vehiculo.php");?>
    </td>
    <td class="fila_der_v">
        <h1>Nuestras bicicletas el&eacute;ctricas</h1>
		<?php 
		switch($_GET["ecofun"])
		{
		 	case "city101": ?>
            <p>ECO FUN CITY 101</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city102": ?>
            <p>ECO FUN CITY 102</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city103": ?>
            <p>ECO FUN CITY 103</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city104": ?>
            <p>ECO FUN CITY 104</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES.</p>
            <? break;
			
			case "city105": ?>
            <p>ECO FUN CITY 105</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Li-ion 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 27 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city106": ?>
            <p>ECO FUN CITY 106</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Li-ion 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 27 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city107": ?>
            <p>ECO FUN CITY 107</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8AH 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city108": ?>
            <p>ECO FUN CITY 108</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Li-ion 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 23.5 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES </p>
            <? break;
			
			case "city109": ?>
            <p>ECO FUN CITY 109</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 36V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 30 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city110": ?>
            <p>ECO FUN CITY 110</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA :Nimh 8Ah 24 V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 27 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 125 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city111": ?>
            <p>ECO FUN CITY 111</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8Ah 24V<br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 125 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city112": ?>
            <p>ECO FUN CITY 112</p>
            <p>POTENCIA DE MOTOR 150/250W <br>
            PAS <br>
            6-clase Shimano velocidad artes <br>
            POTENCIA BATER&Iacute;A Nimh 8Ah 24V <br>
            CARGADOR 220 / 110V, 50 - 60 Hz <br>
            NUM. DE CARGA&gt; 500 veces <br>
            TIEMPO RECARGA 6-8 horas <br>
            PESO 25kg <br>
            TRANSMISION: Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX. DE TRACCION 125kg <br>
            VELOCIDAD MAXIMA 25 kmh <br>
            26 &times; 1,95 <br>
            AUTONOMIA 45 - 60 kilometros <br>
            Marco y ruedas de aleaci&oacute;n. <br>
            219pcs --- 96pcs </p>
            <? break;
			
			case "city113": ?>
            <p>ECO FUN CITY 113</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 125 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city114": ?>
            <p>ECO FUN CITY 114</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 125 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city115": ?>
            <p>ECO FUN CITY 115</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city116": ?>
            <p>ECO FUN CITY 116 </p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "city117": ?>
            <p>ECO FUN CITY 117</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 24V 12 AH lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "expert001": ?>
            <p>ECO FUN EXPERT 001</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 36V 12 AH   lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 28 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX :   45 - 55 KM <br>
            * HOMOLOGADO CEE<br>
            * GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "expert002": ?>
            <p>ECO FUN EXPERT 002 </p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 36V 12 AH   lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 28 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX :   45 - 55 KM <br>
            * HOMOLOGADO CEE<br>
            * GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "expert003": ?>
            <p>ECO FUN EXPERT 003 </p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8aH   24v <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO   DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD   MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX : 45 - 55 KM <br>
            * HOMOLOGADO   CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I   CARGADOR : 6 MESES .</p>
            <? break;
			
			case "expert004": ?>
            <p>ECO FUN EXPERT 004</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : 36V 12 AH   lead-acid <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 28 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX :   45 - 55 KM <br>
            * HOMOLOGADO CEE<br>
            * GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "expert007": ?>
            <p>ECO FUN EXPERT 007 </p>
            <p>BICICLETA ELECTRICA<br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25.5 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 85 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "expert008": ?>
            <p>EXPERT 008</p>
            <p>POTENCIA DE MOTOR <br>
            150/250w <br>
            Sensor inteligente de par <br>
            POTENCIA BATERIA: Nimh 8Ah 24V <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM. DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA 6-8 horas <br>
            PESO 20kg <br>
            1520 &times; 550 &times; 1050mm <br>
            1400 &times; 280 &times; 760mm <br>
            PESO MAX. DE TRACCION: 100 kg <br>
            VELOCIDAD MAXIMA 25 kmh <br>
            20x1.95 <br>
            AUTONOMIA 45 - 60 kilometros <br>
            Marco y ruedas de aleaci&oacute;n <br>
            223pcs --- 96pcs</p>
            <? break;
			
			case "expert009": ?>
            <p>EXPERT 009</p>
            <p>POTENCIA  DE MOTOR <br>
            150/250W <br>
            Sensor inteligente de par <br>
            POTENCIA BATER&Iacute;A:24V 12 AH plomo-&aacute;cido <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM. DE CARGAS:&gt; 500 veces <br>
            TIEMPO DE CARGA 6-8 horas <br>
            PESO 39.2Kg <br>
            TRANSMISION: Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX. DE TRACCI&Oacute;N:120kg <br>
            VELOCIDAD MAXIMA 25 kmh <br>
            16'' <br>
            AUTONOMIA 45-60KM <br>
            Marco y ruedas de aleaci&oacute;n <br>
            223pcs --- 96pcs</p>
            <? break;
			
			case "expert010": ?>
            <p>EXPERT 100</p>
            <p>POTENCIA DEL MOTOR 150/250W <br>
            PAS 6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA: Nimh 8Ah 24V <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 25.5Kg <br>
            TRANSMISION: Cadena <br>
            1400 &times; 280 &times; 760mm <br>
            PESO MAX DE TRACCION: 85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            20X1.95 <br>
            AUTONOMIA: 45 - 60 kilometros <br>
            Marco y ruedas de aleaci&oacute;n <br>
            223pcs --- 96pcs</p>
            <? break;
			
			case "expert070": ?>
            <p>EXPERT 070</p>
            <p>Cuadro: Aluminio</p>
            Color: Metalizado<br>
            Suspensi&oacute;n Delantera<br>
            Cambio 6   velocidades<br>
            Ruedas: 16 Pulgadas<br>
            Timbre<br>
            Autonom&iacute;a 25   Km.<br>
            Bater&iacute;a 24V 10Ah Litio<br>
            Capacidad de carga: 75 Kg.<br>
            Motor:   180 W Brushless<br>
            Peso: 19 Kg<br>
            Plegable: Si<br>
            Tiempo de recarga: 6   horas<br>
            Velocidad m&aacute;xima: 25 Km/<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "mountain202": ?>
            <p>MOUNTAIN 202</p>
            <p>POTENCIA DEL MOTOR<br>
            G 150/2q50W <br>
            6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA:Li-ion 8Ah 24V <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM DE CARGA: &gt; 500 veces <br>
            TIEMPO DE CARGA 6-8 horas <br>
            PESO 27Kg <br>
            TRANSMISION: Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX. DE TRACCION: 120kg <br>
            VELOCIDAD MAXIMA 25 kmh <br>
            26 &times; 1,95 <br>
            AUTONOMIA:45-60KM <br>
            F-agitar marco de absorci&oacute;n de aleaci&oacute;n y neum&aacute;ticos <br>
            219pcs --- 96pc</p>
            <? break;
			
			case "mountain204": ?>
            <p>MOUNTAIN204</p>
            <p>POTENCIA DEL MOTOR<br>
            150/250W <br>
            PAS 6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA:Nimh 8Ah 24V <br>
            CARGAOR: 220 / 110V, 50 - 60 Hz <br>
            NUMERO DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA 6-8 horas <br>
            PESO: 25kg <br>
            TRANSMISION:Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX.DE TRACCION: 125kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            26 &times; 1,95 <br>
            AUTONOMIA: 45 - 60 kilometros <br>
            Marco y ruedas de aleaci&oacute;n. <br>
            219pcs --- 96pcs</p>
            <? break;
			
			case "mountain205": ?>
            <p>ECO FUN MOUNTAIN 205</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Nimh 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 25 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 125 Kg <br>
            DISTANCIA MAX : 45 - 60 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "mountain206": ?>
            <p>ECO FUN MOUNTAIN 206</p>
            <p>BICICLETA ELECTRICA <br>
            POTENCIA MOTOR : 250 W <br>
            POTENCIA BATERIA : Li-ion 8Ah 24V <br>
            CARGADOR : 220/110V ,50 - 60 HZ <br>
            NUM DE CARGA : &gt; 500 . <br>
            TIEMPO DE CARGA : 6-8 HORES <br>
            PESO : 27 Kg . <br>
            TRANSMISION : CADENA . <br>
            VELOCIDAD MAX : 25 KM/H <br>
            PESO MAX DE TRACCION : 120 Kg <br>
            DISTANCIA MAX : 50 - 65 KM <br>
            * TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
            * PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES) .<br>
            * SE CONFIRMARA LA FECHA D'ENTREGA<br>
            * HOMOLOGADO CEE<br>
            *GARANTIA : <br>
            MOTOR : 12 MESES <br>
            BATTERIA : 8 MESES <br>
            CONTROLADOR I CARGADOR : 6 MESES .</p>
            <? break;
			
			case "mountain207": ?>
            <p>MOUNTAIN 207</p>
            <p>POTENCIA DE MOTOR<br>
            G 150/2q50W <br>
            6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA:Li-ion 8Ah 24V <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM.DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 27Kg <br>
            TRANSMISION: Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX. DE TRACCION: 120kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            26 &times; 1,95 <br>
            AUTONOMIA:50-65KM <br>
            Marco: fibras de carbono <br>
            219pcs ---- 96pcs</p>
            <? break;
			
			case "mountain208": ?>
            <p>MOUNTAIN 208</p>
            <p>POTENCIA  DE MOTOR <br>
            G 150/2q50W <br>
            6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA:Li-ion 8Ah 24V <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 27Kg <br>
            TRANSMISION: Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX DE TRACCION: 120kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            26 &times; 1,95 <br>
            AUTONOMIA: 45-60KM <br>
            F-agitar marco de absorci&oacute;n de aleaci&oacute;n y neum&aacute;ticos <br>
            219pcs --- 96pcs</p>
            <? break;
			
			case "zd001": ?>
            <p>ZD-001</p>
            <p>POTENCIA  DE MOTOR<br>
            150/250W <br>
            PAS 6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA: 36V 12 AH plomo-&aacute;cido <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUM DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 28Kg <br>
            TRANSMISION:Cadena <br>
            1370 &times; 260 &times; 660mm <br>
            PESO MAX. DE TRACCION: 85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            16'' <br>
            AUTONOMIA: 45 - 55 kilometros <br>
            Marco y ruedas de aleaci&oacute;n <br>
            238pcs --- 112pc</p>
            <? break;
			
			case "zd002": ?>
            <p>ZD-002</p>
            <p>POTENCIA  DE MOTOR<br>
            150/250W <br>
            PAS <br>
            6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA:36V 12 AH plomo-&aacute;cido <br>
            CARGADOR:220 / 110V, 50 - 60 Hz <br>
            NUMERO DE CARGAS:&gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 28Kg <br>
            TRANSMISION: Cadena <br>
            1370 &times; 260 &times; 660mm <br>
            PESO MAX. DE TRACCION: 85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            16'' <br>
            AUTONOMIA: 45 - 55 kilometros <br>
            Marco y ruedas de aleaci&oacute;n <br>
            238pcs --- 112pcs</p>
            <? break;
			
			case "zd003": ?>
            <p>ZD-003</p>
            <p>POTENCIA  DE MOTOR<br>
            150/250W <br>
            PAS 6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA: Nimh 8Ah 24V <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUMERO DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 25kg <br>
            TRANSMISION: Cadena <br>
            1370 &times; 260 &times; 660mm <br>
            PESO MAX DE TRACCION: 85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            16'' <br>
            AUTONOMIA: 45 - 55 kilometros <br>
            Marco y ruedas de aleaci&oacute;n <br>
            238pcs --- 112pcs</p>
            <? break;
			
			case "zd004": ?>
            <p>ZD-004</p>
            <p>POTENCIA  DE MOTOR<br>
            150/250W <br>
            PAS 6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA: 36V 12 AH plomo-&aacute;cido <br>
            CARGADOR: 220 / 110V, 50 - 60 Hz <br>
            NUMERO DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 28Kg <br>
            TRANSMISION: Cadena <br>
            1370 &times; 260 &times; 660mm <br>
            PESO MAX DE TRACCION:85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            16'' <br>
            AUTONOMIA: 45 - 55 kilometros <br>
            Marco y ruedas de aleaci&oacute;n <br>
            238pcs --- 112pcs</p>
            <? break;
			
			case "zd005": ?>
            <p>ZD-005</p>
            <p>POTENCIA  DE MOTOR<br>
            150/250W <br>
            PAS 6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA: Plomo-&aacute;cido 12Ah 24V <br>
            CARGADOR:220 / 110V, 50 - 60 Hz <br>
            NUM. DE CARGAS: &gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 25kg <br>
            Cadena <br>
            1370 &times; 260 &times; 660mm <br>
            PESO: 85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            16'' <br>
            AUTONOMIA: 45 - 55 kilometros <br>
            Marco de acero <br>
            238pcs --- 112pcs</p>
            <? break;
			
			case "zd006": ?>
            <p>ZD-006</p>
            <p>POTENCIA  DE MOTOR<br>
            150/250W <br>
            PAS <br>
            6-clase Shimano velocidad artes <br>
            POTENCIA BATERIA:24V 12 AH plomo-&aacute;cido <br>
            CARGADOR:220 / 110V, 50 - 60 Hz <br>
            NUM. DE CARGAS:&gt; 500 veces <br>
            TIEMPO DE CARGA: 6-8 horas <br>
            PESO: 27Kg <br>
            TRANSMISION: Cadena <br>
            1450 &times; 280 &times; 760mm <br>
            PESO MAX DE TRACCION: 85kg <br>
            VELOCIDAD MAXIMA: 25 kmh <br>
            16'' <br>
            AUTONOMIA: 45 - 55 kilometros<br>
            Marco de acero<br>
            238pcs---112pcs</p>
            <? break;
		}
		?>
    </td>
        
<?php include("../piecera.php")?>
