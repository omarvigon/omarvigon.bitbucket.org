<?php 
$titulo=" Vehiculos electricos para personas con discapacidad Barcelona";
include("../cabecera.php")?>

<tr>
    <td class="fila_izq">
		<?php include("00bt_vehiculo.php");?>
    </td>
    <td class="fila_der_v">
        <h1>Veh&iacute;culos el&eacute;ctricos para personas con movilidad reducida</h1>
        <p style="text-align:justify"><em>Travelcar Ecofun</em> tambi&eacute;n dispone de  vehiculos el&eacute;ctricos para mejorar la calidad de vida de las personas con  movilidad reducida, facilit&aacute;ndoles su desplazamiento. Asimismo contamos con atractivos  veh&iacute;culos para el traslado de personas &oacute; cargas peque&ntilde;as en recintos feriales,  campos de golf, parques tem&aacute;ticos y jardines privados</p>
        
        <img src="http://www.travelcarecofun.com/img/mobility-ecofun-1.jpg" class="bordeverde2" alt="Vehiculo electrico para personas de movilidad reducida" title="Vehiculo electrico para personas de movilidad reducida">
        <p><b>MOBILITY ECO FUN 011 - 1.390,00&euro;</b></p>
        <p>Dimensiones (mm): 930 x496 x 872 <br>
        al suelo: 30 mm <br>
        Max. Velocidad de avance: A 6 km / h <br>
        Capacidad de Escalada: 12 grados <br>
        Radio de giro: 1,0 m <br>
        Motores: 24V 180W <br>
        Bater&iacute;a: 12V 12Ah x 2 <br>
        Autonom&iacute;a: 17 kil&oacute;metros <br>
        Cargador: 110VAC-230VAC/24VDC-2A <br>
        Tiempo de carga: 8-10h <br>
        Peso Neto 45kg (incluye 2 pilas: 9.6kg) <br>
        Neum&aacute;ticos: 8 &quot; <br>
        Cantidad de contenedores 20'GP: 88pcs <br>
        40'GP: 192pcs <br>
        40'HQ: 240pcs</p>

        <img src="http://www.travelcarecofun.com/img/mobility-ecofun-4.jpg" class="bordeverde2" alt="Vehiculo ecologico para personas de movilidad reducida" title="Vehiculo ecologico para personas de movilidad reducida">
        <p><b>Mobility Eco Fun 004 - 1.650,00&euro;</b></p>
        <p>Fuente de energ&iacute;a  el&eacute;ctrica <br>
        Drive motor 24V 600W <br>
        Sistema el&eacute;ctrico de 24V DC <br>
        Bater&iacute;as 12V50Ahx2 <br>
        Cargador 24V4A <br>
        Contralor 110Asolid <br>
        Frenos autom&aacute;ticos de frenos electromagn&eacute;ticos <br>
        Marco Acero <br>
        Instrumentaci&oacute;n :<br>
        indicadores el&eacute;ctricos / derecho indicador, el indicador de inflexi&oacute;n, Adelante / atr&aacute;s indicador, el indicador de estado, indicador de luz, indicador  de freno / Cuerpo acabado de pintura pl&aacute;stica / Paquete de cart&oacute;n <br>
        Frente neum&aacute;tico 13x5-6 <br>
        Neum&aacute;tico trasero 13x5-6 <br>        
        Max capacidad 140kg/308lb <br>
        Longitud total 1450mm/57.09 <br>
        En general amplia 720mm/28.35 <br>
        Altura total 1050mm/41.34 <br>
        Distancia entre ejes 925mm/36.42 <br>
        al suelo 1.3m/51.18 <br>
        Adelante velocidad 115mm/4.53 <br>
        Radio de giro 12.5 kil&oacute;metros / h <br>
        Garant&iacute;a de la bater&iacute;a del veh&iacute;culo 6 meses el resto dos a&ntilde;os</p>
        
        <img src="http://www.travelcarecofun.com/img/mobility-ecofun-5.jpg" class="bordeverde2" alt="Vehiculo electrico para personas de movilidad reducida" title="Vehiculo electrico para personas de movilidad reducida">
        <p><b>MOBILITY  ECO FUN 005 - 1.480,00&euro;</b></p>
        <p>Fuente de energ&iacute;a  el&eacute;ctrica <br>
        Drive motor 24V 450W <br>
        Sistema el&eacute;ctrico de 24V DC <br>
        Bater&iacute;as 12V17Ahx2 <br>
        Cargador 24V2A <br>
        Contralor 24V45Asolid <br>
        Frenos magn&eacute;ticos de freno <br>
        Parque de freno autom&aacute;tico de freno <br>
        Marco Acero <br>
        Cuerpo acabado de pintura pl&aacute;stica / electroforesis <br>
        Paquete de cart&oacute;n <br>
        Frente neum&aacute;tico 9x3.5-4 <br>
        Neum&aacute;tico trasero 9x3.5-4 <br>
        Instrumentaci&oacute;n&nbsp; indicadores el&eacute;ctricos /  derecho indicador, el indicador de inflexi&oacute;n, / Adelante / atr&aacute;s indicador, el indicador de estado, indicador de luz, indicador  de freno <br>
        Max capacidad 120kg/264lb <br>
        Longitud total 1210mm/47.64 <br>
        En general amplia 560mm/22.05 <br>
        Altura total 1150mm/45.26 <br>
        ejes 755mm/29.72 <br>
        Al suelo &lt;1.2m/47.24 <br>
        Sistema de frenado 72mm/2.83 &nbsp;<br>
        Velocidad delante &nbsp;7 kil&oacute;metros / h <br>
        Radio de giro &nbsp;1.5m/59.06</p>

        <img src="http://www.travelcarecofun.com/img/mobility-ecofun-6.jpg" class="bordeverde2" alt="Vehiculo ecologico para personas de movilidad reducida" title="Vehiculo ecologico para personas de movilidad reducida">
        <p><b>MOBILITY ECO FUN 006 - 1.490,00&euro;</b></p>
        <p>Fuente de energ&iacute;a  el&eacute;ctrica <br>
        Drive motor 24V 450W <br>
        Sistema el&eacute;ctrico de 24V DC <br>
        Bater&iacute;as 12V38Ahx2 <br>
        Cargador 24V4A <br>
        Contralor 24V45Asolid <br>
        Frenos autom&aacute;ticos de frenos electromagn&eacute;ticos <br>
        Parque de freno autom&aacute;tico <br>
        Marco Acero <br>
        Cuerpo acabado de pintura pl&aacute;stica / electroforesis <br>
        Paquete de cart&oacute;n <br>
        Frente neum&aacute;tico 10x4.10/3.50-4 <br>
        Neum&aacute;tico trasero 10x4, 10/3.50-4 <br>
        Instrumentaci&oacute;n <br>
        indicadores El&eacute;ctrico &nbsp;/ derecho  indicador, el indicador de inflexi&oacute;n, <br>
        Adelante / atr&aacute;s indicador, el indicador de estado, indicador de luz, indicador  de freno <br>
        Max capacidad 140kg/308lb <br>
        Longitud total 1360mm/53.57 <br>
        En general amplia 620mm/24.41 <br>
        Altura total 1180mm/46.46 <br>
        Distancia entre ejes 895mm/35.24 <br>
        Distancia de frenado &lt;1,5 m (ajustado por programa) <br>
        Al suelo 120mm/4.72 <br>
        Adelante velocidad de 8.3 kil&oacute;metros / h <br>
        Radio de giro 1.3m/51.18 <br>
        Garant&iacute;a de la bater&iacute;a del veh&iacute;culo es de 6 meses el resto dos a&ntilde;os</p>
        </td>
        
<?php include("../piecera.php")?>