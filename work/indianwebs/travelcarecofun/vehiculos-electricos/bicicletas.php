<?php 
$titulo="Bicicletas electricas alquiler y venta en Barcelona.";
include("../cabecera.php")?>

<tr>
    <td class="fila_izq">
		<?php include("00bt_vehiculo.php");?>
        <p align="right">La <b>Bicicleta El&eacute;ctrica</b> es una bici convencional a la que se le ha  incorporado un peque&ntilde;o motor y unas bater&iacute;as. No se debe confundir con una <em>moto el&eacute;ctrica</em>, ya que la bici el&eacute;ctrica necesita que pedaleemos para activar el motor.</p>
		<p align="right">La combinaci&oacute;n de ambas fuerzas permite que el desplazamiento sea suave y  c&oacute;modo. Inconvenientes como la resistencia que produce el viento de cara o las  fuertes pendientes quedan minimizados con este tipo de bicicletas.</p>
		<p align="right">Las <b>Bicicletas El&eacute;ctricas</b> son muy entretenidas y f&aacute;ciles de manejar. Es muy  gratificante la sensaci&oacute;n que produce el pedalear sin esfuerzo una vez  alcanzada velocidad. En una <b>bicicleta el&eacute;ctrica</b> esta sensaci&oacute;n es constante. La  velocidad se alcanza r&aacute;pida y silenciosamente, mientras realizamos un agradable  ejercicio con las piernas.</p>
		<p align="right">Su f&aacute;cil manejo, similar a una <em>bicicleta convencional</em> y sin olvidar que no es  necesario ning&uacute;n tipo de permiso de circulaci&oacute;n ni seguro, hace que cualquier  persona pueda disfrutar de ella.</p>
    </td>
    <td class="fila_der_v">
        <h1>Nuestras bicicletas el&eacute;ctricas</h1>
        <p class="listado_bicis">
        <a href="bicis.php?ecofun=city101"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city101.gif" alt="EcoFun Bicicleta City 101" title="Eco Fun Bicicleta City 101"></a>
        <a href="bicis.php?ecofun=city102"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city102.gif" alt="EcoFun Bicicleta City 102" title="Eco Fun Bicicleta City 102"></a>
        <a href="bicis.php?ecofun=city103"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city103.gif" alt="EcoFun Bicicleta City 103" title="Eco Fun Bicicleta City 103"></a>
        <a href="bicis.php?ecofun=city104"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city104.gif" alt="EcoFun Bicicleta City 104" title="Eco Fun Bicicleta City 104"></a>
        <a href="bicis.php?ecofun=city105"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city105.gif" alt="EcoFun Bicicleta City 105" title="Eco Fun Bicicleta City 105"></a>
        <a href="bicis.php?ecofun=city106"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city106.gif" alt="EcoFun Bicicleta City 106" title="Eco Fun Bicicleta City 106"></a>
        <a href="bicis.php?ecofun=city107"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city107.gif" alt="EcoFun Bicicleta City 107" title="Eco Fun Bicicleta City 107"></a>
        <a href="bicis.php?ecofun=city108"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city108.gif" alt="EcoFun Bicicleta City 108" title="Eco Fun Bicicleta City 108"></a>
        <a href="bicis.php?ecofun=city109"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city109.gif" alt="EcoFun Bicicleta City 109" title="Eco Fun Bicicleta City 109"></a>
        <a href="bicis.php?ecofun=city110"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city110.gif" alt="EcoFun Bicicleta City 110" title="Eco Fun Bicicleta City 110"></a>
        <a href="bicis.php?ecofun=city111"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city111.gif" alt="EcoFun Bicicleta City 111" title="Eco Fun Bicicleta City 111"></a>
        <a href="bicis.php?ecofun=city112"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city112.gif" alt="EcoFun Bicicleta City 112" title="Eco Fun Bicicleta City 112"></a>
        <a href="bicis.php?ecofun=city113"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city113.gif" alt="EcoFun Bicicleta City 113" title="Eco Fun Bicicleta City 113"></a>
        <a href="bicis.php?ecofun=city114"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city114.gif" alt="EcoFun Bicicleta City 114" title="Eco Fun Bicicleta City 114"></a>
        <a href="bicis.php?ecofun=city115"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city115.gif" alt="EcoFun Bicicleta City 115" title="Eco Fun Bicicleta City 115"></a>
        <a href="bicis.php?ecofun=city116"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city116.gif" alt="EcoFun Bicicleta City 116" title="Eco Fun Bicicleta City 116"></a>
        <a href="bicis.php?ecofun=city117"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-city117.gif" alt="EcoFun Bicicleta City 117" title="Eco Fun Bicicleta City 117"></a>
        <a href="bicis.php?ecofun=expert001"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert001.gif" alt="EcoFun Bicicleta Expert 001" title="Eco Fun Bicicleta Expert 001"></a>
        <a href="bicis.php?ecofun=expert002"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert002.gif" alt="EcoFun Bicicleta Expert 002" title="Eco Fun Bicicleta Expert 002"></a>
        <a href="bicis.php?ecofun=expert003"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert003.gif" alt="EcoFun Bicicleta Expert 003" title="Eco Fun Bicicleta Expert 003"></a>
        <a href="bicis.php?ecofun=expert004"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert004.gif" alt="EcoFun Bicicleta Expert 004" title="Eco Fun Bicicleta Expert 004"></a>
        <a href="bicis.php?ecofun=expert007"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert007.gif" alt="EcoFun Bicicleta Expert 007" title="Eco Fun Bicicleta Expert 007"></a>
        <a href="bicis.php?ecofun=expert008"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert008.gif" alt="EcoFun Bicicleta Expert 008" title="Eco Fun Bicicleta Expert 008"></a>
        <a href="bicis.php?ecofun=expert009"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert009.gif" alt="EcoFun Bicicleta Expert 009" title="Eco Fun Bicicleta Expert 009"></a>
        <a href="bicis.php?ecofun=expert010"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert010.gif" alt="EcoFun Bicicleta Expert 010" title="Eco Fun Bicicleta Expert 010"></a>
        <a href="bicis.php?ecofun=expert070"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-expert070.gif" alt="EcoFun Bicicleta Expert 070" title="Eco Fun Bicicleta Expert 070"></a>
        <a href="bicis.php?ecofun=mountain202"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-mountain202.gif" alt="EcoFun Bicicleta Mountain 202" title="Eco Fun Bicicleta Mountain 202"></a>
        <a href="bicis.php?ecofun=mountain204"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-mountain204.gif" alt="EcoFun Bicicleta Mountain 204" title="Eco Fun Bicicleta Mountain 204"></a>
        <a href="bicis.php?ecofun=mountain205"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-mountain205.gif" alt="EcoFun Bicicleta Mountain 205" title="Eco Fun Bicicleta Mountain 205"></a>
        <a href="bicis.php?ecofun=mountain206"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-mountain206.gif" alt="EcoFun Bicicleta Mountain 206" title="Eco Fun Bicicleta Mountain 206"></a>
        <a href="bicis.php?ecofun=mountain207"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-mountain207.gif" alt="EcoFun Bicicleta Mountain 207" title="Eco Fun Bicicleta Mountain 207"></a>
        <a href="bicis.php?ecofun=mountain208"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-mountain208.gif" alt="EcoFun Bicicleta Mountain 208" title="Eco Fun Bicicleta Mountain 208"></a>
        <a href="bicis.php?ecofun=zd001"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-zd001.gif" alt="EcoFun Bicicleta ZD 01" title="Eco Fun Bicicleta ZD 01"></a>
        <a href="bicis.php?ecofun=zd002"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-zd002.gif" alt="EcoFun Bicicleta ZD 02" title="Eco Fun Bicicleta ZD 02"></a>
        <a href="bicis.php?ecofun=zd003"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-zd003.gif" alt="EcoFun Bicicleta ZD 03" title="Eco Fun Bicicleta ZD 03"></a>
        <a href="bicis.php?ecofun=zd004"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-zd004.gif" alt="EcoFun Bicicleta ZD 04" title="Eco Fun Bicicleta ZD 04"></a>
        <a href="bicis.php?ecofun=zd005"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-zd005.gif" alt="EcoFun Bicicleta ZD 05" title="Eco Fun Bicicleta ZD 05"></a>
        <a href="bicis.php?ecofun=zd006"><img src="http://www.travelcarecofun.com/img/bicicletas/ecofun-zd006.gif" alt="EcoFun Bicicleta ZD 06" title="Eco Fun Bicicleta ZD 06"></a>
        </p>
    </td>
        
<?php include("../piecera.php")?>
