<?php 
$titulo="Vehiculos ecologicos en Barcelona. Bicicletas y scooters electricos para tours personalizados en Barcelona";
include("../cabecera.php");?>

<tr>
    <td class="fila_izq">
      	<?php include("00bt_vehiculo.php");?>
        
        <h1>NOVEDAD!!</h1>
        <p>Ya puedes alquilar en nuestros locales <b>bicicletas con flexo</b> por 1&euro; al d&iacute;a y contratar publicidad en nuestros flexos!!</p>
		<p><img title="Bicicleta eletrica Flexo" src="../img/bicicleta_flexo2.jpg" alt="Bicicleta eletrica Flexo" class="bordeverde"></p>
    </td>
    <td class="fila_der_v">
    	<h1>Vehiculos el&eacute;ctricos ECO FUN</h1>
        <p style="text-align:justify"><em>Travelcar ECOFUN</em>�cree importante pensar en un  turista que en muchas ocasiones, no tenemos en cuenta, que son los turistas con  una movilidad reducida o sobre peso y les es dif�cil caminar o est�n  imposibilitados para ello y por eso tiene dificultades para conocer diferentes  puntos de la ciudad donde el acceso es caminando; el veh�culo que ponemos a su  alcance es un <b>SCOOTER el�ctrico de dos plazas</b> con una velocidad m�xima de 10km  hora y una autonom�a de 60km por recarga; los clientes tiene dos opciones con  este tipo de veh�culo es realizar un paseo por los sitios de inter�s tur�stico donde no puedan tener acceso ning�n veh�culo de motor; �acompa�ado de un gu�a o alquilarlo y pasear a su  aire. El <b>SCOOTER �el�ctrico</b> es un veh�culo que puede circular  por aceras y parques y puede tener acceso a museos, catedrales donde no existan l�mites ni barreras arquitect�nicas.</p>	
        <p style="text-align:justify"><em>Travelcar ECOFUN</em> en su prop�sito de contribuir al Medio Ambiente utiliza para movilizarse  pensada especialmente en un p�blico joven y divertido, <b>motos de 49cc el�ctricas  no contaminantes</b> las cuales alcanza una velocidad m�xima de 40 km y tiene una  autonom�a de 60km por recarga; los clientes tienen dos opciones con este tipo  de veh�culo es realizar un paseo por la ciudad con un gu�a o alquilar la moto y  pasear a su aire.</p>        
        <p style="text-align:justify"><em>Travelcar ECOFUN</em> dispone de un <b>veh�culo el�ctrico</b> con un dise�o inspirado en los a�os 40  o 50� con una velocidad de 40 Km horas y  una autonom�a de 80 Km por recarga adem�s su din�mica es diferente en la que el  visitante en pareja o solo pude realizar sus propias rutas por la ciudad� como si anduviere en un coche  tradicional.</p>       
        <p style="text-align:justify"><em>Diver Bus</em></p><p><b>Veh�culo:</b> Mini&nbsp; Bus El�ctrico y Ecol�gico<br><b>Uso:</b> capacidad de hasta 7 personas, gu�a y conductor (total de 9 pax)<br><b>Flota:</b> 2 veh�culos <br><b>P�blico:</b> Familias, grupos de  amigos de cualquier edad, peque�os  grupos de ejecutivos con&nbsp; poco de tiempo libre, varios  grupos de turistas del hotel.<br><b>Incluye:</b> Conductor y Gu�a si se desea. Seguro.<br><b>Otros servicios:</b> Opci�n de ir acompa�ado de un gu�a tur�stico en el  mismo bus.<br><b>Oportunidad:</b> Paseo por zonas con encanto<br><b>Ocio:</b> turismo seg�n unos est�ndares o personalizado<br><b>Trabajo:</b> traslados a ferias, congresos, comidas,... de grupos de ejecutivos.
        </td>
        
<?php include("../piecera.php")?>