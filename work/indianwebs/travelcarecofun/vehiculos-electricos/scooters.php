<?php 
$titulo="Scooters electricos alquiler y venta en Barcelona.";
include("../cabecera.php")?>

<tr>
    <td class="fila_izq">
      	<?php include("00bt_vehiculo.php");?>
        
        <img src="http://www.travelcarecofun.com/img/moto-electrica.jpg" border="0" title="Motocicleta electrica" alt="Motocicleta electrica">
		<p align="right"><em>Motos el&eacute;ctricas</em> con un precio desde 1.200 &euro; y autonom&iacute;a de 60 km<br><em>Velocidad</em> de 45 km/hora y con un consumo de 0.25 &euro; por recarga.<br><em>Garant&iacute;a de dos a&ntilde;os</em>, y 6 meses la bater&iacute;a &quot;ten tu moto y  c&aacute;rgalas como si fuese tu m&oacute;vil&quot;</p>
		<p align="right"><em>SCOOTER ELECTRICO</em><br>
        <b>PESO MAX DE TRACCION :</b> 200 Kg <br>
        <b>VELOCIDAD MAX :</b>  40-45 Km/h <br>
        <b>DISTANCIA MAX :</b>  65 Km <br>
        <b>TIEMPO DE CARGA :</b>  +- 8 HORAS <br>
        <b>TIPO DE MOTOR :</b>  Brushless motor <br>
        <b>POTENCIA MOTOR :</b>  1500W <br>
        <b>POTENCIA BATERIA :</b>  12 V 38 Ah <br>
        <b>TENSION DE CARGA :</b>  110 - 250 V <br>
        <b>PESO :</b>  149 Kg <br>
        <b>DIMENSION :</b>  1890*665*1160 mm (L*W*H) <br>
        &middot; TRANSPORTE NO INCLUIDO(SE VALORA A PARTE) <br>
        &middot; PRECIO DE VENTA EN ANDORRA (+16% IVA PARA ES)<br>&middot; SE CONFIRMARA LA FECHA D'ENTREGA<br>
        &middot; HOMOLOGADO CEE<br>&middot; GARANTIA : <br>
        <b>MOTOR :</b>  12 MESES <br>
        <b>BATTERIA :</b>  8 MESES <br>
		<b>CONTROLADOR I CARGADOR :</b>  6 MESES</p>
    </td>
    <td class="fila_der_v">

        <h1>Nuestras scooters el&eacute;ctricas</h1>
        <p><img src="http://www.travelcarecofun.com/img/scooter-electricas.gif" border="0"></p>
    </td>
        
<?php include("../piecera.php")?>
