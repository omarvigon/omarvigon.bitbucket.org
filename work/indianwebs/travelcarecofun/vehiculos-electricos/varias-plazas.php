<?php 
$titulo="Vehiculos electricos de 2 a 5 plazas Barcelona";
include("../cabecera.php")?>

<tr>
    <td class="fila_izq">
		<?php include("00bt_vehiculo.php");?>
    </td>
    <td class="fila_der_v">
        <h1>Vehiculos ecol&oacute;gicos de varias plazas</h1>
        
        <img src="http://www.travelcarecofun.com/img/vehiculo_egb399.jpg" class="bordeverde2" alt="Vehiculo electrico ECOFUN-EG399" title="Vehiculo electrico ECOFUN-EG399">
        <p><b>E.CAR ECO FUN 001</b></p>
        <p>Especificaciones <br>
        Completo Dimensiones (mm): 2320 x 1210 x 1710 <br>
        Embalaje Dimensiones (mm): 2320 x 1350 x 1150 <br>
        La distancia entre las ruedas delanteras (mm): 840 <br>
        La distancia entre las ruedas traseras (mm): 985 <br>
        Distancia entre ejes () 1670 <br>
        Al suelo (mm): 127 <br>
        Escalada de Capacidad: &lt;= 25 <br>
        M&aacute;x. Capacidad de carga: 300 kg <br>
        N.W.: 430kg <br>
        Motor: 2200W <br>
        Autonom&iacute;a: 80 kil&oacute;metros <br>
        Bater&iacute;a: 8V150Ah x 6 <br>
        Accesorios (no gratuito): Lluvia cubrir / Volver Espejo / izquierda / derecha de inflexi&oacute;n Lasmp / headlight / bocina / luz de freno <br>
        Contenedor: 8pcs/20 '18pcs / 40 &quot;HQ</p>
        
        <img src="http://www.travelcarecofun.com/img/vehiculo_egb399a.jpg" class="bordeverde2" alt="Vehiculo ecologico ECOFUN-EG399A" title="Vehiculo ecologico ECOFUN-EG399A">
        <p><b>E.CAR  ECO FUN 002</b></p>
        <p>Especificaciones <br>
        N.W.: 480kg <br>
        Motor: 2200W <br>
        Distancia entre ejes () 2420 <br>
        Al suelo(mm): 127 <br>
        Radio de giro (m): 3,75 <br>
        Completo Dimensiones (mm): 3070 x 1210 x 1710 <br>
        Embalaje Dimensiones (mm): 3300 x 1350 x 1150 <br>
        La distancia entre las ruedas delanteras (mm): 840 <br>
        La distancia entre las ruedas traseras (mm): 985 <br>
        Velocidad m&aacute;xima: 23 km/h <br>
        M&aacute;x. Capacidad de carga: 400kg <br>
        Escalada de Capacidad: &lt;= 25 <br>
        
        Autonom&iacute;a : 75 kms. <br>
        Bater&iacute;a: 8V150Ah x 6 <br>
        Contenedor: 6pcs / 40 &quot;HQ</p>
        
        <img src="http://www.travelcarecofun.com/img/vehiculo_ecofun03.jpg" class="bordeverde2" alt="Vehiculo electrico ECOFUN-3" title="Vehiculo electrico ECOFUN-3">
        <p><b>E.CAR  ECO FUN 003</b></p>
        <p>Especificaciones <br>
        LONGITUD 2640 mm <br>
        ANCHO 1525 mm <br>
        HEIGHTUNLOAD mm 1780 <br>
        WHEELBASE mm 1830 <br>
        FRONT 450 mm OVERHANG <br>
        OVERHANG TRASERO 360 mm <br>
        FRENTE RODADURA 1285 mm <br>
        TRASERO RODADURA 1260 mm <br>
        Min. CILINDRADO RADIUS 4,3 <br>
        Max. CAPACIDAD DE GRADOS 20% <br>
        Min. LIQUIDACI&Oacute;N DE TIERRA 200 mm <br>
        Autonom&iacute;a &nbsp;70 kil&oacute;metros <br>
        PASAJEROS / CARGA (kg) 2 personas <br>
        Max. VELOCIDAD Km / h 35 <br>
        GW 950 Kg <br>
        ENFOQUE &Aacute;NGULO 34 <br>
        &Aacute;NGULO DE SALIDA 45</p>
        
        <img src="http://www.travelcarecofun.com/img/vehiculo_ecofun04.jpg" class="bordeverde2" alt="Vehiculo ecologico ECOFUN-4" title="Vehiculo ecologico ECOFUN-4">
        <p><b>E.CAR ECO FUN 004</b></p>
        <p>Especificaciones <br>
        LONGITUD 3400 mm <br>
        ANCHO 1525 mm <br>
        HEIGHTUNLOAD mm 1780 <br>
        WHEELBASE mm 2590 <br>
        FRONT 450 mm OVERHANG <br>
        OVERHANG TRASERO 360 mm <br>
        FRENTE RODADURA 1285 mm <br>
        TRASERO RODADURA 1260 mm <br>
        Min. CILINDRADO RADIUS 5,3 <br>
        Max. CAPACIDAD DE GRADOS 20% <br>
        Min. LIQUIDACI&Oacute;N DE TIERRA 200 mm <br>
        AUTONOM&Iacute;A &nbsp;60 kil&oacute;metros <br>
        PASAJEROS / CARGA (kg) 4 Personas <br>
        Max. VELOCIDAD Km / h 30 <br>
        GW 1150 Kg <br>
        ENFOQUE &Aacute;NGULO 34 <br>
        &Aacute;NGULO DE SALIDA 45</p>
        
        <img src="http://www.travelcarecofun.com/img/vehiculo_ecofun05.jpg" class="bordeverde2" alt="Vehiculo electrico ECOFUN-5" title="Vehiculo electrico ECOFUN-5">
        <p><b>E.CAR  ECO FUN 005</b></p>
        <p>Especificaciones <br>
        LONGITUD 4160 mm <br>
        ANCHO 1525 mm <br>
        HEIGHTUNLOAD mm 1780 <br>
        WHEELBASE mm 3350 <br>
        FRONT 450 mm OVERHANG <br>
        OVERHANG TRASERO 360 mm <br>
        FRENTE RODADURA 1285 mm <br>
        TRASERO RODADURA 1260 mm <br>
        Min. CILINDRADO RADIUS 7,3 <br>
        MAXIMA CAPACIDAD DE GRADOS % 15 <br>
        Min. LIQUIDACI&Oacute;N DE TIERRA 200 mm <br>
        AUTNOMIA 60 kilometros <br>
        PASAJEROS / CARGA (kg) 6 personas <br>
        Max. VELOCIDAD Km / h 25 <br>
        GW 1374 Kg <br>
        ENFOQUE &Aacute;NGULO 34 <br>
        &Aacute;NGULO DE SALIDA 45</p>

        </td>
        
<?php include("../piecera.php")?>