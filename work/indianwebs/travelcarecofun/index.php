<?php 
$titulo="Guia turistico ecologico en Barcelona. Tour personalizado con vehiculos no contaminantes.";
include("cabecera.php"); ?>

		<tr>
			<td class="fila_izq">
				<div id="capa1"><img title="Primer Minibus Electrico" src="img/sello.gif" alt="Primer minibus electrico matriculado en Europa"></div>
                <img title="Vehiculo ecologico en Barcelona" src="img/coche_electrico_ecofun.jpg" alt="Vehiculo Ecofun ecologico en Barcelona">         
		    </td>
			<td class="fila_der">	
				<h1>Travel Car Eco Fun</h1>
				<p><em>Travelcar Ecofun</em>, porque para conducir, no es necesario contaminar.</p>
				<p>Disponemos de una amplia variedad de veh&iacute;culos el&eacute;ctricos y veh&iacute;culos convencionales <b>no contaminantes</b>, bicicletas el&eacute;ctricas, ciclomotores scooters el&eacute;ctricos, motos de diferente potencia el&eacute;ctricas, silla de ruedas el&eacute;ctricas para personas con discapacidad,  minibuses el&eacute;ctricos, y pr&oacute;ximamente  incorporaremos coches el&eacute;ctricos de 2 plazas, coches el&eacute;ctricos de 4 plazas, vech&iacute;culos el&eacute;ctricos de 6  asientos, etc.</p>
			  	<p><em>Travelcar Ecofun</em> a la  vanguardia de <b>vehiculos el&eacute;ctricos</b>, seguimiento a las novedades, nuevos  productos el&eacute;ctricos, creacion de la marca propia <b><em>ECOFUN</em></b>, con bicis, motos, scooters, sillas de ruedas, minibuses, coches  el&eacute;ctricos y servicio post venta.</p>
				<p><em>Travelcar Ecofun</em> le permite <b>alquilar</b> por horas, d&iacute;as, semanas o meses cualquiera de los siguientes  productos: bicicletas  el&eacute;ctricas, scooters motos el&eacute;ctricas, sillas de ruedas el&eacute;ctricas y minibus  el&eacute;ctrico. Disponemos de accesorios  para potenciar su seguridad.</p>
				<p><b><em>TRAVELCAR ECOFUN</em> PORQUE EL  FUTURO ES NUESTRO PRESENTE</b></p>
                <p><img src="img/ecofun_vehiculos.jpg" title="Vehiculos ecologicos Ecofun" alt="Vehiculos ecologicos Ecofun"></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
		  </td>
		
<?php include("piecera.php")?>
