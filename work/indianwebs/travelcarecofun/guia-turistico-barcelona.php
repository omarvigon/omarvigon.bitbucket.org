<?php 
$titulo="Rutas y guias turisticos ecologicas en Barcelona. Tour personalizado con vehiculos no contanimantes.";
include("cabecera.php")?>

<tr>
    <td class="fila_izq" align="right"> 
       	<h3>Con ECOFUN ya es posible hacer turismo sin contaminar!</h3>
		<p><em>Travel Car Eco Fun</em> son profesionales cualificados del sector  tur&iacute;stico, que les ofrecen una visita detallada por los lugares de Barcelona de mayor inter&eacute;s sin esperas y de una forma personalizada. En funci&oacute;n de recorrido la visita durar&aacute; aproximadamente de 2 a 4 horas , tambi&eacute;n con posibilidad de disponer del GU&Iacute;A TUR&Iacute;STICO PERSONALIZADO hasta 8 horas.</p>
        <img src="img/rutas-turisticas-barcelona.jpg" vspace="30">
    </td>
  <td class="fila_der">
  		<h3>Tours en Barcelona para  grupos familiares o de amigos</h3>
		<p><em>Travelcar  ECOFUN</em> pone a disposici&oacute;n de los visitantes una forma de movilidad por la  ciudad novedosa y divertida se trata de un Mini Bus el&eacute;ctrico con una velocidad  de 40 Km hora y una autonom&iacute;a de 80 Km por recarga donde pueden ir 8 pasajeros m&aacute;s el gu&iacute;a y chofer de forma que puedan realizar diferentes rutas y as�  conocer la ciudad de una forma diferente ecol�gica y divertida respetando en  todo momento el medio ambiente.<a href='vehiculos-ecologicos.php'><em><b>[+info]</b></em></a></p>
        <h1>Rutas Tur�sticas en Barcelona</h1>
  		<em>N&ordm; 1</em> Gaudi Town <b>Ciudad de Gaud�</b> <i>Ciutat de Gaud�</i>.............................<b>18,00&euro;</b><br>
    	<br><em>N&ordm;2</em> Olympic City <b>Ciudad Ol�mpica </b><i>Ciutat Ol�mpica</i>.............................<b>28,00&euro;</b><br>    
      	<br><em>N&ordm;3</em> City of Artists <b>Ciudad de Artistas </b> <i>Ciutat d&acute;artistes</i>.......................<b>30,00&euro;</b><br>  
        <br><em>N&ordm; 4</em> Mediterranean City <b>Ciudad Mediterranea </b> <i>Ciutat Mediterr�nia</i>.......<b>30,00&euro;</b><br>
        <br><em>N&ordm; 5</em> Special Route <b>Ruta Especial </b> ....................................................<b>42,00&euro;</b><br>
        <br><em>N&ordm; 6</em> Route Gold Star <b>Ruta Estrella </b> ..................................................<b>55,20&euro;</b><br>
        <br>
        <h5>* For extra seats contact us on our website<br><em>Para plaza adicional consultar en nuestra web</em></h5>
        <h5>* Children 4 to 12 years old-30% discount. Kids under 3 travel FREE<br><em>Ni&ntilde;os de 4 a 12 a�os - 30% descuento. Ni&ntilde;os hasta 3 a�os Viajan GRATIS</em></h5>        
        <h5><em>* La empresa garantiza la ruta contratada pero no se responsabiliza de los cambios que se puedan producir por fuerza mayor o causas ajenas a la empresa.</em></h5>
        <b>IVA NO INCLUIDO</b> <img src="img/credito.gif" hspace="20" align="absmiddle">
    </td>
        
<?php include("piecera.php")?>