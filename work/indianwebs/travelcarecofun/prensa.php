<?php 
$titulo="Noticias Ecologicas Guia turistico ecologico en Barcelona - Travelcar Ecofun";
include("cabecera.php");?>

<tr>
    <td class="fila_izq">
        <h1>Prensa</h1>
        
        <h6>Junio 2008</h6>
       	<b>&bull;</b> <a href="prensa.php?noticia=barcelona-instala-punto-para-recargar-vehiculos-electricos">Barcelona instala un punto para recargar veh&iacute;culos el&eacute;ctricos</a><br />
        <b>&bull;</b> <a href="prensa.php?noticia=coche-electrico-para-2010">El coche el&eacute;ctrico para 2010</a></p>
       
        <h6>Mayo 2008</h6>
        <b>&bull;</b> <a href="prensa.php?noticia=madrid-premiara-con-rebajas-fiscales-vecinos-coches-ecologicos">El Ayuntamiento de Madrid premiar&aacute; con rebajas fiscales a los vecinos con casas y coches ecol&oacute;gicos</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=coches-mas-contaminantes-pagaran-25libras-entrar-centro-londres">Los coches m&aacute;s contaminantes pagar&aacute;n 25 libras para entrar en centro de Londres</a>
        
        <h6>Abril 2008</h6>
 		<b>&bull;</b> <a href="prensa.php?noticia=coches-del-futuro-se-moveran-con-electricidad">Los coches del futuro se mover&aacute;n con electricidad, seg&uacute;n WWF/Adena.</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=venta-coches-ecologicos-crecen">Las ventas de coches ecol&oacute;gicos crecen un 38,5% por el "impuesto verde"</a>
               
        <h6>Marzo 2008</h6>
        <b>&bull;</b> <a href="prensa.php?noticia=coches-verdes-salon-ginebra">Coches 'verdes' y de mayor rendimiento en el Sal&oacute;n de Ginebra</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=coches-mas-contaminantes">Londres, Roma y Berl&iacute;n imponen restricciones a los coches m&aacute;s contaminantes</a>
             
        <h6>Febrero 2008</h6>
  		<b>&bull;</b> <a href="prensa.php?noticia=empresas_certificacion_ambiental">Las empresas espa&ntilde;olas terceras del mundo y primeras de Europa en certificaci&oacute;n ambiental</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=contaminacionn_madrid">Medio Ambiente pide medidas urgentes contra la contaminaci&oacute;n en Madrid</a>
        
        <h6>Enero 2008</h6>
		<b>&bull;</b> <a href="prensa.php?noticia=escasez-de-energia-sera-problema-clave-futuro">El 80% de los espa&ntilde;oles considera que la escasez de energ&iacute;a ser&aacute; uno de los problemas clave del futuro en Occidente</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=energia-solar-en-naves-cocheras-metro">Energ&iacute;a solar en las naves y cocheras del Metro</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=reducir-emisiones-de-co2-automoviles">Europa quiere reducir en un 25% las emisiones de CO2 de los autom&oacute;viles</a>
        
        <h6>Diciembre 2007</h6>
        <b>&bull;</b> <a href="prensa.php?noticia=emisiones_co2">Emisiones de CO2 de los automóviles</a>
        
        <h6>Noviembre 2007</h6>
        <b>&bull;</b> <a href="javascript:popup('1')">Travelcar Ecofun particip&oacute; en el Ecorally de Formentera <b>VER VIDEO</b></a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=punt8oct">Diari El PUNT - 8/Octubre/2007</a><br>
        <b>&bull;</b> <a href="prensa.php?noticia=eco_formentera">La alternativa ecológica a las 'motorinos' llega a Formentera</a>
    </td>
    <td class="fila_der">
    <?php 
    if($_GET["noticia"]) 
            include("prensa/".$_GET["noticia"].".php"); 
    else
            include("prensa/madrid-premiara-con-rebajas-fiscales-vecinos-coches-ecologicos.php"); 
    ?>
    </td>

<?php include("piecera.php")?>