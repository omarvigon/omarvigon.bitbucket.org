<?php 
$titulo="Nuestros establecimientos con vechiculos electricos en Barcelona, Badalona, Castelldefels,Lloret de Mar, Pineda de Mar.";
include("cabecera.php")?>
		
<tr>
      <td class="fila_izq">
		<?php include("alquiler-bicis-motos/00bt_tiendas.php"); ?>
      </td>
      <td class="fila_der">	
      	<h1>Alquiler de motos y bicis el&eacute;ctricas</h1>
  		<img src="img/ruta-ecologica-maresme.jpg" align="right" style="margin:0 0 0 15px;" alt="Ruta Ecologica Maresme" title="Ruta Ecologica Maresme">      	
		<p>Travelcar ECOFUN inicia su actividad en CASTELLDEFELS por  su importante nivel cultural y tur&iacute;stico, con un servicio de alquiler de bicicletas y motos en la playa.</p>
		<p>En el a&ntilde;o 2007 Barcelona recibi&oacute; m&aacute;s de 6 millones y medio de turistas, un 52% del total visitaron la ciudad por razones profesionales, y  muchos visitaron la playa de Castelldefels.</p>
		<p>El servicio de alquiler de bicicletas y motocicletas se extender&aacute; tambi&eacute;n en lugares como Lloret de Mar, Calella, Pineda de Mar, etc.</p>
		<h1>Plan de Acci&oacute;n 2008 Travelcar Ecofun</h1>
		<p><em>Travelcar ECOFUN</em> tiene como objetivo durante los pr�ximos  meses crear <b>rutas ecol�gicas fuera de la ciudad� de Barcelona</b> desde la comarca de El Maresme hasta la Costa Brava en la  provincia de Girona.<br><br>Tambi&eacute;n se extender&aacute;n por las costas de Gava, Castelldefels y Sitges.</p>
		<p>La creaci�n de unas l�neas de minibuses dentro del casco  urbano donde los turistas y ciudadanos de la zona que se sienta sensibilizados  puedan trasladarse� a la terminal de  RENFE y puedan enlazar en Barcelona con nuestros mini buses para disfrutar de  un recorrido y conocer la ciudad de una forma diferente.</p>
		<p><em>Travelcar</em> est� iniciando las negociaciones con los  ayuntamientos y los hoteles de la zona</p>
    </td>    
		
<?php include("piecera.php")?>