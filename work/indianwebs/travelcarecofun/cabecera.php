<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="verify-v1" content="S/8RHw0XHz8uja1ThXxTcZX7HRPQX3vPg94n+/laCS4=" >
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)">
    <meta name="revisit" content="7 days" >
    <meta name="robots" content="index,follow,all" >
    <meta name="lang" content="es,sp,spanish,espa�ol,espanol,castellano" >
	<meta name="description" content="TravelCarEcoFun - Guia turistico ecologico en Barcelona. Guia personalizado con vehiculos no contanimantes. Servicio de alquiler y venta de biciletas y motos electricas en Barcelona.">
	<meta name="keywords" content="TravelCarEcoFun - Guia turistico ecologico en Barcelona, guia turistico barcelona, turismo ecologico, turismo barcelona, vehiculo ecologico, alquiler de bicis electicas,lloguer de motos electricas, agencia viaje, guia ocio, rutas barcelona, guia barcelona, guia turistica, ruta turistica, paseo turistico, hoteles, vuelos, viajes barcelona, viajes, ocio, alquiler vehiculo, alquileres vehiculo, conocer barcelona, viaje barcelona, coches alquiler, motos alquiler, alquiler moto barcelona, alquiler coche barcelona, agencia de viajes, agencia viaje barcelona.">
	<title><?php echo $titulo ?></title>
	<link rel="stylesheet" type="text/css" href="http://www.travelcarecofun.com/estilos.css">
	<script language="javascript" type="text/javascript" src="http://www.travelcarecofun.com/funciones.js"></script>
</head>
<body>

<table cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="filatop1">
		<a href="http://www.travelcarecofun.com/">
        <img src="http://www.travelcarecofun.com/img/cabecera_travelcar_ecofun1.gif" title="TravelCar EcoFun Guia Turistico Ecologico en Barcelona" alt="TravelCar EcoFun Guia Turistico Ecologico en Barcelona"></a>
	</td>
    <td class="filatop2" colspan="2">
		<img src="http://www.travelcarecofun.com/img/cabecera_travelcar_ecofun2.jpg" title="TravelCar EcoFun Guia Turistico Ecologico en Barcelona" alt="TravelCar EcoFun Guia Turistico Ecologico en Barcelona">
	</td>
</tr>
<tr>
	<td class="filarriba" colspan="3"> 
    	<nobr>
    	<a href="http://www.travelcarecofun.com/" target="_blank">
        	<img src="http://www.travelcarecofun.com/img/icon_esp.gif" style="border:1px solid #333333;" align="absmiddle"></a>
        <a href="http://209.85.135.104/translate_c?hl=es&langpair=es%7Cen&u=http://www.travelcarecofun.com/" target="_blank">
            <img src="http://www.travelcarecofun.com/img/icon_ing.gif" style="border:1px solid #333333;" align="absmiddle"></a>&nbsp;&nbsp;
        
		<a href="http://www.travelcarecofun.com/index.php" title="Travel Car Eco Fun - Turismo ecologico en Barcelona">presentaci&oacute;n</a> <cite>|</cite>   
		<a href="http://www.travelcarecofun.com/vehiculos-electricos/index.php" title="Nuestros vehiculos electricos">nuestros veh&iacute;culos</a> <cite>|</cite>
		<a href="http://www.travelcarecofun.com/empresas.php" title="Empresas y hoteles asociadas de Travel Car Eco Fun">empresas</a> <cite>|</cite>  
		<a href="http://www.travelcarecofun.com/prensa.php" title="Notas de prensa y noticias ecologicas">prensa</a> <cite>|</cite>
        <a href="http://www.travelcarecofun.com/establecimientos.php" title="Servicios de alquiler de vehiculos electricos">establecimientos</a> <cite>|</cite>
		<a href="http://www.travelcarecofun.com/guia-turistico-barcelona.php" title="Rutas y guias turisticos en Barcelona">gu&iacute;a tur&iacute;stico</a> <cite>|</cite> 
		<a href="http://www.travelcarecofun.com/reservas.php" title="Reserva la ruta y vehiculo">reservas</a> <cite>|</cite>
        <a href="http://www.travelcarecofun.com/condiciones.php" title="Condiciones generales y preguntas frecuentes de Travelcar Ecofun">condiciones</a> <cite>|</cite>
		<a href="http://www.travelcarecofun.com/contactar.php" title="Contacta con Travelcar Ecofun">contacta</a>
	</td>
</tr>