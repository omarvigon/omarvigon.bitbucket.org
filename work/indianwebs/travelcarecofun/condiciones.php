<?php 
$titulo="Condiciones generales y preguntas frecuentes de Travel Car Eco Fun.";
include("cabecera.php")?>
		
<tr>
      <td class="fila_izq">
		<img src="img/travelcar_ecofun_condiciones.jpg" alt="Travelcar Ecofun Condiciones Generales" title="Travelcar Ecofun Condiciones Generales">
      </td>
      <td class="fila_der">	
      	<h1>Condiciones Generales y preguntas frecuentes</h1>
		<p>Aqu&iacute; encontrar&aacute;s nuestras condiciones  generales y las preguntas m&aacute;s frecuentes.</p>
        <p><b>Documentos necesarios:</b><br>1. DNI,  Tarjeta de residencia&nbsp; o Pasaporte. <br>2. Permiso o  licencia de Conducci&oacute;n v&aacute;lida en Espa&ntilde;a.<br>
          (Ciclomotor,  Coche o Moto). <br>3. Dep&oacute;sito  de 250 &euro;: Tarjeta de Cr&eacute;dito o efectivo<br>
        <p><b>M&eacute;todo de pago</b>:<br> Efectivo o Tarjeta de Cr&eacute;dito</p>
        <ol>
          <li>El  precio incluye candado anti robo&nbsp; y un  casco por moto </li>
          <li>Precios&nbsp; incluyen IVA.</li>
          <li>Edad  m&iacute;nima para los conductores: </li>
        </ol>
        <p>
        <p>A  partir de los diecis&eacute;is (16) a&ntilde;os de edad, siempre&nbsp; y cuando vengan&nbsp; acompa&ntilde;ados&nbsp;  por un mayor, y posean el carnet de ciclomotor.</p>
        <p><b>Recogida y entrega de las motos:</b>En nuestra  oficina de Alquiler y Venta en jornada continua. (09 -20 horas)</p>
        <p><b>Servicio de entrega:</b> A domicilio, Badalona, Santa Coloma  de Gramanet, y San Adrian del Besos, Cinco Euros, Barcelona 8 Euros  adicionales.<br>Ll&aacute;manos  para obtener informaci&oacute;n m&aacute;s detallada. <br>Te  reembolsaremos hasta 5&euro; si vienes en taxi hasta nuestra tienda. (Debes  presentar el recibo correspondiente).</p>
        <p><b>Seguro:</b> Da&ntilde;os a  terceros y otros riesgos est&aacute;n cubiertos con un l&iacute;mite.</p>
        <p><b>Preguntas m&aacute;s frecuentes.</b></p>
        <p><b>1. &iquest;Qu&eacute; se  necesita para alquilar una scooter? &iquest;Necesito alguna licencia especial? </b><br>Tienes que  venir con tu Pasaporte, Tarjeta de Residencia &nbsp;o DNI, tu tarjeta de cr&eacute;dito, o efectivo &nbsp;para pagar el dep&oacute;sito, y tu licencia de  conducci&oacute;n. La ley en Espa&ntilde;a te permite conducir motos de 50 cc y 125 cc con tu  licencia de conducci&oacute;n normal de coche. Necesitas una licencia que sea v&aacute;lida  en Espa&ntilde;a (Coche o moto). Por ejemplo, la licencia convencional de los Estados  Unidos necesita ser traducida por un traductor oficial.</p>
        <p><b>2. &iquest;Puedo  pagar con tarjeta de cr&eacute;dito? </b><br>Si, puedes  pagar con tarjeta de cr&eacute;dito.</p>
        <p><b>3. &iquest;Es  necesario pagar alg&uacute;n dep&oacute;sito</b>? <br>Necesitas  pagar un dep&oacute;sito de 250 &euro;. Puedes pagarlo en efectivo o con tarjeta de  cr&eacute;dito. No se cargar&aacute; nada a la tarjeta a no ser que ocurra algo que as&iacute; lo  requiera.</p>
        <p><b>4. &iquest;Pueden  dos personas usar la moto? </b><br>NO Todas  nuestras scooters est&aacute;n habilitadas para llevar una&nbsp; persona.</p>
        <p><b>5. &iquest;C&oacute;mo  puedo llegar a la tienda? </b><br>Puedes  llegar en metro (l&iacute;nea 2, estaci&oacute;n Pep Ventura) o en autob&uacute;s &nbsp;paran exactamente en frente de nuestra tienda.  Adem&aacute;s, puedes tomar un taxi y te reembolsaremos hasta 5&euro; si tienes el  comprobante de pago.</p>
        <p><b>6. &iquest;Qu&eacute; pasa  si tengo un accidente o si me roban la scooter?</b><br>Est&aacute;s  cubierto contra da&ntilde;os a terceros. Por da&ntilde;os causados a la moto o en caso de  esta ser robada, el seguro reducir&aacute; el monto m&aacute;ximo que deber&aacute;s pagar hasta  500&euro; - En caso de haber cumplido con las normas estipuladas en el contrato de  alquiler.</p>
        <p><b>7. &iquest;Qu&eacute;  cubre el seguro? </b><br>El seguro  cubre da&ntilde;os a terceros y tambi&eacute;n seguro personal y seguro de vida para el  conductor.</p>
        <p><b>8. &iquest;Puedo  conducir fuera de la ciudad? </b><br>Con el  seguro incluido en el precio de alquiler, est&aacute;s cubierto solo dentro del &aacute;rea  metropolitana (30km). Si quieres conducir fuera de la ciudad, deber&aacute;s comprar  un seguro extra, cuyo precio es del doble del seguro normal.</p>
    </td>    
		
<?php include("piecera.php")?>
