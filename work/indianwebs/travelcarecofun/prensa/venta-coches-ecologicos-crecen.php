<h3>Las ventas de coches ecol&oacute;gicos crecen un 38,5% por el &quot;impuesto verde&quot;</h3>
<p>La nueva fiscalidad verde, que entr&oacute; en vigor este a&ntilde;o y que exime del impuesto de matriculaci&oacute;n a los veh&iacute;culos que emiten igual o menos de 120 gramos de CO2 por kil&oacute;metro, ha provocado un aumento de la demanda de turismos y todoterrenos m&aacute;s ecol&oacute;gicos del 38,5%, con una cifra de 40.075 unidades vendidas hasta febrero, seg&uacute;n un estudio elaborado por la consultora MSI para la Federaci&oacute;n de Asociaciones de Concesionarios de Automoci&oacute;n -Faconauto-.<br>
</p>
<p>Sin embargo, los grandes perjudicados por la reforma fiscal son los todoterrenos que, tras unos a&ntilde;os de fuertes crecimientos, redujeron sus ventas un 16,4% en los dos primeros meses de 2008.<br>
</p>
<p>Las entregas de autom&oacute;viles que emiten m&aacute;s de 200 gramos de CO2, sujetos al tipo m&aacute;ximo del 14,75% en el impuesto de matriculaci&oacute;n, cayeron un 47,69% hasta febrero, con 10.614 unidades.<br>
</p>
<p>En los tramos intermedios, las entregas de veh&iacute;culos con un tipo del 4,75% -emisiones de entre 120 y 160 gramos-, registraron un descenso del 3,04%, con 131.906 unidades, mientras que las ventas de coches entre 160 y 200 gramos totalizaron un 22,57% menos.<br>
    <br>
  FUENTE: ABC<br>
  Fecha de Publicaci&oacute;n: 27-03-2008 </p>
