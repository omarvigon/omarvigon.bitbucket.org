<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
			<h2>Ayudando a concienciar</h2>
            <p>La <em>Asociaci&oacute;n para el Reciclado de Productos de Aluminio</em> realiza actividades diversas para promover la recuperaci&oacute;n de envases usados de aluminio por todo el territorio espa&ntilde;ol y para  crear una verdadera cultura sobre el reciclado de este material.</p>
            <p><b>Arpal</b> edita todo tipo de material promocional sobre el reciclado de envases usados de aluminio que se distribuye entre diferentes p&uacute;blicos: gestores de proyectos de reciclado, centros educativos, medios de comunicaci&oacute;n, asociaciones de diversa &iacute;ndole, empresas, etc. El material disponible en arpal es el siguiente:</p>
            <ul>
            <li>Dossier presentaci&oacute;n</li>
            <li>Bolet&iacute;n Aluminews</li>
            <li> Tr&iacute;ptico informativo</li>
            <li>Taller educativo</li>
            <li>V&iacute;deo</li>
            <li>Posters</li>
            <li>Pegatinas</li>
            <li>Contenedores de cart&oacute;n</li>
            </ul>
        </div>
		
<?php include("0piecera.php");?>
