<?php
session_start();

define("CONTRASENA","arpal08");
define("MES_01","Enero");
define("MES_02","Febrero");
define("MES_03","Marzo");
define("MES_04","Abril");
define("MES_05","Mayo");
define("MES_06","Junio");
define("MES_07","Julio");
define("MES_08","Agosto");
define("MES_09","Septiembre");
define("MES_10","Octubre");
define("MES_11","Noviembre");
define("MES_12","Diciembre");
define("IMG_ANCHO","300");

function redireccionar($pagina)
{
	echo "<script language='javascript'>window.open('".$pagina."','_self')</script>";	
}
function comprobar()
{
	if($_POST["contra"]==CONTRASENA)
		$_SESSION["autentico"]=true;
	redireccionar("index.php");
}
function salir()
{
	session_unset();
	session_destroy();
	redireccionar("http://".$_SERVER["HTTP_HOST"]);	
}
function filtrar($nombre_archivo)
{
	$nuevo_nombre=strtolower($nombre_archivo);
	$nuevo_nombre=trim($nuevo_nombre);
	$nuevo_nombre=str_replace(" ","_",$nuevo_nombre);
	$nuevo_nombre=str_replace(",","_",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[��ª���]","a",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[������]","e",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[������]","i",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[�������]","o",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[������]","u",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[��]","c",$nuevo_nombre);
	$nuevo_nombre=ereg_replace("[��]","n",$nuevo_nombre);
	return $nuevo_nombre;
}
function redimensionar($nombre,$ruta)
{
	if(copy($_FILES[$nombre]["tmp_name"],$ruta))
	{
		list($width, $height) = getimagesize($ruta);
		$newwidth = IMG_ANCHO;
		$newheight = floor((IMG_ANCHO * $height) / $width);
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($ruta);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagejpeg($thumb,$ruta);
		imagedestroy($thumb);
		return true;
	}
	else
		return false;	
}
function crear()
{
	if($_POST["modificar"])
		$nombre_archivo=$_POST["modificar"];
	else
		$nombre_archivo=date("y-m-d")."-".filtrar($_POST["titulo"]).".php";
	
	if($_FILES["archivo1"]["type"]=="image/jpeg")
	{
		$nombre_imagen=substr($nombre_archivo,0,strlen($nombre_archivo)-4);
		$nombre_imagen.=".jpg";
		redimensionar("archivo1",$_SERVER["DOCUMENT_ROOT"]."/noticias/files/".$nombre_imagen);
	}
	if($_FILES["archivo2"]["type"]=="image/jpeg")
	{
		$nombre_imagen=substr($nombre_archivo,0,strlen($nombre_archivo)-4);
		$nombre_imagen.="2.jpg";
		redimensionar("archivo2",$_SERVER["DOCUMENT_ROOT"]."/noticias/files/".$nombre_imagen);
	}
	$abierto=fopen("./files/".$nombre_archivo,"w");
	if(fwrite($abierto,$_POST["cuerpo"]))
	{
		fclose($abierto);
		listar();
	}
}
function imagen()
{
	echo '<form name="imagen" action="index.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="modo" value="imagen2">
		<p>(Imagen *.JPG)<input type="file" name="archivo1" size="90"></p>
		<input type="submit" class="boton" value="CREAR">
   	 	</form>';
}
function imagen2()
{
	echo "imagen2...";	
}
function editar()
{
	if($_POST["nombre_archivo"])
	{
		$abierto=fopen("./files/".$_POST["nombre_archivo"],"r");	
		$lector=fread($abierto,filesize("./files/".$_POST["nombre_archivo"]));
		fclose($abierto);
		
		$texto='<form name="editar_texto" action="index.php" method="post">
		<input type="hidden" name="modo" value="crear">
		<input type="hidden" name="modificar" value="'.$_POST["nombre_archivo"].'">
		<textarea name="cuerpo" id="cuerpo" cols="76" rows="22">'.$lector.'</textarea>
    	<div align="right">
			<input type="submit" class="boton" value="CREAR">&nbsp;
			<input type="button" class="boton" value="CANCELAR" onclick=window.open("index.php","_self")>&nbsp;
		</div>
    	</form>';
	}
	else
	{
		$texto='<form name="editar_texto" action="index.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="modo" value="crear">
		<input type="text" name="titulo" size="96" maxlength="64" class="boton_nom">
		<textarea name="cuerpo" id="cuerpo" cols="76" rows="22"></textarea>
		<p>(Imagen *.JPG)<input type="file" name="archivo1" size="90"></p>
		<p>(Imagen *.JPG)<input type="file" name="archivo2" size="90"></p>
    	<div align="right">
			<input type="submit" class="boton" value="CREAR">&nbsp;
			<input type="button" class="boton" value="CANCELAR" onclick=window.open("index.php","_self")>&nbsp;
		</div>
   	 	</form>';
	}
	echo $texto;
}
function borrar()
{
	if($_POST["confirmado"])
	{
		if(unlink("./files/".$_POST["nombre_archivo"]))	listar();
	}
	else
	{
		echo '<p>Esta seguro de eliminar este archivo?</p>
		<form action="index.php" method="post" style="float:left;">
		<input type="hidden" name="modo" value="borrar">
		<input type="hidden" name="nombre_archivo" value="'.$_POST["nombre_archivo"].'">
		<input type="hidden" name="confirmado" value="1">
		<input type="submit" class="boton" value="SI">&nbsp;
		<input type="button" class="boton" value="NO" onclick=window.open("index.php","_self")></form>';
	}			
}
function listar()
{
	$directorio=opendir("./files/"); 
	while($archivo = readdir($directorio))
	{
		if(is_file("./files/".$archivo) && substr($archivo,(strlen($archivo)-3),3)=="php" )
		{
			$ficheros[$i]=$archivo;
       		$i++;
		}	
	}
	closedir($directorio);
	rsort($ficheros);
	$texto='<table cellspacing="0" cellpadding="1">';
	
	for($i=0; $i<count($ficheros); $i++)
	{
		$texto=$texto.'<tr>
		<td width="640"><a href="files/'.$ficheros[$i].'" target="_blank">&raquo;'.$ficheros[$i].'</a></td>
		<td><form action="index.php" method="post"><input type="submit" class="boton2" value="MODIFICAR"><input type="hidden" value="editar" name="modo"><input type="hidden" value="'.$ficheros[$i].'" name="nombre_archivo"></form></td>
		<td><form action="index.php" method="post"><input type="submit" class="boton2" value="ELIMINAR"><input type="hidden" value="borrar" name="modo"><input type="hidden" value="'.$ficheros[$i].'" name="nombre_archivo"></form></td>
		</tr>';
	}
	echo $texto.'</table>';
}
function ultimas($cuantas)
{
	$ruta=$_SERVER["DOCUMENT_ROOT"]."/noticias/files/";
	$contador=0;	
	$directorio=opendir($ruta); 
	
	while($archivo = readdir($directorio))
	{
		if(is_file($ruta.$archivo) && substr($archivo,(strlen($archivo)-3),3)=="php")
		{
			$ficheros[$i]=$archivo;
       		$i++;
		}	
	}
	closedir($directorio);
	rsort($ficheros);
	($cuantas)? $hasta=count($ficheros) : $hasta=5;
	
	for($i=0; $i<$hasta; $i++)
	{
		$fecha=explode("-",$ficheros[$i]);
		$fechatotal=$fecha[2]." de ".constant("MES_".$fecha[1])." de 20".$fecha[0];		
		$abierto=fopen($ruta.$ficheros[$i],"r");
		$lector=fread($abierto,filesize($ruta.$ficheros[$i]));

		$lector=explode("</p>",$lector);
		$archivo_imagen=substr($ficheros[$i],0,strlen($ficheros[$i])-4);
		
		$texto=$texto.'<div class="noticia">
		<a title="Noticias" href="http://'.$_SERVER["HTTP_HOST"].'/prensa/articulo.php?noticia='.$ficheros[$i].'">
		<img src="http://'.$_SERVER["HTTP_HOST"].'/noticias/files/'.$archivo_imagen.'.jpg" onerror=this.src="http://'.$_SERVER["HTTP_HOST"].'/img/ico_alu.gif" alt="Noticias" border="0">'.$fechatotal.$lector[0].'</p>
		M&aacute;s informaci&oacute;n...</a></div>';
		fclose($abierto);
	}
	echo $texto;
}
?>