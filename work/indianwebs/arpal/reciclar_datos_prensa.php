<?php include("0cabecera.php");?>
		
    <div class="cuerpo">
    	<h2>Nota de prensa recuperaci&oacute;n ENVASES DE ALUMINIO <?php echo $_GET["fecha"];?></h2>
		<?php switch($_GET["fecha"]) {
			
		default:
		case 2007: ?>
        <p><em><b>ESPA&Ntilde;A RECUPERA 14.144 TONELADAS DE ALUMINIO PROVENIENTE DE ENVASES EN  EL A&Ntilde;O 2007</b></em></p>
        <p><b>El 2007 fue un buen a&ntilde;o para la recuperaci&oacute;n de envases de aluminio ya  que se reciclaron, en valor absoluto, m&aacute;s de 14.000 toneladas. Esta cifra se ha  obtenido gracias a los datos ofrecidos por Ecoembes y a los datos de un estudio  que realiza Arpal entre los recuperadores tradicionales, un canal muy  importante de recuperaci&oacute;n de aluminio.</b></p>
        <p>Todo el aluminio que se recupera  se recicla ya que este material tiene un alto valor en el mercado de materias  primas recuperadas; de hecho es el material de envase por el que se paga un  precio m&aacute;s alto. Gracias a sus m&uacute;ltiples propiedades, el aluminio puede  reciclarse indefinidamente sin perder calidad y en sus &ldquo;posteriores vidas&rdquo;  puede convertirse en cualquier producto de aluminio (nuevas latas, perfiles de  ventanas, motores, etc.). En sectores como la construcci&oacute;n o el transporte, la  recuperaci&oacute;n de aluminio alcanza tasas pr&oacute;ximas al 100% y en el de los envases  esta cifra es inferior por dos razones b&aacute;sicas: es un material de envase  relativamente nuevo (en Espa&ntilde;a empezaron a fabricarse latas de aluminio en el  a&ntilde;o 1993) y a su atomizaci&oacute;n, ya que los envases usados se encuentran  dispersos.</p>
        <p>Todos estos factores hacen que la  recuperaci&oacute;n de envases de aluminio sea algo diferente a la recuperaci&oacute;n  del&nbsp; resto de los materiales utilizados  en envases y embalajes. Debido a su alto precio en el mercado de materias  primas, los recuperadores tradicionales son muy importantes en el proceso de su  recuperaci&oacute;n. El estudio que realiza <b>Arpal</b> anualmente permite contabilizar la cantidad de envases de aluminio que estos  profesionales reciben en sus plantas, procesan y env&iacute;an a fundici&oacute;n para su  reciclaje. Esta investigaci&oacute;n se lleva a cabo desde hace siete a&ntilde;os y se basa  en conocer &ldquo;puerta a puerta&rdquo; la cantidad de aluminio recuperada por las plantas  de los recuperadores tradicionales, antiguamente llamados chatarreros. 
        El  estudio cuenta con la colaboraci&oacute;n de la Federaci&oacute;n Espa&ntilde;ola de la Recuperaci&oacute;n  (FER), el Gremi de Recuperaci&oacute; de Catalunya y otras entidades del sector de  diversos puntos de la geograf&iacute;a espa&ntilde;ola y cuenta, adem&aacute;s,&nbsp; con el patrocinio de Ecoembes. </p>
        <p>Gracias a esta investigaci&oacute;n  anual, se pueden ofrecer cifras totalmente veraces y certificadas del aluminio  proveniente de los envases que se ha recuperado (y por tanto, reciclado ya que  todo el aluminio que se recoge se recicla), adem&aacute;s de tener una verdadera  radiograf&iacute;a del sector de la recuperaci&oacute;n de envases de aluminio en Espa&ntilde;a.  Desde <b>Arpal</b> tambi&eacute;n se est&aacute;  trabajando para que el aluminio recuperado tenga cada vez m&aacute;s calidad y no se  encuentre contaminado con otros materiales ya que, cuando esto ocurre, se  dificulta el proceso de selecci&oacute;n y preparaci&oacute;n para su reciclado y disminuye su  precio en el mercado de materias secundarias (y, por lo tanto, uno de los  principales incentivos para su recuperaci&oacute;n a trav&eacute;s de todos los canales).</p>
        <p>Jos&eacute; Miguel Benvavente, Secretario  General de <b>Arpal</b> destaca, sobre las  cifras de recuperaci&oacute;n de envases de aluminio de este a&ntilde;o, que ha crecido tanto  en volumen como en unidades, as&iacute; como la importancia de las recogidas  complementarias (recuperadores tradicionales). &ldquo;Este hecho es l&oacute;gico, y la  raz&oacute;n principal se encuentra en el alto precio que el aluminio alcanza en el  mercado de materias recicladas. El porcentaje de recuperaci&oacute;n ha aumentado en  relaci&oacute;n con el a&ntilde;o anterior. En el 2007 hemos alcanzado el 27% y en el 2006  fue un 24%. Esta cifra es la m&aacute;s alta desde que iniciamos las estad&iacute;sticas  sobre reciclado. &ldquo;</p>
        <p>Esta tasa del 27% se considera,  desde <b>Arpal</b>, todav&iacute;a baja y las  razones podr&iacute;an ser que &ldquo;la recogida selectiva debe estar orientada a  materiales de gran volumen y tambi&eacute;n, tal vez, a los altos precios del aluminio  alcanzado en el LME en el 2007, que termin&oacute; el a&ntilde;o en una cotizaci&oacute;n aproximada  de 2300 $/tonelada (settlement cash seller). Para el contrato &ldquo;aluminium alloy&rdquo;  el precio fue bastante pr&oacute;ximo al del primary aluminium&rdquo;, nos explica el  director general de la asociaci&oacute;n.</p>
        <p>&iquest;Qu&eacute; nos depara el futuro  respecto a la recuperaci&oacute;n de envases de aluminio? Jos&eacute; Miguel Benavente lo  tiene muy claro: &ldquo;Las mejoras en la tecnolog&iacute;a de selecci&oacute;n, procesos  adicionales en recuperadores y el crecimiento de los precios, inducido por las  tensiones de demanda de materias primas provocadas por grandes pa&iacute;ses&nbsp; como China e India, nos hacen prever un&nbsp; incremento en la tasa de reciclado.&rdquo;</p>
        <p><img title="Datos de recuperacion de aluminio 2007" src="img/datos_2007.gif" alt="Datos de recuperacion de aluminio 2007" /></p>
        <p><b>RECUPERACI&Oacute;N  DE ENVASES DE ALUMINIO EN ESPA&Ntilde;A 2007</b></p>
        <table align="center" cellpadding="4" border="0">
      	<tr><td><b>ORIGEN</b></td><td><b>TONELADAS</b></td></tr>
      	<tr><td>Recuperadores Tradicionales (Estudio  ARPAL)</td><td>4.545</td></tr>
      	<tr><td>Escorias de Incineraci&oacute;n (Estudio ARPAL)</td><td>1.844</td></tr>
      	<tr><td>Plantas de Selecci&oacute;n (ECOEMBES)</td><td>2.846</td></tr>
      	<tr><td>Plantas de R.S.U. (ECOEMBES)</td><td>3.372</td></tr>
      	<tr><td>Recogida Complementaria (ECOEMBES)</td><td>1.537</td></tr>
        <tr><td><b>Total Recuperaci&oacute;n 2007</b></td><td><b>14.144</b></td></tr>
      	</table>
        <? break;
		
		case 2006: ?>
		<p><em><b>ESPA&Ntilde;A RECUPERA 12.213 TONELADAS DE ENVASES DE ALUMINIO EN EL A&Ntilde;O 2006</b></em></p>
      <p>Despu&eacute;s de realizar una exhaustiva investigaci&oacute;n entre todos los canales donde se recupera aluminio, se ha llegado a la cifra de 12.213 toneladas de envases de aluminio recuperadas en el a&ntilde;o 2006. Este dato incluye las cifras de Ecoembes de material recogido en plantas de selecci&oacute;n, plantas de compostaje y de RSU y en recogidas complementarias, as&iacute; como las obtenidas por los estudios que realiza Arpal entre los recuperadores tradicionales y las plantas de incineraci&oacute;n.</p>
      <p>Para poder conocer perfectamente las toneladas de envases de aluminio recuperadas en Espa&ntilde;a se deben analizar todos los canales de recuperaci&oacute;n. Ecoembes, gestor del Sistema Integrado de Gesti&oacute;n de envases, ofrece las cifras obtenidas en plantas de selecci&oacute;n, plantas de Residuos S&oacute;lidos Urbanos, plantas de compostaje y recogidas complementarias. Debido a la particularidad del aluminio de ser un metal muy valioso (tiene el valor de mercado m&aacute;s alto comparado con el resto de materiales de envase) existe toda una red centenaria de recuperadores tradicionales de este metal. Por ello, hace ya seis a&ntilde;os que <b>Arpal</b> promovi&oacute; realizar una investigaci&oacute;n &ldquo;puerta por puerta&rdquo; entre los &ldquo;chatarreros&rdquo; o recuperadores tradicionales, investigaci&oacute;n que se realiza con la Federaci&oacute;n Espa&ntilde;ola de Recuperaci&oacute;n, el Gremi de Recuperacio de Catalunya y otras asociaciones de recuperadores.</p>
      <p>A continuaci&oacute;n ofrecemos las cifras globales de envases de aluminio recuperados por los diferentes canales durante el a&ntilde;o 2006, cifras totalmente certificables:</p>
      <ul>
      <li>Plantas de selecci&oacute;n: 3.297,51 toneladas</li>
      <li>Plantas RSU/plantas compostaje:1.465,56 toneladas</li>
      <li>Recogidas complementarias: 1.465.56 toneladas</li>
      <li>Recuperadores tradicionales (estudioArpal): 4030,20 toneladas</li>
      <li>Escorias incineraci&oacute;n: 1954,08 toneladas</li>
      </ul>
      <p><b>TOTAL ENVASES ALUMINIO </b><b>RECUPERADOS EN EL A&Ntilde;O 2006: 12.213 toneladas</b></p>
      <p>Seg&uacute;n estos datos, en 2006 se recuperaron en Espa&ntilde;a 4.030,20 toneladas de envases de aluminio proveniente de recuperadores (33% del total) y 1.954,08 toneladas de aluminio (16% del total) de las escorias de incineraci&oacute;n. Los datos de Ecoembes apuntan a 6.228,83 toneladas de envases de aluminio recuperadas y recicladas durante el a&ntilde;o 2006 en plantas de selecci&oacute;n, plantas de compostaje y de RSU y recogidas complementarias; si a esta cifra sumamos las recuperadas en los recuperadores tradicionales obtenemos un total de envases de aluminio recuperados en 2006 de 12.213 toneladas.</p>
		<? break;

		case 2005:?>
		<p><em><b>10.231 TONELADAS DE ENVASES DE ALUMINIO RECUPERADAS EN EL A&Ntilde;O 2005</b></em></p>
      <p><b>Despu&eacute;s de realizar una exhaustiva investigaci&oacute;n entre todos los canales donde se recupera aluminio, se ha llegado a la cifra de 10.231 toneladas de envases de aluminio recuperadas en el a&ntilde;o 2005. Este dato incluye las cifras de Ecoembes de material recogido en plantas de selecci&oacute;n, plantas de compostaje y de RSU y en recogidas complementarias, as&iacute; como las obtenidas por los estudios que realiza Arpal entre los recuperadores tradicionales y las plantas de escorias de incineraci&oacute;n.</b></p>
      <p>Las cifras globales de envases de aluminio recuperados por los diferentes canales durante el a&ntilde;o 2005 son las siguientes:</p>
      <ul>
      <li>Plantas de selecci&oacute;n:1.632 toneladas envases de aluminio</li>
      <li>Plantas RSU/plantas compostaje:1.496 toneladas envases de aluminio</li>
      <li>Recogidas complementarias:3.345 toneladas envases de aluminio</li>
      <li>Recuperadores tradicionales (estudio Arpal):3.758 toneladas envases de aluminio</li>
      </ul>
      <p><b>TOTAL ENVASES ALUMINIO </b><b>RECUPERADOS EN EL A&Ntilde;O 2004: 10.231 toneladas</b></p>
      <p>Seg&uacute;n estos datos, en 2005 se recuperaron en Espa&ntilde;a 3.758 toneladas de envases de aluminio proveniente de recuperadores (36% del total), 3.354 toneladas de aluminio (32% del total) de las recogidas complementarias y 1.496 toneladas en plantas de RSU y compostaje (contenedor amarillo), el 14%.</p>
      <p>Para poder conocer perfectamente las toneladas de envases de aluminio recuperadas en Espa&ntilde;a se deben analizar todos los canales de recuperaci&oacute;n. Ecoembes, gestor del Sistema Integrado de Gesti&oacute;n, ofrece las cifras obtenidas en plantas de selecci&oacute;n, plantas de Residuos S&oacute;lidos Urbanos, plantas de compostaje y recogidas complementarias.</p>
      <p>Debido a la particularidad del aluminio de ser un metal muy valioso (tiene el valor de mercado m&aacute;s alto comparado con el resto de materiales de envase) existe toda una red centenaria de recuperadores tradicionales de este metal. Por ello, hace ya cinco a&ntilde;os que <b>Arpal</b> promovi&oacute; realizar una investigaci&oacute;n &ldquo;puerta por puerta&rdquo; entre los &ldquo;chatarreros&rdquo; o recuperadores tradicionales, investigaci&oacute;n que se realiza con la Federaci&oacute;n Espa&ntilde;ola de Recuperaci&oacute;n, el Gremi de Recuperaci&oacute; de Catalunya y otras asociaciones de recuperadores y cuyo resultado se suma a los datos que ofrece Ecoembes sobre la recogida a trav&eacute;s del contenedor amarillo.</p>
		<? break;
			
		case 2004:?>
		<p><em><b>10.427 TONELADAS DE ENVASES DE ALUMINIO RECUPERADAS EN EL A&Ntilde;O 2004</b></em></p>
      <p><b>Despu&eacute;s de realizar una exhaustiva investigaci&oacute;n entre todos los canales donde se recupera aluminio, se ha llegado a la cifra de 10.427 toneladas de envases de aluminio recuperadas en el a&ntilde;o 2004. Este dato incluye las cifras de Ecoembes de material recogido en plantas de selecci&oacute;n, plantas de compostaje y de RSU y en recogidas complementarias, as&iacute; como las obtenidas por los estudios que realiza Arpal entre los recuperadores tradicionales y las plantas de escorias de incineraci&oacute;n.</b></p>
      <p>Para poder conocer perfectamente las toneladas de envases de aluminio recuperadas en Espa&ntilde;a se deben analizar todos los canales de recuperaci&oacute;n. Ecoembes, gestor del Sistema Integrado de Gesti&oacute;n, ofrece las cifras obtenidas en plantas de selecci&oacute;n, plantas de Residuos S&oacute;lidos Urbanos, plantas de compostaje y recogidas complementarias. Debido a la particularidad del aluminio de ser un metal muy valioso (tiene el valor de mercado m&aacute;s alto comparado con el resto de materiales de envase) existe toda una red centenaria de recuperadores tradicionales de este metal. Por ello, hace ya cuatro a&ntilde;os que <b>Arpal</b> promovi&oacute; realizar una investigaci&oacute;n &ldquo;puerta por puerta&rdquo; entre los &ldquo;chatarreros&rdquo; o recuperadores tradicionales, investigaci&oacute;n que se realiza con la Federaci&oacute;n Espa&ntilde;ola de Recuperaci&oacute;n, el Gremi de Recuperaci&oacute; de Catalunya y otras asociaciones de recuperadores.</p>
      <p>A continuaci&oacute;n ofrecemos las cifras globales de envases de aluminio recuperados por los diferentes canales durante el a&ntilde;o 2004 que, en conjunto, nos ofrecen la verdadera radiograf&iacute;a del sector:</p>
      <ul>
      <li>Plantas de selecci&oacute;n: 1.262 toneladas envases de aluminio</li>
      <li>Plantas RSU/plantas compostaje:1.316 toneladas envases de aluminio</li>
      <li>Recogidas complementarias (incluye escorias de incineraci&oacute;n):4.412 toneladas de envases de aluminio</li>
      <li>Recuperadores tradicionales (estudio Arpal): 3.437 toneladas de envases de aluminio</li>
      </ul>
      <p><b>TOTAL ENVASES ALUMINIO </b><b>RECUPERADOS EN EL A&Ntilde;O 2004: 10.427 toneladas</b></p>
      <p>Seg&uacute;n estos datos, en 2004 se recuperaron en Espa&ntilde;a 3.437 toneladas de envases de aluminio proveniente de recuperadores (33% del total) y 2.711 toneladas de aluminio (26% del total) de las escorias de incineraci&oacute;n. Los datos de Ecoembes apuntan a 6.990 toneladas de envases de aluminio recuperadas y recicladas durante el a&ntilde;o 2004 en plantas de selecci&oacute;n, plantas de compostaje y de RSU y recogidas complementarias; si a esta cifra sumamos las recuperadas en los recuperadores tradicionales obtenemos un total de envases de aluminio recuperados en 2004 de 10.427 toneladas. </p>
      <p><b>C&Oacute;MO SE OBTIENEN LOS DATOS</b></p>
      <p>Para reducir el impacto sobre el medio ambiente producido por los envases y residuos de envases, as&iacute; como para garantizar el funcionamiento del mercado interior, la Uni&oacute;n Europea adopt&oacute; la Directiva 94/62/CE, recientemente modificada por la 2004/12/CE. Esta Directiva fue incorporada a nuestro ordenamiento jur&iacute;dico mediante la Ley 11/1997de 24 de abril de Envases y Residuos de Envases, estableci&eacute;ndose as&iacute; para Espa&ntilde;a unos objetivos de reciclado y valorizaci&oacute;n, en consonancia con lo marcado por la Uni&oacute;n Europea.</p>
      <p>Seg&uacute;n la nueva normativa, para conseguir estos objetivos, se permiten dos sistemas: los Sistemas integrados de gesti&oacute;n de residuos de envases (SIG) y el Sistema de dep&oacute;sito, devoluci&oacute;n y retorno.</p>
      <p>El sistema implantado en nuestro pa&iacute;s es el Sistema Integrado de Gesti&oacute;n a trav&eacute;s de Ecoembalajes Espa&ntilde;a que est&aacute; funcionando satisfactoriamente para la mayor&iacute;a de los materiales. Parece sensato esperar y ver qu&eacute; ocurre en otros pa&iacute;ses en los que est&aacute; implantado o se va a implantar el sistema de dep&oacute;sito para, teniendo en cuenta sus condicionantes y resultados, adoptar aqu&iacute; la soluci&oacute;n m&aacute;s conveniente.</p>
      <p>Para poder llevar a cabo el Sistema Integrado de Gesti&oacute;n se deben celebrar acuerdos entre los fabricantes y distribuidores de envases y los recuperadores de residuos. Todos los agentes involucrados en este proceso est&aacute;n estudiando la forma de alcanzar los objetivos establecidos por las normativas en el plazo establecido. La Asociaci&oacute;n para el reciclado de productos de aluminio (<b>Arpal</b>) se suma a estos esfuerzos y realiza desde hace cuatro a&ntilde;os un estudio para evaluar las toneladas de envases de aluminio que se recuperan en Espa&ntilde;a a trav&eacute;s del tejido empresarial de los recuperadores.</p>
      <p>Este estudio pretende ser complementario al realizado por Ecoembes, que recoge las cantidades de aluminio importadas, las que provienen de plantas de selecci&oacute;n, de plantas de compost, o de recogida complementaria. Se realiza a trav&eacute;s de la FER (Federaci&oacute;n Espa&ntilde;ola de Recuperaci&oacute;n), el Gremi de Recuperaci&oacute; de Catalunya y otras asociaciones de recuperadores.</p>
      <p>Los recuperadores tradicionales, al estar en el mercado del reciclaje y al ser los chatarreros de toda la vida, son imprescindibles para un estudio anual, sobre todo para un material como el aluminio&rdquo;, explica Jos&eacute; Miguel Benavente, Presidente de <b>Arpal.</b> &ldquo;La metodolog&iacute;a de este estudio tiene que ser <em>sui generis</em>, porque se ha de conocer muy bien el mercado y las personas que lo conforman, y se ha de conseguir que te pasen sus datos a cambio, s&oacute;lo, de confidencialidad. Pero en realidad el m&eacute;todo es sencillo: si no s&eacute; algo, voy y se lo pregunto a alguien que lo sepa&rdquo;.</p>
      <p>Como objetivo prioritario, en este estudio se ha intentado obtener datos absolutamente fiables: &ldquo;Estos datos se recogen directamente del recuperador, y adem&aacute;s, las personas a las que hemos preguntado colaboran constantemente con las asociaciones de recuperadores. Adem&aacute;s, el estudio se basa en una exigencia interna. Son datos absolutamente necesarios, debido a las peculiares caracter&iacute;sticas de este material y porque tenemos una nueva legislaci&oacute;n que nos marca objetivos determinados para el 2008. El estudio se realiza con profesionalidad y analiza en profundidad los datos recogidos por Ecoembes para no duplicar resultados&rdquo;, puntualiza el presidente de <b>Arpal.</b></p>
      <p>Los recuperadores han sido, tradicionalmente los que han gestionado el aluminio recuperado y todav&iacute;a hoy son una fuente important&iacute;sima, que se debe contemplar, aunque en ocasiones obtener estas cifras no es tarea f&aacute;cil.</p>
      <p>&ldquo;Hay que tener en cuenta que, a veces, los recuperadores, especialmente con el aluminio, son reacios a dar cifras para proteger sus fuentes de suministro de material. Es un mercado muy r&aacute;pido. Por eso es tan importante obtener una encuesta fiable como la que hemos presentado. Y tambi&eacute;n, por eso, los datos del contenedor amarillo, del sistema pensado como SIG, se tienen que complementar con este estudio ya que hoy por hoy los recuperadores tradicionales juegan un papel important&iacute;simo en la recuperaci&oacute;n de este material. Quien quiere recuperar el envase de aluminio, acaba pasando por recuperadores, por lo que nos encontramos en un lugar estrat&eacute;gico por donde circula la mayor parte del producto&rdquo;.</p>
		<? break;
			
		case 2003:?>
		<p><em><b>EL RECICLADO DE ENVASES DE ALUMINIO ALCANZA EL 25,6% EN EL A&Ntilde;O 2003</b></em></p>
      <p>Los estudios realizados para conocer el reciclado de los envases de aluminio en el a&ntilde;o 2003 han revelado unos resultados mucho mejores que en a&ntilde;os anteriores: en el ejercicio 2003 se han recuperado 11.710 toneladas de envases de este material, que representan el 25,6% del total consumido.</b></p>
      <p>Los diversos estudios que realizan <b>Arpal</b> y Ecoembes para conocer y certificar las toneladas de envases de aluminio recuperados en Espa&ntilde;a han constatado que el pasado a&ntilde;o se recuperaron en Espa&ntilde;a 11.710 toneladas de envases de aluminio por diferentes v&iacute;as:</p>
      <ul>
      <li>Plantas de compostaje 1.295 toneladas</li>
      <li>Recogida selectiva (contenedor amarillo) 1.218 toneladas</li>
      <li>Escorias de incineraci&oacute;n 2.663 toneladas</li>
      <li>Recogidas complementarias (chatarreros tradicionales y otros) 6.534 toneladas</li>
      </ul>
      <p>El an&aacute;lisis de estas cifras muestra que el incremento del reciclado durante el a&ntilde;o 2003 fue de ocho puntos respecto al a&ntilde;o 2002, teniendo en cuenta que el consumo del a&ntilde;o pasado fue de 45.656.</p>
      <p>El incremento se ha debido a varios factores: por un lado, el mejor funcionamiento del Sistema Integrado de Gesti&oacute;n, que cuenta con m&aacute;s plantas de recogida de envases y que adem&aacute;s funcionan de forma m&aacute;s eficaz. En segundo lugar, el estudio que realiza Arpal entre los recuperadores tradicionales, que siguen siendo una fuente importante de recuperaci&oacute;n, est&aacute; ya en una etapa m&aacute;s madura, por lo que la recogida de informaci&oacute;n es m&aacute;s amplia.</p>
      <p>Esta investigaci&oacute;n, que ahora se ha llevado a cabo por cuarto a&ntilde;o consecutivo, se basa en el contacto personal con los recuperadores de toda Espa&ntilde;a para conocer de forma directa y ver&iacute;dica cu&aacute;l es la verdadera cifra de reciclado de envases de aluminio. Por &uacute;ltimo, en el a&ntilde;o 2002 se vivi&oacute; una evoluci&oacute;n decreciente en el precio del aluminio en la Bolsa de Metales de Londres y en el 2003 esta tendencia fue totalmente opuesta, es decir, los precios aumentaron. Este efecto provoc&oacute; un retraimiento de la oferta de envase usado en el 2002 que sali&oacute; al mercado, al incrementar los precios, durante el 2003.</p>
      <p>De todos los envases que forman el Sistema Integrado de Gesti&oacute;n de Ecoembes, el aluminio es el &uacute;nico que cotiza en Bolsa (en la London Metal Exchange &ndash;LME-) y el aumento de precio del a&ntilde;o pasado fue debido principalmente a la demanda espectacular de China, cuyo crecimiento econ&oacute;mico provoca la necesidad de todo tipo de materiales, entre ellos el aluminio.</p>
      <p>Respecto al estudio realizado por Arpal entre los recuperadores tradicionales (chatarreros) y que realiza conjuntamente con la Federaci&oacute;n Espa&ntilde;ola de Recuperaci&oacute;n (FER), el Gremi de Recuperaci&oacute; de Catalunya y otras entidades de recuperadores de diversas Comunidades Aut&oacute;nomas, ha constatado que esta v&iacute;a de recogida gestion&oacute; el pasado a&ntilde;o 4.087 toneladas de envases de aluminio, siendo las latas de bebidas el 78% del total recogido.</p>
      <p>Los datos, por tipo de envase recuperado, pueden verse en la siguiente tabla:</p>
      <table align="center" cellpadding="4" border="0">
      <tr><td><b>TIPO DE ENVASE</b></td><td><b>TONELADAS</b></td><td><b>%</b></td></tr>
      <tr><td>Latas de bebidas</td><td>3.188</td><td>78</td></tr>
      <tr><td>Aerosoles</td><td>449</td><td>11</td></tr>
      <tr><td>Latas de conservas</td><td>245</td><td>6</td></tr>
      <tr><td>Semir&iacute;gidos</td><td>82</td><td>2</td></tr>
      <tr><td>C&aacute;psulas</td><td>123</td><td>3</td></tr>
      </table>
      <p>Gracias a este estudio, se est&aacute; consiguiendo aumentar la cifra de envases de aluminio recuperados ya que los recuperadores tradicionales, que intervienen en la comercializaci&oacute;n de los productos de aluminio, est&aacute;n cada vez m&aacute;s interesados en revelar la cifra de envases que recuperan, cifra que hace cuatro a&ntilde;os era completamente desconocida. De esta manera, se est&aacute; creando una verdadera sinergia entre Arpal y Ecoembes que beneficia, en definitiva, a la fiabilidad de los datos, ya que ahora pueden ofrecerse cifras ver&iacute;dicas y contrastadas de material recuperado y reciclado puesto que todo el aluminio recuperado vuelve a convertirse en materia prima.</p>
      <p>Es de destacar tambi&eacute;n que, para conocer los envases recuperados en las escorias de incineradoras, Arpal inici&oacute; el a&ntilde;o pasado otro estudio entre estas instalaciones, que ahora se ha realizado por segundo a&ntilde;o y cuyas cifras ya est&aacute;n incluidas en el total de los envases recuperados.</p>
      <p>La recuperaci&oacute;n de envases de aluminio es una actividad plenamente consolidada gracias al alto valor que tiene este envase usado en el mercado (superior a 0,70&euro;/kg). De hecho, es el de m&aacute;s valor, muy por encima de los envases de hojalata (0,04&euro;/kg aproximadamente), pl&aacute;stico, vidrio o briks.</p>
      <p>Adem&aacute;s, la recuperaci&oacute;n de este material comporta importantes beneficios medioambientales, ya que el 100% del aluminio puede ser reciclado indefinidamente sin perder calidad y el utilizar aluminio reciclado para fabricar nuevos productos supone un ahorro del 95% de la energ&iacute;a necesaria para su obtenci&oacute;n primaria. Por ello, el aluminio recuperado se recicla en su totalidad.</p>
		<? break;
			
		case 2002:?>
		<p><em><b>LA RECUPERACI&Oacute;N DE ENVASES DE ALUMINIO CRECE UN 44,2% EN EL A&Ntilde;O 2002</b></em></p>
      <p><b>Durante el a&ntilde;o 2002 se recuperaron en Espa&ntilde;a 11.063 toneladas de envases de aluminio, cifra que representa un incremento del 44.2% respecto a los datos del 2001, cuando se recogieron 7.668 toneladas. Los recuperadores tradicionales (&ldquo;chatarreros&rdquo;) siguen siendo la mayor fuente de recuperaci&oacute;n, aunque la participaci&oacute;n del contenedor amarillo ha aumentado considerablemente (34%).</b></p>
      <p>Por tercer a&ntilde;o consecutivo, <b>Arpal</b> ha realizado un estudio exhaustivo para conocer las toneladas de envases de aluminio recuperados en Espa&ntilde;a, los canales de recuperaci&oacute;n y la distribuci&oacute;n nacional de los mismos. Esta investigaci&oacute;n, llevada a cabo con la colaboraci&oacute;n de la Federaci&oacute;n Espa&ntilde;ola de la Recuperaci&oacute;n (FER) as&iacute; como del Gremi de Recuperaci&oacute; de Catalunya y otras entidades del sector de la recuperaci&oacute;n como PIMEV, FEMEPA, Recometal y Armetal, permite obtener una completa radiograf&iacute;a de la recuperaci&oacute;n de los envases de aluminio en Espa&ntilde;a.</p>
      <p>Los envases de aluminio recuperados en el a&ntilde;o 2002, seg&uacute;n datos de este estudio y de ECOEMBES, son los siguientes:</p>
      <table align="center" cellpadding="4">
      <tr><td><b>CANAL DE RECUPERACI&Oacute;N</b></td><td><b>ENVASES ALUMINIO (TONELADAS)</b></td></tr>
      <tr><td>Plantas de selecci&oacute;n</td><td>1.546</td></tr>
      <tr><td>Plantas de compostaje / RSU</td><td>1.313</td></tr>
      <tr><td>Recogidas complementarias </td><td>1.907</td></tr>
      <tr><td>Recuperadores tradicionales </td><td>4.101</td></tr>
      <tr><td>Recuperaci&oacute;n en escorias</td><td>2.196</td></tr>
      <tr><td>TOTAL</td><td>11.063</td></tr>
      </table>
      <p><b>(Los datos relativos a las plantas de selecci&oacute;n, de compostaje y RSU y de recogidas complementarias son de Ecoembes y el de los recuperadores tradicionales del estudio de arpal).</b></p>
      <p>Cabe apreciar los incrementos obtenidos en tanto en plantas de selecci&oacute;n como de compostaje, debidos a la puesta en marcha de nuevas plantas as&iacute; como a una mayor eficacia de las ya existentes.</p>
      <p>La recuperaci&oacute;n de envases de aluminio es una actividad plenamente consolidada gracias al alto valor que tiene este envase usado en el mercado (alrededor de 0,60 &euro;/kg.). De hecho, es el m&aacute;s valor, muy por encima de los envases de hojalata (0,04 &euro;/kg aprox.), pl&aacute;stico, vidrio o briks.</p>
      <p>Adem&aacute;s, la recuperaci&oacute;n de este material comporta importantes beneficios medioambientales ya que el 100% del aluminio puede ser reciclado indefinidamente sin perder calidad y, el utilizar aluminio reciclado para fabricar nuevos productos supone un ahorro del 95% de la energ&iacute;a necesaria para su obtenci&oacute;n primaria. Por ello, todo el aluminio recuperado se recicla en su totalidad.</p>
      <p>Volviendo al estudio, el tipo de envases recuperados por los recuperadores tradicionales y su proporci&oacute;n en el a&ntilde;o 2002 ha sido el siguente: 61% botes de bebidas, 13% conservas, 12% aerosoles, 8% c&aacute;psulas y 6% semirr&iacute;gidos (bandejas de precocinados, etc).</p>
      <p>Si se analiza por Comunidades Aut&oacute;nomas los envases recogidos en los recuperadores tradicionales, Catalu&ntilde;a recupera el mayor n&uacute;mero de envases de aluminio, con un participaci&oacute;n del 29% del total; le siguen la Comunidad Valenciana (13%), Comunidad de Madrid (11%) y el Pa&iacute;s Vasco y Asturias (10%).</p>
      <p>La cifra de envases recuperados por habitante revela que los m&aacute;s &ldquo;recicladores&rdquo; son los asturianos (23.169 envases cada 1000 habitantes), seguidos de los vascos (11.635) y catalanes (10.740), siendo la media nacional 5.786 envases cada 1000 habitantes. Por encima de la media, adem&aacute;s de las Comunidades citadas, se encuentran tambi&eacute;n la Comunidad Valenciana y Navarra.</p>
		<? break;
			
		case 2001:?>
		<p><em><b>LOS RECUPERADORES TRADICIONALES RECUPERAN EL 70% DELTOTAL DE ENVASES DE ALUMINIO RECICLADOS EN EL 2001</b></em></p>
      <p><b>El total de envases de aluminio recuperados en el 2001 fue de 9.761 toneladas, que equivalen al 24,20% del total. El sector factur&oacute; m&aacute;s de 9 millones de euros (1.500 millones de pesetas). </b></p>
      <p>Seg&uacute;n se desprende de un Estudio exhaustivo llevado a cabo por arpal (Asociaci&oacute;n para el Reciclado de los Productos de Aluminio), y en el que ha participado la Federaci&oacute;n Espa&ntilde;ola de la Recuperaci&oacute;n (FER), el Gremi de Recuperaci&oacute; de Catalunya, y otras asociaciones del sector como ARMETAL, PIMEV y FEMEPA, es el recuperador tradicional el colectivo m&aacute;s importante en la recuperaci&oacute;n de envases de aluminio, ya que recuperan cerca del 70% el total de estos envases.</p>
      <p>Este Estudio ha completado el panorama de la recuperaci&oacute;n de envases de aluminio en Espa&ntilde;a en el a&ntilde;o 2001, al a&ntilde;adir los datos obtenidos a los de otros canales como son el contenedor amarillo, los recuperadores complementarios o el compostaje, ofrecidos por ECOEMBALAJES ESPA&Ntilde;A, S.A. (ECOEMBES). Hoy en d&iacute;a, podemos decir que los datos que a continuaci&oacute;n se ofrecen son una fiel radiograf&iacute;a del sector.</p>
      <p><b>RECICLADO DE ENVASES DE ALUMINIO EN ESPA&Ntilde;A EN EL A&Ntilde;O 2001 (en toneladas)</b></p>
      <table align="center" cellpadding="4">
      <tr><td>Contenedor amarillo</td><td>1.067</td></tr>
      <tr><td>Recuperadores complementarios</td><td>1.289</td></tr>
      <tr><td>Recuperadores tradicionales</td><td>6.800*</td></tr>
      <tr><td>Compostaje</td><td>635</td></tr>
      <tr><td>TOTAL</td><td>9.791</td></tr>
      <tr><td>% RECICLADO **</td><td>24,20% **</td></tr>
      </table>
      <p><b>Fuente: Ecoembes y arpal</b></p>
      <p>* Extrapolaci&oacute;n razonable de los datos obtenidos en el estudio teniendo en cuenta que &eacute;ste no ha cubierto todo el territorio nacional, ni se han podido visitar a todos los Agentes que intervienen en el Sector (Recuperadores, Refinadores, etc.) y que las circunstancias del Mercado no favorec&iacute;an, al 100%, la identificaci&oacute;n de las Tm. recuperadas.</p>
      <p>**Consumo de envases de aluminio en Espa&ntilde;a en el a&ntilde;o 2001: 40.489 Tm.</p>
      <p>% Reciclado: 9.791 / 40.489= 24,2%</p>
      <p>La facturaci&oacute;n total del sector ascendi&oacute; en el 2001 a 1.500 millones de pesetas, es decir, super&oacute; los 9 millones de euros. Adem&aacute;s, las inversiones realizadas durante el pasado a&ntilde;o en cuanto a maquinaria y tecnolog&iacute;a como contenedores espec&iacute;ficos, bandas de triaje, Separadores de Foucault, compactadoras, etc., representaron alrededor de 250 millones de pesetas (unos 1,5 millones de euros) mientras que las inversiones en ingenier&iacute;a de procesos (estudios y consultor&iacute;a) y en instalaciones industriales fue de 125 millones de pesetas (750.000 euros). El sector de la recuperaci&oacute;n de envases de aluminio emplea, aproximadamente, a 40 personas de forma directa y a 100 de forma indirecta.</p>
      <p> Centr&aacute;ndonos en el Estudio, los datos obtenidos sobre recuperadores tradicionales se refieren concretamente a cantidades de envases de aluminio recuperadas en el a&ntilde;o 2001 que no son importadas, ni provienen de planta de selecci&oacute;n, ni de plantas de compost o recogida en masa, ni de recogida complementaria. Es decir, las cifras que ha obtenido este Estudio, corresponden a aquellas toneladas de envases de aluminio que no figuran en las bases de datos de ECOEMBES y que son envases de aluminio clasificados de forma independiente adem&aacute;s del porcentaje de envases de aluminio incluidos en el aluminio-cacharro.</p>
      <p>El resultado final del Estudio ha determinado que el total de envases y residuos de envases de aluminio recuperados en plantas de recuperadores tradicionales en el a&ntilde;o 2001 fue de 4.677 aunque esta cifra debe extrapolarse hasta 6.800 para reflejar fielmente la realidad del sector, seg&uacute;n han realizado los especialistas autores del estudio.</p>
      <p><b>Envases recuperados: </b></p>
      <p>1.- Por tipo de envase: (* Cifras extrapoladas)</p>
      <table align="center" cellpadding="4">
      <tr><td>Botes</td><td>3.308</td><td>4.810 *</td></tr>
      <tr><td>Aerosoles</td>
      <td>593</td><td> 862*</td></tr>
      <tr><td>Latas conserva</td><td>473 </td><td>688*</td></tr>
      <tr><td>Semir&iacute;gidos</td><td>130</td><td>189*</td></tr>
      <tr><td>C&aacute;psulas</td><td>173 </td><td>251*</td></tr>
      <tr><td>TOTAL</td><td>4.677 </td><td>6.800* </td></tr>
      </table>
      <p><b>2.- Por habitantes y por Comunidades Aut&oacute;nomas (Sin extrapolar: Referidas a 4.677 Tm. "tocadas" y expresado en kilos)</b></p>
      <p>En el siguiente cuadro se muestran los kilos de envases de aluminio recuperados cada 1.000 habitantes por Comunidades Aut&oacute;nomas: </p>
      <table align="center" cellpadding="4">
      <tr><td>Pa&iacute;s Vasco: 307</td><td>Catalu&ntilde;a: 211</td></tr>
      <tr><td>C. Valenciana: 182 </td><td>Madrid: 166</td></tr>
      <tr><td>Cantabria: 136</td><td> Navarra: 133</td></tr>
      <tr><td>Media Nacional: 114 </td><td>Islas Canarias: 72</td></tr>
      <tr><td>Andaluc&iacute;a: 64</td><td> Murcia: 53</td></tr>
      <tr><td>Castilla y Le&oacute;n: 48 </td><td>Islas Baleares: 34</td></tr>
      <tr><td>Arag&oacute;n: 21 </td><td>Principado de Asturias: 18</td></tr>
      <tr><td>Galicia: 13 </td><td>La Rioja: 4</td></tr>
      <tr><td>Extremadura: 2 </td><td>Castilla-La Mancha: 1</td></tr>
      </table>
      <p><b>3.- N&uacute;mero de envases recuperados por habitantes y Comunidad Aut&oacute;noma (Sin extrapolar: Referidas a las 4.677 Tm. "tocadas")</b></p>
      <p>A continuaci&oacute;n se ofrece el n&uacute;mero de envases de aluminio recuperados por cada 1000 habitantes y por Comunidades Aut&oacute;nomas, teniendo en cuenta que el peso medio del envase de aluminio es de 17 gramos.</p>
      <table align="center" cellpadding="4">
      <tr><td>Pa&iacute;s Vasco: 18.803</td><td>Catalu&ntilde;a: 12.229</td></tr>
      <tr><td>C. Valenciana: 10.708 </td><td>Madrid: 9.778</td></tr>
      <tr><td>Cantabria: 7.987 </td><td>Navarra: 7.825</td></tr>
      <tr><td>Media Nacional: 6.715 </td><td>Islas Canarias: 4.227</td></tr>
      <tr><td>Andaluc&iacute;a: 3.742 </td><td>Murcia: 3.113</td></tr>
      <tr><td>Castilla y Le&oacute;n: 2.847</td><td> Islas Baleares: 2.008</td></tr>
      <tr><td>Arag&oacute;n: 1.226</td><td>Principado de Asturias: 1.039</td></tr>
      <tr><td>Galicia: 753</td><td> La Rioja; 21</td></tr>
      <tr><td>Extremadura: 110 </td><td>Castilla-La Mancha: 67</td></tr>
      </table>
		<? break;
		} ?>
    </div>
		
<?php include("0piecera.php");?>