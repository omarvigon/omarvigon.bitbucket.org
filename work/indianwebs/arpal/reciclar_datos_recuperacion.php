<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>La recuperaci&oacute;n de envases de aluminio en Espa&ntilde;a</h2>
            <p><a href="reciclar_datos_arpal.php"><b>&raquo; C&oacute;mo obtiene Arpal los datos de recuperaci&oacute;n</b></a></p>
            <p>A&ntilde;o 2007</p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2007">Nota de prensa recuperaci&oacute;n envases de aluminio 2007</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2007.pdf" target="_blank">Estudio sobre la recuperaci&oacute;n de envases de aluminio 2007</a></li>
            </ul>
            
            <p>A&ntilde;o 2006</p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2006">Nota de prensa recuperaci&oacute;n envases de aluminio 2006</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2006.pdf" target="_blank">Estudio sobre la recuperaci&oacute;n de envases de aluminio 2006</a></li>
            </ul>
            <p>A&ntilde;o 2005</p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2005">Nota de prensa recuperaci&oacute;n envases de aluminio 2005</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2005.pdf" target="_blank">Estudio sobre la recuperaci&oacute;n de envases de aluminio 2005</a></li>
            </ul>
            <p>A&ntilde;o 2004: </p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2004">Nota de prensa recuperaci&oacute;n envases de aluminio 2004</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2004.pdf" target="_blank">Estudio sobre la recuperaci&oacute;n de envases de aluminio 2004</a></li>
            </ul>
            <p>A&ntilde;o 2003: </p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2003">Nota de prensa recuperaci&oacute;n envases de aluminio 2003</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2003.pdf" target="_blank">Estudio sobre la recuperaci&oacute;n de envases de aluminio 2003</a></li>
            </ul>
            <p>A&ntilde;o 2002: </p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2002">Nota de prensa recuperaci&oacute;n envases de aluminio 2002</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2002.pdf" target="_blank">Estudio recuperaci&oacute;n envases de aluminio 2002</a></li>
            </ul>
            <p>A&ntilde;o 2001: </p>
            <ul>
              <li><a href="reciclar_datos_prensa.php?fecha=2001">Nota de prensa recuperaci&oacute;n envases de aluminio 2001</a></li>
              <li><a href="http://www.aluminio.org/files/informe_arpal_2001.pdf" target="_blank">Estudio recuperaci&oacute;n envases de aluminio 2001</a></li>
            </ul>
        </div>
		
<?php include("0piecera.php");?>
