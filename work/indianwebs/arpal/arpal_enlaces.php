<?php include("0cabecera.php");?>
		
    <div class="cuerpo">
        <h2>Enlaces de inter&eacute;s</h2>
        <ul>
        <li><img title="Latas de Bebida" src="img/link_latas.gif" align="absmiddle" alt="Latas de bebidas"/> <a href="http://www.latasdebebidas.org" target="_blank">Latas de bebidas</a></li>
        <li><img title="Novelis" src="img/link_novelis.gif" align="absmiddle" alt="Novelis"/> <a href="http://www.novelis.com" target="_blank">Novelis</a></li>
        <li><img title="Aliberico" src="img/link_aliberico2.gif" align="absmiddle" alt="Grupo Aliberico"/> <a href="http://www.aliberico.com" target="_blank">Grupo Alib&eacute;rico</a></li>
        <li><img title="Alcoa" src="img/link_alcoa.gif" align="absmiddle" alt="Alcoa" vspace="4"/> <a href="http://www.alcoa.com" target="_blank">Alcoa</a></li>
        <li><img title="Hydro" src="img/link_hydro.gif" align="absmiddle" alt="Norsk Hydro"/> <a href="http://www.hydro.com" target="_blank">Norsk Hydro</a></li>
        <li><img title="Alcan" src="img/link_alcan.gif" align="absmiddle" alt="Alcan" vspace="4"/> <a href="http://www.alcan.com" target="_blank">Alcan Inc. - Imagination Materialized </a></li>
        <li><img title="Aluminio" src="img/link_eaa.gif" align="absmiddle" alt="Aluminium Association"/> <a href="http://www.aluminium.org" target="_blank">European Aluminium Association</a></li>
        <li><img title="Paisaje Limpio" src="img/link_paisaje.gif" align="absmiddle" alt="Paisaje Limpio"/> <a href="http://www.paisajelimpio.com" target="_blank">Paisaje Limpio</a></li>
        <li><img title="Blipvert" src="img/link_blipvert.gif" align="absmiddle" alt="Blipvert" vspace="4"/> <a href="http://www.blipvert.es" target="_blank">Blipvert</a></li>
        <li><img title="Riba Farre" src="img/link_riba.gif" align="absmiddle" alt="Riba Farre" vspace="2"/> <a href="http://www.ribafarre.com" target="_blank">Comercial Riba Farr&eacute;</a></li>
        <li><img title="Eco Embes" src="img/link_ecoembes.gif" align="absmiddle" alt="Ecoembes" vspace="3"/> <a href="http://www.ecoembes.com" target="_blank">Ecoembes</a></li>
        <li><img title="Recuperacion" src="img/link_recuperacion.gif" align="absmiddle" alt="Federacion Recuperacion"/> <a href="http://www.recuperacion.org" target="_blank">Federaci&oacute;n Espa&ntilde;ola de la Recuperaci&oacute;n</a></li>
        <li><img title="Reasnet" src="img/link_deixalles.gif" align="absmiddle" alt="Fundacion Deixalles"/><a href="http://www.reasnet.com/deixalle" target="_blank">Fundaci&oacute; Deixalles</a></li>
        <li><img title="Gremi Recuperacio" src="img/link_gremi.gif" align="absmiddle" alt="Gremi de Recuperacio" vspace="6"/> <a href="http://www.gremirecuperacio.org" target="_blank">Gremi de Recuperaci&oacute; de Catalunya</a></li>
        <li><img title="Revista Aluminio" src="img/link_aluminio.gif" align="absmiddle" alt="Revista Aluminio" vspace="4"/> <a href="http://www.revistaaluminio.com" target="_blank">Revista Aluminio</a></li>
        <li><img title="Tecno Residuos" src="img/link_tecnoresiduos.gif" align="absmiddle" alt="Tecno Residuos"/> <a href="http://www.tecnoresiduos-r3.com" target="_blank">Tecno Residuos R3</a></li>
        <li><img title="Trinijove" src="img/link_trinijove.gif" align="absmiddle" alt="Trinijove"/> <a href="http://www.trinijove.org" target="_blank">Trinijove</a></li>
        </ul>
    </div>
		
<?php include("0piecera.php");?>
