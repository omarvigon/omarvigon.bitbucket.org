<?php include("noticias/funciones.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Arpal Asociacion para el Reciclado de Aluminio</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)" />
    <meta name="revisit" content="7 days" />
    <meta name="robots" content="index,follow,all" />
    <meta name="description" content="Arpal Asociacion para el Reciclado de Aluminio" />
    <meta name="keywords" content="arpal,reciclado aluminio,reciclar aluminio,reciclaje aluminio,reciclaje" />
    <meta name="lang" content="es,sp,spanish,espanol,castellano" />
    <meta name="author" content="www.indianwebs.com" />
    <link rel="stylesheet" type="text/css" href="http://www.aluminio.org/estilos.css" /> 
    <link rel="shortcut icon" href="favicon.ico" />  
    <script type="text/javascript" language="javascript" src="http://www.aluminio.org/funciones.js"></script> 
</head>

<body>

<div id="contenedor">
    <div class="arriba">
    	<a href="http://www.aluminio.org/index.php"><img title="Arpal Aluminio" src="http://www.aluminio.org/img/arpal_cabecera3.jpg" alt="Arpal Aluminio"/></a>
    </div>
    <div class="central">
    	<div class="izq">
    	<p>
    	<a title="Arpal Presentacion" class="super" href="http://www.aluminio.org/arpal.php">Qui&eacute;nes somos</a>
    	<a title="Funciones de Arpal" class="super">Qu&eacute; hacemos</a>
    	<a title="Arpal ayudan a concienciar" href="http://www.aluminio.org/arpal_concienciar.php" class="sub">Ayudando a concienciar</a>
    	<a title="Arpal ayuda a reciclar" href="http://www.aluminio.org/arpal_reciclar.php" class="sub">Ayudando a reciclar</a>
    	<a title="Comunicando a la socidad" href="http://www.aluminio.org/arpal_comunicando.php" class="sub">Comunicando a la sociedad</a>
    	<a title="Relacion con el sector del aluminio" href="http://www.aluminio.org/arpal_sector.php" class="sub">Trabajando con el sector</a>
    	<a title="Arpal Ecoembes" href="http://www.aluminio.org/arpal_ecoembes.php" class="sub">Arpal en Ecoembes</a>
    	<a title="Sala de Prensa" class="super2">Sala de Prensa</a>
        <a title="Notas de prensa" class="sub" href="http://www.aluminio.org/prensa/index.php">Notas de Prensa y entrevistas</a>
        <a title="Articulos" class="sub" href="http://www.aluminio.org/prensa/articulos_entrevistas.php">Art&iacute;culos</a>
        <a title="Archivo fotografico" class="sub" href="http://www.aluminio.org/prensa/archivo_fotografico.php">Archivo fotogr&aacute;fico</a>
        <a title="Taller de Arpal" class="super" href="http://www.aluminio.org/arpal_taller_educativo.php">Taller Educativo</a>
        <a title="Enlaces" class="super" href="http://www.aluminio.org/arpal_enlaces.php">Enlaces de inter&eacute;s</a>
        <a title="Contacta" class="super" href="http://www.aluminio.org/arpal_contacto.php">Contacto</a>
    	</p>
        </div>