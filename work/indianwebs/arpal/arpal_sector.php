<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Relaci&oacute;n con el sector</h2>
			<p>La <em>Asociaci&oacute;n para el Reciclado de Productos de Aluminio</em> realiza actividades diversas para promover la recuperaci&oacute;n de envases usados de aluminio por todo el territorio espa&ntilde;ol y para  crear una verdadera cultura sobre el reciclado de este material.</p>
            <p><b>Arpal</b> est&aacute; en permanente contacto con asociaciones del sector, recuperadores, Administraci&oacute;n, universidades, etc para enviar informaci&oacute;n de &uacute;ltima hora o establecer l&iacute;neas de colaboraci&oacute;n  con todos sus p&uacute;blicos. Tambi&eacute;n realiza visitas a plantas de recuperaci&oacute;n de envases y a otras instalaciones que recuperan productos de aluminio</p>
        </div>
		
<?php include("0piecera.php");?>
