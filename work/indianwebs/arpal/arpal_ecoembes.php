<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Arpal en Ecoembes</h2>
			<p>La <em>Asociaci&oacute;n para el Reciclado de Productos de Aluminio</em> realiza actividades diversas para promover la recuperaci&oacute;n de envases usados de aluminio por todo el territorio espa&ntilde;ol y para  crear una verdadera cultura sobre el reciclado de este material.</p>
            <p><b>Arpal</b> representa a la industria del aluminio en Ecoembalajes Espa&ntilde;a  S.A. (Ecoembes), el Sistema Integrado de Gesti&oacute;n para la recogida selectiva de envases ligeros usados en todo el territorio espa&ntilde;ol.</p>
            <p>Nuestra asociaci&oacute;n participa en las siguientes actividades:</p>
            <ul>
            <li>Comisi&oacute;n Intermateriales: pol&iacute;tica a desarrollar en Espa&ntilde;a y propuestas a la UE.</li>
            <li>Comisi&oacute;n T&eacute;cnica del Aluminio:</li>
                <ul>
                <li>Definici&oacute;n de las Especificaciones T&eacute;cnicas de Material de Recuperaci&oacute;n.</li>
                <li>Definici&oacute;n de las Especificaciones T&eacute;cnicas de Material de Recuperaci&oacute;n.</li>
                <li>Propuesta   de proyectos de reciclado de envases usados de aluminio.</li>
                </ul>
            <li>Consejos de Administraci&oacute;n de Ecoembes.</li>
            <li>Comisi&oacute;n Delegada de Ecoembes.</li>
            <li>Definici&oacute;n y c&aacute;lculo del valor del Punto Verde en el envase de aluminio. Se mantiene el mismo valor para los a&ntilde;os 2001 y 2002 (8,49 PTA/kilo).</li>
            <li>Estudio de ofertas de material recuperado en las plantas de selecci&oacute;n  y asignaci&oacute;n a recuperador.</li>
            <li>Estudio cuantitativo de los envases de aluminio puestos en el mercado.</li>
            <li>Convenios conjuntos con recuperadores complementarios.</li>
            <li>Participaci&oacute;n en el Plan Estrat&eacute;gico de Ecoembes.</li>
            </ul>
        </div>
		
<?php include("0piecera.php");?>
