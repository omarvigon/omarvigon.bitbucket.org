<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Comunicando a la sociedad</h2>
			<p>La <em>Asociaci&oacute;n para el Reciclado de Productos de Aluminio</em> realiza actividades diversas para promover la recuperaci&oacute;n de envases usados de aluminio por todo el territorio espa&ntilde;ol y para  crear una verdadera cultura sobre el reciclado de este material.</p>
            <p><b>Arpal</b> est&aacute; en permanente contacto con los medios de informaci&oacute;n general y t&eacute;cnica a los que difunde las principales actividades de la asociaci&oacute;n y los datos m&aacute;s relevantes del sector. Cada a&ntilde;o edita una recopilaci&oacute;n de prensa o "clipping" donde se recogen los art&iacute;culos publicados en diversas publicaciones sobre el envase de aluminio y su reciclado. La asociaci&oacute;n dispone de diverso material informativo y gr&aacute;fico al servicio de los medios de comunicaci&oacute;n.</p>
        </div>
		
<?php include("0piecera.php");?>
