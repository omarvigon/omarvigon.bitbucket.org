<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Asociaci&oacute;n para el Reciclado de Productos de Aluminio (ARPAL)</h2>
        	<p><b>Arpal</b> es una asociaci&oacute;n espa&ntilde;ola sin &aacute;nimo de lucro que asegura a sus asociados la m&aacute;xima eficacia en la promoci&oacute;n de los productos de aluminio a trav&eacute;s de su reciclado. Naci&oacute; en abril de 1994 con el objetivo de promover el reciclado de envases y botes de bebidas de aluminio usados y de otros productos del mismo material.</p>
			<p><b>Arpal</b> est&aacute; constituida por las siguientes entidades :</p>
            <ul>
            <li>Alcan Aluminio Espa&ntilde;a, S.A.</li>
            <li>Hydro Aluminium</li>
            <li>Alib&eacute;rico Packaging</li>
            <li>Alcoa</li>
            <li>Novelis</li>
            </ul>
            <p>La misi&oacute;n de <b>Arpal</b> es promover el mayor n&uacute;mero de iniciativas y actividades para fomentar y extender en Espa&ntilde;a el h&aacute;bito de recoger y <em>reciclar productos de aluminio</em>, con el fin de de contribuir a la conservaci&oacute;n del medio ambiente y los recursos naturales mediante la implantaci&oacute;n eficaz de la cultura del reciclado.</p>
            <p>Las actividades de la asociaci&oacute;n est&aacute;n dirigidas hacia la promoci&oacute;n del aluminio a trav&eacute;s de la idea de la <em>recuperaci&oacute;n selectiva de envases usados</em> y otros productos de aluminio, para disminuir el volumen de &eacute;stos y contribuir al ahorro de energ&iacute;a, colaborando de esta manera al desarrollo del concepto "Aluminio para las Generaciones Futuras".</p>
        </div>
		
<?php include("0piecera.php");?>
