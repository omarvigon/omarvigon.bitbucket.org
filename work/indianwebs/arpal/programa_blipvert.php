<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Blipvert, campa&ntilde;a de recogida de latas de bebidas en las playas</h2>
			<p><img title="Blipvert" src="img/programa_blipvert.jpg" alt="Blipvert" align="left" style="margin:0 10px 20px 0;"/>Blipvert lleva a cabo, desde hace quince a&ntilde;os la campa&ntilde;a &ldquo;Catalunya  Platja Neta&rdquo;, que recupera latas de bebidas en las playas de esta  Comunidad Aut&oacute;noma durante los meses estivales. Ellos son los  organizadores de este evento y cuentan con la colaboraci&oacute;n de m&aacute;s de 30  ayuntamientos, adem&aacute;s del patrocinio de la empresa envasadora Cervezas  Damm. </p>
			<p>La recogida de los envases met&aacute;licos se realiza en las propias  playas mediante contenedores compactadores que prensan los botes y que  tienen capacidad para 2000 unidades.</p>
			<p>Se trata de una actividad  plenamente consolidada y la media de en envases recuperados en cada  edici&oacute;n es de aproximadamente un mill&oacute;n. Desde 1993, Blipvert ha  recuperado m&aacute;s de 20 millones de latas de bebidas en las playas  catalanas mediante los 300 contenedores que tiene instalados a lo largo  del litoral. </p>
			<p>Los Ayuntamientos participantes son: Barcelona, Banyoles,  Begur, Caldes d&rsquo;Estrac, Calella, Calella de Palafrugell, Calonge,  Cambrils, Canet de Mar, Castelldefels, Cubelles, La Pineda-Salou,  L&rsquo;Escala-Emp&uacute;ries, Llafranc, Matar&oacute;, Palafrugell, Palamos, Pals, Pineda  de Mar, Platja d&rsquo;Aro, Sant Andreu de Llavaneres, Sant Feliu de Gu&iacute;xols,  Sant Pol de Mar, Sant Vicen&ccedil; de Montalt, Santa Susanna, Sitges,  Tamariu, Tarragona, Tossa de Mar, Vilanova i la Geltr&uacute;.</p>
			<p><b>M&aacute;s informaci&oacute;n: Blipvert: 93 846 71 85</b></p>  
      </div>
		
<?php include("0piecera.php");?>
