<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>&iquest;Por qu&eacute; reciclar aluminio?</h2>
			<p>Debido al considerable ahorro producido durante el reciclado y a la sencillez del proceso, el aluminio se empez&oacute; a reciclar muy pronto y hoy en d&iacute;a es una actividad normal, t&eacute;cnicamente resuelta y rentable. El proceso de reciclado del aluminio aporta, adem&aacute;s, importantes beneficios medioambientales, econ&oacute;micos y sociales:</p>
            <ul>
              <li>Al producir aluminio a partir de chatarra<b> existe un ahorro del 95% de la energ&iacute;a </b>si se compara con la producci&oacute;n a partir del mineral.</li>
              <li>En el proceso de reciclado <b>no cambian las caracter&iacute;sticas del material</b> ya que se obtiene un producto con las mismas propiedades. Adem&aacute;s, el aluminio puede reciclarse indefinidamente y sin disminuir la calidad  del mismo.</li>
              <li>El <b>100% del material</b> puede ser reciclado.</li>
              <li>En el proceso de reciclado de latas <b>no hay  que eliminar otro tipo de materiales</b>, ya que tanto la tapa como la lata son de aluminio; en general, un  producto es m&aacute;s f&aacute;cil de reciclar si est&aacute; compuesto por un &uacute;nico material.</li>
              <li>Las latas vac&iacute;as se pueden aplastar f&aacute;cilmente, ocupando muy poco volumen, por lo que son<b> f&aacute;ciles de transportar</b>.</li>
              <li>El  reciclado es un proceso rentable porque <b>el aluminio es un metal valioso</b>: por ejemplo, las latas de bebidas usadas recogidas alcanzan un alto valor en el mercado. </li>
              <li><b>Reutilizaci&oacute;n  indefinida: </b>El aluminio recuperado, una vez seleccionado y prensado, se funde y con &eacute;l  se fabrican nuevos lingotes de aluminio que se utilizan para cualquier aplicaci&oacute;n. </li>
            </ul>
            
        </div>
		
<?php include("0piecera.php");?>
