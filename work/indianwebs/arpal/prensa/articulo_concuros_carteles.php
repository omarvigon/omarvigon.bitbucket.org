<?php include("../0cabecera.php");?>
		
        <div class="cuerpo">
        <h2>Concurso de carteles &ldquo;LAS LATAS TIENEN  MUCHAS VIDAS&rdquo;</h2>
        <p>Tres  j&oacute;venes mujeres estudiantes de Dise&ntilde;o en Algeciras, Oviedo y Tarragona  han resultado las ganadoras del concurso nacional de carteles &ldquo;Las  latas tienen muchas vidas&rdquo;, convocado por el Consorcio para la Gesti&oacute;n  de Residuos S&oacute;lidos de Asturias (COGERSA) en el contexto de las  Jornadas Motiva 08 que organiza la Escuela de Arte de Oviedo.</p>
        <p>Tras revisar las 95 candidaturas recibidas de toda Espa&ntilde;a, el jurado  decidi&oacute; otorgar el primer premio a la propuesta presentada por Cristina  Guti&eacute;rrez Guti&eacute;rrez, alumna de la Escuela de Dise&ntilde;o de Algeciras. El  cartel recoge el dibujo de una lata que encierra en su interior la cara  de un gato, en alusi&oacute;n a las m&uacute;ltiples vidas que se dice que ambos  pueden tener. El jurado destac&oacute; la &ldquo;originalidad, claridad y simpat&iacute;a&rdquo;  de la&nbsp; propuesta. Su creadora ser&aacute; galardonada con un premio de 3.000  euros y un diploma.</p>
        <p>La segunda clasificada es una alumna de la Escuela de Arte de  Oviedo, Andrea Guerra Arjona, y la tercera, Marina Sanz Farr&eacute;s, que  mand&oacute; su cartel desde la Escola d&acute;Art i Disseny&nbsp; de Tarragona. Ambas  recibir&aacute;n un premio en met&aacute;lico de 2.000 y 1.000 euros respectivamente.</p>
        <p>Junto a los tres proyectos ganadores, el jurado decidi&oacute; la selecci&oacute;n  de otras 19 obras que formar&aacute;n parte de una exposici&oacute;n itinerante que  se inaugurar&aacute; el pr&oacute;ximo lunes d&iacute;a 7 de abril en la Escuela de Arte de  Oviedo y coincidiendo con la apertura de la und&eacute;cima edici&oacute;n de las  Jornadas de Dise&ntilde;o Motiva. En esa misma fecha, tambi&eacute;n se har&aacute; entrega  de los galardones.</p>
        <p>Todos los carteles presentados al certamen giran en torno a la  tem&aacute;tica propuesta por COGERSA sobre el reciclaje de los envases de  acero y aluminio. El concurso fue convocado el pasado 31 de enero y el  plazo de presentaci&oacute;n de candidaturas finaliz&oacute; el 31 de marzo.</p>
        <p>El Jurado estuvo presidido por la responsable de Educaci&oacute;n Ambiental  de COGERSA, Elena Fern&aacute;ndez, e integrado tambi&eacute;n por los dise&ntilde;adores  asturianos Antonio Acebal, Marcos Recuero y Carmen Santamarina.</p>
        <p>M&aacute;s  informaci&oacute;n: <a href="http://www.cogersa.es" target="_blank">www.cogersa.es</a></p>
        </div>
		
<?php include("../0piecera.php");?>
