<?php include("../0cabecera.php");?>
		
        <div class="cuerpo">
        <h2>El aeropuerto de M&aacute;laga fomenta el reciclado de envases y la concienciaci&oacute;n ciudadana</h2>
        <p><b>La Fundaci&oacute;n  Trinijove ha desarrollado un proyecto de recogida selectiva y concienciaci&oacute;n  hacia el reciclado de envases </b></p>
        <p>Desde  el d&iacute;a 6 de junio hasta pr&oacute;ximo mes de diciembre puede verse una  exposici&oacute;n informativa en el aeropuerto de M&aacute;laga sobre la importancia  de reciclar envases.</p>
        <p> La obra expone tres igl&uacute;s informativos, que  corresponden a los tres contenedores donde se pueden depositar los  residuos en la v&iacute;a p&uacute;blica, con explicaciones sobre los materiales que  deben ir a cada uno de ellos y muestras de diferentes objetos  reciclados.</p>
        <p>Tambi&eacute;n pueden verse diversos paneles informativos con la cadena de  reciclado y&nbsp; las ventajas de colaborar en la recuperaci&oacute;n de los  distintos materiales y la pol&iacute;tica medioambiental del aeropuerto. </p>
        <p>Y  de la teor&iacute;a a la pr&aacute;ctica: actualmente ya se est&aacute; recogiendo de forma  selectiva los envases de vidrio y las latas de bebidas en todas las  dependencias del aeropuerto, que se inici&oacute; el pasado mes de agosto.</p>
        <p>Arpal  ha colaborado en la organizaci&oacute;n de la recogida selectiva y la  colocaci&oacute;n de los contenedores especiales para latas. Asimismo, el  presidente de la asociaci&oacute;n, Jos&eacute; Miguel Benavente,&nbsp; acudi&oacute; a la  inauguraci&oacute;n de la exposici&oacute;n.</p>
        <p>Otras entidades que est&aacute;n  colaborando en este proyecto son: Ecoembes, Ecovidrio, Fundaci&oacute;n la  Caixa, Fundaci&oacute;n Trinijove y el propio aeropuerto de M&aacute;laga.</p>
        </div>
		
<?php include("../0piecera.php");?>
