<?php include("../0cabecera.php");?>
		
        <div class="cuerpo">
            <h2>Archivo fotogr&aacute;fico</h2>
			<p><br />&raquo; Clemente Gonz&aacute;lez Soler, Presidente de Arpal<br />
            <a href="../img/archivo1.jpg" target="_blank"><img title="Clemente Gonzalez Soler" src="../img/archivo1_mini.jpg" border="0" vspace="6" alt="Presidente de Arpal"/></a>
            <a href="../img/archivo2.jpg" target="_blank"><img title="Clemente Gonzalez Soler" src="../img/archivo2_mini.jpg" border="0" vspace="6" alt="Presidente de Arpal"/></a></p>
            
            <p><br />&raquo; Jose Miguel Benavente, Secretario General de Arpal<br />
            <a href="../img/archivo3.jpg" target="_blank"><img title="Jose Miguel Benavente" src="../img/archivo3_mini.jpg" border="0" vspace="6" alt="Secretario General de Arpal"/></a>
            <a href="../img/archivo4.jpg" target="_blank"><img title="Jose Miguel Benavente" src="../img/archivo4_mini.jpg" border="0" vspace="6" alt="Secretario General de Arpal"/></a></p>
            
            <p><br />&raquo; Envases de aluminio<br />
            <a href="../img/archivo5.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo5_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a>
            <img title="Envase Aluminio" src="../img/archivo6.jpg" vspace="6" alt="Envases de Aluminio"/>
            <a href="../img/archivo8.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo8_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a></p>
            
            <p><br />&raquo; Recuperaci&oacute;n de envases de aluminio<br />
            <a href="../img/archivo9.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo9_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a>
            <a href="../img/archivo10.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo10_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a>
            <a href="../img/archivo11.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo11_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a>
            <a href="../img/archivo12.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo12_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a>
            <a href="../img/archivo13.jpg" target="_blank"><img title="Envase Aluminio" src="../img/archivo13_mini.jpg" border="0" vspace="6" alt="Envases de Aluminio"/></a></p>
            
            <p><br />&raquo; Separador de Foucault<br />
            <a href="../img/archivo14.jpg" target="_blank"><img title="Separador de Foucault" src="../img/archivo14_mini.jpg" border="0" vspace="6" alt="Separador de Foucault"/></a>&nbsp;&nbsp;
            <a href="../img/archivo15.jpg" target="_blank"><img title="Separador de Foucault" src="../img/archivo15_mini.jpg" border="0" vspace="6" alt="Separador de Foucault"/></a></p>
            
            <p><br />&raquo; Lingotes de Aluminio<br />
            <a href="../img/archivo16.jpg" target="_blank"><img title="Lingotes de Aluminio" src="../img/archivo16_mini.jpg" border="0" vspace="6" alt="Lingote Aluminio"/></a></p>
        </div>
		
<?php include("../0piecera.php");?>