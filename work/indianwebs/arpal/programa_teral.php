<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Teral, S.L., reciclado de envases de bebidas en la comunidad de Arag&oacute;n</h2>
            <p>Con este slogan, Teral S.L. inici&oacute; su  andadura en el a&ntilde;o 1996 con el objetivo de reciclar latas de bebidas de  aluminio a la vez que ofrec&iacute;a una salida laboral a mayores de 40 a&ntilde;os  desempleados.</p>
			<p>Hoy, Teral S.L.  es una empresa plenamente consolidada con 13 persona en plantilla que  recupera, adem&aacute;s de envases de latas de bebidas otro tipo de productos  met&aacute;licos para su posterior reciclaje.</p>
			<p>Tienen  instalados contenedores amarillos de dise&ntilde;o propio en miles de puntos  distribuidos por la Comunidad &nbsp;Aut&oacute;noma de Arag&oacute;n y dispone de una  planta perfectamente equipada para la selecci&oacute;n y preparaci&oacute;n de los  envases met&aacute;licos para su reciclado.</p>
			<p>Est&aacute;  incrementando el n&uacute;mero de puntos de recogida y modernizando su  equipamiento poco a poco para adaptarse mejor a las necesidades de esta  industria en expansi&oacute;n y en beneficio del medio ambiente.</p>
			<p><b>M&aacute;s informaci&oacute;n:</b><br>Ctra  Madrid, Km 314,800 Zaragoza.<br>Tel&eacute;fono:  976 33 18 00<br>P&aacute;gina  web: <a href="http://www.teralsl.com/" target="_blank">www.teralsl.com</a></b><br>Email:&nbsp; info@teralsl.com</p>
        </div>
		
<?php include("0piecera.php");?>
