<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
        	<h2>Tecnoresiduos R3, recogida de botes en la comunidad de Madrid</h2>
            <p><img title="Tecnoresiduos" src="img/programa_tecnoresiduos.jpg" alt="Tecnoresiduos" align="left" style="margin:0 10px 20px 0;"/>Tecnoresiduos tiene &nbsp;instalados cerca de 150  contenedores para latas de bebidas usadas tanto en la v&iacute;a p&uacute;blica como  en empresas. La empresa recoge peri&oacute;dicamente los envases depositados y  los transporta a su planta de Rivas Vaciamadrid donde realiza la  separaci&oacute;n por materiales (aluminio y hojalata), el prensado y la  confecci&oacute;n de balas que luego son transportadas a una fundici&oacute;n.</p>
			<p>La empresa, que se distingue por su vocaci&oacute;n ambiental no s&oacute;lo por el  trabajo que realiza sino tambi&eacute;n por su filosof&iacute;a, cuenta con las  certificaciones ambientales ISO 14.001 y EMAS y forma parte de la "Red  Entorno de empresas comprometidas con el medio ambiente".</p>
			<p>Tambi&eacute;n realiza acciones de educaci&oacute;n ambiental recibiendo en su planta  a escolares que aprenden cu&aacute;l es el proceso de reciclado y la  importancia de separar los distintos residuos.</p>
			<p>M&aacute;s  informaci&oacute;n en: <a href="http://www.tecnoresiduos-r3.com" target="_blank">www.tecnoresiduos-r3.com</a></p>
        </div>
		
<?php include("0piecera.php");?>
