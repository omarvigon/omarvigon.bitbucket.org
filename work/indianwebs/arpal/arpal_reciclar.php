<?php include("0cabecera.php");?>
		
        <div class="cuerpo">
			<h2>Ayudando a reciclar</h2>
            <p>La <em>Asociaci&oacute;n para el Reciclado de Productos de Aluminio</em> realiza actividades diversas para promover la recuperaci&oacute;n de envases usados de aluminio por todo el territorio espa&ntilde;ol y para  crear una verdadera cultura sobre el reciclado de este material.</p>
            <p><b>Arpal</b> apoya nuevas iniciativas que tengan por objetivo la recogida y el reciclado de envases usados de aluminio asesorando sobre la puesta en marcha, facilitando el contacto con compradores de los envases recogidos, aportando material promocional, de recogida y educativo y difundiendo estos proyectos en su bolet&iacute;n Aluminews, en la web y con notas de prensa a los medios de comunicaci&oacute;n.</p>
        </div>
		
<?php include("0piecera.php");?>
