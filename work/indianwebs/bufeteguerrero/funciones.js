var altura=15;
function areas()
{
	if(altura<105)
	{
		altura=altura+6;
		document.getElementById('areas').style.height=altura;
		setTimeout("areas()");	
	}
	else
	{
		var txtarea="<a href='javascript:seccion(3)'>AREAS</a>";
		txtarea=txtarea+"<div id='lista2'>";
		txtarea=txtarea+"<a href='javascript:areas2(1)'>Derecho Civil</a>";
		txtarea=txtarea+"<a href='javascript:areas2(2)'>Derecho Mercantil</a>";
		txtarea=txtarea+"<a href='javascript:areas2(3)'>Derecho Laboral</a>";
		txtarea=txtarea+"<a href='javascript:areas2(4)'>Derecho Fiscal</a>";
		txtarea=txtarea+"<a href='javascript:areas2(5)'>Derecho Procesal</a>";
		txtarea=txtarea+"</div>";
		document.getElementById('areas').innerHTML=txtarea;
	}
}
function areas2(num)
{
	var temp;
	switch(num)
	{
		case 1:
		temp="<h1>AREAS : Civil</h1><p>Es el Derecho Civil el que m�s presente est� en las actuaciones cotidianas de nuestras vidas, pues rige las relaciones privadas entre las personas, en muchas ocasiones fuente de conflicto y, en la mayor�a de los casos, s�lo necesitados de una regulaci�n espec�fica, que necesita el concurso de profesionales que le oriente para poder defender mejor nuestros derechos.</p>";
		break;
		
		case 2:
		temp="<h1>AREAS : Mercantil</h1><p>Tanto los empresarios individuales como las sociedades, en el desarrollo de su actividad diaria, necesitan contar con una Asesor�a Jur�dica, para afrontar cuestiones legales que se plantean a diario.</p>";
		break;
		
		case 3:
		temp="<h1>AREAS : Laboral</h1><p>El Derecho del trabajo, es, sin ning�n g�nero de dudas, el que se encuentra m�s presente en nuestra sociedad.</p><p>Tanto trabajadores como empresarios se enfrentan todos los d�as a situaciones que requieren del asesoramiento y actuaci�n de profesionales especializados en Derecho Laboral.</p>";
		break;
		
		case 4:
		temp="<h1>AREAS : Fiscal</h1><p>En estos casos las reclamaciones administrativas tienen como finalidad que un ente administrativo nos otorgue una concesi�n, permiso, subvenci�n o licencia.</p><p>Nuestro equipo puede facilitarle los medios y la informaci�n necesaria para realizar con �xito estas reclamaciones administrativas.</p><p>Asimismo, puede ocurrir que las reclamaciones administrativas, aunque est�n perfectamente justificadas, sean desestimadas por la administraci�n, o que la administraci�n no resuelva nuestras reclamaciones en el tiempo adecuado.</p>";
		break;
		
		case 5:
		temp="<h1>AREAS : Procesal</h1><p>El Derecho Penal es del Derecho P�blico por excelencia, pues en el mismo se considera afectada la Sociedad en sus m�s amplios t�rminos.</p><p>El hecho de encontrarse involucrado en la comisi�n de un delito o falta, ya como v�ctima o ya como acusado, supone siempre una situaci�n interspectiva y desagradable pues afecta a los bienes m�s fundamentales de la persona.</p><p>Es por esa raz�n que estamos ante una de las materias que necesita con mayor raz�n, el asesoramiento de especialistas, que asuman la defensa de tales bienes y derechos y nos representen ante los Tribunales de Justicia.</p>";
		break;
	}
	document.getElementById('capa1').innerHTML=temp;
}
function profesionales()
{
	if(altura<155)
	{
		altura=altura+8;
		document.getElementById('profe').style.height=altura;
		setTimeout("profesionales()");	
	}
	else
	{
		var txtarea="<a href='javascript:seccion(4)'>PROFESIONALES</a>";
		txtarea=txtarea+"<div id='lista1'>";
		txtarea=txtarea+"<a href='javascript:profe2(1)'>Bartolome Delgado</a>";
		txtarea=txtarea+"<a href='javascript:profe2(2)'>Bego&ntilde;a Guerrero</a>";
		txtarea=txtarea+"<a href='javascript:profe2(3)'>Cristina Aguila</a>";
		txtarea=txtarea+"<a href='javascript:profe2(4)'>Jose Antonio Baeza</a>";
		txtarea=txtarea+"<a href='javascript:profe2(5)'>Luis Miguel Guerrero</a>";
		txtarea=txtarea+"<a href='javascript:profe2(6)'>Manuel Guerrero de Castro</a>";
		txtarea=txtarea+"<a href='javascript:profe2(7)'>Patricia Diaz</a>";
		txtarea=txtarea+"<a href='javascript:profe2(8)'>Sandra Camacho</a>";
		txtarea=txtarea+"</div>";
		document.getElementById('profe').innerHTML=txtarea;
	}
}
function profe2(num)
{
	var temp;
	switch(num)
	{
		case 1:
		temp="<h1>PROFESIONALES</h1><h2>Bartolome Delgado</h2><img src='img/personal1.jpg' vspace='8'>";
		temp=temp+"<h3>Curriculum</h3>Seminario de Actualizaci�n en Inform�tica Jur�dica";
		temp=temp+"<h3>Datos</h3>Edad";
		temp=temp+"<h3>Departamento</h3>Fiscal";
		temp=temp+"<h3>Formacion academica</h3>Diplomatura";
		temp=temp+"<h3>Idiomas</h3>Castellano - Catalan - Ingles";
		temp=temp+"<h3>E-mail</h3><a href='mailto:bd@bufeteguerrero.com'>bd@bufeteguerrero.com</a>";
		break;
		
		case 2:
		temp="<h1>PROFESIONALES</h1><h2>Bego&ntilde;a Guerrero</h2><img src='img/personal2.jpg' vspace='8'>";
		temp=temp+"<h3>Curriculum</h3>Seminario de Actualizaci�n en Inform�tica Jur�dica";
		temp=temp+"<h3>Datos</h3>Edad";
		temp=temp+"<h3>Departamento</h3>Mercantil";
		temp=temp+"<h3>Formacion academica</h3>Licenciatura";
		temp=temp+"<h3>Idiomas</h3>Castellano - Catalan - Ingles";
		temp=temp+"<h3>E-mail</h3><a href='mailto:bg@bufeteguerrero.com'>bg@bufeteguerrero.com</a>";
		break;
		
		case 3:
		temp="<h1>PROFESIONALES</h1><h2>Cristina Aguila</h2><img src='img/personal1.jpg' vspace='8'>";
		temp=temp+"<h3>Curriculum</h3>Seminario de Actualizaci�n en Inform�tica Jur�dica";
		temp=temp+"<h3>Datos</h3>Edad";
		temp=temp+"<h3>Departamento</h3>Laboral";
		temp=temp+"<h3>Formacion academica</h3>Diplomatura";
		temp=temp+"<h3>Idiomas</h3>Castellano - Catalan - Ingles";
		temp=temp+"<h3>E-mail</h3><a href='mailto:ca@bufeteguerrero.com'>ca@bufeteguerrero.com</a>";
		break;
		
		default:
		break;
	}
	document.getElementById('capa1').innerHTML=temp;
}
function seccion(num)
{
	document.getElementById('areas').innerHTML="<a href='javascript:seccion(3)'>AREAS</a>";
	document.getElementById('areas').style.height="15px";
	document.getElementById('profe').innerHTML="<a href='javascript:seccion(4)'>PROFESIONALES</a>";
	document.getElementById('profe').style.height="15px";
	posicion=-260;
	altura=15;

	document.all['imagen1'].src="./img/bufete"+num+".jpg";
	mover();
	
	var temp1;
	var temp2;
	switch(num)
	{
		case 1:
		temp1="HISTORIA";
		temp2="<img src='img/linea1.gif' border='0'><p>A trav�s de nuestra p�gina web, pretendemos acercarle a la realidad de nuestro bufete de abogados, con el prop�sito de que pueda conocernos mejor y evaluar la calidad de los servicios jur�dicos que le ofrecemos.</p><p>Sea cual sea su problema, en nuestro despacho encontrar� el apoyo humano y profesional que necesita, a trav�s de un equipo de abogados especializados en las diferentes ramas del derecho, le ofrecer�n el asesoramiento legal y jur�dico m�s adecuado, encontrando la soluci�n m�s eficaz a su problema.";
		break;
		
		case 2:
		temp1="PRESENTACION";
		temp2="<img src='img/linea1.gif' border='0'><p>Tanto los empresarios individuales como las sociedades, en el desarrollo de su actividad diaria, necesitan contar con una Asesoria Juridica, para afrontar cuestiones legales que se plantean a diario.</p><p>Nuestra Asesoria Juridica, formada por un experimentado equipo de profesionales de la abogac�a, le puede ayudar a usted como empresario en tres momentos diferenciados: Adquisici�n de la condici�n de empresario; antes de formar una empresa pueden surgir dudas sobre cual es la forma jur�dica id�nea que debe adoptar la empresa.</p><p>Nuestra Asesoria Juridica le informar� de cual es la m�s ventajosa para sus intereses.</p><p>Tanto si desea constituir una sociedad, como si pretende ejercer como empresario individual, es necesario que observe con una serie de requisitos, que le ayudaremos a cumplimentar.";
		break;
		
		case 3:
		temp1="&nbsp;";
		temp2="&nbsp;";
		areas();
		break;
		
		case 4:
		temp1="&nbsp;";
		temp2="&nbsp;";
		profesionales();
		break;
		
		case 5:
		temp1="CONTACTAR";
		temp2="<img src='img/linea1.gif' border='0'><p>Ind�quenos el contenido de su consulta de la forma m�s precisa posible, y, a la mayor brevedad, le enviaremos la evaluaci�n de la misma para su aprobaci�n.</p><form name='formulario' method='post' action='email.php'>Nombre:<br><input type='text' name='nombre' size='43'><br>Empresa:<br><input type='text' name='empresa' size='43'><br>Email:<br><input type='text' name='email' size='43'><br>Comentario:<br><textarea name='comentario' cols='43' rows='7'></textarea><p align='center'><input type='submit' value='ENVIAR'></p></form>";
		break;
	}
	document.getElementById('capa1').innerHTML="<h1>"+temp1+"</h1>"+temp2;
}

var posicion;
function mover()
{
	if(posicion<0)
	{
		posicion=posicion+8;
		document.getElementById('capa1').style.left=posicion;
		setTimeout("mover()");	
	}
}