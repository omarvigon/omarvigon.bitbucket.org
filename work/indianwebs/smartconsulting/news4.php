<?php
	include("head.php");
?>
<td valign="top" class="centro">
<p class="titolMain"><img src="images/arrowBig.jpg">NEWS</p>
							
<p align="center"><strong>SI  SE PUEDE, PERI&Oacute;DICO OFICIAL DE LA II FERIA  LATINOAMERICANA</strong></p>
<p><img width="228" height="169" src="images/news4_clip_image002.gif" align="right" hspace="16" />Smart Consulting y el Peri&oacute;dico Si se Puede han alcanzado un acuerdo de  colaboraci&oacute;n por el que este &uacute;ltimo ser&aacute; el diario oficial de la II edici&oacute;n de  la feria latinoamericana &ldquo;Vive Latinoam&eacute;rica&rdquo; que se celebrar&aacute; el pr&oacute;ximo d&iacute;a  17 y 18 de Marzo en el recinto ferial de la Farga del Hospitalet.<br />
  &nbsp;<br />
  Si se Puede es el diario m&aacute;s destacado dirigido a  todos los nuevos residentes en Espa&ntilde;a con la m&aacute;s amplia informaci&oacute;n de inter&eacute;s  para la integraci&oacute;n de todos los ciudadanos en la sociedad espa&ntilde;ola. Sus m&aacute;s de  155.000 ejemplares (PGD)&nbsp; se distribuyen  cada s&aacute;bado en m&aacute;s de 2.000 puntos en Catalu&ntilde;a, la Comunidad de Madrid y  Levante (Comunidad Valenciana y Murcia), con una red de entrega en mano y otra  en locales afines, como  locutorios y comercios frecuentados por residentes extranjeros.</p>
<p><img width="77" height="77" src="images/news4_clip_image004.jpg" align="left" hspace="16" />En virtud del  acuerdo suscrito, Si se Puede ofrece a todos los expositores una oferta  especial publicitaria en su edici&oacute;n para Catalu&ntilde;a, que servir&aacute;&nbsp; para potenciar la imagen de las empresas que  exponen en la feria, destacando sus productos y/o servicios. Esta oferta es un  anuncio formato 101 mm. x 87,5 mm.(ancho y alto) que ser&aacute; publicado la semana  previa, la semana del evento y que tambi&eacute;n se incluir&aacute; en el reportaje final  sobre los resultados alcanzados en la feria.</p>
<p>Para m&aacute;s informaci&oacute;n contactar con el Sr. Jordi Sabat, al n&ordm; 91.517.99.15 <b>(<a href="mailto:jsabat@sisepuede.es">jsabat@sisepuede.es</a>)</b> o con el Sr. Carlos Ruiz al n&ordm; 93.481.51.64 <b>(<a href="mailto:cruiz@sisepuede.es">cruiz@sisepuede.es</a>)</b>.</p>
<p>Cordialmente,</p>
<p>Enric Garc&iacute;a <br />
  Director General&nbsp;&nbsp;&nbsp;&nbsp; </p>
<p align="justify"><br />
<br />
</p>
						
<p>&nbsp;</p>

</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>

</body>
</html>