<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>SMART CONSULTING</title>
    <style type="text/css" media="all">
        @import "css/template2.css";</style>
    <script language="javascript">
		var abierto=false;
		
		function AbrirVentana(url,tamx,tamy)
		{
			width = window.screen.width;
			height = window.screen.height;
			posx = width / 2 - tamx / 2;
			posy = height / 2 - tamy / 2;
			VentanaNueva = open(url,"Informaci�","toolbar=no,directories=no,scrollbars=0,menubar=no,status=no,resizable=no,width=" + tamx + "px,height=" + tamy + "px,left=" + posx + "px,top=" + posy + "px");
			abierto=true;//Esta variable se pone a true cuando se abre la ventana.	
		}
	</script>
	
	<meta name="description" content="Smart Consulting" />
    <meta name="keywords" content="Smart Consulting, comunicaci�,producci�,sponsorship" />
</head>

<body>
<?php
	include("intranet/class/mysql.class.php");
	include("intranet/class/date.class.php");
	$my = new SQL();

	$numImg = 6;
	$a = (rand() % $numImg) + 1;
?>

<table border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
	<td colspan="2"><span><img src="images/top<?php print $a; ?>.png" border="0"></span></td>
</tr>
<tr><td colspan="2" class="btarriba">
	<br>
			<a href="index.php">Inicio&nbsp;&nbsp;&nbsp;|</a>&nbsp;&nbsp;&nbsp;
			<a href="eventos.php">Eventos y Comunicaci�n&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a>
			<a href="prensa.php">Prensa&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a>
			<a href="clientes.php">Clientes&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a>
			<a href="patrocinio.php">Patrocinio&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a>
			<a href="suscripcion.php">Suscripci�n&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a>
			<a href="contacto.php">Contacto&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</a><br><br>
	 </td>
</tr>
<td valign="top" class="btizquierda"><br>
	
	<table cellspacing="0" cellpadding="8">
	<tr>
		<td colspan="2"><p class="titulonoticias"><br><br>Proyectos en curso</p></td></tr>
	<tr>
		<td><img src="images/mini_3.gif"></td>
		<td><a href="proyectos3.php">III Feria Latinoamericana</a></td></tr>
	<tr>
		<td colspan="2"><p class="titulonoticias"><br><br>Noticias de Actualidad</p></td></tr>
		<tr>
		<td><img src="images/mini7.gif"></td>
		<td><a href="news6.php">Vive! Latinoam�rica fue un �xito de p�blico en su segunda edici�n.</a></td></tr>
	<tr>
		<td><img src="images/mini5.gif"></td>
		<td><a href="news4.php">'SI SE PUEDE', periodico oficial de la II Feria Latinoamericana</a></td></tr>
	<tr>
		<td><img src="images/minindian.gif"></td>
		<td><a href="news1.php">IndianWebs es la empresa seleccionada para el nuevo portal de smartconsulting.es</a></td></tr>
	<tr>
		<td><img src="images/mini13.gif"></td>
		<td><a href="news3.php">La I Feria Latinoamericana, &eacute;xito de convivencia e integraci&oacute;n</a></td></tr>
		
	<tr>
		<td colspan="2"><p class="titulonoticias"><br><br>Proyectos Realizados </p></td></tr>
	<tr>
		<td><img src="images/mini2.gif"></td>
		<td><a href="proyectos2.php">II Feria Latinoamericana</a></td></tr>
	<tr>
		<td><img src="images/mini1.gif"></td>
		<td><a href="proyectos1.php">I Feria Latinoamericana</a></td></tr>
	</table>
</td>