<?php
	include("head.php");
?>
<td class="centro">

<p class="titolMain"><img src="images/arrowBig.jpg">III FERIA LATINOAMERICANA</p>
<p class="titolSub"><img src="images/arrowSub.jpg">Descripción</p>

	<p>Vive!  Latinoam&eacute;rica se ha consolidado como el evento ferial mas grande de Europa  dedicado al colectivo latino, por lo que se ha convertido en un referente a  tener en cuenta por todas aquellas empresas que quieran captar y fidelizar a  este colectivo.&nbsp; Es una gran oportunidad  para darse a conocer y satisfacer la demanda creciente de estos ciudadanos.</p>

	<p>La  segunda edici&oacute;n fue una autentica fiesta de diversidad, multiculturalidad y  convivencia en la que expositores alcanzaron sus objetivos y el p&uacute;blico que  asisti&oacute; disfrut&oacute; del ambiente cordial, divertido y familiar.</p>
	<p>En  esta nueva edici&oacute;n queremos potenciar la participaci&oacute;n de empresas  inmobiliarias y de moda de Latinoam&eacute;rica que a trav&eacute;s de Vive! Latinoam&eacute;rica  est&eacute;n interesados en exponer sus productos y entrar en el mercado europeo.</p>
	<p>Ficha  T&eacute;cnica:</p>
	<p>Fecha: 15 y  16 de Marzo 2008<br />
	  Lugar: Recinto ferial La Farga  de L'Hospitalet<br />
	  Zona Exposici&oacute;n: 11 a  21 horas<br />
	  Expositores: 10 a  22 horas</p>
	<p>Para  contrataci&oacute;n:<br />
	  Departamento Comercial<br />
	  Tel&eacute;fono: 93.203.01.08</p>
	<p><br />
    </p>
	<div class="titolSub"><img src="images/arrowSub.jpg">Imágenes</div>
		<p align="center"><img src="images/feria3latina.jpg" border="0"></p>

	<br/>
		<div class="titolSub"><img src="images/arrowSub.jpg">Ficheros</div>
	<div>
		<br />
		<ul>
		<li><a href="files/3dossier_latinoamerica2008.pdf" target="_blank">Dossier Vive! Latinoamerica 2008</a></li>
		<li><a href="files/3plano_recinto.pdf" target="_blank">Plano del recinto</a></li>		
		<li><a href="files/3normas_participacion.pdf" target="_blank">Normas de participación</a></li>
		<li><a href="files/3formulario_inscripcion.pdf" target="_blank">Formulario de inscripción</a></li>
		</ul>	
	</div>
	<br>
	<div class="titolSub">
	  <p><img src="images/arrowSub.jpg">Sponsors</p>
	</div>
	<div><img src="images/logos/ford.gif" hspace="24"><br>
	  <br></div>
	<div class="titolSub">
	  <p><img src="images/arrowSub.jpg">Colaboran</p>
	</div>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/logos/banesto.gif" hspace="5">
		<img src="images/logos/heineken.gif">
		<img src="images/logos/etnika.gif" hspace="5">
		<img src="images/logos/telegiros.jpg">
</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>

</body>
</html>