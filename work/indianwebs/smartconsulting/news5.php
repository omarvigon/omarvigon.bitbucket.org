<?php
	include("head.php");
?>
<td valign="top" class="centro">
<p class="titolMain"><img src="images/arrowBig.jpg">NEWS</p>
							
<p align="center" style="font-weight: bold">CUENTA ATR&Aacute;S PARA LA II  FERIA LATINOAMERICANA</p>
<p><strong>La II Feria Latinoamericana &ldquo;Vive! Latinoam&eacute;rica&rdquo;,  que se celebra el 17 y 18 de marzo en el recinto La Farga de L&rsquo;Hospitalet de  Llobregat (Barcelona), es el evento ideal para disfrutar en familia de la  cultura y tradiciones latinoamericanas y conocer la oferta tur&iacute;stica de este  continente.</strong></p>
<img src="images/news5_clip_image002_0000.jpg" align="right" hspace="12" />
<p>&ldquo;Vive! Latinoam&eacute;rica&rdquo; es la  mayor feria del mundo latino en Europa. En 8.500 m2 de exposici&oacute;n re&uacute;ne los  pa&iacute;ses latinoamericanos con mayor presencia en Espa&ntilde;a. Esto es, Ecuador,  Colombia, Per&uacute;, Bolivia, M&eacute;xico, Brasil, Argentina, Chile, Rep&uacute;blica Dominicana  y Venezuela.</p>
<p>En &ldquo;Vive! Latinoam&eacute;rica&rdquo;  alrededor de 80 empresas y asociaciones ofrecen sus productos y servicios al  colectivo latinoamericano residente en Espa&ntilde;a y al p&uacute;blico local interesado en  su cultura.</p>
<p><img src="images/news5_clip_image002.jpg" /></p>
<p>Este  a&ntilde;o &ldquo;Vive! Latinoam&eacute;rica&rdquo; cuenta con dos nuevas &aacute;reas: en primer lugar, <strong>selecci&oacute;n  de personal</strong>, con la participaci&oacute;n del Ministerio de Defensa y otras grandes  empresas. En la primera edici&oacute;n de &ldquo;Vive! Latinoam&eacute;rica&rdquo; se entregaron m&aacute;s de  cuatro mil curr&iacute;culums. En segundo lugar, <strong>emprendedores</strong>, dedicada a la  promoci&oacute;n de nuevas iniciativas empresariales por parte de latinoamericanos.  Este sector est&aacute; encabezado por la Fundaci&oacute;n Un Sol M&oacute;n, obra social de la  Caixa de Catalunya. </p>
<p>Entre  los expositores, destaca Ford Espa&ntilde;a, patrocinador de la feria por segundo a&ntilde;o,  que expone la amplia gama de veh&iacute;culos que tiene en el mercado espa&ntilde;ol. Tambi&eacute;n  participar&aacute;n empresas de env&iacute;o de dinero, agencias de viajes, entidades  financieras, inmobiliarias,&nbsp; tiendas de moda  y asesor&iacute;as legales, entre otras.</p>
<p>&ldquo;Vive! Latinoam&eacute;rica&rdquo; tambi&eacute;n es una gran  fiesta de la diversidad e integraci&oacute;n de las culturas y pueblos que forman este  continente y un punto de encuentro para las asociaciones e instituciones  latinoamericanas de todo el pa&iacute;s. El evento incluye espect&aacute;culos folkl&oacute;ricos  con m&uacute;sica y bailes t&iacute;picos, desfiles de moda latina, adem&aacute;s sorteos de  productos, viajes y un Ford Fiesta, entre otras sorpresas.</p>
<p>Cabe resaltar la oferta gastron&oacute;mica, con platos  tan t&iacute;picos como los tacos mexicanos o las arepas colombianas, adem&aacute;s de zumos  de frutas tropicales, como la guayaba o el mango, y la popular Inca Kola  peruana.</p>
<p>Para los ni&ntilde;os, la diversi&oacute;n est&aacute; asegurada en &ldquo;Vive! Latinoam&eacute;rica&rdquo; con  una zona de 300 m2 de juegos y actividades.</p>
<p>Barcelona, 16 de febrero de 2007<br />
    <strong>Dto. Comunicaci&oacute;n Vive!  Latinoam&eacute;rica</strong></p>
<p><strong>Para m&aacute;s informaci&oacute;n:</strong><br />
  On Comunicaci&oacute;n<br />
  Nina Garc&iacute;a<br />
  Tel.: 93 238 68 20<br />
  prensa1@oncomunicacion.es</p>
<p align="justify"><br />
<br />
</p>
						
<p>&nbsp;</p>

</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>

</body>
</html>