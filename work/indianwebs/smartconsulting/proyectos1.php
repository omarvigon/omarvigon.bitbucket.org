<?php
	include("head.php");
?>
<td class="centro">

<p class="titolMain"><img src="images/arrowBig.jpg">FERIA LATINOAMERICANA</p>
<p class="titolSub"><img src="images/arrowSub.jpg">Descripci�n</p>

	<p>Por primera vez una feria dedicada a los ciudadanos de origen latinoamericano (ecuatorianos, colombianos, peruanos, argentinos, bolivianos, chilenos, venezolanos, brasile�os, etc.), cuyo objetivo es reunir de forma masiva a instituciones, asociaciones, consulados, medios de comunicaci�n y empresarios que a trav�s de la gastronomia, el turismo, la cultura, los productos y los servicios, dar�n a conocer la amplia oferta que ponen a disposici�n de los latinoamericanos.<br><br>
	
Nuestro deseo es que sea el gran evento anual que consiga ser el referente y via para la integraci�n social de sus gentes y que en el se expongan los servicios y negocios actuales y los que constantemente son creados para satisfacer la demanda creciente del colectivo latinoamericano.<br>
<br>
Nuestro objetivo es que adem�s sea una fiesta de la diversidad cultural, con conferencias, actividades folkloricas y culturales; que se hable de personas de sus valores, intereses e ilusiones. Que el p�blico que asista a la Feria sean familias latinas, empresarios y familias espa�olas que quieran conocer m�s sobre las costumbres, el turismo, la historia,la gastronomia de los paises latinos, en definitiva que logremos integrar a todos como ciudadanos que convivimos en Catalu�a.<br />
<br />
  </p>

	<p class="titolSub"><img src="images/arrowSub.jpg">Im�genes</p>
		<table width=100% cellspacing=5	cellpadding=5>
		<tr><td align="center"><a href="javascript:AbrirVentana('fotos.html?foto=proyectos/1/1.jpg',425 + 15,283 + 10);"><img src="images/1.gif" border="0"></a>
		</td><td align="center"><a href="javascript:AbrirVentana('fotos.html?foto=proyectos/1/3.jpg',425 + 15,283 + 10);"><img src="images/3.gif" border="0"></a></td></tr>
		<td align="center"><a href="javascript:AbrirVentana('fotos.html?foto=proyectos/1/ejercito.jpg',443 + 15,295 + 10);"><img src="images/ejercito.gif" border="0"></a></td>
		<td align="center"><a href="javascript:AbrirVentana('fotos.html?foto=proyectos/1/penedes.jpg',443 + 15,295 + 10);"><img src="images/penedes.gif" border="0"></a></td></tr></table>
	<br/>
		<p class="titolSub"><img src="images/arrowSub.jpg">Ficheros</p>
		<ul><li><a href="proyectos/1/ficheros/Dossier Prensa.pdf" target="_blank">Dossier Prensa</a></</li>
		<li><a href="files/video_feria.wmv"><img src="images/movieicon.png" border="0"> Video del programa emitido en TVE</a></li></ul>
		
		<div class="titolSub">
		  <p><img src="images/arrowSub.jpg">Sponsors</p>
		</div>
		<div><img src="images/logos/ford.gif" hspace="16"><br/>
	  <br/>	</div>
		
		<div class="titolSub">
		  <p><img src="images/arrowSub.jpg"> Colaboran</p>
		</div>
		<div><img src="images/logos/banesto.gif" hspace="16"><img src="images/logos/heineken.gif"/><br>
	  <br></div>
		
</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>

</body>
</html>