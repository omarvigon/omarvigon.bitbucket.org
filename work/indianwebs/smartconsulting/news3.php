<?php
	include("head.php");
?>
<td valign="top" class="centro">
<p class="titolMain"><img src="images/arrowBig.jpg">NEWS</p>
							
LA I FERIA LATINOAMERICANA, �XITO DE CONVIVENCIA E INTEGRACI�N
<p align="justify">Dalgia, 8 junio 2006: La I Feria Latinoamericana, cerraba sus puertas el pasado 28 de mayo en la Farga de L&#8217;Hospitalet cumpliendo sus previsiones m�s optimistas.<br />
<br />
15.214 personas visitaron entre el s�bado y el domingo, d�a de mayor afluencia, la primera edici�n de este certamen que por primera vez hermanaba folklore, cultura y gastronom�a con una amplia oferta de productos y servicios para el latinoamericano. <br />
<br />
El s�bado a las 13:00 horas se proced�a a la inauguraci�n oficial con la presencia de la Concejal de Asuntos Sociales del Ayuntamiento de L&#8217;Hospitalet, Dolors Fern�ndez, la c�nsul de Republica Dominicana,  Nelly Perez, el c�nsul de Uruguay, Joaqu�n Piriz, y el director de la feria, Enric Garc�a.<br />
<br />
Entre los 130 expositores participantes uno de los sectores con mayor n�mero de peticiones de informaci�n fue el de los seguros, reflejo de la gran demanda por adquirir  un seguro que cubra la repatriaci�n a su pa�s en caso de muerte. Mapfre, Seguros Vitalicio, Agrupaci�n Mutua, La Alian�a y DKV fueron algunas de las empresas con mayor demanda.<br />
<br />
Tambi�n las compa��as dedicadas al sector inmobiliario recibieron la visita de numerosos latinos interesados en adquirir una casa en pa�ses como Ecuador (www.portalinmobiliariodeecuador.com) y Rep�blica Dominicana. <br />
<br />
Algunas empresas como Sagarra Abogados ofrecen al inmigrante una completa oferta de pisos en Barcelona con precios que oscilan entre los 180.000 y  los 240.000 euros: &#8220;nuestras viviendas les permiten integrarse desde el principio en la sociedad de forma digna y sin tener que pasar por situaciones comprometidas&#8221;, explicaba Fredi Debois, coordinador del departamento inmobiliario. La misma compa��a se encarga de realizar el estudio de viabilidad de la hipoteca, lo que tambi�n ayuda a la hora de acceder a esta primera vivienda.<br />
<br />
El caso de la inmobiliaria Dani&Gabi es diferente ya que su oferta se dirige principalmente al catal�n o espa�ol que desea adquirir su segunda vivienda en una zona tur�stica de alto nivel de la Rep�blica Dominicana. Cada tres meses organizan un viaje a la zona de destino y ense�an la vivienda &#8220;in situ&#8221; al futuro comprador, cuyo contrato se formaliza en Espa�a. <br />
<br />
Tambi�n numerosas fueron las empresas de servicios financieros y legales: desde Servicenter, que informaba al visitante latino sobre la posibilidad de montar un negocio (el 15% de sus clientes son latinoamericanos) hasta Conexi�n Latina, que ofrec�a servicios jur�dicos integrales y asesoramiento sobre inversiones en sus pa�s de origen. <br />
<br />
La oferta de empleo fue otro de los grandes protagonistas de la I Feria Latinoamericana. &#8220;Con solo el permiso de residencia y  una edad comprendida entre los 18 y los 27 a�os un inmigrante latinoamericano puede entrar a formar parte de las fuerzas armadas&#8221;, explicaba el teniente coronel Bruno Alonso. De hecho el 7% de los profesionales del ej�rcito espa�ol son procedentes de Am�rica Latina y representar�n en pocos a�os a unos 5.000 efectivos. En el stand de las Fuerzas Armadas Espa�olas los responsables ofrec�an informaci�n sobre los cursos de selecci�n y el modo de acceder a las m�s de 3.700 plazas disponibles para el 2006. <br />
<br />
Los Mossos d&#8217;Escuadra tambi�n se dieron cita en la feria con el objetivo de acercarse m�s a este colectivo social y generar imagen de proximidad y confianza. De hecho la feria se celebr� sin ning�n tipo de contratiempo en un evento familiar y cordial de integraci�n y convivencia. <br />
<br />
El papel de las asociaciones de ayuda a la integraci�n fue el que m�s llam� la atenci�n de los medios de comunicaci�n. La asociaci�n colombiana Mira explicaba con detalle los cursos de catal�n e ingl�s que ofrecen a los compatriotas que llegan a Catalunya, y c�mo consegu�an sacar adelante un peri�dico mensual del mismo nombre con informaci�n de inter�s para el inmigrante. <br />
<br />
En otro �rea diferente pero tambi�n de apoyo a proyectos de desarrollo, participaron los Capuchinos de Catalunya y Baleares (SSIM). Misioneros solidarios que no dudaron en dejar una hucha en cada stand para recaudar dinero y destinarlo a ayudas de emergencia, alimentaci�n, vestimenta, a ni�os de barrios marginales en Colombia.<br />
<br />
Importante tambi�n la presencia de los gobiernos de Pichincha en Ecuador promocionando su oferta tur�stica y el gobierno de Montecristi ( Republica Dominicana) dando a conocer su proyecto urban�stico Crist�bal Colon, en el que inversores catalanes est�n muy interesados a desarrollar.<br />
<br />
Pero si algo acapar� las miradas de 15.214 visitantes fue la amplia oferta gastron�mica, cultural, de moda y complementos. Los restaurantes, los colmados, y las tiendas de ropa y artesan�a superaron con creces sus expectativas ya que incluso el domingo a �ltima hora casi algunas ya casi no dispon�an de producto: empanadas colombianas y peruanas, cervezas �tnicas, frutas ex�ticas, burritos y enchiladas, rones y aguardientes, caipiri�as y mojitos, se mezclaban con bikinis brasile�os, vaqueros ajustados, peinados imposibles, y una colorista bisuter�a. <br />
<br />
Mientras, al otro lado del pabell�n, el stand del parque tem�tico Isla Fantas�a y del Aquarium de Barcelona se llenaba ni�os entre los 3 y 8 a�os deseosos de llevarse todos los globos y regalos disponibles.<br />
<br />
De fondo y durante dos d�as de manera ininterrumpida, la m�sica aut�ctona hacia del sal�n una fiesta alegre y animada: la batucada brasile�a se convert�a en la estrella del s�bado despu�s de circular entre los stands y la multitud de visitantes que prolongaron el encuentro hasta las  nueve y media de la noche. En el escenario de 100m2 con 8000 watios de potencia, durante el d�a, se alternaban las danzas ind�genas de la asociaci�n Runa Pacha con los bailes bolivianos de Sayari Urus. <br />
<br />
El domingo el broche de oro musical lo pon�an los mariachis a ritmo de rancheras despu�s de dos d�as de convivencia y multiculturalidad entre un publico familiar.<br />
<br />
</p>
						
<p><img border=0 src="images/13.gif"></p>

</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>

</body>
</html>