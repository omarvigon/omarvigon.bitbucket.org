<?php
	include("head.php");
	include("class/news.class.php");
	include("class/mysql.class.php");
	include("class/date.class.php");
?>
	<div id="content">
			<?php
				include("top.php");
			?>
	     	<div id="contingut">
	     		<div id="contingut_main">
					<?php
						include("menu_contingut.php");
					?>
					<div id="contingut_dins">
						<div id="titolMain">
							<img src="images/arrowBig.jpg" align="absmiddle" />INSERTAR NOTICIA
						</div>
						<div align="center">
							<?php
								$val = "A�adir noticia";
								$visible = " checked";	
								
								if(isset($_POST["submitted"]))
								{
									$data = $_POST["data"];
									$title = $_POST["title"];
									$desc = $_POST["desc"];
									$visible = $_POST["visible"];
									$link = $_POST["webLink"];
									
									/* Check if data is null, if it is null, we give the current date value,
									if not null, translate it to the English format which is used on the db */
									
									if($data == "")
									{
										$data = date('Y-m-d');
									}
									else
									{
										$date = new dDate($data);
										$data = $date->SpanishToEng();	
									}
									
									/* Check the rest of values, if not correct, add the error message in the error Array */
									
									$error = Array();
									
									if($title == "")
									{
										$error[] = "El t�tulo es nulo.";	
									}
									
									if($desc == "")
									{
										$error[] = "La descripci�n es nula.";	
									}
									
									$numImages = 1;
									
									if(count($error) == 0)
									{
										
										$my = new SQL();
										$images = Array();
										$new = new News($my,"news");
										
										for($i = 0; $i <= $numImages; $i++)
										{
											if($_FILES["image" . $i]["error"] != 0 && $_FILES["image" . $i]["error"] != 4)
											{
												$error[] = "Hay un error en la imagen " . $i;
											}
											else
											{
												if($_FILES["image" . $i]["error"] == 0)
												{
													$images[] = $_FILES["image" . $i];	
												}
											}
										}
										
										if(!isset($_POST["id"]))
										{
											$new->addNews($title,$data,$desc,$visible,$link,$images);
											$msg = "Noticia a�adida correctamente";
										}
										else
										{
											$new->updateNews($title,$data,$desc,$visible,$link,$_POST["id"],$images);	
											$msg = "Noticia modificada correctamente";
										}
										
										print "<div align=\"center\" class=\"success\">";
										print $msg;
										print "</div>";
									}
									else
									{
										print "<div align=\"left\"><br/>Hay errores en el formulario:";
										print "<ul>";
										foreach($error as $err)
										{
											print "<li>" . $err . "</li>";	
										}
										print "</ul>";
										print "<input type=\"button\" onClick=\"javascript:history.back();\" value=\"Solucionar problemas...\">";
										print "</div>";
									}
								}
								else
								{
								if(isset($_GET["id"]))
								{
									$my = new SQL();
									
									$new = $my->do_selec("SELECT * FROM news WHERE id = '" . $_GET["id"] . "'");	
									$title = $new[1]["title"];
									$desc = $new[1]["description"];
									$visible = $new[1]["publics"];
									$fecha = new dDate($new[1]["data"]);
									$data = $fecha->EngToSpanish();
									$img = $new[1]["img"];
									$link = $new[1]["link"];
									$id = $_GET["id"];
									
									$val = "Modificar noticia";
									
									if($visible == 'y')
									{
										$visible = " checked";	
									}
									else
									{
										$visible = "";	
									}
									
								}	
							?>
							
							<form action="addNews.php" method="POST" enctype="multipart/form-data">
							<table class="formulari">
								<?php
									if(isset($_GET["id"]))
									{
										print "<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">";
									}
								?>
								<tr><td class="left">T�tulo:</td><td><input type="text" size="60" maxlength="255" name="title" value="<? print $title; ?>"></td></tr>
								<tr><td class="left">Fecha:</td><td><input type="text" size="14" maxlength="10" name="data" value="<? print $data; ?>"> (dd/mm/aaaa) (En blanco para fecha actual)</td></tr>
								<tr><td class="left">Descripci�n:</td><td><textarea name="desc" cols=45 rows=5><? print $desc; ?></textarea></td></tr>
								<tr><td class="left">Visible:</td><td><input type="checkbox" name="visible" value="1" <? print $visible; ?>> (Indica si la noticia sale en la p�gina principal)</td></tr>
								<tr><td class="left">Imatge associada:</td><td valign="top"><input type="file" name="image1">
								<?php
									if(isset($_GET["id"]) && $img != "")
									{
										print "<img align=\"top\" src=\"news/" . $id . "/thumbs/" . $img . "\">";
									}									
								?></td></tr>
								<tr><td class="left">Link de la imatge:</td><td valign="top"><input type="text" size="30" name="webLink" value="<? print $link; ?>"></td>
								<tr><td class="left">&nbsp;</td><td><input type="submit" name="submitted" value="<? print $val; ?>"></td></tr>
							</table>
							</form>
							
							<?php
								}
							?>
							
						</div>
					</div>
					<br style="clear:both" />
				</div>
	    	</div>
	    	<br style="clear:both" />
	    </div>
	    <?php
	    	include("bottom.php");
	    ?>
	</div>
</body>
</html>