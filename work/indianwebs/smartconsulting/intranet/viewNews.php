<?php
	include("head.php");
	include("class/news.class.php");
	include("class/mysql.class.php");
	include("class/date.class.php");
?>
	<div id="content">
			<?php
				include("top.php");
			?>
	     	<div id="contingut">
	     		<div id="contingut_main">
					<?php
						include("menu_contingut.php");
					?>
					<div id="contingut_dins">
						<div id="titolMain">
							<img src="images/arrowBig.jpg" align="absmiddle" />MODIFICAR / BORRAR NOT�CIAS
						</div>
						<br/>
						<div align="left">
						<?php
						
							$my = new SQL();
							
							if(isset($_GET["del"]) && $_GET["del"] == 1)
							{
								$noti = new News($my,"news");
								$noti->delete($_GET["id"]);	
							}
							
							$query = "SELECT * FROM news ORDER BY data DESC";
							$news = $my->do_selec($query);
							$num = array_shift($news);
							print "Actualmente hay <b>" . $num . "</b> noticias en la base de datos <br/><br/> Para consultar la descripci�n de una noticia pulse en su t�tulo y aparecer� el contenido completo.<br/><br/>";
							print "<table width=100% border=0 cellspacing=0 cellpadding=4>";
							print "<tr><td width=70 class=\"cap\">Fecha</td><td width=320 class=\"cap\">T�tulo</td><td class=\"cap\">Visible</td><td class=\"cap\">Modif.</td><td class=\"cap\">Borrar</td></tr>";
							foreach($news as $new)
							{
								$date = new dDate($new["data"]);
								print "<tr><td>" . $date->EngToSpanish() . "</td><td><a href=\"javascript:showDesc(" . $new["id"] . ");\">" . $new["title"] . "</a></td><td align=\"center\">" . str_replace('y','S�',$new["publics"]) . "</td><td align=\"center\">[ <a href=\"addNews.php?id=" . $new["id"] . "\">X</a> ]</td><td align=\"center\">[ <a href=\"javascript:ConfirmDel(" . $new["id"] . ",'news')\">X</a> ]</td></tr>";
								print "<tr><td height=10>&nbsp;</td><td colspan=4><div id=\"desc" . $new["id"] . "\" class=\"descEscond\">" . nl2br($new["description"]) . "</div></td></tr>";
							}
							print "</table>";
						?>
						</div>
					</div>
					<br style="clear:both" />
				</div>
	    	</div>
	    	<br style="clear:both" />
	    </div>
	    <?php
	    	include("bottom.php");
	    ?>
	</div>
</body>
</html>