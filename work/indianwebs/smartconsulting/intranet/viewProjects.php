<?php
	include("head.php");
	include("class/project.class.php");
	include("class/mysql.class.php");
	include("class/date.class.php");
?>
	<div id="content">
			<?php
				include("top.php");
			?>
	     	<div id="contingut">
	     		<div id="contingut_main">
					<?php
						include("menu_contingut.php");
					?>
					<div id="contingut_dins">
						<div id="titolMain">
							<img src="images/arrowBig.jpg" align="absmiddle" />MODIFICAR / BORRAR PROYECTOS
						</div>
						<br/>
						<div align="left">
						<?php
						
							$my = new SQL();
							
							if(isset($_GET["del"]) && $_GET["del"] == 1)
							{
								$pro = new Project($my,"projects","imagesprojects");
								$pro->delete($_GET["id"]);	
							}
							
							$query = "SELECT * FROM projects ORDER BY data DESC";
							$news = $my->do_selec($query);
							$num = array_shift($news);
							print "Actualmente hay <b>" . $num . "</b> proyectos en la base de datos <br/><br/> Para consultar la descripci�n y las im�genes de un proyecto pulse en su t�tulo y aparecer� el contenido completo.<br/><br/>";
							print "<table width=100% border=0 cellspacing=0 cellpadding=4>";
							print "<tr><td width=320 class=\"cap\">T�tulo proyecto</td><td class=\"cap\" align=\"center\" width=180>Modificar</td><td class=\"cap\" align=\"center\">Borrar</td></tr>";
							foreach($news as $new)
							{
								$date = new dDate($new["data"]);
								print "<tr><td><a href=\"javascript:showDesc(" . $new["id"] . ");\">" . $new["title"] . "</a></td><td align=\"center\">[ <a href=\"addProjects.php?id=" . $new["id"] . "\">X</a> ]</td><td align=\"center\">[ <a href=\"javascript:ConfirmDel(" . $new["id"] . ",'proj')\">X</a> ]</td></tr>";
								print "<tr><td colspan=3><div id=\"desc" . $new["id"] . "\" class=\"descEscond\"><b>Descripci�n</b><br/><br/>" . nl2br($new["description"]) . "";
								 
								$q = "SELECT * FROM imagesprojects WHERE proj = '" . $new["id"] . "' ORDER BY ref";
								$images = $my->do_selec($q);
								
								print "<br/><br/><b>Im�genes</b><br/><table width=100% cellspacing=5	cellpadding=5><tr>";
								$i = 0;
								$num = array_shift($images);
								
								$numImages = 2;
								$colspan = false;
								
								if($num % $numImages != 0)
								{
									$colspan = true;	
								}
								
								foreach($images as $img)
								{
									if($i % $numImages == 0)
									{
										if($i > 0)
										{
											print "</tr>";	
										}
									}
									
									$col = "";
									if($colspan && $i == $num - 1)
									{
										$col = " colspan=\"" . $numImages . "\"";
									}
									
									$size = getImageSize("../proyectos/" . $img["proj"] . "/" . $img["rutaImagen"]);
									print "<td" . $col . " align=\"center\"><a href=\"javascript:AbrirVentana('fotos.html?foto=../proyectos/" . $img["proj"] . "/" . $img["rutaImagen"] . "'," . $size[0] . " + 15," . $size[1] . " + 10);\"><img src=\"../proyectos/" . $new["id"] . "/thumbs/" . $img["rutaImagen"] . "\" align=\"absmiddle\"/></a></td>";	
									$i++;
								}
								print "</tr></table></div>";
								
								print "</td></tr>";
							}
							print "</table>";
						?>
						</div>
					</div>
					<br style="clear:both" />
				</div>
	    	</div>
	    	<br style="clear:both" />
	    </div>
	    <?php
	    	include("bottom.php");
	    ?>
	</div>
</body>
</html>