<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-15" />
    <title>SMART CONSULTING</title>
    <style type="text/css" media="all">
        @import "css/template2.css";
    </style>
        
    <script language="JavaScript">
    	function ConfirmDel(id,type)
    	{
    		if(type == 'news')
    		{
    			msg = "Est� seguro de borrar la noticia";
    			dest = 'viewNews.php?del=1&id=' + id;
    		}	
    		else if(type == 'proj')
    		{
    			msg = "Est� seguro de borrar el proyecto";
    			dest = 'viewProjects.php?del=1&id=' + id;
    		}
    		else if(type == 'suscription')
    		{
    			msg = "Est� seguro de borrar esta suscripcion";
    			dest = 'newsletter.php?del=1&id=' + id;
    		}
    		else
    		{
    			msg = "Est� seguro";	
    		}
    		
    		if(window.confirm(msg + '?'))
    		{
    			document.location.href = dest;
    		}
    	}
    	
    	function showDesc(id)
    	{
    		if(document.getElementById("desc" + id).style.display == 'none' || document.getElementById("desc" + id).style.display == '')
    		{
    			document.getElementById("desc" + id).style.display = 'block';	
    		}
    		else
    		{
    			document.getElementById("desc" + id).style.display = 'none'
    		}
    	}
    	
		var abierto=false;
		
		function AbrirVentana(url,tamx,tamy)
		{
			width = window.screen.width;
			height = window.screen.height;
			posx = width / 2 - tamx / 2;
			posy = height / 2 - tamy / 2;
			VentanaNueva = open(url,"Informaci�","toolbar=no,directories=no,scrollbars=0,menubar=no,status=no,resizable=no,width=" + tamx + "px,height=" + tamy + "px,left=" + posx + "px,top=" + posy + "px");
			abierto=true;//Esta variable se pone a true cuando se abre la ventana.	
		}

    </script>
</head>
<body>