<?php
	include("head.php");
	include("class/project.class.php");
	include("class/mysql.class.php");
	include("class/date.class.php");
	include("class/FileUpload.class.php");
?>
	<div id="content">
			<?php
				include("top.php");
			?>
	     	<div id="contingut">
	     		<div id="contingut_main">
					<?php
						include("menu_contingut.php");
					?>
					<div id="contingut_dins">
						<div id="titolMain">
							<img src="images/arrowBig.jpg" align="absmiddle" />INSERTAR PROYECTO
						</div>
						<div align="center">
							<?php
								$val = "A�adir proyecto";
								$numImages = 4;
								$numFiles = 8;
								$numSponsors = 8;
								$numColaboran = 8;
								
								if(isset($_POST["submitted"]))
								{
									$data = date('Y-m-d');
									$title = $_POST["title"];
									$desc = $_POST["desc"];
									
									/* Check the rest of values, if not correct, add the error message in the error Array */
									
									$error = Array();
									
									if($title == "")
									{
										$error[] = "El t�tulo es nulo.";	
									}
									
									if($desc == "")
									{
										$error[] = "La descripci�n es nula.";	
									}
									
									/* Check the images attached to the project, we just look it the first image has been uploaded */
									
									if(!isset($_POST["id"]))
									{
									
										$images = Array();
										
										if($_FILES["image1"]["error"] != 0)
										{
											$error[] = "Hay un error en la im�gen 1";	
										}
										else
										{
											$images[0] = $_FILES["image1"];
										}
										
										for($i = 2; $i <= $numImages; $i++)
										{
											if($_FILES["image" . $i]["error"] != 0 && $_FILES["image" . $i]["error"] != 4)
											{
												$error[] = "Hay un error en la imagen " . $i;
											}
											else
											{
												if($_FILES["image" . $i]["error"] == 0)
												{
													$images[] = $_FILES["image" . $i];	
												}
											}
										}
										
										$ficheros = Array();
										
										for($i = 1; $i <= $numFiles; $i++)
										{
											if($_FILES["file" . $i]["error"] != 0 && $_FILES["file" . $i]["error"] != 4)
											{
												$error[] = "Hay un error en el fichero " . $i;
											}
											else
											{
												if($_FILES["file" . $i]["error"] == 0)
												{
													$ficheros[] = $_FILES["file" . $i];	
												}
											}
										}
										
										$sponsors = Array();
										
										for($i = 1; $i <= $numSponsors; $i++)
										{
											if($_FILES["sponsor" . $i]["error"] != 0 && $_FILES["sponsor" . $i]["error"] != 4)
											{
												$error[] = "Hay un error en el archivo del sponsor " . $i;
											}
											else
											{
												if($_FILES["sponsor" . $i]["error"] == 0)
												{
													$sponsors[] = $_FILES["sponsor" . $i];	
												}
											}
										}
										
										$colaboran = Array();
										
										for($i = 1; $i <= $numColaboran; $i++)
										{
											if($_FILES["colaboran" . $i]["error"] != 0 && $_FILES["colaboran" . $i]["error"] != 4)
											{
												$error[] = "Hay un error en el archivo del colaborador " . $i;
											}
											else
											{
												if($_FILES["colaboran" . $i]["error"] == 0)
												{
													$colaboran[] = $_FILES["colaboran" . $i];	
												}
											}
										}
										
									}
									
									/* All possible errors have been checked at this point */
									
									if(count($error) == 0)
									{
										
										$my = new SQL();

										$project = new Project($my,"projects","imagesprojects");
										$fileUpload = new FileUpload("files",$my,"ficheros","file");
										$logoUpload = new FileUpload("sponsorsprojects",$my,"sponsors","sponsor");
										$logoColUpload = new FileUpload("colprojects",$my,"sponsors","colaboran");
										
										if(!isset($_POST["id"]))
										{
											$numProj = $project->addProject($title,$data,$desc,$images);
											
											if(count($ficheros) > 0)
											{
												$fileUpload->addFiles($ficheros,$numProj);
											}
											
											if(count($sponsors) > 0)
											{
												$logoUpload->addFiles($sponsors,$numProj);
											}
											
											if(count($colaboran) > 0)
											{
												$logoColUpload->addFiles($colaboran,$numProj);
											}
											
											$msg = "Proyecto a�adido correctamente";
										}
										else
										{
											$project->updateProject($title,$data,$desc,$_FILES,$_POST["borrar"],$_POST["OrigImage"],$_POST["id"]);	
											$fileUpload->updateFiles($_FILES,$_POST["borrarFile"],$_POST["OrigFiles"],$_POST["id"]);
											$logoUpload->updateFiles($_FILES,$_POST["borrarSponsor"],$_POST["OrigSponsors"],$_POST["id"]);
											$logoColUpload->updateFiles($_FILES,$_POST["borrarColaboran"],$_POST["OrigColaboran"],$_POST["id"]);
											$msg = "Proyecto modificado correctamente";
										}
										
										print "<div align=\"center\" class=\"success\">";
										print $msg;
										print "</div>";
									}
									else
									{
										print "<div align=\"left\"><br/>Hay errores en el formulario:";
										print "<ul>";
										foreach($error as $err)
										{
											print "<li>" . $err . "</li>";	
										}
										print "</ul>";
										print "<input type=\"button\" onClick=\"javascript:history.back();\" value=\"Solucionar problemas...\">";
										print "</div>";
									}
								}
								else
								{
									if(isset($_GET["id"]))
									{
										$my = new SQL();
										
										$new = $my->do_selec("SELECT * FROM projects WHERE id = '" . $_GET["id"] . "'");	
										$title = $new[1]["title"];
										$desc = $new[1]["description"];
										$visible = $new[1]["publics"];
										$fecha = new dDate($new[1]["data"]);
										$data = $fecha->EngToSpanish();
										$id = $_GET["id"];
										
										$imagesPro = $my->do_selec("SELECT * FROM imagesprojects WHERE proj = '" . $id . "' ORDER BY ref");
										$numImagesReal = array_shift($imagesPro);
										
										$filesPro = $my->do_selec("SELECT * FROM files WHERE idProject = '" . $id . "' ORDER BY ordered");
										$numFilesReal = array_shift($filesPro);
										
										$sponsorsPro = $my->do_selec("SELECT * FROM sponsorsprojects WHERE idProject = '" . $id . "' ORDER BY ordered");
										$numSponsorsReal = array_shift($sponsorsPro);
										
										$colaboranPro = $my->do_selec("SELECT * FROM colprojects WHERE idProject = '" . $id . "' ORDER BY ordered");
										$numColReal = array_shift($colaboranPro);
										
										$val = "Modificar proyecto";
										
									}	
							?>
							
							<form action="addProjects.php" method="POST" enctype="multipart/form-data">
							<table class="formulari">
								<?php
									if(isset($_GET["id"]))
									{
										print "<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">";	
										for($i = 1; $i <= $numImages; $i++)
										{
											print "<input type=\"hidden\" name=\"OrigImage[" . $i . "]\" value=\"" . $imagesPro[$i - 1]["rutaImagen"] . "\">";	
										}
										
										for($i = 1; $i <= $numFiles; $i++)
										{
											print "<input type=\"hidden\" name=\"OrigFiles[" . $i . "]\" value=\"" . $filesPro[$i - 1]["link"] . "\">";
										}
										
										for($i = 1; $i <= $numSponsors; $i++)
										{
											print "<input type=\"hidden\" name=\"OrigSponsors[" . $i . "]\" value=\"" . $sponsorsPro[$i - 1]["link"] . "\">";
										}
										
										for($i = 1; $i <= $numColaboran; $i++)
										{
											print "<input type=\"hidden\" name=\"OrigColaboran[" . $i . "]\" value=\"" . $colaboranPro[$i - 1]["link"] . "\">";
										}
									}
								?>
								<tr><td class="left">T�tulo:</td><td><input type="text" size="30" maxlength="200" name="title" value="<? print $title; ?>"></td></tr>
								<tr><td class="left">Descripci�n:</td><td><textarea name="desc" cols=44 rows=5><? print $desc; ?></textarea></td></tr>
								<tr><td colspan="2"><b>Im�genes</b><hr></td></tr>
								<?

								for($i = 1; $i <= $numImages; $i++)
								{
									print "<tr><td class=\"left\" colspan=2><table><tr><td width=72>Im�gen " . $i . ":</td><td><input type=\"file\" value=\"" . $imagesPro[$i - 1]["rutaImagen"] . "\" name=\"image" . $i . "\"></td>";
									
									if(isset($_GET["id"]))
									{
										if($imagesPro[$i - 1]["rutaImagen"] == "")
										{
											print "<td align=\"left\">Ninguna</td>";	
										}
										else
										{
											print "<td align=\"center\">&nbsp;<img src=\"../proyectos/" . $id . "/thumbs/" . $imagesPro[$i - 1]["rutaImagen"] . "\" align=\"absmiddle\" height=\"50\" />";
											
											if($i > 1)
											{
												print "&nbsp;&nbsp;&nbsp;&nbsp;[ Borrar ] <input type=\"checkbox\" name=\"borrar[$i]\"value=0>";
											}
											else
											{
												print "&nbsp;";
											}
											
											print "</td>";	
										}
									}
									
									print "</tr></table>";
									
									print "</td></tr>";
								}
								
								if(isset($_GET["id"]))
								{
									?>
										
										<tr><td class="left">&nbsp;</td><td><ul><b>Instrucciones de uso para las im�genes:</b><br/><br/><li>Para no modificar la im�gen actual dejar el campo en blanco y el checkbox de borrar desactivado.</li><li>Para cambiar o a�adir una im�gen seleccionar la nueva im�gen y dejar el checkbox de borrar desactivado. La im�gen actual, en caso de exisitir, ser� borrada autom�ticamente.</li><li>Para eliminar una im�gen sin sustituirla por otra, marcar el checkbox borrar en la im�gen.</li></ul><br/></td></tr>
									<?
								}
								?>
								<tr><td class="left">&nbsp;</td><td>La im�gen 1 ser� a partir de la cu�l el programa har� un corte para mostrarlo en la p�gina principal.<br/><br/>No es obligatorio a�adir 4 im�genes, tan solo la <b>primera es obligatoria</b>.<br/><br/></td></tr>
								<tr><td colspan="2"><b>Ficheros</b><hr></td></tr>
								<?
								
								for($i = 1; $i <= $numFiles; $i++)
								{
									print "<tr><td class=\"left\" colspan=2><table><tr><td width=72>Fichero " . $i . ":</td><td><input type=\"file\" value=\"" . $filesPro[$i - 1]["link"] . "\" name=\"file" . $i . "\"></td>";
									
									if(isset($_GET["id"]))
									{
										if($filesPro[$i - 1] == "")
										{
											print "<td align=\"left\">Ninguno</td>";	
										}
										else
										{
											print "<td align=\"center\">&nbsp;<a href=\"proyectos/" . $id . "/ficheros/" . $filesPro[$i - 1]["link"] . "\" target=\"_blank\">" . $filesPro[$i - 1]["link"] . "</a>";
											print "&nbsp;&nbsp;&nbsp;&nbsp;[ Borrar ] <input type=\"checkbox\" name=\"borrarFile[$i]\"value=0>";
											print "</td>";	
										}
									}
									
									print "</tr></table>";
									
									print "</td></tr>";
								}
								?>
								<tr><td colspan="2"><b>Sponsors</b><hr></td></tr>
								<?
								
								for($i = 1; $i <= $numSponsors; $i++)
								{
									print "<tr><td class=\"left\" colspan=2><table><tr><td width=72>Logo " . $i . ":</td><td><input type=\"file\" value=\"" . $sponsorsPro[$i - 1]["link"] . "\" name=\"sponsor" . $i . "\"></td>";
									
									if(isset($_GET["id"]))
									{
										if($sponsorsPro[$i - 1] == "")
										{
											print "<td align=\"left\">Ninguno</td>";	
										}
										else
										{
											print "<td align=\"center\">&nbsp;<a href=\"proyectos/" . $id . "/sponsors/" . $sponsorsPro[$i - 1]["link"] . "\" target=\"_blank\">" . $sponsorsPro[$i - 1]["link"] . "</a>";
											print "&nbsp;&nbsp;&nbsp;&nbsp;[ Borrar ] <input type=\"checkbox\" name=\"borrarSponsor[$i]\"value=0>";
											print "</td>";	
										}
									}
									
									print "</tr></table>";
									
									print "</td></tr>";
								}
								?>
								<tr><td colspan="2"><b>Colaboran</b><hr></td></tr>
								<?
								
								for($i = 1; $i <= $numColaboran; $i++)
								{
									print "<tr><td class=\"left\" colspan=2><table><tr><td width=72>Logo " . $i . ":</td><td><input type=\"file\" value=\"" . $sponsorsPro[$i - 1]["link"] . "\" name=\"colaboran" . $i . "\"></td>";
									
									if(isset($_GET["id"]))
									{
										if($colaboranPro[$i - 1] == "")
										{
											print "<td align=\"left\">Ninguno</td>";	
										}
										else
										{
											print "<td align=\"center\">&nbsp;<a href=\"proyectos/" . $id . "/sponsors/" . $colaboranPro[$i - 1]["link"] . "\" target=\"_blank\">" . $colaboranPro[$i - 1]["link"] . "</a>";
											print "&nbsp;&nbsp;&nbsp;&nbsp;[ Borrar ] <input type=\"checkbox\" name=\"borrarColaboran[$i]\"value=0>";
											print "</td>";	
										}
									}
									
									print "</tr></table>";
									
									print "</td></tr>";
								}
								?>
								<tr><td class="left">&nbsp;</td><td><input type="submit" name="submitted" value="<? print $val; ?>"></td></tr>
							</table>
							</form>
							
							<?php
								}
							?>
							
						</div>
					</div>
					<br style="clear:both" />
				</div>
	    	</div>
	    	<br style="clear:both" />
	    </div>
	    <?php
	    	include("bottom.php");
	    ?>
	</div>
</body>
</html>