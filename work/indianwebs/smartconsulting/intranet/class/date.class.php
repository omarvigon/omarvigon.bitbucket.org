<?php
class dDate
{
	var $data;
	
	function dDate($string)
	{
		$this->data = $string;
	}
	
	function EngToSpanish()
	{	
		$any = substr($this->data,0,4);
		$mes = substr($this->data,5,2);
		$dia = substr($this->data,8,2);
		$this->data = $dia . "/" . $mes . "/" . $any;
		return $this->data;
	}
	
	function withMonth()
	{
		$mesos = Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre");
		$any = substr($this->data,0,4);
		$dia = substr($this->data,8,2);
		$mes = substr($this->data,5,2);
		return $mesos[$mes - 1] . " " . $any;
	}
	
	function SpanishToEng()
	{
		$any = substr($this->data,6,4);
		$mes = substr($this->data,3,2);
		$dia = substr($this->data,0,2);
		$this->data = $any . "-" . $mes . "-" . $dia;
		return $this->data;
	}
}
?>