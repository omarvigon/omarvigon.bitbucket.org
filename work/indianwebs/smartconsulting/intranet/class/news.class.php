<?php

class News
{
	var $table;
	var $fields;
	var $dbresource;
	var $maxSizeImageSmall = 150;
	
	function News($dbresource,$table)
	{
		$this->dbresource = $dbresource;
		$this->table = $table;
	}
	
	function addNews($title,$date,$desc,$visible,$link,$images)
	{
		
		$a = array_shift($images);
		
		$q = "INSERT INTO " . $this->table . " VALUES (NULL,'" . addslashes($title) . "',\"" . addslashes($desc) . "\",'" . $date . "','" . $visible . "','" . $images[0]["name"] . "','" . $link . "')";
		$this->dbresource->insert($q);
		
		$s = "SELECT MAX(id) AS maximumID FROM " . $this->table;
		$res = $this->dbresource->do_selec($s);
		$max = 1;
		if($res[0] != "")
		{
			$max = $res[0];
		}
		
		@mkdir("../news/" . $max);
		@mkdir("../news/" . $max . "/thumbs");
		
		$dir = "../news/" . $max . "/";
		
		/* Reduce image size and store them in the thumbs directory */
		
		$this->reduceAndSave($dir,$images);
		
		/* Move big images to the destination directory and save the link on the database */
		
		$a = 1;
		foreach($images as $img)
		{
			move_uploaded_file($img["tmp_name"], $dir . $img["name"]);
			$a++;
		}
	}
	
	function updateNews($title,$date,$desc,$visible,$link,$id,$images)
	{
		$imag = "";
		if(count($images) >= 2)
		{
			$a = array_shift($images);	
			$max = $id;
			@mkdir("../news/" . $max);
			@mkdir("../news/" . $max . "/thumbs");
			
			$dir = "../news/" . $max . "/";
			
			/* Reduce image size and store them in the thumbs directory */
			
			$this->reduceAndSave($dir,$images);
			
			/* Move big images to the destination directory and save the link on the database */
			
			$a = 1;
			foreach($images as $img)
			{
				move_uploaded_file($img["tmp_name"], $dir . $img["name"]);
				$a++;
			}
			
			$imag = "img = '" . $img["name"] . "',";
		}

		$q = "UPDATE " . $this->table . " SET link = '" . $link . "'," . $imag . "title = '" . $title . "', description = '" . addslashes($desc) . "', data = '" . $date . "', publics = '" . $visible . "' WHERE id = '" . $id . "'";
		$this->dbresource->update($q);
	}
	
	function delete($id)
	{
		$q = "DELETE FROM " . $this->table . " WHERE id = '" . $id . "'";
		$this->dbresource->delete($q);	
	}
	
	function reduceAndSave($dir,$images)
	{
		foreach($images as $img)
		{
			$origin = imageCreateFromJpeg($img["tmp_name"]);
			$tam = getImageSize($img["tmp_name"]);
			$size = $this->getNewSize($tam[0],$tam[1],$this->maxSizeImageSmall);
			$dest = imageCreateTrueColor($size[0],$size[1]);
			imagecopyresampled ($dest, $origin, 0, 0, 0, 0, $size[0], $size[1], $tam[0], $tam[1]);
			imageJPEG($dest,$dir . "thumbs/" . $img["name"]);
		}
	}
	
	function getNewSize($sizex,$sizey,$maxSize)
	{
		$tamBigger = $sizex;
		if($sizey > $tamBigger)
		{
			$tamBigger = $sizey;	
		}
		
		$desf = $maxSize / $tamBigger;
		
		
		return Array(round($sizex * $desf), round($sizey * $desf));
	}	
}

?>