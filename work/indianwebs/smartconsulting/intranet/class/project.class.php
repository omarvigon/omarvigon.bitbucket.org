<?php

class Project
{
	var $table;
	var $tableImages;
	var $fields;
	var $dbresource;
	var $sizeIndexImage = 50;  //size in px
	var $maxSizeImageSmall = 250; //size in px
	
	function Project($dbresource,$table,$tableImages)
	{
		$this->dbresource = $dbresource;
		$this->tableImages = $tableImages;
		$this->table = $table;
	}
	
	function addProject($title,$date,$desc,$images)
	{
		$s = "SELECT MAX(id) AS maximumID FROM projects";
		$res = $this->dbresource->do_selec($s);
		$max = 1;
		if($res[0] != "")
		{
			$max = $res[0] + 1;
		}
		
		/* Make directorys to store the images */
		
		@mkdir("../proyectos/" . $max);
		@mkdir("../proyectos/" . $max . "/thumbs");
		
		$dir = "../proyectos/" . $max . "/";
		
		/* Reduce image size and store them in the thumbs directory */
		
		$this->reduceAndSave($dir,$images);
		
		/* Make the image which appears in the main site */
		
		$orig = $dir . "thumbs/" . $images[0]["name"];
		
		$this->makeImageMainPage($dir,$orig);
		
		/* Move big images to the destination directory and save the link on the database */
		
		$a = 1;
		foreach($images as $img)
		{
			move_uploaded_file($img["tmp_name"], $dir . $img["name"]);
			$q = "INSERT INTO " . $this->tableImages . " VALUES ('" . $max . "','" . $img["name"] . "','" . $a . "')";	
			$this->dbresource->insert($q);
			$a++;
		}

		$q = "INSERT INTO " . $this->table . " VALUES ('" . $max . "','" . $desc . "','" . $title . "','" . $date . "')";
		$this->dbresource->insert($q);
		
		return $max;
	}
	
	function makeImageMainPage($dir,$img)
	{
			$origin = imageCreateFromJpeg($img);
			$tam = getImageSize($img);
			$size = Array($this->sizeIndexImage,$this->sizeIndexImage);
			$dest = imageCreateTrueColor($size[0],$size[1]);
			imagecopyresampled ($dest, $origin, 0, 0, ($tam[0] / 2) - ($size[0] / 2), ($tam[1] / 2) - ($size[1] / 2), $size[0], $size[1], $size[0], $size[1]);
			imageJPEG($dest,$dir . "portrait000.jpg");
	}
	
	function reduceAndSave($dir,$images)
	{
		foreach($images as $img)
		{
			$origin = imageCreateFromJpeg($img["tmp_name"]);
			$tam = getImageSize($img["tmp_name"]);
			$size = $this->getNewSize($tam[0],$tam[1],$this->maxSizeImageSmall);
			$dest = imageCreateTrueColor($size[0],$size[1]);
			imagecopyresampled ($dest, $origin, 0, 0, 0, 0, $size[0], $size[1], $tam[0], $tam[1]);
			imageJPEG($dest,$dir . "thumbs/" . $img["name"]);
		}
	}
	
	function getNewSize($sizex,$sizey,$maxSize)
	{
		$tamBigger = $sizex;
		if($sizey > $tamBigger)
		{
			$tamBigger = $sizey;	
		}
		
		$desf = $maxSize / $tamBigger;
		
		
		return Array(round($sizex * $desf), round($sizey * $desf));
	}	
	
	function updateProject($title,$date,$desc,$files,$borrar,$origImages,$id)
	{
		/* PRE : Suponiendo que se han seguido las instrucciones de uso correctamente */
		
		$this->borrarImages($borrar,$origImages,$id);
		
		$filesKeys = array_keys($files);
		
		$files2 = Array();
		
		$a = 0;
		
		foreach($files as $file)
		{
			if(substr($filesKeys[$a],0,5) == "image")
			{
				$files2[] = $file;
			}	
			$a++;
		}
		
		$this->updateImages($files2,$origImages,$id);
		
		$q = "UPDATE " . $this->table . " SET title = '" . $title . "', description = '" . $desc . "' WHERE id = '" . $id . "'";
		$this->dbresource->update($q);
	}
	
	function updateImages($files,$origImages,$id)
	{
		$sol = Array();
		$a = 1;
		
		$makePortrait = false;	
		
		foreach($files as $file)
		{
			if($file["error"] == 0)
			{
				$sol[$a] = $file;	
				if($a == 1)
				{
					/* First image modified */
					$makePortrait = true;	
				}
			}
			$a++;
		}	
		
		$dir = "../proyectos/" . $id . "/";
		
		/* Reduce modified images size and store them in the thumbs directory */
		
		$this->reduceAndSave($dir,$sol);
		
		/* Make the image which appears in the main site */
		
		if($makePortrait)
		{
			$orig = $dir . "thumbs/" . $sol[1]["name"];
			$this->makeImageMainPage($dir,$orig);
		}
		
		$solKeys = array_keys($sol);
		

		$s = "SELECT MAX(ref) AS maximumID FROM " . $this->tableImages . " WHERE proj = '" . $id . "'";
		$res = $this->dbresource->do_selec($s);
		$max = 1;
		if($res[0] != "")
		{
			$max = $res[0] + 1;
		}
		
		$a = 0;
		
		foreach($sol as $solucio)
		{
			if(!isset($origImages[$solKeys[$a]]) || $origImages[$solKeys[$a]] == "")
			{
				$q = "INSERT INTO " . $this->tableImages . " VALUES ('" . $id . "','" . $solucio["name"] . "','" . $max . "')";	
				$this->dbresource->insert($q);
				$max++;
			}
			else
			{
				@unlink("../proyectos/" . $id . "/" . $origImages[$keysBorrar[$i]]);
				$this->dbresource->update("UPDATE " . $this->tableImages . " SET rutaImagen = '" . $solucio["name"] . "' WHERE proj = '" . $id . "' AND rutaImagen = '" . $origImages[$solKeys[$a]] . "'");	
			}
			move_uploaded_file($solucio["tmp_name"], $dir . $solucio["name"]);
			$a++;
		}
	}
	
	function borrarImages($borrar,$origImages,$id)
	{
		if(is_array($borrar))
		{
			$keysBorrar = array_keys($borrar);
			for($i = 0; $i <= (count($borrar) - 1); $i++)
			{
				 @unlink("../proyectos/" . $id . "/" . $origImages[$keysBorrar[$i]]);
				 @unlink("../proyectos/" . $id . "/thumbs/" . $origImages[$keysBorrar[$i]]);
				 $this->dbresource->delete("DELETE FROM " . $this->tableImages . " WHERE proj = '" . $id . "' AND rutaImagen = '" . $origImages[$keysBorrar[$i]] . "'");
				 
				 $q = "UPDATE " . $this->tableImages . " SET ref = (ref - 1) WHERE proj = '" . $id . "' AND ref > " . $keysBorrar[count($keysBorrar) - 1 - $i] . "";
				 $this->dbresource->update($q);
			}
		}
	}
	
	function delete_files($target, $exceptions, $output=true)
	{
	   $sourcedir = opendir($target);
	   
	   while(false !== ($filename = readdir($sourcedir)))
	   {
	       if(!in_array($filename, $exceptions))
	       {
	           if($output)
	           { echo "Processing: ".$target."/".$filename."<br>"; }
	           if(is_dir($target."/".$filename))
	           {
	               // recurse subdirectory; call of function recursive
	               $this->delete_files($target."/".$filename, $exceptions,false);
	           }
	           else if(is_file($target."/".$filename))
	           {
	               @unlink($target."/".$filename);
	           }
	       }
	   }
	   
	   closedir($sourcedir);
	   if(rmdir($target))
	   { return true; }
	   else
	   { return false; }
	}
	
	function delete($id)
	{
		/* Delete the images physically stored */
		
		$exceptions = Array(".","..");
		$this->delete_files("../proyectos/" . $id, $exceptions, false);
		
		/* Delete the project and the images associated */
		
		$q = "DELETE FROM " . $this->table . " WHERE id = '" . $id . "'";
		$this->dbresource->delete($q);	
		$s = "DELETE FROM " . $this->tableImages . " WHERE proj = '" . $id . "'";
		$this->dbresource->delete($s);
		
		/* Delete files and logos */
		
		$s = "DELETE FROM files WHERE idProject = '" . $id . "'";
		$this->dbresource->delete($s);
		$s = "DELETE FROM sponsorsprojects WHERE idProject = '" . $id . "'";
		$this->dbresource->delete($s);
		$s = "DELETE FROM colprojects WHERE idProject = '" . $id . "'";
		$this->dbresource->delete($s);
	}
}

?>