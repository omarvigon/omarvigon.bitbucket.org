<?php
	class FileUpload
	{
		var $table;
		var $dbresource;
		var $numFiles = 8;
		var $directory = "ficheros";
		var $nameFile = "file";
		
		function FileUpload($table,$dbresource,$directory,$nameFile)
		{
			$this->table = $table;
			$this->dbresource = $dbresource;
			$this->directory = $directory;
			$this->nameFile = $nameFile;
		}
		
		function addFiles($files,$numProj)
		{
			$dir = "../proyectos/" . $numProj . "/" . $this->directory . "/";
			@mkdir($dir);
			
			$a = 0;
			
			foreach($files as $fich)
			{
				move_uploaded_file($fich["tmp_name"], $dir . $fich["name"]);
				$q = "INSERT INTO " . $this->table . " VALUES ('" . $numProj . "','" . $fich["name"] . "','" . $a . "')";	
				$this->dbresource->insert($q);	
				$a++;
			}	
		}
		
		function stripExt($string)
		{
			$a = explode(".",$string);
			$str = "";
			for($i=0; $i < count($a) - 1; $i++)
			{
				$str .= $a[$i];
				if($i != count($a) - 2)
				{
					$str .= ".";	
				}
			}
				
			return $str;
		}
		
		function updateFiles($files,$borrar,$origFiles,$id)
		{
			$dir = "../proyectos/" . $id . "/" . $this->directory . "/";
			@mkdir($dir);
			
			$this->delFiles($borrar,$origFiles,$id);
			
			$filesKeys = array_keys($files);
			$files2 = Array();
			$a = 0;
			
			foreach($files as $file)
			{
				if(substr($filesKeys[$a],0,strlen($this->nameFile)) == $this->nameFile)
				{
					$files2[] = $file;
				}	
				$a++;
			}
			
			$sol = Array();
			$a = 1;
			
			foreach($files2 as $file)
			{
				if($file["error"] == 0)
				{
					$sol[$a] = $file;	
				}
				$a++;
			}	
			
			$dir = "../proyectos/" . $id . "/" . $this->directory . "/";
	
			$solKeys = array_keys($sol);
	
			$a = 0;
			
			$s = "SELECT MAX(ordered) AS maximumID FROM " . $this->table . " WHERE idProject = '" . $id . "'";
			$res = $this->dbresource->do_selec($s);
			$max = 0;
			if($res[0] != "")
			{
				$max = $res[0] + 1;
			}
			
			foreach($sol as $solucio)
			{
				if(!isset($origFiles[$solKeys[$a]]) || $origFiles[$solKeys[$a]] == "")
				{
					$q = "INSERT INTO " . $this->table . " VALUES ('" . $id . "','" . $solucio["name"] . "','" . $max . "')";	
					$this->dbresource->insert($q);
					$max++;
				}
				else
				{
					@unlink("../proyectos/" . $id . "/" . $this->directory . "/" . $orig[$keysBorrar[$i]]);
					$this->dbresource->update("UPDATE " . $this->table . " SET link = '" . $solucio["name"] . "' WHERE idProject = '" . $id . "' AND link = '" . $origFiles[$solKeys[$a]] . "'");	
				}
				
				move_uploaded_file($solucio["tmp_name"], $dir . $solucio["name"]);
				$a++;
			}
		}
		
		function delFiles($borrar,$orig,$id)
		{
			if(is_array($borrar))
			{
				$keysBorrar = array_keys($borrar);
				for($i = 0; $i <= (count($borrar) - 1); $i++)
				{
					 @unlink("../proyectos/" . $id . "/" . $this->directory . "/" . $orig[$keysBorrar[$i]]);
					 $this->dbresource->delete("DELETE FROM " . $this->table . " WHERE idProject = '" . $id . "' AND link = '" . $orig[$keysBorrar[$i]] . "'");
					 
					$q = "UPDATE " . $this->table . " SET ordered = (ordered - 1) WHERE idProject = '" . $id . "' AND ordered > " . ($keysBorrar[count($keysBorrar) - 1 - $i] - 1) . "";
				 	$this->dbresource->update($q);
				}
			}
		}
	}
?>