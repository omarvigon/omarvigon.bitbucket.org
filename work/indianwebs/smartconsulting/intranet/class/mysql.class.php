<?php 

class SQL
{
	var $host = "localhost";
	var $link;
	var $result;
	//var $user = "root";
	//var $pass;
	var $user = "web";
	var $pass = "m0x0sm4rt";
	var $db = "smartWeb";
	//var $log;
	
	function SQL()
	{
		if(!($this->link=mysql_connect($this->host,$this->user,$this->pass)))
		{
			echo "Error conectando a la base de datos";
			exit();
		}
		
		if (!mysql_select_db($this->db,$this->link))
		{
			echo "Error conectando";
			exit();
		}
		else
		{
			//echo "Correct";	
		}
		
		//$this->log = new log();
	}
	
	function insert($ins)
	{
		//$this->log->guardar($ins);
		mysql_query($ins);	
	}
	
	function update($up)
	{
		//$this->log->guardar($up);
		$ups = mysql_query($up);
		return mysql_affected_rows();			
	}
	
	function delete($del)
	{
		//$this->log->guardar($del);
		$res = mysql_query($del);	
		return mysql_affected_rows();
	}
	
	function updateUser($login,$p1,$p2,$name)
	{
		$correcto = true;	
		$error = Array();
		
		if($p1 != $p2)
		{
			$correcto = false;
			$error[] = "Los passwords no coinciden";	
		}
		else
		{
			$rowsChanged = $this->update("UPDATE users SET password = '" . $p1 . "', name = '" . $name . "' WHERE login = '" . $login . "'");	
		}
		
		return $error;
	}
	
	function insertUser($login,$p1,$p2,$name)
	{
		$correcto = true;
		$error = Array();
		
		$view = $this->do_selec("SELECT * FROM users WHERE login = '" . $login . "'");
		
		if($view[0] != 0)
		{ 
			$error[] = "Ya existe el usuario";
			$correcto = false;	
		}
		
		if($p1 != $p2)
		{
			$error[] = "Los passwords no coinciden";
			$correcto = false;	
		}
		
		if($correcto)
		{
			//print "No hay errores";
			$rowsChanged = $this->insert("INSERT INTO users VALUES('" . $login . "','" . $p1 . "','" . $name . "')");	
		}
		
		return $error;
	}
	
	function showAssigUser($user,$mss)
	{
		$viewE = $this->do_selec("SELECT DISTINCT(pais) FROM assignacions WHERE login = '" . $user . "' ORDER BY pais");
				
		print "\n<li>" . $user . "</li>";
		print "<ul>";
		
		if(!is_array($viewE))
		{
			print "<li>No existen asignaciones para este usuario</li>";
		}
		else
		{
			foreach($viewE as $assig)
			{
					print "\n<li>" . $mss->transLand($assig) . "</li>";
					print "<ul>";
					
					$viewD = $this->do_selec("SELECT DISTINCT(division) FROM assignacions WHERE pais = '" . $assig . "' AND login = '" . $user . "' ORDER BY pais");
					foreach($viewD as $division)
					{
						print "\n<li>" . $mss->transDiv($division,$assig) . "</li>";	
						print "<ul>";
							$viewEqui = $this->do_selec("SELECT DISTINCT(equipo) FROM assignacions WHERE division = '" . $division . "' AND pais = '" . $assig . "' AND login = '" . $user . "' ORDER BY pais");
							foreach($viewEqui as $equipo)
							{
								if($equipo == "ALL")
								{
									$arrEquip = $mss->showTeamsDiv($division);
									foreach($arrEquip as $squad)
									{
										print "<li>" . $squad["ShortName"] . "</li>";
									}
								}
								else
								{
									print "<li>" . $mss->transTeam($equipo,$division) . "</li>";
								}
							}				
						print "</ul>";
					}
					
					print "</ul>";
			}
		}
		
		print "\n</ul>";	
	}
	
	function do_selec($query)
	{
		$this->result = mysql_query($query,$this->link);
		$rows = mysql_num_rows($this->result);
		$fields = mysql_num_fields($this->result);
		
		if($fields != 1)
		{
			$view = Array($rows);
			while($assoc = mysql_fetch_assoc($this->result))
			{
				$view[] = $assoc;	
			}
		}
		else
		{
			$objeto = mysql_fetch_field($this->result);
			
			while($obj = mysql_fetch_array($this->result))
			{
				$view[] = $obj[$objeto->name];
			}	
		}
		
		return $view;
		
		//Returns a vector of Vectors with the first element being the number of rows that the query returns
		// and the rest is the result of the query
	}
	function close()
	{
		mysql_close($this->link);	
	}
} 

?>