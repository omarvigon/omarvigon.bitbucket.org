<?php
	function textToParagraphs($text)
	{
		return "<p>" . str_replace("\n","</p><p>", $text) . "</p>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-15" />
    <title>SMART CONSULTING</title>
    <style>
    
    body
    {
    	font-family	: Verdana,Tahoma;
    	font-size	: 12px;
    	margin-top	: 60px;
		background-color:#AAC785;
    }
    
    body a
    {
    	color		: #79A31F;	
    }
    	
    #header
    {
    	background : url('http://smartconsulting.es/newsletter/images/topNewsLetter.jpg') no-repeat;
    	height		: 156px;
    	
    }
    
    #content
    {
    	width		: 870px;
    	margin-left	: auto;
    	margin-right: auto;
    }
    
    #data
    {
    	position	: relative;
    	top			: 120px;
    	left		: 85px;
    	padding		: 6px;
    	text-align	: center;
    	width		: 190px;
    	font-size	: 18px;
    	font-weight	: bold;
    	color		: #EF4E3C;
    }
    
    #bottom
    {
    	text-align	: center;
    	padding-top	: 10px;
    	padding-bottom	: 10px;
		margin-left:2px;
    	font-weight	: bold;
		background:url('http://smartconsulting.es/newsletter/images/bottom.gif') no-repeat;
    }
    
    #newsHeader
    {
    	background	: url('http://smartconsulting.es/newsletter/images/costatTitle.jpg') no-repeat;
    	padding		: 6px;
    	font-size	: 16px;
    	font-weight	: bold;
    	padding-left: 40px;
    	color		: #fff;
    }
    
    #main
    {
		margin-left:89px;
    	padding-top	: 10px;
    	padding-left: 40px;
    	/*background	: url('http://smartconsulting.es/newsletter/images/leftMain.jpg') repeat-y;*/
		background-color:#FFFFFF;
		width:654px;
    }
    
    .headerTitle
    {
    	/*background	: url('http://smartconsulting.es/newsletter/images/costatTitle.jpg') no-repeat left 50%;*/
    	padding-left: 30px;
    	margin-top	: 10px;
    	padding-bottom	: 0px;
    	font-size	: 20px;
    	font-weight	: bold;
    	color		: #575B60;
    }
    
    .newsContingut
    {
    	margin-left	: 0px;
    	margin-top		: 0px;
    	padding			: 1px;
    	text-align		: center;
    }
    
    .newsContingut p
    {
    	text-align		: justify;
    }
    
    .newsContingut img
    {
    	margin-left		: auto;
    	margin-right	: auto;
    	//border			: 1px solid #000;
    }
    
	</style>
</head>
<body>
	<div id="content">
		<div id="header">
			<div id="data">
				<?php
					include("../intranet/class/date.class.php");
					$dat = new dDate(date('Y-m-d'));
					print $dat->withMonth();
				?>
			</div>
		</div>
	    <div id="main">
	    	<div class="news">
	    		<?php
	    			include("../intranet/class/mysql.class.php");
	    			$query = "SELECT * FROM news WHERE MONTH(data) = '" . date('m') . "'";
	    			$host = "http://smartconsulting.es/";
	    			$my = new SQL();
	    			$res = $my->do_selec($query);
	    			$num = array_shift($res);
	    			foreach($res as $new)
	    			{
	    		?>
	    		<div class="headerTitle">
	    			<?php print $new["title"]; ?>
	    		</div>
	    		<div class="newsContingut">
	    			<table cellpadding="5" width="600" style="border-left:6px solid #E96E06;border-top:6px solid #E96E06;">
	    			<tr><td valign="top">
	    			<?php print textToParagraphs($new["description"]); ?>
	    			</td><td valign="top"><p><?php if($new["img"] != "") print "<img src=\"" . $host . "news/" . $new["id"] . "/thumbs/" . $new["img"] . "\">";?></p></td></tr>
	    			</table>
	    		</div>
	    		<?
	    			}
	    		?>
	    	</div>
	    </div>	
	    <div id="bottom">
	    	 Newsletter Smart Consulting
	    </div>
	</div>
</body>
</html>
