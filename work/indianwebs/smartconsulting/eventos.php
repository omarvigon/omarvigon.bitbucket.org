<?php
	include("head.php");
?>
<td class="centro">
<p class="titolMain"><img src="images/arrowBig.jpg">Eventos</p>
<p><br>Investigamos, definimos y organizamos proyectos de gran envergadura  dirigidos a los ciudadanos de origen latinoamericano, nuestro equipo de  marketing se encarga de desarrollar nuevas ideas que son estudiadas  minuciosamente y nuestro departamento comercial las implementa.</p>
<p>Contamos con la experiencia de haber desarrollado proyectos empresariales y  deportivos complejos, siempre dirigidos a fidelizar al p&uacute;blico latino, por todo  ello grandes empresas nos avalan d&aacute;ndonos su confianza y apoyando todas nuestras  ideas y proyectos.</p>

<div align="center"><br><br><img src="images/eventos2.gif"><br><br><br></div>

<p>Hemos creado y consolidado el proyecto ferial latinoamericano m&aacute;s grande  hasta la fecha hecho en Europa (Vive Latinoam&eacute;rica), que en su primera edici&oacute;n  hizo que el recinto Ferial de La   Farga de L&rsquo;Hospitalet&nbsp; 8.500 m&sup2; quedase peque&ntilde;o.  Las empresas que asistieron ofrecieron sus servicios y productos. Una de las  &aacute;reas m&aacute;s importantes fue la que ocuparon varias empresas en la b&uacute;squeda de  trabajadores para cubrir sus necesidades de producci&oacute;n y desarrollo  empresarial, en dos d&iacute;as se entregaron m&aacute;s de 4.000 CV.</p>

<p><span class="titolMain"><img src="images/arrowBig.jpg" /> Comunicaci&oacute;n</span></p>
<p><br>Muchas veces las empresas se plantean preguntas como: &iquest;Qu&eacute; imagen gr&aacute;fica  debo aplicar? &iquest;C&oacute;mo captar la atenci&oacute;n del latino? &iquest;En que medio de  comunicaci&oacute;n radio, televisi&oacute;n, prensa obtendr&eacute; mayor rentabilidad y en que  emisora, revista o diario? &iquest;He de realizar la misma campa&ntilde;a si me dirijo a  ciudadanos colombianos, ecuatorianos, peruanos, argentinos, bolivianos,  uruguayos, dominicanos, etc.?.<br />
  &nbsp;<br />
  A todas estas cuestiones podemos ofrecerte la respuesta m&aacute;s adecuada para  que alcances tus objetivos. En definitiva ofrecemos a nuestros clientes  soluciones globales de comunicaci&oacute;n, ayud&aacute;ndoles a conseguir sus objetivos  estrat&eacute;gicos y potenciar su marca en los colectivos latinos. </p>
  
<div align="center"><br>
  <br><br><img src="images/eventos1.gif"><br><br><br>
</div>

<p>Somos partners de los m&aacute;s representativos medios de comunicaci&oacute;n latinos a  nivel nacional de esta forma podr&aacute;s desarrollar tus campa&ntilde;as de comunicaci&oacute;n  con la mayor eficacia y a un precio ajustado a tus expectativas.</p><br><br>

</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>		
</body>
</html>