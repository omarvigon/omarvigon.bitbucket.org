<?php
	include("head.php");
?>
<td valign="top" class="centro">
<p class="titolMain"><img src="images/arrowBig.jpg">NEWS</p>
							
<p align="center" style="font-weight: bold">EL EVENTO DE LA INTEGRACI&Oacute;N</p>
<p><strong>Vive! Latinoam&eacute;rica, nuevamente con el patrocinio de Ford  Espa&ntilde;a, fue un &eacute;xito de p&uacute;blico en su segunda edici&oacute;n. Con 21.000 visitantes ya  se ha consolidado como el evento ideal para disfrutar en familia de la cultura  y las tradiciones latinoamericanas, adem&aacute;s permiti&oacute; a los asistentes conocer  las ofertas de productos y servicios dirigidos al colectivo latino</strong><strong>.</strong></p>
<p>Estuvieron  presentes 86 empresas y asociaciones que ocuparon 8.500m2 de exposici&oacute;n. La  amplia oferta gastron&oacute;mica de diversos pa&iacute;ses latinoamericanos Colombia, Per&uacute;,  M&eacute;xico, Argentina, Bolivia y Rep&uacute;blica Dominicana, entre otros y el escenario  de 60m2, en el que grupos folkl&oacute;ricos como el Carnaval de Barranquilla, Per&uacute;  Ritmos y Costumbres o Mariachis Semblanza, entre otros, hicieron vibrar al  p&uacute;blico asistente. Tambi&eacute;n destac&oacute; la presencia de la marca internacional de  moda colombiana Faride que a trav&eacute;s de su representante en Espa&ntilde;a Kiubo,  ofreci&oacute; un intenso y espectacular desfile de moda digno de las pasarelas internacionales  m&aacute;s importantes.</p> 
<p align="center"><img src="images/noticia6_1.jpg" alt="Feria Latinoamericana"></p>
<p>Este a&ntilde;o <strong>Vive! Latinoam&eacute;rica</strong> cont&oacute; con  dos nuevas &aacute;reas: en primer lugar, <strong>selecci&oacute;n de personal</strong>,  con la participaci&oacute;n de los Mossos d&rsquo;Esquadra y del Ministerio de Defensa,  entre otras grandes empresas. En segundo lugar, <strong>el &aacute;rea de  emprendedores</strong>, a cargo de la Fundaci&oacute; Un Sol M&oacute;n, obra social  de Caixa Catalunya, dedicada a la promoci&oacute;n y asesoramiento de nuevas  iniciativas empresariales por parte de los ciudadanos latinos. </p>
<p>La feria tambi&eacute;n cont&oacute; con una &aacute;rea  infantil de 300 m2, a cargo de la empresa Mundo Sonrisas y patrocinada por  Banesto, en la que ni&ntilde;os y padres disfrutaron de los juegos y las actividades  dirigidas por monitores especializados.&nbsp;  Por otra parte, <strong>Vive! Latinoam&eacute;rica</strong> tuvo la presencia del Real Club Deportivo Espanyol, en su stand destacados  jugadores firmaron aut&oacute;grafos, se hicieron fotograf&iacute;as con ni&ntilde;os y regalaron  obsequios del club a los asistentes.<br />
<p align="center"><img src="images/public.jpg" alt="Feria Latinoamericana"></p>
Participaron tambi&eacute;n inmobiliarias,  agencias de viaje, tiendas de moda, empresas de alimentaci&oacute;n, entre otros  negocios vinculados al colectivo latino que reside en Espa&ntilde;a. Adem&aacute;s estuvieron  presentes las principales empresas de env&iacute;o de dinero, entidades financieras y  aseguradoras que prestan servicios a los inmigrantes.<br />
  Fue una aut&eacute;ntica fiesta de diversidad, multiculturalidad y convivencia en  la que los expositores alcanzaron sus objetivos y el p&uacute;blico que asisti&oacute;  disfrut&oacute; del ambiente cordial, divertido y familiar.</p>
<p>Quiero agradeceros la confianza que hab&eacute;is depositado en la organizaci&oacute;n de  la feria y deciros que ya estamos preparando una nueva edici&oacute;n para el 2008.<br />
  Os esperamos!</p>
<p>Cordialmente,</p>
<p>Enric Garc&iacute;a<br />
  Director General</p>
<p align="justify"><br />
<br />
</p>
						
<p>&nbsp;</p>

</td>
</tr>
<tr><td colspan="2"><img src="images/bottom.gif"></td></tr>
</table>

</body>
</html>