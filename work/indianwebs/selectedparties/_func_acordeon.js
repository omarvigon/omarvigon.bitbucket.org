//scripts have been put here as a courtesy to you, the sourcecode viewer.this script uses mootools: http://mootools.net

var stretchers = $$('div.accordion');
var togglers = $$('h3.toggler');
stretchers.setStyles({'height': '0', 'overflow': 'hidden'});

window.addEvent('load', function(){
	
//initialization of togglers effects
togglers.each(function(toggler, i)
{
	toggler.color = toggler.getStyle('background-color');
	toggler.$tmp.first = toggler.getFirst();
	toggler.$tmp.fx = new Fx.Style(toggler, 'background-color', {'wait': false, 'transition': Fx.Transitions.Quart.easeOut});
});	
	
//the accordion
var myAccordion = new Accordion(togglers, stretchers, {	
	'opacity': false,
	'start': false,
	'transition': Fx.Transitions.Quad.easeOut,	
	onActive: function(toggler){
		toggler.$tmp.fx.start('#ff0000');
		toggler.$tmp.first.setStyle('color','#fff');
	},
	onBackground: function(toggler){
		toggler.$tmp.fx.stop();
		toggler.setStyle('background-color', toggler.color).$tmp.first.setStyle('color', '#222');
	}
});
	
//open the accordion section relative to the url
var found = 0;
$$('h3.toggler a').each(function(link, i)
{
	if (window.location.hash.test(link.hash)) found = i;
});
myAccordion.display(found);
	
//the draggable ball
var ball = $('header').getElement('h1');
var ballfx = new Fx.Styles(ball, {duration: 1000, 'transition': Fx.Transitions.Elastic.easeOut});
new Drag.Base(ball, {
	onComplete: function(){
		ballfx.start({'top': 13, 'left': 358});
	}
	});
});