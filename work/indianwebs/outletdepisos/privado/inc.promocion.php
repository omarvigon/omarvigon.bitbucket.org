<form action="usuario.php?modo=promocion2" method="post" name="f_inmueble">
<table cellpadding="1" cellspacing="1" class="tabla_registro">
<tr>
    <td width="200">Nombre de la Promoci&oacute;n:</td>
    <td colspan="3"><input type="text" name="nombre" maxlength="64" class="caja1" /></td>
</tr>
<tr>
    <td>Tipolog&iacute;a:</td>
    <td colspan="3"><select name="tipologia2" class="caja1" >
        <?php include("includes/opt.tipos.promo.php") ?>
        </select></td>
</tr>
<tr>
    <td>Provincia:</td>
    <td colspan="3"><select name="cp" class="caja1" onchange="cambio_provincia(this.value,5)">
        <option value="0">(Seleccionar...)</option>
        <?php include("includes/opt.provincias.php");?>
        </select></td>
</tr>
<tr>
    <td>Localidad:</td>
    <td id="celda_localidad_5" colspan="3"><select name="localidad" class="caja1">
        <option value="0">(Todos...)</option></select></td>
</tr>
<tr>
    <td>Zona / Urbanizaci&oacute;n / Barrio:</td>
    <td colspan="3"><input type="text" name="barrio" maxlength="64" class="caja1" /></td>
</tr>
<tr>
     <td>Tipo de Via:</td>
     <td colspan="3"><select name="tipovia"  class="caja1">
     <? include("includes/opt.vias.php");?>
     </select></td>
</tr>
<tr>
    <td>Direcci&oacute;n:</td>
    <td width="290"><input type="text" name="direccion" maxlength="64" class="caja1" /></td>
    <td width="40">&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>Direcci&oacute;n visible en la web:</td>
    <td colspan="3"><input type="checkbox" name="dato_visible" value="1"/></td>
</tr>
<tr>
    <td>Descripci&oacute;n de la promoci&oacute;n:</td>
    <td colspan="3"><textarea name="descripcion" cols="64" rows="8"></textarea></td>
</tr>
</table>

<table cellpadding="1" cellspacing="1" class="tabla_registro">
</tr>
<tr>
    <td width="200">Estado:</td>
    <td colspan="3"><select name="dato_estado" class="caja1">
        <?php include("includes/opt.estados.promo.php");?></select></td>
</tr>
<tr>
        <td>Zona Comunitaria:</td>
        <td width="80"><input type="checkbox" name="extra_zona" value="1" /></td>
        <td width="140">Suelo:</td>
        <td width="200"><select name="extra_suelo">
        	<option value="No">Seleccionar</option>
            <option value="Parquet">Parquet</option>
            <option value="Gres">Gres</option>
            <option value="Ceramica">Cer&aacute;mica</option>
            <option value="Moqueta">Moqueta</option></select></td>
    </tr>
    <tr>
        <td>Terraza:</td>
        <td><input type="checkbox" name="extra_terraza" value="1" /></td>
        <td>Piscina:</td>
        <td><select name="extra_piscina">
        	<option value="No">No</option>
            <option value="Privada">Privada</option>
            <option value="Comunitaria">Comunitaria</option></select></td>
    </tr>
    <tr>
        <td>Parking:</td>
        <td><input type="checkbox" name="extra_parking" value="1" /></td>
        <td>Aire acondicionado:</td>
        <td><select name="extra_aire">
            <option value="No">No</option>
            <option value="Frio">Frio</option>
            <option value="Frio-Calor">Frio/Calor</option></select></td>
    </tr>
    <tr>
        <td>Trastero:</td>
        <td><input type="checkbox" name="extra_trastero" value="1" /></td>
        <td>Jardin:</td>
        <td><select name="extra_jardin">
        	<option value="No">No</option>
            <option value="Privado">Privado</option>
            <option value="Comunitario">Comunitario</option></select></td>
    </tr>
    <tr>
        <td>Video-portero:</td>
        <td><input type="checkbox" name="extra_video" value="1" /></td>
        <td>Calefacci&oacute;n:</td>
        <td><select name="extra_calefaccion">
            <option value="No">No</option>
            <option value="Central">Central</option>
            <option value="Individual">Individual</option></select></td>
    </tr>
    <tr>
        <td>Servicio de porter&iacute;a:</td>
        <td><input type="checkbox" name="extra_porteria" value="1" /></td>
        <td>Ascensor:</td>
        <td><input type="checkbox" name="extra_ascensor" value="1" /></td>
    </tr>
    <tr>
        <td>Armarios empotrados:</td>
        <td><input type="checkbox" name="extra_armarios" value="1" /></td>
        <td>Antena parab&oacute;lica:</td>
        <td><input type="checkbox" name="extra_antena" value="1" /></td>
    </tr>
    <tr>
        <td>Persianas el&eacute;ctricas:</td>
        <td><input type="checkbox" name="extra_persianas" value="1" /></td>
        <td>Dom&oacute;tica:</td>
        <td><input type="checkbox" name="extra_domotica" value="1" /></td>
    </tr>  
<tr>
    <td colspan="4"><hr size="1" /></td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td align="center"  colspan="2"><br /><br /><input type="submit" value="   PASO 2   " /></td>
</tr>
</table>
</form>
