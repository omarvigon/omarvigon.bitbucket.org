	<form action="usuario.php?modo=inmueble2" method="post" name="f_inmueble">
    <input type="hidden" name="oculto" value="1"/>
    <table cellpadding="1" cellspacing="1" class="tabla_registro">
    <tr>
        <td>Tipolog&iacute;a:</td>
        <td colspan="3"><select name="tipologia1" class="caja1" >
            <option value="0">(Seleccionar...)</option>
            <?php include("includes/opt.tipos.php");?></select></td>
    </tr>
    <tr>
        <td>Provincia:</td>
        <td colspan="3"><select name="cp" class="caja1" onchange="cambio_provincia(this.value,4)">
            <option value="0">(Seleccionar...)</option>
            <?php include("includes/opt.provincias.php");?></select></td>
    </tr>
    <tr>
        <td>Localidad:</td>
        <td id="celda_localidad_4" colspan="3"><select name="localidad" class="caja1">
        <option value="0">(Todos...)</option></select></td>
    </tr>
    <tr>
        <td>Zona / Urbanizaci&oacute;n / Barrio:</td>
        <td colspan="3"><input type="text" name="barrio" maxlength="64" class="caja1" /></td>
    </tr>
    <tr>
        <td>Tipo de Via:</td>
        <td colspan="3"><select name="tipovia"  class="caja1">
        <? include("includes/opt.vias.php");?>
        </select></td>
    </tr>
    <tr>
        <td>Direccion:</td>
        <td><input type="text" name="direccion" maxlength="64" style="width:240px;" /></td>
        <td>Piso:</td>
        <td><input type="text" name="extra_piso" maxlength="2" size="2" />
            Puerta:<input type="text" name="extra_puerta" maxlength="2" size="2" />
            Bloque:<input type="text" name="extra_bloque" maxlength="2" size="2" />
            Esc.:<input type="text" name="extra_escalera" maxlength="2" size="2" /></td>
    </tr>
    <tr>
        <td>Direccion visible en la web:</td>
        <td colspan="3"><input type="checkbox" name="dato_visible" value="1"/></td>
    </tr>
    <tr>
        <td>Superficie (m&sup2;):</td>
        <td colspan="3"><input type="text" name="dato_metros" maxlength="4" class="caja2" /></td>
    </tr>
    <tr>
        <td>Habitaciones:</td>
        <td colspan="3"><select name="dato_habs" class="caja1">
            <option value="1">1</option><option value="2">2</option><option value="3">3</option>
            <option value="4">4</option><option value="5">5</option><option value="6">6</option>
            <option value="7">7</option><option value="8">8</option><option value="9">m&aacute;s de 8</option></select></td>
    </tr>
    <tr>
        <td>Ba&ntilde;os:</td>
        <td colspan="3"><select name="extra_banos" class="caja1">
            <option value="1">1</option><option value="2">2</option><option value="3">3</option>
            <option value="4">4</option><option value="5">5</option><option value="6">m&aacute;s de 5</option></select></td>
    </tr>
    <tr>
        <td>Descripcion:</td>
        <td colspan="3"><textarea name="descripcion" cols="64" rows="8"></textarea></td>
    </tr>
    </table>
    
    <table cellpadding="1" cellspacing="1" class="tabla_registro">
    </tr>
    <tr>
        <td width="200">Estado:</td>
        <td colspan="3"><select name="dato_estado">
            <?php include("includes/opt.estados.php");?></select></td>
    </tr>
    <tr>
        <td>Antig&uuml;edad:</td>
        <td colspan="3"><select name="extra_antiguo">
            <option value="0">(Seleccionar)</option>
            <option value="5"><?=ANT_5;?></option>
            <option value="10"><?=ANT_10;?></option>
            <option value="20"><?=ANT_20;?></option>
            <option value="25"><?=ANT_25;?></option></select></td>
    </tr>
    <tr>
        <td>Zona Comunitaria:</td>
        <td width="80"><input type="checkbox" name="extra_zona" value="1" /></td>
        <td width="140">Suelo:</td>
        <td width="200"><select name="extra_suelo">
        	<option value="No">Seleccionar</option>
            <option value="Parquet">Parquet</option>
            <option value="Gres">Gres</option>
            <option value="Ceramica">Cer&aacute;mica</option>
            <option value="Moqueta">Moqueta</option></select></td>
    </tr>
    <tr>
        <td>Terraza:</td>
        <td><input type="checkbox" name="extra_terraza" value="1" /></td>
        <td>Piscina:</td>
        <td><select name="extra_piscina">
        	<option value="No">No</option>
            <option value="Privada">Privada</option>
            <option value="Comunitaria">Comunitaria</option></select></td>
    </tr>
    <tr>
        <td>Parking:</td>
        <td><input type="checkbox" name="extra_parking" value="1" /></td>
        <td>Aire acondicionado:</td>
        <td><select name="extra_aire">
            <option value="No">No</option>
            <option value="Frio">Frio</option>
            <option value="Frio-Calor">Frio/Calor</option></select></td>
    </tr>
    <tr>
        <td>Trastero:</td>
        <td><input type="checkbox" name="extra_trastero" value="1" /></td>
        <td>Jardin:</td>
        <td><select name="extra_jardin">
        	<option value="No">No</option>
            <option value="Privado">Privado</option>
            <option value="Comunitario">Comunitario</option></select></td>
    </tr>
    <tr>
        <td>Video-portero:</td>
        <td><input type="checkbox" name="extra_video" value="1" /></td>
        <td>Calefacci&oacute;n:</td>
        <td><select name="extra_calefaccion">
            <option value="No">No</option>
            <option value="Central">Central</option>
            <option value="Individual">Individual</option></select></td>
    </tr>
    <tr>
        <td>Servicio de porter&iacute;a:</td>
        <td><input type="checkbox" name="extra_porteria" value="1" /></td>
        <td>Ascensor:</td>
        <td><input type="checkbox" name="extra_ascensor" value="1" /></td>
    </tr>
    <tr>
        <td>Armarios empotrados:</td>
        <td><input type="checkbox" name="extra_armarios" value="1" /></td>
        <td>Antena parab&oacute;lica:</td>
        <td><input type="checkbox" name="extra_antena" value="1" /></td>
    </tr>
    <tr>
        <td>Persianas el&eacute;ctricas:</td>
        <td><input type="checkbox" name="extra_persianas" value="1" /></td>
        <td>Dom&oacute;tica:</td>
        <td><input type="checkbox" name="extra_domotica" value="1" /></td>
    </tr>  
    <tr>
        <td colspan="4"><hr size="1" /></td>
    </tr>
    <tr>
        <td>Precio Inicial: (&euro;)</td>
        <td colspan="3"><input type="text" name="dato_precio" maxlength="8" class="caja2" /></td>
    </tr>
    <tr>
        <td>Descuento: (%)</td>
        <td colspan="3"><input type="text" name="dato_descuento" maxlength="3" class="caja2" /></td>
    </tr>
    <tr>
        <td><input type="button" onclick="calcular()" value="  CALCULAR PRECIO OUTLET  " /></td>
        <td colspan="3"><input type="text" name="calculado" maxlength="3" class="caja2" readonly="readonly" /></td>
    </tr>
    <tr>
        <td colspan="4"><hr size="1" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center"  colspan="2"><br /><br /><input type="submit" value="  PASO 2  " /></td>
    </tr>
    </table>
    </form>