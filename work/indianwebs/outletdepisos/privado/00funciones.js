function paso3()
{
	document.f_inmueble2.action="usuario.php?modo=inmueble3";
	document.f_inmueble2.submit();
}
function paso3promo()
{
	document.f_inmueble2.action="usuario.php?modo=promocion3";
	document.f_inmueble2.submit();
}
function paso4promo()
{
	document.f_inmueble3.action="usuario.php?modo=promocion4";
	document.f_inmueble3.submit();
}
function cargando()
{
	document.getElementById("especial").style.display="block";
}
function borrar(cual,archivo)
{
	if(cual==1)
	{
		document.f_mod_img.oculto.value="img_borra";
		document.f_mod_img.oculto2.value=archivo;
		document.f_mod_img.submit();
	}
	else
	{
		document.f_mod_file.oculto.value="file_borra";
		document.f_mod_file.oculto2.value=archivo;
		document.f_mod_file.submit();
	}
}
function popup(tipo,piso,email)
{
	window.open("http://www.outletdepisos.es/contacto.php?tipo="+tipo+"&piso="+piso+"&mail="+email,"_blank","width=560,height=400,scrollbars=no,resizable=no,directories=no,location=no,menubar=no,status=no,toolbar=no");	
}
function cambia_tipo()
{
	if(document.f_registro.tipo.value==1 || document.f_registro.tipo.value==4)
	{
		document.getElementById("file_logo1").innerHTML="Logo (*.JPG):";
		document.getElementById("file_logo2").innerHTML="<input type='file' name='logo' size='64'/>";
	}
	else
	{
		document.getElementById("file_logo1").innerHTML="&nbsp;";
		document.getElementById("file_logo2").innerHTML="&nbsp;";
	}
}
function cambio_quebusca()
{
	if(document.f_busq.quebusca.value=="200")
	{
		document.getElementById("cambio_tipo").innerHTML="<select name='tipologia' class='mid'><option value='0'>(Todos...)</option><option value='101'>Promocion de pisos</option><option value='102'>Promocion de casas pareadas</option><option value='103'>Promocion de casas independientes</option></select>";
		document.getElementById("cambio_estado").innerHTML="<select name='estado' class='mid'><option value='0'>(Todos...)</option><option value='5'>Finalizada</option><option value='6'>En construccion</option></select>";
		document.f_busq.action="http://www.outletdepisos.es/promociones.php";
	}
	else
	{
		document.getElementById("cambio_tipo").innerHTML="<select name='tipologia' class='mid'><option value='0'>(Todos...)</option><option value='1'>Piso</option><option value='5'>Apartamento</option><option value='7'>Loft</option><option value='8'>Estudio</option><option value='6'>Duplex</option><option value='4'>Atico</option><option value='10'>Planta baja con terraza</option><option value='9'>Atico duplex</option><option value='2'>Casa pareada</option><option value='3'>Casa independiente</option></select>";
		document.getElementById("cambio_estado").innerHTML="<select name='estado' class='mid'><option value='0'>(Todos...)</option><option value='1'>Obra nueva</option><option value='2'>Reformado</option><option value='3'>Buen estado</option><option value='4'>A reformar</option><option value='7'>Seminuevo</option></select>";
		document.f_busq.action="http://www.outletdepisos.es/index.php";
	}
}
function cambio_provincia(cual,numero)
{
	var texto=ajaxinclude("http://www.outletdepisos.es/includes/provincias/"+cual+".htm");
	document.getElementById("celda_localidad_"+numero).innerHTML="<select name='localidad' class='mid'><option value='0'>(Todos...)</option>"+texto+"</select>";
}
function ajaxinclude(url) 
{
	var page_request = false
	if (window.XMLHttpRequest) 
		page_request = new XMLHttpRequest()
	else if (window.ActiveXObject)
	{ 
		try {page_request = new ActiveXObject("Msxml2.XMLHTTP")} 
		catch (e)
		{
			try{page_request = new ActiveXObject("Microsoft.XMLHTTP")}
			catch (e){}
		}
	}
	else
		return false
	page_request.open('GET', url, false)
	page_request.send(null)
	if (window.location.href.indexOf("http")==-1 || page_request.status==200)
		return (page_request.responseText)
}
function validar()
{
	var errores=false;
	
	if(document.f_registro.tipo.value=="0")
		errores=true;	
	if(document.f_registro.telefono1.value=="" || isNaN(document.f_registro.telefono1.value))
		errores=true;	
	if(document.f_registro.nombre.value=="" || document.f_registro.apellidos.value=="")
		errores=true;
	if(document.f_registro.email.value.search (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig) )
		errores=true;
	if(!document.f_registro.aceptar.checked)
		errores=true;
	
	if(!errores)
		document.f_registro.submit();
	else
		alert("Los campos (*) son obligatorios");
		
}
function calcularpromo()
{
	var x1=document.f_inmueble2.dato_precio.value;
	var x2=document.f_inmueble2.dato_descuento.value;
	var x3=document.f_inmueble2.precio_outlet.value;
	
	if((x1!="") && (x2!="") && (x3!=""))
		alert("Debe dejar el campo vacio del valor que quiera calcular");
	else
	{
		if(x1=="")
		document.f_inmueble2.dato_precio.value=(x3*100)/x2;
		if(x2=="")
		document.f_inmueble2.dato_descuento.value=(x3*100)/x1;
		if(x3=="")
		document.f_inmueble2.precio_outlet.value=(x1*x2)/100;
	}
}
function calcular()
{	
	var precio=document.f_inmueble.dato_precio.value;
	var descuento=document.f_inmueble.dato_descuento.value;
	
	var x=precio-(precio*descuento/100);
	document.f_inmueble.calculado.value=x+" euros";
}
function favoritos()
{
	if(window.sidebar)
	{
		alert("Pulse Ctrl+D");
		//window.sidebar.addPanel("http://www.outletdepisos.es","OutletPisos");
	}
	else
		window.external.AddFavorite("http://www.outletdepisos.es","OutletPisos");
}
/*function iniciar()
{
	var elemento=document.getElementById("img_ficha_1");
	if (elemento.addEventListener)
		elemento.addEventListener("click",imgno,false);
	else	
		elemento.attachEvent("onclick",imgno);
}
function imgno()
{document.getElementById("img_ficha_1").src='http://www.outletdepisos.es/img/no.gif';}
*/

/*****************************sortable.js***************************************/

addEvent(window, "load", sortables_init);

var SORT_COLUMN_INDEX;

function sortables_init() {
    // Find all tables with class sortable and make them sortable
    if (!document.getElementsByTagName) return;
    tbls = document.getElementsByTagName("table");
    for (ti=0;ti<tbls.length;ti++) {
        thisTbl = tbls[ti];
        if (((' '+thisTbl.className+' ').indexOf("sortable") != -1) && (thisTbl.id)) {
            //initTable(thisTbl.id);
            ts_makeSortable(thisTbl);
        }
    }
}
function ts_makeSortable(table) {
    if (table.rows && table.rows.length > 0) {
        var firstRow = table.rows[0];
    }
    if (!firstRow) return;
    
    // We have a first row: assume it's the header, and make its contents clickable links
    for (var i=0;i<firstRow.cells.length;i++) {
        var cell = firstRow.cells[i];
        var txt = ts_getInnerText(cell);
        cell.innerHTML = '<a href="#" class="sortheader" '+ 
        'onclick="ts_resortTable(this, '+i+');return false;">' + 
        txt+'<span class="sortarrow">&nbsp;</span></a>';
    }
}
function ts_getInnerText(el) {
	if (typeof el == "string") return el;
	if (typeof el == "undefined") { return el };
	if (el.innerText) return el.innerText;	//Not needed but it is faster
	var str = "";
	
	var cs = el.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++) {
		switch (cs[i].nodeType) {
			case 1: //ELEMENT_NODE
				str += ts_getInnerText(cs[i]);
				break;
			case 3:	//TEXT_NODE
				str += cs[i].nodeValue;
				break;
		}
	}
	return str;
}
function ts_resortTable(lnk,clid) {
    // get the span
    var span;
    for (var ci=0;ci<lnk.childNodes.length;ci++) {
        if (lnk.childNodes[ci].tagName && lnk.childNodes[ci].tagName.toLowerCase() == 'span') span = lnk.childNodes[ci];
    }
    var spantext = ts_getInnerText(span);
    var td = lnk.parentNode;
    var column = clid || td.cellIndex;
    var table = getParent(td,'TABLE');
    
    // Work out a type for the column
    if (table.rows.length <= 1) return;
    var itm = ts_getInnerText(table.rows[1].cells[column]);
    sortfn = ts_sort_caseinsensitive;
    if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d\d\d$/)) sortfn = ts_sort_date;
    if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d$/)) sortfn = ts_sort_date;
    if (itm.match(/^[£$]/)) sortfn = ts_sort_currency;
    if (itm.match(/^[\d\.]+$/)) sortfn = ts_sort_numeric;
    SORT_COLUMN_INDEX = column;
    var firstRow = new Array();
    var newRows = new Array();
    for (i=0;i<table.rows[0].length;i++) { firstRow[i] = table.rows[0][i]; }
    for (j=1;j<table.rows.length;j++) { newRows[j-1] = table.rows[j]; }

    newRows.sort(sortfn);

    if (span.getAttribute("sortdir") == 'down') {
        ARROW = '&nbsp;';
        newRows.reverse();
        span.setAttribute('sortdir','up');
    } else {
        ARROW = '&nbsp;';
        span.setAttribute('sortdir','down');
    }
    
    // We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
    // don't do sortbottom rows
    for (i=0;i<newRows.length;i++) { if (!newRows[i].className || (newRows[i].className && (newRows[i].className.indexOf('sortbottom') == -1))) table.tBodies[0].appendChild(newRows[i]);}
    // do sortbottom rows only
    for (i=0;i<newRows.length;i++) { if (newRows[i].className && (newRows[i].className.indexOf('sortbottom') != -1)) table.tBodies[0].appendChild(newRows[i]);}
    
    // Delete any other arrows there may be showing
    var allspans = document.getElementsByTagName("span");
    for (var ci=0;ci<allspans.length;ci++) {
        if (allspans[ci].className == 'sortarrow') {
            if (getParent(allspans[ci],"table") == getParent(lnk,"table")) { // in the same table as us?
                allspans[ci].innerHTML = '&nbsp;';
            }
        }
    }
        
    span.innerHTML = ARROW;
}
function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}
function ts_sort_date(a,b) {
    // y2k notes: two digit years less than 50 are treated as 20XX, greater than 50 are treated as 19XX
    aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
    bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
    if (aa.length == 10) {
        dt1 = aa.substr(6,4)+aa.substr(3,2)+aa.substr(0,2);
    } else {
        yr = aa.substr(6,2);
        if (parseInt(yr) < 50) { yr = '20'+yr; } else { yr = '19'+yr; }
        dt1 = yr+aa.substr(3,2)+aa.substr(0,2);
    }
    if (bb.length == 10) {
        dt2 = bb.substr(6,4)+bb.substr(3,2)+bb.substr(0,2);
    } else {
        yr = bb.substr(6,2);
        if (parseInt(yr) < 50) { yr = '20'+yr; } else { yr = '19'+yr; }
        dt2 = yr+bb.substr(3,2)+bb.substr(0,2);
    }
    if (dt1==dt2) return 0;
    if (dt1<dt2) return -1;
    return 1;
}
function ts_sort_currency(a,b) { 
    aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).replace(/[^0-9.]/g,'');
    bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).replace(/[^0-9.]/g,'');
    return parseFloat(aa) - parseFloat(bb);
}

function ts_sort_numeric(a,b) { 
    aa = parseFloat(ts_getInnerText(a.cells[SORT_COLUMN_INDEX]));
    if (isNaN(aa)) aa = 0;
    bb = parseFloat(ts_getInnerText(b.cells[SORT_COLUMN_INDEX])); 
    if (isNaN(bb)) bb = 0;
    return aa-bb;
}
function ts_sort_caseinsensitive(a,b) {
    aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).toLowerCase();
    bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).toLowerCase();
    if (aa==bb) return 0;
    if (aa<bb) return -1;
    return 1;
}
function ts_sort_default(a,b) {
    aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
    bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
    if (aa==bb) return 0;
    if (aa<bb) return -1;
    return 1;
}
function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
} 
/*
	Lightbox JS: Fullsize Image Overlays by Lokesh Dhakar - http://www.huddletogether.com
	For more information on this script, visit: http://huddletogether.com/projects/lightbox/
	Script featured on Dynamic Drive code library Jan 24th, 06': http://www.dynamicdrive.com
	Licensed under the Creative Commons Attribution 2.5 License - http://creativecommons.org/licenses/by/2.5/ (basically, do anything you want, just leave my name and link)
	Functions
	- getPageScroll()- getPageSize()- pause()- getKey()- listenKey()
	- showLightbox()- hideLightbox()- initLightbox()- addLoadEvent()
	Function Calls- addLoadEvent(initLightbox)
*/
var loadingImage = 'http://www.outletdepisos.es/img/ico_loading.gif';		
var closeButton = 'http://www.outletdepisos.es/img/ico_close.gif';		

// Returns array with x,y page scroll values.
// Core code from - quirksmode.org
function getPageScroll(){

	var yScroll;

	if (self.pageYOffset) {
		yScroll = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop){	 // Explorer 6 Strict
		yScroll = document.documentElement.scrollTop;
	} else if (document.body) {// all other Explorers
		yScroll = document.body.scrollTop;
	}

	arrayPageScroll = new Array('',yScroll) 
	return arrayPageScroll;
}
// Returns array with page width, height and window width, height
// Core code from - quirksmode.org
// Edit for Firefox by pHaez
function getPageSize()
{	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}

	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}
// Pauses code execution for specified time. Uses busy code, not good.
// Code from http://www.faqts.com/knowledge_base/view.phtml/aid/1602
function pause(numberMillis) {
	var now = new Date();
	var exitTime = now.getTime() + numberMillis;
	while (true) {
		now = new Date();
		if (now.getTime() > exitTime)
			return;
	}
}
// Gets keycode. If 'x' is pressed then it hides the lightbox.

function getKey(e){
	if (e == null) { // ie
		keycode = event.keyCode;
	} else { // mozilla
		keycode = e.which;
	}
	key = String.fromCharCode(keycode).toLowerCase();
	
	if(key == 'x'){ hideLightbox(); }
}

function listenKey () {	document.onkeypress = getKey; }
	
// Preloads images. Pleaces new image in lightbox then centers and displays.
function showLightbox(objLink)
{
	// prep objects
	var objOverlay = document.getElementById('overlay');
	var objLightbox = document.getElementById('lightbox');
	var objCaption = document.getElementById('lightboxCaption');
	var objImage = document.getElementById('lightboxImage');
	var objLoadingImage = document.getElementById('loadingImage');
	var objLightboxDetails = document.getElementById('lightboxDetails');

	
	var arrayPageSize = getPageSize();
	var arrayPageScroll = getPageScroll();

	// center loadingImage if it exists
	if (objLoadingImage) {
		objLoadingImage.style.top = (arrayPageScroll[1] + ((arrayPageSize[3] - 35 - objLoadingImage.height) / 2) + 'px');
		objLoadingImage.style.left = (((arrayPageSize[0] - 20 - objLoadingImage.width) / 2) + 'px');
		objLoadingImage.style.display = 'block';
	}

	// set height of Overlay to take up whole page and show
	objOverlay.style.height = (arrayPageSize[1] + 'px');
	objOverlay.style.display = 'block';
	// preload image
	imgPreload = new Image();

	imgPreload.onload=function(){
		objImage.src = objLink.href;

		// center lightbox and make sure that the top and left values are not negative
		// and the image placed outside the viewport
		var lightboxTop = arrayPageScroll[1] + ((arrayPageSize[3] - 35 - imgPreload.height) / 2);
		var lightboxLeft = ((arrayPageSize[0] - 20 - imgPreload.width) / 2);
		
		objLightbox.style.top = (lightboxTop < 0) ? "0px" : lightboxTop + "px";
		objLightbox.style.left = (lightboxLeft < 0) ? "0px" : lightboxLeft + "px";


		objLightboxDetails.style.width = imgPreload.width + 'px';
		
		if(objLink.getAttribute('title')){
			objCaption.style.display = 'block';
			//objCaption.style.width = imgPreload.width + 'px';
			objCaption.innerHTML = objLink.getAttribute('title');
		} else {
			objCaption.style.display = 'none';
		}
		
		// A small pause between the image loading and displaying is required with IE,
		// this prevents the previous image displaying for a short burst causing flicker.
		if (navigator.appVersion.indexOf("MSIE")!=-1){
			pause(250);
		} 

		if (objLoadingImage) {	objLoadingImage.style.display = 'none'; }
		objLightbox.style.display = 'block';

		// After image is loaded, update the overlay height as the new image might have
		// increased the overall page height.
		arrayPageSize = getPageSize();
		objOverlay.style.height = (arrayPageSize[1] + 'px');
		
		// Check for 'x' keypress
		listenKey();

		return false;
	}
	imgPreload.src = objLink.href;
}
function hideLightbox()
{
	// get objects
	objOverlay = document.getElementById('overlay');
	objLightbox = document.getElementById('lightbox');
	objOverlay.style.display = 'none';
	objLightbox.style.display = 'none';
	// disable keypress listener
	document.onkeypress = '';
}
//Function runs on window load, going through link tags looking for rel="lightbox".
//These links receive onclick events that enable the lightbox display for their targets.
//The function also inserts html markup at the top of the page which will be used as a container for the overlay pattern and the inline image.
function initLightbox()
{
	if (!document.getElementsByTagName){ return; }
	var anchors = document.getElementsByTagName("a");

	for (var i=0; i<anchors.length; i++){
		var anchor = anchors[i];

		if (anchor.getAttribute("href") && (anchor.getAttribute("rel") == "lightbox")){
			anchor.onclick = function () {showLightbox(this); return false;}
		}
	}
	// the rest of this code inserts html looks like this:
	// <div id="overlay"><a href="#" onclick="hideLightbox(); return false;"><img id="loadingImage" /></a></div>
	// <div id="lightbox">
	//		<a href="#" onclick="hideLightbox(); return false;" title="Click anywhere to close image"><img id="closeButton" /><img id="lightboxImage" /></a>
	//		<div id="lightboxDetails">
	//			<div id="lightboxCaption"></div><div id="keyboardMsg"></div>
	//		</div>
	// </div>
	
	var objBody = document.getElementsByTagName("body").item(0);
	
	// create overlay div and hardcode some functional styles (aesthetic styles are in CSS file)
	var objOverlay = document.createElement("div");
	objOverlay.setAttribute('id','overlay');
	objOverlay.onclick = function () {hideLightbox(); return false;}
	objOverlay.style.display = 'none';
	objOverlay.style.position = 'absolute';
	objOverlay.style.top = '0';
	objOverlay.style.left = '0';
	objOverlay.style.zIndex = '90';
 	objOverlay.style.width = '100%';
	objBody.insertBefore(objOverlay, objBody.firstChild);
	
	var arrayPageSize = getPageSize();
	var arrayPageScroll = getPageScroll();

	// preload and create loader image
	var imgPreloader = new Image();
	
	// if loader image found, create link to hide lightbox and create loadingimage
	imgPreloader.onload=function(){

		var objLoadingImageLink = document.createElement("a");
		objLoadingImageLink.setAttribute('href','#');
		objLoadingImageLink.onclick = function () {hideLightbox(); return false;}
		objOverlay.appendChild(objLoadingImageLink);
		
		var objLoadingImage = document.createElement("img");
		objLoadingImage.src = loadingImage;
		objLoadingImage.setAttribute('id','loadingImage');
		objLoadingImage.style.position = 'absolute';
		objLoadingImage.style.zIndex = '150';
		objLoadingImageLink.appendChild(objLoadingImage);

		imgPreloader.onload=function(){};	//	clear onLoad, as IE will flip out w/animated gifs

		return false;
	}
	imgPreloader.src = loadingImage;

	// create lightbox div, same note about styles as above
	var objLightbox = document.createElement("div");
	objLightbox.setAttribute('id','lightbox');
	objLightbox.style.display = 'none';
	objLightbox.style.position = 'absolute';
	objLightbox.style.zIndex = '100';	
	objBody.insertBefore(objLightbox, objOverlay.nextSibling);
	
	// create link
	var objLink = document.createElement("a");
	objLink.setAttribute('href','#');
	objLink.setAttribute('title','Cerrar');
	objLink.onclick = function () {hideLightbox(); return false;}
	objLightbox.appendChild(objLink);

	// preload and create close button image
	var imgPreloadCloseButton = new Image();

	// if close button image found, 
	imgPreloadCloseButton.onload=function(){

		var objCloseButton = document.createElement("img");
		objCloseButton.src = closeButton;
		objCloseButton.setAttribute('id','closeButton');
		objCloseButton.style.position = 'absolute';
		objCloseButton.style.zIndex = '200';
		objLink.appendChild(objCloseButton);

		return false;
	}
	imgPreloadCloseButton.src = closeButton;

	// create image
	var objImage = document.createElement("img");
	objImage.setAttribute('id','lightboxImage');
	objLink.appendChild(objImage);
	
	// create details div, a container for the caption and keyboard message
	var objLightboxDetails = document.createElement("div");
	objLightboxDetails.setAttribute('id','lightboxDetails');
	objLightbox.appendChild(objLightboxDetails);

	// create caption
	var objCaption = document.createElement("div");
	objCaption.setAttribute('id','lightboxCaption');
	objCaption.style.display = 'none';
	objLightboxDetails.appendChild(objCaption);

	// create keyboard message
	/*var objKeyboardMsg = document.createElement("div");
	objKeyboardMsg.setAttribute('id','keyboardMsg');
	objKeyboardMsg.innerHTML = 'Pulse X para cerrar';
	objLightboxDetails.appendChild(objKeyboardMsg);*/
}
// Adds event to window.onload without overwriting currently assigned onload functions.
// Function found at Simon Willison's weblog - http://simon.incutio.com/
function addLoadEvent(func)
{	
	var oldonload = window.onload;
	if (typeof window.onload != 'function'){
    	window.onload = func;
	} else {
		window.onload = function(){
		oldonload();
		func();
		}
	}
}
addLoadEvent(initLightbox);	