<div id="caja_buscar">    
    <div class="bt_200">BUSQUEDAS</div>
    <form action="http://www.outletdepisos.es/index.php" name="f_busq" method="post">
    <input type="hidden" name="oculto" value="1">
    <table cellpadding="0" cellspacing="1">
    <tr>
        <td width="100">&nbsp;&iquest;Que busca?</td>
        <td><select name="quebusca" class="mid" onchange="cambio_quebusca()">
        <option value="100">Viviendas</option><option value="200">Promociones</option></select></td>
    </tr>
    <tr>
        <td width="100">&nbsp;Tipolog&iacute;a</td>
        <td id="cambio_tipo"><select name="tipologia" class="mid">
        <option value="0">(Todos...)</option>
        <?php include("opt.tipos.php");?></select></td>
    </tr>
    <tr>
        <td>&nbsp;Estado</td>
        <td id="cambio_estado"><select name="estado" class="mid">
        <option value="0">(Todos...)</option>
        <?php include("opt.estados.php");?></select></td>
    </tr>
    <tr>
        <td>&nbsp;Provincia</td>
        <td><select name="provincia" onchange="cambio_provincia(this.value,1)" class="mid">
        <option value="0">(Todos...)</option>
        <?php include("opt.provincias.php") ?></select></td>
    </tr>
    <tr>
        <td>&nbsp;Localidad</td>
        <td id="celda_localidad_1"><select name="localidad" class="mid">
        <option value="0">(Todos...)</option></select></td>
    </tr>
    <tr>
        <td>&nbsp;Precio</td>
        <td><select name="precio" class="mid">
        <option value="0">(Todos...)</option>
        <option value="100000">hasta 100.000&euro;</option>
        <option value="200000">hasta 200.000&euro;</option>
        <option value="300000">hasta 300.000&euro;</option>
        <option value="400000">hasta 400.000&euro;</option>
        <option value="500000">hasta 500.000&euro;</option>
        <option value="600000">hasta 600.000&euro;</option>
        <option value="750000">hasta 750.000&euro;</option>
        <option value="900000">hasta 900.000&euro;</option>
        <option value="1500000">hasta 1.500.000&euro;</option>
        <option value="2000000">m&aacute;s de 1.500.000&euro;</option></select></td>
    </tr>
    <tr>
        <td>&nbsp;Metros</td>
        <td><select name="metros" class="mid">
        <option value="0">(Todos...)</option>
        <option value="40">hasta 50m2</option>
        <option value="49">a partir de 50m2</option>
        <option value="74">a partir de 75m2</option>
        <option value="99">a partir de 100m2</option>
        <option value="124">a partir de 125m2</option>
        <option value="149">a partir de 150m2</option>
        <option value="199">a partir de 200m2</option>
        <option value="349">a partir de 350m2</option>
        <option value="499">a partir de 500m2</option></select></td>
    </tr>
    <tr>
        <td>&nbsp;Habitaciones</td>
        <td><select name="habitaciones" class="mid">
        <option value="0">(Todos...)</option>
        <option value="1">a partir de 1 hab</option>
        <option value="2">a partir de 2 hab</option>
        <option value="3">a partir de 3 hab</option>
        <option value="4">a partir de 4 hab</option>
        <option value="5">a partir de 5 hab</option>
        <option value="10">a partir de 10hab</option></select></td>
    </tr>
    <tr>
        <td>&nbsp;Descuento</td>
        <td><select name="descuento" class="mid">
        <option value="0">(Todos...)</option>
        <option value="1">10%-20%</option>
        <option value="2">20%-30%</option>
        <option value="3">30%-40%</option>
        <option value="4">m&aacute;s de 40%</option></select></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="right"><input type="submit" value="BUSCAR INMUEBLE" class="mid"></td>
    </tr>
    </table>
    </form>
</div> 