<?php include($_SERVER["DOCUMENT_ROOT"]."/01cabecera.php");?>
	<title>AVISO LEGAL OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS</title>
    <meta name="description" content="OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS" />
	<meta name="keywords" content="OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS" />
    <meta name="title" content="OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS" />
<?php include($_SERVER["DOCUMENT_ROOT"]."/02cabecera.php");?>  
    
    <div id="cuerpo">
    <table cellpadding="3" cellspacing="0" class="tb_piso">
    <tr>
    	<td class="lineagris">Aviso Legal</td>
    </tr>
    <tr>
    	<td>
        <p><b>1. PROPIEDAD</b></p>
        <p>El portal de Internet denominado <a href="http://www.outletdepisos.es">www.outletdepisos.es</a> (en adelante el Portal) es propiedad de:</p>
        <p>Alejandro Melero Garcia (en adelante La Propiedad)<br>CIF: 43.531.283-B<br>Passeig Frederica Montseny n&ordm;4, 3&ordm;2&ordf; <br>Montgat (Barcelona)</p>
        <p>La Propiedad se reserva el derecho de modificar las  Condiciones Generales de Uso seg&uacute;n la legislaci&oacute;n vigente del momento.</p>
        <p>Todo Usuario que utilice uno o varios servicios del  Portal reconoce y acepta expresamente todos los t&eacute;rminos y condiciones  referidos en las Condiciones Generales de Uso.</p>
        <p>Las  Condiciones Generales de Uso referidas en el documento no son excluyentes de  que exista&nbsp; la posibilidad de que para  acceder o contratar alg&uacute;n servicio espec&iacute;fico del Portal, por sus  caracter&iacute;sticas particulares, los Usuarios sean sometidos, adem&aacute;s de a las  Condiciones Generales de Uso, a sus propias condiciones particulares de uso (en  adelante las Condiciones Particulares de Uso). &Euml;stas &uacute;ltimas ser&aacute;n contempladas  como anexo a las Condiciones Generales de Uso. </p>
        <p><b>2.  USO DEL PORTAL</b></p>
        <p>En t&eacute;rminos generales, el uso del Portal no conllevar&aacute;  ning&uacute;n tipo de registro por parte del Usuario. Por el contrario, existen  determinados servicios en los que ser&aacute; necesario e imprescindible el registro  del Usuario.</p>
        <p>En dicho registro el Usuario deber&aacute; aportar los  siguientes datos personales:</p>
        <ol>
        <li>Nombre y apellidos.</li><li>Direcci&oacute;n</li>
        <li>Tel&eacute;fono</li><li>Direcci&oacute;n de correo electr&oacute;nico</li>
        <li>Contrase&ntilde;a personal para el uso del servicio</li>
        </ol>
        <p>El uso de determinados servicios estar&aacute; sujeto a una  contraprestaci&oacute;n econ&oacute;mica seg&uacute;n las Condiciones Particulares anexas a las  Condiciones Generales de Uso del Portal.</p>
        <p>El Usuario reconoce y acepta que el uso de los  contenidos y servicios ofrecidos por el Portal ser&aacute; bajo su exclusivo riesgo y  responsabilidad.</p>
        <p>El Usuario se compromete a utilizar el Portal, sus servicios y su  contenido&nbsp; conforme a la ley, la moral, y  las Condiciones Generales de Uso, as&iacute; como las Condiciones Particulares  aplicables a un servicio determinado.</p>
        <p><b>3.  PROHIBICIONES</b></p>
        <p>El Usuario se compromete expresamente a no utilizar el  contenido y los servicios del Portal para llevar a cabo actividades il&iacute;citas,  que constituyan un delito, que atenten contra los derechos humanos o incumplan  la regulaci&oacute;n sobre propiedad intelectual e industrial.</p>
        <p>En particular, el Usuario se compromete a no divulgar  ni transmitir a terceros, cualquier tipo de material e informaci&oacute;n (datos  contenidos, mensajes, dibujos, archivos de sonido e imagen, fotograf&iacute;as,  software, etc.) que sean contrarios a la ley, la moral, y las Condiciones  Generales y Particulares de Uso.</p>
        <p>Queda totalmente prohibido introducir en el Portal  cualquier contenido de tipo racista, xen&oacute;fobo, pornogr&aacute;fico y/o que atente a  los derechos humanos y en definitiva, en contra de la ley.</p>
        <p>Est&aacute;n  especialmente prohibidos el insulto gratuito, la incitaci&oacute;n a la violencia o al  odio, cualquier acci&oacute;n contraria a los derechos del menor y la infancia, la  copia y distribuci&oacute;n de material protegido por derechos de autor o de  copyright, el env&iacute;o masivo de correos electr&oacute;nicos no solicitados (conocido  como correo SPAM) o cualquier tipo de comunicaci&oacute;n similar, la extracci&oacute;n de  datos personales u otro tipo de informaci&oacute;n o software de nuestros sistemas  inform&aacute;ticos y en general cualquier otra actividad que sea contraria a la ley,  considerando que La Propiedad se rige por las leyes espa&ntilde;olas. En el caso de  detectarse a alg&uacute;n Usuario realizando dichas actividades prohibidas, La  Propiedad es libre de tomar medidas tales como impedir el acceso al infractor y  cancelar sus cuentas de Usuario, e incluso seg&uacute;n la gravedad alertar a las  autoridades competentes o emprender acciones legales para la compensaci&oacute;n del  da&ntilde;o causado.</p>
        <p><b>4.  PROPIEDAD INDUSTRIAL E INTELECTUAL</b></p>
        <p>La  Propiedad se reserva todos los derechos de  propiedad intelectual e industrial sobre los contenidos del su Portal. Quedan  prohibidos la copia, el uso comercial, la explotaci&oacute;n, modificaci&oacute;n,  reproducci&oacute;n, difusi&oacute;n, transformaci&oacute;n, distribuci&oacute;n, env&iacute;o y transmisi&oacute;n por  cualquier medio, as&iacute; como la posterior publicaci&oacute;n, exhibici&oacute;n, comunicaci&oacute;n  p&uacute;blica o representaci&oacute;n total o parcial. Si desea realizar alguna de estas  acciones, deber&aacute; obtener de La Propiedad una autorizaci&oacute;n oficial previa por  escrito.</p>
        <p>El Usuario autoriza a reproducir, divulgar, distribuir y comunicar de forma  p&uacute;blica las fotograf&iacute;as, videos e informaci&oacute;n que inserte en su anuncio para  ser publicadas en el Portal.</p>
        <p><b>5. RESPONSABILIDADES</b></p>
        <p>La Propiedad no ser&aacute; responsable de los fallos que  pudieran producirse en las comunicaciones, incluido el borrado, transmisi&oacute;n  incompleta o retrasos en la remisi&oacute;n, no comprometi&eacute;ndose tampoco a que la red  de transmisi&oacute;n est&eacute; operativa en todo momento. Tampoco podr&aacute; ser responsable en  caso de que un tercero, quebrantando las medidas de seguridad establecidas por La  Propiedad, acceda a los mensajes o datos, coloque virus inform&aacute;ticos o produzca  cualquier otro tipo de da&ntilde;o en el sistema inform&aacute;tico que pueda repercutir en  sus Usuarios.</p>
        <p>La  Propiedad en ning&uacute;n caso puede asumir ninguna responsabilidad ni da ninguna  garant&iacute;a o soporte sobre aquello que el Usuario muestra o publicita en el Portal,  ya que se entiende que la responsabilidad sobre todo lo que ofrece un anuncio,  o cualquier tipo de contenido objeto de la comunicaci&oacute;n comercial en general,  debe ser en todo caso del anunciante</p>
        <p><b>6. RESOLUCI&Oacute;N</b></p>
        <p>La Propiedad, se reserva el derecho de resolver y dar  por finiquitada la relaci&oacute;n con el Usuario de forma inmediata restringiendo su  acceso a el Portal, sin perjuicio de la responsabilidad por da&ntilde;os y perjuicios  que se pudiera derivar, si detecta un uso inadecuado del mismo incumpliendo  alguna de las Condiciones Generales o Especiales de uso.</p>
        <p>El Usuario responder&aacute; de los da&ntilde;os y perjuicios que La  Propiedad sufra de forma directa e indirecta, como consecuencia del  incumplimiento de cualquiera de las obligaciones derivadas de las Condiciones Generales  o Especiales de uso del Portal.</p>
        <p><b>7. VARIOS</b><b> </b></p>
        <p>La Propiedad ha adoptado  una Pol&iacute;tica de protecci&oacute;n de datos de car&aacute;cter personal, incluyendo el  registro de las mismas en la Agencia de Protecci&oacute;n de Datos, pero no puede  garantizar la invulnerabilidad absoluta de sus sistemas de seguridad, ni puede  garantizar la seguridad o inviolabilidad de dichos datos en su transmisi&oacute;n a  trav&eacute;s de la red.<br>
        Aunque  pone todos sus esfuerzos en ello, La Propiedad no puede garantizar la licitud,  fiabilidad y utilidad de los contenidos y servicios, as&iacute; como tampoco su  veracidad o exactitud</p>
        <p>El  Usuario acepta que los datos personales por &eacute;l facilitados o que se faciliten  en el futuro a La Propiedad puedan ser objeto de tratamiento en un fichero de  datos de car&aacute;cter personal, conforme a su Pol&iacute;tica de protecci&oacute;n de datos de  car&aacute;cter personal.</p>
        <p>La Propiedad se reserva el derecho a efectuar cuantas modificaciones estime  oportunas, pudiendo variar, eliminar o incluir nuevos servicios y/o contenidos  del Portal.</p>
        <p>El acceso y contrataci&oacute;n de cualquier tipo de servicio ofrecido por el Portal  quedar&aacute; restringido &uacute;nica y exclusivamente a mayores de 18 a&ntilde;os.</p>
        <p>La  Propiedad del Portal se reserva el derecho de poder tratar y utilizar los datos  personales de los Usuarios en otro Portal de su propiedad denominado <a href="http://www.panamainversion.com">www.panamainversion.com</a> , con las mismas  finalidades que se han indicado en relaci&oacute;n con sus productos y servicios.</p>
        <p>As&iacute; mismo, La Propiedad se  reserva el derecho de enviar y difundir entre los Usuarios a trav&eacute;s del correo  electr&oacute;nico cualquier campa&ntilde;a publicitaria y/o de inter&eacute;s para el Usuario desde  cualquiera de sus Portales de internet, previa autorizaci&oacute;n del mismo  implementada en las Condiciones Particulares de Uso.</p>
        <p><b>8.  DURACI&Oacute;N DEL SERVICIO</b></p>
        <p>La prestaci&oacute;n de los servicios del Portal tiene una duraci&oacute;n indefinida. No  obstante, La Propiedad se reserva el derecho de suspender o interrumpir de  cualquier forma y sin necesidad de preaviso, la prestaci&oacute;n de tales servicios  en el momento que convenga oportuno.</p>
        <p><b>9. JURSIDICCI&Oacute;N</b></p>
        <p>El Usuario, con expresa  renuncia a su propio fuero, acepta como legislaci&oacute;n rectora de las presentes  condiciones la espa&ntilde;ola y se somete para la resoluci&oacute;n de cuantos litigios  pudieran derivarse de las mismas a los Juzgados y Tribunales de Barcelona.<b></b></p>
	  </td>
    </tr>
    </table>
<?php include($_SERVER["DOCUMENT_ROOT"]."/03piecera.php");?>