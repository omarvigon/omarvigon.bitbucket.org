<?php include($_SERVER["DOCUMENT_ROOT"]."/01cabecera.php");?>
	<title>INVERSORES OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS</title>
    <meta name="description" content="OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS" />
	<meta name="keywords" content="OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS" />
    <meta name="title" content="OUTLET DE PISOS PORTAL INMOBILIARIO OUTLET ESPA&Ntilde;A OUTLET APARTAMENTOS" />
<?php include($_SERVER["DOCUMENT_ROOT"]."/02cabecera.php");?>  
    
    <div id="cuerpo">
    <table cellpadding="3" cellspacing="0" class="tb_piso">
    <tr>
    	<td class="lineagris">TARIFAS 2008</td>
    </tr>
    <tr>
    	<td>
		  <p><u>M&Oacute;DULO VIVIENDAS:</u></p>
          <table cellspacing="0" cellpadding="2">
          <tr>
          <td width="120">PRODUCTO</td>
          <td width="80">PVP/mes *</td>
          <td>INCLUYE</td>
          </tr>
          <tr>
          <td><b>Individual</b></td>
          <td>10&euro;</td>
          <td>1 inmueble en el M&oacute;dulo Viviendas</td>
          </tr>
          <tr>
          <td><b>Bono 5</b></td>
          <td>30&euro;</td>
          <td>Hasta 5 inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          <tr>
          <td><b>Bono 30</b></td>
          <td>99&euro;</td>
          <td>Hasta 30 inmuebles en el M&oacute;dulo  Viviendas</td>
          </tr>
          <tr>
          <td><b>Bono 60</b> </td>
          <td>149&euro;</td>
          <td>Hasta 60 inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          <tr>
          <td><b>Bono 120</b></td>
          <td>249&euro;</td>
          <td>Hasta 120 inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          <tr>
          <td><b>Bono 300</b></td>
          <td>399&euro;</td>
          <td>Hasta 300 inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          </table>
          <p><b>Para m&aacute;s de 300 inmuebles</b> <b><u>consultar</u></b><br>*Iva no  incluido</p>
          
          <p><u>M&Oacute;DULO PROMOCIONES:</u></p>
          <table cellspacing="0" cellpadding="2">
          <tr>
          <td width="120">PRODUCTO</td>
          <td width="80">PVP/mes *</td>
          <td>INCLUYE</td>
          </tr>
          <tr>
          <td><b>Promoci&oacute;n b&aacute;sica</b></td>
          <td>149&euro; </td>
          <td>Ficha de la promoci&oacute;n en el M&oacute;dulo Promociones + 30  inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          <tr>
          <td><b>Promoci&oacute;n 60</b></td>
          <td>249&euro;<b></b></td>
          <td>Ficha de la promoci&oacute;n en el M&oacute;dulo  Promociones + 60 inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          <tr>
          <td><b>Promoci&oacute;n 120</b></td>
          <td>349&euro;</td>
          <td>Ficha  de la promoci&oacute;n en el M&oacute;dulo Promociones + 120 inmuebles en el M&oacute;dulo Viviendas </td>
          </tr>
          <tr>
          <td><b>Promoci&oacute;n 300</b></td>
          <td>499&euro;<b></b></td>
          <td>Ficha de la promoci&oacute;n en  el M&oacute;dulo Promociones + 300 inmuebles en el M&oacute;dulo Viviendas</td>
          </tr>
          </table>
          <p><b>Para promociones de m&aacute;s de 300  inmuebles <u>consultar</u></b><br>*Iva no  incluido<br>
          Nota: Los  inmuebles insertados en la ficha una promoci&oacute;n, ser&aacute;n insertados  autom&aacute;ticamente por el sistema en el m&oacute;dulo de viviendas como inmuebles  independientes. Desde la ficha individual de un inmueble singular de la  promoci&oacute;n, el cliente interesado podr&aacute; acceder directamente a la ficha de la  promoci&oacute;n y viceversa.<b></b></p>
          <p><b><u>TARIFAS DE POSICIONAMIENTO Y  SUPLEMENTOS:</u></b></p>
          
          <table cellspacing="0" cellpadding="2">
          <tr>
          <td width="120">PRODUCTO</td>
          <td width="80">PVP/mes *</td>
          <td>INCLUYE</td>
          </tr>
          <tr>
          <td><b>Destacado</b></td>
          <td><b>10&euro;</b></td>
          <td>Anuncio destacado en color amarillo  en el men&uacute; de selecci&oacute;n</td>
          </tr>
          <tr>
          <td><b>Urge</b></td>
          <td><b> 10&euro;</b></td>
          <td>Cartelito URGE en el men&uacute; de selecci&oacute;n</td>
          </tr>
          <tr>
          <td><b></b><b>Posicionado</b></td>
          <td><b> 15&euro;</b></td>
          <td>Anuncio ubicado en las 5 primeras  posiciones en el men&uacute; de selecci&oacute;n seg&uacute;n su localidad</td>
          </tr>
          </table>
          <p>*Iva no  incluido</p>
          
          <p><b><u>DESCUENTOS EN LA CONTRATACI&Oacute;N DE  SERVICIOS:</u></b><b>*</b></p>
          <table cellspacing="0" cellpadding="2">
          <tr>
          <td width="100">PERIODO</td>
          <td>DESCUENTO</td>
          </tr>
          <tr>
          <td><b>1 mes</b></td>
          <td><b>0%</b></td>
          </tr>
          <tr>
          <td><b>2 meses</b></td>
          <td><b>5%</b></td>
          </tr>
          <tr>
          <td><b>3 meses</b></td>
          <td><b>10%</b></td>
          </tr>
          <tr>
          <td><b>6 meses</b></td>
          <td><b>15%</b></td>
          </tr>
          <tr>
          <td><b>1 a&ntilde;o</b></td>
          <td><b>20%</b></td>
          </tr>
          </table>
          <p><b>*</b>Descuentos aplicados directamente en el importe final  de la contrataci&oacute;n del servicio.</p>

    	</td>
    </tr>
    </table>    
<?php include($_SERVER["DOCUMENT_ROOT"]."/03piecera.php");?>